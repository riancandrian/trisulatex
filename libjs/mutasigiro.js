var base_url = window.location.pathname.split('/');

  $(document).ready(function(){
    if(($("#giro").val() != "")){
      cek();
      if(($("#bank").val() != "")){
      cek_rekening();
      
    }
    }
    $("#giro").change(cek);
    $("#bank").change(cek_rekening);
    
    // $("#cap, #tanpa_cap").change(function () {
    //   if ($("#cap").is(":checked")) {
    //       // alert('cap');
    //       $("#_cap1").removeAttr("disabled");
    //       $("#_cap2").removeAttr("disabled");
    //   }
    //   else if ($("#tanpa_cap").is(":checked")) {
    //     $("#_cap1").prop( "checked", false );
    //     $("#_cap2").prop( "checked", false );
    //     $("#_cap1").prop( "disabled", true );
    //     $("#_cap2").prop( "disabled", true );
    //   }
    // })
  })

  var PM = [];
  var temp = [];
  

  function cek() {
    var giro = $("#giro").val();
    //console.log(base_url[1]+'/MutasiGiro/getmerk');
    $.ajax({
        url : '/'+base_url[1]+'/MutasiGiro/getgiro',
        type: "POST",
        data:{id_giro:giro},
        dataType:'json',
        success: function(data)
        { 
          var html = '';
          if (data <= 0) {
            console.log('-----------data giro kosong------------');
          } else {
            //console.log('ajax giro berhasil');
            //console.log(data);
            $("#IDPerusahaan").val(data["Nama"]);
            $("#NoFaktur").val(data["Nomor_Faktur"]);
            $("#NilaiGiro").val(data["Nilai"]);
            $("#NomorGiro").val(data["Nomor_Giro"]);
            $("#JenisGiro").val(data["Jenis_Giro"]);
            $("#IDGiro").val(data["IDGiro"]);
            $("#IDBaruP").val(data["IDBaruP"]);
            // get_warna(data["Kode_Corak"]);
          }
           
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
  }

  function cek_rekening() {
    var bank = $("#bank").val();
    //console.log(base_url[1]+'/MutasiGiro/getmerk');
    $.ajax({
        url : '/'+base_url[1]+'/MutasiGiro/getbank',
        type: "POST",
        data:{id_bank:bank},
        dataType:'json',
        success: function(data)
        { 
          var html = '';
          if (data <= 0) {
            console.log('-----------data bank kosong------------');
          } else {
            //console.log('ajax bank berhasil');
            //console.log(data);
            $("#NomorRekening").val(data["Nomor_Rekening"]);
            $("#NamaBank").val(data["Nama_COA"]);
            // $("#NilaiGiro").val(data["Nilai"]);
            // $("#NomorGiro").val(data["Nomor_Giro"]);
            // get_warna(data["Kode_Corak"]);
          }
           
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
  }


  function initialization(){
    field = null;
    field = {
      "Jenis_giro" : $("#JenisGiro").val(),
      "Nama_Perusahaan" : $("#IDPerusahaan").val(),
      "IDPerusahaan" : $("#IDBaruP").val(),
      "No_Faktur" : $("#NoFaktur").val(),
      "Tanggal_Faktur" : $("#TanggalFaktur").val(),
      "IDGiro" : $("#IDGiro").val(),
      "NilaiGiro" : $("#NilaiGiro").val(),
      "NomorGiro" : $("#NomorGiro").val(),
      "Status" : $("#Status").val(),
      "Nomor_baru" : $("#NomorBaru").val(),
      "NamaBank" : $("#NamaBank").val(),
      "NomorRekening" : $("#NomorRekening").val(),
      "Nomor_lama" : '-',
    }

    return field;
  }

  $("#tambah_sementara").click(function(){
    var field = initialization();
    PM.push(field);
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);

  });

  function renderJSON(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    if(data != null){
      PM = [];
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Jenis_giro+"</td>"+
        "<td>"+PM[i].Nama_Perusahaan+"</td>"+
        "<td>"+PM[i].No_Faktur+"</td>"+
        "<td>"+PM[i].Tanggal_Faktur+"</td>"+
        "<td>"+PM[i].NilaiGiro+"</td>"+
        "<td>"+PM[i].NomorGiro+"</td>"+
        "<td>"+PM[i].Status+"</td>"+
        // "<td>"+PM[i].Nomor_baru+"</td>"+
        "<td>"+PM[i].NamaBank+"</td>"+
        "<td>"+PM[i].NomorRekening+"</td>"+
        "<td>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"+
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
        totalQty += parseInt(PM[i].NilaiGiro);
        // grandTotal += parseInt(PM[i].Sub_total);
      }
      $("#Total").val(totalQty);
      // $("#Grand_total").val(grandTotal);
      //console.log(totalQty);
     
      //-----Buat Field inputan sementara kosong lagi
    }
  }

  function renderJSONE(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    if(data != null){
      PM = [];
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        PM[i].NilaiGiro = data[i].Nilai;
        PM[i].NomorGiro = data[i].Nomor_Giro;
        PM[i].Nama_Perusahaan = '-';
        PM[i].No_Faktur = '-';
        PM[i].NamaBank = '-';
        PM[i].NomorRekening = '-';
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Jenis_giro+"</td>"+
        "<td>"+PM[i].Nama_Perusahaan+"</td>"+
        "<td>"+PM[i].No_Faktur+"</td>"+
        "<td>"+PM[i].Tanggal_Faktur+"</td>"+
        "<td>"+PM[i].NilaiGiro+"</td>"+
        "<td>"+PM[i].NomorGiro+"</td>"+
        "<td>"+PM[i].Status+"</td>"+
        // "<td>"+PM[i].Nomor_baru+"</td>"+
        "<td>"+PM[i].NamaBank+"</td>"+
        "<td>"+PM[i].NomorRekening+"</td>"+
        "<td>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"+
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
      }
  
    }
  }

  function deleteItem(i){

      if(PM[i].id !=''){
       temp.push(PM[i]);
     }

     PM.splice(i,1);
     renderJSON(PM);
   }

   function fieldPo(){
    var data1 = {
      "Tanggal" :$("#Tanggal").val(),
      "Nomor" :$("#Nomor").val(),
      "Total" :$("#Total").val(),
      "Keterangan" :$("#Keterangan").val(),
      "IDMataUang" :1,
      "Kurs" :1,
      "Batal" : "aktif",
      
    }
    return data1;
  }
   
  function save(){
    var data1 = fieldPo();
    
    console.log('-----------Prosessimpan---------------')
    // console.log(PM)
    // console.log(data1)
    $.ajax({
      url : '/'+base_url[1]+'/MutasiGiro/simpan_mutasigiro',
      type: "POST",
      data:{_data1:data1, _data2:PM},
      dataType:'json',
      success: function(data)
      { 
        zeroValue;
        console.log('----------Ajax berhasil-----');
        // console.log(data);
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        window.location.href = "/"+base_url[1]+"/MutasiGiro/index";
        // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
        // document.getElementById("simpan").setAttribute("onclick", "return false;");
        // document.getElementById("print").setAttribute("style", "cursor: pointer;");
        // document.getElementById("print").setAttribute("onclick", "return true;");
        // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/MutasiGiro/print_data_mutasigiro/'+data1['id_mutasigiro_tti']);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
           $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
  }

  function save_change(){
    var data1 = fieldPo();
    var idMG = $("#IDMG").val();
    console.log(data1);

    $.ajax({

      url : '/'+base_url[1]+'/MutasiGiro/ubah_mutasigiro',

      type: "POST",

      data: {

       "_data1" : data1,
       "_data2": PM,
       "_data3" : temp,
       "_id":idMG

     },

     // dataType: 'json',

     success: function (msg, status) {
      $('#alert').html('<div class="pesan sukses">Data berhasil Diubah</div>');
        window.location.href = "/"+base_url[1]+"/MutasiGiro/index";
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/MutasiGiro/print_data_mutasigiro/'+idMG);

    },
    error: function(msg, status, data){
      // alert("Failure"+msg+status+data);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
}



  function zeroValue(){
    $("#Tanggal").val('');
    $("#Nomor").val('');
    $("#Tanggal_selesai").val('');
    $("#corak").val('');
    $("#merk").val('');
    $("#Lot").val('');
    $("#Total_qty").val('');
    $("#PIC").val('');
    $("#Keterangan").val('');

  }
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}