/**
 * Created by ASUS on 9/24/2018.
 */
function exportexcel() {
        if ($('#keyword').val()) {
            keyword = $('#keyword').val();
        } else {
            keyword = '-';
        }

        if ($('#jenispencarian').val()) {
            search_type = $('#jenispencarian').val();
        }

        if ($('#date_from').val()) {
            date_from = $('#date_from').val();
        } else {
            date_from = '-';
        }

        if ($('#date_until').val()) {
            date_until = $('#date_until').val();
        } else {
            date_until = '-';
        }

        window.open(base_url + "Laporan_bo/exportToExcel/" + keyword + '/' + search_type + '/' + date_from + '/' + date_until);
}