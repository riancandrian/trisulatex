var base_url = window.location.pathname.split('/');

$(document).ready(function(){
  if(($("#corak").val() != "")){
    cek();
    if(($("#warna").val() != "")){
      get_warna($("#corak").val()); 
    }
  }
  if(($("#Satuan").val() != "")){
    cek2();
    
  }

  $("#corak").change(cek);
  $("#Satuan").change(cek2);
})

var PM = [];
var temp = [];


function cek() {
  var corak = $("#corak").val();
  $.ajax({
    url : '/'+base_url[1]+'/So/getmerk',
    type: "POST",
    data:{id_corak:corak},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data merk kosong------------');
      } else {
        $("#merk").val(data["Merk"]);
        $("#idbarang").val(data["IDBarang"]);
        $("#idmerk").val(data["IDMerk"]);

        get_warna(data["Kode_Corak"]);
      }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
    }
  });
}



function cek2() {
  var barang = $("#idbarang").val();
  var satuan = $("#Satuan").val();

    //console.log(base_url[1]+'/Sos/getmerk');
    $.ajax({
      url : '/'+base_url[1]+'/So/get_harga',
      type: "POST",
      data:{id_barang:barang, id_satuan:satuan},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data harga jual kosong------------');
          $('#alert').html('<div class="pesan sukses">Harga Jual Tidak Tersedia</div>');
          $('#harga-123').val('');
        } else {
            //console.log(data);
            //$("#merk").val(data["Merk"]);
            $("#harga-123").val(rupiah(data["Harga_Jual"]));
            

          }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
        }
      });
  }

  function get_warna(kodecorak){
    var _kodecorak = kodecorak;
    $.ajax({
      url : '/'+base_url[1]+'/So/getwarna',
      type: "POST",
      data:{id_kodecorak:_kodecorak},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          html = '<option value="">--Data Warna Kosong--</option>';
        } else {
          //console.log(data);
          html += '<option value="">--Silahkan Pilih Warna--</option>';
          for (var i = 0 ; i < data.length; i++) {
            html +='<option value="'+data[i]["IDWarna"]+'" data-warna="'+data[i]["Warna"]+'"> '+data[i]["Warna"]+' </option>';
          }
        }
        $("#warna").html(html);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function initialization(){
    field = null;
    field = {
      "Corak" : $("#corak").val(),
      "IDCorak": $("#corak option:selected").attr('data-corak'),
      "Merk" : $("#merk").val(),
      "IDMerk" : $("#idmerk").val(),
      "IDBarang" : $("#idbarang").val(),
      "Warna": $("#warna option:selected").attr('data-warna'),
      "IDWarna" : $("#warna").val(),
      "Qty" : $("#Qty").val(),
      "Saldo" : $("#Qty").val(),
      "Satuan" : $("#Satuan").val(),
      "IDSatuan": $("#Satuan option:selected").attr('data-satuan'),
      "Harga" : $("#harga-123").val(),
      "Sub_total" : $("#Qty").val() * $("#harga-123").val().replace(".",""),
    }

    return field;
  }

  $("#tambah_sementara").click(function(){
    var field = initialization();
    PM.push(field);
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);

  });

  function renderJSON(data){  
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    var grandTotal = 0;
    var totaldpp=0;
    if(data != null){

      if($("#corak").val()==""){
        $('#alert').html('<div class="pesan sukses">Corak Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#merk").val()==""){
        $('#alert').html('<div class="pesan sukses">Qty Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#warna").val()==""){
        $('#alert').html('<div class="pesan sukses">Warna Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#Qty").val()==""){
        $('#alert').html('<div class="pesan sukses">Qty Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#Satuan").val()==""){
        $('#alert').html('<div class="pesan sukses">Satuan Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#harga-123").val()==""){
        $('#alert').html('<div class="pesan sukses">Harga Tidak Boleh Kosong</div>');
        PM.pop();
      }else{
        PM = [];
      // $("#tabel-cart-asset").html("");
      var table = $('#tabelfsdfsf').DataTable();
      table.clear().draw();
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var totalharga= PM[i].Harga.replace(/\./g,'')* PM[i].Qty;
        var totalqty= PM[i].Qty;

        table.row.add([
          i+1,
          PM[i].IDCorak,
          PM[i].Merk,
          PM[i].Warna,
          PM[i].Qty,
          PM[i].IDSatuan,
          rupiahkoma(PM[i].Harga.replace(".","")),
          rupiahkoma(PM[i].Sub_total),
          "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+","+totalharga+","+totalqty+")'>Hapus </a>"
          ]).draw().nodes().to$().addClass('rowarray'+i);
        totalQty += parseFloat(PM[i].Qty);
        grandTotal += parseFloat(PM[i].Sub_total);


        if($("#Status_ppn").val()=="Include"){
          totaldpp += parseFloat(PM[i].Sub_total);
          dpp=  100/110*totaldpp;
          ppn= dpp*0.1;
          $("#totaldpp").val(totaldpp);
          $("#DPP").val(rupiahkoma(Math.round(dpp)));
          $("#PPN").val(rupiahkoma(Math.round(ppn)));
          total= totaldpp-ppn;
          $("#total_invoice").val(rupiahkoma(total+ppn));
        }else{
          totaldpp += parseFloat(PM[i].Sub_total);
          ppn= totaldpp*0.1;
          $("#totaldpp").val(totaldpp);
          $("#DPP").val(rupiahkoma(Math.round(totaldpp)));
          $("#PPN").val(rupiahkoma(Math.round(ppn)));
          $("#total_invoice").val(rupiahkoma(totaldpp+ppn));
        }
      }
      $("#Total_qty").val(totalQty);
      $("#Grand_total").val(grandTotal);
      //console.log(totalQty);

      //-----Buat Field inputan sementara kosong lagi
      $('#warna').val('');
      $('#Qty').val('');
      $('#corak').val("");
      $('#Satuan').val("");
      //$('input[name=corak]:selected').val('');
      $("#merk").val(data["Merk"]);
      $('input[name=Satuan]:selected').val('');
      $('#Qty').val('');
      $('#harga-123').val('');
    }
  }
}

function ubah_status_ppn()
{
  if($("#Status_ppn").val()=="Include"){
    totaldpp = $("#totaldpp").val();
    dpp=  100/110*totaldpp;
    ppn= dpp*0.1;
    $("#DPP").val(rupiahkoma(Math.round(dpp)));
    $("#PPN").val(rupiahkoma(Math.round(ppn)));
    total= totaldpp-ppn;
    $("#total_invoice").val(rupiahkoma(total+ppn));
  }else{
    totaldpp = $("#totaldpp").val();
    ppn= totaldpp*0.1;
    $("#DPP").val(rupiahkoma(Math.round(totaldpp)));
    $("#PPN").val(rupiahkoma(Math.round(ppn)));
    $("#total_invoice").val(rupiahkoma(pasrseFloat(totaldpp)+parseFloat(ppn)));
  }
}

function renderJSONE(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    if(data != null){
      PM = [];
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Corak+"</td>"+
        "<td>"+PM[i].Merk+"</td>"+
        "<td>"+PM[i].Warna+"</td>"+
        "<td>"+PM[i].Qty+"</td>"+
        "<td>"+PM[i].Satuan+"</td>"+
        "<td>"+rupiahkoma(PM[i].Harga)+"</td>"+
        "<td>"+rupiahkoma(PM[i].Sub_total)+"</td>"+
        "<td>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"+
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
      }

    }
  }

  function deleteItem(i, totalharga, totalqty){
 //  if(PM[i].id !=''){
 //   temp.push(PM[i]);
 // }
 $('.rowarray'+i).remove();
 PM.splice(i,1);
 $("#Grand_total").val(rupiahkoma(($("#Grand_total").val().replace(/\./g,'')) - parseFloat(totalharga)));
 $("#Total_qty").val(rupiahkoma($("#Total_qty").val() - parseFloat(totalqty)));

 // renderJSON(PM);
}

function fieldPo(){
  var data1 = {
    "Tanggal" :$("#Tanggal").val(),
    "Nomor" :$("#Nomor").val(),
    "IDCustomer" :$("#IDCustomer").val(),
    "TOP" : $("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo" :$("#Tanggal_jatuh_tempo").val(),
    "IDMataUang" :1,
    "Kurs" :1,
    "Total_qty" :$("#Total_qty").val(),
    "Saldo" :$("#Total_qty").val(),
    "No_po_customer" :$("#No_po_customer").val(),
    "Grand_total" :$("#Grand_total").val(),
    "Status_ppn" :$("#Status_ppn").val(),
    "DPP" :$("#DPP").val(),
    "PPN" :$("#PPN").val(),
    "Keterangan" :$("#Keterangan").val(),
    "Batal" : "aktif",

  }
  return data1;
}

function save(){
  var data1 = fieldPo();
  $("#IDCustomer").css('border', '');
  $("#No_po_customer").css('border', '');
  $("#Keterangan").css('border', '');

  if($("#IDCustomer").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Customer Tidak Boleh Kosong</div>');
    $("#IDCustomer").css('border', '1px #C33 solid').focus();
  }else if($("#No_po_customer").val()==""){
    $('#alert').html('<div class="pesan sukses">Data No PO Customer Tidak Boleh Kosong</div>');
    $("#No_po_customer").css('border', '1px #C33 solid').focus();
  }else{

    $.ajax({
      url : '/'+base_url[1]+'/So/simpan_so',
      type: "POST",
      data:{_data1:data1, _data2:PM},
      dataType:'json',
      success: function(data)
      { 
        zeroValue;
        console.log('----------Ajax berhasil-----');
        console.log(data);
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        window.location.href = "/"+base_url[1]+"/So/index";
        document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
        document.getElementById("simpan").setAttribute("onclick", "return false;");
        document.getElementById("print").setAttribute("style", "cursor: pointer;");
        document.getElementById("print").setAttribute("onclick", "return true;");
        document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/So/print_data_so/'+data1['id_so_tti']);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
  }
}

  // function save_change(){
  //   var data1 = fieldPo();
  //   var idPO = $("#IDPO").val();
  //   // console.log(data1);
  //   // console.log(idPO);
  //   // console.log(PM);
  //   // console.log(temp);
  //   console.log('----------- Prosessimpan---------------')

  //   $.ajax({
  //     url : '/'+base_url[1]+'/So/ubah_so',
  //     type: "POST",
  //     data:{_data1:data1, _data2:PM, _data3:temp, _id:idPO},
  //     dataType:'json',
  //     success: function(data)
  //     { 
  //       console.log('----------Ajax berhasil-----');
  //       console.log(data);
  //       $('#alert').html('<div class="pesan sukses">Data berhasil diubah</div>');

  //     },
  //     error: function (jqXHR, textStatus, errorThrown)
  //     {
  //       console.log(jqXHR);
  //       console.log(textStatus);
  //       console.log(errorThrown);
  //     }
  //   });
  // }

  function save_change(){
    var data1 = fieldPo();
    var idSOK = $("#IDSOK").val();
    console.log(data1);

    $.ajax({

      url : '/'+base_url[1]+'/So/ubah_so',

      type: "POST",

      data: {

        "_data1" : data1,
        "_data2": PM,
        "_data3" : temp,
        "_id":idSOK

      },

     // dataType: 'json',

     success: function (msg, status) {
       $('#alert').html('<div class="pesan sukses">Data berhasil Diubah</div>');
       window.location.href = "/"+base_url[1]+"/So/index";

       document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
       document.getElementById("simpan").setAttribute("onclick", "return false;");
       document.getElementById("print").setAttribute("style", "cursor: pointer;");
       document.getElementById("print").setAttribute("onclick", "return true;");
       document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/So/print_data_so/'+idPO);

     },
     error: function(msg, status, data){
      // alert("Failure"+msg+status+data);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
  }



  function zeroValue(){
    $("#Tanggal").val('');
    $("#Nomor").val('');
    $("#Tanggal_selesai").val('');
    $("#corak").val('');
    $("#merk").val('');
    $("#Lot").val('');
    $("#Total_qty").val('');
    $("#PIC").val('');
    $("#Keterangan").val('');

  }
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}