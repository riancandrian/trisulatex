var base_url = window.location.pathname.split('/');

  $(document).ready(function(){
    if(($("#corak").val() != "")){
      cek();
      if(($("#warna").val() != "")){
      get_warna($("#corak").val());
      
    }
    }
    $("#corak").change(cek);
  })

  var PM = [];
  var temp = [];
  

  function cek() {
    var corak = $("#corak").val();
    //console.log(base_url[1]+'/Po/getmerk');
    $.ajax({
        url : '/'+base_url[1]+'/Po/getmerk',
        type: "POST",
        data:{id_corak:corak},
        dataType:'json',
        success: function(data)
        { 
          var html = '';
          if (data <= 0) {
            console.log('-----------data kosong------------');
          } else {
            // html +='<option selected value="'+data["IDMerk"]+'"> '+data["Merk"]+'</option>';
            // // console.log('berhasil');
            // // $("#merk").val(data["IDCorak"]);
            // $("#merk").html(html);
            //console.log(data);
            $("#merk").val(data["Merk"]);
            $("#merk").data("idmerk",data["IDMerk"]);
            $("#merk").data("idbarang",data["IDBarang"]);
            get_warna(data["Kode_Corak"]);

          }
           
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
  }

  function get_warna(kodecorak){
    var _kodecorak = kodecorak;
    $.ajax({
      url : '/'+base_url[1]+'/Po/getwarna',
      type: "POST",
      data:{id_kodecorak:_kodecorak},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data warna kosong------------');
        } else {
          //console.log(data);
          html += '<option value="">--Silahkan Pilih Warna--</option>';
          for (var i = 0 ; i < data.length; i++) {
            html +='<option value="'+data[i]["IDWarna"]+'" data-warna="'+data[i]["Warna"]+'"> '+data[i]["Warna"]+' </option>';
          }
          $("#warna").html(html);
        }
         
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
    });
  }

  function initialization(){
     var field = null;
     field = {
      "IDKP": "",
      "barcode":$("#barcode").val(),
      "grade":$("#grade").val(),
      "corak":$("#corak").val(),
      "qty_yard":$("#qty_yard").val(),
      "warna":$("#warna").val(),
      "qty_meter":$("#qty_meter").val(),
      "merk":$("#merk").val(),
      "harga":$("#harga").val(),
    }

    return field;
  }

   function initialization2(){
     var field = null;
     field = {
      "IDKP": "",
      "Barcode":$("#barcode").val(),
      "Grade":$("#grade").val(),
      "Corak":$("#corak").val(),
      "Qty_yard":$("#qty_yard").val(),
      "Warna":$("#warna").val(),
      "Qty_meter":$("#qty_meter").val(),
      "Harga":$("#harga").val(),
    }

    return field;
  }


    $("#add-to-cart").click(function(){
    var field = initialization();
    // if(!findItem(field)){
      PM.push(field);
    // }
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);

  });

    $("#add-to-cart-edit").click(function(){
    var field = initialization2();
    // if(!findItem(field)){
      PM.push(field);
    // }
    setTimeout(function(){
      renderJSONE(PM);
      field = null;
    },500);

  });

      function renderJSON(data){
    if(data != null){
      if(PM == null)
        PM = [];
      var totalyard = 0;
      var totalmeter = 0;
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].barcode+"</td>"+
        "<td>"+PM[i].corak+"</td>"+
        "<td>"+PM[i].warna+"</td>"+
        "<td>"+PM[i].merk+"</td>"+
        "<td>"+PM[i].grade+"</td>"+
        "<td>"+PM[i].qty_yard+"</td>"+
        "<td>"+PM[i].qty_meter+"</td>"+
        "<td>"+PM[i].harga+"</td>"+
        "<td class='center' style='white-space:nowrap'>" +
        "<span>" +
        "  <a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
        "</span>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
        totalyard+= parseInt(PM[i].qty_yard);
        totalmeter+= parseInt(PM[i].qty_meter);
      }
      $("#total_yard").val(totalyard);
      $("#total_meter").val(totalmeter);

        }
      }

    function renderJSONE(data){
    if(data != null){
      if(PM == null)
        PM = [];
      var totalyard = 0;
      var totalmeter = 0;
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Barcode+"</td>"+
        "<td>"+PM[i].Corak+"</td>"+
        "<td>"+PM[i].Warna+"</td>"+
        "<td>"+PM[i].Grade+"</td>"+
        "<td>"+PM[i].Qty_yard+"</td>"+
        "<td>"+PM[i].Qty_meter+"</td>"+
        "<td>"+PM[i].Harga+"</td>"+
        "<td class='center' style='white-space:nowrap'>" +
        "<span>" +
        "  <a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItemEdit("+i+")'>Hapus </a>"+
        "</span>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
        totalyard+= parseInt(PM[i].Qty_yard);
        totalmeter+= parseInt(PM[i].Qty_meter);
      }
      $("#total_yard").val(totalyard);
      $("#total_meter").val(totalmeter);

        }
      }

            function deleteItem(i){

        if(PM[i].id !=''){
         temp.push(PM[i]);
       }

       PM.splice(i,1);
       renderJSON(PM);
     }

     function deleteItemEdit(i){

        if(PM[i].id !=''){
         temp.push(PM[i]);
       }

       PM.splice(i,1);
       renderJSONE(PM);
     }

     function fieldPersediaan(){
      var data1 = {
        "IDKP" : $("#IDKP").val(),
        "tanggal" :$("#tanggal").val(),
        "total_yard":$("#total_yard").val(),
        "no_kp":$("#no_kp").val(),
        "jenis":$("#jenis").val(),
        "total_meter":$("#total_meter").val(),
        "keterangan":$("#keterangan").val()
      }
      return data1;
    }

    function fieldPersediaan2(){
      var data1 = {
        "IDKP" : $("#IDKP").val(),
        "tanggal" :$("#tanggal").val(),
        "total_yard":$("#total_yard").val(),
        "no_kp":$("#no_kp").val(),
        "jenis":$("#jenis").val(),
        "total_meter":$("#total_meter").val(),
        "keterangan":$("#keterangan").val()
      }
      return data1;
    }

     function SavePersediaan(){
     var data1 = fieldPersediaan();
     console.log(data1);
     console.log(PM);

     $.ajax({

      url: '/'+base_url[1]+'/KoreksiPersediaan/simpan_koreksi_persediaan',

      type: "POST",

      data: {

       "data1" : data1,
       "data2": PM

     },

     dataType: 'json',

     success: function (msg, status) {
      // window.location.href = "index";
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/Pembelian_asset/print/'+data1['Nomor']);

    },
    error: function(msg, status){
      console.log(msg);
     $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
   }

      function savePersediaanChange(){
    var data1 = fieldPersediaan2();
    console.log(data1);
    console.log(PM);

    $.ajax({

      url: '/'+base_url[1]+'/KoreksiPersediaan/ubah_koreksi_persediaan',

      type: "POST",

      data: {

       "data1" : data1,
       "data2": PM,
       "data3" : temp

     },

     // dataType: 'json',

     success: function (msg, status) {
   $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/KoreksiPersediaan/print/'+data1['Nomor']);


    },
    error: function(msg, status, data){
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });


  }