 var base_url = window.location.pathname.split('/');
 document.getElementById("print_").disabled = true;
 function Penerimaan(){
    this.IDAsset;
    this.Qty;
    this.Harga_Satuan;
    this.IDPOAssetDetail;
    this.IDPOAsset;
    this.Nama_Asset;
    this.Subtotal;
}

var PM = new Array();
var Total = new Array();
index = 0;

$(document).ready(function(){
$("#no_sjc").change(function(){
    no_sjc = $('#no_sjc').val();
    $.post(baseUrl + 'Penerimaan_barang_asset/check_PO', {'IDPOAsset' : no_sjc})
    .done(function(response) {

        var json = JSON.parse(response);
            //console.log(json);
            if (!json.error) {
                $.each(json.data, function (key, value) {
                    $("#id_po_umum").val(value.IDPOAsset);
                    var M = new Penerimaan();
                    if(Total == null)
                        Total = new Array();
                    M.IDAsset           = value.IDAsset;
                    M.Qty               = value.Saldo;
                    M.Harga_Satuan      = value.Harga_Satuan;
                    M.IDPOAssetDetail   = value.IDPOAssetDetail;
                    M.IDPOAsset         = value.IDPOAsset;
                    M.Nama_Asset        = value.Nama_Asset;
                    M.Subtotal          = value.Subtotal;

                    PM[index] = M;
                    index++;
                    var table = $('#tabelfsdfsf').DataTable();
                    table.clear().draw();
                    Total_qty = 0;
                    Total_harga = 0;
                    for(i=0; i < index; i++){

                        table.row.add([
                          i+1,
                          PM[i].Nama_Asset,
                          "<input type='text' name='qty_e' id='qty_e"+i+"' value='"+PM[i].Qty+"'>",
                          rupiah(PM[i].Harga_Satuan)
                          ]).draw().nodes().to$().addClass('rowarray'+i);

                        Total_qty += parseInt(PM[i].Qty);
                        Total_harga += parseInt(PM[i].Harga_Satuan);
                    }
                    $("#total_qty").val(Total_qty);
                    $("#total_harga").val(rupiah(Total_harga));

                });

            } else if($("#no_sjc").val()==''){
                
            }else {
                $('#alert').html('<div class="pesan sukses">Tidak Ada Data Dengan No PO Tersebut</div>');
            }

        });
    });
});
// }


function save() {
 $("#id_supplier").css('border', '');
 $("#no_sjc").css('border', '');

 if($("#id_supplier").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Supplier Tidak Boleh Kosong</div>');
    $("#id_supplier").css('border', '1px #C33 solid').focus();
}else if($("#no_sjc").val()==""){
    $('#alert').html('<div class="pesan sukses">Data PO Tidak Boleh Kosong</div>');
    $("#no_sjc").css('border', '1px #C33 solid').focus();
}else{
   for(i=0; i < index; i++){
    PM[i].Qty = $("#qty_e"+i).val();
}
console.log(PM);
$.post(baseUrl + 'Penerimaan_barang_asset/store', {"data" : PM, 'Total_qty' : $('#total_qty').val(), 'Total_harga' : $('#total_harga').val(), 'id_supplier' : $('#id_supplier').val(), Keterangan : $('#keterangan').val(), id_po_umum : $('#id_po_umum').val(), tanggal : $('#tanggal').val(), no_pb : $('#no_bo').val()})
.done(function(res) {
    console.log("Fsafafa");
    $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
    window.location.href = "/"+base_url[1]+"/Penerimaan_barang_asset/create";
});
}
}

var PM = [];
var temp = [];

function renderJSON(data){
    if(data != null){
        if(PM == null)
            PM = [];
        var table = $('#tabelfsdfsf').DataTable();
        table.clear().draw();
        Total_qty= 0;
        Total_harga= 0;
        for(i = 0; i<data.length;i++){
            PM[i] = data[i];
            table.row.add([
              i+1,
              PM[i].Nama_Asset,
              PM[i].Qty,
              rupiah(PM[i].Harga_Satuan),
              rupiah(PM[i].Qty*PM[i].Harga_Satuan)
              ]).draw().nodes().to$().addClass('rowarray'+i);
            Total_qty = parseInt(PM[i].Qty);
            Total_harga = parseInt(PM[i].Harga_Satuan);
        }

        $('#total_qty').val(Total_qty);
        $('#total_harga').val(rupiah(Total_harga));
    }
}

function deleteItem(i){

    if(PM[i].id !=''){
        temp.push(PM[i]);
    }

    $('.rowarray'+i).remove();
    PM.splice(i,1);
}

function field(){
    var data = {
        "IDTBS" : $("#IDTBS").val(),
        "Tanggal" :$("#Tanggal").val(),
        "IDSupplier":$("#id_supplier").val(),
        "Keterangan":$("#Keterangan").val()
    }
    return data;
}

function saveChange(){
    var data = field();
    
    $.ajax({

        url: "../update_",
        type: "POST",
        data: {
            "data1" : data,
            "data2" : temp
        },

        success: function (msg, status) {
            console.log(msg);
            $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
            window.location.href = "/"+base_url[1]+"/Penerimaan_barang_asset";
            document.getElementById("simpan_").disabled = true;
            document.getElementById("print_").disabled = false;
        },
        error: function(msg, status, data){
            console.log(msg);
            alert("Failure "+msg+status+data);
        }

    });
}

function remove_data() {
    var row = $(this).parent().parent().parent();
    row.remove();
}

function print_cek() {
    // sjc = $('#no_bo').val();

    window.open(baseUrl +"Penerimaan_barang_umum2/print_data/");
}

function print_cek_edit() {
    sjc = $('#IDTBS').val();

    window.open(baseUrl +"Penerimaan_barang_umum2/print_data_edit/" + sjc);
}

