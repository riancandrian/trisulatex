      
      function renderJSONE(data){
        if(data != null){
          if(PM == null)
            PM = [];
          $("#tb_preview_penerimaan").html("");
          for(i = 0; i<data.length;i++){
            PM[i] = data[i];
            value = "<tr>"+
            "<td class='text-center'>"+(i+1)+"</td>"+
            "<td>"+PM[i].Barcode+"</td>"+
            "<td>"+PM[i].Corak+"</td>"+
            "<td>"+PM[i].Warna+"</td>"+
            "<td>"+PM[i].Merk+"</td>"+
            "<td>"+PM[i].Qty_yard+"</td>"+
            "<td class='text-center'>"+PM[i].Qty_meter+"</td>"+
            "<td>"+PM[i].Satuan+"</td>"+
            "<td>"+rupiah(PM[i].Harga)+"</td>"+
            "<td>"+rupiah(PM[i].Sub_total)+"</td>"+
            "</tr>";
            $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);
             if($("#Status_ppn").val()=="Include"){
              totaldpp += parseFloat(PM[i].Sub_total);
              ppn= totaldpp*0.1;
              $("#DPP").val(rupiah(totaldpp-ppn));
              $("#PPN").val(rupiah(ppn));
              total= totaldpp-ppn;
              $("#total_invoice").val(rupiah(total+ppn));
          }else{
              totaldpp += parseFloat(PM[i].Sub_total);
              ppn= totaldpp*0.1;
              $("#DPP").val(rupiah(totaldpp));
              $("#PPN").val(rupiah(ppn));
              $("#total_invoice").val(rupiah(totaldpp+ppn));
          }
          }

           $("#Total").val(totaldpp);

        }
      }

      function fieldInvoice(){
      var data1 = {
        "IDFBA" : $("#IDFBA").val(),
        "Tanggal" :$("#Tanggal").val(),
        "Nomor":$("#Nomor").val(),
        "Total_nilai_perolehan":$("#Total_nilai_perolehan").val(),
        "Total_akumulasi_penyusutan":$("#Total_akumulasi_penyusutan").val(),
        "Total_nilai_buku":$("#Total_nilai_buku").val()
      }
      return data1;
    }

    function ubah_status_ppn()
{
  disc = $("#Discount").val();
 if($("#Status_ppn").val()=="Include"){
  ppn= $("#Total").val()*0.1;
  $("#DPP").val(rupiah($("#Total").val()-ppn));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= $("#Total").val()-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}else if($("#Status_ppn").val()=="Nonppn"){
  ppn= 0;
  $("#DPP").val(rupiah($("#Total").val()));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= $("#Total").val()-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}else{
  ppn= $("#Total").val()*0.1;
  $("#DPP").val(rupiah($("#Total").val()));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}
}


      