 var base_url = window.location.pathname.split('/');
 document.getElementById("print_").disabled = true;
 function Penerimaan(){
     this.IDTBS;
     this.Barcode;
     this.Corak;
     this.Warna;
     this.Merk;
     this.Qty_yard;
     this.Qty_meter;
     this.Grade;
     this.Satuan;
     this.NoPO;
     this.NoSO;
     this.Party;
     this.Indent;
     this.Lebar;
     this.Tanggal;
     this.NoPB;
     this.ID_supplier;
     this.NoSJ;
     this.Keterangan;
     this.Total_yard;
     this.Total_meter;
     this.Harga;
     this.Total_harg;
 }

 var PM = new Array();
 var Total = new Array();
 index = 0;

/*var PB = document.getElementById("no_sjc");
PB.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {*/

        // function search_sjc() {
            $(document).ready(function(){
                $("#no_sjc").change(function(){
                    $(".rowid").remove();
    // PM = [];
    no_sjc = $('#no_sjc').val();
    //alert($('#no_sjc').val());

    $.post(baseUrl + 'PenerimaanBarang/check_SJC', {'no_sjc' : no_sjc})
    .done(function(response) {

        var json = JSON.parse(response);
            //console.log(json);
            if (!json.error) {
                index=0;
                $.each(json.data, function (key, value) {

                    $('#no_so').val(value.NoSO);
                    $('#no_po').val(value.NoPO);

                    var M = new Penerimaan();

                    if(Total == null)
                        Total = new Array();

                    M.Barcode = value.Barcode;
                    M.Corak = value.Corak;
                    M.Warna = value.Warna;
                    M.Remark = value.Remark;
                    M.Qty_yard = value.Qty_yard;
                    M.Qty_meter = value.Qty_meter;
                    M.Grade = value.Grade;
                    M.Satuan = value.Satuan;
                    M.NoPO = value.NoPO;
                    M.NoSO = value.NoSO;
                    M.Party = value.Party;
                    M.Indent = value.Indent;
                    M.Lebar = value.Lebar;
                    M.Merk = value.Merk;
                    M.NoSJ = no_sjc;
                    M.Harga = value.Harga;
                    M.IDTBS = $('#IDTBS').val();
                    M.Tanggal = $('#tanggal').val();
                    M.ID_supplier = $('#id_supplier').val();
                    M.NoPB = $('#no_bo').val();
                    M.Keterangan = $('#keterangan').val();
                    M.Keterangan = $('#keterangan').val();
                    M.Total_harga = value.Total_harga;
                    //M.Total_yard = parseInt(M.Total_yard) + parseInt(value.Qty_yard);
                    //M.Total_meter = parseInt(M.Total_meter) + parseInt(value.Qty_meter);

                    PM[index] = M;
                    index++;

                    // $("#tb_preview_penerimaan").html("");
                    Total_yard = 0;
                    Total_meter = 0;
                    Total_harga=0;
                    var table = $('#tabelfsdfsf').DataTable();
                    table.clear().draw();
                    for(i=0; i < index; i++){
                        table.row.add([
                            i+1,
                            PM[i].Barcode,
                            PM[i].Corak,
                            PM[i].Warna,
                            PM[i].Merk,
                            PM[i].Qty_yard,
                            PM[i].Qty_meter,
                            PM[i].Grade,
                            PM[i].Satuan,
                            // rupiah(PM[i].Harga)
                            ]).draw();
                        $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);

                        Total_yard = parseFloat(Total_yard) + parseFloat(PM[i].Qty_yard);
                        Total_meter = parseFloat(Total_meter) + parseFloat(PM[i].Qty_meter);
                        Total_harga += parseFloat(PM[i].Harga);
                    }

                    M.Total_yard = parseFloat(Total_yard);
                    M.Total_meter = Total_meter;

                    Total[0] = M.Total_yard;
                    Total[1] = M.Total_meter;
                    $("#total_harga").val(rupiah(Total_harga));
                    $('#total_meter').val(M.Total_meter);
                    $('#total_yard').val(M.Total_yard);

                });

            } else if($("#no_sjc").val()==''){
                
            }else {
                $('#alert').html('<div class="pesan sukses">Tidak ada data dengan nomor SJ Tersebut</div>');
            }

        });
});
            });
// }


function save() {
    $("#no_sjc").css('border', '');

    if($("#no_sjc").val()==""){
        $('#alert').html('<div class="pesan sukses">Data No SJC Tidak Boleh Kosong</div>');
        $("#no_sjc").css('border', '1px #C33 solid').focus();
    }else{
        $.post(baseUrl + 'PenerimaanBarang/store', {"data" : PM, "Total" : Total, 'id_supplier' : $('#id_supplier').val(), Keterangan : $('#keterangan').val(), total_harga : $('#total_harga').val()})
        .done(function(res) {
            $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
            document.getElementById("simpan_").disabled = true;
            document.getElementById("simpan_").setAttribute("style", "cursor: no-drop;");
            document.getElementById("print_").disabled = false;
            document.getElementById("print_").setAttribute("style", "cursor: pointer;");

        });
    } 
}

var PM = [];
var temp = [];

function renderJSON(data){
    if(data != null){
        if(PM == null)
            PM = [];
        console.log(data);
        $("#tabel-cart-asset").html("");
        Total_yard = 0;
        Total_meter = 0;
        for(i = 0; i<data.length;i++){
            PM[i] = data[i];
            var value =
            "<tr>" +
            "<td>"+(i+1)+"</td>"+
            "<td>"+PM[i].Barcode+"</td>"+
            "<td>"+PM[i].Corak+"</td>"+
            "<td>"+PM[i].Warna+"</td>"+
            "<td>"+PM[i].Merk+"</td>"+
            "<td class='text-center'>"+PM[i].Qty_yard+"</td>"+
            "<td class='text-center'>"+PM[i].Qty_meter+"</td>"+
            "<td>"+PM[i].Grade+"</td>"+
            "<td>"+PM[i].Satuan+"</td>"+
            // "<td>"+rupiah(PM[i].Harga)+"</td>"+
            "</tr>";
            $("#tabel-cart-asset").append(value);
            Total_yard = parseFloat(Total_yard) + parseFloat(PM[i].Qty_yard);
            Total_meter = parseFloat(Total_meter) + parseFloat(PM[i].Qty_meter);
            Total_meter2= Total_meter.toFixed(2)
        }
        
        $('#total_meter').val(Total_meter2);
        $('#total_yard').val(Total_yard);
    }
}

function deleteItem(i){

    if(PM[i].id !=''){
        temp.push(PM[i]);
    }

    PM.splice(i,1);
    renderJSON(PM);
}

function field(){
    var data = {
        "IDTBS" : $("#IDBTS").val(),
        "Tanggal" :$("#Tanggal").val(),
        "IDSupplier":$("#id_supplier").val(),
        "Keterangan":$("#Keterangan").val()
    }
    return data;
}

function saveChange(){
    var data = field();
    // console.log($("#Keterangan").val());

    $.ajax({

        url: "../update_",
        type: "POST",
        data: {
            "data1" : data,
            "data2" : temp
        },

        success: function (msg, status) {
            // window.location.href = "../index";
            $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');

            document.getElementById("simpan_").disabled = true;
            document.getElementById("print_").disabled = false;
        },
        error: function(msg, status, data){
            alert("Failure "+msg+status+data);
        }

    });
}

function remove_data() {
    var row = $(this).parent().parent().parent();
    row.remove();
}

function print_cek() {
    // sjc = $('#no_bo').val();

    window.open(baseUrl +"PenerimaanBarang/print_data/");
}

function print_cek_edit() {
    sjc = $('#IDBTS').val();

    window.open(baseUrl +"PenerimaanBarang/print_data_edit/" + sjc);
}