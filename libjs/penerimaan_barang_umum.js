/**
 * Created by ASUS on 9/11/2018.
 */
 var base_url = window.location.pathname.split('/');
 document.getElementById("print_").disabled = true;

 $(document).ready(function(){
  if(($("#corak").val() != "")){
    cek();
    if(($("#warna").val() != "")){
      get_warna($("#corak").val());

    }
  }
  $("#corak").change(cek);


})

 var PM = [];
 var temp = [];


 function cek() {
  var corak = $("#corak").val();
    //console.log(base_url[1]+'/Po/getmerk');
    $.ajax({
      url : '/'+base_url[1]+'/Po/getmerk',
      type: "POST",
      data:{id_corak:corak},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data kosong------------');
        } else {
            // html +='<option selected value="'+data["IDMerk"]+'"> '+data["Merk"]+'</option>';
            // // console.log('berhasil');
            // // $("#merk").val(data["IDCorak"]);
            // $("#merk").html(html);
            //console.log(data);
            $("#merk").val(data["Merk"]);
            get_warna(data["Kode_Corak"]);

          }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
        }
      });
  }

  function cek_supplier()
  {
    var totalinv=0;
    var Nomor = $("#no_po").val();
    $.ajax({
      url : '/'+base_url[1]+'/penerimaan_barang_umum/cek_supplier',
      type: "POST",
      data:{Nomor:Nomor},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data kosong------------');
        } else {
         $("#NamaSupplier").val(data[0].Nama);
         $("#id_supplier").val(data[0].IDSupplier);
       }
     },
     error: function (jqXHR, textStatus, errorThrown)
     {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
    }
  });
  }

  function get_warna(kodecorak){
    var _kodecorak = kodecorak;
    $.ajax({
      url : '/'+base_url[1]+'/Po/getwarna',
      type: "POST",
      data:{id_kodecorak:_kodecorak},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data warna kosong------------');
        } else {
          //console.log(data);
          html += '<option value="">--Silahkan Pilih Warna--</option>';
          for (var i = 0 ; i < data.length; i++) {
            html +='<option value="'+data[i]["Warna"]+'" data-warna="'+data[i]["IDWarna"]+'"> '+data[i]["Warna"]+' </option>';
          }
          $("#warna").html(html);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }


  function getCorak(selected)
  {
    var corak = '';
    $.ajax({
      type: "POST",
      async: false,
      url: baseUrl + "Penerimaan_barang_umum/get_corak_by_id",
      data: {
        'id': selected
      },
      success: function (response) {
        var json = JSON.parse(response);
            //console.log(json);
            if (!json.error) {
              corak = json['Corak'];
                //$.each(json.data, function (key, value) {
                //    corak = value.Corak;
                //});
              } else {
                corak = '';
              }
            },
            error: function (e) {
              console.log(e);
            }
          });
    //console.log(corak);
    return corak;
  }

  function list_satuan(selected)
  {
    $.post(baseUrl + "Penerimaan_barang_umum/get_satuan")
    .done(function(data){
            //console.log(data);
            res = $.parseJSON(data);
            $("#vsatuan").html("<option value='' disabled>Pilih Satuan</option>");
            for(i=0; i<res.length; i++){
              sel = "";
              if(res[i].Satuan == selected)
                sel = "selected";
              $("#vsatuan").html($("#vsatuan").html()+"<option value='"+res[i].Satuan+"' "+sel+">"+res[i].Satuan+"</option>");
            }
          });
  }


  function list_corak(selected) {
    $.post(baseUrl + "Penerimaan_barang_umum/get_corak")
    .done(function(data){

      res = $.parseJSON(data);
      $("#vcorak").html("<option value='' disabled>Pilih Corak</option>");
      for(i=0; i<res.length; i++){
        sel = "";
        if(res[i].IDCorak == selected)
          sel = "selected";
        $("#vcorak").html($("#vcorak").html()+"<option value='"+res[i].IDCorak+"' "+sel+">"+res[i].Corak+"</option>");
      }
    });
  }

  function list_warna(id_corak, selected) {
    //console.log(id_corak, selected);
    $.post(baseUrl + "Penerimaan_barang_umum/getColorByStyle", {"id_corak" : id_corak})
    .done(function(result){
      console.log(result);
            //res = $.parseJSON(result);
            var json = JSON.parse(result);
            if (!json.error) {
              var res = json.data;
              $("#vwarna").html("<option value='' disabled>Pilih Warna</option>");
              for (i = 0; i < res.length; i++) {
                sel = "";
                if (res[i].Warna == selected)
                  sel = "selected";
                $("#vwarna").html($("#vwarna").html() + "<option value='" + res[i].Warna + "' " + sel + ">" + res[i].Warna + "</option>");
              }
            }
          });
  }

  function getColor() {
    $('#vwarna option').remove();

    var selected = $('#vcorak');
    $.ajax({
      url: baseUrl + 'Penerimaan_barang_umum/getColorByStyle',
      type: "POST",
      data : {
        'id_corak' : selected.find(":selected").val()
      },
      success: function (result) {
        var json = JSON.parse(result);
        if (!json.error) {
          var datas = json.data;
                //console.log(datas);
                $('#vwarna').append(
                  $("<option></option>").attr("value", '').text('Pilih Warna').attr("selected", '').attr("disabled", '')
                  );
                $.each(datas, function( key, value ) {

                  $('#vwarna').append(
                    $("<option></option>").attr("value", value.Warna).text(value.Warna)
                    );

                });
              }
            },
            error: function (e) {
              console.log('blum dipilih');
            }
          });
  }

  function Penerimaan(){
    this.Barcode;
    this.Corak;
    this.Warna;
    this.Merk;
    this.Qty_yard;
    this.Qty_meter;
    this.Grade;
    this.Satuan;
    this.NoPO;
    this.NoSO;
    this.Party;
    this.Indent;
    this.Lebar;
    this.Tanggal;
    this.NoPB;
    this.ID_supplier;
    this.NoSJ;
    this.Keterangan;
    this.Total_yard;
    this.Total_meter;
    this.IDTBSDetail;
  }

// var PM = new Array();
// var Total = new Array();
// index = 0;
// Total_yard = 0;
// Total_meter = 0;

// function TambahPembelianPreview(){
//     if(PM == null)
//         PM = new Array();
//     var M = new Penerimaan();

//     if(Total == null)
//         Total = new Array();

//     M.Barcode = $('#barcode').val();
//     M.Corak = $('#corak').val();
//     M.Warna = $('#warna').val();
//     M.Qty_yard = $('#qty_yard').val();
//     M.Qty_meter = $('#qty_meter').val();
//     M.Grade = $('#grade').val();
//     M.Satuan = $('#satuan').val();
//     M.NoPO = $('#no_po').val();
//     M.NoSO = $('#no_so').val();
//     M.Merk = $('#merk').val();
//     M.NoSJ = $('#no_sjc').val();
//     M.Tanggal = $('#tanggal').val();
//     M.ID_supplier = $('#id_supplier').val();
//     M.NoPB = $('#no_bo').val();
//     M.Keterangan = $('#keterangan').val();


//     if(M.Barcode != ""  && M.Corak != "" && M.Warna != "" && M.Qty_yard > 0 && M.Qty_meter != "" && M.ID_supplier != ""){
//         //$('#tb_preview_penerimaan').css({"visibility":"visible","height":0});
//         PM[index] = M;
//         index++;

//         $("#barcode").val('').change();
//         $("#corak").val('').change();
//         $("#warna").val('').change();
//         $("#qty_yard").val('');
//         $("#qty_meter").val('');
//         $("#grade").val('');
//         $("#satuan").val('').change();
//         $('#merk').val('');
//         $("#no_po").not(":selected").attr("disabled", "disabled").val(M.NoPO);
//         $("#no_so").not(":selected").attr("disabled", "disabled").val(M.NoSO);
//         $('#no_sjc').not(":selected").attr("disabled", "disabled").val(M.NoSJ);
//         $('#tanggal').not(":selected").attr("disabled", "disabled").val(M.Tanggal);
//         $("#id_supplier").not(":selected").attr("disabled", "disabled").val(M.ID_supplier);
//         //$('#no_bo').val();
//         //$('#keterangan').val();

//         if (Total[0] == 0 || Total[0] == null) {
//             //console.log('jika tidak ' + Total[0] + ' ' + Total[1]);
//             Total_yard = parseFloat(Total_yard) + parseFloat(M.Qty_yard);
//             Total_meter = parseFloat(Total_meter) + parseFloat(M.Qty_meter);
//             M.Total_meter = Total_meter;
//             M.Total_yard = Total_yard;

//             Total[0] = M.Total_yard;
//             Total[1] = M.Total_meter;
//         } else {
//             //console.log('jika ada ' + Total[0] + ' ' + Total[1] + ' : ' + M.Qty_yard + ' ' + M.Qty_meter);
//             Total[0] = parseFloat(Total[0]) + parseFloat(M.Qty_yard);
//             Total[1] = parseFloat(Total[1]) + parseFloat(M.Qty_meter);
//         }

//         $('#total_yard').val(Total[0]);
//         $('#total_meter').val(Total[1]);

//         $("#tabel-cart-asset").html("");
//         for(i=0; i<index; i++){
//             value = "<tr>"+
//             "<td class='text-center'>"+(i+1)+"</td>"+
//             "<td id='barcode_tmp' class='text-left'>"+PM[i].Barcode+"</td>"+
//             "<td class='text-left'>"+PM[i].Corak+"</td>"+
//             "<td class='text-left'>"+PM[i].Warna+"</td>"+
//             "<td class='text-left'>"+PM[i].Merk+"</td>"+
//             "<td class='text-left'>"+PM[i].Qty_yard+"</td>"+
//             "<td class='text-left'>"+PM[i].Qty_meter+"</td>"+
//             "<td class='text-left'>"+PM[i].Grade+"</td>"+
//             "<td class='text-left'>"+PM[i].Satuan+"</td>"+
//             "<td class='text-center'>" +
//             "<span>"+
//             "<a href='#' class='action-icons c-edit' id='bootbox-confirm' onclick='editList("+i+")' title='Ubah Data'>Ubah </a>"+
//             "</span>"+
//             "<span>" +
//             "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
//             "</span>" +
//             "</td>" +
//             "</tr>";
//             $("#tabel-cart-asset").html($("#tabel-cart-asset").html()+value);
//         }
//     } else {
//         alert("Mohon Isi Data Dengan Benar!");
//     }
// }

// function deleteList(i) {
//     Total[0] = (parseFloat(Total[0]) - parseFloat(PM[i].Qty_yard));
//     Total[1] = (parseFloat(Total[1]) - parseFloat(PM[i].Qty_meter));

//     $('#total_yard').val(Total[0]);
//     $('#total_meter').val(Total[1]);

//     PM.splice(i,1);
//     renderPreview(PM);
// }

var PM = [];
var temp = [];

function initialization23(){
 var field = null;
 if($("#satuan").val()=='Yard'){
   field = {
    "IDTBS": "",
    "Barcode":$("#barcode").val(),
    "Corak":$("#corak").val(),
    "Warna":$("#warna").val(),
    "Merk":$("#merk").val(),
    "Qty_yard":$("#qty_yard").val(),
    "Qty_meter":$("#qty_meter").val(),
    "Grade":$("#grade").val(),
    "Satuan":$("#satuan").val(),
    "Harga":$("#harga").val(),
    "Total_harga" :$("#harga").val()*$("#qty_yard").val()
  }
}else if($("#satuan").val()=='Meter'){
 field = {
  "IDTBS": "",
  "Barcode":$("#barcode").val(),
  "Corak":$("#corak").val(),
  "Warna":$("#warna").val(),
  "Merk":$("#merk").val(),
  "Qty_yard":$("#qty_yard").val(),
  "Qty_meter":$("#qty_meter").val(),
  "Grade":$("#grade").val(),
  "Satuan":$("#satuan").val(),
  "Harga":$("#harga").val(),
  "Total_harga" :$("#harga").val()*$("#qty_meter").val()
}
}

return field;
}

$("#add-to-cart").click(function(){
  var field = initialization23();
    // if(!findItem(field)){
      PM.push(field);
    // }
    setTimeout(function(){
      renderPreview(PM);
      field = null;
    },500);

  });

function findItem(data){
  if(PM != null){
    for(i = 0; i < PM.length;i++){
      PM[i] = data;
      return true;
    }
  }
  return false;
}

function renderPreview(data) {
  if(data != null){
    if(PM == null)
      PM = [];
    var totalyard = 0;
    var totalmeter = 0;
        //console.log(data);
        // $("#tabel-cart-asset").html("");
        var table = $('#tabelfsdfsf').DataTable();
        table.clear().draw();
        for(i = 0; i<data.length; i++){
          PM[i] = data[i];
            // if($("#barcode").val()==""){
            //   $('#alert').html('<div class="pesan sukses">Barcode Tidak Boleh Kosong</div>');
            //   PM.pop();
            // }else if($("#corak").val()==""){
            //   $('#alert').html('<div class="pesan sukses">Corak Tidak Boleh Kosong</div>');
            //   PM.pop();
            // }else if($("#warna").val()==""){
            //   $('#alert').html('<div class="pesan sukses">Warna Tidak Boleh Kosong</div>');
            //   PM.pop();
            // }else if($("#merk").val()==""){
            //   $('#alert').html('<div class="pesan sukses">Merk Tidak Boleh Kosong</div>');
            //   PM.pop();
            // }else if($("#qty_yard").val()==""){
            //   $('#alert').html('<div class="pesan sukses">Qty Yard Tidak Boleh Kosong</div>');
            //   PM.pop();
            // }else if($("#qty_meter").val()==""){
            //   $('#alert').html('<div class="pesan sukses">Qty Meter Tidak Boleh Kosong</div>');
            //   PM.pop();
            // }else if($("#grade").val()==""){
            //   $('#alert').html('<div class="pesan sukses">Grade Tidak Boleh Kosong</div>');
            //   PM.pop();
            // }else if($("#satuan").val()==""){
            //   $('#alert').html('<div class="pesan sukses">Satuan Tidak Boleh Kosong</div>');
            //   PM.pop();
            // }else{
                // var total_pbnt= PM[i].Harga*PM[i].Qty_yard;
              // if(PM[i].Satuan=='Yard'){
                table.row.add([
                  i+1,
                  PM[i].Barcode,
                  PM[i].Corak,
                  PM[i].Warna,
                  PM[i].Merk,
                  PM[i].Qty_yard,
                  PM[i].Qty_meter,
                  PM[i].Grade,
                  PM[i].Satuan,
                  PM[i].Harga,
                  PM[i].Total_harga,
                  "<span>"+
                  "<a href='#' class='action-icons c-edit' id='bootbox-confirm' onclick='editList("+i+")' title='Ubah Data'>Ubah </a>"+
                  "</span>"+
                  "<span>" +
                  "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
                  "</span>"
                  ]).draw().nodes().to$().addClass('rowarray'+i);
            // }
            // else if(PM[i].Satuan=='meter'){
            //   table.row.add([
            //     i+1,
            //     PM[i].Barcode,
            //     PM[i].Corak,
            //     PM[i].Warna,
            //     PM[i].Merk,
            //     PM[i].Qty_yard,
            //     PM[i].Qty_meter,
            //     PM[i].Grade,
            //     PM[i].Satuan,
            //     PM[i].Harga,
            //     PM[i].total_pbnt,
            //     "<span>"+
            //     "<a href='#' class='action-icons c-edit' id='bootbox-confirm' onclick='editList("+i+")' title='Ubah Data'>Ubah </a>"+
            //     "</span>"+
            //     "<span>" +
            //     "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
            //     "</span>"
            //     ]).draw().nodes().to$().addClass('rowarray'+i);
            // }

            totalyard+= parseInt(PM[i].Qty_yard);
            totalmeter+= parseInt(PM[i].Qty_meter);

            $("#total_yard").val(totalyard);
            $("#total_meter").val(totalmeter);

            $('#barcode').val('');
            $('#qty_yard').val('');
            $('#qty_meter').val('');
            $('#grade').val('');
            $('#satuan').val('');
            $('#harga').val('');
            // }
          }
        }
      }

      function deleteItem(i){

        if(PM[i].id !=''){
          temp.push(PM[i]);
        }

        $('.rowarray'+i).remove();
        PM.splice(i,1);
        // renderPreview(PM);
      }

      function editList(i) {
        list_satuan(PM[i].Satuan);
        list_corak(PM[i].Corak);
        list_warna(PM[i].Corak, PM[i].Warna);
        $('#vbarcode').val(PM[i].Barcode);
    //$('#vcorak').val(PM[i].Corak);
    //$('#vwarna').val(PM[i].Warna);
    $('#vmerk').val(PM[i].Merk);
    $('#vgrade').val(PM[i].Grade);
    $('#vqty_yard').val(PM[i].Qty_yard);
    $('#vqty_meter').val(PM[i].Qty_meter);
    //$('#vsatuan').val(PM[i].Satuan);
    $('#vindex').val(i);
    $('#tmp_qty_yard').val(PM[i].Qty_yard);
    $('#tmp_qty_meter').val(PM[i].Qty_meter);
    $('#basic-modal-content').modal('show');
  }

  function selected_corak(selected) {
    $.post(baseUrl + "Penerimaan_barang_umum/get_corak")
    .done(function(data){
            //console.log(data);
            res = $.parseJSON(data);
            $("#vcorak").html("<option value='' disabled>Pilih Corak</option>");
            for(i=0; i<res.length; i++){
              sel = "";
              if(res[i].Corak == selected)
                sel = "selected";
              $("#vcorak").html($("#vcorak").html()+"<option data-corak='"+res[i].Corak+"' value='"+res[i].IDCorak+"' "+sel+">"+res[i].Corak+"</option>");
            }
          });
  }

  function save_tmp_create() {
    i = $('#vindex').val();
    PM[i].Barcode = $('#vbarcode').val();
    PM[i].Corak = $('#vcorak').val();
    PM[i].Warna = $('#vwarna').val();
    PM[i].Merk = $('#vmerk').val();
    PM[i].Grade = $('#vgrade').val();
    PM[i].Qty_yard = $('#vqty_yard').val();
    PM[i].Qty_meter = $('#vqty_meter').val();
    PM[i].Satuan = $('#vsatuan').val();

    // Total[0] = ((parseFloat(Total[0]) - parseFloat($('#tmp_qty_yard').val())) + parseFloat(PM[i].Qty_yard));
    // Total[1] = ((parseFloat(Total[1]) - parseFloat($('#tmp_qty_meter').val())) + parseFloat(PM[i].Qty_meter));

    // $('#total_yard').val(Total[0]);
    // $('#total_meter').val(Total[1]);

    //console.log(Total[0] + ' ' + Total[1]);
    renderPreview(PM);
  }

  function BatalPembelian(){
    index = 0;
    PM = null;
    $('#daftar-pembelian-preview').css({"visibility":"hidden","height":0});
  }

  function fieldPenerimaanUmum(){
    var data1 = {
      "Tanggal" : $("#tanggal").val(),
      "NoSJ" :$("#no_sjc").val(),
      "NoPB":$("#no_bo").val(),
      "NoPO":$("#no_po").val(),
      "ID_Supplier":$("#id_supplier").val(),
      "NoSO":$("#no_so").val(),
      "Total_yard":$("#total_yard").val(),
      "Total_meter":$("#total_meter").val(),
      "Keterangan":$("#keterangan").val(),
    }
    return data1;
  }

  function TambahPenerimaan(){
    var data2= fieldPenerimaanUmum();
    $("#id_supplier").css('border', '');
    $("#no_sjc").css('border', '');
    $("#no_po").css('border', '');
    $("#no_so").css('border', '');

    if($("#id_supplier").val()==""){
      $('#alert').html('<div class="pesan sukses">Data Supplier Tidak Boleh Kosong</div>');
      $("#id_supplier").css('border', '1px #C33 solid').focus();
    }else if($("#no_sjc").val()==""){
      $('#alert').html('<div class="pesan sukses">Data SJC Tidak Boleh Kosong</div>');
      $("#no_sjc").css('border', '1px #C33 solid').focus();
    }else if($("#no_po").val()==""){
      $('#alert').html('<div class="pesan sukses">Data PO Tidak Boleh Kosong</div>');
      $("#no_po").css('border', '1px #C33 solid').focus();
    }else if($("#no_so").val()==""){
      $('#alert').html('<div class="pesan sukses">Data SO Tidak Boleh Kosong</div>');
      $("#no_so").css('border', '1px #C33 solid').focus();
    }else{
      id_supplier = $("#id_supplier").val();
      $.post(baseUrl + 'Penerimaan_barang_umum/store', {"data": PM, "data2" : data2, "id_supplier": id_supplier})
      .done(function (res) {
                //console.log(res);
                if (res) {
                  document.getElementById("simpan").disabled = true;
                  // document.getElementById("print_").disabled = false;
                  $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
                  window.location.href = "/"+base_url[1]+"/Penerimaan_barang_umum/create";
                } else {
                  $('#alert').html('<div class="pesan sukses">Isi Form Dengan Lengkap</div>');
                }
                index = 0;
                PM = null;
              });
    }
  }

  var temp = [];

  function renderJSON(data){
    if(data != null){
      if(PM == null)
        PM = [];
      var table = $('#tabelfsdfsf').DataTable();
      table.clear().draw();
      for(i = 0; i<data.length; i++){
        PM[i] = data[i];
        if(PM[i].Satuan=='Yard')
        {
          totalharga= PM[i].Harga*PM[i].Qty_yard;
        }else{
          totalharga= PM[i].Harga*PM[i].Qty_meter;
        }

        table.row.add([
          i+1,
          PM[i].Barcode,
          PM[i].Corak,
          PM[i].Warna,
          PM[i].Grade,
          PM[i].Qty_yard,
          PM[i].Qty_meter,
          PM[i].Satuan,
          PM[i].Harga,
          totalharga,
          "<span>"+
          "<a href='#' class='action-icons c-edit' id='bootbox-confirm' onclick='editList("+i+")' title='Ubah Data'>Ubah </a>"+
          "</span>"+
          "<span>" +
          "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
          "</span>"
          ]).draw().nodes().to$().addClass('rowarray'+i);

      }
    }
  }

  function editSelected(i) {
    list_satuan(PM[i].Satuan);
    selected_corak(PM[i].Corak);
    list_warna(PM[i].IDCorak, PM[i].Warna);

    $('#vbarcode').val(PM[i].Barcode);
    //$('#vcorak').val(PM[i].Corak);
    //$('#vwarna').val(PM[i].Warna);
    $('#vmerk').val(PM[i].Merk);
    $('#vgrade').val(PM[i].Grade);
    $('#vqty_yard').val(PM[i].Qty_yard);
    $('#vqty_meter').val(PM[i].Qty_meter);
    //$('#vsatuan').val(PM[i].Satuan);
    $('#vindex').val(i);
    $('#tmp_qty_yard').val(PM[i].Qty_yard);
    $('#tmp_qty_meter').val(PM[i].Qty_meter);
    $('#basic-modal-content').modal('show');
  }

  function deleteSelected(i){

    if(PM[i] !=''){
      temp.push(PM[i]);
    }

    PM.splice(i,1);
    renderJSON(PM);
  }

  function editItem(i) {
    list_satuan(PM[i].Satuan);
    list_corak(PM[i].Corak);
    list_warna(PM[i].IDCorak, PM[i].Warna);

    $('#IDTBSDetail').val(PM[i].IDTBSDetail);
    //$('#corak').val(PM[i].Corak);
    $('#Merk').val(PM[i].Merk);
    $('#vgrade').val(PM[i].Grade);
    $('#vqty_yard').val(PM[i].Qty_yard);
    $('#vqty_meter').val(PM[i].Qty_meter);
    //$('#satuan').val(PM[i].Satuan);
    $('#index').val(i);
    $('#basic-modal-content').modal('show');
  }

  function save_tmp() {
    i = $('#vindex').val();
    a = $('#vcorak').val();
    PM[i].Corak = $('#vcorak option:selected').data("corak");
    PM[i].Warna = $('#vwarna').val();
    PM[i].Grade = $('#vgrade').val();
    PM[i].Qty_yard = $('#vqty_yard').val();
    PM[i].Qty_meter = $('#vqty_meter').val();
    PM[i].Satuan = $('#vsatuan').val();
    console.log('res tmp : ' + PM[i]);
    renderJSON(PM);
  }

  function field(){
    var data = {
      "IDTBS" :$("#IDTBS").val(),
      "NoBO" :$("#no_bo").val(),
      "Tanggal" :$("#Tanggal").val(),
      "IDSupplier":$("#id_supplier").val(),
      "Keterangan":$("#Keterangan").val()
    }
    return data;
  }

  function initialization(){
    corak = getCorak($("#corak").val());
    //console.log(corak);
    var field = null;
    field = {
      "Barcode":$("#barcode").val(),
      "Corak": corak,
      "IDCorak":$("#corak").val(),
      "Warna":$("#warna").val(),
      "Grade":$("#grade").val(),
      "Merk":$("#merk").val(),
      "Qty_yard":$("#qty_yard").val(),
      "Qty_meter":$("#qty_meter").val(),
      "Satuan":$("#satuan").val(),
      "IDTBSDetail": '',
      "Saldo_meter" : 0,
      "Saldo_yard" : 0,
      "NoSO" : $('#no_so').val(),
      "Harga" : $('#harga').val()
    }

    return field;
  }

  function addToDetail() {

    var field = initialization();
    PM.push(field);
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);
  }

  function saveChange(){

    var data = field();
    console.log("#IDTBS");

    $.ajax({

      url: "../update",
      type: "POST",
      data: {
        "data1" : data,
        "data2" : temp,
        "data3" : PM
      },

      success: function (msg, status) {
        window.location.href = "../index";
      },
      error: function(msg, status, data){
        alert("Failure "+msg+status+data);
      }

    });
  }

  function updateChange() {
    var data = field();
    console.log(PM);
    console.log(data);

    $.ajax({

      url: "../updateChange",
      type: "POST",
      data: {
        "data1" : data,
        "data2" : temp,
        "data3" : PM
      },

      success: function (msg, status) {
        document.getElementById("simpan").disabled = true;
        // document.getElementById("print_").disabled = false;
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        window.location.href = "/"+base_url[1]+"/Penerimaan_barang_umum/";
      },
      error: function(msg, status, data){
        $('#alert').html('<div class="pesan sukses">Isi Form Dengan Lengkap</div>');
      }

    });
  }

  function print_cek() {
  //$(document).on('click', '#print_', function () {
    // sjc = $('#no_bo').val();
    // console.log(sjc + '--');

    window.open(baseUrl +"Penerimaan_barang_umum/print_data/");
    //});
  }

  function print_cek_edit() {
    sjc = $('#IDTBS').val();

    window.open(baseUrl +"Penerimaan_barang_umum/print_data_edit/" + sjc);
  }

