var base_url = window.location.pathname.split('/');

$(document).ready(function(){
  if(($("#corak").val() != "")){
    cek();
    if(($("#warna").val() != "")){
      get_warna($("#corak").val());
      
    }
  }
  $("#corak").change(cek);

  $("#cap, #tanpa_cap").change(function () {
    if ($("#cap").is(":checked")) {
          // alert('cap');
          $("#_cap1").removeAttr("disabled");
          $("#_cap2").removeAttr("disabled");
        }
        else if ($("#tanpa_cap").is(":checked")) {
          $("#_cap1").prop( "checked", false );
          $("#_cap2").prop( "checked", false );
          $("#_cap1").prop( "disabled", true );
          $("#_cap2").prop( "disabled", true );
        }
      })
})

var PM = [];
var temp = [];

function cek_tab()
{
  if($("#PIC").val()!="")
  {
    $('#tab_1').html('<a href="#tab1" style="font-weight:bold">Input Warna</a>');
    $('#tab_2').html('<a href="#tab2" style="color:red">Metode Packing</a>');
    $("#tab_2").removeClass("disabled");
  }

  if($("#Bentuk").val()!="" && $("#Panjang").val()!="" && $("#Point").val()!="" && $("#Kirim").val()!="")
  {
    $('#tab_1').html('<a href="#tab1" style="font-weight:bold">Input Warna</a>');
    $('#tab_2').html('<a href="#tab2" style="font-weight:bold">Metode Packing</a>');
    $("#tab_3").html('<a href="#tab3" style="color:red">Cap Pinggir</a>');
    $("#tab_3").removeClass("disabled");
  }

  if($("#Sample").val()!="" && $("#M10").val()!="" && $("#Kain").val()!="" && $("#Lembaran").val()!="")
  {
    $('#tab_1').html('<a href="#tab1" style="font-weight:bold">Input Warna</a>');
    $('#tab_2').html('<a href="#tab2" style="font-weight:bold">Metode Packing</a>');
    $("#tab_3").html('<a href="#tab3" style="font-weight:bold">Cap Pinggir</a>');
    $("#tab_4").html('<a href="#tab4" style="color:red">Catatan</a>');
    $("#tab_4").removeClass("disabled");
    console.log($("#cap").val());
  }

  if($("#Keterangan").val()!="")
  {
    $('#tab_1').html('<a href="#tab1" style="font-weight:bold">Input Warna</a>');
    $('#tab_2').html('<a href="#tab2" style="font-weight:bold">Metode Packing</a>');
    $("#tab_3").html('<a href="#tab3" style="font-weight:bold">Cap Pinggir</a>');
    $("#tab_4").html('<a href="#tab4" style="font-weight:bold">Catatan</a>');
  }
}

function cek() {
  var corak = $("#corak").val();
    //console.log(base_url[1]+'/Po/getmerk');
    $.ajax({
      url : '/'+base_url[1]+'/Po/getmerk',
      type: "POST",
      data:{id_corak:corak},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data kosong------------');
        } else {
            // html +='<option selected value="'+data["IDMerk"]+'"> '+data["Merk"]+'</option>';
            // // console.log('berhasil');
            // // $("#merk").val(data["IDCorak"]);
            // $("#merk").html(html);
            //console.log(data);
            $("#merk").val(data["Merk"]);
            $("#merk").data("idmerk",data["IDMerk"]);
            $("#merk").data("idbarang",data["IDBarang"]);
            get_warna(data["Kode_Corak"]);

          }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
        }
      });
  }

  function get_warna(kodecorak){
    var _kodecorak = kodecorak;
    $.ajax({
      url : '/'+base_url[1]+'/Po/getwarna',
      type: "POST",
      data:{id_kodecorak:_kodecorak},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0 || data == null || data =="") {
          html = '<option value="">--Data Warna Kosong--</option>';
        } else {
          //console.log(data);
          html += '<option value="">--Silahkan Pilih Warna--</option>';
          for (var i = 0 ; i < data.length; i++) {
            html +='<option value="'+data[i]["IDWarna"]+'" data-warna="'+data[i]["Warna"]+'"> '+data[i]["Warna"]+' </option>';
          }
        }
          $("#warna").html(html);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function initialization(){
    field = null;
    field = {
      "Warna": $("#warna option:selected").attr('data-warna'),
      "IDWarna" : $("#warna").val(),
      "Qty" : $("#Qty").val(),
      "Saldo" : $("#Qty").val(),
      "Harga" : $("#harga-123").val(),
      // "Total" : $("#Harga").val()*$("#Qty").val(),
    }

    return field;
  }

  $("#tambah_sementara").click(function(){
    var field = initialization();
    PM.push(field);
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);

  });

  function renderJSON(data){
    var totalQty = 0;
    var Total_warna = 0;
    if(data != null){
      
      if($("#warna").val()==""){
        $('#alert').html('<div class="pesan sukses">Warna Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#Qty").val()==""){
        $('#alert').html('<div class="pesan sukses">Qty Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#harga-123").val()==""){
        $('#alert').html('<div class="pesan sukses">Harga Tidak Boleh Kosong</div>');
        PM.pop();
      }else{
        PM = [];
       var table = $('#tabelfsdfsf').DataTable();
       table.clear().draw();
       for(i = 0; i<data.length;i++){
         PM[i] = data[i];
         var totalharga= PM[i].Harga.replace(/\./g,'')* PM[i].Qty;
         var totalqty= PM[i].Qty;
         table.row.add([
           i+1,
           PM[i].Warna,
           rupiahkoma(PM[i].Qty),
           rupiahkoma(PM[i].Harga.replace(/\./g,'')),
           rupiahkoma(totalharga),
           "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+","+totalharga+","+totalqty+")'>Hapus </a>"
           ]).draw().nodes().to$().addClass('rowarray'+i);
         totalQty += parseFloat(PM[i].Qty);
         Total_warna += PM[i].Harga.replace(/\./g,'')* PM[i].Qty;
        
      }

      $('#warna').val('');
      $('#Qty').val('');
      $('#harga-123').val('');

      $("#Total_qty").val(rupiahkoma(totalQty));
      $("#Totalwarna").val(rupiahkoma(Total_warna));
    }
      //console.log(totalQty);

      //-----Buat Field inputan sementara kosong lagi

    }
  }

  function renderJSONE(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    var Total_warna = 0;
    if(data != null){
      PM = [];

        var table = $('#tabelfsdfsf').DataTable();
        table.clear().draw();
        for(i = 0; i<data.length;i++){
          PM[i] = data[i];

          var totalharga= PM[i].Harga.replace(/\./g,'')* PM[i].Qty;
          var totalqty= PM[i].Qty;
          table.row.add([
            i+1,
            PM[i].Warna,
            rupiahkoma(PM[i].Qty),
            rupiahkoma(PM[i].Harga.replace(/\./g,'')),
            rupiahkoma(PM[i].Harga.replace(/\./g,'')* PM[i].Qty),
            "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+","+totalharga+","+totalqty+")'>Hapus </a>"
            ]).draw().nodes().to$().addClass('rowarray'+i);
          Total_warna += PM[i].Harga.replace(/\./g,'')* PM[i].Qty;
      // }
    }
    $("#Totalwarna").val(rupiahkoma(Total_warna));

  }
}

function deleteItem(i, totalharga, totalqty){
 //  if(PM[i].id !=''){
 //   temp.push(PM[i]);
 // }
 $('.rowarray'+i).remove();
 PM.splice(i,1);
 $("#Totalwarna").val(rupiahkoma(($("#Totalwarna").val().replace(/\./g,'')) - parseFloat(totalharga)));
 $("#Total_qty").val(rupiahkoma($("#Total_qty").val() - parseFloat(totalqty)));
 // renderJSON(PM);
}

function fieldPo(){
  var posisi ='-';
  var posisi1 ='-';

  if ($("#cap").is(":checked")) {
    if($("#_cap1").is(":checked")) {
      posisi = $("#_cap1").val();
    }

    if($("#_cap2").is(":checked")) {
      posisi1 = $("#_cap2").val();
    }
  }


  var data1 = {
    "Tanggal" :$("#Tanggal").val(),
    "Nomor" :$("#Nomor").val(),
    "Tanggal_Selesai" :$("#Tanggal_selesai").val(),
    "IDSupplier" :$("#IDSupplier").val(),
    "corak" :$("#corak").val(),
    "merk" :$("#merk").data("idmerk"),
    "IDAgen" :$("#IDAgen").data("id"),
    "Lot" :$("#Lot").val(),
    "Jenis_Pesanan" :$('input[name=Jenis_Pesanan]:checked').val(),
    "Total_qty" :$("#Total_qty").val(),
    "Saldo" :$("#Total_qty").val(),
    "PIC" :$("#PIC").val(),
    "Prioritas" :$('input[name=Prioritas]:checked').val(),
    "Bentuk" :$("#Bentuk").val(),
    "Panjang" :$("#Panjang").val(),
    "Point" :$("#Point").val(),
    "Kirim" :$("#Kirim").val(),
    "Stamping" :$('input[name=cap]:checked').val(),
    "Posisi" :posisi,
    "Posisi1" :posisi1,
    "Sample" :$("#Sample").val(),
    "M10" :$("#M10").val(),
    "Kain" :$("#Kain").val(),
    "Lembaran" :$("#Lembaran").val(),
    "Keterangan" :$("#Keterangan").val(),
    "Jenis_PO" :"PO TTI",
    "Batal" : "aktif",
    "Grand_total" : $("#Totalwarna").val(),
    "IDBarang" : $("#merk").data("idbarang")
  }
  return data1;
}

function save(){
  var data1 = fieldPo();
  $("#corak").css('border', '');
  $("#merk").css('border', '');
  $("#Lot").css('border', '');
  $("#PIC").css('border', '');
  $("#Sample").css('border', '');
  $("#M10").css('border', '');
  $("#Keterangan").css('border', '');
  $("#Kain").css('border', '');
  $("#Lembaran").css('border', '');

  if($("#corak").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Corak Tidak Boleh Kosong</div>');
    $("#corak").css('border', '1px #C33 solid').focus();
  }else if($("#merk").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Merk Tidak Boleh Kosong</div>');
    $("#merk").css('border', '1px #C33 solid').focus();
  }else if($("#Lot").val()==""){
    $('#alert').html('<div class="pesan sukses">Data LOT Tidak Boleh Kosong</div>');
    $("#Lot").css('border', '1px #C33 solid').focus();
  }else if($("#PIC").val()==""){
    $('#alert').html('<div class="pesan sukses">Data PIC Tidak Boleh Kosong</div>');
    $("#PIC").css('border', '1px #C33 solid').focus();
  }else if($("#Sample").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Album Tidak Boleh Kosong</div>');
    $("#Sample").css('border', '1px #C33 solid').focus();
  }else if($("#M10").val()==""){
    $('#alert').html('<div class="pesan sukses">Data M10/20 Tidak Boleh Kosong</div>');
    $("#M10").css('border', '1px #C33 solid').focus();
  }else if($("#Kain").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Kain Tidak Boleh Kosong</div>');
    $("#Kain").css('border', '1px #C33 solid').focus();
  }else if($("#Lembaran").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Lembaran Tidak Boleh Kosong</div>');
    $("#Lembaran").css('border', '1px #C33 solid').focus();
  }else if($("#Keterangan").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Keterangan Tidak Boleh Kosong</div>');
    $("#Keterangan").css('border', '1px #C33 solid').focus();
  }else{
    console.log('-----------Prosessimpan---------------')
    $("#loading-status").ajaxStart(function(){
      $(this).show();
    });
    $.ajax({
      url : '/'+base_url[1]+'/Po/simpan_po',
      type: "POST",
      data:{_data1:data1, _data2:PM},
      dataType:'json',
      success: function(data)
      { 
        zeroValue;
        console.log('----------Ajax berhasil-----');
        $("#loading-status").ajaxStop(function(){
          $(this).hide();
        });
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        window.location.href = "/"+base_url[1]+"/Po/create";
        // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
        // document.getElementById("simpan").setAttribute("onclick", "return false;");
        // document.getElementById("print").setAttribute("style", "cursor: pointer;");
        // document.getElementById("print").setAttribute("onclick", "return true;");
        // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/Po/print_data_po/'+data['IDPO']);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        $('#alert').html('<div class="pesan sukses">Error Data</div>');
      }
    });
  }
}

  // function save_change(){
  //   var data1 = fieldPo();
  //   var idPO = $("#IDPO").val();
  //   // console.log(data1);
  //   // console.log(idPO);
  //   // console.log(PM);
  //   // console.log(temp);
  //   console.log('----------- Prosessimpan---------------')

  //   $.ajax({
  //     url : '/'+base_url[1]+'/Po/ubah_po',
  //     type: "POST",
  //     data:{_data1:data1, _data2:PM, _data3:temp, _id:idPO},
  //     dataType:'json',
  //     success: function(data)
  //     { 
  //       console.log('----------Ajax berhasil-----');
  //       console.log(data);
  //       $('#alert').html('<div class="pesan sukses">Data berhasil diubah</div>');

  //     },
  //     error: function (jqXHR, textStatus, errorThrown)
  //     {
  //       console.log(jqXHR);
  //       console.log(textStatus);
  //       console.log(errorThrown);
  //     }
  //   });
  // }

  function save_change(){
    var data1 = fieldPo();
    var idPO = $("#IDPO").val();
    console.log(data1);

    $.ajax({

      url : '/'+base_url[1]+'/Po/ubah_po',

      type: "POST",

      data: {

        "_data1" : data1,
        "_data2": PM,
        "_data3" : temp,
        "_id":idPO

      },

     // dataType: 'json',

     success: function (msg, status) {
       $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
       window.location.href = "/"+base_url[1]+"/Po/index";
       
      // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      // document.getElementById("simpan").setAttribute("onclick", "return false;");
      // document.getElementById("print").setAttribute("style", "cursor: pointer;");
      // document.getElementById("print").setAttribute("onclick", "return true;");
      // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/Po/print_data_po/'+idPO);

    },
    error: function(msg, status, data){
      // alert("Failure"+msg+status+data);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
  }



  function zeroValue(){
    $("#Tanggal").val('');
    $("#Nomor").val('');
    $("#Tanggal_selesai").val('');
    $("#corak").val('');
    $("#merk").val('');
    $("#Lot").val('');
    $("#Total_qty").val('');
    $("#PIC").val('');
    $("#Keterangan").val('');

  }
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

