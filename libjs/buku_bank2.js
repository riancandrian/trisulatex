var base_url = window.location.pathname.split('/');

$(document).ready(function(){
  if(($("#KodePerkiraan1").val() != "")){
    cek();
  }
  if(($("#KodePerkiraan2").val() != "")){
    cek2();
  }

  if(($("#Posisi").val() != "")){
    cek3();
  }

  $("#KodePerkiraan1").change(cek);
  $("#KodePerkiraan2").change(cek2);
  $("#Posisi").change(cek3);
})

var PM = [];
var temp = [];

function cek() {
  var namaCOA1 = $("#KodePerkiraan1 option:selected").attr('data-namaCOA');
  $("#NamaPerkiraan1").val(namaCOA1);
}

function cek2() {
  var namaCOA2 = $("#KodePerkiraan2 option:selected").attr('data-namaCOA');
  $("#NamaPerkiraan2").val(namaCOA2);

}

function cek3() {
  var posisiBaru = $("#Posisi").val();
    //alert(posisiBaru);
    //console.log(posisiBaru);
    if (posisiBaru == "Kredit") {
      _posisiBaru = "Debet"
    } else {
      _posisiBaru = "Kredit"
    }
    $("#ubahPosisi").html(_posisiBaru);
    
  }

  function initialization(){
    field = null;
    var _kredit= 0;
    var _debet = 0;
    if ($("#Posisi").val() == 'Kredit') {
      _kredit = $("#KreditatauDebet").val().replace(/\./g,'');
    } else if ($("#Posisi").val() == 'Debet'){
      _debet = $("#KreditatauDebet").val().replace(/\./g,'');
    }
    field = {
      "IDCOA" : $("#KodePerkiraan2").val(),
      "Nama_COA" : $("#NamaPerkiraan2").val(),
      "Kode_COA": $("#KodePerkiraan2 option:selected").attr('data-kodeCOA'),
      "Kredit" : _kredit,
      "Debet" : _debet,
      "Posisi" : $("#Posisi").val(),
      "IDMataUang" : 1,
      "Kurs" : 14000,
      "Keterangan" : $("#Keterangan2").val(),

    }

    return field;
  }

  function initialization2(){
    field = null;
    var _kredit= 0;
    var _debet = 0;
    if ($("#Posisi").val() == 'Kredit') {
      _kredit = $("#KreditatauDebet").val().replace(/\./g,'');
    } else if ($("#Posisi").val() == 'Debet'){
      _debet = $("#KreditatauDebet").val().replace(/\./g,'');
    }
    field = {
      "IDCOA" : $("#KodePerkiraan2").val(),
      "Kode_COA": $("#KodePerkiraan2 option:selected").attr('data-Kode_COA'),
      "Nama_COA" : $("#NamaPerkiraan2").val(),
      "Kredit" : _kredit,
      "Debet" : _debet,
      "Posisi" : $("#Posisi").val(),
      "IDMataUang" : 1,
      "Kurs" : 14000,
      "Keterangan" : $("#Keterangan2").val(),

    }

    return field;
  }

  $("#tambah_sementara").click(function(){
    var field = initialization();
    PM.push(field);
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);
    
  });

  $("#tambah_sementara_edit").click(function(){
    var field = initialization2();
    PM.push(field);
    setTimeout(function(){
      renderJSONE(PM);
      field = null;
    },500);
    
  });

  function renderJSON(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    var totaldebet=0;
    var totalkredit=0;
    if(data != null){
      PM = [];
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Kode_COA+"</td>"+
        "<td>"+PM[i].Nama_COA+"</td>"+
        // "<td>"+PM[i].Posisi+"</td>"+
        "<td>"+rupiah(PM[i].Kredit)+"</td>"+
        "<td>"+rupiah(PM[i].Debet)+"</td>"+
        "<td>"+PM[i].Keterangan+"</td>"+
        "<td>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"+
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
        totalQty += parseInt(PM[i].NilaiGiro);
        totaldebet += parseInt(PM[i].Debet);
        totalkredit += parseInt(PM[i].Kredit);
      }
      $("#Total").val(totalQty);
      $("#KodePerkiraan1").prop("disabled", true);
      $("#total_debet").val(totaldebet);
      $("#total_kredit").val(totalkredit);
      //console.log(totalQty);

      //-----Buat Field inputan sementara kosong lagi
    }
  }

  function renderJSONE(data){
    var totalQty = 0;
    var totalkredit =0;
    var totaldebet = 0;

    if(data != null){
      PM = [];
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Kode_COA+"</td>"+
        "<td>"+PM[i].Nama_COA+"</td>"+
        "<td>"+PM[i].Posisi+"</td>"+
        "<td>"+rupiah(PM[i].Kredit)+"</td>"+
        "<td>"+rupiah(PM[i].Debet)+"</td>"+
        "<td>"+PM[i].Keterangan+"</td>"+
        "<td>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItemEdit("+i+")'>Hapus </a>"+
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
        totaldebet += parseInt(PM[i].Debet);
        totalkredit += parseInt(PM[i].Kredit);
      }
      $("#total_debet").val(totaldebet);
      $("#total_kredit").val(totalkredit);
    }
  }

  function deleteItem(i){

    if(PM[i].id !=''){
     temp.push(PM[i]);
   }

   PM.splice(i,1);
   renderJSON(PM);
 }

 function deleteItemEdit(i){

  if(PM[i].id !=''){
   temp.push(PM[i]);
 }

 PM.splice(i,1);
 renderJSONE(PM);
}

function fieldPo(){
  var data1 = {
    "Tanggal" :$("#Tanggal").val(),
    "Nomor" :$("#Nomor").val(),
    "Keterangan" :$("#Keterangan").val(),
    "Posisi" : $("#Posisi").val(),
    "IDCOA" : $("#KodePerkiraan1").val(),
    "Batal" : "aktif",
    "total_debet" : $("#total_debet").val(),
    "total_kredit" : $("#total_kredit").val(),
  }
  return data1;
}

function save(){
  var data1 = fieldPo();
  
  $.ajax({
    url : '/'+base_url[1]+'/BukuBank/simpan_bukubank',
    type: "POST",
    data:{_data1:data1, _data2:PM},
    dataType:'json',
    success: function(data)
    { 
      console.log(data);
      zeroValue;
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        // window.location.href = "/"+base_url[1]+"/BukuBank/index"; 
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
    // window.location.href = "/"+base_url[1]+"/BukuBank/index"; 
  }

  function save_change(){
    var data1 = fieldPo();
    var IDBB = $("#IDBukuBank").val();
    console.log(PM);

    $.ajax({

      url : '/'+base_url[1]+'/BukuBank/ubah_bukubank',

      type: "POST",

      data: {

       "_data1" : data1,
       "_data2": PM,
       "_data3" : temp,
       "_id":IDBB

     },

     // dataType: 'json',

     success: function (msg, status) {
      $('#alert').html('<div class="pesan sukses">Data berhasil Diubah</div>');
      //   window.location.href = "/"+base_url[1]+"/BukuBank/index";
      
      // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      // document.getElementById("simpan").setAttribute("onclick", "return false;");
      // document.getElementById("print").setAttribute("style", "cursor: pointer;");
      // document.getElementById("print").setAttribute("onclick", "return true;");
      // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/JurnalUmum/print_data_jurnalumum/'+IDJU);

    },
    error: function(msg, status, data){
      // alert("Failure"+msg+status+data);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
  }



  function zeroValue(){
    $("#Tanggal").val('');
    $("#Nomor").val('');
    $("#Tanggal_selesai").val('');
    $("#corak").val('');
    $("#merk").val('');
    $("#Lot").val('');
    $("#Total_qty").val('');
    $("#PIC").val('');
    $("#Keterangan").val('');

  }
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}