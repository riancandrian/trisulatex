var base_url = window.location.pathname.split('/');
var PM = [];
var temp = [];

//-----------------------------------------------------------------------ready
$(document).ready(function(){

  var html = '';
  var field = null;

  $("#Barcode").change(function(){
    var barcod = $("#Barcode");
    //alert(barcod.val());

    console.log('--Proses Ajax--');
    //----------------------------------------Ajax Kirim data barcode
    $.ajax({
      url : '/'+base_url[1]+'/MakloonCelup/getstok',
      type: "POST",
      data:{barcode:barcod.val()},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data kosong------------');
        } else {
          //console.log('Data Ada');
          //console.log(data[0]['Barcode']);
          field = {
            "Barcode" : data[0]['Barcode'],
            "IDCorak" : data[0]['IDCorak'],
            "IDWarna" : data[0]['IDWarna'],
            "IDMerk" : data[0]['IDMerk'],
            "Merk" : data[0]['Merk'],
            "Warna" : data[0]['Warna'],
            "Corak" : data[0]['Corak'],
            "Satuan" : data[0]['Satuan'],
            "Qty_yard" : data[0]['Qty_yard'],
            "Qty_meter" : data[0]['Qty_meter'],
            "Saldo_yard" : data[0]['Saldo_yard'],
            "Saldo_meter" : data[0]['Saldo_meter'],
            "Grade" : data[0]['Grade'],
            "IDSatuan" : data[0]['IDSatuan'],
            "IDBarang" : data[0]['IDBarang'],
            "Lebar" : '-',
          }
          //console.log(field);
          PM.push(field);
          setTimeout(function(){
            renderJSON(PM);
            field = null;
          },500);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
      }
    }); //----------------------------------------end ajax
    barcod.val('');
  });
}); //-----------------------------------------------------------------------end ready

//----------------------------------------render Data sementara
function renderJSON(data){
  // console.log('-----------------')
  // console.log(data);
  var totalYard = 0;
  var totalMeter = 0;
  if(data != null){
    PM = [];
    // $("#tabel-cart-asset").html("");
    var table = $('#tabelfsdfsf').DataTable();
       table.clear().draw();
    for(i = 0; i<data.length;i++){
      PM[i] = data[i];
      // var value =
      // "<tr>" +
      // "<td>"+(i+1)+"</td>"+
      // "<td>"+PM[i].Barcode+"</td>"+
      // "<td>"+PM[i].Corak+"</td>"+
      // "<td>"+PM[i].Warna+"</td>"+
      // "<td>"+PM[i].Merk+"</td>"+
      // "<td>"+PM[i].Qty_yard+"</td>"+
      // "<td>"+PM[i].Qty_meter+"</td>"+
      // "<td>"+PM[i].Grade+"</td>"+
      // "<td>"+PM[i].Satuan+"</td>"+
      // "<td class='text-center'>" +
      // "<span>" +
      // "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
      // "</span>" +
      // "</td>"+
      // "</tr>";
      // $("#tabel-cart-asset").append(value);
      table.row.add([
          i+1,
          PM[i].Barcode,
          PM[i].Corak,
          PM[i].Warna,
          PM[i].Merk,
          PM[i].Qty_yard,
          PM[i].Qty_meter,
          PM[i].Grade,
          PM[i].Satuan,
          "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"
          ]).draw().nodes().to$().addClass('rowarray'+i);

      totalYard += parseInt(PM[i].Qty_yard);
      totalMeter += parseInt(PM[i].Qty_meter);
      $("#Qty_yard").data("totalYard",PM[i].Saldo_yard);
      $("#Qty_meter").data("totalmeter",PM[i].Saldo_meter);
    }
    $("#Qty_yard").val(totalYard);
    $("#Qty_meter").val(totalMeter);
    
    // //console.log(totalQty);

    // //-----Buat Field inputan sementara kosong lagi
    // $('#warna').val('');
    // $('#Qty').val('');
  }
}
//----------------------------------------End render Data sementara


//---------------------------------------- render Data Edit
function renderJSONE(data){
  // console.log('-----------------')
  // console.log(data);
  var totalQty = 0;
  if(data != null){
    PM = [];
    // $("#tabel-cart-asset").html("");
    var table = $('#tabelfsdfsf').DataTable();
       table.clear().draw();
    for(i = 0; i<data.length;i++){
      PM[i] = data[i];
      // var value =
      // "<tr>" +
      // "<td>"+(i+1)+"</td>"+
      // "<td>"+PM[i].Barcode+"</td>"+
      // "<td>"+PM[i].Corak+"</td>"+
      // "<td>"+PM[i].Warna+"</td>"+
      // "<td>"+PM[i].Merk+"</td>"+
      // "<td>"+PM[i].Qty_yard+"</td>"+
      // "<td>"+PM[i].Qty_meter+"</td>"+
      // "<td>"+PM[i].Grade+"</td>"+
      // "<td>"+PM[i].Satuan+"</td>"+
      // "<td class='text-center'>" +
      // "<span>" +
      // "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
      // "</span>" +
      // "</td>"+
      // "</tr>";
      // $("#tabel-cart-asset").append(value);
      table.row.add([
          i+1,
          PM[i].Barcode,
          PM[i].Corak,
          PM[i].Warna,
          PM[i].Merk,
          PM[i].Qty_yard,
          PM[i].Qty_meter,
          PM[i].Grade,
          PM[i].Satuan,
          "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"
          ]).draw().nodes().to$().addClass('rowarray'+i);
    }

  }
}
//----------------------------------------End render Data Edit

//----------------------------------------Delete Item Sementara
function deleteItem(i){

  if(PM[i].id !=''){
   temp.push(PM[i]);
 }

$('.rowarray'+i).remove();
 PM.splice(i,1);
}
//----------------------------------------End Delete Item Sementara


//------------------------------------------Get Field Makloon
function fieldMakloon(){


  var data1 = {
    "Tanggal" :$("#Tanggal").val(),
    "Nomor" :$("#Nomor").val(),
    "IDSupplier" :$("#IDSupplier").val(),
    "IDPO" :$("#IDPO").val(),
    "Total_qty_yard" :$("#Qty_yard").val(),
    "Total_qty_meter" :$("#Qty_meter").val(),
    "Saldo_yard" :$("#Qty_yard").val(),
    "Saldo_meter" :$("#Qty_meter").val(),
    "Keterangan" :$("#Keterangan").val(),
    "Batal" :'aktif',
  }
  return data1;
}


//-----------------------------------Simpan
function save(){
  var data1 = fieldMakloon();

  $("#IDPO").css('border', '');
  $("#Keterangan").css('border', '');

  if($("#IDPO").val()==""){
    $('#alert').html('<div class="pesan sukses">Data PO Tidak Boleh Kosong</div>');
    $("#IDPO").css('border', '1px #C33 solid').focus();
  }else if($("#Keterangan").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Keterangan Tidak Boleh Kosong</div>');
    $("#Keterangan").css('border', '1px #C33 solid').focus();
  }else{
  $.ajax({
    url : '/'+base_url[1]+'/MakloonCelup/simpan_makloon',
    type: "POST",
    data:{_data1:data1, _data2:PM},
    dataType:'json',
    success: function(data)
    { 
      console.log('----------Ajax berhasil-----');
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      // window.location.href = "/"+base_url[1]+"/MakloonCelup/index";
       document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
      document.getElementById("printdata").setAttribute("onclick", "return true;");
      document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/MakloonCelup/print/'+data['IDSJM']);
        //console.log(data);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
}
}

//----------------------------------Simpan Edit
function save_change(){
  var data1 = fieldMakloon();
   var IDSJM = $("#IDSJM").val();
  console.log(data1);

  $.ajax({

    url : '/'+base_url[1]+'/MakloonCelup/ubah_makloon',

    type: "POST",

    data: {
      "_id" : IDSJM,
     "_data1" : data1,
     "_data2": PM,
     "_data3" : temp

   },

     // dataType: 'json',

     success: function (data) {
      // window.location.href = "/"+base_url[1]+"/MakloonCelup/index"
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      // window.location.href = "/"+base_url[1]+"/MakloonCelup/index";
       document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
      document.getElementById("printdata").setAttribute("onclick", "return true;");
      document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/MakloonCelup/print/'+IDSJM);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
       console.log(jqXHR);
      // console.log(textStatus);
      // console.log(errorThrown);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
    }

  });
}
//-----------------------------------End Simpan Edit




