var base_url = window.location.pathname.split('/');

var PM = [];
var temp = [];

function initialization(){
  field = null;
  field = {
    "GroupAsset" : $("#GroupAsset").val(),
    "NamaGroupAsset" : $("#GroupAsset option:selected").attr('data-namagroupAsset'),
    "NamaAsset": $("#Nama option:selected").attr('data-namaAsset'),
    "Nama" : $("#Nama").val(),
    "Qty" : $("#Qty").val(),
    "Saldo" : $("#Qty").val(),
    "Harga" : $("#harga-123").val(),
    "NamaSatuan": $("#Satuan option:selected").attr('data-namasatuan'),
  }

  return field;
}

$("#tambah_sementara").click(function(){
  var field = initialization();
  PM.push(field);
  setTimeout(function(){
    renderJSON(PM);
    field = null;
  },500);

});

function renderJSON(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    var Total_warna = 0;
    if(data != null){
      if($("#Nama").val()==""){
        $('#alert').html('<div class="pesan sukses">Nama Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#Satuan").val()==""){
        $('#alert').html('<div class="pesan sukses">Satuan Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#Qty").val()==""){
        $('#alert').html('<div class="pesan sukses">Qty Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#harga-123").val()==""){
        $('#alert').html('<div class="pesan sukses">Harga Tidak Boleh Kosong</div>');
        PM.pop();
      }else{
        PM = [];
        var table = $('#tabelfsdfsf').DataTable();
        table.clear().draw();
        for(i = 0; i<data.length;i++){
          PM[i] = data[i];
          PM[i].Total=PM[i].Harga.replace(/\./g,'')* PM[i].Qty.replace(/\./g,'')
          totalharga=PM[i].Total;
          totalqty=PM[i].Qty.replace(/\./g,'');
          table.row.add([
            i+1,
            PM[i].NamaGroupAsset,
            PM[i].NamaAsset,
            rupiah(PM[i].Qty.replace(/\./g,'')),
            rupiah(PM[i].Harga.replace(/\./g,'')),
            rupiah(PM[i].Total),
            "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+","+totalharga+","+totalqty+")'>Hapus </a>"
            ]).draw().nodes().to$().addClass('rowarray'+i);
          totalQty += parseInt(PM[i].Qty.replace(/\./g,''));
          Total_warna += PM[i].Total;
        }
        $("#Total_qty").val(rupiah(totalQty));

        $('#harga-123').val('');
        $('#Qty').val('');
        $("#Totalwarna").val(rupiah(Total_warna));
      }
    }
  }


  function initialization2(){
    field = null;
    field = {
      "Group_Asset"       : $("#GroupAsset option:selected").attr('data-namagroupAsset'),
      "IDGroupAsset"      : $("#GroupAsset").val(),
      "Nama_Asset"        : $("#Nama option:selected").attr('data-namaAsset'),
      "Nama"              : $("#Nama").val(),
      "IDAsset"           : $("#Nama").val(),
      "Qty"               : $("#Qty").val(),
      "Saldo"             : $("#Qty").val(),
      "Harga_Satuan"      : $("#harga-123").val(),
      "IDSatuan"          : $("#Satuan").val(),
    }

    return field;
  }


  $("#tambah_sementara2").click(function(){
    var field = initialization2();
    PM.push(field);
    setTimeout(function(){
      renderJSONE(PM);
      field = null;
    },500);

  });

  function renderJSONE(data){
    //console.log('==============CEK MASUK');
    var totalQty = 0;
    var Total_warna = 0;
    if(data != null){
      PM = [];

      var table = $('#tabelfsdfsf').DataTable();
      table.clear().draw();
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        PM[i].Subtotal=PM[i].Harga_Satuan.replace(/\./g,'')*PM[i].Qty.replace(/\./g,'');
        totalharga=PM[i].Subtotal;
        totalqty=PM[i].Qty.replace(/\./g,'');
        table.row.add([
          i+1,
          PM[i].Group_Asset,
          PM[i].Nama_Asset,
          rupiah(PM[i].Qty.replace(/\./g,'')),
          rupiah(PM[i].Harga_Satuan.replace(/\./g,'')),
          rupiah(PM[i].Subtotal),
          "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+","+totalharga+","+totalqty+")'>Hapus </a>"
          ]).draw().nodes().to$().addClass('rowarray'+i);
        Total_warna += PM[i].Subtotal;
        totalQty += parseInt(PM[i].Qty.replace(/\./g,''));
      // }
    }
    $("#Totalwarna").val(rupiah(Total_warna));
    $("#Total_qty").val(rupiah(totalQty));
  }
}

function deleteItem(i, totalharga, totalqty){

 $('.rowarray'+i).remove();
 PM.splice(i,1);
 $("#Totalwarna").val(rupiah(($("#Totalwarna").val().replace(/\./g,'')) - parseFloat(totalharga)));
 $("#Total_qty").val(($("#Total_qty").val()) - parseFloat(totalqty));
 // renderJSON(PM);
}

function fieldPo(){
  var data1 = {
    "IDPOAsset"         :$('#Nomor').val(),
    "Total_Qty"         :$("#Total_qty").val(),
    "Keterangan"        :$("#Keterangan").val(),
    "Tanggal"           :$("#Tanggal").val(),
    "Nomor"             :$("#Nomor").val(),
    "IDSupplier"        :$("#IDSupplier").val(),
    "Saldo"             :$("#Total_qty").val(),
    "Grand_total"       : $("#Totalwarna").val(),
  }
  return data1;
}

function save(){
  var data1 = fieldPo();
    $.ajax({
      url : '/'+base_url[1]+'/PO_Asset/simpan_po',
      type: "POST",
      data:{_data1:data1, _data2:PM},
      dataType:'json',
      success: function(data)
      { 
        //zeroValue;
        console.log('----------Ajax berhasil-----');
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        window.location.href = "/"+base_url[1]+"/PO_Asset/create";
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(data1);
        console.log(jqXHR);
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        window.location.href = "/"+base_url[1]+"/PO_Asset/create";
      }
    });
  }


function save_change(){
  var data1 = fieldPo();
  var idPO = $("#IDPO").val();
  $.ajax({
    url : '/'+base_url[1]+'/PO_Asset/ubah_po',
    type: "POST",
    data: {
      "_data1"  : data1,
      "_data2"  : PM,
      "_data3"  : temp,
      "_id"     : idPO
    },
      success: function (msg, status) {
       $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
       window.location.href = "/"+base_url[1]+"/PO_Asset/index";
      },
      error: function(msg, status, data){
        console.log(msg);
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
}

function get_asset(){
  var groupasset = $("#GroupAsset").val();
  var namagroupasset = $("#GroupAsset option:selected").attr('data-namagroupasset');
  $.ajax({
    url : '/'+base_url[1]+'/PO_Asset/getAsset',
    type: "POST",
    data:{IDGroupAsset:namagroupasset},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      var html2 = '';
      if (data <= 0) {

        console.log('-----------data Asset kosong------------');
        html2 += '<option value="">Data Asset Kosong</option>';
        $("#Nama").html(html2);
      } else {
          html += '<option value="">--Silahkan Pilih Asset--</option>';
          for (var i = 0 ; i < data.length; i++) {
            html +='<option value="'+data[i]["IDAsset"]+'" data-namaAsset="'+data[i]["Nama_Asset"]+'"> '+data[i]["Nama_Asset"]+' </option>';
        
      }
          $("#Nama").html(html);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
}

function zeroValue(){
  $("#Tanggal").val('');
  $("#Nomor").val('');
  $("#Total_qty").val('');
  $("#Keterangan").val('');

}
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}