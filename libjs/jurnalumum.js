var base_url = window.location.pathname.split('/');

$(document).ready(function(){
  if(($("#NamaPerkiraan1").val() != "")){
    cek();
  }
  if(($("#KodePerkiraan2").val() != "")){
    cek2();
  }

  if(($("#Posisi").val() != "")){
    cek3();
  }

  $("#NamaPerkiraan1").change(cek);
  $("#KodePerkiraan2").change(cek2);
  $("#Posisi").change(cek3);
})

var PM = [];
var temp = [];

function cek() {
  var namaCOA1 = $("#NamaPerkiraan1 option:selected").attr('data-kodeCOA');
  $("#KodePerkiraan1").val(namaCOA1);
}

function cek2() {
  var namaCOA2 = $("#KodePerkiraan2 option:selected").attr('data-namaCOA');
  $("#NamaPerkiraan2").val(namaCOA2);

}

function cek3() {
  var posisiBaru = $("#Posisi").val();
    //alert(posisiBaru);
    //console.log(posisiBaru);
    if (posisiBaru == "Kredit") {
      _posisiBaru = "Kredit"
    } else {
      _posisiBaru = "Debet"
    }
    $("#ubahPosisi").html(_posisiBaru);
    
  }

  function initialization(){
    field = null;
    var _kredit= 0;
    var _debet = 0;
    if ($("#Posisi").val() == 'Kredit') {
      _kredit = $("#uangsejumlah").val().replace(/\./g,'');
    } else if ($("#Posisi").val() == 'Debet'){
      _debet = $("#uangsejumlah").val().replace(/\./g,'');
    }
    field = {
      "IDCOA" : $("#KodePerkiraan1").val(),
      "Namacoa": $("#NamaPerkiraan1 option:selected").attr('data-Namacoa'),
      "Nama_COA" : $("#NamaPerkiraan1").val(),
      "Kredit" : _kredit,
      "Debet" : _debet,
      "Posisi" : $("#Posisi").val(),
      "IDMataUang" : 1,
      "Kurs" : 1,
      "Keterangan" : $("#Keterangan").val(),
    }

    return field;
  }

  $("#tambah_sementara").click(function(){
    var field = initialization();
    PM.push(field);
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);
    
  });

  function renderJSON(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    var totaldebit = 0;
    var totalkredit = 0;
    var selisih = 0;

    if(data != null){
      PM = [];
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        PM[i].Posisi
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].IDCOA+"</td>"+
        "<td>"+PM[i].Namacoa+"</td>"+
        "<td>"+rupiah(PM[i].Debet)+"</td>"+
        "<td>"+rupiah(PM[i].Kredit)+"</td>"+
        "<td>"+PM[i].Keterangan+"</td>"+
        "<td>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"+
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
        totalQty += parseFloat(PM[i].NilaiGiro);
        totaldebit += parseFloat(PM[i].Debet);
        totalkredit += parseFloat(PM[i].Kredit);
        selisih= totaldebit-totalkredit;
        // grandTotal += parseInt(PM[i].Sub_total);
      }
      $("#Total").val(rupiah(totalQty));
      $("#totaldebet").val(rupiah(totaldebit));
      $("#totalkredit").val(rupiah(totalkredit));
      $("#selisih").val(rupiah(selisih));
      //console.log(totalQty);

      //-----Buat Field inputan sementara kosong lagi
    }

    $("#uangsejumlah").val("");
    $("#Keterangan").val("");
  }

  function initialization_edit(){
    field = null;
    var _kredit= 0;
    var _debet = 0;
    if ($("#Posisi").val() == 'Kredit') {
      _kredit = $("#uangsejumlah").val().replace(/\./g,'');
    } else if ($("#Posisi").val() == 'Debet'){
      _debet = $("#uangsejumlah").val().replace(/\./g,'');
    }
    field = {
      "IDCOA" : $("#NamaPerkiraan1").val(),
      "Nama_COA": $("#NamaPerkiraan1 option:selected").attr('data-Namacoa'),
      "Kode_COA" : $("#NamaPerkiraan1 option:selected").attr('data-kodeCOA'),
      "Kredit" : _kredit,
      "Debet" : _debet,
      "Posisi" : $("#Posisi").val(),
      "IDMataUang" : 1,
      "Kurs" : 1,
      "Keterangan" : $("#Keterangan2").val(),
    }

    return field;
  }

   $("#tambah_sementara_edit").click(function(){
    var field = initialization_edit();
    PM.push(field);
    setTimeout(function(){
      renderJSONE(PM);
      field = null;
    },500);
    
  });

  function renderJSONE(data){
   var totaldebit = 0;
   var totalkredit = 0;
   var selisih = 0;
   var totalQty = 0;
   if(data != null){
    PM = [];
    $("#tabel-cart-asset").html("");
    for(i = 0; i<data.length;i++){
      PM[i] = data[i];
      var value =
      "<tr>" +
      "<td>"+(i+1)+"</td>"+
      "<td>"+PM[i].Kode_COA+"</td>"+
      "<td>"+PM[i].Nama_COA+"</td>"+
      "<td>"+rupiah(PM[i].Debet)+"</td>"+
      "<td>"+rupiah(PM[i].Kredit)+"</td>"+
      "<td>"+PM[i].Keterangan+"</td>"+
      "<td>" +
      "<div class='hidden-sm hidden-xs action-buttons'>"+
      "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItemedit("+i+")'>Hapus </a>"+
      "</div>" +
      "</td>"+
      "</tr>";
      $("#tabel-cart-asset").append(value);
      totalQty += parseFloat(PM[i].NilaiGiro);
      totaldebit += parseFloat(PM[i].Debet);
      totalkredit += parseFloat(PM[i].Kredit);
      selisih= totaldebit-totalkredit;
    }

    $("#Total").val(rupiah(totalQty));
    $("#totaldebet").val(rupiah(totaldebit));
    $("#totalkredit").val(rupiah(totalkredit));
    $("#selisih").val(rupiah(selisih));

  }
}

function deleteItem(i){

  if(PM[i].id !=''){
   temp.push(PM[i]);
 }

 PM.splice(i,1);
 renderJSON(PM);
}

function deleteItemedit(i){

  if(PM[i].id !=''){
   temp.push(PM[i]);
 }

 PM.splice(i,1);
 renderJSONE(PM);
}

function fieldPo(){
  var data1 = {
    "Tanggal" :$("#Tanggal").val(),
    "Nomor" :$("#Nomor").val(),
    "Total_debit" :$("#totaldebet").val(),
    "Total_kredit" : $("#totalkredit").val(),
    "Batal" : "aktif",
  }
  return data1;
}

function save(){
  var data1 = fieldPo();

  if($("#selisih").val() != 0)
  {
    $('#alert').html('<div class="pesan sukses">Debit dan Kredit Belum Balance</div>');
  }else{
    $.ajax({
      url : '/'+base_url[1]+'/JurnalUmum/simpan_jurnalumum',
      type: "POST",
      data:{_data1:data1, _data2:PM},
      dataType:'json',
      success: function(data)
      { 
        zeroValue;
        console.log('----------Ajax berhasil-----');
        //console.log(data);
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        window.location.href = "/"+base_url[1]+"/JurnalUmum/index";
        // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
        // document.getElementById("simpan").setAttribute("onclick", "return false;");
        // document.getElementById("print").setAttribute("style", "cursor: pointer;");
        // document.getElementById("print").setAttribute("onclick", "return true;");
        // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/JurnalUmum/print_data_jurnalumum/'+data1['id_jurnalumum_tti']);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
  }
}

function save_change(){
  var data1 = fieldPo();
  var IDJU = $("#IDJU").val();
  console.log(PM);
   if($("#selisih").val() != 0)
  {
    $('#alert').html('<div class="pesan sukses">Debit dan Kredit Belum Balance</div>');
  }else{
  $.ajax({

    url : '/'+base_url[1]+'/JurnalUmum/ubah_jurnalumum',

    type: "POST",

    data: {

     "_data1" : data1,
     "_data2": PM,
     "_data3" : temp,
     "_id":IDJU

   },

     // dataType: 'json',

     success: function (msg, status) {
      $('#alert').html('<div class="pesan sukses">Data berhasil Diubah</div>');
      window.location.href = "/"+base_url[1]+"/JurnalUmum/index";
      
      // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      // document.getElementById("simpan").setAttribute("onclick", "return false;");
      // document.getElementById("print").setAttribute("style", "cursor: pointer;");
      // document.getElementById("print").setAttribute("onclick", "return true;");
      // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/JurnalUmum/print_data_jurnalumum/'+IDJU);

    },
    error: function(msg, status, data){
      // alert("Failure"+msg+status+data);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
}
}



function zeroValue(){
  $("#Tanggal").val('');
  $("#Nomor").val('');
  $("#Tanggal_selesai").val('');
  $("#corak").val('');
  $("#merk").val('');
  $("#Lot").val('');
  $("#Total_qty").val('');
  $("#PIC").val('');
  $("#Keterangan").val('');

}
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}