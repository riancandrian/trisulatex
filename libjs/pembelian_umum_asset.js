      
function renderJSONE(data){
  if(data != null){
    if(PM == null)
      PM = [];
    $("#tb_preview_penerimaan").html("");
    for(i = 0; i<data.length;i++){
      PM[i] = data[i];
      value = "<tr>"+
      "<td class='text-center'>"+(i+1)+"</td>"+
      "<td>"+PM[i].Nama_Asset+"</td>"+
      "<td>"+PM[i].Qty+"</td>"+

      "<td>"+"<input type=text value="+rupiah(PM[i].Harga_Satuan)+" name=harga_satuan id=harga_satuan"+i+" onkeyup=hitung_total("+i+")>"+"</td>"+
      "<td>"+"<input value="+rupiah(PM[i].Subtotal)+" type=text name=harga_total_b id=harga_total_b"+i+" value=0><input value="+PM[i].Sub_total+" type=hidden name=harga_total id=harga_total"+i+" value=0>"+"</td>"+
      "</tr>";
      $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);

      totaldpp=0;
      totalqty=0;
      totalqty += parseFloat(PM[i].Qty);
      totaldpp += parseFloat(PM[i].Subtotal);

      disc = 0;
      if($("#Status_ppn").val()=="Include"){
       if(disc == 0)
       {
         dpp=  100/110*totaldpp;
         ppn= dpp*0.1;
         $("#DPP").val(rupiah(Math.round(dpp)));
         $("#PPN").val(rupiah(Math.round(ppn)));
         $("#Discount_total").val(rupiah(disc));
         total= totaldpp-ppn;
         $("#total_invoice").val(rupiah(total-disc+ppn));
         $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
       }else{
         if(disc == 0){
           ppn= totaldpp*0.1;
           $("#DPP").val(rupiah(Math.round(totaldpp)));
           $("#PPN").val(rupiah(Math.round(ppn)));
           $("#Discount_total").val(rupiah(disc));
           $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
           $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
         }else{
           ppn= (totaldpp-disc)*0.1;
           $("#DPP").val(rupiah(Math.round(totaldpp)));
           $("#PPN").val(rupiah(Math.round(ppn)));
           $("#Discount_total").val(rupiah(disc));
           $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
           $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
         }
       }
     }else{
      ppn= totaldpp*0.1;
      $("#DPP").val(rupiah(totaldpp));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(disc);
      $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
    }
  }
  $("#Total").val(totaldpp);
  $("#total_qty").val(totalqty);
}
}

function fieldInvoice(){
  var data1 = {
    "IDFBA" : $("#IDFBA").val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor":$("#Nomor").val(),
    "Total_nilai_perolehan":$("#Total_nilai_perolehan").val(),
    "Total_akumulasi_penyusutan":$("#Total_akumulasi_penyusutan").val(),
    "Total_nilai_buku":$("#Total_nilai_buku").val()
  }
  return data1;
}

function ubah_status_ppn()
{
  disc = $("#Discount").val();
  if($("#Status_ppn").val()=="Include"){
    // ppn= $("#Total").val()*0.1;
    if(disc==0){
      dpp=  100/110*$("#Total").val();
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(Math.round(dpp)));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      dpp=  100/110*($("#Total").val()-disc);
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(Math.round(dpp)));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }else if($("#Status_ppn").val()=="Nonppn"){
    ppn= 0;
    $("#DPP").val(rupiah(Math.round($("#Total").val())));
    $("#PPN").val(rupiah(Math.round(ppn)));
    $("#Discount_total").val(rupiah(disc));
    total= $("#Total").val()-ppn;
    $("#total_invoice").val(rupiah(total-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
  }else{
    if(disc == 0){
      ppn= $("#Total").val()*0.1;
      $("#DPP").val(rupiah(Math.round($("#Total").val())));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      ppn= ($("#Total").val()-disc)*0.1;
      $("#DPP").val(rupiah(Math.round($("#Total").val())));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }
}


