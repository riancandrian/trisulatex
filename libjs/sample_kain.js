/**
 * Created by ASUS on 11/5/2018.
 */
 var base_url = window.location.pathname.split('/');
 var PM = [];
 var temp = [];

 $(document).ready(function(){

    var html = '';
    var field = null;

    $("#Barcode").change(function(){
        var barcod = $("#Barcode");
        //alert(barcod.val());

        console.log('--Proses Ajax--');
        //----------------------------------------Ajax Kirim data barcode
        $.ajax({
            url : '/'+base_url[1]+'/SampleKain/getstok',
            type: "POST",
            data:{barcode:barcod.val()},
            dataType:'json',
            success: function(data)
            {
                var html = '';
                if (data <= 0) {
                    console.log('-----------data kosong------------');
                } else {
                    field = {
                        "Barcode" : data[0]['Barcode'],
                        "IDCorak" : data[0]['IDCorak'],
                        "IDWarna" : data[0]['IDWarna'],
                        "IDMerk" : data[0]['IDMerk'],
                        "Merk" : data[0]['Merk'],
                        "Warna" : data[0]['Warna'],
                        "Corak" : data[0]['Corak'],
                        "Satuan" : data[0]['Satuan'],
                        "grade" : data[0]['Grade'],
                        "Qty_yard" : data[0]['Qty_yard'],
                        "Qty_meter" : data[0]['Qty_meter'],
                        "Saldo_yard" : data[0]['Saldo_yard'],
                        "Saldo_meter" : data[0]['Saldo_meter'],
                        "Grade" : data[0]['Grade'],
                        "IDSatuan" : data[0]['IDSatuan'],
                        "IDBarang" : data[0]['IDBarang'],
                        "Lebar" : '-',
                        "Harga" : parseFloat(data[0]['Harga']),
                        "Qty_jual" : 0,
                        "Qty_meter_jual" : 0
                    }
                    PM.push(field);
                    setTimeout(function(){
                        renderJSON(PM);
                        field = null;
                    },500);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
        barcod.val('');
    });
});

 function renderJSON(data){
    var totalYard = 0;
    var totalMeter = 0;
    if(data != null){
        PM = [];
        // $("#tabel-cart-asset").html("");
        var table = $('#tabelfsdfsf').DataTable();
        table.clear().draw();
        for(i = 0; i<data.length;i++){
            PM[i] = data[i];
            // var value =
            //     "<tr>" +
            //     "<td>"+(i+1)+"</td>"+
            //     "<td>"+PM[i].Barcode+"</td>"+
            //     "<td>"+PM[i].Corak+"</td>"+
            //     "<td>"+PM[i].Warna+"</td>"+
            //     "<td>"+PM[i].Grade+"</td>"+
            //     "<td>" +
            //         "<input type='text' value='"+PM[i].Qty_yard+"' name='qty_jual"+i+"' id='qty_jual"+i+"' onkeyup=Qty_jual("+i+")>" +
            //         "<input type='hidden' name='qty_meter_convert"+i+"' id='qty_meter_convert"+i+"'>" +
            //     "</td>"+
            //     "<td>"+PM[i].Satuan+"</td>"+
            //     "<td>"+PM[i].Harga+"</td>"+
            //     "<td>" +
            //     "<div class='hidden-sm hidden-xs action-buttons'>"+
            //     "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
            //     "</div>" +
            //     "</td>"+
            //     "</tr>";
            // $("#tabel-cart-asset").append(value);

            table.row.add([
                i+1,
                PM[i].Barcode,
                PM[i].Corak,
                PM[i].Warna,
                PM[i].Grade,
                "<input type='text' value='"+PM[i].Saldo_yard+"' name='qty_jual"+i+"' id='qty_jual"+i+"' onkeyup=Qty_jual("+i+")>" +
                "<input type='hidden' name='qty_meter_convert"+i+"' id='qty_meter_convert"+i+"'>",
                PM[i].Satuan,
                PM[i].Harga,
                "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"
                ]).draw();
            totalYard += parseInt(PM[i].Qty_yard);
            totalMeter += parseInt(PM[i].Qty_meter);
            $("#Qty_yard").data("totalYard",PM[i].Saldo_yard);
            $("#Qty_meter").data("totalmeter",PM[i].Saldo_meter);
            x = $("#qty_jual"+i).val() * 0.9144;
            document.getElementById("qty_meter_convert"+i).value = x.toFixed(2);

            PM[i].Qty_jual = $("#qty_jual"+i).val();
            PM[i].Qty_meter_jual = parseFloat(x.toFixed(2));

        }
        $("#Qty_yard").val(totalYard);
        $("#Qty_meter").val(totalMeter);
    }
}

function Qty_jual(i) {
    x = $("#qty_jual"+i).val() * 0.9144;
    document.getElementById("qty_meter_convert"+i).value = x.toFixed(2);

    PM[i].Qty_jual = $("#qty_jual"+i).val();
    PM[i].Qty_meter_jual = parseFloat(x.toFixed(2));
}

function deleteItem(i){
    if(PM[i].id !=''){
        temp.push(PM[i]);
    }

    PM.splice(i,1);
    renderJSON(PM);
}

function fieldSampleKain(){
    var data1 = {
        "id_fjsp" :$("#id_makloon_jahit").val(),
        "Tanggal" :$("#Tanggal").val(),
        "Nomor" :$("#Nomor").val(),
        "IDCustomer" :$("#IDSupplier").val(),
        // "IDPO" :$("#IDPO").val(),
        //"Total_qty_yard" :$("#Qty_yard").val(),
        //"Total_qty_meter" :$("#Qty_meter").val(),
        //"Saldo_yard" :$("#Qty_yard").val(),
        //"Saldo_meter" :$("#Qty_meter").val(),
        "keterangan" : $('#keterangan').val()
    }
    return data1;
}

//-----------------------------------Simpan
function save(){
    $("#IDSupplier").css('border', '');

    if($("#IDSupplier").val()==""){
        $('#alert').html('<div class="pesan sukses">Supplier Tidak Boleh Kosong</div>');
        $("#IDSupplier").css('border', '1px #C33 solid').focus();
    }else{
        var data1 = fieldSampleKain();
        var idsk = '_';
        //console.log(PM);
        $.ajax({
            url : '/'+base_url[1]+'/SampleKain/store',
            type: "POST",
            data:{_data1:data1, _data2:PM},
            dataType:'json',
            success: function(data)
            {
                console.log('----------Ajax berhasil-----');
                $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
                document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
                document.getElementById("simpan").setAttribute("onclick", "return false;");
                document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
                document.getElementById("printdata").setAttribute("onclick", "return true;");
                document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/SampleKain/print_/'+ data1['Nomor']);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
            }
        });
    }
}