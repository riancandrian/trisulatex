var base_url = window.location.pathname.split('/');

  $(document).ready(function(){
    if(($("#IDPAC").val() != "")){
      cek();
    //   if(($("#warna").val() != "")){
    //   get_warna($("#IDPAC").val());
      
    // }
    }
    $("#IDPAC").change(cek);

  })

  var PM = [];
  var temp = [];
  

  function cek() {
    $(".rowid2").remove();
    PM = [];
    var _idpac = $("#IDPAC").val();
    //console.log(base_url[1]+'/SuratJalan/getmerk');
    //console.log(_idpac);
    $.ajax({
        url : '/'+base_url[1]+'/SuratJalan/get_detailpackinglist',
        type: "POST",
        data:{idPAC:_idpac},
        dataType:'json',
        success: function(data)
        { 
          var html = '';
          if (data <= 0) {
            console.log('-----------data harga jual kosong------------');
          } else {
            //---------------Set value customer

            var cusTomer = $("#IDPAC option:selected").data('namacustomer');
            var idCus = $("#IDPAC option:selected").attr('data-idcustomer');
            $("#IDCustomer").val(cusTomer);
            $('#IDCustomer').data('idcustomer',idCus);
            //----------------end set value customer
            for (let i = 0; i < data.length; i++) {
              field = {
                "Barcode" : data[i]['Barcode'],
                "IDCorak" : data[i]['IDCorak'],
                "IDWarna" : data[i]['IDWarna'],
                "IDMerk" : data[i]['IDMerk'],
                "Merk" : data[i]['Merk'],
                "Warna" : data[i]['Warna'],
                "Corak" : data[i]['Corak'],
                "Satuan" : data[i]['Satuan'],
                "Qty_yard" : data[i]['Qty_yard'],
                "Qty_meter" : data[i]['Qty_meter'],
                "Saldo_yard" : data[i]['Saldo_yard'],
                "Saldo_meter" : data[i]['Saldo_meter'],
                "Grade" : data[i]['Grade'],
                "IDSatuan" : data[i]['IDSatuan'],
                "IDBarang" : data[i]['IDBarang'],
                "Lebar" : '-',
                "Nama_Barang" : data[i]['Nama_Barang'],
                "Total_pcs_grade_a" : data[i]['Total_pcs_grade_a'],
                "Total_pcs_grade_b" : data[i]['Total_pcs_grade_b'],
                "Total_pcs_grade_e" : data[i]['Total_pcs_grade_e'],
                "Total_pcs_grade_s" : data[i]['Total_pcs_grade_s'],
                "IDPACDetail" : data[i]['IDPACDetail'],
              }
              console.log(field);
              PM.push(field);
              setTimeout(function(){
                renderJSON(PM);
                field = null;
              },500);
              
            }
          }
           
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
  }

  function renderJSON(data){
    

    totalQty = 0;
    saldoQty = 0;
    if(data != null){
      PM = [];

      // $("#tabel-cart-asset").html("");
      var table = $('#tabelfsdfsf').DataTable();
        table.clear().draw();
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        if (PM[i].Grade == 'A') {
          _td = PM[i].Total_pcs_grade_a;
        } else if (PM[i].Grade == 'B') {
          _td = PM[i].Total_pcs_grade_b;
        } else if (PM[i].Grade == 'C') {
          _td = PM[i].Total_pcs_grade_e;
        } else if (PM[i].Grade == 'D') {
          _td = PM[i].Total_pcs_grade_s;
        }

        if (PM[i].Saldo_meter != null) {
          PM[i].Qty_tampil = PM[i].Saldo_meter
        } else {
          PM[i].Qty_tampil = PM[i].Saldo_yard
        }
      
        // var value =
        // "<tr class='rowid2'>" +
        // "<td>"+(i+1)+"</td>"+
        // "<td>"+PM[i].Corak+"</td>"+
        // "<td>"+PM[i].Nama_Barang+"</td>"+
        // "<td>"+PM[i].Merk+"</td>"+
        // "<td>"+PM[i].Warna+"</td>"+
        // "<td>"+PM[i].Grade+"</td>"+
        // "<td><input type=number step=0.01 min=0 max="+PM[i].Qty_yard+" value="+PM[i].Qty_yard+" onkeyup=convert('C',"+i+") name=_panjangyard id=_panjangyard"+i+"></td>"+
        // "<td><input type=number step=0.01 min=0 max="+PM[i].Qty_tampil+" value="+PM[i].Qty_tampil+" onkeyup=convert('F',"+i+") name=_panjangmeter id=_panjangmeter"+i+"></td>"+
        // "<td>" +
        // "<div class='hidden-sm hidden-xs action-buttons'>"+
        // "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"+
        // "</div>" +
        // "</td>"+
        // "</tr>";
       
        // $("#tabel-cart-asset").append(value);

        table.row.add([
              i+1,
              PM[i].Corak,
              PM[i].Nama_Barang,
              PM[i].Merk,
              PM[i].Warna,
              PM[i].Grade,
             "<input type=number step=0.01 min=0 max="+PM[i].Saldo_yard+" value="+PM[i].Saldo_yard+" onkeyup=convert('C',"+i+") name=_panjangyard id=_panjangyard"+i+">",
             "<input type=number step=0.01 min=0 max="+PM[i].Qty_tampil+" value="+PM[i].Qty_tampil+" onkeyup=convert('F',"+i+") name=_panjangmeter id=_panjangmeter"+i+">",
              PM[i].Grade,
              PM[i].Satuan,
              "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"
              ]).draw();

        totalQty += parseInt(PM[i].Saldo_yard);
        saldoQty += parseInt(PM[i].Saldo_yard);
      }
      $("#Total_qty").val(totalQty);
      $("#Saldo_qty").val(saldoQty);
     
    }
  }

  function renderJSONE(data){
    totalQty = 0;
    saldoQty = 0;
    if(data != null){
      PM = [];
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        if (PM[i].Saldo_roll != null) {
          PM[i].Qty_tampil = PM[i].Saldo_roll
        } else {
          PM[i].Qty_tampil = PM[i].Saldo_yard
        }
      
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Corak+"</td>"+
        "<td>"+PM[i].Nama_Barang+"</td>"+
        "<td>"+PM[i].Merk+"</td>"+
        "<td>"+PM[i].Warna+"</td>"+
        "<td>"+PM[i].Grade+"</td>"+
// <<<<<<< Updated upstream
        "<td><input type=number step=0.01 min=0 max="+PM[i].Saldo_yard+" value="+PM[i].Saldo_yard+" onkeyup=convert('C',"+i+") name=_panjangyard id=_panjangyard"+i+"></td>"+
        "<td><input type=number step=0.01 min=0 max="+PM[i].Qty_tampil+" value="+PM[i].Qty_tampil+" onkeyup=convert('F',"+i+") name=_panjangmeter id=_panjangmeter"+i+"></td>"+
// =======
//         "<td><input type=text value="+PM[i].Qty_yard+" onkeyup=convert('C',"+i+") name=_panjangyard id=_panjangyard"+i+"></td>"+
//         "<td><input type=text value="+PM[i].Qty_roll+" onkeyup=convert('F',"+i+") name=_panjangmeter id=_panjangmeter"+i+"></td>"+
// >>>>>>> Stashed changes
        "<td>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItemEdit("+i+")'>Hapus </a>"+
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
        totalQty += parseInt(PM[i].Saldo_yard);
        saldoQty += parseInt(PM[i].Saldo_yard);
      }
     $("#Total_qty").val(totalQty);
      $("#Saldo_qty").val(saldoQty);
    }
  }

  function convert(degree, i) {
    var x;
    if (degree == "C") {
        x = document.getElementById("_panjangyard"+i).value * 0.9144
        document.getElementById("_panjangmeter"+i).value = x.toFixed(2);
    } else {
        x = (document.getElementById("_panjangmeter"+i).value * 1.09361);
        document.getElementById("_panjangyard"+i).value = x.toFixed(2);
    }
  }

  function deleteItem(i){

      if(PM[i].id !=''){
       temp.push(PM[i]);
     }

     PM.splice(i,1);
     renderJSON(PM);
   }

   function deleteItemEdit(i){

      if(PM[i].id !=''){
       temp.push(PM[i]);
     }

     PM.splice(i,1);
     renderJSONE(PM);
   }

   function fieldPo(){
    var data1 = {
      "Tanggal" :$("#Tanggal").val(),
      "Nomor" :$("#Nomor").val(),
      "IDPAC" :$("#IDPAC").val(),
      "IDCustomer" :$("#IDCustomer").data('idcustomer'),
      "Tanggal_kirim" :$("#Tanggal_kirim").val(),
      "IDMataUang" :1,
      "Kurs" :1,
      "Total_qty" :$("#Total_qty").val(),
      "Saldo_qty" :$("#Saldo_qty").val(),
      "Keterangan" :$("#Keterangan").val(),
      "Batal" : "aktif",
      
    }
    return data1;
  }

  function fieldPo2(){
    var data1 = {
      "Tanggal" :$("#Tanggal").val(),
      "Nomor" :$("#Nomor").val(),
      "IDPAC" :$("#IDPAC2").val(),
      "IDCustomer" :$("#IDCustomer").data('idcustomer'),
      "Tanggal_kirim" :$("#Tanggal_kirim").val(),
      "IDMataUang" :1,
      "Kurs" :1,
      "Total_qty" :$("#Total_qty").val(),
      "Saldo_qty" :$("#Saldo_qty").val(),
      "Keterangan" :$("#Keterangan").val(),
      "Batal" : "aktif",
      
    }
    return data1;
  }
   
  function save(){
    var data1 = fieldPo();
  $("#IDPAC").css('border', '');
  $("#IDCustomer").css('border', '');

  if($("#IDPAC").val()==""){
    $('#alert').html('<div class="pesan sukses">Data PAC Tidak Boleh Kosong</div>');
    $("#IDPAC").css('border', '1px #C33 solid').focus();
  }else if($("#IDCustomer").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Customer Tidak Boleh Kosong</div>');
    $("#IDCustomer").css('border', '1px #C33 solid').focus();
  }else{
    console.log(PM);
    for (let i = 0; i < PM.length; i++) {
      PM[i].yardnew = $("#_panjangyard"+i).val();
      PM[i].meternew = $("#_panjangmeter"+i).val();
    }
    console.log('-----------Prosessimpan---------------')
    // console.log(PM)
    // console.log(data1)
    $.ajax({
      url : '/'+base_url[1]+'/SuratJalan/simpan_suratjalan',
      type: "POST",
      data:{_data1:data1, _data2:PM},
      dataType:'json',
      success: function(data)
      { 
        zeroValue;
        console.log('----------Ajax berhasil-----');
        console.log(data);
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        // window.location.href = "/"+base_url[1]+"/SuratJalan/index";
        document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
        document.getElementById("simpan").setAttribute("onclick", "return false;");
        document.getElementById("print").setAttribute("style", "cursor: pointer;");
        document.getElementById("print").setAttribute("onclick", "return true;");
        document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/SuratJalan/print_data_suratjalan/'+data['IDSJCK']);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
           $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
  }
}

  function save_change(){
    var data1 = fieldPo2();
    var IDSJCK = $("#IDSJCK").val();
    for (let i = 0; i < PM.length; i++) {
      PM[i].yardnew = $("#_panjangyard"+i).val();
      PM[i].meternew = $("#_panjangmeter"+i).val();
    }
    console.log(data1);

    $.ajax({

      url : '/'+base_url[1]+'/SuratJalan/ubah_suratjalan',

      type: "POST",

      data: {

       "_data1" : data1,
       "_data2": PM,
       "_data3" : temp,
       "_id":IDSJCK

     },

     // dataType: 'json',

     success: function (msg, status) {
      $('#alert').html('<div class="pesan sukses">Data berhasil Diubah</div>');
        window.location.href = "/"+base_url[1]+"/SuratJalan/index";
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/SuratJalan/print_data_suratjalan/'+idPO);

    },
    error: function(msg, status, data){
       alert("Failure"+msg+status+data);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
}



  function zeroValue(){
    $("#Tanggal").val('');
    $("#Nomor").val('');
    $("#Tanggal_selesai").val('');
    $("#corak").val('');
    $("#merk").val('');
    $("#Lot").val('');
    $("#Total_qty").val('');
    $("#PIC").val('');
    $("#Keterangan").val('');

  }
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}