var base_url = window.location.pathname.split('/');

var PM = [];
var temp = [];

function initialization(){
  field = null;
  field = {
    "GroupBarang" : $("#GroupBarang").val(),
    "NamaGroupBarang" : $("#GroupBarang option:selected").attr('data-namagroupbarang'),
    "NamaBarang": $("#Nama option:selected").attr('data-namabarang'),
    "Nama" : $("#Nama").val(),
    "Qty" : $("#Qty").val(),
    "Saldo" : $("#Qty").val(),
    "Harga" : $("#harga-123").val(),
    "Satuan" : $("#Satuan").val(),
    "NamaSatuan": $("#Satuan option:selected").attr('data-namasatuan'),
  }

  return field;
}

$("#tambah_sementara").click(function(){
  var field = initialization();
  PM.push(field);
  setTimeout(function(){
    renderJSON(PM);
    field = null;
  },500);

});

function renderJSON(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    var Total_warna = 0;
    if(data != null){
      if($("#Nama").val()==""){
        $('#alert').html('<div class="pesan sukses">Nama Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#Satuan").val()==""){
        $('#alert').html('<div class="pesan sukses">Satuan Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#Qty").val()==""){
        $('#alert').html('<div class="pesan sukses">Qty Tidak Boleh Kosong</div>');
        PM.pop();
      }else if($("#harga-123").val()==""){
        $('#alert').html('<div class="pesan sukses">Harga Tidak Boleh Kosong</div>');
        PM.pop();
      }else{
        PM = [];
        var table = $('#tabelfsdfsf').DataTable();
        table.clear().draw();
        for(i = 0; i<data.length;i++){
          PM[i] = data[i];
          PM[i].Total=PM[i].Harga.replace(/\./g,'')* PM[i].Qty.replace(/\./g,'')
          totalharga=PM[i].Total;
          totalqty=PM[i].Qty.replace(/\./g,'');
          table.row.add([
            i+1,
            PM[i].NamaGroupBarang,
            PM[i].NamaBarang,
            rupiah(PM[i].Qty.replace(/\./g,'')),
            PM[i].NamaSatuan,
            rupiah(PM[i].Harga.replace(/\./g,'')),
            rupiah(PM[i].Total),
            "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+","+totalharga+","+totalqty+")'>Hapus </a>"
            ]).draw().nodes().to$().addClass('rowarray'+i);
          totalQty += parseInt(PM[i].Qty.replace(/\./g,''));
          Total_warna += PM[i].Total;
        }
        $("#Total_qty").val(rupiah(totalQty));

        $('#harga-123').val('');
        $('#Qty').val('');
        $("#Totalwarna").val(rupiah(Total_warna));
      }
    }
  }


  function initialization2(){
    field = null;
    field = {
      "Group_Barang" : $("#GroupBarang option:selected").attr('data-namagroupbarang'),
      "IDGroupBarang" : $("#GroupBarang").val(),
      "Nama_Barang": $("#Nama option:selected").attr('data-namabarang'),
      "Nama" : $("#Nama").val(),
      "Qty" : $("#Qty").val(),
      "Saldo" : $("#Qty").val(),
      "Harga_satuan" : $("#harga-123").val(),
      "Satuan" : $("#Satuan option:selected").attr('data-namasatuan'),
      "IDSatuan":  $("#Satuan").val(),
    }

    return field;
  }


  $("#tambah_sementara2").click(function(){
    var field = initialization2();
    PM.push(field);
    setTimeout(function(){
      renderJSONE(PM);
      field = null;
    },500);

  });

  function renderJSONE(data){
    var totalQty = 0;
    var Total_warna = 0;
    if(data != null){
      PM = [];

      var table = $('#tabelfsdfsf').DataTable();
      table.clear().draw();
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        PM[i].Sub_total=PM[i].Harga_satuan.replace(/\./g,'')*PM[i].Qty.replace(/\./g,'');
        totalharga=PM[i].Sub_total;
        totalqty=PM[i].Qty.replace(/\./g,'');
        table.row.add([
          i+1,
          PM[i].Group_Barang,
          PM[i].Nama_Barang,
          rupiah(PM[i].Qty.replace(/\./g,'')),
          PM[i].Satuan,
          rupiah(PM[i].Harga_satuan),
          rupiah(PM[i].Sub_total),
          "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+","+totalharga+","+totalqty+")'>Hapus </a>"
          ]).draw().nodes().to$().addClass('rowarray'+i);
        Total_warna += PM[i].Sub_total;
        totalQty += parseInt(PM[i].Qty.replace(/\./g,''));
      // }
    }
    $("#Totalwarna").val(rupiah(Total_warna));
    $("#Total_qty").val(rupiah(totalQty));
  }
}

function deleteItem(i, totalharga, totalqty){

 $('.rowarray'+i).remove();
 PM.splice(i,1);
 $("#Totalwarna").val(rupiah(($("#Totalwarna").val().replace(/\./g,'')) - parseFloat(totalharga)));
 $("#Total_qty").val(($("#Total_qty").val()) - parseFloat(totalqty));
 // renderJSON(PM);
}

function fieldPo(){
  var posisi ='-';
  var posisi1 ='-';

  if ($("#cap").is(":checked")) {
    if($("#_cap1").is(":checked")) {
      posisi = $("#_cap1").val();
    }

    if($("#_cap2").is(":checked")) {
      posisi1 = $("#_cap2").val();
    }
  }
  
  
  var data1 = {
    "id_po_umum":$('#id_po_umum').val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor" :$("#Nomor").val(),
    "Tanggal_Selesai" :$("#Tanggal_selesai").val(),
    "IDSupplier" :$("#IDSupplier").val(),
    "IDAgen" :$("#IDAgen").data("id"),
    "Total_qty" :$("#Total_qty").val(),
    "Saldo" :$("#Total_qty").val(),
    "Catatan" :$("#Keterangan").val(),
    "Grand_total" : $("#Totalwarna").val(),
  }
  return data1;
}

function save(){
  var data1 = fieldPo();
  
  $("#Tanggal_selesai").css('border', '');
  $("#IDSupplier").css('border', '');

  if($("#Tanggal_selesai").val()==""){
    $('#alert').html('<div class="pesan sukses">Tanggal Selesai Tidak Boleh Kosong</div>');
    $("#Tanggal_selesai").css('border', '1px #C33 solid').focus();
  }else if($("#IDSupplier").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Supplier Tidak Boleh Kosong</div>');
    $("#IDSupplier").css('border', '1px #C33 solid').focus();
  }else{

    $.ajax({
      url : '/'+base_url[1]+'/PoUmum2/simpan_po_umum2',
      type: "POST",
      data:{_data1:data1, _data2:PM},
      dataType:'json',
      success: function(data)
      { 
        zeroValue;
        console.log('----------Ajax berhasil-----');
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        window.location.href = "/"+base_url[1]+"/PoUmum2/create";
        // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
        // document.getElementById("simpan").setAttribute("onclick", "return false;");
        // document.getElementById("print").setAttribute("style", "cursor: pointer;");
        // document.getElementById("print").setAttribute("onclick", "return true;");
        // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/PoUmum2/print_umum/'+data['IDPOUmum']);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
  }
}

function save_change(){
  var data1 = fieldPo();
  var idPO = $("#IDPO").val();

  $.ajax({

    url : '/'+base_url[1]+'/PoUmum2/ubah_po',

    type: "POST",

    data: {

      "_data1" : data1,
      "_data2": PM,
      "_data3" : temp,
      "_id":idPO

    },

     // dataType: 'json',

     success: function (msg, status) {
       $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
       window.location.href = "/"+base_url[1]+"/PoUmum2/index";
        // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
        // document.getElementById("simpan").setAttribute("onclick", "return false;");
        // document.getElementById("print").setAttribute("style", "cursor: pointer;");
        // document.getElementById("print").setAttribute("onclick", "return true;");
        // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/PoUmum/print_umum/'+idPO);

      },
      error: function(msg, status, data){
        console.log(msg);
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

      }

    });
}

function get_barang(){
  var groupbarang = $("#GroupBarang").val();
  var namagroupbarang = $("#GroupBarang option:selected").attr('data-namagroupbarang');
  $.ajax({
    url : '/'+base_url[1]+'/PoUmum2/getbarang',
    type: "POST",
    data:{IDGroupBarang:namagroupbarang},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      var html2 = '';
      if (data <= 0) {

        console.log('-----------data barang kosong------------');
        html2 += '<option value="">Data Barang Kosong</option>';
        $("#Nama").html(html2);
      } else {
          html += '<option value="">--Silahkan Pilih barang--</option>';
          for (var i = 0 ; i < data.length; i++) {
            if(data[i]["IDBarang"]=="" || data[i]["IDBarang"] == null){
            html +='<option value="'+data[i]["IDAsset"]+'" data-namabarang="'+data[i]["Nama_Asset"]+'"> '+data[i]["Nama_Asset"]+' </option>';
        }else{
            html +='<option value="'+data[i]["IDBarang"]+'" data-namabarang="'+data[i]["Nama_Barang"]+'"> '+data[i]["Nama_Barang"]+' </option>';
        }
      }
          $("#Nama").html(html);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
}

function zeroValue(){
  $("#Tanggal").val('');
  $("#Nomor").val('');
  $("#Tanggal_selesai").val('');
  $("#corak").val('');
  $("#merk").val('');
  $("#Lot").val('');
  $("#Total_qty").val('');
  $("#PIC").val('');
  $("#Keterangan").val('');

}
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}