var base_url = window.location.pathname.split('/');
var PM = [];
var temp = [];

//-----------------------------------------------------------------------ready
$(document).ready(function(){

  var html = '';
  var field = null;

  $("#Barcode").change(function(){
    var barcod = $("#Barcode");
    //alert(barcod.val());

    console.log('--Proses Ajax--');
    //----------------------------------------Ajax Kirim data barcode
    $.ajax({
      url : '/'+base_url[1]+'/MakloonCelup/getstok',
      type: "POST",
      data:{barcode:barcod.val()},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data kosong------------');
        } else {
          //console.log('Data Ada');
          //console.log(data[0]['Barcode']);
          field = {
            "Barcode" : data[0]['Barcode'],
            "IDCorak" : data[0]['IDCorak'],
            "IDWarna" : data[0]['IDWarna'],
            "IDMerk" : data[0]['IDMerk'],
            "Merk" : data[0]['Merk'],
            "Warna" : data[0]['Warna'],
            "Corak" : data[0]['Corak'],
            "Satuan" : data[0]['Satuan'],
            "Qty_yard" : data[0]['Qty_yard'],
            "Qty_meter" : data[0]['Qty_meter'],
            "Saldo_yard" : data[0]['Saldo_yard'],
            "Saldo_meter" : data[0]['Saldo_meter'],
            "Grade" : data[0]['Grade'],
            "IDSatuan" : data[0]['IDSatuan'],
            "IDBarang" : data[0]['IDBarang'],
            "Lebar" : '-',
          }
          //console.log(field);
          PM.push(field);
          setTimeout(function(){
            renderJSON(PM);
            field = null;
          },500);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
      }
    }); //----------------------------------------end ajax
    barcod.val('');
  });
}); //-----------------------------------------------------------------------end ready


function renderJSON(data){
  // console.log('-----------------')
  // console.log(data);
  var totalYard = 0;
  var totalMeter = 0;
  if(data != null){
    PM = [];
    $("#tabel-cart-asset").html("");
    for(i = 0; i<data.length;i++){
      PM[i] = data[i];
      var value =
      "<tr>" +
      "<td>"+(i+1)+"</td>"+
      "<td>"+PM[i].Barcode+"</td>"+
      "<td>"+PM[i].Corak+"</td>"+
      "<td>"+PM[i].Warna+"</td>"+
      "<td>"+PM[i].Merk+"</td>"+
      "<td>"+"<input type=text name=q_yard id=q_yard"+i+" value=0>"+"</td>"+
      "<td>"+"<input type=text name=q_meter id=q_meter"+i+" value=0>"+"</td>"+
      "<td>"+PM[i].Grade+"</td>"+
      "<td>"+PM[i].Satuan+"</td>"+
      "<td class='text-center'>" +
      "<span>" +
      "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
      "</span>" +
      "</td>"+
      "</tr>";
      $("#tabel-cart-asset").append(value);
     totalYard+= parseInt(PM[i].Qty_yard);
     totalMeter+= parseInt(PM[i].Qty_meter);

    }
    $("#total_yard").val(totalYard);
      $("#total_meter").val(totalMeter);
    
    // //console.log(totalQty);

    // //-----Buat Field inputan sementara kosong lagi
    // $('#warna').val('');
    // $('#Qty').val('');
  }
}



function renderJSONE(data){
    if(data != null){
      if(PM == null)
        PM = [];
  var totalYard = 0;
  var totalMeter = 0;
      $("#tabel-cart-asset").html("");
    for(i = 0; i<data.length;i++){
      PM[i] = data[i];
      var value =
      "<tr>" +
      "<td>"+(i+1)+"</td>"+
      "<td>"+PM[i].Barcode+"</td>"+
      "<td>"+PM[i].Corak+"</td>"+
      "<td>"+PM[i].Warna+"</td>"+
      "<td>"+"<input type=text name=q_yard id=q_yard"+i+" value="+PM[i].Qty_yard+">"+"</td>"+
      "<td>"+"<input type=text name=q_meter id=q_meter"+i+" value="+PM[i].Qty_meter+">"+"</td>"+
      "<td>"+PM[i].Grade+"</td>"+
      "<td class='text-center'>" +
      "<span>" +
      "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
      "</span>" +
      "</td>"+
      "</tr>";
      $("#tabel-cart-asset").append(value);
     totalYard+= parseInt(PM[i].Qty_yard);
     totalMeter+= parseInt(PM[i].Qty_meter);

    }
    $("#total_yard").val(totalYard);
      $("#total_meter").val(totalMeter);

        }
      }

       function deleteItem(i){

        if(PM[i].id !=''){
         temp.push(PM[i]);
       }

       PM.splice(i,1);
       renderJSONE(PM);
     }


     function fieldPersediaan(){
      var data1 = {
        "tanggal" :$("#tanggal").val(),
        "total_yard":$("#total_yard").val(),
        "no_kp":$("#no_kp").val(),
        "jenis":$("#jenis").val(),
        "total_meter":$("#total_meter").val(),
        "keterangan":$("#keterangan").val()
      }
      return data1;
    }

    function fieldPersediaan2(){
      var data1 = {
        "IDKPScan" : $("#IDKPScan").val(),
        "tanggal" :$("#tanggal").val(),
        "total_yard":$("#total_yard").val(),
        "no_kp":$("#no_kp").val(),
        "jenis":$("#jenis").val(),
        "total_meter":$("#total_meter").val(),
        "keterangan":$("#keterangan").val()
      }
      return data1;
    }

    function harga_qty(i)
{
  for(i=0; i < PM.length; i++){
    PM[i].q_yard = $("#q_yard"+i).val();
    PM[i].q_meter = $("#q_meter"+i).val();
    console.log(PM[i].q_yard);
    console.log(PM[i].q_meter);
}
// return PM.q_yard;
}

  function SavePersediaanScan(){
     var data1 = fieldPersediaan();
     var data2 = harga_qty();
     // console.log(PM);
     $.ajax({

      url: '/'+base_url[1]+'/KoreksiPersediaanScan/simpan_koreksi_persediaan_scan',

      type: "POST",

      data: {

       "data1" : data1,
       "data2": PM

     },

     dataType: 'json',

     success: function (msg, status) {
      // window.location.href = "index";
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/Pembelian_asset/print/'+data1['Nomor']);

    },
    error: function(msg, status){
     $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
   }

    function SavePersediaanScanChange(){
    var data1 = fieldPersediaan2();
     var data2 = harga_qty();
    // console.log(data1);
    console.log(data1);

    $.ajax({

      url: '/'+base_url[1]+'/KoreksiPersediaanScan/ubah_koreksi_persediaan_scan',

      type: "POST",

      data: {

       "data1" : data1,
       "data2": PM,
       "data3" : temp

     },

     // dataType: 'json',

     success: function (msg, status) {
   $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/KoreksiPersediaan/print/'+data1['Nomor']);


    },
    error: function(msg, status, data){
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });


  }