var base_url = window.location.pathname.split('/');
var PM = [];
var temp = [];

//-----------------------------------------------------------------------ready
$(document).ready(function(){
  
  var html = '';
  var field = null;

  $("#Barcode").change(function(){
    var barcod = $("#Barcode");
    var idSOK = $("#IDSOK");
    //alert(barcod.val());
     //console.log(idSOK.val());
    if (idSOK.val() == "") {
      barcod.val('');
      alert("Pilih SOK dulu");
    } else {
       //console.log('--Proses Ajax--');
      //----------------------------------------Ajax Kirim data barcode
      $.ajax({
        url : '/'+base_url[1]+'/PackingList/getstok',
        type: "POST",
        data:{barcode:barcod.val(), IDSOK:idSOK.val()},
        dataType:'json',
        success: function(data)
        { 
          var html = '';
          if (data <= 0) {
            //console.log('-----------data kosong------------');
            $('#alert').html('<div class="pesan sukses">Data Tidak Tersedia</div>');
            barcod.val('');
          } else {
            //console.log(data);
            //console.log(data[0]['Barcode']);
            field = {
              "Barcode" : data[0]['Barcode'],
              "IDCorak" : data[0]['IDCorak'],
              "IDWarna" : data[0]['IDWarna'],
              "IDMerk" : data[0]['IDMerk'],
              "Merk" : data[0]['Merk'],
              "Warna" : data[0]['Warna'],
              "Corak" : data[0]['Corak'],
              "Satuan" : data[0]['Satuan'],
              "Qty_yard" : data[0]['Qty_yard'],
              "Qty_meter" : data[0]['Qty_meter'],
              "Saldo_yard" : data[0]['Saldo_yard'],
              "Saldo_meter" : data[0]['Saldo_meter'],
              "Grade" : data[0]['Grade'],
              "IDSatuan" : data[0]['IDSatuan'],
              "IDBarang" : data[0]['IDBarang'],
              "Lebar" : '-',
              "IDStok" : data[0]['IDStok'],
            }
            //console.log(field);
            PM.push(field);
            setTimeout(function(){
              renderJSON(PM);
              field = null;
            },500);
          }
          
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
      }); //----------------------------------------end ajax
      barcod.val('');
    }   
  });
}); //-----------------------------------------------------------------------end ready



//----------------------------------------render Data sementara
function renderJSON(data){
  // console.log('-----------------')
  // console.log(data);
  var totalYardA = 0;
  var totalGradeA = 0;
  var totalYardB = 0;
  var totalGradeB = 0;
  var totalYardE = 0;
  var totalGradeE = 0;
  var totalYardS = 0;
  var totalGradeS = 0;
  
  if(data != null){
    PM = [];
    // $("#tabel-cart-asset").html("");
    var table = $('#tabelfsdfsf').DataTable();
        table.clear().draw();
    for(i = 0; i<data.length;i++){
      PM[i] = data[i];
      // var value =
      // "<tr>" +
      // "<td>"+(i+1)+"</td>"+
      // "<td>"+PM[i].Barcode+"</td>"+
      // "<td>"+PM[i].Corak+"</td>"+
      // "<td>"+PM[i].Warna+"</td>"+
      // "<td>"+PM[i].Merk+"</td>"+
      // "<td>"+"<input type=number step=0.01 min=0 max="+PM[i].Qty_yard+" value="+PM[i].Qty_yard+" onkeyup=convert('C',"+i+") name=_panjangyard id=_panjangyard"+i+"><input type=hidden value="+PM[i].Qty_meter+" name=_panjangmeter id=_panjangmeter"+i+">"+"</td>"+
      // "<td>"+PM[i].Grade+"</td>"+
      // "<td>"+PM[i].Satuan+"</td>"+
      // "<td>" +
      // "<div class='hidden-sm hidden-xs action-buttons'>"+
      //  "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
      // "</div>" +
      // "</td>"+
      // "</tr>";
      // $("#tabel-cart-asset").append(value);
       table.row.add([
              i+1,
              PM[i].Barcode,
              PM[i].Corak,
              PM[i].Warna,
              PM[i].Merk,
              "<input type=number step=0.01 min=0 max="+PM[i].Saldo_yard+" value="+PM[i].Saldo_yard+" onkeyup=convert('C',"+i+") name=_panjangyard id=_panjangyard"+i+"><input type=hidden value="+PM[i].Qty_meter+" name=_panjangmeter id=_panjangmeter"+i+">",
              PM[i].Grade,
              PM[i].Satuan,
              "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"
              ]).draw();

      if (PM[i].Grade == 'A') {
        totalYardA += parseInt(PM[i].Saldo_yard);
        totalGradeA += 1;
      } else if (PM[i].Grade == 'B') {
        totalYardB += parseInt(PM[i].Saldo_yard);
        totalGradeB += 1;
      } else if (PM[i].Grade == 'C') {
        totalYardC += parseInt(PM[i].Saldo_yard);
        totalGradeC += 1;
      } else if (PM[i].Grade == 'D') {
        totalYardD += parseInt(PM[i].Saldo_yard);
        totalGradeD += 1;
      }

    }
    $("#Total_qty_grade_a").val(totalYardA);
    $("#Total_pcs_grade_a").val(totalGradeA);
    $("#Total_qty_grade_b").val(totalYardB);
    $("#Total_pcs_grade_b").val(totalGradeB);
    $("#Total_qty_grade_e").val(totalYardE);
    $("#Total_pcs_grade_e").val(totalGradeE);
    $("#Total_qty_grade_s").val(totalYardS);
    $("#Total_pcs_grade_s").val(totalGradeS);
    
    // //console.log(totalQty);
   
    // //-----Buat Field inputan sementara kosong lagi
    // $('#warna').val('');
    // $('#Qty').val('');
  }
}
//----------------------------------------End render Data sementara

function convert(degree, i) {
  var x;
  if (degree == "C") {
      x = document.getElementById("_panjangyard"+i).value * 0.9144
      document.getElementById("_panjangmeter"+i).value = x.toFixed(2);
  } else {
      x = (document.getElementById("_panjangmeter"+i).value * 1.09361);
      document.getElementById("_panjangyard"+i).value = x.toFixed(2);
  }
}


//---------------------------------------- render Data Edit
function renderJSONE(data){
  // console.log('-----------------')
  // console.log(data);
  var totalQty = 0;
  var totalYardA = 0;
  var totalGradeA = 0;
  var totalYardB = 0;
  var totalGradeB = 0;
  var totalYardE = 0;
  var totalGradeE = 0;
  var totalYardS = 0;
  var totalGradeS = 0;
  if(data != null){
    PM = [];
    $("#tabel-cart-asset").html("");
    for(i = 0; i<data.length;i++){
      PM[i] = data[i];
      var value =
      "<tr>" +
      "<td>"+(i+1)+"</td>"+
      "<td>"+PM[i].Barcode+"</td>"+
      "<td>"+PM[i].Corak+"</td>"+
      "<td>"+PM[i].Warna+"</td>"+
      "<td>"+PM[i].Merk+"</td>"+
      "<td>"+"<input type=number step=0.01 min=0 max="+PM[i].Saldo_yard+" value="+PM[i].Saldo_yard+" onkeyup=convert('C',"+i+") name=_panjangyard id=_panjangyard"+i+"><input type=hidden value="+PM[i].Saldo_meter+" name=_panjangmeter id=_panjangmeter"+i+">"+"</td>"+
      // "<td>"+PM[i].Qty_meter+"</td>"+
      "<td>"+PM[i].Grade+"</td>"+
      "<td>"+PM[i].Satuan+"</td>"+
      "<td>" +
      "<div class='hidden-sm hidden-xs action-buttons'>"+
       "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItemEdit("+i+")'>Hapus </a>"+
      "</div>" +
      "</td>"+
      "</tr>";
      $("#tabel-cart-asset").append(value);
       if (PM[i].Grade == 'A') {
        totalYardA += parseInt(PM[i].Saldo_yard);
        totalGradeA += 1;
      } else if (PM[i].Grade == 'B') {
        totalYardB += parseInt(PM[i].Saldo_yard);
        totalGradeB += 1;
      } else if (PM[i].Grade == 'C') {
        totalYardC += parseInt(PM[i].Saldo_yard);
        totalGradeC += 1;
      } else if (PM[i].Grade == 'D') {
        totalYardD += parseInt(PM[i].Saldo_yard);
        totalGradeD += 1;
      }
    }
     $("#Total_qty_grade_a").val(totalYardA);
    $("#Total_pcs_grade_a").val(totalGradeA);
    $("#Total_qty_grade_b").val(totalYardB);
    $("#Total_pcs_grade_b").val(totalGradeB);
    $("#Total_qty_grade_e").val(totalYardE);
    $("#Total_pcs_grade_e").val(totalGradeE);
    $("#Total_qty_grade_s").val(totalYardS);
    $("#Total_pcs_grade_s").val(totalGradeS);

  }
}
//----------------------------------------End render Data Edit

//----------------------------------------Delete Item Sementara
function deleteItem(i){

  if(PM[i].id !=''){
   temp.push(PM[i]);
 }

 PM.splice(i,1);
 renderJSON(PM);
}

function deleteItemEdit(i){

  if(PM[i].id !=''){
   temp.push(PM[i]);
 }

 PM.splice(i,1);
 renderJSONE(PM);
}
//----------------------------------------End Delete Item Sementara


//------------------------------------------Get Field Makloon
function initialization(){
  var data1 = {
    "Tanggal" :$("#Tanggal").val(),
    "Nomor" :$("#Nomor").val(),
    "IDCustomer" :$("#IDCustomer").val(),
    "IDSOK" : $("#IDSOK").val(),
    "IDMataUang" :1,
    "Kurs" :1,
    "Total_qty_grade_a" :$("#Total_qty_grade_a").val(),
    "Total_pcs_grade_a" :$("#Total_pcs_grade_a").val(),
    "Total_qty_grade_b" :$("#Total_qty_grade_b").val(),
    "Total_pcs_grade_b" :$("#Total_pcs_grade_b").val(),
    "Total_qty_grade_e" :$("#Total_qty_grade_e").val(),
    "Total_pcs_grade_e" :$("#Total_pcs_grade_e").val(),
    "Total_qty_grade_s" :$("#Total_qty_grade_s").val(),
    "Total_pcs_grade_s" :$("#Total_pcs_grade_s").val(),
    "Keterangan" :$("#Keterangan").val(),
    "Batal" : "aktif",
  }
  return data1;
}


//-----------------------------------Simpan
function save(){
  var data1 = initialization();
    //console.log(data1)
    //console.log('----------- Prosessimpan---------------')

  $("#IDSOK").css('border', '');
  $("#NamaCustomer").css('border', '');

  if($("#IDSOK").val()==""){
    $('#alert').html('<div class="pesan sukses">Data SOK Tidak Boleh Kosong</div>');
    $("#IDSOK").css('border', '1px #C33 solid').focus();
  }else if($("#NamaCustomer").val()==""){
    $('#alert').html('<div class="pesan sukses">Data Customer Tidak Boleh Kosong</div>');
    $("#NamaCustomer").css('border', '1px #C33 solid').focus();
  }else{
    for (let i = 0; i < PM.length; i++) {
      PM[i].yardnew = $("#_panjangyard"+i).val();
      PM[i].meternew = $("#_panjangmeter"+i).val();
    }
    console.log(PM);
    $.ajax({
      url : '/'+base_url[1]+'/PackingList/simpan_packinglist',
      type: "POST",
      data:{_data1:data1, _data2:PM},
      dataType:'json',
      success: function(data)
      { 
        console.log('----------Ajax berhasil-----');
        //console.log(data);
        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
        // window.location.href = "/"+base_url[1]+"/Packinglist/index";
        document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
      document.getElementById("printdata").setAttribute("onclick", "return true;");
      document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/PackingList/print_packing_list/'+data['IDPAC']);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // console.log(jqXHR);
        // console.log(textStatus);
        // console.log(errorThrown);
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
}
}

//----------------------------------Simpan Edit
 function save_change(){
    var data1 = initialization();
    var idPAC = $("#IDPAC").val();

    for (let i = 0; i < PM.length; i++) {
      PM[i].yardnew = $("#_panjangyard"+i).val();
      PM[i].meternew = $("#_panjangmeter"+i).val();
    }

    $.ajax({

      url : '/'+base_url[1]+'/PackingList/ubah_packinglist',

      type: "POST",

      data: {

       "_data1" : data1,
       "_data2": PM,
       "_data3" : temp,
        "_id":idPAC

     },

     // dataType: 'json',

     success: function (data) {
      
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      window.location.href = "/"+base_url[1]+"/PackingList/index";
      //   document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      // document.getElementById("simpan").setAttribute("onclick", "return false;");
      // document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
      // document.getElementById("printdata").setAttribute("onclick", "return true;");
      // document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/PackingList/print/'+data1['Nomor']);
      // console.log('--------------berhasil----------');
      // console.log(data);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      // console.log(jqXHR);
      // console.log(textStatus);
      // console.log(errorThrown);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
    }

  });
}


  function cek_customer()
{
  var Nomor = $("#IDSOK").val();
  $.ajax({
    url : '/'+base_url[1]+'/PackingList/cek_customer',
    type: "POST",
    data:{Nomor:Nomor},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data kosong------------');
      } else {
       $("#NamaCustomer").val(data[0].Nama);
       $("#IDCustomer").val(data[0].IDCustomer)
  }
},
  error: function (jqXHR, textStatus, errorThrown)
  {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
  }
});
}


//-----------------------------------End Simpan Edit




