   var base_url = window.location.pathname.split('/');
   var PM = [];
   var temp = [];
   index = 0;
   function initialization(){
     var field = null;
     field = {
      "IDTBSH": "",
      "IDBarang":$("#IDBarang").val(),
      "namabarang" : $("#IDBarang option:selected").attr('data-namabarang'),
      "IDMerk":$("#IDMerk").val(),
      "NamaMerk":$("#NamaMerk").val(),
      "Qty":$("#Qty").val(),
      "IDSatuan":$("#IDSatuan").val(),
      "NamaSatuan":$("#NamaSatuan").val(),
      "Harga":$("#harga-123").val(),
      "Total_harga" : $("#harga-123").val().replace(/\./g,'')*$("#Qty").val(),
    }

    return field;
  }

  function initialization2(){
   var field = null;
   field = {
    "IDTBSH": "",
    "Nama_Barang":$("#Nama_Barang").val(),
    "IDBarang" : $("#Nama_Barang option:selected").attr('data-namabarang'),
    "Merk":$("#Merk").val(),
    "NamaMerk":$("#NamaMerk").val(),
    "Qty":$("#Qty").val(),
    "Satuan":$("#Satuan").val(),
    "NamaSatuan":$("#NamaSatuan").val(),
    "Harga":$("#harga-123").val(),
    "Total_harga" : $("#harga-123").val().replace(/\./g,'')*$("#Qty").val(),
  }

  return field;
}


$("#add-to-cart").click(function(){
  var field = initialization();
    // if(!findItem(field)){
      PM.push(field);
    // }
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);

  });

$("#add-to-cart-edit").click(function(){
  var field = initialization2();
    // if(!findItem(field)){
      PM.push(field);
    // }
    setTimeout(function(){
      renderJSONE(PM);
      field = null;
    },500);

  });



function findItem(data){
  if(PM != null){
    for(i = 0; i < PM.length;i++){
      PM[i] = data;
      return true;
    }
  }
  return false;
}

function renderJSON(data){
  if(data != null){
    if(PM == null)
      PM = [];

    var totalQty = 0;
      var table = $('#tabelfsdfsf').DataTable();
      table.clear().draw();
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        if($("#IDBarang").val()==""){
          $('#alert').html('<div class="pesan sukses">Barang Tidak Boleh Kosong</div>');
          PM.pop();
        }else if($("#Qty").val()==""){
          $('#alert').html('<div class="pesan sukses">Qty Tidak Boleh Kosong</div>');
          PM.pop();
        }else if($("#IDMerk").val()==""){
          $('#alert').html('<div class="pesan sukses">Merk Tidak Boleh Kosong</div>');
          PM.pop();
        }else if($("#IDSatuan").val()==""){
          $('#alert').html('<div class="pesan sukses">Satuan Tidak Boleh Kosong</div>');
          PM.pop();
        }else{
          table.row.add([
            i+1,
            PM[i].namabarang,
            PM[i].NamaMerk,
            PM[i].Qty,
            PM[i].NamaSatuan,
            PM[i].Harga,
            rupiah(PM[i].Total_harga),
            "<span>"+
            "<a href='#' class='action-icons c-edit' id='bootbox-confirm' onclick='editList("+i+")' title='Ubah Data'>Ubah </a>"+
            "</span>"+
            "<span>" +
            "<a href='#' class='action-icons c-delete' id='bootbox-confirm' onclick='deleteItem("+i+")'>Hapus </a>"+
            "</span>"
            ]).draw().nodes().to$().addClass('rowarray'+i);
          totalQty+= parseInt(PM[i].Qty);

          $("#Total_qty").val(totalQty);
        }
      }
    }
  }

  function renderJSONE(data){
    if(data != null){
      if(PM == null)
        PM = [];
      
      var totalQty = 0;
      $("#tabel-cart-pb-jahit").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Nama_Barang+"</td>"+
        "<td>"+PM[i].Merk+"</td>"+
        "<td>"+PM[i].Qty+"</td>"+
        "<td>"+PM[i].Satuan+"</td>"+
        "<td>"+PM[i].Harga+"</td>"+
        "<td>"+rupiah(PM[i].Total_harga)+"</td>"+
        "<td class='text-center'>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "<span>" +
        "<a href='#' class='action-icons c-edit' id='bootbox-confirm' title='Ubah Data' onclick='editList("+i+")'>Ubah </a>"+
        "</span>" +
        "<span>"+
        "<a href='#' class='action-icons c-delete'  id='bootbox-confirm' onclick='deleteItem("+i+")'>Delete"+
        "</a>"+
        "</span>"
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-pb-jahit").append(value);
        totalQty+= parseInt(PM[i].Qty);
      }
      $("#Total_qty").val(totalQty);
      
    }
  }
  
  

  function deleteItem(i){

    if(PM[i].id !=''){
     temp.push(PM[i]);
   }

   $('.rowarray'+i).remove();
   PM.splice(i,1);
   // renderJSON(PM);
 }

 function fieldpbjahit(){
  var data1 = {
    "IDTBSH" :$("#IDTBSH").val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor":$("#Nomor").val(),
    "IDSupplier":$("#IDSupplier").val(),
    "IDSJH":$("#IDSJH").val(),
    "Nomor_supplier":$("#Nomor_supplier").val(),
    "Total_qty":$("#Total_qty").val(),
    "Keterangan":$("#Keterangan").val()
  }
  return data1;
}

function fieldpbjahit2(){
  var data1 = {
    "IDTBSH" : $("#IDTBSH").val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor":$("#Nomor").val(),
    "IDSupplier":$("#IDSupplier").val(),
    "IDSJH":$("#IDSJH").val(),
    "Nomor_supplier":$("#Nomor_supplier").val(),
    "Total_qty":$("#Total_qty").val(),
    "Keterangan":$("#Keterangan").val()
  }
  return data1;
}

function savepbjahit(){
 var data1 = fieldpbjahit();

 $("#IDSJH").css('border', '');
 $("#Nomor_supplier").css('border', '');
 $("#NamaSupplier").css('border', '');

 if($("#IDSJH").val()==""){
  $('#alert').html('<div class="pesan sukses">No Makloon Jahit Tidak Boleh Kosong</div>');
  $("#IDSJH").css('border', '1px #C33 solid').focus();
}else if($("#Nomor_supplier").val()==""){
  $('#alert').html('<div class="pesan sukses">Nomor Supplier Tidak Boleh Kosong</div>');
  $("#Nomor_supplier").css('border', '1px #C33 solid').focus();
}else if($("#NamaSupplier").val()==""){
  $('#alert').html('<div class="pesan sukses">Data Supplier Tidak Boleh Kosong</div>');
  $("#NamaSupplier").css('border', '1px #C33 solid').focus();
}else{
 $.ajax({

  url: "simpan_penerimaan_barang_jahit",

  type: "POST",

  data: {

   "data1" : data1,
   "data2": PM

 },

 dataType: 'json',

 success: function (data) {
      // window.location.href = "index";
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/PenerimaanBarangJahit/printdata/'+data['IDTBSH']);

    },
    error: function(msg, status){
      console.log(msg);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
}
}

function saveChangepbjahit(){
  var data1 = fieldpbjahit2();
  console.log(PM);

  $.ajax({

    url: "../ubah_pb_jahit",

    type: "POST",

    data: {

     "data1" : data1,
     "data2": PM,
     "data3" : temp

   },

     // dataType: 'json',

     success: function (msg, status) {
       $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');

       document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
       document.getElementById("simpan").setAttribute("onclick", "return false;");
       document.getElementById("print").setAttribute("style", "cursor: pointer;");
       document.getElementById("print").setAttribute("onclick", "return true;");
       document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/PenerimaanBarangJahit/printdata/'+data1['IDTBSH']);

     },
     error: function(msg, status, data){
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });
}

function editList(i) {
  list_barang(PM[i].IDBarang);
  list_merk(PM[i].IDMerk);
  list_satuan(PM[i].IDSatuan);

  $('#vqty').val(PM[i].Qty);
  $('#vindex').val(i);
  $('#basic-modal-content').modal('show');
}

function list_barang(selected) {
  $.post('/'+base_url[1]+ "/PenerimaanBarangJahit/get_barang")
  .done(function(data){
            //console.log(data);
            res = $.parseJSON(data);
            $("#vbarang").html("<option value='' disabled>Pilih Barang</option>");
            for(i=0; i<res.length; i++){
              sel = "";
              if(res[i].IDBarang == selected)
                sel = "selected";
              $("#vbarang").html($("#vbarang").html()+"<option value='"+res[i].Nama_Barang+"' "+sel+">"+res[i].Nama_Barang+"</option>");
            }
          });
}
function list_merk(selected) {
  $.post('/'+base_url[1]+ "/PenerimaanBarangJahit/get_merk")
  .done(function(data){
            //console.log(data);
            res = $.parseJSON(data);
            $("#vmerk").html("<option value='' disabled>Pilih Merk</option>");
            for(i=0; i<res.length; i++){
              sel = "";
              if(res[i].IDMerk == selected)
                sel = "selected";
              $("#vmerk").html($("#vmerk").html()+"<option value='"+res[i].Merk+"' "+sel+">"+res[i].Merk+"</option>");
            }
          });
}

function list_satuan(selected) {
  $.post('/'+base_url[1]+ "/PenerimaanBarangJahit/get_satuan")
  .done(function(data){
    res = $.parseJSON(data);
    $("#vsatuan").html("<option value='' disabled>Pilih Satuan</option>");
    for(i=0; i<res.length; i++){
      sel = "";
      if(res[i].IDSatuan == selected)
        sel = "selected";
      $("#vsatuan").html($("#vsatuan").html()+"<option value='"+res[i].Satuan+"' "+sel+">"+res[i].Satuan+"</option>");
    }
  });
}

function save_tmp_create(i) {

  i = $('#vindex').val();
  console.log(i);
  PM[i].IDBarang = $('#vbarang').val();
  PM[i].IDMerk = $('#vmerk').val();
  PM[i].Qty = $('#vqty').val();
  PM[i].IDSatuan = $('#vsatuan').val();


  renderJSON(PM);
}

function save_tmp_edit(i) {

  i = $('#vindex').val();
  console.log(i);
  PM[i].IDBarang = $('#vbarang').val();
  PM[i].IDMerk = $('#vmerk').val();
  PM[i].Qty = $('#vqty').val();
  PM[i].IDSatuan = $('#vsatuan').val();


  renderJSONE(PM);
}

function cek_supplier()
{
  var Nomor = $("#IDSJH").val();
  $.ajax({
    url : '/'+base_url[1]+'/PenerimaanBarangJahit/cek_supplier',
    type: "POST",
    data:{Nomor:Nomor},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data kosong------------');
      } else {
       $("#NamaSupplier").val(data[0].Nama);
       $("#IDSupplier").val(data[0].IDSupplier);
     }
   },
   error: function (jqXHR, textStatus, errorThrown)
   {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
  }
});
}



function cek_barang()
{
  var Nomor = $("#IDBarang").val();
  $.ajax({
    url : '/'+base_url[1]+'/PenerimaanBarangJahit/cek_barang',
    type: "POST",
    data:{Nomor:Nomor},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data kosong------------');
      } else {
       $("#IDMerk").val(data[0].IDMerk);
       $("#IDSatuan").val(data[0].IDSatuan);
       $("#NamaMerk").val(data[0].Merk);
       $("#NamaSatuan").val(data[0].Satuan);
     }
   },
   error: function (jqXHR, textStatus, errorThrown)
   {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
  }
});
}

function cek_barang_edit()
{
  var Nomor = $("#Nama_Barang option:selected").attr('data-namabarang');
  $.ajax({
    url : '/'+base_url[1]+'/PenerimaanBarangJahit/cek_barang',
    type: "POST",
    data:{Nomor:Nomor},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data kosong------------');
      } else {
       $("#NamaMerk").val(data[0].IDMerk);
       $("#NamaSatuan").val(data[0].IDSatuan);
       $("#Merk").val(data[0].Merk);
       $("#Satuan").val(data[0].Satuan);
     }
   },
   error: function (jqXHR, textStatus, errorThrown)
   {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
  }
});
}

function cek_surat()
{
  var Nomor = $("#IDSJH").val();
  $.ajax({
    url : '/'+base_url[1]+'/PenerimaanBarangJahit/cek_surat',
    type: "POST",
    data:{Nomor:Nomor},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data kosong------------');
      } else {
       console.log(data);
	   $("#tabel-cart-surat-jahit").html("");
		  for(i = 0; i<data.length;i++){
			PM[i] = data[i];
			var value =
			"<tr>" +
			"<td>"+(i+1)+"</td>"+
			"<td>"+PM[i].Barcode+"</td>"+
			"<td>"+PM[i].Corak+"</td>"+
			"<td>"+PM[i].Warna+"</td>"+
			"<td>"+PM[i].Merk+"</td>"+
			"<td>"+PM[i].Qty_yard+"</td>"+
			"<td>"+PM[i].Qty_meter+"</td>"+
			"<td>"+PM[i].Grade+"</td>"+
			"<td>"+PM[i].Satuan+"</td>"+
			"</tr>";
			$("#tabel-cart-surat-jahit").append(value);
			
		  }
		   
     }
     
   },
   error: function (jqXHR, textStatus, errorThrown)
   {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
  }
}); 
}



