var base_url = window.location.pathname.split('/');

$(document).ready(function(){
  if(($("#Satuan").val() != "")){
    cek();
    //   if(($("#warna").val() != "")){
    //   get_warna($("#barang").val());
    
    // }
  }
  $("#Satuan").change(cek);

})

var PM = [];
var temp = [];


function cek() {
  var barang = $("#barang").val();
  var satuan = $("#Satuan").val();
    //console.log(base_url[1]+'/Sos/getmerk');
    $.ajax({
      url : '/'+base_url[1]+'/Sos/get_harga',
      type: "POST",
      data:{id_barang:barang, id_satuan:satuan},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data harga jual kosong------------');
          $('#alert').html('<div class="pesan sukses">Harga Jual Tidak Tersedia</div>');
          $('#Harga').val('');
        } else {
          console.log(data);
            //$("#merk").val(data["Merk"]);
            $("#harga-123").val(rupiah(data["Harga_Jual"]));
            $("#idmerk").val(data["IDMerk"]);
            $("#idcorak").val(data["IDCorak"]);
            
            
          }
          
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
        }
      });
  }

  function get_barang(){
    var groupbarang = $("#groupbarang").val();
    $.ajax({
      url : '/'+base_url[1]+'/Sos/getbarang',
      type: "POST",
      data:{IDGroupBarang:groupbarang},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        var html2 = '';
        if (data <= 0) {

          console.log('-----------data barang kosong------------');
          html2 += '<option value="">Data Barang Kosong</option>';
          $("#barang").html(html2);
        } else {
          //console.log(data);
          html += '<option value="">--Silahkan Pilih barang--</option>';
          for (var i = 0 ; i < data.length; i++) {
            html +='<option value="'+data[i]["IDBarang"]+'" data-namabarang="'+data[i]["Nama_Barang"]+'"> '+data[i]["Nama_Barang"]+' </option>';
          }
          $("#barang").html(html);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function initialization(){
    field = null;
    field = {
      "Corak" : $("#idcorak").val(),
      "IDMerk" : $("#idmerk").val(),
      "IDBarang" : $("#barang").val(),
      "Nama_Barang": $("#barang option:selected").attr('data-namabarang'),
      "Qty" : $("#Qty").val(),
      "Saldo" : $("#Qty").val(),
      "IDSatuan" : $("#Satuan").val(),
      "Satuan" : $("#Satuan option:selected").attr('data-namasatuan'),
      "Harga" : $("#harga-123").val(),
      "Sub_total" : $("#Qty").val() * $("#harga-123").val().replace(".",""),
      "Warna" : "-",
    }

    return field;
  }

  $("#tambah_sementara").click(function(){
    var field = initialization();
    PM.push(field);
    setTimeout(function(){
      renderJSON(PM);
      field = null;
    },500);

  });

  function renderJSON(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    var grandTotal = 0;
    var totaldpp=0;
    if(data != null){
      if($("#groupbarang").val()==""){
       $('#alert').html('<div class="pesan sukses">Group barang Tidak Boleh Kosong</div>');
       PM.pop();
     }else if($("#barang").val()==""){
      $('#alert').html('<div class="pesan sukses">Barang Tidak Boleh Kosong</div>');
      PM.pop();
    }else if($("#Qty").val()==""){
      $('#alert').html('<div class="pesan sukses">Qty Tidak Boleh Kosong</div>');
      PM.pop();
    }else if($("#Satuan").val()==""){
      $('#alert').html('<div class="pesan sukses">Satuan Tidak Boleh Kosong</div>');
      PM.pop();
    }else if($("#harga-123").val()==""){
      $('#alert').html('<div class="pesan sukses">Harga Tidak Boleh Kosong</div>');
      PM.pop();
    }else{
      PM = [];
      // $("#tabel-cart-asset").html("");
      var table = $('#tabelfsdfsf').DataTable();
      table.clear().draw();
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        // var value =
        // "<tr>" +
        // "<td>"+(i+1)+"</td>"+
        // "<td>"+PM[i].IDBarang+"</td>"+
        // "<td>"+PM[i].Qty+"</td>"+
        // "<td>"+PM[i].Satuan+"</td>"+
        // "<td>"+rupiah(PM[i].Harga)+"</td>"+
        // "<td>"+rupiah(PM[i].Sub_total)+"</td>"+
        // "<td>" +
        // "<div class='hidden-sm hidden-xs action-buttons'>"+
        // "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"+
        // "</div>" +
        // "</td>"+
        // "</tr>";
        // $("#tabel-cart-asset").append(value);
        table.row.add([
          i+1,
          PM[i].Nama_Barang,
          PM[i].Qty,
          PM[i].Satuan,
          rupiah(PM[i].Harga),
          rupiah(PM[i].Sub_total),
          "<a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"
          ]).draw().nodes().to$().addClass('rowarray'+i);
        totalQty += parseInt(PM[i].Qty);
        grandTotal += parseInt(PM[i].Sub_total);


        if($("#Status_ppn").val()=="Include"){
          totaldpp += parseInt(PM[i].Sub_total);
          dpp=  100/110*totaldpp;
          ppn= dpp*0.1;
          $("#totaldpp").val(totaldpp);
          $("#DPP").val(rupiah(Math.round(dpp)));
          $("#PPN").val(rupiah(Math.round(ppn)));
          total= totaldpp-ppn;
          $("#total_invoice").val(rupiah(total+ppn));
        }else{
          totaldpp += parseInt(PM[i].Sub_total);
          ppn= totaldpp*0.1;
           $("#totaldpp").val(totaldpp);
          $("#DPP").val(rupiah(Math.round(totaldpp)));
          $("#PPN").val(rupiah(Math.round(ppn)));
          $("#total_invoice").val(rupiah(totaldpp+ppn));
        }
      }
      $("#Total_qty").val(totalQty);
      $("#Grand_total").val(grandTotal);
      //console.log(totalQty);
      
      //-----Buat Field inputan sementara kosong lagi
      $('#Qty').val('');
      //$('#Satuan').val("");
      $('#harga-123').val('');
    }
  }
}

function ubah_status_ppn()
{
  if($("#Status_ppn").val()=="Include"){
    totaldpp = $("#totaldpp").val();
    dpp=  100/110*totaldpp;
    ppn= dpp*0.1;
    $("#DPP").val(rupiah(Math.round(dpp)));
    $("#PPN").val(rupiah(Math.round(ppn)));
    total= totaldpp-ppn;
    $("#total_invoice").val(rupiah(total+ppn));
  }else{
    totaldpp = $("#totaldpp").val();
    ppn= totaldpp*0.1;
    $("#DPP").val(rupiah(Math.round(totaldpp)));
    $("#PPN").val(rupiah(Math.round(ppn)));
    $("#total_invoice").val(rupiah(pasrseFloat(totaldpp)+parseFloat(ppn)));
  }
}

function renderJSONE(data){
    // console.log('-----------------')
    // console.log(data);
    var totalQty = 0;
    if(data != null){
      PM = [];
      $("#tabel-cart-asset").html("");
      for(i = 0; i<data.length;i++){
        PM[i] = data[i];
        var value =
        "<tr>" +
        "<td>"+(i+1)+"</td>"+
        "<td>"+PM[i].Nama_Barang+"</td>"+
        "<td>"+PM[i].Qty+"</td>"+
        "<td>"+PM[i].Satuan+"</td>"+
        "<td>"+PM[i].Harga+"</td>"+
        "<td>"+PM[i].Sub_total+"</td>"+
        "<td>" +
        "<div class='hidden-sm hidden-xs action-buttons'>"+
        "  <a href='#'  id='bootbox-confirm' title='Delete' onclick='deleteItem("+i+")'>Hapus </a>"+
        "</div>" +
        "</td>"+
        "</tr>";
        $("#tabel-cart-asset").append(value);
      }
      
    }
  }

  function deleteItem(i){

    if(PM[i].id !=''){
     temp.push(PM[i]);
   }

   $('.rowarray'+i).remove();
   
   PM.splice(i,1);
 }

 function fieldPo(){
  var data1 = {
    "Tanggal" :$("#Tanggal").val(),
    "Nomor" :$("#Nomor").val(),
    "IDCustomer" :$("#IDCustomer").val(),
    "TOP" :$("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo" :$("#Tanggal_jatuh_tempo").val(),
    "IDMataUang" :1,
    "Kurs" :1,
    "Total_qty" :$("#Total_qty").val(),
    "Saldo" :$("#Total_qty").val(),
    "No_po_customer" :$("#No_po_customer").val(),
    "Grand_total" :$("#Grand_total").val(),
    "Keterangan" :$("#Keterangan").val(),
    "Batal" : "aktif",
    "Status_ppn" :$("#Status_ppn").val(),
    "DPP" :$("#DPP").val(),
    "PPN" :$("#PPN").val(),
    
  }
  return data1;
}

function save(){
  var data1 = fieldPo();
  
  console.log('-----------Prosessimpan---------------')
    // console.log(PM)
    // console.log(data1)
    $("#IDCustomer").css('border', '');
    $("#No_po_customer").css('border', '');

    if($("#IDCustomer").val()==""){
      $('#alert').html('<div class="pesan sukses">Data Customer Tidak Boleh Kosong</div>');
      $("#IDCustomer").css('border', '1px #C33 solid').focus();
    }else if($("#No_po_customer").val()==""){
      $('#alert').html('<div class="pesan sukses">Data No PO Customer Tidak Boleh Kosong</div>');
      $("#No_po_customer").css('border', '1px #C33 solid').focus();
    }else{
      $.ajax({
        url : '/'+base_url[1]+'/Sos/simpan_sos',
        type: "POST",
        data:{_data1:data1, _data2:PM},
        dataType:'json',
        success: function(data)
        { 
          zeroValue;
          console.log('----------Ajax berhasil-----');
          console.log(data);
          $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
          window.location.href = "/"+base_url[1]+"/Sos/index";
        // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
        // document.getElementById("simpan").setAttribute("onclick", "return false;");
        // document.getElementById("print").setAttribute("style", "cursor: pointer;");
        // document.getElementById("print").setAttribute("onclick", "return true;");
        // document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/Sos/print_data_sos/'+data1['id_sos_tti']);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
       $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
     }
   });
    }
  }

  function save_change(){
    var data1 = fieldPo();
    var IDSOS = $("#IDSOS").val();
    console.log(data1);

    $.ajax({

      url : '/'+base_url[1]+'/Sos/ubah_sos',

      type: "POST",

      data: {

       "_data1" : data1,
       "_data2": PM,
       "_data3" : temp,
       "_id":IDSOS

     },

     // dataType: 'json',

     success: function (msg, status) {
      $('#alert').html('<div class="pesan sukses">Data berhasil Diubah</div>');
      window.location.href = "/"+base_url[1]+"/Sos/index";
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/Sos/print_data_sos/'+idPO);

    },
    error: function(msg, status, data){
     alert("Failure"+msg+status+data);
     $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

   }

 });
  }



  function zeroValue(){
    $("#Tanggal").val('');
    $("#Nomor").val('');
    $("#Tanggal_selesai").val('');
    $("#corak").val('');
    $("#merk").val('');
    $("#Lot").val('');
    $("#Total_qty").val('');
    $("#PIC").val('');
    $("#Keterangan").val('');

  }
// function deleteItem(){
//   console.log('deleted fuck');
// }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}