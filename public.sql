/*
 Navicat Premium Data Transfer

 Source Server         : erp
 Source Server Type    : PostgreSQL
 Source Server Version : 90517
 Source Host           : localhost:5432
 Source Catalog        : erp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90517
 File Encoding         : 65001

 Date: 12/06/2019 20:08:35
*/


-- ----------------------------
-- Sequence structure for tbl_agen_IDAgen_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_agen_IDAgen_seq";
CREATE SEQUENCE "public"."tbl_agen_IDAgen_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_asset_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_asset_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_asset_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_asset_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_asset_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_asset_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_bank_IDBank_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_bank_IDBank_seq";
CREATE SEQUENCE "public"."tbl_bank_IDBank_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_bank_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_bank_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_bank_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_barang_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_barang_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_barang_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_barang_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_barang_IDGroupBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDGroupBarang_seq";
CREATE SEQUENCE "public"."tbl_barang_IDGroupBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_barang_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_barang_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_barang_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_barang_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_barcode_print_IDBarcodePrint_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barcode_print_IDBarcodePrint_seq";
CREATE SEQUENCE "public"."tbl_barcode_print_IDBarcodePrint_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDBO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDBO_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDBO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_coa_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_coa_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDCOASaldoAwal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDGroupCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDGroupCOA_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDGroupCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_corak_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_corak_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_corak_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_corak_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_corak_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_corak_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_customer_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_customer_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_customer_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_customer_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_customer_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_customer_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_giro_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_giro_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_giro_IDGiro_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDGiro_seq";
CREATE SEQUENCE "public"."tbl_giro_IDGiro_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_giro_IDPerusahaan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDPerusahaan_seq";
CREATE SEQUENCE "public"."tbl_giro_IDPerusahaan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_asset_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_asset_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_group_asset_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_barang_IDGroupBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_barang_IDGroupBarang_seq";
CREATE SEQUENCE "public"."tbl_group_barang_IDGroupBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_coa_IDGroupCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_coa_IDGroupCOA_seq";
CREATE SEQUENCE "public"."tbl_group_coa_IDGroupCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_customer_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_customer_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_group_customer_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_supplier_IDGroupSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_supplier_IDGroupSupplier_seq";
CREATE SEQUENCE "public"."tbl_group_supplier_IDGroupSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_user_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_user_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_group_user_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_gudang_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_gudang_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_gudang_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDHargaJual_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDHargaJual_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDHargaJual_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_hutang_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_hutang_IDHutang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDHutang_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDHutang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_hutang_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_in_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_in_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDIn_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDIn_seq";
CREATE SEQUENCE "public"."tbl_in_IDIn_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_in_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_in_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_in_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDIP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDIP_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDIP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDPO_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDIPDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDIP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDIP_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDKartuStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDKartuStok_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDKartuStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDStok_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDKonversi_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDKonversi_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDKonversi_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDSatuanBerat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDSatuanBerat_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDSatuanBerat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDSatuanKecil_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDSatuanKecil_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDSatuanKecil_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDKPDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_IDKPScan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kota_IDKota_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kota_IDKota_seq";
CREATE SEQUENCE "public"."tbl_kota_IDKota_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDLapPers_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDLapPers_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDLapPers_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_mata_uang_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_mata_uang_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_mata_uang_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_IDMenu_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_IDMenu_seq";
CREATE SEQUENCE "public"."tbl_menu_IDMenu_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_detail_IDMenuDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_detail_IDMenuDetail_seq";
CREATE SEQUENCE "public"."tbl_menu_detail_IDMenuDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_detail_IDMenu_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_detail_IDMenu_seq";
CREATE SEQUENCE "public"."tbl_menu_detail_IDMenu_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDMenuDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDMenuDetail_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDMenuDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDMenuRole_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDMenuRole_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDMenuRole_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_merk_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_merk_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_merk_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_out_IDOut_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_out_IDOut_seq";
CREATE SEQUENCE "public"."tbl_out_IDOut_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDCOA_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDFBPembayaran_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDFBPembayaran_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDFBPembayaran_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_IDFBA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_IDFBA_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_IDFBA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDFBADetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDFBADetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBADetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDFBA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDFBA_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDFBDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDFBDetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDFBDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDTBSDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDTBSDetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDTBSDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_grand_total_IDFBGrandTotal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq";
CREATE SEQUENCE "public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_grand_total_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_grand_total_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_grand_total_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_piutang_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_piutang_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_piutang_IDPiutang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDPiutang_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDPiutang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDAgen_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDAgen_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDAgen_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDPO_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDPODetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDPODetail_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDPODetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDPO_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDFB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDRBDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDRBDetail_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDRBDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_saldo_awal_asset_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_saldo_awal_asset_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_saldo_awal_asset_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_saldo_awal_asset_IDSaldoAwalAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq";
CREATE SEQUENCE "public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_satuan_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_satuan_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_satuan_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDStok_seq";
CREATE SEQUENCE "public"."tbl_stok_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_AsalIDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_AsalIDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_history_AsalIDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_AsalIDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_AsalIDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_history_AsalIDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDHistoryStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDHistoryStok_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDHistoryStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDStok_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDStokOpname_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDStokOpname_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDStokOpname_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_suplier_IDGroupSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_suplier_IDGroupSupplier_seq";
CREATE SEQUENCE "public"."tbl_suplier_IDGroupSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_suplier_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_suplier_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_suplier_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDPO_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDSJM_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDSJM_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDSJM_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSJM_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_tampungan_IDT_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_tampungan_IDT_seq";
CREATE SEQUENCE "public"."tbl_tampungan_IDT_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDPO_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDTBSDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDTBSH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDUMCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDUMCustomer_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDUMCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_user_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_user_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_user_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_user_IDUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_user_IDUser_seq";
CREATE SEQUENCE "public"."tbl_user_IDUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_warna_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_warna_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_warna_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for tbl_agen
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_agen";
CREATE TABLE "public"."tbl_agen" (
  "IDAgen" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_agen_IDAgen_seq"'::regclass),
  "Kode_Perusahaan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Perusahaan" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Initial" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default",
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_Tlp" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Image" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(70) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_agen
-- ----------------------------
INSERT INTO "public"."tbl_agen" VALUES ('P000001', 'PMK123', 'PMK', 'PMKA', 'jl. cimahi', '8', '+62543353', '1535334789_1044348767.png', 'aktif', '2342342');

-- ----------------------------
-- Table structure for tbl_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_asset";
CREATE TABLE "public"."tbl_asset" (
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_asset_IDAsset_seq"'::regclass),
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_asset_IDGroupAsset_seq"'::regclass),
  "Kode_Asset" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Asset" varchar(200) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Coa_Asset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Coa_Akumulasi_Asset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Coa_Beban_Asset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_asset
-- ----------------------------
INSERT INTO "public"."tbl_asset" VALUES ('P100015', 'P000005', 'FUR007', 'Lemari Arsip Hp I St 270', 'aktif', 'P000227', 'P000052', 'P000441');
INSERT INTO "public"."tbl_asset" VALUES ('P100021', 'P000005', 'FUR013', 'Pintu Lemari Hp I St 701', 'aktif', 'P000233', 'P000096', 'P000489');
INSERT INTO "public"."tbl_asset" VALUES ('P100035', 'P000005', 'FUR027', 'Lemari Arsip Hp 1 + Kk L. Arsip', 'aktif', 'P000247', 'P000146', 'P000454');
INSERT INTO "public"."tbl_asset" VALUES ('P100050', 'P000005', 'FUR042', 'Meja Kantor', 'aktif', 'P000262', 'P000047', 'P000511');
INSERT INTO "public"."tbl_asset" VALUES ('P100060', 'P000005', 'FUR052', 'Software Accurate', 'aktif', 'P000272', 'P000165', 'P000446');
INSERT INTO "public"."tbl_asset" VALUES ('P100071', 'P000005', 'FUR063', 'Meja Kantor (Hp I -300)', 'aktif', 'P000283', 'P000186', 'P000525');
INSERT INTO "public"."tbl_asset" VALUES ('P100084', 'P000005', 'FUR076', 'Besi Siku Lobang 4X4X4Mtr', 'aktif', 'P000296', 'P000158', 'P000493');
INSERT INTO "public"."tbl_asset" VALUES ('P100000', 'P000004', 'BGN001', 'Ruko ITC Baranangsiang Blok F (1)', 'aktif', 'P000212', 'P000034', 'P000412');
INSERT INTO "public"."tbl_asset" VALUES ('P100001', 'P000004', 'BGN002', 'Ruko ITC Baranangsiang Blok F (2)', 'aktif', 'P000213', 'P000035', 'P000413');
INSERT INTO "public"."tbl_asset" VALUES ('P100002', 'P000004', 'BGN003', 'Ruko ITC Baranangsiang Blok F (3)', 'aktif', 'P000214', 'P000036', 'P000414');
INSERT INTO "public"."tbl_asset" VALUES ('P100003', 'P000004', 'BGN004', 'Apartemen Bekasi Ex Kim', 'aktif', 'P000215', 'P000037', 'P000415');
INSERT INTO "public"."tbl_asset" VALUES ('P100004', 'P000007', 'MSN001', 'Mesin Electronic Selvage Letter Jack (1)', 'aktif', 'P000216', 'P000038', 'P000416');
INSERT INTO "public"."tbl_asset" VALUES ('P100005', 'P000007', 'MSN002', 'Mesin Electronic Selvage Letter Jack (2)', 'aktif', 'P000217', 'P000039', 'P000417');
INSERT INTO "public"."tbl_asset" VALUES ('P100006', 'P000003', 'KND001', 'Motor Vario', 'aktif', 'P000218', 'P000040', 'P000418');
INSERT INTO "public"."tbl_asset" VALUES ('P100007', 'P000003', 'KND002', 'Kijang Inova Diesel 2014', 'aktif', 'P000219', 'P000041', 'P000419');
INSERT INTO "public"."tbl_asset" VALUES ('P100008', 'P000003', 'KND003', 'Kijang Inova Diesel 2018', 'aktif', 'P000220', 'P000042', 'P000420');
INSERT INTO "public"."tbl_asset" VALUES ('P100009', 'P000005', 'FUR001', 'AC Aolia', 'aktif', 'P000221', 'P000073', 'P000443');
INSERT INTO "public"."tbl_asset" VALUES ('P100010', 'P000005', 'FUR002', 'Bor Maktec & Dudukan', 'aktif', 'P000222', 'P000065', 'P000421');
INSERT INTO "public"."tbl_asset" VALUES ('P100011', 'P000005', 'FUR003', 'Istana Alumunium', 'aktif', 'P000223', 'P000043', 'P000438');
INSERT INTO "public"."tbl_asset" VALUES ('P100012', 'P000005', 'FUR004', 'Kaca Film', 'aktif', 'P000224', 'P000060', 'P000577');
INSERT INTO "public"."tbl_asset" VALUES ('P100013', 'P000005', 'FUR005', 'Kursi Manager Hp 62', 'aktif', 'P000225', 'P000200', 'P000502');
INSERT INTO "public"."tbl_asset" VALUES ('P100014', 'P000005', 'FUR006', 'Laci Sorong Hp I Mb 135', 'aktif', 'P000226', 'P000125', 'P000430');
INSERT INTO "public"."tbl_asset" VALUES ('P100030', 'P000005', 'FUR022', 'Rak Buku 3 Susun BK 504 (2)', 'aktif', 'P000242', 'P000119', 'P000495');
INSERT INTO "public"."tbl_asset" VALUES ('P100076', 'P000005', 'FUR068', 'Mesin Ketokan', 'aktif', 'P000288', 'P000080', 'P000558');
INSERT INTO "public"."tbl_asset" VALUES ('P100017', 'P000005', 'FUR009', 'Meja Kantor Hp I Od 032', 'aktif', 'P000229', 'P000075', 'P000555');
INSERT INTO "public"."tbl_asset" VALUES ('P100018', 'P000005', 'FUR010', 'Meja Kantor Hp I Od 300', 'aktif', 'P000230', 'P000178', 'P000487');
INSERT INTO "public"."tbl_asset" VALUES ('P100019', 'P000005', 'FUR011', 'Meja Kantor Hp V Hod 5055', 'aktif', 'P000231', 'P000110', 'P000468');
INSERT INTO "public"."tbl_asset" VALUES ('P100020', 'P000005', 'FUR012', 'Mesin Potong', 'aktif', 'P000232', 'P000091', 'P000473');
INSERT INTO "public"."tbl_asset" VALUES ('P100022', 'P000005', 'FUR014', 'Rak Buku 3 Susun BK 503 (1)', 'aktif', 'P000234', 'P000112', 'P000474');
INSERT INTO "public"."tbl_asset" VALUES ('P100023', 'P000005', 'FUR015', 'Rak Buku 3 Susun BK 504 (1)', 'aktif', 'P000235', 'P000097', 'P000522');
INSERT INTO "public"."tbl_asset" VALUES ('P100024', 'P000005', 'FUR016', 'Telepon Pabx (1)', 'aktif', 'P000236', 'P000145', 'P000464');
INSERT INTO "public"."tbl_asset" VALUES ('P100025', 'P000005', 'FUR017', 'Teralis', 'aktif', 'P000237', 'P000087', 'P000552');
INSERT INTO "public"."tbl_asset" VALUES ('P100026', 'P000005', 'FUR018', 'Lemari Arsip ST 230A HP 1', 'aktif', 'P000238', 'P000175', 'P000488');
INSERT INTO "public"."tbl_asset" VALUES ('P100027', 'P000005', 'FUR019', 'Pintu Lemari St 701A Hp 1', 'aktif', 'P000239', 'P000111', 'P000494');
INSERT INTO "public"."tbl_asset" VALUES ('P100028', 'P000005', 'FUR020', 'Rak Buku 3 Susun BK 503 (2)', 'aktif', 'P000240', 'P000117', 'P000497');
INSERT INTO "public"."tbl_asset" VALUES ('P100029', 'P000005', 'FUR021', 'Rak Buku 3 Susun BK 503 (3)', 'aktif', 'P000241', 'P000120', 'P000496');
INSERT INTO "public"."tbl_asset" VALUES ('P100031', 'P000005', 'FUR023', 'Speaker Simbada Cst Z100', 'aktif', 'P000243', 'P000118', 'P000465');
INSERT INTO "public"."tbl_asset" VALUES ('P100032', 'P000005', 'FUR024', 'Meja Komputer (1)', 'aktif', 'P000244', 'P000088', 'P000553');
INSERT INTO "public"."tbl_asset" VALUES ('P100033', 'P000005', 'FUR025', 'Meja Komputer (2)', 'aktif', 'P000245', 'P000176', 'P000475');
INSERT INTO "public"."tbl_asset" VALUES ('P100034', 'P000005', 'FUR026', 'Rak Besi Untuk Kain', 'aktif', 'P000246', 'P000098', 'P000523');
INSERT INTO "public"."tbl_asset" VALUES ('P100036', 'P000005', 'FUR028', 'Meja Kantor Hp 1', 'aktif', 'P000248', 'P000077', 'P000554');
INSERT INTO "public"."tbl_asset" VALUES ('P100037', 'P000005', 'FUR029', 'Pintu Lemari Hp 1', 'aktif', 'P000249', 'P000177', 'P000556');
INSERT INTO "public"."tbl_asset" VALUES ('P100038', 'P000005', 'FUR030', 'Air Conditioner', 'aktif', 'P000250', 'P000179', 'P000568');
INSERT INTO "public"."tbl_asset" VALUES ('P100039', 'P000005', 'FUR031', 'Partisi Kayu', 'aktif', 'P000251', 'P000191', 'P000569');
INSERT INTO "public"."tbl_asset" VALUES ('P100040', 'P000005', 'FUR032', 'Rak Dan Tralis Ruko E-9', 'aktif', 'P000252', 'P000192', 'P000544');
INSERT INTO "public"."tbl_asset" VALUES ('P100041', 'P000005', 'FUR033', 'Mesin Potong', 'aktif', 'P000253', 'P000167', 'P000490');
INSERT INTO "public"."tbl_asset" VALUES ('P100042', 'P000005', 'FUR034', 'Telepon Pabx (2)', 'aktif', 'P000254', 'P000113', 'P000491');
INSERT INTO "public"."tbl_asset" VALUES ('P100043', 'P000005', 'FUR035', 'Rak Besi + Multiplek', 'aktif', 'P000255', 'P000114', 'P000545');
INSERT INTO "public"."tbl_asset" VALUES ('P100044', 'P000005', 'FUR036', 'Kursi Administrasi', 'aktif', 'P000256', 'P000168', 'P000546');
INSERT INTO "public"."tbl_asset" VALUES ('P100045', 'P000005', 'FUR037', 'Sekat Alumunium', 'aktif', 'P000257', 'P000169', 'P000550');
INSERT INTO "public"."tbl_asset" VALUES ('P100046', 'P000005', 'FUR038', 'Handtruck Trolley', 'aktif', 'P000258', 'P000173', 'P000486');
INSERT INTO "public"."tbl_asset" VALUES ('P100047', 'P000005', 'FUR039', 'AC Panasonic', 'aktif', 'P000259', 'P000109', 'P000472');
INSERT INTO "public"."tbl_asset" VALUES ('P100048', 'P000005', 'FUR040', 'Joint Table', 'aktif', 'P000260', 'P000095', 'P000521');
INSERT INTO "public"."tbl_asset" VALUES ('P100049', 'P000005', 'FUR041', 'Laci Sorong ', 'aktif', 'P000261', 'P000144', 'P000425');
INSERT INTO "public"."tbl_asset" VALUES ('P100051', 'P000005', 'FUR043', 'Meja Komputer (3)', 'aktif', 'P000263', 'P000134', 'P000515');
INSERT INTO "public"."tbl_asset" VALUES ('P100052', 'P000005', 'FUR044', 'Rak Buku Expo 3183', 'aktif', 'P000264', 'P000138', 'P000447');
INSERT INTO "public"."tbl_asset" VALUES ('P100053', 'P000005', 'FUR045', 'Tv Lg 21 Fl 4Rg', 'aktif', 'P000265', 'P000069', 'P000559');
INSERT INTO "public"."tbl_asset" VALUES ('P100054', 'P000005', 'FUR046', 'Hekter Max 12 L', 'aktif', 'P000266', 'P000182', 'P000503');
INSERT INTO "public"."tbl_asset" VALUES ('P100055', 'P000005', 'FUR047', 'Pisau Potong Polytex 50Cm-5Mm', 'aktif', 'P000267', 'P000126', 'P000543');
INSERT INTO "public"."tbl_asset" VALUES ('P100056', 'P000005', 'FUR048', 'Rengh Kertas Primo', 'aktif', 'P000268', 'P000166', 'P000433');
INSERT INTO "public"."tbl_asset" VALUES ('P100057', 'P000005', 'FUR049', 'Koper Sampel Dan Pakaian', 'aktif', 'P000269', 'P000055', 'P000564');
INSERT INTO "public"."tbl_asset" VALUES ('P100058', 'P000005', 'FUR050', 'Tangga Alumunium', 'aktif', 'P000270', 'P000187', 'P000460');
INSERT INTO "public"."tbl_asset" VALUES ('P100059', 'P000005', 'FUR051', 'Rak Besi', 'aktif', 'P000271', 'P000083', 'P000542');
INSERT INTO "public"."tbl_asset" VALUES ('P100061', 'P000005', 'FUR053', 'Komputer LCD E-9', 'aktif', 'P000273', 'P000068', 'P000557');
INSERT INTO "public"."tbl_asset" VALUES ('P100062', 'P000005', 'FUR054', 'Rak Dan Lemari', 'aktif', 'P000274', 'P000180', 'P000466');
INSERT INTO "public"."tbl_asset" VALUES ('P100063', 'P000005', 'FUR055', 'Mesin Potong & Pisau', 'aktif', 'P000275', 'P000089', 'P000579');
INSERT INTO "public"."tbl_asset" VALUES ('P100064', 'P000005', 'FUR056', 'Printer Xerox Phaser 3124', 'aktif', 'P000276', 'P000202', 'P000422');
INSERT INTO "public"."tbl_asset" VALUES ('P100065', 'P000005', 'FUR057', 'Komputer Server', 'aktif', 'P000277', 'P000044', 'P000483');
INSERT INTO "public"."tbl_asset" VALUES ('P100066', 'P000005', 'FUR058', 'Filter Pompa Air', 'aktif', 'P000278', 'P000106', 'P000451');
INSERT INTO "public"."tbl_asset" VALUES ('P100067', 'P000005', 'FUR059', 'Rak Untuk M20', 'aktif', 'P000279', 'P000074', 'P000492');
INSERT INTO "public"."tbl_asset" VALUES ('P100068', 'P000005', 'FUR060', 'Tangki Air (1)', 'aktif', 'P000280', 'P000115', 'P000467');
INSERT INTO "public"."tbl_asset" VALUES ('P100069', 'P000005', 'FUR061', 'Meja Komputer + Rak', 'aktif', 'P000281', 'P000090', 'P000449');
INSERT INTO "public"."tbl_asset" VALUES ('P100070', 'P000005', 'FUR062', 'Rak Arsip', 'aktif', 'P000282', 'P000071', 'P000563');
INSERT INTO "public"."tbl_asset" VALUES ('P100072', 'P000005', 'FUR064', 'Meja Kantor (Hp I -302)', 'aktif', 'P000284', 'P000148', 'P000432');
INSERT INTO "public"."tbl_asset" VALUES ('P100073', 'P000005', 'FUR065', 'Tangki Air (2)', 'aktif', 'P000285', 'P000054', 'P000571');
INSERT INTO "public"."tbl_asset" VALUES ('P100074', 'P000005', 'FUR066', 'Monitor Lcd Lg 15,6"', 'aktif', 'P000286', 'P000194', 'P000459');
INSERT INTO "public"."tbl_asset" VALUES ('P100075', 'P000005', 'FUR067', 'Lemari Besi', 'aktif', 'P000287', 'P000082', 'P000457');
INSERT INTO "public"."tbl_asset" VALUES ('P100077', 'P000005', 'FUR069', 'Hard Disk External Toshiba 500 Gb', 'aktif', 'P000289', 'P000181', 'P000504');
INSERT INTO "public"."tbl_asset" VALUES ('P100078', 'P000005', 'FUR070', 'Lemari Rak Arsip', 'aktif', 'P000290', 'P000127', 'P000537');
INSERT INTO "public"."tbl_asset" VALUES ('P100079', 'P000005', 'FUR071', 'Lemari Arsip', 'aktif', 'P000291', 'P000160', 'P000458');
INSERT INTO "public"."tbl_asset" VALUES ('P100080', 'P000005', 'FUR072', 'Alat Potong Kain Gramasi + Klem', 'aktif', 'P000292', 'P000081', 'P000444');
INSERT INTO "public"."tbl_asset" VALUES ('P100081', 'P000005', 'FUR073', 'Mesin Cutter Tsc', 'aktif', 'P000293', 'P000066', 'P000572');
INSERT INTO "public"."tbl_asset" VALUES ('P100082', 'P000005', 'FUR074', 'Pemadam Kebakaran', 'aktif', 'P000294', 'P000195', 'P000562');
INSERT INTO "public"."tbl_asset" VALUES ('P100083', 'P000005', 'FUR075', 'AC Sharp Plasma', 'aktif', 'P000295', 'P000185', 'P000535');
INSERT INTO "public"."tbl_asset" VALUES ('P100085', 'P000005', 'FUR077', 'Conference Table Ct 3B Highpoint', 'aktif', 'P000297', 'P000116', 'P000540');
INSERT INTO "public"."tbl_asset" VALUES ('P100086', 'P000005', 'FUR078', 'Lemari Arsip VCL 492', 'aktif', 'P000298', 'P000163', 'P000484');
INSERT INTO "public"."tbl_asset" VALUES ('P100087', 'P000005', 'FUR079', 'Locker Ace Hardware', 'aktif', 'P000299', 'P000107', 'P000485');
INSERT INTO "public"."tbl_asset" VALUES ('P100088', 'P000005', 'FUR080', 'Monitor Lcd Acer 16"', 'aktif', 'P000300', 'P000108', 'P000514');
INSERT INTO "public"."tbl_asset" VALUES ('P100089', 'P000005', 'FUR081', 'Office Desk Hod 5052 Highpoint', 'aktif', 'P000301', 'P000137', 'P000573');
INSERT INTO "public"."tbl_asset" VALUES ('P100090', 'P000005', 'FUR082', 'Rak Alumunium', 'aktif', 'P000302', 'P000196', 'P000435');
INSERT INTO "public"."tbl_asset" VALUES ('P100091', 'P000005', 'FUR083', 'Rak Hanger', 'aktif', 'P000303', 'P000057', 'P000506');
INSERT INTO "public"."tbl_asset" VALUES ('P100092', 'P000005', 'FUR084', 'Rak Kain Baru', 'aktif', 'P000304', 'P000129', 'P000501');
INSERT INTO "public"."tbl_asset" VALUES ('P100016', 'P000005', 'FUR008', 'Listrik', 'aktif', 'P000228', 'P000063', 'P000452');
INSERT INTO "public"."tbl_asset" VALUES ('P100125', 'P000005', 'FUR117', 'Printer Canon Lbp 6030', 'aktif', 'P000337', 'P000174', 'P000584');
INSERT INTO "public"."tbl_asset" VALUES ('P100126', 'P000005', 'FUR118', 'Printer & Scanner', 'aktif', 'P000338', 'P000207', 'P000583');
INSERT INTO "public"."tbl_asset" VALUES ('P100127', 'P000005', 'FUR119', 'Laptop', 'aktif', 'P000339', 'P000206', 'P000509');
INSERT INTO "public"."tbl_asset" VALUES ('P100128', 'P000004', 'BDP001', 'Bangunan Dalam Proses', 'aktif', 'P000388', NULL, NULL);
INSERT INTO "public"."tbl_asset" VALUES ('P100093', 'P000005', 'FUR085', 'Tralis Dan Canopy', 'aktif', 'P000305', 'P000124', 'P000477');
INSERT INTO "public"."tbl_asset" VALUES ('P100094', 'P000005', 'FUR086', 'Lemari Arsip 2 Set', 'aktif', 'P000306', 'P000100', 'P000448');
INSERT INTO "public"."tbl_asset" VALUES ('P100095', 'P000005', 'FUR087', 'Od +Drawers Central Lock Hp', 'aktif', 'P000307', 'P000070', 'P000440');
INSERT INTO "public"."tbl_asset" VALUES ('P100096', 'P000005', 'FUR088', 'Big Wheel F/Hand Trolley + Puller', 'aktif', 'P000308', 'P000062', 'P000479');
INSERT INTO "public"."tbl_asset" VALUES ('P100097', 'P000005', 'FUR089', 'Table Fan Industrial', 'aktif', 'P000309', 'P000102', 'P000527');
INSERT INTO "public"."tbl_asset" VALUES ('P100098', 'P000005', 'FUR090', 'Kursi Chitose', 'aktif', 'P000310', 'P000150', 'P000470');
INSERT INTO "public"."tbl_asset" VALUES ('P100099', 'P000005', 'FUR091', 'Rak Besi Untuk Kain', 'aktif', 'P000311', 'P000093', 'P000498');
INSERT INTO "public"."tbl_asset" VALUES ('P100100', 'P000005', 'FUR092', 'Furniture', 'aktif', 'P000312', 'P000121', 'P000427');
INSERT INTO "public"."tbl_asset" VALUES ('P100101', 'P000005', 'FUR093', 'Pengaman Lift', 'aktif', 'P000313', 'P000049', 'P000439');
INSERT INTO "public"."tbl_asset" VALUES ('P100102', 'P000005', 'FUR094', 'Pompa Shimizu U/ E9', 'aktif', 'P000314', 'P000061', 'P000516');
INSERT INTO "public"."tbl_asset" VALUES ('P100103', 'P000005', 'FUR095', 'Printer Hp Laserjet M1132', 'aktif', 'P000315', 'P000139', 'P000533');
INSERT INTO "public"."tbl_asset" VALUES ('P100104', 'P000005', 'FUR096', 'Komputer Admin', 'aktif', 'P000316', 'P000156', 'P000561');
INSERT INTO "public"."tbl_asset" VALUES ('P100105', 'P000005', 'FUR097', 'Pisau Potong', 'aktif', 'P000317', 'P000184', 'P000476');
INSERT INTO "public"."tbl_asset" VALUES ('P100106', 'P000005', 'FUR098', 'Fax Panasonic', 'aktif', 'P000318', 'P000099', 'P000428');
INSERT INTO "public"."tbl_asset" VALUES ('P100107', 'P000005', 'FUR099', 'Lemari Besi Admin', 'aktif', 'P000319', 'P000050', 'P000560');
INSERT INTO "public"."tbl_asset" VALUES ('P100108', 'P000005', 'FUR100', 'Notebook Lenovo', 'aktif', 'P000320', 'P000183', 'P000434');
INSERT INTO "public"."tbl_asset" VALUES ('P100109', 'P000005', 'FUR101', 'Kursi Kantor (1)', 'aktif', 'P000321', 'P000056', 'P000513');
INSERT INTO "public"."tbl_asset" VALUES ('P100110', 'P000005', 'FUR102', 'Rak Besi Baru', 'aktif', 'P000322', 'P000136', 'P000505');
INSERT INTO "public"."tbl_asset" VALUES ('P100111', 'P000005', 'FUR103', 'Rak Admin', 'aktif', 'P000323', 'P000128', 'P000453');
INSERT INTO "public"."tbl_asset" VALUES ('P100112', 'P000005', 'FUR104', 'Pompa Air', 'aktif', 'P000324', 'P000076', 'P000423');
INSERT INTO "public"."tbl_asset" VALUES ('P100113', 'P000005', 'FUR105', 'Rak Besi F11', 'aktif', 'P000325', 'P000045', 'P000539');
INSERT INTO "public"."tbl_asset" VALUES ('P100114', 'P000005', 'FUR106', 'CPU Baru', 'aktif', 'P000326', 'P000162', 'P000578');
INSERT INTO "public"."tbl_asset" VALUES ('P100115', 'P000005', 'FUR107', 'Acer Notebook', 'aktif', 'P000327', 'P000201', 'P000481');
INSERT INTO "public"."tbl_asset" VALUES ('P100116', 'P000005', 'FUR108', 'Peralatan CCTV (3)', 'aktif', 'P000328', 'P000104', 'P000471');
INSERT INTO "public"."tbl_asset" VALUES ('P100117', 'P000005', 'FUR109', 'Rak Besi 2015', 'aktif', 'P000329', 'P000094', 'P000512');
INSERT INTO "public"."tbl_asset" VALUES ('P100118', 'P000005', 'FUR110', 'UPS (2)', 'aktif', 'P000330', 'P000135', 'P000570');
INSERT INTO "public"."tbl_asset" VALUES ('P100119', 'P000005', 'FUR111', 'Komputer + Printer', 'aktif', 'P000331', 'P000193', 'P000429');
INSERT INTO "public"."tbl_asset" VALUES ('P100120', 'P000005', 'FUR112', 'Meja + Kursi', 'aktif', 'P000332', 'P000051', 'P000461');
INSERT INTO "public"."tbl_asset" VALUES ('P100121', 'P000005', 'FUR113', 'CPU Unit', 'aktif', 'P000333', 'P000084', 'P000507');
INSERT INTO "public"."tbl_asset" VALUES ('P100122', 'P000005', 'FUR114', 'Kursi Kantor (2)', 'aktif', 'P000334', 'P000130', 'P000508');
INSERT INTO "public"."tbl_asset" VALUES ('P100123', 'P000005', 'FUR115', 'Rak Arsip Baru', 'aktif', 'P000335', 'P000131', 'P000529');
INSERT INTO "public"."tbl_asset" VALUES ('P100124', 'P000005', 'FUR116', 'Telepon Panasonic', 'aktif', 'P000336', 'P000152', 'P000551');

-- ----------------------------
-- Table structure for tbl_bank
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_bank";
CREATE TABLE "public"."tbl_bank" (
  "IDBank" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_bank_IDBank_seq"'::regclass),
  "Nomor_Rekening" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_bank_IDCoa_seq"'::regclass),
  "Atas_Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_bank
-- ----------------------------
INSERT INTO "public"."tbl_bank" VALUES ('P000003', '1563087777', 'P000004', 'PT Prima Moda Kreasindo', 'aktif');
INSERT INTO "public"."tbl_bank" VALUES ('P000004', '1561554477', 'P000005', 'Suyanto Tanumihardja', 'aktif');
INSERT INTO "public"."tbl_bank" VALUES ('P000005', '800150490700', 'P000623', 'PT Prima Moda Kreasindo', 'aktif');
INSERT INTO "public"."tbl_bank" VALUES ('P000006', '00000100772', 'P000006', 'PT Prima Moda Kreasindo', 'aktif');

-- ----------------------------
-- Table structure for tbl_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_barang";
CREATE TABLE "public"."tbl_barang" (
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDBarang_seq"'::regclass),
  "IDGroupBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDGroupBarang_seq"'::regclass),
  "Kode_Barang" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Barang" varchar(200) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDMerk_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDSatuan_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDCorak_seq"'::regclass),
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Warna_Cust" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_barang
-- ----------------------------
INSERT INTO "public"."tbl_barang" VALUES ('P000002', 'Pb00002', 'Topi', 'Topi', 'Prk0026', 'P000003', 'P000028', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P000003', 'Pb00005', 'FUR', 'Furniture', 'Prk0026', 'P000003', 'P000028', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P000006', 'GB00001', 'K015', 'Libero', 'Prk0015', 'P000004', 'P000015', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000a', 'GB00001', 'K002', '03131', 'Prk0001', 'P000004', 'P000001', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000b', 'GB00001', 'K004', '03422', 'Prk0007', 'P000004', 'P000007', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000c', 'GB00001', 'K008', '03643', 'Prk0009', 'P000004', 'P000009', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000d', 'GB00001', 'K009', '11431', 'Prk0023', 'P000004', 'P000023', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000e', 'GB00001', 'K010', '21453', 'Prk0014', 'P000004', 'P000014', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000f', 'GB00001', 'K011', '21455', 'Prk0016', 'P000004', 'P000016', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000g', 'GB00001', 'K012', '21472', 'Prk0013', 'P000004', 'P000013', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000h', 'GB00001', 'K013', '21526', 'Prk0017', 'P000004', 'P000017', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000i', 'GB00001', 'K016', '61288', 'Prk0024', 'P000004', 'P000024', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000j', 'GB00001', 'K017', '68345', 'Prk0022', 'P000004', 'P000022', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000k', 'GB00001', 'K018', '70604', 'Prk0019', 'P000004', 'P000019', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000l', 'GB00001', 'K019', '77308', 'Prk0018', 'P000004', 'P000018', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000m', 'GB00001', 'K020', '88210', 'Prk0020', 'P000004', 'P000020', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000p', 'GB00001', 'K024', '93067', 'Prk0025', 'P000004', 'P000025', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000q', 'GB00001', 'K001', '03128', 'Prk0003', 'P000004', 'P000003', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000s', 'GB00001', 'K003', '03202', 'Prk0010', 'P000004', 'P000010', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P000004', 'GB00001', 'K007', '03430', 'Prk0005', 'P000004', 'P000005', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P000005', 'GB00001', 'K025', '93609', 'Prk0011', 'P000004', 'P000011', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000n', 'GB00001', 'K022', '93055', 'Prk0004', 'P000004', 'P000004', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000o', 'GB00001', 'K006', '03425', 'Prk0008', 'P000004', 'P000008', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000p', 'GB00001', 'K021', '88280', 'Prk0021', 'P000004', 'P000021', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000q', 'GB00001', 'K026', '26443', 'Prk0012', 'P000004', 'P000003', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P000001', 'GB00002', 'Kemeja', 'Kemeja', 'Prk0026', 'P000003', 'P000028', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P000007', 'GB00001', 'K014', '26297', 'Prk0012', 'P000004', 'P000012', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000t', 'GB00001', 'K005', '03424', 'Prk0006', 'P000004', 'P000026', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P00000u', 'GB00001', 'K023', '93063', 'Prk0002', 'P000004', 'P000027', 'aktif', NULL);

-- ----------------------------
-- Table structure for tbl_barcode_print
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_barcode_print";
CREATE TABLE "public"."tbl_barcode_print" (
  "Barcode" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "CustDes" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "WarnaCust" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Panjang_Yard" float8,
  "Panjang_Meter" float8,
  "Grade" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarcodePrint" int8 NOT NULL DEFAULT nextval('"tbl_barcode_print_IDBarcodePrint_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_booking_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_booking_order";
CREATE TABLE "public"."tbl_booking_order" (
  "IDBO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_booking_order_IDBO_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_selesai" date,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_booking_order_IDCorak_seq"'::regclass),
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_booking_order_IDMataUang_seq"'::regclass),
  "Kurs" varchar COLLATE "pg_catalog"."default",
  "Qty" float8,
  "Saldo" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_booking_order
-- ----------------------------
INSERT INTO "public"."tbl_booking_order" VALUES ('P000001', '2019-03-18', 'BO19PMKA00001', '2019-03-29', 'P000004', 0, NULL, 230, 23, 'tes', 'Tidak Aktif');

-- ----------------------------
-- Table structure for tbl_buku_bank
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_buku_bank";
CREATE TABLE "public"."tbl_buku_bank" (
  "IDBukuBank" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_buku_bank
-- ----------------------------
INSERT INTO "public"."tbl_buku_bank" VALUES ('P000002', '2019-03-06', 'BB19PMKA00002', 'P000004', 'Kredit', '', 'aktif');
INSERT INTO "public"."tbl_buku_bank" VALUES ('P000004', '2019-03-06', 'BB19PMKA00004', 'P000004', 'Kredit', '', 'aktif');
INSERT INTO "public"."tbl_buku_bank" VALUES ('P000005', '2019-04-02', 'BB19PMKA00005', 'P000004', 'Kredit', '', 'aktif');
INSERT INTO "public"."tbl_buku_bank" VALUES ('P000003', '2019-03-05', 'BB19PMKA00003', 'P000004', 'Debet', '', 'tidak aktif');
INSERT INTO "public"."tbl_buku_bank" VALUES ('P000001', '2019-03-01', 'BB19PMKA00001', 'P000004', 'Kredit', NULL, 'aktif');

-- ----------------------------
-- Table structure for tbl_buku_bank_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_buku_bank_detail";
CREATE TABLE "public"."tbl_buku_bank_detail" (
  "IDBukuBankDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBukuBank" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8,
  "Kredit" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_buku_bank_detail
-- ----------------------------
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P000003', 'P000002', 'P000014', 'Debet', 115008500, 0, 1, 14000, 'PPH 21  RISNADI T KRISNADI TALAR  
');
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P000004', 'P000003', 'P000014', 'Debet', 115008500, 0, 1, 14000, 'PPH 21  RISNADI T KRISNADI TALAR  
');
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P000005', 'P000004', 'P000634', 'Kredit', 0, 31328690, 1, 14000, 'KAS NEGARA / PPH 25 MASA FEB''19  PRIMA MODA
');
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P000006', 'P000004', 'P000632', 'Kredit', 0, 28984004, 1, 14000, 'KAS NEGARA / PPH 21 MASA FEB''19  PRIMA MODA (GAJI KARY & KOMISI )
');
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P000007', 'P000004', 'P000633', 'Kredit', 0, 509046, 1, 14000, 'KAS NEGARA / PPH 23 MASA FEB''19  PRIMA MODA

');
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P000008', 'P000005', 'P000622', 'Kredit', 0, 6195000, 1, 14000, 'PT. MANDIRI TUNAS FINANCE / BCA : EH 623146 / CICILAN KE 2
');
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P000009', 'P000005', 'P000405', 'Kredit', 0, 6195000, 1, 14000, 'PT. MANDIRI TUNAS FINANCE / BCA : EH 623146 / CICILAN KE 2
');
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P00000a', 'P000005', 'P000405', 'Kredit', 0, 6195000, 1, 14000, 'PT. MANDIRI TUNAS FINANCE / BCA : EH 623146 / CICILAN KE 2
');
INSERT INTO "public"."tbl_buku_bank_detail" VALUES ('P000002', 'P000002', 'P000598', 'Kredit', 0, 12600000, 1, 14000, 'Toni Rustandi / M20 Black Wool');

-- ----------------------------
-- Table structure for tbl_buku_kas
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_buku_kas";
CREATE TABLE "public"."tbl_buku_kas" (
  "IDBukuKas" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_uang" float8
)
;

-- ----------------------------
-- Records of tbl_buku_kas
-- ----------------------------
INSERT INTO "public"."tbl_buku_kas" VALUES ('P000001', '2019-03-08', 'BK19PMKA00001', 'P000002', 'Kredit', NULL, 'aktif', NULL);
INSERT INTO "public"."tbl_buku_kas" VALUES ('P000002', '2019-03-04', 'BK19PMKA00002', 'P000001', 'Kredit', NULL, 'aktif', NULL);

-- ----------------------------
-- Table structure for tbl_buku_kas_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_buku_kas_detail";
CREATE TABLE "public"."tbl_buku_kas_detail" (
  "IDBukuKasDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBukuKas" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Kredit" float8
)
;

-- ----------------------------
-- Records of tbl_buku_kas_detail
-- ----------------------------
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000001', 'P000001', 'P000002', 390000, 1, 1, '', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000002', 'P000001', 'P000002', 1060000, 1, 1, '', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000003', 'P000001', 'P000002', 310000, 1, 1, 'Service vario', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000004', 'P000001', 'P000002', 650000, 1, 1, 'Karung, Klam, Tali', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000005', 'P000002', 'P000001', 80000, 1, 1, 'Herona ke Bpk.Riki', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000006', 'P000002', 'P000001', 80000, 1, 1, 'Herona ke Bpk.Anwar', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000007', 'P000002', 'P000001', 101000, 1, 1, 'Jasatama Polamedia', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000008', 'P000002', 'P000001', 140000, 1, 1, 'Tara paket', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P000009', 'P000002', 'P000001', 40000, 1, 1, 'Lampu tusuk & rem', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P00000a', 'P000002', 'P000001', 120000, 1, 1, 'RTK', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P00000b', 'P000002', 'P000001', 15000, 1, 1, 'Parkir', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P00000c', 'P000002', 'P000001', 137000, 1, 1, 'RTK Sample', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P00000d', 'P000002', 'P000001', 181000, 1, 1, 'RTK Sample', 0);
INSERT INTO "public"."tbl_buku_kas_detail" VALUES ('P00000e', 'P000002', 'P000001', 200000, 1, 1, 'BBM mobil box', 0);

-- ----------------------------
-- Table structure for tbl_coa
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_coa";
CREATE TABLE "public"."tbl_coa" (
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_IDCoa_seq"'::regclass),
  "Kode_COA" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_COA" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Modal" float8
)
;

-- ----------------------------
-- Records of tbl_coa
-- ----------------------------
INSERT INTO "public"."tbl_coa" VALUES ('P000001', '1110110', 'Kas Kecil', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000002', '1110211', 'Kas Besar', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000039', '1803000.0002', 'Ak.Depresiasi -  Mesin Electronic Selvage Letter Jack (2)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000007', '1130200', 'Deposito Berjangka IDR', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000008', '1210100', 'Piutang Giro Mundur Pihak Ketiga', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000009', '1221000', 'Piutang Usaha Pihak Ketiga', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000011', '1222000', 'Piutang Usaha Pihak Berelasi', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000013', '1223000', 'Piutang Lain-Lain Pihak Ketiga', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000014', '1224000', 'Piutang Lain-Lain Pihak Berelasi', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000015', '1320350', 'Persediaan Dalam Proses - Seragam', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000016', '1320355', 'Persediaan Dalam Proses - Aksesoris', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000017', '1320400', 'Persediaan Dalam Proses - Kain', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000018', '1330100', 'Persediaan Barang Kain', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000019', '1330110', 'Persediaan Barang Seragam', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000020', '1330300', 'Persediaan Barang Accesoris', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000021', '1390000', 'Penerimaan Barang/Penerimaan Invoice', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000022', '1429990', 'Suspense - Lainnya.', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000023', '1511000', 'Uang Muka Pembelian Pihak Berelasi', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000024', '1512000', 'Uang Muka Pembelian Pihak Ketiga', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000025', '1520010', 'PPN Masukan', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000026', '1520020', 'Pajak Dibayar Dimuka - PPh 22', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000027', '1520030', 'Pajak Dibayar Dimuka - PPh 23', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000040', '1804000.0001', 'Ak.Depresiasi - Motor Vario', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000041', '1804000.0002', 'Ak.Depresiasi - Kijang Inova Diesel 2014', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000030', '1520050', 'Pajak Dibayar Dimuka - PPh 4(2)', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000031', '1530100', 'Biaya Dibayar Dimuka - Sewa', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000032', '1530200', 'Biaya Dibayar Dimuka - Asuransi', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000033', '1530999', 'Biaya Dibayar Dimuka - Lainnya', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000034', '1802000.0001', 'Ak.Depresiasi - Ruko ITC Baranangsiang Blok F (1)', 'anak', 'aktif', 'P000003', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000035', '1802000.0002', 'Ak.Depresiasi - Ruko ITC Baranangsiang Blok F (2)', 'anak', 'aktif', 'P000003', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000036', '1802000.0003', 'Ak.Depresiasi - Ruko ITC Baranangsiang Blok F (3)', 'anak', 'aktif', 'P000003', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000037', '1802000.0004', 'Ak.Depresiasi - Apartemen Bekasi Ex Kim', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000038', '1803000.0001', 'Ak.Depresiasi -  Mesin Electronic Selvage Letter Jack (1)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000004', '1120051', 'Bank Central Asia - 1563087777', 'anak', 'aktif', 'P000001', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000042', '1804000.0003', 'Ak.Depresiasi - Kijang Inova Diesel 2018', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000005', '1120056', 'Bank Central Asia - 1561554477', 'anak', 'aktif', 'P000001', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000073', '1805000.0001', 'Ak.Depresiasi - AC Aolia', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000065', '1805000.0002', 'Ak.Depresiasi - Bor Maktec & Dudukan', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000043', '1805000.0003', 'Ak.Depresiasi - Istana Alumunium', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000060', '1805000.0004', 'Ak.Depresiasi - Kaca Film', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000052', '1805000.0007', 'Ak.Depresiasi - Lemari Arsip Hp I St 270', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000063', '1805000.0008', 'Ak.Depresiasi - Listrik', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000075', '1805000.0009', 'Ak.Depresiasi - Meja Kantor Hp I Od 032', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000077', '1805000.0028', 'Ak.Depresiasi - Meja Kantor Hp 1', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000047', '1805000.0042', 'Ak.Depresiasi - Meja Kantor', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000069', '1805000.0045', 'Ak.Depresiasi - Tv Lg 21 Fl 4Rg', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000055', '1805000.0049', 'Ak.Depresiasi - Koper Sampel Dan Pakaian', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000068', '1805000.0053', 'Ak.Depresiasi - Komputer LCD E-9', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000044', '1805000.0057', 'Ak.Depresiasi - Komputer Server', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000074', '1805000.0059', 'Ak.Depresiasi - Rak Untuk M20', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000071', '1805000.0062', 'Ak.Depresiasi - Rak Arsip', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000054', '1805000.0065', 'Ak.Depresiasi - Tangki Air (2)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000066', '1805000.0073', 'Ak.Depresiasi - Mesin Cutter Tsc', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000057', '1805000.0083', 'Ak.Depresiasi - Rak Hanger', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000070', '1805000.0087', 'Ak.Depresiasi - Od +Drawers Central Lock Hp', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000062', '1805000.0088', 'Ak.Depresiasi - Big Wheel F/Hand Trolley + Puller', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000049', '1805000.0093', 'Ak.Depresiasi - Pengaman Lift', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000061', '1805000.0094', 'Ak.Depresiasi - Pompa Shimizu U/ E9', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000050', '1805000.0099', 'Ak.Depresiasi - Lemari Besi Admin', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000056', '1805000.0101', 'Ak.Depresiasi - Kursi Kantor (1)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000076', '1805000.0104', 'Ak.Depresiasi - Pompa Air', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000045', '1805000.0105', 'Ak.Depresiasi - Rak Besi F11', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000051', '1805000.0112', 'Ak.Depresiasi - Meja + Kursi', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000110', '1805000.0011', 'Ak.Depresiasi - Meja Kantor Hp V Hod 5055', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000091', '1805000.0012', 'Ak.Depresiasi - Mesin Potong', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000096', '1805000.0013', 'Ak.Depresiasi - Pintu Lemari Hp I St 701', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000112', '1805000.0014', 'Ak.Depresiasi - Rak Buku 3 Susun BK 503 (1)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000097', '1805000.0015', 'Ak.Depresiasi - Rak Buku 3 Susun BK 504 (1)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000145', '1805000.0016', 'Ak.Depresiasi - Telepon Pabx (1)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000087', '1805000.0017', 'Ak.Depresiasi - Teralis', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000111', '1805000.0019', 'Ak.Depresiasi - Pintu Lemari St 701A Hp 1', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000117', '1805000.0020', 'Ak.Depresiasi - Rak Buku 3 Susun BK 503 (2)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000120', '1805000.0021', 'Ak.Depresiasi - Rak Buku 3 Susun BK 503 (3)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000119', '1805000.0022', 'Ak.Depresiasi - Rak Buku 3 Susun BK 504 (2)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000118', '1805000.0023', 'Ak.Depresiasi - Speaker Simbada Cst Z100', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000088', '1805000.0024', 'Ak.Depresiasi - Meja Komputer (1)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000098', '1805000.0026', 'Ak.Depresiasi - Rak Besi Untuk Kain', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000184', '1805000.0097', 'Ak.Depresiasi - Pisau Potong', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000146', '1805000.0027', 'Ak.Depresiasi - Lemari Arsip Hp 1 + Kk L. Arsip', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000113', '1805000.0034', 'Ak.Depresiasi - Telepon Pabx (2)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000114', '1805000.0035', 'Ak.Depresiasi - Rak Besi + Multiplek', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000109', '1805000.0039', 'Ak.Depresiasi - AC Panasonic', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000095', '1805000.0040', 'Ak.Depresiasi - Joint Table', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000144', '1805000.0041', 'Ak.Depresiasi - Laci Sorong ', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000134', '1805000.0043', 'Ak.Depresiasi - Meja Komputer (3)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000138', '1805000.0044', 'Ak.Depresiasi - Rak Buku Expo 3183', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000126', '1805000.0047', 'Ak.Depresiasi - Pisau Potong Polytex 50Cm-5Mm', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000083', '1805000.0051', 'Ak.Depresiasi - Rak Besi', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000089', '1805000.0055', 'Ak.Depresiasi - Mesin Potong & Pisau', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000106', '1805000.0058', 'Ak.Depresiasi - Filter Pompa Air', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000115', '1805000.0060', 'Ak.Depresiasi - Tangki Air (1)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000090', '1805000.0061', 'Ak.Depresiasi - Meja Komputer + Rak', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000148', '1805000.0064', 'Ak.Depresiasi - Meja Kantor (Hp I -302)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000082', '1805000.0067', 'Ak.Depresiasi - Lemari Besi', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000080', '1805000.0068', 'Ak.Depresiasi - Mesin Ketokan', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000127', '1805000.0070', 'Ak.Depresiasi - Lemari Rak Arsip', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000081', '1805000.0072', 'Ak.Depresiasi - Alat Potong Kain Gramasi + Klem', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000116', '1805000.0077', 'Ak.Depresiasi - Conference Table Ct 3B Highpoint', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000107', '1805000.0079', 'Ak.Depresiasi - Locker Ace Hardware', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000108', '1805000.0080', 'Ak.Depresiasi - Monitor Lcd Acer 16"', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000137', '1805000.0081', 'Ak.Depresiasi - Office Desk Hod 5052 Highpoint', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000129', '1805000.0084', 'Ak.Depresiasi - Rak Kain Baru', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000124', '1805000.0085', 'Ak.Depresiasi - Tralis Dan Canopy', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000100', '1805000.0086', 'Ak.Depresiasi - Lemari Arsip 2 Set', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000102', '1805000.0089', 'Ak.Depresiasi - Table Fan Industrial', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000093', '1805000.0091', 'Ak.Depresiasi - Rak Besi Untuk Kain', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000121', '1805000.0092', 'Ak.Depresiasi - Furniture', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000139', '1805000.0095', 'Ak.Depresiasi - Printer Hp Laserjet M1132', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000099', '1805000.0098', 'Ak.Depresiasi - Fax Panasonic', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000136', '1805000.0102', 'Ak.Depresiasi - Rak Besi Baru', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000128', '1805000.0103', 'Ak.Depresiasi - Rak Admin', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000104', '1805000.0108', 'Ak.Depresiasi - Peralatan CCTV (3)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000094', '1805000.0109', 'Ak.Depresiasi - Rak Besi 2015', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000135', '1805000.0110', 'Ak.Depresiasi - UPS (2)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000084', '1805000.0113', 'Ak.Depresiasi - CPU Unit', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000130', '1805000.0114', 'Ak.Depresiasi - Kursi Kantor (2)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000131', '1805000.0115', 'Ak.Depresiasi - Rak Arsip Baru', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000213', '1702500.0002', 'Aset Tetap -  Bangunan Ruko ITC Baranangsiang Blok F (2)', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000214', '1702500.0003', 'Aset Tetap -  Bangunan Ruko ITC Baranangsiang Blok F (3)', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000215', '1702500.0004', 'Aset Tetap -  Bangunan Apartemen Bekasi Ex Kim', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000216', '1703001.0001', 'Aset Tetap -  Mesin Electronic Selvage Letter Jack (1)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000217', '1703001.0002', 'Aset Tetap -  Mesin Electronic Selvage Letter Jack (2)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000218', '1704500.0001', 'Aset Tetap -  Kendaraan Motor Vario', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000210', '1614200', 'Investasi Asosiasi - Bina Citra Sentosa', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000211', '1701000', 'Aset Tetap - Tanah', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000200', '1805000.0005', 'Ak.Depresiasi - Kursi Manager Hp 62', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000178', '1805000.0010', 'Ak.Depresiasi - Meja Kantor Hp I Od 300', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000175', '1805000.0018', 'Ak.Depresiasi - Lemari Arsip ST 230A HP 1', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000176', '1805000.0025', 'Ak.Depresiasi - Meja Komputer (2)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000177', '1805000.0029', 'Ak.Depresiasi - Pintu Lemari Hp 1', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000179', '1805000.0030', 'Ak.Depresiasi - Air Conditioner', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000191', '1805000.0031', 'Ak.Depresiasi - Partisi Kayu', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000192', '1805000.0032', 'Ak.Depresiasi - Rak Dan Tralis Ruko E-9', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000167', '1805000.0033', 'Ak.Depresiasi - Mesin Potong', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000168', '1805000.0036', 'Ak.Depresiasi - Kursi Administrasi', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000169', '1805000.0037', 'Ak.Depresiasi - Sekat Alumunium', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000173', '1805000.0038', 'Ak.Depresiasi - Handtruck Trolley', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000182', '1805000.0046', 'Ak.Depresiasi - Hekter Max 12 L', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000166', '1805000.0048', 'Ak.Depresiasi - Rengh Kertas Primo', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000187', '1805000.0050', 'Ak.Depresiasi - Tangga Alumunium', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000165', '1805000.0052', 'Ak.Depresiasi - Software Accurate', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000180', '1805000.0054', 'Ak.Depresiasi - Rak Dan Lemari', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000202', '1805000.0056', 'Ak.Depresiasi - Printer Xerox Phaser 3124', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000186', '1805000.0063', 'Ak.Depresiasi - Meja Kantor (Hp I -300)', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000194', '1805000.0066', 'Ak.Depresiasi - Monitor Lcd Lg 15,6"', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000181', '1805000.0069', 'Ak.Depresiasi - Hard Disk External Toshiba 500 Gb', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000160', '1805000.0071', 'Ak.Depresiasi - Lemari Arsip', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000195', '1805000.0074', 'Ak.Depresiasi - Pemadam Kebakaran', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000185', '1805000.0075', 'Ak.Depresiasi - AC Sharp Plasma', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000158', '1805000.0076', 'Ak.Depresiasi - Besi Siku Lobang 4X4X4Mtr', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000163', '1805000.0078', 'Ak.Depresiasi - Lemari Arsip VCL 492', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000196', '1805000.0082', 'Ak.Depresiasi - Rak Alumunium', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000150', '1805000.0090', 'Ak.Depresiasi - Kursi Chitose', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000156', '1805000.0096', 'Ak.Depresiasi - Komputer Admin', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000183', '1805000.0100', 'Ak.Depresiasi - Notebook Lenovo', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000162', '1805000.0106', 'Ak.Depresiasi - CPU Baru', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000201', '1805000.0107', 'Ak.Depresiasi - Acer Notebook', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000193', '1805000.0111', 'Ak.Depresiasi - Komputer + Printer', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000152', '1805000.0116', 'Ak.Depresiasi - Telepon Panasonic', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000174', '1805000.0117', 'Ak.Depresiasi - Printer Canon Lbp 6030', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000207', '1805000.0118', 'Ak.Depresiasi - Printer & Scanner', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000206', '1805000.0119', 'Ak.Depresiasi - Laptop', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000221', '1705000.0001', 'Aset Tetap -  AC Aolia', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000626', '2222000', 'Utang Usaha Pihak Berelasi', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000228', '1705000.0008', 'Aset Tetap -  Listrik', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000229', '1705000.0009', 'Aset Tetap -  Meja Kantor Hp I Od 032', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000233', '1705000.0013', 'Aset Tetap -  Pintu Lemari Hp I St 701', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000238', '1705000.0018', 'Aset Tetap -  Lemari Arsip ST 230A HP 1', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000239', '1705000.0019', 'Aset Tetap -  Pintu Lemari St 701A Hp 1', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000240', '1705000.0020', 'Aset Tetap -  Rak Buku 3 Susun BK 503 (2)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000241', '1705000.0021', 'Aset Tetap -  Rak Buku 3 Susun BK 503 (3)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000242', '1705000.0022', 'Aset Tetap -  Rak Buku 3 Susun BK 504 (2)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000243', '1705000.0023', 'Aset Tetap -  Speaker Simbada Cst Z100', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000244', '1705000.0024', 'Aset Tetap -  Meja Komputer (1)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000245', '1705000.0025', 'Aset Tetap -  Meja Komputer (2)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000246', '1705000.0026', 'Aset Tetap -  Rak Besi Untuk Kain', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000247', '1705000.0027', 'Aset Tetap -  Lemari Arsip Hp 1 + Kk L. Arsip', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000248', '1705000.0028', 'Aset Tetap -  Meja Kantor Hp 1', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000249', '1705000.0029', 'Aset Tetap -  Pintu Lemari Hp 1', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000250', '1705000.0030', 'Aset Tetap -  Air Conditioner', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000251', '1705000.0031', 'Aset Tetap -  Partisi Kayu', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000252', '1705000.0032', 'Aset Tetap -  Rak Dan Tralis Ruko E-9', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000253', '1705000.0033', 'Aset Tetap -  Mesin Potong', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000254', '1705000.0034', 'Aset Tetap -  Telepon Pabx (2)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000255', '1705000.0035', 'Aset Tetap -  Rak Besi + Multiplek', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000256', '1705000.0036', 'Aset Tetap -  Kursi Administrasi', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000257', '1705000.0037', 'Aset Tetap -  Sekat Alumunium', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000258', '1705000.0038', 'Aset Tetap -  Handtruck Trolley', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000259', '1705000.0039', 'Aset Tetap -  AC Panasonic', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000260', '1705000.0040', 'Aset Tetap -  Joint Table', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000261', '1705000.0041', 'Aset Tetap -  Laci Sorong ', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000262', '1705000.0042', 'Aset Tetap -  Meja Kantor', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000263', '1705000.0043', 'Aset Tetap -  Meja Komputer (3)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000264', '1705000.0044', 'Aset Tetap -  Rak Buku Expo 3183', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000265', '1705000.0045', 'Aset Tetap -  Tv Lg 21 Fl 4Rg', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000266', '1705000.0046', 'Aset Tetap -  Hekter Max 12 L', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000267', '1705000.0047', 'Aset Tetap -  Pisau Potong Polytex 50Cm-5Mm', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000268', '1705000.0048', 'Aset Tetap -  Rengh Kertas Primo', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000269', '1705000.0049', 'Aset Tetap -  Koper Sampel Dan Pakaian', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000270', '1705000.0050', 'Aset Tetap -  Tangga Alumunium', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000271', '1705000.0051', 'Aset Tetap -  Rak Besi', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000272', '1705000.0052', 'Aset Tetap -  Software Accurate', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000273', '1705000.0053', 'Aset Tetap -  Komputer LCD E-9', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000274', '1705000.0054', 'Aset Tetap -  Rak Dan Lemari', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000275', '1705000.0055', 'Aset Tetap -  Mesin Potong & Pisau', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000276', '1705000.0056', 'Aset Tetap -  Printer Xerox Phaser 3124', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000277', '1705000.0057', 'Aset Tetap -  Komputer Server', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000278', '1705000.0058', 'Aset Tetap -  Filter Pompa Air', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000279', '1705000.0059', 'Aset Tetap -  Rak Untuk M20', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000280', '1705000.0060', 'Aset Tetap -  Tangki Air (1)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000281', '1705000.0061', 'Aset Tetap -  Meja Komputer + Rak', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000282', '1705000.0062', 'Aset Tetap -  Rak Arsip', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000283', '1705000.0063', 'Aset Tetap -  Meja Kantor (Hp I -300)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000284', '1705000.0064', 'Aset Tetap -  Meja Kantor (Hp I -302)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000285', '1705000.0065', 'Aset Tetap -  Tangki Air (2)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000286', '1705000.0066', 'Aset Tetap -  Monitor Lcd Lg 15,6"', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000287', '1705000.0067', 'Aset Tetap -  Lemari Besi', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000288', '1705000.0068', 'Aset Tetap -  Mesin Ketokan', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000289', '1705000.0069', 'Aset Tetap -  Hard Disk External Toshiba 500 Gb', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000220', '1704500.0003', 'Aset Tetap -  Kendaraan Kijang Inova Diesel 2018', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000222', '1705000.0002', 'Aset Tetap -  Bor Maktec & Dudukan', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000223', '1705000.0003', 'Aset Tetap -  Istana Alumunium', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000224', '1705000.0004', 'Aset Tetap -  Kaca Film', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000225', '1705000.0005', 'Aset Tetap -  Kursi Manager Hp 62', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000226', '1705000.0006', 'Aset Tetap -  Laci Sorong Hp I Mb 135', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000227', '1705000.0007', 'Aset Tetap -  Lemari Arsip Hp I St 270', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000230', '1705000.0010', 'Aset Tetap -  Meja Kantor Hp I Od 300', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000231', '1705000.0011', 'Aset Tetap -  Meja Kantor Hp V Hod 5055', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000232', '1705000.0012', 'Aset Tetap -  Mesin Potong', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000234', '1705000.0014', 'Aset Tetap -  Rak Buku 3 Susun BK 503 (1)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000235', '1705000.0015', 'Aset Tetap -  Rak Buku 3 Susun BK 504 (1)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000236', '1705000.0016', 'Aset Tetap -  Telepon Pabx (1)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000237', '1705000.0017', 'Aset Tetap -  Teralis', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000308', '1705000.0088', 'Aset Tetap -  Big Wheel F/Hand Trolley + Puller', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000309', '1705000.0089', 'Aset Tetap -  Table Fan Industrial', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000310', '1705000.0090', 'Aset Tetap -  Kursi Chitose', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000329', '1705000.0109', 'Aset Tetap -  Rak Besi 2015', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000330', '1705000.0110', 'Aset Tetap -  UPS (2)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000331', '1705000.0111', 'Aset Tetap -  Komputer + Printer', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000332', '1705000.0112', 'Aset Tetap -  Meja + Kursi', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000333', '1705000.0113', 'Aset Tetap -  CPU Unit', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000334', '1705000.0114', 'Aset Tetap -  Kursi Kantor (2)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000335', '1705000.0115', 'Aset Tetap -  Rak Arsip Baru', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000336', '1705000.0116', 'Aset Tetap -  Telepon Panasonic', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000337', '1705000.0117', 'Aset Tetap -  Printer Canon Lbp 6030', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000338', '1705000.0118', 'Aset Tetap -  Printer & Scanner', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000339', '1705000.0119', 'Aset Tetap -  Laptop', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000291', '1705000.0071', 'Aset Tetap -  Lemari Arsip', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000292', '1705000.0072', 'Aset Tetap -  Alat Potong Kain Gramasi + Klem', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000293', '1705000.0073', 'Aset Tetap -  Mesin Cutter Tsc', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000294', '1705000.0074', 'Aset Tetap -  Pemadam Kebakaran', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000295', '1705000.0075', 'Aset Tetap -  AC Sharp Plasma', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000296', '1705000.0076', 'Aset Tetap -  Besi Siku Lobang 4X4X4Mtr', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000297', '1705000.0077', 'Aset Tetap -  Conference Table Ct 3B Highpoint', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000298', '1705000.0078', 'Aset Tetap -  Lemari Arsip VCL 492', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000299', '1705000.0079', 'Aset Tetap -  Locker Ace Hardware', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000300', '1705000.0080', 'Aset Tetap -  Monitor Lcd Acer 16"', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000301', '1705000.0081', 'Aset Tetap -  Office Desk Hod 5052 Highpoint', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000302', '1705000.0082', 'Aset Tetap -  Rak Alumunium', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000303', '1705000.0083', 'Aset Tetap -  Rak Hanger', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000304', '1705000.0084', 'Aset Tetap -  Rak Kain Baru', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000305', '1705000.0085', 'Aset Tetap -  Tralis Dan Canopy', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000306', '1705000.0086', 'Aset Tetap -  Lemari Arsip 2 Set', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000311', '1705000.0091', 'Aset Tetap -  Rak Besi Untuk Kain', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000312', '1705000.0092', 'Aset Tetap -  Furniture', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000313', '1705000.0093', 'Aset Tetap -  Pengaman Lift', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000314', '1705000.0094', 'Aset Tetap -  Pompa Shimizu U/ E9', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000315', '1705000.0095', 'Aset Tetap -  Printer Hp Laserjet M1132', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000316', '1705000.0096', 'Aset Tetap -  Komputer Admin', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000317', '1705000.0097', 'Aset Tetap -  Pisau Potong', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000318', '1705000.0098', 'Aset Tetap -  Fax Panasonic', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000319', '1705000.0099', 'Aset Tetap -  Lemari Besi Admin', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000320', '1705000.0100', 'Aset Tetap -  Notebook Lenovo', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000321', '1705000.0101', 'Aset Tetap -  Kursi Kantor (1)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000322', '1705000.0102', 'Aset Tetap -  Rak Besi Baru', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000323', '1705000.0103', 'Aset Tetap -  Rak Admin', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000307', '1705000.0087', 'Aset Tetap -  Od +Drawers Central Lock Hp', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000324', '1705000.0104', 'Aset Tetap -  Pompa Air', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000325', '1705000.0105', 'Aset Tetap -  Rak Besi F11', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000326', '1705000.0106', 'Aset Tetap -  CPU Baru', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000327', '1705000.0107', 'Aset Tetap -  Acer Notebook', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000328', '1705000.0108', 'Aset Tetap -  Peralatan CCTV (3)', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000388', '1722000.0001', 'Bangunan Dalam Proses', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000389', '1910000', 'Aset Pajak Tangguhan', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000390', '1930200', 'Aset Tidak Lancar Lainnya', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000391', '6120110', 'B. Administrasi Umum - Gaji & Upah', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000392', '6120120', 'B. Administrasi Umum - Lembur', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000393', '6120210', 'B. Administrasi Umum - Tunjangan & Bonus', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000394', '6120220', 'B. Administrasi Umum - Kesejahteraan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000395', '6120230', 'B. Administrasi Umum - Asuransi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000396', '6120310', 'B. Administrasi Umum - Pemeliharaan Gedung', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000397', '6120410', 'B. Administrasi Umum - Pemeliharaan Kendaraan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000398', '6120510', 'B. Administrasi Umum - Jamuan & Representasi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000399', '6120515', 'B. Administrasi Umum - Sumbangan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000400', '6120550', 'B. Administrasi Umum - Perizinan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000401', '6120560', 'B. Administrasi Umum - Keanggotaan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000402', '6120610', 'B. Administrasi Umum - Rumah Tangga', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000403', '6120620', 'B. Administrasi Umum - Alat Tulis Kantor', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000404', '6120710', 'B. Administrasi Umum - Informasi Teknologi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000405', '6120810', 'B. Administrasi Umum - Biaya Bank', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000406', '6120910', 'B. Administrasi Umum - Sewa', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000407', '6121010', 'B. Administrasi Umum - Komunikasi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000408', '6121110', 'B. Administrasi Umum - Perjalanan & Akomodasi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000409', '6121120', 'B. Administrasi Umum - Transportasi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000410', '6121210', 'B. Administrasi Umum - Konsultan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000411', '6121310', 'B. Administrasi Umum - Sumber Daya Manusia', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000416', '6121412.0001', 'B. Administrasi Umum - Depresiasi Mesin Electronic Selvage Letter Jack (1)', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000417', '6121412.0002', 'B. Administrasi Umum - Depresiasi Mesin Electronic Selvage Letter Jack (2)', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000420', '6121413.0003', 'B. Administrasi Umum - Depresiasi Kijang Inova Diesel 2018', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000413', '6121411.0002', 'B. Administrasi Umum - Depresiasi Ruko ITC Baranangsiang Blok F (2)', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000414', '6121411.0003', 'B. Administrasi Umum - Depresiasi Ruko ITC Baranangsiang Blok F (3)', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000421', '6121414.0002', 'B. Administrasi Umum - Depresiasi Bor Maktec & Dudukan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000430', '6121414.0006', 'B. Administrasi Umum - Depresiasi Laci Sorong Hp I Mb 135', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000425', '6121414.0041', 'B. Administrasi Umum - Depresiasi Laci Sorong ', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000422', '6121414.0056', 'B. Administrasi Umum - Depresiasi Printer Xerox Phaser 3124', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000493', '6121414.0076', 'B. Administrasi Umum - Depresiasi Besi Siku Lobang 4X4X4Mtr', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000427', '6121414.0092', 'B. Administrasi Umum - Depresiasi Furniture', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000428', '6121414.0098', 'B. Administrasi Umum - Depresiasi Fax Panasonic', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000423', '6121414.0104', 'B. Administrasi Umum - Depresiasi Pompa Air', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000429', '6121414.0111', 'B. Administrasi Umum - Depresiasi Komputer + Printer', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000438', '6121414.0003', 'B. Administrasi Umum - Depresiasi Istana Alumunium', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000441', '6121414.0007', 'B. Administrasi Umum - Depresiasi Lemari Arsip Hp I St 270', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000452', '6121414.0008', 'B. Administrasi Umum - Depresiasi Listrik', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000487', '6121414.0010', 'B. Administrasi Umum - Depresiasi Meja Kantor Hp I Od 300', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000468', '6121414.0011', 'B. Administrasi Umum - Depresiasi Meja Kantor Hp V Hod 5055', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000473', '6121414.0012', 'B. Administrasi Umum - Depresiasi Mesin Potong', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000489', '6121414.0013', 'B. Administrasi Umum - Depresiasi Pintu Lemari Hp I St 701', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000474', '6121414.0014', 'B. Administrasi Umum - Depresiasi Rak Buku 3 Susun BK 503 (1)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000464', '6121414.0016', 'B. Administrasi Umum - Depresiasi Telepon Pabx (1)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000488', '6121414.0018', 'B. Administrasi Umum - Depresiasi Lemari Arsip ST 230A HP 1', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000465', '6121414.0023', 'B. Administrasi Umum - Depresiasi Speaker Simbada Cst Z100', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000475', '6121414.0025', 'B. Administrasi Umum - Depresiasi Meja Komputer (2)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000454', '6121414.0027', 'B. Administrasi Umum - Depresiasi Lemari Arsip Hp 1 + Kk L. Arsip', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000490', '6121414.0033', 'B. Administrasi Umum - Depresiasi Mesin Potong', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000491', '6121414.0034', 'B. Administrasi Umum - Depresiasi Telepon Pabx (2)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000486', '6121414.0038', 'B. Administrasi Umum - Depresiasi Handtruck Trolley', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000472', '6121414.0039', 'B. Administrasi Umum - Depresiasi AC Panasonic', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000447', '6121414.0044', 'B. Administrasi Umum - Depresiasi Rak Buku Expo 3183', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000433', '6121414.0048', 'B. Administrasi Umum - Depresiasi Rengh Kertas Primo', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000460', '6121414.0050', 'B. Administrasi Umum - Depresiasi Tangga Alumunium', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000446', '6121414.0052', 'B. Administrasi Umum - Depresiasi Software Accurate', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000466', '6121414.0054', 'B. Administrasi Umum - Depresiasi Rak Dan Lemari', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000483', '6121414.0057', 'B. Administrasi Umum - Depresiasi Komputer Server', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000451', '6121414.0058', 'B. Administrasi Umum - Depresiasi Filter Pompa Air', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000492', '6121414.0059', 'B. Administrasi Umum - Depresiasi Rak Untuk M20', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000467', '6121414.0060', 'B. Administrasi Umum - Depresiasi Tangki Air (1)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000449', '6121414.0061', 'B. Administrasi Umum - Depresiasi Meja Komputer + Rak', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000432', '6121414.0064', 'B. Administrasi Umum - Depresiasi Meja Kantor (Hp I -302)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000459', '6121414.0066', 'B. Administrasi Umum - Depresiasi Monitor Lcd Lg 15,6"', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000457', '6121414.0067', 'B. Administrasi Umum - Depresiasi Lemari Besi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000458', '6121414.0071', 'B. Administrasi Umum - Depresiasi Lemari Arsip', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000444', '6121414.0072', 'B. Administrasi Umum - Depresiasi Alat Potong Kain Gramasi + Klem', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000484', '6121414.0078', 'B. Administrasi Umum - Depresiasi Lemari Arsip VCL 492', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000485', '6121414.0079', 'B. Administrasi Umum - Depresiasi Locker Ace Hardware', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000435', '6121414.0082', 'B. Administrasi Umum - Depresiasi Rak Alumunium', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000477', '6121414.0085', 'B. Administrasi Umum - Depresiasi Tralis Dan Canopy', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000448', '6121414.0086', 'B. Administrasi Umum - Depresiasi Lemari Arsip 2 Set', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000440', '6121414.0087', 'B. Administrasi Umum - Depresiasi Od +Drawers Central Lock Hp', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000479', '6121414.0088', 'B. Administrasi Umum - Depresiasi Big Wheel F/Hand Trolley + Puller', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000470', '6121414.0090', 'B. Administrasi Umum - Depresiasi Kursi Chitose', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000439', '6121414.0093', 'B. Administrasi Umum - Depresiasi Pengaman Lift', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000476', '6121414.0097', 'B. Administrasi Umum - Depresiasi Pisau Potong', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000434', '6121414.0100', 'B. Administrasi Umum - Depresiasi Notebook Lenovo', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000453', '6121414.0103', 'B. Administrasi Umum - Depresiasi Rak Admin', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000481', '6121414.0107', 'B. Administrasi Umum - Depresiasi Acer Notebook', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000471', '6121414.0108', 'B. Administrasi Umum - Depresiasi Peralatan CCTV (3)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000461', '6121414.0112', 'B. Administrasi Umum - Depresiasi Meja + Kursi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000625', '2221000', 'Utang Usaha Pihak Ketiga', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000522', '6121414.0015', 'B. Administrasi Umum - Depresiasi Rak Buku 3 Susun BK 504 (1)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000552', '6121414.0017', 'B. Administrasi Umum - Depresiasi Teralis', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000494', '6121414.0019', 'B. Administrasi Umum - Depresiasi Pintu Lemari St 701A Hp 1', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000497', '6121414.0020', 'B. Administrasi Umum - Depresiasi Rak Buku 3 Susun BK 503 (2)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000496', '6121414.0021', 'B. Administrasi Umum - Depresiasi Rak Buku 3 Susun BK 503 (3)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000495', '6121414.0022', 'B. Administrasi Umum - Depresiasi Rak Buku 3 Susun BK 504 (2)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000553', '6121414.0024', 'B. Administrasi Umum - Depresiasi Meja Komputer (1)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000523', '6121414.0026', 'B. Administrasi Umum - Depresiasi Rak Besi Untuk Kain', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000544', '6121414.0032', 'B. Administrasi Umum - Depresiasi Rak Dan Tralis Ruko E-9', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000545', '6121414.0035', 'B. Administrasi Umum - Depresiasi Rak Besi + Multiplek', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000546', '6121414.0036', 'B. Administrasi Umum - Depresiasi Kursi Administrasi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000550', '6121414.0037', 'B. Administrasi Umum - Depresiasi Sekat Alumunium', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000521', '6121414.0040', 'B. Administrasi Umum - Depresiasi Joint Table', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000511', '6121414.0042', 'B. Administrasi Umum - Depresiasi Meja Kantor', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000515', '6121414.0043', 'B. Administrasi Umum - Depresiasi Meja Komputer (3)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000503', '6121414.0046', 'B. Administrasi Umum - Depresiasi Hekter Max 12 L', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000543', '6121414.0047', 'B. Administrasi Umum - Depresiasi Pisau Potong Polytex 50Cm-5Mm', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000542', '6121414.0051', 'B. Administrasi Umum - Depresiasi Rak Besi', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000525', '6121414.0063', 'B. Administrasi Umum - Depresiasi Meja Kantor (Hp I -300)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000504', '6121414.0069', 'B. Administrasi Umum - Depresiasi Hard Disk External Toshiba 500 Gb', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000537', '6121414.0070', 'B. Administrasi Umum - Depresiasi Lemari Rak Arsip', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000535', '6121414.0075', 'B. Administrasi Umum - Depresiasi AC Sharp Plasma', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000540', '6121414.0077', 'B. Administrasi Umum - Depresiasi Conference Table Ct 3B Highpoint', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000514', '6121414.0080', 'B. Administrasi Umum - Depresiasi Monitor Lcd Acer 16"', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000506', '6121414.0083', 'B. Administrasi Umum - Depresiasi Rak Hanger', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000501', '6121414.0084', 'B. Administrasi Umum - Depresiasi Rak Kain Baru', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000527', '6121414.0089', 'B. Administrasi Umum - Depresiasi Table Fan Industrial', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000498', '6121414.0091', 'B. Administrasi Umum - Depresiasi Rak Besi Untuk Kain', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000516', '6121414.0094', 'B. Administrasi Umum - Depresiasi Pompa Shimizu U/ E9', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000533', '6121414.0095', 'B. Administrasi Umum - Depresiasi Printer Hp Laserjet M1132', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000513', '6121414.0101', 'B. Administrasi Umum - Depresiasi Kursi Kantor (1)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000505', '6121414.0102', 'B. Administrasi Umum - Depresiasi Rak Besi Baru', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000539', '6121414.0105', 'B. Administrasi Umum - Depresiasi Rak Besi F11', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000512', '6121414.0109', 'B. Administrasi Umum - Depresiasi Rak Besi 2015', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000507', '6121414.0113', 'B. Administrasi Umum - Depresiasi CPU Unit', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000508', '6121414.0114', 'B. Administrasi Umum - Depresiasi Kursi Kantor (2)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000529', '6121414.0115', 'B. Administrasi Umum - Depresiasi Rak Arsip Baru', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000551', '6121414.0116', 'B. Administrasi Umum - Depresiasi Telepon Panasonic', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000509', '6121414.0119', 'B. Administrasi Umum - Depresiasi Laptop', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000588', '6121510', 'B. Administrasi Umum - Langganan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000589', '6121710', 'B. Administrasi Umum - Air & Listrik', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000590', '6121910', 'B. Administrasi Umum - Pajak', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000591', '6122010', 'B. Administrasi Umum - Asuransi Gedung', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000592', '6122020', 'B. Administrasi Umum - Asuransi Kendaraan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000593', '6129900', 'B. Administrasi Umum - Lainnya', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000594', '7120110', 'Beban Lain - Bunga Pinjaman Pihak Ketiga', 'anak', 'aktif', 'P000005', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000595', '7120120', 'Beban Lain - Bunga Kredit Lokal', 'anak', 'aktif', 'P000005', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000596', '7129990', 'Beban Lain - Beban Lainnya', 'anak', 'aktif', 'P000005', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000597', '6110100', 'B. Penjualan - Promosi dan Iklan', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000598', '6110150', 'B. Penjualan - Percetakan', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000599', '6110400', 'B. Penjualan - Desain', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000600', '6110500', 'B. Penjualan - Sample Penjualan', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000601', '6110510', 'B. Penjualan - Uji Laboratorium', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000602', '6112200', 'B. Penjualan - Komisi', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000603', '6113000', 'B. Penjualan - Klaim', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000604', '6114110', 'B. Penjualan - Gaji Bag.Penjualan', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000605', '6114210', 'B. Penjualan - Tunjangan Bag.Penjualan', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000606', '6114220', 'B. Penjualan - Kesejahteraan Bag.Penjualan', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000607', '6114230', 'B. Penjualan - Asuransi Bag.Penjualan', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000608', '6115110', 'B. Penjualan - Perjalanan & Akomodasi', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000609', '6115120', 'B. Penjualan - Transportasi', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000611', '6115510', 'B. Penjualan - Jamuan & Representasi', 'anak', 'aktif', 'P000006', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000612', '4110100', 'Harga Pokok Penjualan - Kain', 'anak', 'aktif', 'P000008', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000613', '4110115', 'Harga Pokok Penjualan - Seragam', 'anak', 'aktif', 'P000008', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000614', '4110120', 'Harga Pokok Penjualan - Aksesoris', 'anak', 'aktif', 'P000008', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000615', '5260010', 'Angkutan & Kuli', 'anak', 'aktif', 'P000008', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000616', '2811000', 'Kepentingan Non Pengendali', 'anak', 'aktif', 'P000007', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000617', '2910100', 'Modal Disetor', 'anak', 'aktif', 'P000007', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000618', '2950100', 'Saldo Laba Ditahan', 'anak', 'aktif', 'P000007', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000619', '2950101', 'Laba (Rugi) Tahun Berjalan', 'anak', 'aktif', 'P000007', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000620', '2630000', 'Utang Devidend', 'anak', 'aktif', 'P000009', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000621', '2710100', 'Utang Bank Jangka Panjang', 'anak', 'aktif', 'P000009', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000622', '2340000', 'Utang Leasing', 'anak', 'aktif', 'P000009', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000623', '2120100', 'Utang Bank Jangka Pendek', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000624', '2210100', 'Utang Giro Mundur Pihak Ketiga', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000577', '6121414.0004', 'B. Administrasi Umum - Depresiasi Kaca Film', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000555', '6121414.0009', 'B. Administrasi Umum - Depresiasi Meja Kantor Hp I Od 032', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000554', '6121414.0028', 'B. Administrasi Umum - Depresiasi Meja Kantor Hp 1', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000556', '6121414.0029', 'B. Administrasi Umum - Depresiasi Pintu Lemari Hp 1', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000568', '6121414.0030', 'B. Administrasi Umum - Depresiasi Air Conditioner', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000569', '6121414.0031', 'B. Administrasi Umum - Depresiasi Partisi Kayu', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000559', '6121414.0045', 'B. Administrasi Umum - Depresiasi Tv Lg 21 Fl 4Rg', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000564', '6121414.0049', 'B. Administrasi Umum - Depresiasi Koper Sampel Dan Pakaian', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000557', '6121414.0053', 'B. Administrasi Umum - Depresiasi Komputer LCD E-9', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000579', '6121414.0055', 'B. Administrasi Umum - Depresiasi Mesin Potong & Pisau', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000563', '6121414.0062', 'B. Administrasi Umum - Depresiasi Rak Arsip', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000571', '6121414.0065', 'B. Administrasi Umum - Depresiasi Tangki Air (2)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000558', '6121414.0068', 'B. Administrasi Umum - Depresiasi Mesin Ketokan', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000572', '6121414.0073', 'B. Administrasi Umum - Depresiasi Mesin Cutter Tsc', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000562', '6121414.0074', 'B. Administrasi Umum - Depresiasi Pemadam Kebakaran', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000573', '6121414.0081', 'B. Administrasi Umum - Depresiasi Office Desk Hod 5052 Highpoint', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000561', '6121414.0096', 'B. Administrasi Umum - Depresiasi Komputer Admin', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000560', '6121414.0099', 'B. Administrasi Umum - Depresiasi Lemari Besi Admin', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000578', '6121414.0106', 'B. Administrasi Umum - Depresiasi CPU Baru', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000570', '6121414.0110', 'B. Administrasi Umum - Depresiasi UPS (2)', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000584', '6121414.0117', 'B. Administrasi Umum - Depresiasi Printer Canon Lbp 6030', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000583', '6121414.0118', 'B. Administrasi Umum - Depresiasi Printer & Scanner', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000627', '2223000', 'Utang Lain-Lain Pihak Ketiga', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000628', '2224000', 'Utang Lain-Lain Pihak Berelasi', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000629', '2320100', 'Utang Bank Jangka Panjang Yang Jatuh Tempo', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000630', '2410100', 'PPN Keluaran - Terutang', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000631', '2410200', 'PPH 4(2) - Terutang', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000632', '2410400', 'PPh 21 - Terutang', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000633', '2410500', 'PPh 23 - Terutang', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000634', '2410600', 'PPh 25 - Terutang', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000635', '2410700', 'PPh 26 - Terutang', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000636', '2410800', 'PPh 29 - Terutang', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000637', '2420000', 'Kewajiban Pajak Tangguhan ', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000638', '2510100', 'BYMH - Asuransi Karyawan', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000639', '2510200', 'BYMH - Bunga', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000640', '2510400', 'BYMH - Fee Manajemen', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000642', '2610000', 'Penerimaan YMH', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000643', '2620100.0001', 'Uang Muka Penjualan Pihak Ketiga', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000644', '2620190', 'Penerimaan Yang Belum Diketahui', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000645', '2620200', 'Uang Muka Penjualan Pihak Berelasi', 'anak', 'aktif', 'P000010', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000646', '7210100', 'Untung/Rugi atas Penjualan Aset', 'anak', 'aktif', 'P000011', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000647', '7110120', 'Pendapatan Lain - Pendapatan Jasa Giro', 'anak', 'aktif', 'P000012', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000648', '7110140', 'Pendapatan Lain - Pendapatan Bunga', 'anak', 'aktif', 'P000012', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000649', '7790400', 'Pendapatan Lain - Pendapatan Lainnya', 'anak', 'aktif', 'P000012', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000650', '3110200', 'Penjualan', 'anak', 'aktif', 'P000013', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000651', '3210200', 'Retur Penjualan', 'anak', 'aktif', 'P000014', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000003', '1120041', 'Bank CIMB NIAGA', 'anak', 'aktif', 'P000001', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000415', '6121411.0004', 'B. Administrasi Umum - Depresiasi Apartemen Bekasi Ex Kim', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000028', '1520040', 'Pajak Dibayar Dimuka - PPh 25', 'anak', 'aktif', 'P000001', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000212', '1702500.0001', 'Aset Tetap -  Bangunan Ruko ITC Baranangsiang Blok F (1)', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000219', '1704500.0002', 'Aset Tetap -  Kendaraan Kijang Inova Diesel 2014', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000290', '1705000.0070', 'Aset Tetap -  Lemari Rak Arsip', 'anak', 'aktif', 'P000002', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000413', '6115140', 'B. Penjualan - Pengepakan', 'anak', 'aktif', 'P000006', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000006', '1120061', 'Bank Nusantara Parahyangan - 00000100772', 'anak', 'aktif', 'P000001', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000419', '6121413.0002', 'B. Administrasi Umum - Depresiasi Kijang Inova Diesel 2014', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000418', '6121413.0001', 'B. Administrasi Umum - Depresiasi Motor Vario', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000029', '1520045', 'Pajak Dibayar Dimuka - PPh 29', 'anak', 'aktif', 'P000003', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000125', '1805000.0006', 'Ak.Depresiasi - Laci Sorong Hp I Mb 135', 'anak', 'aktif', 'P000003', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000641', '2519990.0001', 'BYMH - Lainnya', 'anak', 'aktif', 'P000010', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000412', '6121411.0001', 'B. Administrasi Umum - Depresiasi Ruko ITC Baranangsiang Blok F (1)', 'anak', 'aktif', 'P000004', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000443', '6121414.0001', 'B. Administrasi Umum - Depresiasi AC Aolia', 'anak', 'aktif', 'P000004', NULL);
INSERT INTO "public"."tbl_coa" VALUES ('P000502', '6121414.0005', 'B. Administrasi Umum - Depresiasi Kursi Manager Hp 62', 'anak', 'aktif', 'P000004', NULL);

-- ----------------------------
-- Table structure for tbl_coa_saldo_awal
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_coa_saldo_awal";
CREATE TABLE "public"."tbl_coa_saldo_awal" (
  "IDCOASaldoAwal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDCOASaldoAwal_seq"'::regclass),
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDCoa_seq"'::regclass),
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDGroupCOA_seq"'::regclass),
  "Nilai_Saldo_Awal" float8,
  "Aktif" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_coa_saldo_awal
-- ----------------------------
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000001', 'P000001', 'P000001', 1473850, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000002', 'P000002', 'P000001', 32676800, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000003', 'P000027', 'P000001', 19173000, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000004', 'P000032', 'P000001', 21737864, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000005', 'P000005', 'P000001', 84715599, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000006', 'P000617', 'P000007', 5500000000, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000007', 'P000630', 'P000010', 56942711, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000008', 'P000632', 'P000010', 16620823, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P000009', 'P000633', 'P000010', 120000, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P00000a', 'P000640', 'P000010', 3240000, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P00000b', 'P000028', 'P000001', 439829449, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P00000c', 'P000641', 'P000010', 1365001, 'aktif');
INSERT INTO "public"."tbl_coa_saldo_awal" VALUES ('P00000d', 'P000006', 'P000001', 61142281, 'aktif');

-- ----------------------------
-- Table structure for tbl_corak
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_corak";
CREATE TABLE "public"."tbl_corak" (
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_corak_IDCorak_seq"'::regclass),
  "Kode_Corak" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Corak" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_corak_IDMerk_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_corak
-- ----------------------------
INSERT INTO "public"."tbl_corak" VALUES ('P000007', '03422', '03422', 'aktif', 'Prk0007');
INSERT INTO "public"."tbl_corak" VALUES ('P000008', '03425', '03425', 'aktif', 'Prk0008');
INSERT INTO "public"."tbl_corak" VALUES ('P000009', '03643', '03643', 'aktif', 'Prk0009');
INSERT INTO "public"."tbl_corak" VALUES ('P000010', '03202', '03202', 'aktif', 'Prk0010');
INSERT INTO "public"."tbl_corak" VALUES ('P000011', '93609', '93609', 'aktif', 'Prk0011');
INSERT INTO "public"."tbl_corak" VALUES ('P000012', '26297', '26297', 'aktif', 'Prk0012');
INSERT INTO "public"."tbl_corak" VALUES ('P000013', '21472', '21472', 'aktif', 'Prk0013');
INSERT INTO "public"."tbl_corak" VALUES ('P000014', '21453', '21453', 'aktif', 'Prk0014');
INSERT INTO "public"."tbl_corak" VALUES ('P000015', '26427', '26427', 'aktif', 'Prk0015');
INSERT INTO "public"."tbl_corak" VALUES ('P000016', '21455', '21455', 'aktif', 'Prk0016');
INSERT INTO "public"."tbl_corak" VALUES ('P000017', '21526', '21526', 'aktif', 'Prk0017');
INSERT INTO "public"."tbl_corak" VALUES ('P000018', '77308', '77308', 'aktif', 'Prk0018');
INSERT INTO "public"."tbl_corak" VALUES ('P000019', '70604', '70604', 'aktif', 'Prk0019');
INSERT INTO "public"."tbl_corak" VALUES ('P000020', '88210', '88210', 'aktif', 'Prk0020');
INSERT INTO "public"."tbl_corak" VALUES ('P000021', '88280', '88280', 'aktif', 'Prk0021');
INSERT INTO "public"."tbl_corak" VALUES ('P000022', '68345', '68345', 'aktif', 'Prk0022');
INSERT INTO "public"."tbl_corak" VALUES ('P000023', '11431', '11431', 'aktif', 'Prk0023');
INSERT INTO "public"."tbl_corak" VALUES ('P000024', '61288', '61288', 'aktif', 'Prk0024');
INSERT INTO "public"."tbl_corak" VALUES ('P000025', '93067', '93067', 'aktif', 'Prk0025');
INSERT INTO "public"."tbl_corak" VALUES ('P000001', '03131', '03131', 'aktif', 'Prk0001');
INSERT INTO "public"."tbl_corak" VALUES ('P000002', '03128', '03128', 'aktif', 'Prk0003');
INSERT INTO "public"."tbl_corak" VALUES ('P000003', '26443', '26443', 'aktif', 'Prk0012');
INSERT INTO "public"."tbl_corak" VALUES ('P000004', '93055', '93055', 'aktif', 'Prk0004');
INSERT INTO "public"."tbl_corak" VALUES ('P000005', '03430', '03430', 'aktif', 'Prk0005');
INSERT INTO "public"."tbl_corak" VALUES ('P000026', '03424', '03424', 'aktif', 'Prk0006');
INSERT INTO "public"."tbl_corak" VALUES ('P000027', '93063', '93063', 'aktif', 'Prk0002');

-- ----------------------------
-- Table structure for tbl_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_customer";
CREATE TABLE "public"."tbl_customer" (
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_customer_IDCustomer_seq"'::regclass),
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_customer_IDGroupCustomer_seq"'::regclass),
  "Kode_Customer" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default",
  "No_Telpon" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Fax" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_KTP" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_customer
-- ----------------------------
INSERT INTO "public"."tbl_customer" VALUES ('P100003', 'P000001', 'CRLT03', 'PT. Cakra Kencana', 'Jl. Mangga Dua Raya Blok C 6/12 Jl. Arteri Mangga Dua Raya  ', '-', 'P000010', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100004', 'P000001', 'CRLT04', 'PT. Mido Indonesia', 'Jl. Abdul Wahab Rt 001/Rw 008 No. 38 Kedaung Sawangan Depok', '-', 'P000010', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100005', 'P000001', 'CRLT05', 'PT. Permata Busana Mas', 'Jl. Mangga Dua Raya Blok D2/No.22', '-', 'P000010', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100006', 'P000001', 'CRLT06', 'PT. Savana Lestari', 'Jl. Mangga Dua Raya Blok C1/17 ', '-', 'P000010', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100007', 'P000001', 'CRLT07', 'PT. Sinar Abadi Citranusa', 'Jl. Mangga Dua Raya Blok C2/No.31', '-', 'P000010', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100008', 'P000001', 'CRLT08', 'PT. Tricitra Busana Mas', 'Ruko Textile Mangga Dua Blok C2/6 Jl. Arteri Mangga Dua Raya', '-', 'P000010', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100010', 'P000002', 'C3RD01', 'Agung Jaya', 'Jl. Dewi Sartika 26 - 27 Ciputat', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100011', 'P000002', 'C3RD02', 'Aneka Textile', 'Tembusan R.S. Akademis Kav. 8-9', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100012', 'P000002', 'C3RD03', 'Azalea', 'Pertokoan Bongkaran Mega No. 24', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100013', 'P000002', 'C3RD04', 'Bpk. Chris Permana', 'Taman Kopo Indah I Blok D-100', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100014', 'P000002', 'C3RD05', 'Bpk. Erwin', 'Taman Rahayu Iii C2 No. 9', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100015', 'P000002', 'C3RD06', 'Bpk. Herbin', 'Jl. Bukit Barisan Dalam No. 20', '021-6015219', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100016', 'P000002', 'C3RD07', 'Bpk. Irzal', 'Ruko Itc Baranang Siang Blok F4 Kosambi', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100018', 'P000002', 'C3RD09', 'Bpk. Untung', 'Jl. Gempol Sari Komplek Melong Asri No. 33 Rt.008/Rw.028 Kel. Melong, Kec. Cimahi Selatan - Bandung', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100019', 'P000002', 'C3RD10', 'Cahaya Mas, Medan', 'Jl. Kumango No. 60', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100020', 'P000002', 'C3RD11', 'Cahaya Mas, Surabaya', 'Jl. Slompretan No. 82 ', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100021', 'P000002', 'C3RD12', 'Cahaya Putri', 'Jl. Pasar Senen Blok 1 Lt 2 No. 84 B Proyek Senen', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100022', 'P000002', 'C3RD13', 'Cahaya Textile', 'Jl. Pengadilan (Ruko Pengadilan) No. 3F-G', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100023', 'P000002', 'C3RD14', 'Cempaka, Surabaya', 'Pertokoan Bongkaran Mega No. 24', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100024', 'P000002', 'C3RD15', 'Ci Wawa', 'Jl. Jamika ', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100025', 'P000002', 'C3RD16', 'Citra Busana Mas', 'Jl. Slompretan No. 84/Iii', '021-4210044', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100026', 'P000002', 'C3RD17', 'CV. Amarta Wisesa', 'Jl. Besar Ijen No. 81', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100027', 'P000002', 'C3RD18', 'CV. Bahagia Baru', 'Jl. Wot Gandul Dalam No. 121', '061 - 88815887', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100028', 'P000002', 'C3RD19', 'CV. Bayu Mandiri', 'Jl. Reog Iii No. 7', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100029', 'P000002', 'C3RD20', 'CV. Melati', 'Jl. Gang Warung No. 43', '021-8510232', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100030', 'P000002', 'C3RD21', 'CV. Mentari Bunga', '-', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100031', 'P000002', 'C3RD22', 'CV. Rapih Jaya', 'Jl. Bkr No. 99 Lingkar Selatan', '022-7800605', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100032', 'P000002', 'C3RD23', 'CV. Remaja', 'Jl. Raya Rangunan No. 44 Pasar Minggu', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100033', 'P000002', 'C3RD24', 'Eka Jaya', 'Jl. Tengkuruk Permai No. 42-43', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100034', 'P000002', 'C3RD25', 'Family Textile', '-', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100036', 'P000002', 'C3RD27', 'Hari-Hari', 'Jl.Ir. H. Juanda Pertokoan Bekasi Blok 2 No. 31-32', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100037', 'P000002', 'C3RD28', 'Ibu. Ay Lan', 'Kopo Mas Blok G No. 25', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100038', 'P000002', 'C3RD29', 'Ikobana', 'Jl. Tebah Iii No. 25 Jakarta Selatan', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100039', 'P000002', 'C3RD30', 'Indra Makmur', 'Jl. Kaka Tua Blok Y No. 7 Cipinang Indah Ii Jakarta Timur', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100040', 'P000002', 'C3RD31', 'Jakartatex', 'Jl. Perniagaan Baru No. 36 C (Kesawan) Medan', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100041', 'P000002', 'C3RD32', 'Luhur Makmur', 'Jl. Merdeka Gg. Areng No.2', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100042', 'P000002', 'C3RD33', 'CV. Mentari Jaya', 'Jl. Tamim N0. 5 ', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100043', 'P000002', 'C3RD34', 'Michael Lesmana Salim', '-', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100045', 'P000002', 'C3RD36', 'New Gunung Langit', 'Jl. Sam Ratulangi No. 24-28', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100046', 'P000002', 'C3RD37', 'New Ratu', 'Jl. Pintu Kecil No. 57 E', '022-4222065', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100048', 'P000002', 'C3RD39', 'PT. Bina Busana Internusa', 'Jl. Pulo Buaran Ii Blok Q No. 1 Kawasan Industri Pulogadung', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100049', 'P000002', 'C3RD40', 'PT. Dekatama Centra', 'Jl. Mekar Mulya No. 33 Gedebage ', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100050', 'P000002', 'C3RD41', 'PT. Derifis Utama Dicipta', 'Gedung Pembina Graha Lt. 1 R 8A, Jl. D.I Panjaitan No. 45', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100051', 'P000002', 'C3RD42', 'PT. Gracia Multi Moda', 'Jl. Bukit Barisan Dalam No. 20', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100052', 'P000002', 'C3RD43', 'PT. Inkabiz Indonesia', 'Jl. Tarumanegara No. 47 Pisangan Ciputat', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100053', 'P000002', 'C3RD44', 'PT. Just Jait Indonesia', 'Komp. Batununggal Indah I No. 12', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100054', 'P000002', 'C3RD45', 'PT. Kharisma Cemerlang', 'Ruko Pengampon Square Blok G No. 3 Jl. Semut Baru ', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100055', 'P000002', 'C3RD46', 'PT. Lestari Sentosa', 'Jl. Tanjun Duren Raya No. 119 A', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100056', 'P000002', 'C3RD47', 'PT. Multi Garmen Jaya', 'Jl. Moch. Toha No. 215 Km 7,3', '022-4238165', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100057', 'P000002', 'C3RD48', 'PT. Pesona Sinjang Kencana', 'Jl. Bumi No. 52-54 Kebayoran Baru', '021-30052568', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100002', 'P000001', 'CRLT02', 'PT. Bintang Cipta Sejahtera', 'Ruko Pengampon Square Blok G No. 3 Jl. Semut Baru Bongkaran Pabean Cantikan', '-', 'P000022', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100009', 'P000001', 'CRLT09', 'PT. Trisula Textile Industries Tbk.', 'Jl. Mahar Martanegara No. 170, Kota Cimahi', '022-6613333', 'P000006', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100075', 'P000002', 'C3RD66', 'TK. Nata 23', 'Jl. Abu Hasan No. 34', '022-4209052', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100076', 'P000002', 'C3RD67', 'TK. PG', 'Jl. Mangga Dua Blok C2 No. 37', '(022)-86065356 ', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100077', 'P000002', 'C3RD68', 'TK. Selamat Abadi', 'Jl. Jend. Sudirman No. 20', '022-5200433', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100078', 'P000002', 'C3RD69', 'UD. Busana Mas', 'Jl. Slompretan No. 98', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100079', 'P000002', 'C3RD70', 'UD. Cempaka Semarang', 'Jl. Gang Warung No. 58', '08158717829', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100080', 'P000002', 'C3RD71', 'Bpk. Wahyu Utomo', 'Ruko Textile Mangga Dua Blok D-2 No. 26-27', '0254-200922', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100081', 'P000002', 'C3RD72', 'Plara Textile', 'Jailolo', '0000', 'P00002b', '0000', 'plara@gmail.com', '0000', '0000', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100082', 'P000002', 'C3RD73', 'Bpk. San San', 'Komp. Gempol Asri Raya 1 No. 122, Rt/Rw 004/010, Kel. Gempolsari', '0000', 'P000001', '0000', 'sansan@gmail.com', '000', '000', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100083', 'P000002', 'C3RD74', 'Ibu Ida', 'xxxx', '000', 'P000001', '000', 'ida@gmail.com', '000', '000', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100084', 'P000002', 'C3RD75', 'CV. Ashta Asiti', 'Gg. Tower, Pasir Biru, Cibiru, Kota Bandung, Jawa Barat 40615', '000', 'P000001', '000', 'ashta@x.com', '000', '000', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100001', 'P000001', 'CRLT01', 'PT. Bina Citra Sentosa', 'Jl. Kapten Pier Tandean No. 14', '-', 'P000024', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100017', 'P000002', 'C3RD08', 'Bpk. Ng Bak Hong', 'Jl. Cileungsir No. 8 Rt/Rw 011/01, Cideng Gambir', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100035', 'P000002', 'C3RD26', 'PT. Golden Flower', 'Jl. Karimun Jawa Desa Gedang Anak', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100044', 'P000002', 'C3RD35', 'Modera Utama', 'Ruko Itc Baranang Siang Blok E 9 Kosambi', '021-5521440', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100047', 'P000002', 'C3RD38', 'PT. Axa Setara Apparel', 'Ruko Prominence 38 G / 45 Alam Sutra Panunggangan Timur Pinang ', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100058', 'P000002', 'C3RD49', 'PT. Samudra Lautan Biru', 'Sedayu Square Blok E/23-25, Ringroad Cengkareng, Depan Taman Kencana', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100059', 'P000002', 'C3RD50', 'PT. Teratai Widjaya', 'Jl. Raya Simpang Tiga Karadenan No. 35 Cibinong', '021-22065370', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100060', 'P000002', 'C3RD51', 'PT. Trisco Tailored Apparel Manufacturing', 'Jl. Raya Kopo Soreang Km. 11,5 Cilampeni Katapang', '022-6040287', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100061', 'P000002', 'C3RD52', 'PT. Unimax Cipta Busana', 'Jl. Sunter Agung Barat 1 Blok A3 No. 23', '0741-23323', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100062', 'P000002', 'C3RD53', 'Remaja', '-', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100063', 'P000002', 'C3RD54', 'PT. Restu Ibu Mandiri', 'Jl. Indrayasa No. 124, Ruko Singgasana Pradan', '061 - 88815887', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100064', 'P000002', 'C3RD55', 'Samudra Lampung', '-', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100065', 'P000002', 'C3RD56', 'Sandang Sari', 'Jl. Jendr. Soeprapto No. 78', '022-95056000/7502022', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100066', 'P000002', 'C3RD57', 'TK. Sasami', 'Jl. Dr. Rajiman No. 165', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100067', 'P000002', 'C3RD58', 'PD. Slamet Lestari', 'Jl. Dulatif No. 68 B', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100068', 'P000002', 'C3RD59', 'Sinar Abadi', 'Jl. Lmu Nurtanio No. 57 A ', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100069', 'P000002', 'C3RD60', 'PD. Sinar Raya', 'Jl. Ahmad Yani Iii No. 28', '021-7251705', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100070', 'P000002', 'C3RD61', 'Sukses Mandiri', 'Jl. Merdeka Gg. Areng No. 02', '022-5892003', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100071', 'P000002', 'C3RD62', 'Sumatra Jaya', 'Jl. Mr. Asaad No. 23', '022 - 7833250', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100072', 'P000002', 'C3RD63', 'Tetap Jaya', 'Jl. Kumango No. 60', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100073', 'P000002', 'C3RD64', 'TK. Cahaya Jakarta', 'Blok 3 Lantai Dasar No. 65-68 (Penampungan)', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P100074', 'P000002', 'C3RD65', 'TK. Jaya Makmur', '-', '-', 'P00002a', '-', 'Example@example.com', '-', '-', 'aktif');

-- ----------------------------
-- Table structure for tbl_down_payment
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_down_payment";
CREATE TABLE "public"."tbl_down_payment" (
  "IDDownPayment" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "NoDP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalDP" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Aktif" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_giro
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_giro";
CREATE TABLE "public"."tbl_giro" (
  "IDGiro" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDGiro_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDFaktur_seq"'::regclass),
  "IDPerusahaan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDPerusahaan_seq"'::regclass),
  "Jenis_Faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_Faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBank" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_Giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_Faktur" date,
  "Tanggal_Cair" date,
  "Nilai" int8,
  "Status_Giro" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_Giro" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_group_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_asset";
CREATE TABLE "public"."tbl_group_asset" (
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_asset_IDGroupAsset_seq"'::regclass),
  "Kode_Group_Asset" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Asset" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tarif_Penyusutan" float8,
  "Umur" int2
)
;

-- ----------------------------
-- Records of tbl_group_asset
-- ----------------------------
INSERT INTO "public"."tbl_group_asset" VALUES ('P000003', 'KND', 'Kendaraan', 'aktif', 0.5, 4);
INSERT INTO "public"."tbl_group_asset" VALUES ('P000004', 'BGN', 'Bangunan', 'aktif', 0.05, 20);
INSERT INTO "public"."tbl_group_asset" VALUES ('P000005', 'FUR', 'Peralatan & Inventaris', 'aktif', 0.5, 4);
INSERT INTO "public"."tbl_group_asset" VALUES ('P000006', 'TNH', 'Tanah', 'aktif', 0, 20);
INSERT INTO "public"."tbl_group_asset" VALUES ('P000007', 'MSN', 'Mesin', 'aktif', 0, 8);

-- ----------------------------
-- Table structure for tbl_group_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_barang";
CREATE TABLE "public"."tbl_group_barang" (
  "IDGroupBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_barang_IDGroupBarang_seq"'::regclass),
  "Kode_Group_Barang" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Barang" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_barang
-- ----------------------------
INSERT INTO "public"."tbl_group_barang" VALUES ('GB00002', 'seragam', 'seragam', 'aktif');
INSERT INTO "public"."tbl_group_barang" VALUES ('GB00001', 'K', 'Kain', 'aktif');

-- ----------------------------
-- Table structure for tbl_group_coa
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_coa";
CREATE TABLE "public"."tbl_group_coa" (
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_coa_IDGroupCOA_seq"'::regclass),
  "Nama_Group" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Normal_Balance" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kode_Group_COA" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_coa
-- ----------------------------
INSERT INTO "public"."tbl_group_coa" VALUES ('P000010', 'KEWAJIBAN JANGKA PENDEK', 'Kredit', 'Aktif', 'KEWAJIBAN JANGKA PENDEK');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000008', 'BEBAN POKOK PENJUALAN', 'Debit', 'Aktif', 'HPP');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000015', '-', 'debet', 'aktif', '-');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000001', 'ASET LANCAR', 'Debit', 'Aktif', 'ASET LANCAR');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000002', 'ASET TIDAK LANCAR 1', 'Debit', 'Aktif', 'ASET TIDAK LANCAR 1');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000003', 'ASET TIDAK LANCAR 2', 'Kredit', 'Aktif', 'ASET TIDAK LANCAR 2');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000004', 'BEBAN ADMINISTRASI UMUM', 'Debit', 'Aktif', 'BEBAN ADMINISTRASI UMUM');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000005', 'BEBAN LAINNYA', 'Debit', 'Aktif', 'BEBAN LAINNYA');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000006', 'BEBAN PENJUALAN', 'Debit', 'Aktif', 'BEBAN PENJUALAN');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000007', 'EKUITAS', 'Kredit', 'Aktif', 'EKUITAS');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000009', 'KEWAJIBAN JANGKA PANJANG', 'Kredit', 'Aktif', 'KEWAJIBAN JANGKA PANJANG');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000011', 'PENDAPATAN (BEBAN) LAINNYA', 'Debit', 'Aktif', 'PENDAPATAN (BEBAN) LAINNYA');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000012', 'PENDAPATAN LAINNYA', 'Kredit', 'Aktif', 'PENDAPATAN LAINNYA');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000013', 'PENJUALAN', 'Kredit', 'Aktif', 'PENJUALAN');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000014', 'RETUR PENJUALAN', 'Debit', 'Aktif', 'RETUR PENJUALAN');

-- ----------------------------
-- Table structure for tbl_group_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_customer";
CREATE TABLE "public"."tbl_group_customer" (
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_customer_IDGroupCustomer_seq"'::regclass),
  "Kode_Group_Customer" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Group_Customer" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_customer
-- ----------------------------
INSERT INTO "public"."tbl_group_customer" VALUES ('P000001', 'CRLT', 'PIHAK BERELASI', 'aktif');
INSERT INTO "public"."tbl_group_customer" VALUES ('P000002', 'C3RD', 'PIHAK KETIGA', 'aktif');

-- ----------------------------
-- Table structure for tbl_group_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_supplier";
CREATE TABLE "public"."tbl_group_supplier" (
  "IDGroupSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_supplier_IDGroupSupplier_seq"'::regclass),
  "Kode_Group_Supplier" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Supplier" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_supplier
-- ----------------------------
INSERT INTO "public"."tbl_group_supplier" VALUES ('P000001', 'SRLT', 'PIHAK BERELASI', 'aktif');
INSERT INTO "public"."tbl_group_supplier" VALUES ('P000002', 'S3RD', 'PIHAK KETIGA', 'aktif');

-- ----------------------------
-- Table structure for tbl_group_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_user";
CREATE TABLE "public"."tbl_group_user" (
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_group_user_IDGroupUser_seq"'::regclass),
  "Kode_Group_User" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_User" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_user
-- ----------------------------
INSERT INTO "public"."tbl_group_user" VALUES (5, '123', 'admin');

-- ----------------------------
-- Table structure for tbl_gudang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_gudang";
CREATE TABLE "public"."tbl_gudang" (
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_gudang_IDGudang_seq"'::regclass),
  "Kode_Gudang" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Gudang" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_gudang
-- ----------------------------
INSERT INTO "public"."tbl_gudang" VALUES ('P000001', 'GDTTI', 'Gudang Pabrik', 'Aktif');
INSERT INTO "public"."tbl_gudang" VALUES ('P000002', 'GDPMK', 'Gudang PMK', 'Aktif');

-- ----------------------------
-- Table structure for tbl_harga_jual_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_harga_jual_barang";
CREATE TABLE "public"."tbl_harga_jual_barang" (
  "IDHargaJual" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDHargaJual_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDBarang_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDSatuan_seq"'::regclass),
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDGroupCustomer_seq"'::regclass),
  "Modal" numeric(100) DEFAULT NULL::numeric,
  "Harga_Jual" numeric(100) DEFAULT NULL::numeric
)
;

-- ----------------------------
-- Records of tbl_harga_jual_barang
-- ----------------------------
INSERT INTO "public"."tbl_harga_jual_barang" VALUES ('P000001', 'P00000e', 'P000004', 'P000002', 0, 35454);

-- ----------------------------
-- Table structure for tbl_hutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_hutang";
CREATE TABLE "public"."tbl_hutang" (
  "Tanggal_Hutang" date,
  "Jatuh_Tempo" date,
  "No_Faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDSupplier_seq"'::regclass),
  "Jenis_Faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_Hutang" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Awal" numeric(100) DEFAULT NULL::numeric,
  "Pembayaran" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Akhir" numeric(100) DEFAULT NULL::numeric,
  "IDHutang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDHutang_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDFaktur_seq"'::regclass),
  "UM" float8 DEFAULT 0,
  "DISC" float8 DEFAULT 0,
  "Retur" float8 DEFAULT 0,
  "Selisih" float8 DEFAULT 0,
  "Kontra_bon" float8 DEFAULT 0,
  "Saldo_kontra_bon" float8 DEFAULT 0
)
;

-- ----------------------------
-- Records of tbl_hutang
-- ----------------------------
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-15', '2019-03-31', '19000071', 'P000007', 'FA', 13632500, 13632500, 0, 13632500, 'P000001', 'P000001', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000281', 'P000004', 'FA', 73512602, 73512602, 0, 73512602, 'P00000x', 'P00000x', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-15', '2019-03-31', '19000073', 'P000007', 'FA', 1818006, 1818006, 0, 1818006, 'P000002', 'P000002', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-08', '2019-03-24', 'SI/19/00031', 'P000003', 'FA', 2299000, 2299000, 0, 2299000, 'P000003', 'P000003', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-07', '2019-04-23', 'SI/19/00197', 'P000003', 'FA', 21660000, 21660000, 0, 21660000, 'P000004', 'P000004', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-08', '2019-04-24', 'SI/19/00203', 'P000003', 'FA', 23427000, 23427000, 0, 23427000, 'P000005', 'P000005', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-07', '2019-04-23', 'SI/19/00261', 'P000003', 'FA', 13547000, 13547000, 0, 13547000, 'P000006', 'P000006', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-11', '2019-04-27', 'SI/19/00263', 'P000003', 'FA', 7600000, 7600000, 0, 7600000, 'P000007', 'P000007', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-18', '2019-05-04', 'SI/19/00262', 'P000003', 'FA', 21261000, 21261000, 0, 21261000, 'P000008', 'P000008', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', 'SI/19/00290', 'P000003', 'FA', 19988000, 19988000, 0, 19988000, 'P000009', 'P000009', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', 'SI/19/00290', 'P000003', 'FA', 21071000, 21071000, 0, 21071000, 'P00000a', 'P00000a', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-28', '2019-05-14', 'SI/19/00301', 'P000003', 'FA', 20805000, 20805000, 0, 20805000, 'P00000b', 'P00000b', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-28', '2019-05-14', 'SI/19/00301', 'P000003', 'FA', 20805000, 20805000, 0, 20805000, 'P00000c', 'P00000c', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-28', '2019-05-14', 'SI/19/00301', 'P000003', 'FA', 42066001, 42066001, 0, 42066001, 'P00000d', 'P00000d', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-17', '2019-03-02', '901000225', 'P000004', 'FA', 1409485, 1409485, 0, 1409485, 'P00000e', 'P00000e', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-17', '2019-03-02', '901000231', 'P000004', 'FA', 20822357, 20822357, 0, 20822357, 'P00000f', 'P00000f', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-17', '2019-03-02', '901000232', 'P000004', 'FA', 18535473, 18535473, 0, 18535473, 'P00000g', 'P00000g', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-17', '2019-03-02', '901000237', 'P000004', 'FA', 42370020, 42370020, 0, 42370020, 'P00000h', 'P00000h', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-17', '2019-03-02', '901000272', 'P000004', 'FA', 3762000, 3762000, 0, 3762000, 'P00000i', 'P00000i', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-17', '2019-03-02', '901000373', 'P000004', 'FA', 95113744, 95113744, 0, 95113744, 'P00000j', 'P00000j', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-18', '2019-03-03', '901000216', 'P000004', 'FA', 22210650, 22210650, 0, 22210650, 'P00000k', 'P00000k', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-20', '2019-03-05', '901000381', 'P000004', 'FA', 37025184, 37025184, 0, 37025184, 'P00000l', 'P00000l', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000284', 'P000004', 'FA', 19875570, 19875570, 0, 19875570, 'P00000y', 'P00000y', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-22', '2019-03-07', '901000235', 'P000004', 'FA', 1611500, 1611500, 0, 1611500, 'P00000m', 'P00000m', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-26', '2019-03-11', '901000027', 'P000004', 'FA', 17414331, 17414331, 0, 17414331, 'P00000n', 'P00000n', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-26', '2019-03-11', '901000265', 'P000004', 'FA', 106937530, 106937530, 0, 106937530, 'P00000o', 'P00000o', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-26', '2019-03-11', '901000266', 'P000004', 'FA', 19077834, 19077834, 0, 19077834, 'P00000p', 'P00000p', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-26', '2019-03-11', '901000267', 'P000004', 'FA', 125816328, 125816328, 0, 125816328, 'P00000q', 'P00000q', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-26', '2019-03-11', '901000274', 'P000004', 'FA', 586575, 586575, 0, 586575, 'P00000r', 'P00000r', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-26', '2019-03-11', '901000379', 'P000004', 'FA', 18006890, 18006890, 0, 18006890, 'P00000s', 'P00000s', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-26', '2019-03-11', '901000388', 'P000004', 'FA', 82343408, 82343408, 0, 82343408, 'P00000t', 'P00000t', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-26', '2019-03-11', '901000389', 'P000004', 'FA', 1694000, 1694000, 0, 1694000, 'P00000u', 'P00000u', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000279', 'P000004', 'FA', 126514179, 126514179, 0, 126514179, 'P00000v', 'P00000v', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000280', 'P000004', 'FA', 18558034, 18558034, 0, 18558034, 'P00000w', 'P00000w', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000285', 'P000004', 'FA', 23766930, 23766930, 0, 23766930, 'P00000z', 'P00000z', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000286', 'P000004', 'FA', 72246769, 72246769, 0, 72246769, 'P000010', 'P000010', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000287', 'P000004', 'FA', 18935931, 18935931, 0, 18935931, 'P000011', 'P000011', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000302', 'P000004', 'FA', 33707520, 33707520, 0, 33707520, 'P000012', 'P000012', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000421', 'P000004', 'FA', 69294166, 69294166, 0, 69294166, 'P000013', 'P000013', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000494', 'P000004', 'FA', 56649072, 56649072, 0, 56649072, 'P000014', 'P000014', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000495', 'P000004', 'FA', 93052080, 93052080, 0, 93052080, 'P000015', 'P000015', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000496', 'P000004', 'FA', 76997435, 76997435, 0, 76997435, 'P000016', 'P000016', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000497', 'P000004', 'FA', 100177287, 100177287, 0, 100177287, 'P000017', 'P000017', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000498', 'P000004', 'FA', 35978349, 35978349, 0, 35978349, 'P000018', 'P000018', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000499', 'P000004', 'FA', 1579270, 1579270, 0, 1579270, 'P000019', 'P000019', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000500', 'P000004', 'FA', 70041510, 70041510, 0, 70041510, 'P00001a', 'P00001a', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000502', 'P000004', 'FA', 18632163, 18632163, 0, 18632163, 'P00001b', 'P00001b', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000503', 'P000004', 'FA', 76256181, 76256181, 0, 76256181, 'P00001c', 'P00001c', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000504', 'P000004', 'FA', 79009431, 79009431, 0, 79009431, 'P00001d', 'P00001d', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000512', 'P000004', 'FA', 712800, 712800, 0, 712800, 'P00001e', 'P00001e', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000515', 'P000004', 'FA', 418000, 418000, 0, 418000, 'P00001f', 'P00001f', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000501', 'P000004', 'FA', 26025801, 26025801, 0, 26025801, 'P00001g', 'P00001g', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-29', '2019-03-14', '901000613', 'P000004', 'FA', 70756868, 70756868, 0, 70756868, 'P00001h', 'P00001h', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-02', '2019-03-18', '901000646', 'P000004', 'FA', 35833050, 35833050, 0, 35833050, 'P00001i', 'P00001i', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-04', '2019-03-20', '901000707', 'P000004', 'FA', 35736197, 35736197, 0, 35736197, 'P00001j', 'P00001j', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-09', '2019-03-25', '901000870', 'P000004', 'FA', 15357595, 15357595, 0, 15357595, 'P00001k', 'P00001k', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-10', '2019-03-26', '901000645', 'P000004', 'FA', 37096730, 37096730, 0, 37096730, 'P00001l', 'P00001l', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-10', '2019-03-26', '901000770', 'P000004', 'FA', 19144125, 19144125, 0, 19144125, 'P00001m', 'P00001m', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-15', '2019-03-31', '901000643', 'P000004', 'FA', 19517025, 19517025, 0, 19517025, 'P00001n', 'P00001n', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-15', '2019-03-31', '901000697', 'P000004', 'FA', 45389898, 45389898, 0, 45389898, 'P00001o', 'P00001o', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-15', '2019-03-31', '901000768', 'P000004', 'FA', 19127763, 19127763, 0, 19127763, 'P00001p', 'P00001p', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-15', '2019-03-31', '901000853', 'P000004', 'FA', 40595074, 40595074, 0, 40595074, 'P00001q', 'P00001q', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-16', '2019-04-01', '901000866', 'P000004', 'FA', 39213075, 39213075, 0, 39213075, 'P00001r', 'P00001r', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-17', '2019-04-02', '901000867', 'P000004', 'FA', 34469875, 34469875, 0, 34469875, 'P00001s', 'P00001s', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-17', '2019-04-02', '901000868', 'P000004', 'FA', 1395900, 1395900, 0, 1395900, 'P00001t', 'P00001t', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-17', '2019-04-02', '901000869', 'P000004', 'FA', 19946520, 19946520, 0, 19946520, 'P00001u', 'P00001u', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-17', '2019-04-02', '901000873', 'P000004', 'FA', 23475870, 23475870, 0, 23475870, 'P00001v', 'P00001v', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-17', '2019-04-02', '901000875', 'P000004', 'FA', 19896030, 19896030, 0, 19896030, 'P00001w', 'P00001w', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-17', '2019-04-02', '901001179', 'P000004', 'FA', 41693430, 41693430, 0, 41693430, 'P00001x', 'P00001x', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-19', '2019-04-04', '901000771', 'P000004', 'FA', 37564065, 37564065, 0, 37564065, 'P00001y', 'P00001y', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-22', '2019-04-07', '901000769', 'P000004', 'FA', 34518000, 34518000, 0, 34518000, 'P00001z', 'P00001z', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-22', '2019-04-07', '901000876', 'P000004', 'FA', 99885745, 99885745, 0, 99885745, 'P000020', 'P000020', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-19', '2019-04-07', '901000877', 'P000004', 'FA', 519200, 519200, 0, 519200, 'P000021', 'P000021', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-23', '2019-04-08', '901001035', 'P000004', 'FA', 38272839, 38272839, 0, 38272839, 'P000022', 'P000022', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-25', '2019-04-10', '901000879', 'P000004', 'FA', 22497750, 22497750, 0, 22497750, 'P000023', 'P000023', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-25', '2019-04-10', '901001027', 'P000004', 'FA', 171883644, 171883644, 0, 171883644, 'P000024', 'P000024', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-28', '2019-04-13', '901001028', 'P000004', 'FA', 17854307, 17854307, 0, 17854307, 'P000025', 'P000025', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-28', '2019-04-13', '901001044', 'P000004', 'FA', 96589507, 96589507, 0, 96589507, 'P000026', 'P000026', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-29', '2019-04-14', '901001045', 'P000004', 'FA', 55677325, 55677325, 0, 55677325, 'P000027', 'P000027', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-29', '2019-04-14', '901001048', 'P000004', 'FA', 18893226, 18893226, 0, 18893226, 'P000028', 'P000028', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-20', '2019-04-15', '901001088', 'P000004', 'FA', 73103547, 73103547, 0, 73103547, 'P000029', 'P000029', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001001', 'P000004', 'FA', 19068143, 19068143, 0, 19068143, 'P00002a', 'P00002a', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001002', 'P000004', 'FA', 75263513, 75263513, 0, 75263513, 'P00002b', 'P00002b', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001003', 'P000004', 'FA', 90729658, 90729658, 0, 90729658, 'P00002c', 'P00002c', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001004', 'P000004', 'FA', 17802241, 17802241, 0, 17802241, 'P00002d', 'P00002d', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001005', 'P000004', 'FA', 24096600, 24096600, 0, 24096600, 'P00002e', 'P00002e', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001006', 'P000004', 'FA', 35880024, 35880024, 0, 35880024, 'P00002f', 'P00002f', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001007', 'P000004', 'FA', 56169640, 56169640, 0, 56169640, 'P00002g', 'P00002g', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001011', 'P000004', 'FA', 73959683, 73959683, 0, 73959683, 'P00002h', 'P00002h', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001013', 'P000004', 'FA', 18446841, 18446841, 0, 18446841, 'P00002i', 'P00002i', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001014', 'P000004', 'FA', 9944550, 9944550, 0, 9944550, 'P00002j', 'P00002j', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001020', 'P000008', 'FA', 71918550, 71918550, 0, 71918550, 'P00002k', 'P00002k', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001021', 'P000004', 'FA', 74566523, 74566523, 0, 74566523, 'P00002l', 'P00002l', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001022', 'P000004', 'FA', 23286780, 23286780, 0, 23286780, 'P00002m', 'P00002m', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001058', 'P000004', 'FA', 19703035, 19703035, 0, 19703035, 'P00002n', 'P00002n', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001058', 'P000004', 'FA', 19703035, 19703035, 0, 19703035, 'P00002o', 'P00002o', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001061', 'P000004', 'FA', 40206122, 40206122, 0, 40206122, 'P00002p', 'P00002p', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001067', 'P000004', 'FA', 87477390, 87477390, 0, 87477390, 'P00002q', 'P00002q', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001073', 'P000004', 'FA', 110799810, 110799810, 0, 110799810, 'P00002r', 'P00002r', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001076', 'P000004', 'FA', 38146185, 38146185, 0, 38146185, 'P00002s', 'P00002s', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001078', 'P000004', 'FA', 115361730, 115361730, 0, 115361730, 'P00002t', 'P00002t', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001080', 'P000004', 'FA', 69060915, 69060915, 0, 69060915, 'P00002v', 'P00002v', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001082', 'P000004', 'FA', 36365038, 36365038, 0, 36365038, 'P00002w', 'P00002w', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001084', 'P000004', 'FA', 40787940, 40787940, 0, 40787940, 'P00002x', 'P00002x', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001111', 'P000004', 'FA', 7387875, 7387875, 0, 7387875, 'P00002y', 'P00002y', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001112', 'P000004', 'FA', 65309471, 65309471, 0, 65309471, 'P00002z', 'P00002z', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001113', 'P000004', 'FA', 62764634, 62764634, 0, 62764634, 'P000030', 'P000030', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001119', 'P000004', 'FA', 68373933, 68373933, 0, 68373933, 'P000031', 'P000031', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001121', 'P000004', 'FA', 5806350, 5806350, 0, 5806350, 'P000032', 'P000032', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001122', 'P000004', 'FA', 83297965, 83297965, 0, 83297965, 'P000033', 'P000033', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001123', 'P000004', 'FA', 19466288, 19466288, 0, 19466288, 'P000034', 'P000034', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001125', 'P000004', 'FA', 56334218, 56334218, 0, 56334218, 'P000035', 'P000035', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001134', 'P000004', 'FA', 20413663, 20413663, 0, 20413663, 'P000036', 'P000036', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001156', 'P000004', 'FA', 72793068, 72793068, 0, 72793068, 'P000037', 'P000037', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001019', 'P000004', 'FA', 16688331, 16688331, 0, 16688331, 'P000038', 'P000038', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '901001064', 'P000004', 'FA', 36048549, 36048549, 0, 36048549, 'P000039', 'P000039', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-01', '2019-04-17', '901001200', 'P000004', 'FA', 18099684, 18099684, 0, 18099684, 'P00003a', 'P00003a', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-04', '2019-04-20', '901001199', 'P000004', 'FA', 14811019, 14811019, 0, 14811019, 'P00003b', 'P00003b', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-04', '2019-04-20', '901001201', 'P000004', 'FA', 467775, 467775, 0, 467775, 'P00003c', 'P00003c', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-14', '2019-04-30', '901001295', 'P000004', 'FA', 18754474, 18754474, 0, 18754474, 'P00003d', 'P00003d', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-19', '2019-05-05', '901001327', 'P000004', 'FA', 24454128, 24454128, 0, 24454128, 'P00003e', 'P00003e', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-21', '2019-05-07', '901001428', 'P000004', 'FA', 111879625, 111879625, 0, 111879625, 'P00003f', 'P00003f', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-23', '2019-05-09', '901001408', 'P000004', 'FA', 2677455, 2677455, 0, 2677455, 'P00003g', 'P00003g', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-23', '2019-05-09', '901001409', 'P000004', 'FA', 22466373, 22466373, 0, 22466373, 'P00003h', 'P00003h', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-23', '2019-05-09', '901001412', 'P000004', 'FA', 19618500, 19618500, 0, 19618500, 'P00003i', 'P00003i', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-23', '2019-05-09', '901001466', 'P000004', 'FA', 64096263, 64096263, 0, 64096263, 'P00003j', 'P00003j', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', '901001417', 'P000004', 'FA', 81950308, 81950308, 0, 81950308, 'P00003k', 'P00003k', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', '901001418', 'P000004', 'FA', 21329583, 21329583, 0, 21329583, 'P00003l', 'P00003l', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', '901001446', 'P000004', 'FA', 38881838, 38881838, 0, 38881838, 'P00003m', 'P00003m', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', '901001447', 'P000004', 'FA', 42453957, 42453957, 0, 42453957, 'P00003n', 'P00003n', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', '901001448', 'P000004', 'FA', 17548823, 17548823, 0, 17548823, 'P00003o', 'P00003o', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', '901001449', 'P000004', 'FA', 24706440, 24706440, 0, 24706440, 'P00003p', 'P00003p', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-25', '2019-05-11', '901001451', 'P000004', 'FA', 24383480, 24383480, 0, 24383480, 'P00003q', 'P00003q', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-26', '2019-05-12', '901001486', 'P000004', 'FA', 53928724, 53928724, 0, 53928724, 'P00003r', 'P00003r', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-26', '2019-05-12', '901001487', 'P000004', 'FA', 19381395, 19381395, 0, 19381395, 'P00003s', 'P00003s', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-28', '2019-05-14', '901001572', 'P000004', 'FA', 19029764, 19029764, 0, 19029764, 'P00003t', 'P00003t', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-28', '2019-05-14', '901001573', 'P000004', 'FA', 56275107, 56275107, 0, 56275107, 'P00003u', 'P00003u', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-28', '2019-05-14', '901001590', 'P000004', 'FA', 18089500, 18089500, 0, 18089500, 'P00003v', 'P00003v', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-28', '2019-05-14', '901001625', 'P000004', 'FA', 19361372, 19361372, 0, 19361372, 'P00003w', 'P00003w', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-03', '2019-02-16', '2638/XII/18', 'P000001', 'FA', 39654000, 39654000, 0, 39654000, 'P00003x', 'P00003x', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-06', '2019-02-20', '2239/XII/18', 'P000001', 'FA', 11609890, 11609890, 0, 11609890, 'P00003y', 'P00003y', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-07', '2019-02-20', '2244/XII/18', 'P000001', 'FA', 1152000, 1152000, 0, 1152000, 'P00003z', 'P00003z', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-07', '2019-03-02', '2288/XII/18', 'P000001', 'FA', 4266000, 4266000, 0, 4266000, 'P000040', 'P000040', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-08', '2019-04-24', '220/2/19', 'P000001', 'FA', 25252498, 25252498, 0, 25252498, 'P000046', 'P000046', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-31', '2019-04-16', '180/1/19', 'P000001', 'FA', 50042500, 50042500, 0, 50042500, 'P000045', 'P000045', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-21', '2019-04-06', '114/1/19', 'P000001', 'FA', 5846000, 5846000, 0, 5846000, 'P000044', 'P000044', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-14', '2019-03-30', '77/1/19', 'P000001', 'FA', 540000, 540000, 0, 540000, 'P000043', 'P000043', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-09', '2019-03-25', '54/1/19', 'P000001', 'FA', 176000, 176000, 0, 176000, 'P000042', 'P000042', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-12-21', '2019-03-06', '2315/XII/18', 'P000001', 'FA', 7434000, 7434000, 0, 7434000, 'P000041', 'P000041', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-12', '2019-04-28', '247/12/19', 'P000001', 'FA', 943500, 943500, 0, 943500, 'P000047', 'P000047', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2019-02-19', '2019-05-05', '311/2/19', 'P000001', 'FA', 1110000, 1110000, 0, 1110000, 'P000048', 'P000048', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-02-12', '2018-02-16', '1802016', 'P000006', 'FA', 3065250, 3065250, 0, 3065250, 'P000049', 'P000049', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_hutang" VALUES ('2018-05-02', '2018-05-17', '1805014', 'P000004', 'FA', 914500, 914500, 0, 914500, 'P00004a', 'P00004a', 0, 0, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for tbl_in
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_in";
CREATE TABLE "public"."tbl_in" (
  "IDIn" int8 NOT NULL DEFAULT nextval('"tbl_in_IDIn_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDMerk_seq"'::regclass),
  "Panjang_yard" float8,
  "Panjang_meter" float8,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_in
-- ----------------------------
INSERT INTO "public"."tbl_in" VALUES (88, 'BARCODE02', NULL, NULL, NULL, 'P000008', 'P000001', 'P000028', 'Prk0001', 350, 320.04, 'A', NULL, 'P000004');
INSERT INTO "public"."tbl_in" VALUES (89, 'BARCODE01', NULL, NULL, NULL, 'P000008', 'P000001', 'P000028', 'Prk0001', 200, 182.88, 'A', NULL, 'P000004');
INSERT INTO "public"."tbl_in" VALUES (90, '0319PMK03447304', NULL, NULL, NULL, 'P000004', 'P000005', 'P000052', 'Prk0005', 50, 45.72, 'A', NULL, 'P000004');

-- ----------------------------
-- Table structure for tbl_instruksi_pengiriman
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_instruksi_pengiriman";
CREATE TABLE "public"."tbl_instruksi_pengiriman" (
  "IDIP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDIP_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDSupplier_seq"'::regclass),
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDPO_seq"'::regclass),
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_kirim" date,
  "Total_yard" float8,
  "Total_meter" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_instruksi_pengiriman_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_instruksi_pengiriman_detail";
CREATE TABLE "public"."tbl_instruksi_pengiriman_detail" (
  "IDIPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDIPDetail_seq"'::regclass),
  "IDIP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDIP_seq"'::regclass),
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_jurnal
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal";
CREATE TABLE "public"."tbl_jurnal" (
  "IDJurnal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8,
  "Kredit" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total_debet" float8,
  "Total_kredit" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Saldo" float8
)
;

-- ----------------------------
-- Records of tbl_jurnal
-- ----------------------------
INSERT INTO "public"."tbl_jurnal" VALUES ('P900001', '2019-02-25', '11/010', 'P900001', 'P900001', 'SA', 'P000009', 13431000, 0, 1, 1, 13431000, 0, 'Piutang SA', 13431000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900002', '2019-02-25', '01/041', 'P900002', 'P900002', 'SA', 'P000009', 21726500, 0, 1, 1, 21726500, 0, 'Piutang SA', 21726500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900003', '2019-02-25', '12/093', 'P900003', 'P900003', 'SA', 'P000009', 34983000, 0, 1, 1, 34983000, 0, 'Piutang SA', 34983000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900004', '2019-02-25', '02/081', 'P900004', 'P900004', 'SA', 'P000009', 35788750, 0, 1, 1, 35788750, 0, 'Piutang SA', 35788750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900005', '2019-02-25', '09/062', 'P900005', 'P900005', 'SA', 'P000009', 14413000, 0, 1, 1, 14413000, 0, 'Piutang SA', 14413000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900006', '2019-02-25', '10/091', 'P900006', 'P900006', 'SA', 'P000009', 33889000, 0, 1, 1, 33889000, 0, 'Piutang SA', 33889000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900007', '2019-02-25', '10/171', 'P900007', 'P900007', 'SA', 'P000009', 2760000, 0, 1, 1, 2760000, 0, 'Piutang SA', 2760000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900008', '2019-02-25', '11/051', 'P900008', 'P900008', 'SA', 'P000009', 3290000, 0, 1, 1, 3290000, 0, 'Piutang SA', 3290000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900009', '2019-02-25', '11/102', 'P900009', 'P900009', 'SA', 'P000009', 9405000, 0, 1, 1, 9405000, 0, 'Piutang SA', 9405000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900010', '2019-02-25', '12/002', 'P900010', 'P900010', 'SA', 'P000009', 2961000, 0, 1, 1, 2961000, 0, 'Piutang SA', 2961000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900011', '2019-02-25', '12/099', 'P900011', 'P900011', 'SA', 'P000009', 2944000, 0, 1, 1, 2944000, 0, 'Piutang SA', 2944000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900012', '2019-02-25', '12/127', 'P900012', 'P900012', 'SA', 'P000009', 5264000, 0, 1, 1, 5264000, 0, 'Piutang SA', 5264000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900013', '2019-02-25', '01/112', 'P900013', 'P900013', 'SA', 'P000009', 5900000, 0, 1, 1, 5900000, 0, 'Piutang SA', 5900000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900014', '2019-02-25', '01/121', 'P900014', 'P900014', 'SA', 'P000009', 9316000, 0, 1, 1, 9316000, 0, 'Piutang SA', 9316000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000005', '2019-03-19', 'P000001', 'P000001', 'P000001', 'SA', 'P000001', 1473850, 0, 1, 14000, 1473850, 0, '-', 1473850);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000006', '2019-03-19', 'P000002', 'P000002', 'P000002', 'SA', 'P000002', 32676800, 0, 1, 14000, 32676800, 0, '-', 32676800);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000007', '2019-03-19', 'P000003', 'P000003', 'P000003', 'SA', 'P000027', 19173000, 0, 1, 14000, 19173000, 0, '-', 19173000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000008', '2019-03-19', 'P000004', 'P000004', 'P000004', 'SA', 'P000032', 21737864, 0, 1, 14000, 21737864, 0, '-', 21737864);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000009', '2019-03-19', 'P000005', 'P000005', 'P000005', 'SA', 'P000005', 84715599, 0, 1, 14000, 84715599, 0, '-', 84715599);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00000a', '2019-03-19', 'P000006', 'P000006', 'P000006', 'SA', 'P000617', 0, 5500000000, 1, 14000, 0, 5500000000, '-', 5500000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00000b', '2019-03-19', 'P000007', 'P000007', 'P000007', 'SA', 'P000630', 0, 56942711, 1, 14000, 0, 56942711, '-', 56942711);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00000c', '2019-03-19', 'P000008', 'P000008', 'P000008', 'SA', 'P000632', 0, 16620823, 1, 14000, 0, 16620823, '-', 16620823);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00000d', '2019-03-19', 'P000009', 'P000009', 'P000009', 'SA', 'P000633', 0, 120000, 1, 14000, 0, 120000, '-', 120000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00000e', '2019-03-19', 'P00000a', 'P00000a', 'P00000a', 'SA', 'P000640', 0, 3240000, 1, 14000, 0, 3240000, '-', 3240000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00000f', '2019-03-19', 'P00000b', 'P00000b', 'P00000b', 'SA', 'P000028', 439829449, 0, 1, 14000, 439829449, 0, '-', 439829449);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00000g', '2019-03-19', 'P00000c', 'P00000c', 'P00000c', 'SA', 'P000641', 0, 1365001, 1, 14000, 0, 1365001, '-', 1365001);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00000h', '2019-03-19', 'P00000d', 'P00000d', 'P00000d', 'SA', 'P000006', 61142281, 0, 1, 14000, 61142281, 0, '-', 61142281);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900015', '2019-02-25', '02/044', 'P900015', 'P900015', 'SA', 'P000009', 7661000, 0, 1, 1, 7661000, 0, 'Piutang SA', 7661000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900016', '2019-02-25', '02/095', 'P900016', 'P900016', 'SA', 'P000009', 17823000, 0, 1, 1, 17823000, 0, 'Piutang SA', 17823000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900017', '2019-02-25', '12/003', 'P900017', 'P900017', 'SA', 'P000009', 14090250, 0, 1, 1, 14090250, 0, 'Piutang SA', 14090250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900018', '2019-02-25', '12/004', 'P900018', 'P900018', 'SA', 'P000009', 1260000, 0, 1, 1, 1260000, 0, 'Piutang SA', 1260000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900019', '2019-02-25', '12/019', 'P900019', 'P900019', 'SA', 'P000009', 8820000, 0, 1, 1, 8820000, 0, 'Piutang SA', 8820000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900020', '2019-02-25', '12/043', 'P900020', 'P900020', 'SA', 'P000009', 2887500, 0, 1, 1, 2887500, 0, 'Piutang SA', 2887500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900021', '2019-02-25', '12/058', 'P900021', 'P900021', 'SA', 'P000009', 5610500, 0, 1, 1, 5610500, 0, 'Piutang SA', 5610500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900022', '2019-02-25', '12/101', 'P900022', 'P900022', 'SA', 'P000009', 4545000, 0, 1, 1, 4545000, 0, 'Piutang SA', 4545000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900023', '2019-02-25', '12/115', 'P900023', 'P900023', 'SA', 'P000009', 12600000, 0, 1, 1, 12600000, 0, 'Piutang SA', 12600000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900024', '2019-02-25', '12/128', 'P900024', 'P900024', 'SA', 'P000009', 7647500, 0, 1, 1, 7647500, 0, 'Piutang SA', 7647500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900025', '2019-02-25', '12/129', 'P900025', 'P900025', 'SA', 'P000009', 3146000, 0, 1, 1, 3146000, 0, 'Piutang SA', 3146000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900026', '2019-02-25', '12/145', 'P900026', 'P900026', 'SA', 'P000009', 1488000, 0, 1, 1, 1488000, 0, 'Piutang SA', 1488000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900027', '2019-02-25', '01/006', 'P900027', 'P900027', 'SA', 'P000009', 7866000, 0, 1, 1, 7866000, 0, 'Piutang SA', 7866000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900028', '2019-02-25', '01/014', 'P900028', 'P900028', 'SA', 'P000009', 54558000, 0, 1, 1, 54558000, 0, 'Piutang SA', 54558000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900029', '2019-02-25', '01/019', 'P900029', 'P900029', 'SA', 'P000009', 7560000, 0, 1, 1, 7560000, 0, 'Piutang SA', 7560000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900030', '2019-02-25', '01/036', 'P900030', 'P900030', 'SA', 'P000009', 6101000, 0, 1, 1, 6101000, 0, 'Piutang SA', 6101000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900031', '2019-02-25', '01/053', 'P900031', 'P900031', 'SA', 'P000009', 1232000, 0, 1, 1, 1232000, 0, 'Piutang SA', 1232000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900032', '2019-02-25', '01/075', 'P900032', 'P900032', 'SA', 'P000009', 8477000, 0, 1, 1, 8477000, 0, 'Piutang SA', 8477000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900033', '2019-02-25', '01/076', 'P900033', 'P900033', 'SA', 'P000009', 1800000, 0, 1, 1, 1800000, 0, 'Piutang SA', 1800000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900034', '2019-02-25', '01/095', 'P900034', 'P900034', 'SA', 'P000009', 3230000, 0, 1, 1, 3230000, 0, 'Piutang SA', 3230000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900035', '2019-02-25', '01/116', 'P900035', 'P900035', 'SA', 'P000009', 3285000, 0, 1, 1, 3285000, 0, 'Piutang SA', 3285000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900036', '2019-02-25', '01/123', 'P900036', 'P900036', 'SA', 'P000009', 4128500, 0, 1, 1, 4128500, 0, 'Piutang SA', 4128500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900037', '2019-02-25', '01/156', 'P900037', 'P900037', 'SA', 'P000009', 3146000, 0, 1, 1, 3146000, 0, 'Piutang SA', 3146000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900038', '2019-02-25', '02/002', 'P900038', 'P900038', 'SA', 'P000009', 4312000, 0, 1, 1, 4312000, 0, 'Piutang SA', 4312000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900039', '2019-02-25', '02/024', 'P900039', 'P900039', 'SA', 'P000009', 1567500, 0, 1, 1, 1567500, 0, 'Piutang SA', 1567500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900040', '2019-02-25', '02/033', 'P900040', 'P900040', 'SA', 'P000009', 3150000, 0, 1, 1, 3150000, 0, 'Piutang SA', 3150000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900041', '2019-02-25', '02/042', 'P900041', 'P900041', 'SA', 'P000009', 2970000, 0, 1, 1, 2970000, 0, 'Piutang SA', 2970000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900042', '2019-02-25', '02/048', 'P900042', 'P900042', 'SA', 'P000009', 2880000, 0, 1, 1, 2880000, 0, 'Piutang SA', 2880000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900043', '2019-02-25', '02/063', 'P900043', 'P900043', 'SA', 'P000009', 11157250, 0, 1, 1, 11157250, 0, 'Piutang SA', 11157250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900044', '2019-02-25', '02/084', 'P900044', 'P900044', 'SA', 'P000009', 11686500, 0, 1, 1, 11686500, 0, 'Piutang SA', 11686500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900045', '2019-02-25', '03/083', 'P900045', 'P900045', 'SA', 'P000009', 234000, 0, 1, 1, 234000, 0, 'Piutang SA', 234000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900046', '2019-02-25', '03/221', 'P900046', 'P900046', 'SA', 'P000009', 3162000, 0, 1, 1, 3162000, 0, 'Piutang SA', 3162000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900047', '2019-02-25', '01/002', 'P900047', 'P900047', 'SA', 'P000009', 621000, 0, 1, 1, 621000, 0, 'Piutang SA', 621000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900048', '2019-02-25', '10/041', 'P900048', 'P900048', 'SA', 'P000009', 7904000, 0, 1, 1, 7904000, 0, 'Piutang SA', 7904000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900049', '2019-02-25', '10/043', 'P900049', 'P900049', 'SA', 'P000009', 4140500, 0, 1, 1, 4140500, 0, 'Piutang SA', 4140500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900050', '2019-02-25', '10/064', 'P900050', 'P900050', 'SA', 'P000009', 2898000, 0, 1, 1, 2898000, 0, 'Piutang SA', 2898000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900051', '2019-02-25', '10/072', 'P900051', 'P900051', 'SA', 'P000009', 3665500, 0, 1, 1, 3665500, 0, 'Piutang SA', 3665500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900052', '2019-02-25', '10/122', 'P900052', 'P900052', 'SA', 'P000009', 3725000, 0, 1, 1, 3725000, 0, 'Piutang SA', 3725000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900053', '2019-02-25', '10/145', 'P900053', 'P900053', 'SA', 'P000009', 3283000, 0, 1, 1, 3283000, 0, 'Piutang SA', 3283000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900054', '2019-02-25', '10/154', 'P900054', 'P900054', 'SA', 'P000009', 9702000, 0, 1, 1, 9702000, 0, 'Piutang SA', 9702000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900055', '2019-02-25', '10/160', 'P900055', 'P900055', 'SA', 'P000009', 9800000, 0, 1, 1, 9800000, 0, 'Piutang SA', 9800000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900056', '2019-02-25', '10/173', 'P900056', 'P900056', 'SA', 'P000009', 7400000, 0, 1, 1, 7400000, 0, 'Piutang SA', 7400000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900057', '2019-02-25', '11/003', 'P900057', 'P900057', 'SA', 'P000009', 6669000, 0, 1, 1, 6669000, 0, 'Piutang SA', 6669000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900058', '2019-02-25', '11/027', 'P900058', 'P900058', 'SA', 'P000009', 6993500, 0, 1, 1, 6993500, 0, 'Piutang SA', 6993500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900059', '2019-02-25', '11/049', 'P900059', 'P900059', 'SA', 'P000009', 3528000, 0, 1, 1, 3528000, 0, 'Piutang SA', 3528000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900060', '2019-02-25', '11/072', 'P900060', 'P900060', 'SA', 'P000009', 6762000, 0, 1, 1, 6762000, 0, 'Piutang SA', 6762000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900061', '2019-02-25', '11/075', 'P900061', 'P900061', 'SA', 'P000009', 1764000, 0, 1, 1, 1764000, 0, 'Piutang SA', 1764000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900062', '2019-02-25', '11/089', 'P900062', 'P900062', 'SA', 'P000009', 7657000, 0, 1, 1, 7657000, 0, 'Piutang SA', 7657000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900063', '2019-02-25', '11/093', 'P900063', 'P900063', 'SA', 'P000009', 6946500, 0, 1, 1, 6946500, 0, 'Piutang SA', 6946500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900064', '2019-02-25', '11/105', 'P900064', 'P900064', 'SA', 'P000009', 3430000, 0, 1, 1, 3430000, 0, 'Piutang SA', 3430000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900065', '2019-02-25', '11/125', 'P900065', 'P900065', 'SA', 'P000009', 2606250, 0, 1, 1, 2606250, 0, 'Piutang SA', 2606250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900066', '2019-02-25', '11/131', 'P900066', 'P900066', 'SA', 'P000009', 3501000, 0, 1, 1, 3501000, 0, 'Piutang SA', 3501000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900067', '2019-02-25', '11/154', 'P900067', 'P900067', 'SA', 'P000009', 13060000, 0, 1, 1, 13060000, 0, 'Piutang SA', 13060000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900068', '2019-02-25', '12/011', 'P900068', 'P900068', 'SA', 'P000009', 3590000, 0, 1, 1, 3590000, 0, 'Piutang SA', 3590000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900069', '2019-02-25', '12/016', 'P900069', 'P900069', 'SA', 'P000009', 2220000, 0, 1, 1, 2220000, 0, 'Piutang SA', 2220000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900070', '2019-02-25', '12/028', 'P900070', 'P900070', 'SA', 'P000009', 4250000, 0, 1, 1, 4250000, 0, 'Piutang SA', 4250000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900071', '2019-02-25', '12/076', 'P900071', 'P900071', 'SA', 'P000009', 2737500, 0, 1, 1, 2737500, 0, 'Piutang SA', 2737500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900072', '2019-02-25', '12/088', 'P900072', 'P900072', 'SA', 'P000009', 13408000, 0, 1, 1, 13408000, 0, 'Piutang SA', 13408000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900073', '2019-02-25', '12/107', 'P900073', 'P900073', 'SA', 'P000009', 2660000, 0, 1, 1, 2660000, 0, 'Piutang SA', 2660000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900074', '2019-02-25', '12/113', 'P900074', 'P900074', 'SA', 'P000009', 6296500, 0, 1, 1, 6296500, 0, 'Piutang SA', 6296500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900075', '2019-02-25', '12/142', 'P900075', 'P900075', 'SA', 'P000009', 7254000, 0, 1, 1, 7254000, 0, 'Piutang SA', 7254000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900076', '2019-02-25', '12/150', 'P900076', 'P900076', 'SA', 'P000009', 14626500, 0, 1, 1, 14626500, 0, 'Piutang SA', 14626500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900077', '2019-02-25', '12/158', 'P900077', 'P900077', 'SA', 'P000009', 4165000, 0, 1, 1, 4165000, 0, 'Piutang SA', 4165000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900078', '2019-02-25', '12/169', 'P900078', 'P900078', 'SA', 'P000009', 1254000, 0, 1, 1, 1254000, 0, 'Piutang SA', 1254000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900079', '2019-02-25', '01/004', 'P900079', 'P900079', 'SA', 'P000009', 4047000, 0, 1, 1, 4047000, 0, 'Piutang SA', 4047000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900080', '2019-02-25', '01/032', 'P900080', 'P900080', 'SA', 'P000009', 8109500, 0, 1, 1, 8109500, 0, 'Piutang SA', 8109500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900081', '2019-02-25', '01/048', 'P900081', 'P900081', 'SA', 'P000009', 2951000, 0, 1, 1, 2951000, 0, 'Piutang SA', 2951000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900082', '2019-02-25', '01/072', 'P900082', 'P900082', 'SA', 'P000009', 10573250, 0, 1, 1, 10573250, 0, 'Piutang SA', 10573250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900083', '2019-02-25', '01/105', 'P900083', 'P900083', 'SA', 'P000009', 36752000, 0, 1, 1, 36752000, 0, 'Piutang SA', 36752000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900084', '2019-02-25', '01/114', 'P900084', 'P900084', 'SA', 'P000009', 60819000, 0, 1, 1, 60819000, 0, 'Piutang SA', 60819000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900085', '2019-02-25', '01/118', 'P900085', 'P900085', 'SA', 'P000009', 66348000, 0, 1, 1, 66348000, 0, 'Piutang SA', 66348000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900086', '2019-02-25', '12/016', 'P900086', 'P900086', 'SA', 'P000009', -2220000, 0, 1, 1, -2220000, 0, 'Piutang SA', -2220000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900087', '2019-02-25', '12/088', 'P900087', 'P900087', 'SA', 'P000009', -925000, 0, 1, 1, -925000, 0, 'Piutang SA', -925000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900088', '2019-02-25', '01/146', 'P900088', 'P900088', 'SA', 'P000009', 25470000, 0, 1, 1, 25470000, 0, 'Piutang SA', 25470000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900089', '2019-02-25', '01/161', 'P900089', 'P900089', 'SA', 'P000009', 2618000, 0, 1, 1, 2618000, 0, 'Piutang SA', 2618000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900090', '2019-02-25', '01/168', 'P900090', 'P900090', 'SA', 'P000009', 179303000, 0, 1, 1, 179303000, 0, 'Piutang SA', 179303000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900091', '2019-02-25', '01/169', 'P900091', 'P900091', 'SA', 'P000009', 3780000, 0, 1, 1, 3780000, 0, 'Piutang SA', 3780000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900092', '2019-02-25', '01/182', 'P900092', 'P900092', 'SA', 'P000009', 9625500, 0, 1, 1, 9625500, 0, 'Piutang SA', 9625500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900093', '2019-02-25', '01/192', 'P900093', 'P900093', 'SA', 'P000009', 2866500, 0, 1, 1, 2866500, 0, 'Piutang SA', 2866500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900094', '2019-02-25', '02/006', 'P900094', 'P900094', 'SA', 'P000009', 4008000, 0, 1, 1, 4008000, 0, 'Piutang SA', 4008000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900095', '2019-02-25', '02/035', 'P900095', 'P900095', 'SA', 'P000009', 54435000, 0, 1, 1, 54435000, 0, 'Piutang SA', 54435000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900096', '2019-02-25', '02/061', 'P900096', 'P900096', 'SA', 'P000009', 5304000, 0, 1, 1, 5304000, 0, 'Piutang SA', 5304000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900097', '2019-02-25', '02/082', 'P900097', 'P900097', 'SA', 'P000009', 3525000, 0, 1, 1, 3525000, 0, 'Piutang SA', 3525000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900098', '2019-02-25', '02/085', 'P900098', 'P900098', 'SA', 'P000009', 3430000, 0, 1, 1, 3430000, 0, 'Piutang SA', 3430000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900099', '2019-02-25', '02/110', 'P900099', 'P900099', 'SA', 'P000009', 11052000, 0, 1, 1, 11052000, 0, 'Piutang SA', 11052000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900100', '2019-02-25', '02/135', 'P900100', 'P900100', 'SA', 'P000009', 11837000, 0, 1, 1, 11837000, 0, 'Piutang SA', 11837000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900101', '2019-02-25', '02/141', 'P900101', 'P900101', 'SA', 'P000009', 1470000, 0, 1, 1, 1470000, 0, 'Piutang SA', 1470000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900102', '2019-02-25', 'RETUR', 'P900102', 'P900102', 'SA', 'P000009', -1054750, 0, 1, 1, -1054750, 0, 'Piutang SA', -1054750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900103', '2019-02-25', '06/026', 'P900103', 'P900103', 'SA', 'P000009', 6014000, 0, 1, 1, 6014000, 0, 'Piutang SA', 6014000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900104', '2019-02-25', '06/063', 'P900104', 'P900104', 'SA', 'P000009', 24343500, 0, 1, 1, 24343500, 0, 'Piutang SA', 24343500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900105', '2019-02-25', '06/086', 'P900105', 'P900105', 'SA', 'P000009', 17250000, 0, 1, 1, 17250000, 0, 'Piutang SA', 17250000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900106', '2019-02-25', '06/110', 'P900106', 'P900106', 'SA', 'P000009', 24768000, 0, 1, 1, 24768000, 0, 'Piutang SA', 24768000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900107', '2019-02-25', '06/152', 'P900107', 'P900107', 'SA', 'P000009', 3564000, 0, 1, 1, 3564000, 0, 'Piutang SA', 3564000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900108', '2019-02-25', '07/008', 'P900108', 'P900108', 'SA', 'P000009', 8946000, 0, 1, 1, 8946000, 0, 'Piutang SA', 8946000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900109', '2019-02-25', '07/051', 'P900109', 'P900109', 'SA', 'P000009', 3571500, 0, 1, 1, 3571500, 0, 'Piutang SA', 3571500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900110', '2019-02-25', '07/082', 'P900110', 'P900110', 'SA', 'P000009', 13026000, 0, 1, 1, 13026000, 0, 'Piutang SA', 13026000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900111', '2019-02-25', '07/086', 'P900111', 'P900111', 'SA', 'P000009', 5611500, 0, 1, 1, 5611500, 0, 'Piutang SA', 5611500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900112', '2019-02-25', '07/090', 'P900112', 'P900112', 'SA', 'P000009', 9288000, 0, 1, 1, 9288000, 0, 'Piutang SA', 9288000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900113', '2019-02-25', '07/091', 'P900113', 'P900113', 'SA', 'P000009', 5334000, 0, 1, 1, 5334000, 0, 'Piutang SA', 5334000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900114', '2019-02-25', '07/102', 'P900114', 'P900114', 'SA', 'P000009', 4234000, 0, 1, 1, 4234000, 0, 'Piutang SA', 4234000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900115', '2019-02-25', '07/108', 'P900115', 'P900115', 'SA', 'P000009', 13803000, 0, 1, 1, 13803000, 0, 'Piutang SA', 13803000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900116', '2019-02-25', '07/114', 'P900116', 'P900116', 'SA', 'P000009', 7488000, 0, 1, 1, 7488000, 0, 'Piutang SA', 7488000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900117', '2019-02-25', '07/127', 'P900117', 'P900117', 'SA', 'P000009', 7263500, 0, 1, 1, 7263500, 0, 'Piutang SA', 7263500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900118', '2019-02-25', '07/142', 'P900118', 'P900118', 'SA', 'P000009', 6096500, 0, 1, 1, 6096500, 0, 'Piutang SA', 6096500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900119', '2019-02-25', '07/144', 'P900119', 'P900119', 'SA', 'P000009', 11180000, 0, 1, 1, 11180000, 0, 'Piutang SA', 11180000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900120', '2019-02-25', '07/170', 'P900120', 'P900120', 'SA', 'P000009', 129500, 0, 1, 1, 129500, 0, 'Piutang SA', 129500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900121', '2019-02-25', '08/014', 'P900121', 'P900121', 'SA', 'P000009', 11514250, 0, 1, 1, 11514250, 0, 'Piutang SA', 11514250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900122', '2019-02-25', '08/021', 'P900122', 'P900122', 'SA', 'P000009', 2300500, 0, 1, 1, 2300500, 0, 'Piutang SA', 2300500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900123', '2019-02-25', '08/064', 'P900123', 'P900123', 'SA', 'P000009', 35030000, 0, 1, 1, 35030000, 0, 'Piutang SA', 35030000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900124', '2019-02-25', '08/079', 'P900124', 'P900124', 'SA', 'P000009', 1470000, 0, 1, 1, 1470000, 0, 'Piutang SA', 1470000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900125', '2019-02-25', '08/099', 'P900125', 'P900125', 'SA', 'P000009', 22063750, 0, 1, 1, 22063750, 0, 'Piutang SA', 22063750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900126', '2019-02-25', '08/109', 'P900126', 'P900126', 'SA', 'P000009', 6825000, 0, 1, 1, 6825000, 0, 'Piutang SA', 6825000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900127', '2019-02-25', '08/113', 'P900127', 'P900127', 'SA', 'P000009', 2520000, 0, 1, 1, 2520000, 0, 'Piutang SA', 2520000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900128', '2019-02-25', '08/153', 'P900128', 'P900128', 'SA', 'P000009', 7200000, 0, 1, 1, 7200000, 0, 'Piutang SA', 7200000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900129', '2019-02-25', '08/199', 'P900129', 'P900129', 'SA', 'P000009', 12343250, 0, 1, 1, 12343250, 0, 'Piutang SA', 12343250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900130', '2019-02-25', '08/215', 'P900130', 'P900130', 'SA', 'P000009', 15506250, 0, 1, 1, 15506250, 0, 'Piutang SA', 15506250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900131', '2019-02-25', '08/227', 'P900131', 'P900131', 'SA', 'P000009', 14742000, 0, 1, 1, 14742000, 0, 'Piutang SA', 14742000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900132', '2019-02-25', '08/257', 'P900132', 'P900132', 'SA', 'P000009', 10824000, 0, 1, 1, 10824000, 0, 'Piutang SA', 10824000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900133', '2019-02-25', '09/032', 'P900133', 'P900133', 'SA', 'P000009', 4860000, 0, 1, 1, 4860000, 0, 'Piutang SA', 4860000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900134', '2019-02-25', '09/075', 'P900134', 'P900134', 'SA', 'P000009', 6720000, 0, 1, 1, 6720000, 0, 'Piutang SA', 6720000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900135', '2019-02-25', '09/086', 'P900135', 'P900135', 'SA', 'P000009', 5670000, 0, 1, 1, 5670000, 0, 'Piutang SA', 5670000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900136', '2019-02-25', '09/087', 'P900136', 'P900136', 'SA', 'P000009', 12134000, 0, 1, 1, 12134000, 0, 'Piutang SA', 12134000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900137', '2019-02-25', '09/111', 'P900137', 'P900137', 'SA', 'P000009', 8490750, 0, 1, 1, 8490750, 0, 'Piutang SA', 8490750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900138', '2019-02-25', '09/217', 'P900138', 'P900138', 'SA', 'P000009', 7048000, 0, 1, 1, 7048000, 0, 'Piutang SA', 7048000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900139', '2019-02-25', 'RETUR', 'P900139', 'P900139', 'SA', 'P000009', -6784500, 0, 1, 1, -6784500, 0, 'Piutang SA', -6784500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900140', '2019-02-25', '02/132', 'P900140', 'P900140', 'SA', 'P000009', 13920500, 0, 1, 1, 13920500, 0, 'Piutang SA', 13920500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900141', '2019-02-25', '09/005', 'P900141', 'P900141', 'SA', 'P000009', 1520000, 0, 1, 1, 1520000, 0, 'Piutang SA', 1520000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900142', '2019-02-25', '09/097', 'P900142', 'P900142', 'SA', 'P000009', 1242000, 0, 1, 1, 1242000, 0, 'Piutang SA', 1242000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900143', '2019-02-25', '10/016', 'P900143', 'P900143', 'SA', 'P000009', 2415000, 0, 1, 1, 2415000, 0, 'Piutang SA', 2415000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900144', '2019-02-25', '10/163', 'P900144', 'P900144', 'SA', 'P000009', 13196250, 0, 1, 1, 13196250, 0, 'Piutang SA', 13196250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900145', '2019-02-25', '11/074', 'P900145', 'P900145', 'SA', 'P000009', 3168000, 0, 1, 1, 3168000, 0, 'Piutang SA', 3168000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900146', '2019-02-25', '11/103', 'P900146', 'P900146', 'SA', 'P000009', 11540250, 0, 1, 1, 11540250, 0, 'Piutang SA', 11540250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900147', '2019-02-25', '12/092', 'P900147', 'P900147', 'SA', 'P000009', 2346000, 0, 1, 1, 2346000, 0, 'Piutang SA', 2346000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900148', '2019-02-25', '02/066', 'P900148', 'P900148', 'SA', 'P000009', 2343000, 0, 1, 1, 2343000, 0, 'Piutang SA', 2343000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900149', '2019-02-25', '10/001', 'P900149', 'P900149', 'SA', 'P000009', 89550000, 0, 1, 1, 89550000, 0, 'Piutang SA', 89550000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900150', '2019-02-25', '10/042', 'P900150', 'P900150', 'SA', 'P000009', 50456250, 0, 1, 1, 50456250, 0, 'Piutang SA', 50456250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900151', '2019-02-25', '10/044', 'P900151', 'P900151', 'SA', 'P000009', 32516750, 0, 1, 1, 32516750, 0, 'Piutang SA', 32516750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900152', '2019-02-25', '10/062', 'P900152', 'P900152', 'SA', 'P000009', 14894000, 0, 1, 1, 14894000, 0, 'Piutang SA', 14894000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900153', '2019-02-25', '10/089', 'P900153', 'P900153', 'SA', 'P000009', 667344000, 0, 1, 1, 667344000, 0, 'Piutang SA', 667344000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900154', '2019-02-25', '10/101', 'P900154', 'P900154', 'SA', 'P000009', 62558000, 0, 1, 1, 62558000, 0, 'Piutang SA', 62558000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900155', '2019-02-25', '10/125', 'P900155', 'P900155', 'SA', 'P000009', 14652000, 0, 1, 1, 14652000, 0, 'Piutang SA', 14652000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900156', '2019-02-25', '11/045', 'P900156', 'P900156', 'SA', 'P000009', 28548000, 0, 1, 1, 28548000, 0, 'Piutang SA', 28548000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900157', '2019-02-25', '11/070', 'P900157', 'P900157', 'SA', 'P000009', 13754000, 0, 1, 1, 13754000, 0, 'Piutang SA', 13754000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900158', '2019-02-25', '11/141', 'P900158', 'P900158', 'SA', 'P000009', 75994500, 0, 1, 1, 75994500, 0, 'Piutang SA', 75994500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900159', '2019-02-25', '12/064', 'P900159', 'P900159', 'SA', 'P000009', 15312000, 0, 1, 1, 15312000, 0, 'Piutang SA', 15312000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900160', '2019-02-25', '12/091', 'P900160', 'P900160', 'SA', 'P000009', 10578000, 0, 1, 1, 10578000, 0, 'Piutang SA', 10578000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900161', '2019-02-25', '01/010', 'P900161', 'P900161', 'SA', 'P000009', 12361500, 0, 1, 1, 12361500, 0, 'Piutang SA', 12361500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900162', '2019-02-25', '01/131', 'P900162', 'P900162', 'SA', 'P000009', 43788000, 0, 1, 1, 43788000, 0, 'Piutang SA', 43788000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900163', '2019-02-25', '01/144', 'P900163', 'P900163', 'SA', 'P000009', 22770000, 0, 1, 1, 22770000, 0, 'Piutang SA', 22770000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900164', '2019-02-25', '01/167', 'P900164', 'P900164', 'SA', 'P000009', 19973250, 0, 1, 1, 19973250, 0, 'Piutang SA', 19973250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900165', '2019-02-25', '01/184', 'P900165', 'P900165', 'SA', 'P000009', 49919750, 0, 1, 1, 49919750, 0, 'Piutang SA', 49919750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900166', '2019-02-25', '01/193', 'P900166', 'P900166', 'SA', 'P000009', 11561000, 0, 1, 1, 11561000, 0, 'Piutang SA', 11561000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900167', '2019-02-25', '02/053', 'P900167', 'P900167', 'SA', 'P000009', 59010750, 0, 1, 1, 59010750, 0, 'Piutang SA', 59010750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900168', '2019-02-25', '02/106', 'P900168', 'P900168', 'SA', 'P000009', 13005000, 0, 1, 1, 13005000, 0, 'Piutang SA', 13005000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900169', '2019-02-25', '02/142', 'P900169', 'P900169', 'SA', 'P000009', 9450000, 0, 1, 1, 9450000, 0, 'Piutang SA', 9450000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900170', '2019-02-25', '03/028', 'P900170', 'P900170', 'SA', 'P000009', 5347500, 0, 1, 1, 5347500, 0, 'Piutang SA', 5347500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900171', '2019-02-25', '04/002', 'P900171', 'P900171', 'SA', 'P000009', 8659500, 0, 1, 1, 8659500, 0, 'Piutang SA', 8659500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900172', '2019-02-25', '04/064', 'P900172', 'P900172', 'SA', 'P000009', 4485000, 0, 1, 1, 4485000, 0, 'Piutang SA', 4485000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900173', '2019-02-25', '04/176', 'P900173', 'P900173', 'SA', 'P000009', 4605750, 0, 1, 1, 4605750, 0, 'Piutang SA', 4605750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900174', '2019-02-25', '05/072', 'P900174', 'P900174', 'SA', 'P000009', 53199000, 0, 1, 1, 53199000, 0, 'Piutang SA', 53199000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900175', '2019-02-25', '05/136', 'P900175', 'P900175', 'SA', 'P000009', 4157250, 0, 1, 1, 4157250, 0, 'Piutang SA', 4157250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900176', '2019-02-25', '05/155', 'P900176', 'P900176', 'SA', 'P000009', 2498500, 0, 1, 1, 2498500, 0, 'Piutang SA', 2498500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900177', '2019-02-25', '05/200', 'P900177', 'P900177', 'SA', 'P000009', 3122250, 0, 1, 1, 3122250, 0, 'Piutang SA', 3122250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900178', '2019-02-25', '05/215', 'P900178', 'P900178', 'SA', 'P000009', 3915750, 0, 1, 1, 3915750, 0, 'Piutang SA', 3915750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900179', '2019-02-25', '05/230', 'P900179', 'P900179', 'SA', 'P000009', 2932500, 0, 1, 1, 2932500, 0, 'Piutang SA', 2932500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900180', '2019-02-25', '06/042', 'P900180', 'P900180', 'SA', 'P000009', 3760500, 0, 1, 1, 3760500, 0, 'Piutang SA', 3760500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900181', '2019-02-25', '06/081', 'P900181', 'P900181', 'SA', 'P000009', 2984250, 0, 1, 1, 2984250, 0, 'Piutang SA', 2984250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900182', '2019-02-25', '07/022', 'P900182', 'P900182', 'SA', 'P000009', 25978500, 0, 1, 1, 25978500, 0, 'Piutang SA', 25978500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900183', '2019-02-25', '07/109', 'P900183', 'P900183', 'SA', 'P000009', 15939000, 0, 1, 1, 15939000, 0, 'Piutang SA', 15939000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900184', '2019-02-25', '07/121', 'P900184', 'P900184', 'SA', 'P000009', 2893250, 0, 1, 1, 2893250, 0, 'Piutang SA', 2893250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900185', '2019-02-25', '07/216', 'P900185', 'P900185', 'SA', 'P000009', 2415000, 0, 1, 1, 2415000, 0, 'Piutang SA', 2415000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900186', '2019-02-25', '08/203', 'P900186', 'P900186', 'SA', 'P000009', 4260000, 0, 1, 1, 4260000, 0, 'Piutang SA', 4260000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900187', '2019-02-25', '09/125', 'P900187', 'P900187', 'SA', 'P000009', 5325000, 0, 1, 1, 5325000, 0, 'Piutang SA', 5325000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900188', '2019-02-25', '10/109', 'P900188', 'P900188', 'SA', 'P000009', 9514000, 0, 1, 1, 9514000, 0, 'Piutang SA', 9514000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900189', '2019-02-25', '10/164', 'P900189', 'P900189', 'SA', 'P000009', 5918000, 0, 1, 1, 5918000, 0, 'Piutang SA', 5918000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900190', '2019-02-25', '11/037', 'P900190', 'P900190', 'SA', 'P000009', 48599500, 0, 1, 1, 48599500, 0, 'Piutang SA', 48599500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900191', '2019-02-25', '11/086', 'P900191', 'P900191', 'SA', 'P000009', 22649000, 0, 1, 1, 22649000, 0, 'Piutang SA', 22649000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900192', '2019-02-25', '12/049', 'P900192', 'P900192', 'SA', 'P000009', 9531750, 0, 1, 1, 9531750, 0, 'Piutang SA', 9531750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900193', '2019-02-25', '12/068', 'P900193', 'P900193', 'SA', 'P000009', 6993500, 0, 1, 1, 6993500, 0, 'Piutang SA', 6993500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900194', '2019-02-25', '12/133', 'P900194', 'P900194', 'SA', 'P000009', 4828000, 0, 1, 1, 4828000, 0, 'Piutang SA', 4828000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900195', '2019-02-25', '12/138', 'P900195', 'P900195', 'SA', 'P000009', 4167750, 0, 1, 1, 4167750, 0, 'Piutang SA', 4167750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900196', '2019-02-25', '12/147', 'P900196', 'P900196', 'SA', 'P000009', 12425000, 0, 1, 1, 12425000, 0, 'Piutang SA', 12425000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900197', '2019-02-25', '01/016', 'P900197', 'P900197', 'SA', 'P000009', 17040000, 0, 1, 1, 17040000, 0, 'Piutang SA', 17040000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900198', '2019-02-25', '01/052', 'P900198', 'P900198', 'SA', 'P000009', 7011250, 0, 1, 1, 7011250, 0, 'Piutang SA', 7011250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900199', '2019-02-25', '01/175', 'P900199', 'P900199', 'SA', 'P000009', 9654250, 0, 1, 1, 9654250, 0, 'Piutang SA', 9654250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900200', '2019-02-25', '02/032', 'P900200', 'P900200', 'SA', 'P000009', 3157250, 0, 1, 1, 3157250, 0, 'Piutang SA', 3157250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900201', '2019-02-25', '02/065', 'P900201', 'P900201', 'SA', 'P000009', 15768000, 0, 1, 1, 15768000, 0, 'Piutang SA', 15768000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900202', '2019-02-25', '02/087', 'P900202', 'P900202', 'SA', 'P000009', 9417000, 0, 1, 1, 9417000, 0, 'Piutang SA', 9417000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900203', '2019-02-25', '02/109', 'P900203', 'P900203', 'SA', 'P000009', 2372500, 0, 1, 1, 2372500, 0, 'Piutang SA', 2372500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900204', '2019-02-25', '02/144', 'P900204', 'P900204', 'SA', 'P000009', 9380500, 0, 1, 1, 9380500, 0, 'Piutang SA', 9380500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900205', '2019-02-25', '10/015', 'P900205', 'P900205', 'SA', 'P000009', 3162000, 0, 1, 1, 3162000, 0, 'Piutang SA', 3162000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900206', '2019-02-25', '12/025', 'P900206', 'P900206', 'SA', 'P000009', 5225000, 0, 1, 1, 5225000, 0, 'Piutang SA', 5225000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900207', '2019-02-25', '01/084', 'P900207', 'P900207', 'SA', 'P000009', 17274000, 0, 1, 1, 17274000, 0, 'Piutang SA', 17274000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900208', '2019-02-25', '02/020', 'P900208', 'P900208', 'SA', 'P000009', 25256000, 0, 1, 1, 25256000, 0, 'Piutang SA', 25256000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900209', '2019-02-25', '02/108', 'P900209', 'P900209', 'SA', 'P000009', 5081750, 0, 1, 1, 5081750, 0, 'Piutang SA', 5081750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900210', '2019-02-25', '07/213', 'P900210', 'P900210', 'SA', 'P000009', 3898500, 0, 1, 1, 3898500, 0, 'Piutang SA', 3898500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900211', '2019-02-25', '08/053', 'P900211', 'P900211', 'SA', 'P000009', 24717000, 0, 1, 1, 24717000, 0, 'Piutang SA', 24717000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900212', '2019-02-25', '08/108', 'P900212', 'P900212', 'SA', 'P000009', 11715000, 0, 1, 1, 11715000, 0, 'Piutang SA', 11715000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900213', '2019-02-25', '08/135', 'P900213', 'P900213', 'SA', 'P000009', 141162000, 0, 1, 1, 141162000, 0, 'Piutang SA', 141162000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900214', '2019-02-25', '08/150', 'P900214', 'P900214', 'SA', 'P000009', 23047500, 0, 1, 1, 23047500, 0, 'Piutang SA', 23047500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900215', '2019-02-25', '08/193', 'P900215', 'P900215', 'SA', 'P000009', 4934500, 0, 1, 1, 4934500, 0, 'Piutang SA', 4934500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900216', '2019-02-25', '09/058', 'P900216', 'P900216', 'SA', 'P000009', 2414000, 0, 1, 1, 2414000, 0, 'Piutang SA', 2414000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900217', '2019-02-25', '09/106', 'P900217', 'P900217', 'SA', 'P000009', 9656000, 0, 1, 1, 9656000, 0, 'Piutang SA', 9656000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900218', '2019-02-25', '10/099', 'P900218', 'P900218', 'SA', 'P000009', 11076000, 0, 1, 1, 11076000, 0, 'Piutang SA', 11076000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900219', '2019-02-25', '10/175', 'P900219', 'P900219', 'SA', 'P000009', 19045750, 0, 1, 1, 19045750, 0, 'Piutang SA', 19045750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900220', '2019-02-25', '11/050', 'P900220', 'P900220', 'SA', 'P000009', 5112000, 0, 1, 1, 5112000, 0, 'Piutang SA', 5112000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900221', '2019-02-25', '11/157', 'P900221', 'P900221', 'SA', 'P000009', 12318500, 0, 1, 1, 12318500, 0, 'Piutang SA', 12318500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900222', '2019-02-25', '12/112', 'P900222', 'P900222', 'SA', 'P000009', 15637750, 0, 1, 1, 15637750, 0, 'Piutang SA', 15637750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900223', '2019-02-25', '01/050', 'P900223', 'P900223', 'SA', 'P000009', 7891250, 0, 1, 1, 7891250, 0, 'Piutang SA', 7891250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900224', '2019-02-25', '01/064', 'P900224', 'P900224', 'SA', 'P000009', 1828250, 0, 1, 1, 1828250, 0, 'Piutang SA', 1828250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900225', '2019-02-25', '01/150', 'P900225', 'P900225', 'SA', 'P000009', 8979000, 0, 1, 1, 8979000, 0, 'Piutang SA', 8979000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900226', '2019-02-25', '02/007', 'P900226', 'P900226', 'SA', 'P000009', 3230250, 0, 1, 1, 3230250, 0, 'Piutang SA', 3230250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900227', '2019-02-25', '04/181', 'P900227', 'P900227', 'SA', 'P000009', 6364000, 0, 1, 1, 6364000, 0, 'Piutang SA', 6364000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900228', '2019-02-25', '05/098', 'P900228', 'P900228', 'SA', 'P000009', 558000, 0, 1, 1, 558000, 0, 'Piutang SA', 558000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900229', '2019-02-25', '06/031', 'P900229', 'P900229', 'SA', 'P000009', 838500, 0, 1, 1, 838500, 0, 'Piutang SA', 838500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900230', '2019-02-25', '07/127', 'P900230', 'P900230', 'SA', 'P000009', 976500, 0, 1, 1, 976500, 0, 'Piutang SA', 976500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900231', '2019-02-25', '09/042', 'P900231', 'P900231', 'SA', 'P000009', 617500, 0, 1, 1, 617500, 0, 'Piutang SA', 617500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900232', '2019-02-25', '09/134', 'P900232', 'P900232', 'SA', 'P000009', 1232000, 0, 1, 1, 1232000, 0, 'Piutang SA', 1232000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900233', '2019-02-25', '09/154', 'P900233', 'P900233', 'SA', 'P000009', 261800, 0, 1, 1, 261800, 0, 'Piutang SA', 261800);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900234', '2019-02-25', '11/024', 'P900234', 'P900234', 'SA', 'P000009', 1034000, 0, 1, 1, 1034000, 0, 'Piutang SA', 1034000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900235', '2019-02-25', '11/109', 'P900235', 'P900235', 'SA', 'P000009', 1448750, 0, 1, 1, 1448750, 0, 'Piutang SA', 1448750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900236', '2019-02-25', '11/111', 'P900236', 'P900236', 'SA', 'P000009', 1452000, 0, 1, 1, 1452000, 0, 'Piutang SA', 1452000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900237', '2019-02-25', '11/130', 'P900237', 'P900237', 'SA', 'P000009', 378750, 0, 1, 1, 378750, 0, 'Piutang SA', 378750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900238', '2019-02-25', '11/181', 'P900238', 'P900238', 'SA', 'P000009', 2185000, 0, 1, 1, 2185000, 0, 'Piutang SA', 2185000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900239', '2019-02-25', '12/007', 'P900239', 'P900239', 'SA', 'P000009', 1232000, 0, 1, 1, 1232000, 0, 'Piutang SA', 1232000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900240', '2019-02-25', '01/098', 'P900240', 'P900240', 'SA', 'P000009', 388000, 0, 1, 1, 388000, 0, 'Piutang SA', 388000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00006o', '2019-03-31', 'JU19PMKA00001', 'P000001', 'P000001', 'JU', 'P000032', 0, 1001478, 1, 14000, 0, 1001478, 'Amortisasi bulanan', 1001478);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00006p', '2019-03-31', 'JU19PMKA00002', 'P000002', 'P000002', 'JU', 'P000596', 663227, 0, 1, 14000, 663227, 0, 'Bunga Leasing', 663227);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00006q', '2019-03-31', 'JU19PMKA00002', 'P000002', 'P000003', 'JU', 'P000622', 0, 663227, 1, 14000, 0, 663227, 'Bunga Leasing', 663227);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900241', '2019-02-25', '02/040', 'P900241', 'P900241', 'SA', 'P000009', 2109750, 0, 1, 1, 2109750, 0, 'Piutang SA', 2109750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900242', '2019-02-25', '02/080', 'P900242', 'P900242', 'SA', 'P000009', 630000, 0, 1, 1, 630000, 0, 'Piutang SA', 630000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900243', '2019-02-25', '11/011', 'P900243', 'P900243', 'SA', 'P000009', 20440000, 0, 1, 1, 20440000, 0, 'Piutang SA', 20440000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900244', '2019-02-25', '11/018', 'P900244', 'P900244', 'SA', 'P000009', 22080000, 0, 1, 1, 22080000, 0, 'Piutang SA', 22080000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900245', '2019-02-25', '01/011', 'P900245', 'P900245', 'SA', 'P000009', 18000000, 0, 1, 1, 18000000, 0, 'Piutang SA', 18000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900246', '2019-02-25', '02/132', 'P900246', 'P900246', 'SA', 'P000009', 12730500, 0, 1, 1, 12730500, 0, 'Piutang SA', 12730500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900247', '2019-02-25', '01/066', 'P900247', 'P900247', 'SA', 'P000009', 323000, 0, 1, 1, 323000, 0, 'Piutang SA', 323000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900248', '2019-02-25', '02/099', 'P900248', 'P900248', 'SA', 'P000009', 129447500, 0, 1, 1, 129447500, 0, 'Piutang SA', 129447500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900249', '2019-02-25', '11/002', 'P900249', 'P900249', 'SA', 'P000009', 13397000, 0, 1, 1, 13397000, 0, 'Piutang SA', 13397000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900250', '2019-02-25', '11/068', 'P900250', 'P900250', 'SA', 'P000009', 14522000, 0, 1, 1, 14522000, 0, 'Piutang SA', 14522000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900251', '2019-02-25', '11/168', 'P900251', 'P900251', 'SA', 'P000009', 72261000, 0, 1, 1, 72261000, 0, 'Piutang SA', 72261000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900252', '2019-02-25', '11/169', 'P900252', 'P900252', 'SA', 'P000009', 72224000, 0, 1, 1, 72224000, 0, 'Piutang SA', 72224000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900253', '2019-02-25', '12/009', 'P900253', 'P900253', 'SA', 'P000009', 11761000, 0, 1, 1, 11761000, 0, 'Piutang SA', 11761000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900254', '2019-02-25', '12/021', 'P900254', 'P900254', 'SA', 'P000009', 9744750, 0, 1, 1, 9744750, 0, 'Piutang SA', 9744750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900255', '2019-02-25', '12/079', 'P900255', 'P900255', 'SA', 'P000009', 16777000, 0, 1, 1, 16777000, 0, 'Piutang SA', 16777000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900256', '2019-02-25', '01/031', 'P900256', 'P900256', 'SA', 'P000009', 9404750, 0, 1, 1, 9404750, 0, 'Piutang SA', 9404750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900257', '2019-02-25', '01/042', 'P900257', 'P900257', 'SA', 'P000009', 18503000, 0, 1, 1, 18503000, 0, 'Piutang SA', 18503000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900258', '2019-02-25', '01/058', 'P900258', 'P900258', 'SA', 'P000009', 3648000, 0, 1, 1, 3648000, 0, 'Piutang SA', 3648000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900259', '2019-02-25', '01/083', 'P900259', 'P900259', 'SA', 'P000009', 12744500, 0, 1, 1, 12744500, 0, 'Piutang SA', 12744500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900260', '2019-02-25', '01/111', 'P900260', 'P900260', 'SA', 'P000009', 10881000, 0, 1, 1, 10881000, 0, 'Piutang SA', 10881000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900261', '2019-02-25', '01/166', 'P900261', 'P900261', 'SA', 'P000009', 13924250, 0, 1, 1, 13924250, 0, 'Piutang SA', 13924250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900262', '2019-02-25', '02/008', 'P900262', 'P900262', 'SA', 'P000009', 9139500, 0, 1, 1, 9139500, 0, 'Piutang SA', 9139500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900263', '2019-02-25', '02/012', 'P900263', 'P900263', 'SA', 'P000009', 29099850, 0, 1, 1, 29099850, 0, 'Piutang SA', 29099850);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900264', '2019-02-25', '02/015', 'P900264', 'P900264', 'SA', 'P000009', 11232000, 0, 1, 1, 11232000, 0, 'Piutang SA', 11232000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900265', '2019-02-25', '02/079', 'P900265', 'P900265', 'SA', 'P000009', 11657500, 0, 1, 1, 11657500, 0, 'Piutang SA', 11657500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900266', '2019-02-25', '02/133', 'P900266', 'P900266', 'SA', 'P000009', 12622500, 0, 1, 1, 12622500, 0, 'Piutang SA', 12622500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900267', '2019-02-25', '02/095', 'P900267', 'P900267', 'SA', 'P000009', 131080, 0, 1, 1, 131080, 0, 'Piutang SA', 131080);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900268', '2019-02-25', '04/157', 'P900268', 'P900268', 'SA', 'P000009', 59040, 0, 1, 1, 59040, 0, 'Piutang SA', 59040);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900269', '2019-02-25', '07/204', 'P900269', 'P900269', 'SA', 'P000009', 446500, 0, 1, 1, 446500, 0, 'Piutang SA', 446500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900270', '2019-02-25', '09/001', 'P900270', 'P900270', 'SA', 'P000009', 4515000, 0, 1, 1, 4515000, 0, 'Piutang SA', 4515000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900271', '2019-02-25', '09/029', 'P900271', 'P900271', 'SA', 'P000009', 16117500, 0, 1, 1, 16117500, 0, 'Piutang SA', 16117500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900272', '2019-02-25', '09/045', 'P900272', 'P900272', 'SA', 'P000009', 8494000, 0, 1, 1, 8494000, 0, 'Piutang SA', 8494000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900273', '2019-02-25', '09/050', 'P900273', 'P900273', 'SA', 'P000009', 4949000, 0, 1, 1, 4949000, 0, 'Piutang SA', 4949000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900274', '2019-02-25', '09/098', 'P900274', 'P900274', 'SA', 'P000009', 9809500, 0, 1, 1, 9809500, 0, 'Piutang SA', 9809500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900275', '2019-02-25', '09/114', 'P900275', 'P900275', 'SA', 'P000009', 5439500, 0, 1, 1, 5439500, 0, 'Piutang SA', 5439500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900276', '2019-02-25', '09/117', 'P900276', 'P900276', 'SA', 'P000009', 7052000, 0, 1, 1, 7052000, 0, 'Piutang SA', 7052000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900277', '2019-02-25', '09/121', 'P900277', 'P900277', 'SA', 'P000009', 5342750, 0, 1, 1, 5342750, 0, 'Piutang SA', 5342750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900278', '2019-02-25', '09/143', 'P900278', 'P900278', 'SA', 'P000009', 15824000, 0, 1, 1, 15824000, 0, 'Piutang SA', 15824000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900279', '2019-02-25', '10/003', 'P900279', 'P900279', 'SA', 'P000009', 18608500, 0, 1, 1, 18608500, 0, 'Piutang SA', 18608500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900280', '2019-02-25', '10/023', 'P900280', 'P900280', 'SA', 'P000009', 5257000, 0, 1, 1, 5257000, 0, 'Piutang SA', 5257000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900281', '2019-02-25', '10/033', 'P900281', 'P900281', 'SA', 'P000009', 8060500, 0, 1, 1, 8060500, 0, 'Piutang SA', 8060500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900282', '2019-02-25', '10/052', 'P900282', 'P900282', 'SA', 'P000009', 5702000, 0, 1, 1, 5702000, 0, 'Piutang SA', 5702000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900283', '2019-02-25', '10/067', 'P900283', 'P900283', 'SA', 'P000009', 9549500, 0, 1, 1, 9549500, 0, 'Piutang SA', 9549500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900284', '2019-02-25', '10/085', 'P900284', 'P900284', 'SA', 'P000009', 5904500, 0, 1, 1, 5904500, 0, 'Piutang SA', 5904500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900285', '2019-02-25', '10/087', 'P900285', 'P900285', 'SA', 'P000009', 7073500, 0, 1, 1, 7073500, 0, 'Piutang SA', 7073500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900286', '2019-02-25', '10/090', 'P900286', 'P900286', 'SA', 'P000009', 9460000, 0, 1, 1, 9460000, 0, 'Piutang SA', 9460000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900287', '2019-02-25', '10/092', 'P900287', 'P900287', 'SA', 'P000009', 10275000, 0, 1, 1, 10275000, 0, 'Piutang SA', 10275000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900288', '2019-02-25', '10/134', 'P900288', 'P900288', 'SA', 'P000009', 8514000, 0, 1, 1, 8514000, 0, 'Piutang SA', 8514000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900289', '2019-02-25', '10/142', 'P900289', 'P900289', 'SA', 'P000009', 9116000, 0, 1, 1, 9116000, 0, 'Piutang SA', 9116000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900290', '2019-02-25', '11/019', 'P900290', 'P900290', 'SA', 'P000009', 6514500, 0, 1, 1, 6514500, 0, 'Piutang SA', 6514500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900291', '2019-02-25', '11/021', 'P900291', 'P900291', 'SA', 'P000009', 8131000, 0, 1, 1, 8131000, 0, 'Piutang SA', 8131000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900292', '2019-02-25', '11/047', 'P900292', 'P900292', 'SA', 'P000009', 3139000, 0, 1, 1, 3139000, 0, 'Piutang SA', 3139000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900293', '2019-02-25', '11/053', 'P900293', 'P900293', 'SA', 'P000009', 20038000, 0, 1, 1, 20038000, 0, 'Piutang SA', 20038000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900294', '2019-02-25', '11/057', 'P900294', 'P900294', 'SA', 'P000009', 4369000, 0, 1, 1, 4369000, 0, 'Piutang SA', 4369000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900295', '2019-02-25', '11/064', 'P900295', 'P900295', 'SA', 'P000009', 4300000, 0, 1, 1, 4300000, 0, 'Piutang SA', 4300000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900296', '2019-02-25', '11/067', 'P900296', 'P900296', 'SA', 'P000009', 5145000, 0, 1, 1, 5145000, 0, 'Piutang SA', 5145000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900297', '2019-02-25', '11/087', 'P900297', 'P900297', 'SA', 'P000009', 8987000, 0, 1, 1, 8987000, 0, 'Piutang SA', 8987000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900298', '2019-02-25', '11/096', 'P900298', 'P900298', 'SA', 'P000009', 7453750, 0, 1, 1, 7453750, 0, 'Piutang SA', 7453750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900299', '2019-02-25', '11/115', 'P900299', 'P900299', 'SA', 'P000009', 8428000, 0, 1, 1, 8428000, 0, 'Piutang SA', 8428000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900300', '2019-02-25', '11/139', 'P900300', 'P900300', 'SA', 'P000009', 9554750, 0, 1, 1, 9554750, 0, 'Piutang SA', 9554750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900301', '2019-02-25', '11/177', 'P900301', 'P900301', 'SA', 'P000009', 5719000, 0, 1, 1, 5719000, 0, 'Piutang SA', 5719000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900302', '2019-02-25', '12/006', 'P900302', 'P900302', 'SA', 'P000009', 4816000, 0, 1, 1, 4816000, 0, 'Piutang SA', 4816000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900303', '2019-02-25', '12/052', 'P900303', 'P900303', 'SA', 'P000009', 8643000, 0, 1, 1, 8643000, 0, 'Piutang SA', 8643000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900304', '2019-02-25', '12/070', 'P900304', 'P900304', 'SA', 'P000009', 2408000, 0, 1, 1, 2408000, 0, 'Piutang SA', 2408000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900305', '2019-02-25', '12/072', 'P900305', 'P900305', 'SA', 'P000009', 4851000, 0, 1, 1, 4851000, 0, 'Piutang SA', 4851000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900306', '2019-02-25', '12/105', 'P900306', 'P900306', 'SA', 'P000009', 15050000, 0, 1, 1, 15050000, 0, 'Piutang SA', 15050000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900307', '2019-02-25', '01/001', 'P900307', 'P900307', 'SA', 'P000009', 82089500, 0, 1, 1, 82089500, 0, 'Piutang SA', 82089500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900308', '2019-02-25', '01/005', 'P900308', 'P900308', 'SA', 'P000009', 12068000, 0, 1, 1, 12068000, 0, 'Piutang SA', 12068000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900309', '2019-02-25', '01/020', 'P900309', 'P900309', 'SA', 'P000009', 4753000, 0, 1, 1, 4753000, 0, 'Piutang SA', 4753000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900310', '2019-02-25', '01/030', 'P900310', 'P900310', 'SA', 'P000009', 4321500, 0, 1, 1, 4321500, 0, 'Piutang SA', 4321500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900311', '2019-02-25', '01/035', 'P900311', 'P900311', 'SA', 'P000009', 13824500, 0, 1, 1, 13824500, 0, 'Piutang SA', 13824500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900312', '2019-02-25', '01/043', 'P900312', 'P900312', 'SA', 'P000009', 5332000, 0, 1, 1, 5332000, 0, 'Piutang SA', 5332000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900313', '2019-02-25', '01/082', 'P900313', 'P900313', 'SA', 'P000009', 10900500, 0, 1, 1, 10900500, 0, 'Piutang SA', 10900500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900314', '2019-02-25', '01/149', 'P900314', 'P900314', 'SA', 'P000009', 2684000, 0, 1, 1, 2684000, 0, 'Piutang SA', 2684000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900315', '2019-02-25', '01/165', 'P900315', 'P900315', 'SA', 'P000009', 5038000, 0, 1, 1, 5038000, 0, 'Piutang SA', 5038000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900316', '2019-02-25', '01/170', 'P900316', 'P900316', 'SA', 'P000009', 4444000, 0, 1, 1, 4444000, 0, 'Piutang SA', 4444000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900317', '2019-02-25', '02/019', 'P900317', 'P900317', 'SA', 'P000009', 3450000, 0, 1, 1, 3450000, 0, 'Piutang SA', 3450000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900318', '2019-02-25', '02/023', 'P900318', 'P900318', 'SA', 'P000009', 9086000, 0, 1, 1, 9086000, 0, 'Piutang SA', 9086000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900319', '2019-02-25', '02/062', 'P900319', 'P900319', 'SA', 'P000009', 3300000, 0, 1, 1, 3300000, 0, 'Piutang SA', 3300000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900320', '2019-02-25', '02/083', 'P900320', 'P900320', 'SA', 'P000009', 15042000, 0, 1, 1, 15042000, 0, 'Piutang SA', 15042000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900321', '2019-02-25', '01/090', 'P900321', 'P900321', 'SA', 'P000009', 312000, 0, 1, 1, 312000, 0, 'Piutang SA', 312000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900322', '2019-02-25', '12/015', 'P900322', 'P900322', 'SA', 'P000009', 78500, 0, 1, 1, 78500, 0, 'Piutang SA', 78500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900323', '2019-02-25', '01/007', 'P900323', 'P900323', 'SA', 'P000009', 1305000, 0, 1, 1, 1305000, 0, 'Piutang SA', 1305000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900324', '2019-02-25', '01/040', 'P900324', 'P900324', 'SA', 'P000009', 18821750, 0, 1, 1, 18821750, 0, 'Piutang SA', 18821750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900325', '2019-02-25', '01/044', 'P900325', 'P900325', 'SA', 'P000009', 470050, 0, 1, 1, 470050, 0, 'Piutang SA', 470050);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900326', '2019-02-25', '02/009', 'P900326', 'P900326', 'SA', 'P000009', 21964000, 0, 1, 1, 21964000, 0, 'Piutang SA', 21964000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900327', '2019-02-25', '02/009', 'P900327', 'P900327', 'SA', 'P000009', 872100, 0, 1, 1, 872100, 0, 'Piutang SA', 872100);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900328', '2019-02-25', '02/009', 'P900328', 'P900328', 'SA', 'P000009', 193800, 0, 1, 1, 193800, 0, 'Piutang SA', 193800);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900329', '2019-02-25', '02/116', 'P900329', 'P900329', 'SA', 'P000009', 161500, 0, 1, 1, 161500, 0, 'Piutang SA', 161500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900330', '2019-02-25', '10/117', 'P900330', 'P900330', 'SA', 'P000009', 1277500, 0, 1, 1, 1277500, 0, 'Piutang SA', 1277500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900331', '2019-02-25', '10/156', 'P900331', 'P900331', 'SA', 'P000009', 5483500, 0, 1, 1, 5483500, 0, 'Piutang SA', 5483500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900332', '2019-02-25', '11/137', 'P900332', 'P900332', 'SA', 'P000009', 1380000, 0, 1, 1, 1380000, 0, 'Piutang SA', 1380000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900333', '2019-02-25', '12/032', 'P900333', 'P900333', 'SA', 'P000009', 2750000, 0, 1, 1, 2750000, 0, 'Piutang SA', 2750000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900334', '2019-02-25', '12/095', 'P900334', 'P900334', 'SA', 'P000009', 27251325, 0, 1, 1, 27251325, 0, 'Piutang SA', 27251325);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900335', '2019-02-25', '12/154', 'P900335', 'P900335', 'SA', 'P000009', 1241000, 0, 1, 1, 1241000, 0, 'Piutang SA', 1241000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900336', '2019-02-25', '01/086', 'P900336', 'P900336', 'SA', 'P000009', 1275000, 0, 1, 1, 1275000, 0, 'Piutang SA', 1275000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900337', '2019-02-25', '02/001', 'P900337', 'P900337', 'SA', 'P000009', 4471500, 0, 1, 1, 4471500, 0, 'Piutang SA', 4471500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900338', '2019-02-25', '02/129', 'P900338', 'P900338', 'SA', 'P000009', 5398750, 0, 1, 1, 5398750, 0, 'Piutang SA', 5398750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900339', '2019-02-25', '12/065', 'P900339', 'P900339', 'SA', 'P000009', 28736250, 0, 1, 1, 28736250, 0, 'Piutang SA', 28736250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900340', '2019-02-25', '01/128', 'P900340', 'P900340', 'SA', 'P000009', 18079000, 0, 1, 1, 18079000, 0, 'Piutang SA', 18079000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900341', '2019-02-25', '01/151', 'P900341', 'P900341', 'SA', 'P000009', 7090000, 0, 1, 1, 7090000, 0, 'Piutang SA', 7090000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900342', '2019-02-25', '01/152', 'P900342', 'P900342', 'SA', 'P000009', 825000, 0, 1, 1, 825000, 0, 'Piutang SA', 825000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900343', '2019-02-25', '02/092', 'P900343', 'P900343', 'SA', 'P000009', 214598500, 0, 1, 1, 214598500, 0, 'Piutang SA', 214598500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900344', '2019-02-25', '01/079', 'P900344', 'P900344', 'SA', 'P000009', 3773000, 0, 1, 1, 3773000, 0, 'Piutang SA', 3773000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900345', '2019-02-25', '02/004', 'P900345', 'P900345', 'SA', 'P000009', 21111000, 0, 1, 1, 21111000, 0, 'Piutang SA', 21111000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900346', '2019-02-25', '10/131', 'P900346', 'P900346', 'SA', 'P000009', 168000, 0, 1, 1, 168000, 0, 'Piutang SA', 168000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900347', '2019-02-25', '02/031', 'P900347', 'P900347', 'SA', 'P000009', 1632000, 0, 1, 1, 1632000, 0, 'Piutang SA', 1632000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900348', '2019-02-25', '06/010', 'P900348', 'P900348', 'SA', 'P000009', 11818750, 0, 1, 1, 11818750, 0, 'Piutang SA', 11818750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900349', '2019-02-25', '07/019', 'P900349', 'P900349', 'SA', 'P000009', 9037500, 0, 1, 1, 9037500, 0, 'Piutang SA', 9037500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900350', '2019-02-25', '07/032', 'P900350', 'P900350', 'SA', 'P000009', 6964000, 0, 1, 1, 6964000, 0, 'Piutang SA', 6964000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900351', '2019-02-25', '07/034', 'P900351', 'P900351', 'SA', 'P000009', 1680000, 0, 1, 1, 1680000, 0, 'Piutang SA', 1680000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900352', '2019-02-25', '07/207', 'P900352', 'P900352', 'SA', 'P000009', 2475000, 0, 1, 1, 2475000, 0, 'Piutang SA', 2475000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900353', '2019-02-25', '08/005', 'P900353', 'P900353', 'SA', 'P000009', 1125000, 0, 1, 1, 1125000, 0, 'Piutang SA', 1125000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900354', '2019-02-25', '08/011', 'P900354', 'P900354', 'SA', 'P000009', 1350000, 0, 1, 1, 1350000, 0, 'Piutang SA', 1350000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900355', '2019-02-25', '08/033', 'P900355', 'P900355', 'SA', 'P000009', 1343000, 0, 1, 1, 1343000, 0, 'Piutang SA', 1343000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900356', '2019-02-25', '08/043', 'P900356', 'P900356', 'SA', 'P000009', 43489500, 0, 1, 1, 43489500, 0, 'Piutang SA', 43489500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900357', '2019-02-25', '08/062', 'P900357', 'P900357', 'SA', 'P000009', 1181250, 0, 1, 1, 1181250, 0, 'Piutang SA', 1181250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900358', '2019-02-25', '08/067', 'P900358', 'P900358', 'SA', 'P000009', 4187000, 0, 1, 1, 4187000, 0, 'Piutang SA', 4187000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900359', '2019-02-25', '08/118', 'P900359', 'P900359', 'SA', 'P000009', 7141600, 0, 1, 1, 7141600, 0, 'Piutang SA', 7141600);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900360', '2019-02-25', '09/013', 'P900360', 'P900360', 'SA', 'P000009', 9141250, 0, 1, 1, 9141250, 0, 'Piutang SA', 9141250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900361', '2019-02-25', '09/022', 'P900361', 'P900361', 'SA', 'P000009', 6497750, 0, 1, 1, 6497750, 0, 'Piutang SA', 6497750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900362', '2019-02-25', '09/032', 'P900362', 'P900362', 'SA', 'P000009', 18609250, 0, 1, 1, 18609250, 0, 'Piutang SA', 18609250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900363', '2019-02-25', '09/101', 'P900363', 'P900363', 'SA', 'P000009', 4393500, 0, 1, 1, 4393500, 0, 'Piutang SA', 4393500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900364', '2019-02-25', '09/111', 'P900364', 'P900364', 'SA', 'P000009', 3653750, 0, 1, 1, 3653750, 0, 'Piutang SA', 3653750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900365', '2019-02-25', '09/145', 'P900365', 'P900365', 'SA', 'P000009', 5000500, 0, 1, 1, 5000500, 0, 'Piutang SA', 5000500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900366', '2019-02-25', '10/045', 'P900366', 'P900366', 'SA', 'P000009', 2942750, 0, 1, 1, 2942750, 0, 'Piutang SA', 2942750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900367', '2019-02-25', '10/081', 'P900367', 'P900367', 'SA', 'P000009', 20856000, 0, 1, 1, 20856000, 0, 'Piutang SA', 20856000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900368', '2019-02-25', '10/098', 'P900368', 'P900368', 'SA', 'P000009', 2250000, 0, 1, 1, 2250000, 0, 'Piutang SA', 2250000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900369', '2019-02-25', '10/130', 'P900369', 'P900369', 'SA', 'P000009', 34894250, 0, 1, 1, 34894250, 0, 'Piutang SA', 34894250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900370', '2019-02-25', '10/141', 'P900370', 'P900370', 'SA', 'P000009', 2531250, 0, 1, 1, 2531250, 0, 'Piutang SA', 2531250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900371', '2019-02-25', '10/177', 'P900371', 'P900371', 'SA', 'P000009', 46730500, 0, 1, 1, 46730500, 0, 'Piutang SA', 46730500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900372', '2019-02-25', '10/179', 'P900372', 'P900372', 'SA', 'P000009', 13557000, 0, 1, 1, 13557000, 0, 'Piutang SA', 13557000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900373', '2019-02-25', '11/095', 'P900373', 'P900373', 'SA', 'P000009', 11081250, 0, 1, 1, 11081250, 0, 'Piutang SA', 11081250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900374', '2019-02-25', '11/153', 'P900374', 'P900374', 'SA', 'P000009', 3862500, 0, 1, 1, 3862500, 0, 'Piutang SA', 3862500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900375', '2019-02-25', '12/053', 'P900375', 'P900375', 'SA', 'P000009', 7766750, 0, 1, 1, 7766750, 0, 'Piutang SA', 7766750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900376', '2019-02-25', '12/111', 'P900376', 'P900376', 'SA', 'P000009', 26149000, 0, 1, 1, 26149000, 0, 'Piutang SA', 26149000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900377', '2019-02-25', '12/132', 'P900377', 'P900377', 'SA', 'P000009', 2625000, 0, 1, 1, 2625000, 0, 'Piutang SA', 2625000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900378', '2019-02-25', '12/165', 'P900378', 'P900378', 'SA', 'P000009', 2550000, 0, 1, 1, 2550000, 0, 'Piutang SA', 2550000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900379', '2019-02-25', '01/047', 'P900379', 'P900379', 'SA', 'P000009', 5137500, 0, 1, 1, 5137500, 0, 'Piutang SA', 5137500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900380', '2019-02-25', '01/062', 'P900380', 'P900380', 'SA', 'P000009', 6754500, 0, 1, 1, 6754500, 0, 'Piutang SA', 6754500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900381', '2019-02-25', '01/067', 'P900381', 'P900381', 'SA', 'P000009', 7437000, 0, 1, 1, 7437000, 0, 'Piutang SA', 7437000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900382', '2019-02-25', '01/099', 'P900382', 'P900382', 'SA', 'P000009', 5690250, 0, 1, 1, 5690250, 0, 'Piutang SA', 5690250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900383', '2019-02-25', '01/154', 'P900383', 'P900383', 'SA', 'P000009', 6058250, 0, 1, 1, 6058250, 0, 'Piutang SA', 6058250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900384', '2019-02-25', '01/171', 'P900384', 'P900384', 'SA', 'P000009', 5120500, 0, 1, 1, 5120500, 0, 'Piutang SA', 5120500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900385', '2019-02-25', '01/176', 'P900385', 'P900385', 'SA', 'P000009', 13282500, 0, 1, 1, 13282500, 0, 'Piutang SA', 13282500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900386', '2019-02-25', '01/190', 'P900386', 'P900386', 'SA', 'P000009', 40581000, 0, 1, 1, 40581000, 0, 'Piutang SA', 40581000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900387', '2019-02-25', '02/005', 'P900387', 'P900387', 'SA', 'P000009', 4995500, 0, 1, 1, 4995500, 0, 'Piutang SA', 4995500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900388', '2019-02-25', '02/043', 'P900388', 'P900388', 'SA', 'P000009', 4142500, 0, 1, 1, 4142500, 0, 'Piutang SA', 4142500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900389', '2019-02-25', '02/114', 'P900389', 'P900389', 'SA', 'P000009', 6726500, 0, 1, 1, 6726500, 0, 'Piutang SA', 6726500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900390', '2019-02-25', '02/136', 'P900390', 'P900390', 'SA', 'P000009', 5913000, 0, 1, 1, 5913000, 0, 'Piutang SA', 5913000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900391', '2019-02-25', '09/094', 'P900391', 'P900391', 'SA', 'P000009', 3557500, 0, 1, 1, 3557500, 0, 'Piutang SA', 3557500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900392', '2019-02-25', '10/069', 'P900392', 'P900392', 'SA', 'P000009', 5937750, 0, 1, 1, 5937750, 0, 'Piutang SA', 5937750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900393', '2019-02-25', '10/112', 'P900393', 'P900393', 'SA', 'P000009', 3001500, 0, 1, 1, 3001500, 0, 'Piutang SA', 3001500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900394', '2019-02-25', '10/151', 'P900394', 'P900394', 'SA', 'P000009', 7395000, 0, 1, 1, 7395000, 0, 'Piutang SA', 7395000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900395', '2019-02-25', '11/007', 'P900395', 'P900395', 'SA', 'P000009', 52961250, 0, 1, 1, 52961250, 0, 'Piutang SA', 52961250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900396', '2019-02-25', '11/017', 'P900396', 'P900396', 'SA', 'P000009', 8308500, 0, 1, 1, 8308500, 0, 'Piutang SA', 8308500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900397', '2019-02-25', '11/088', 'P900397', 'P900397', 'SA', 'P000009', 9048000, 0, 1, 1, 9048000, 0, 'Piutang SA', 9048000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900398', '2019-02-25', '12/027', 'P900398', 'P900398', 'SA', 'P000009', 3306000, 0, 1, 1, 3306000, 0, 'Piutang SA', 3306000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900399', '2019-02-25', '12/054', 'P900399', 'P900399', 'SA', 'P000009', 9113250, 0, 1, 1, 9113250, 0, 'Piutang SA', 9113250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900400', '2019-02-25', '12/108', 'P900400', 'P900400', 'SA', 'P000009', 6958000, 0, 1, 1, 6958000, 0, 'Piutang SA', 6958000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900401', '2019-02-25', '12/110', 'P900401', 'P900401', 'SA', 'P000009', 9352500, 0, 1, 1, 9352500, 0, 'Piutang SA', 9352500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900402', '2019-02-25', '01/110', 'P900402', 'P900402', 'SA', 'P000009', 10190500, 0, 1, 1, 10190500, 0, 'Piutang SA', 10190500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900403', '2019-02-25', '02/073', 'P900403', 'P900403', 'SA', 'P000009', 1846750, 0, 1, 1, 1846750, 0, 'Piutang SA', 1846750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900404', '2019-02-25', '11/135', 'P900404', 'P900404', 'SA', 'P000009', 1468500, 0, 1, 1, 1468500, 0, 'Piutang SA', 1468500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900405', '2019-02-25', '12/159', 'P900405', 'P900405', 'SA', 'P000009', 3293000, 0, 1, 1, 3293000, 0, 'Piutang SA', 3293000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900406', '2019-02-25', '01/141', 'P900406', 'P900406', 'SA', 'P000009', 6450000, 0, 1, 1, 6450000, 0, 'Piutang SA', 6450000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900407', '2019-02-25', '10/106', 'P900407', 'P900407', 'SA', 'P000009', 6450000, 0, 1, 1, 6450000, 0, 'Piutang SA', 6450000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900408', '2019-02-25', '11/164', 'P900408', 'P900408', 'SA', 'P000009', 8300000, 0, 1, 1, 8300000, 0, 'Piutang SA', 8300000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900409', '2019-02-25', '12/073', 'P900409', 'P900409', 'SA', 'P000009', 3400000, 0, 1, 1, 3400000, 0, 'Piutang SA', 3400000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900410', '2019-02-25', '10/011', 'P900410', 'P900410', 'SA', 'P000009', 2673000, 0, 1, 1, 2673000, 0, 'Piutang SA', 2673000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900411', '2019-02-25', '10/020', 'P900411', 'P900411', 'SA', 'P000009', 74088000, 0, 1, 1, 74088000, 0, 'Piutang SA', 74088000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900412', '2019-02-25', '10/028', 'P900412', 'P900412', 'SA', 'P000009', 2546250, 0, 1, 1, 2546250, 0, 'Piutang SA', 2546250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900413', '2019-02-25', '10/065', 'P900413', 'P900413', 'SA', 'P000009', 7157500, 0, 1, 1, 7157500, 0, 'Piutang SA', 7157500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900414', '2019-02-25', '10/079', 'P900414', 'P900414', 'SA', 'P000009', 7262500, 0, 1, 1, 7262500, 0, 'Piutang SA', 7262500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900415', '2019-02-25', '10/118', 'P900415', 'P900415', 'SA', 'P000009', 5044500, 0, 1, 1, 5044500, 0, 'Piutang SA', 5044500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900416', '2019-02-25', '10/139', 'P900416', 'P900416', 'SA', 'P000009', 3071250, 0, 1, 1, 3071250, 0, 'Piutang SA', 3071250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900417', '2019-02-25', '10/161', 'P900417', 'P900417', 'SA', 'P000009', 7210000, 0, 1, 1, 7210000, 0, 'Piutang SA', 7210000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900418', '2019-02-25', '10/168', 'P900418', 'P900418', 'SA', 'P000009', 3500000, 0, 1, 1, 3500000, 0, 'Piutang SA', 3500000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900419', '2019-02-25', '11/035', 'P900419', 'P900419', 'SA', 'P000009', 5670000, 0, 1, 1, 5670000, 0, 'Piutang SA', 5670000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900420', '2019-02-25', '11/038', 'P900420', 'P900420', 'SA', 'P000009', 3118500, 0, 1, 1, 3118500, 0, 'Piutang SA', 3118500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900421', '2019-02-25', '11/043', 'P900421', 'P900421', 'SA', 'P000009', 10850000, 0, 1, 1, 10850000, 0, 'Piutang SA', 10850000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900422', '2019-02-25', '11/058', 'P900422', 'P900422', 'SA', 'P000009', 13073750, 0, 1, 1, 13073750, 0, 'Piutang SA', 13073750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900423', '2019-02-25', '11/061', 'P900423', 'P900423', 'SA', 'P000009', 2025000, 0, 1, 1, 2025000, 0, 'Piutang SA', 2025000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900424', '2019-02-25', '11/073', 'P900424', 'P900424', 'SA', 'P000009', 3010000, 0, 1, 1, 3010000, 0, 'Piutang SA', 3010000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900425', '2019-02-25', '11/090', 'P900425', 'P900425', 'SA', 'P000009', 1470000, 0, 1, 1, 1470000, 0, 'Piutang SA', 1470000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900426', '2019-02-25', '11/101', 'P900426', 'P900426', 'SA', 'P000009', 7875000, 0, 1, 1, 7875000, 0, 'Piutang SA', 7875000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900427', '2019-02-25', '11/104', 'P900427', 'P900427', 'SA', 'P000009', 3885000, 0, 1, 1, 3885000, 0, 'Piutang SA', 3885000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900428', '2019-02-25', '11/106', 'P900428', 'P900428', 'SA', 'P000009', 2673000, 0, 1, 1, 2673000, 0, 'Piutang SA', 2673000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900429', '2019-02-25', '11/112', 'P900429', 'P900429', 'SA', 'P000009', 4914675, 0, 1, 1, 4914675, 0, 'Piutang SA', 4914675);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900430', '2019-02-25', '11/146', 'P900430', 'P900430', 'SA', 'P000009', 92592500, 0, 1, 1, 92592500, 0, 'Piutang SA', 92592500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900431', '2019-02-25', '11/156', 'P900431', 'P900431', 'SA', 'P000009', 5775000, 0, 1, 1, 5775000, 0, 'Piutang SA', 5775000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900432', '2019-02-25', '12/001', 'P900432', 'P900432', 'SA', 'P000009', 4242000, 0, 1, 1, 4242000, 0, 'Piutang SA', 4242000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900433', '2019-02-25', '12/045', 'P900433', 'P900433', 'SA', 'P000009', 2905000, 0, 1, 1, 2905000, 0, 'Piutang SA', 2905000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900434', '2019-02-25', '12/082', 'P900434', 'P900434', 'SA', 'P000009', 8522500, 0, 1, 1, 8522500, 0, 'Piutang SA', 8522500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900435', '2019-02-25', '12/089', 'P900435', 'P900435', 'SA', 'P000009', 6892500, 0, 1, 1, 6892500, 0, 'Piutang SA', 6892500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900436', '2019-02-25', '12/106', 'P900436', 'P900436', 'SA', 'P000009', 1470000, 0, 1, 1, 1470000, 0, 'Piutang SA', 1470000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900437', '2019-02-25', '12/124', 'P900437', 'P900437', 'SA', 'P000009', 2940000, 0, 1, 1, 2940000, 0, 'Piutang SA', 2940000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900438', '2019-02-25', '12/131', 'P900438', 'P900438', 'SA', 'P000009', 2430000, 0, 1, 1, 2430000, 0, 'Piutang SA', 2430000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900439', '2019-02-25', '12/140', 'P900439', 'P900439', 'SA', 'P000009', 1620000, 0, 1, 1, 1620000, 0, 'Piutang SA', 1620000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900440', '2019-02-25', '12/166', 'P900440', 'P900440', 'SA', 'P000009', 1215000, 0, 1, 1, 1215000, 0, 'Piutang SA', 1215000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900441', '2019-02-25', '01/008', 'P900441', 'P900441', 'SA', 'P000009', 2730000, 0, 1, 1, 2730000, 0, 'Piutang SA', 2730000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900442', '2019-02-25', '01/024', 'P900442', 'P900442', 'SA', 'P000009', 1923750, 0, 1, 1, 1923750, 0, 'Piutang SA', 1923750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900443', '2019-02-25', '01/069', 'P900443', 'P900443', 'SA', 'P000009', 8662500, 0, 1, 1, 8662500, 0, 'Piutang SA', 8662500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900444', '2019-02-25', '01/070', 'P900444', 'P900444', 'SA', 'P000009', 1176000, 0, 1, 1, 1176000, 0, 'Piutang SA', 1176000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900445', '2019-02-25', '01/092', 'P900445', 'P900445', 'SA', 'P000009', 4420000, 0, 1, 1, 4420000, 0, 'Piutang SA', 4420000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900446', '2019-02-25', '01/117', 'P900446', 'P900446', 'SA', 'P000009', 454750, 0, 1, 1, 454750, 0, 'Piutang SA', 454750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900447', '2019-02-25', '01/127', 'P900447', 'P900447', 'SA', 'P000009', 5792500, 0, 1, 1, 5792500, 0, 'Piutang SA', 5792500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900448', '2019-02-25', '01/136', 'P900448', 'P900448', 'SA', 'P000009', 2614500, 0, 1, 1, 2614500, 0, 'Piutang SA', 2614500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900449', '2019-02-25', '01/147', 'P900449', 'P900449', 'SA', 'P000009', 2520000, 0, 1, 1, 2520000, 0, 'Piutang SA', 2520000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900450', '2019-02-25', '01/153', 'P900450', 'P900450', 'SA', 'P000009', 6702500, 0, 1, 1, 6702500, 0, 'Piutang SA', 6702500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900451', '2019-02-25', '01/155', 'P900451', 'P900451', 'SA', 'P000009', 1826000, 0, 1, 1, 1826000, 0, 'Piutang SA', 1826000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900452', '2019-02-25', '01/162', 'P900452', 'P900452', 'SA', 'P000009', 55449250, 0, 1, 1, 55449250, 0, 'Piutang SA', 55449250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900453', '2019-02-25', '01/163', 'P900453', 'P900453', 'SA', 'P000009', 17937500, 0, 1, 1, 17937500, 0, 'Piutang SA', 17937500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900454', '2019-02-25', '01/183', 'P900454', 'P900454', 'SA', 'P000009', 6622000, 0, 1, 1, 6622000, 0, 'Piutang SA', 6622000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900455', '2019-02-25', '01/189', 'P900455', 'P900455', 'SA', 'P000009', 2232500, 0, 1, 1, 2232500, 0, 'Piutang SA', 2232500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900456', '2019-02-25', '02/016', 'P900456', 'P900456', 'SA', 'P000009', 3745000, 0, 1, 1, 3745000, 0, 'Piutang SA', 3745000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900457', '2019-02-25', '02/025', 'P900457', 'P900457', 'SA', 'P000009', 6422500, 0, 1, 1, 6422500, 0, 'Piutang SA', 6422500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900458', '2019-02-25', '02/050', 'P900458', 'P900458', 'SA', 'P000009', 5845000, 0, 1, 1, 5845000, 0, 'Piutang SA', 5845000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900459', '2019-02-25', '02/067', 'P900459', 'P900459', 'SA', 'P000009', 3852000, 0, 1, 1, 3852000, 0, 'Piutang SA', 3852000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900460', '2019-02-25', '02/068', 'P900460', 'P900460', 'SA', 'P000009', 3727500, 0, 1, 1, 3727500, 0, 'Piutang SA', 3727500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900461', '2019-02-25', '02/077', 'P900461', 'P900461', 'SA', 'P000009', 3195500, 0, 1, 1, 3195500, 0, 'Piutang SA', 3195500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900462', '2019-02-25', '02/119', 'P900462', 'P900462', 'SA', 'P000009', 2568000, 0, 1, 1, 2568000, 0, 'Piutang SA', 2568000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900463', '2019-02-25', '02/122', 'P900463', 'P900463', 'SA', 'P000009', 5145000, 0, 1, 1, 5145000, 0, 'Piutang SA', 5145000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900464', '2019-02-25', '02/076', 'P900464', 'P900464', 'SA', 'P000009', 33853750, 0, 1, 1, 33853750, 0, 'Piutang SA', 33853750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900465', '2019-02-25', '02/102', 'P900465', 'P900465', 'SA', 'P000009', 29930000, 0, 1, 1, 29930000, 0, 'Piutang SA', 29930000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900466', '2019-02-25', '02/011', 'P900466', 'P900466', 'SA', 'P000009', 1617000, 0, 1, 1, 1617000, 0, 'Piutang SA', 1617000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900467', '2019-02-25', '02/014', 'P900467', 'P900467', 'SA', 'P000009', 2737500, 0, 1, 1, 2737500, 0, 'Piutang SA', 2737500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900468', '2019-02-25', '02/017', 'P900468', 'P900468', 'SA', 'P000009', 758500, 0, 1, 1, 758500, 0, 'Piutang SA', 758500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900469', '2019-02-25', '02/021', 'P900469', 'P900469', 'SA', 'P000009', 2517900, 0, 1, 1, 2517900, 0, 'Piutang SA', 2517900);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900470', '2019-02-25', '02/045', 'P900470', 'P900470', 'SA', 'P000009', 1342000, 0, 1, 1, 1342000, 0, 'Piutang SA', 1342000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900471', '2019-02-25', '02/072', 'P900471', 'P900471', 'SA', 'P000009', 1278000, 0, 1, 1, 1278000, 0, 'Piutang SA', 1278000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900472', '2019-02-25', '02/091', 'P900472', 'P900472', 'SA', 'P000009', 2013000, 0, 1, 1, 2013000, 0, 'Piutang SA', 2013000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900473', '2019-02-25', '02/096', 'P900473', 'P900473', 'SA', 'P000009', 5493250, 0, 1, 1, 5493250, 0, 'Piutang SA', 5493250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900474', '2019-02-25', '02/100', 'P900474', 'P900474', 'SA', 'P000009', 1914000, 0, 1, 1, 1914000, 0, 'Piutang SA', 1914000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900475', '2019-02-25', '02/124', 'P900475', 'P900475', 'SA', 'P000009', 2887500, 0, 1, 1, 2887500, 0, 'Piutang SA', 2887500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900476', '2019-02-25', '02/138', 'P900476', 'P900476', 'SA', 'P000009', 4890000, 0, 1, 1, 4890000, 0, 'Piutang SA', 4890000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900477', '2019-02-25', '10/066', 'P900477', 'P900477', 'SA', 'P000009', 3861000, 0, 1, 1, 3861000, 0, 'Piutang SA', 3861000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900478', '2019-02-25', '02/049', 'P900478', 'P900478', 'SA', 'P000009', 44596500, 0, 1, 1, 44596500, 0, 'Piutang SA', 44596500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900479', '2019-02-25', '11/184', 'P900479', 'P900479', 'SA', 'P000009', 7718500, 0, 1, 1, 7718500, 0, 'Piutang SA', 7718500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900480', '2019-02-25', '12/041', 'P900480', 'P900480', 'SA', 'P000009', 11996500, 0, 1, 1, 11996500, 0, 'Piutang SA', 11996500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900481', '2019-02-25', '02/060', 'P900481', 'P900481', 'SA', 'P000009', 8382000, 0, 1, 1, 8382000, 0, 'Piutang SA', 8382000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900482', '2019-02-25', '12/143', 'P900482', 'P900482', 'SA', 'P000009', 180687000, 0, 1, 1, 180687000, 0, 'Piutang SA', 180687000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900483', '2019-02-25', '12/126', 'P900483', 'P900483', 'SA', 'P000009', 28500000, 0, 1, 1, 28500000, 0, 'Piutang SA', 28500000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900484', '2019-02-25', '03/119', 'P900484', 'P900484', 'SA', 'P000009', 1155000, 0, 1, 1, 1155000, 0, 'Piutang SA', 1155000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900485', '2019-02-25', '03/188', 'P900485', 'P900485', 'SA', 'P000009', 6487250, 0, 1, 1, 6487250, 0, 'Piutang SA', 6487250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900486', '2019-02-25', '03/189', 'P900486', 'P900486', 'SA', 'P000009', 2700000, 0, 1, 1, 2700000, 0, 'Piutang SA', 2700000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900487', '2019-02-25', '03/190', 'P900487', 'P900487', 'SA', 'P000009', 22100750, 0, 1, 1, 22100750, 0, 'Piutang SA', 22100750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900488', '2019-02-25', '03/197', 'P900488', 'P900488', 'SA', 'P000009', 1405250, 0, 1, 1, 1405250, 0, 'Piutang SA', 1405250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900489', '2019-02-25', '04/041', 'P900489', 'P900489', 'SA', 'P000009', 14052500, 0, 1, 1, 14052500, 0, 'Piutang SA', 14052500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900490', '2019-02-25', '11/030', 'P900490', 'P900490', 'SA', 'P000009', 46429500, 0, 1, 1, 46429500, 0, 'Piutang SA', 46429500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900491', '2019-02-25', '11/042', 'P900491', 'P900491', 'SA', 'P000009', 1615000, 0, 1, 1, 1615000, 0, 'Piutang SA', 1615000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900492', '2019-02-25', '11/100', 'P900492', 'P900492', 'SA', 'P000009', 14000000, 0, 1, 1, 14000000, 0, 'Piutang SA', 14000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900493', '2019-02-25', '11/120', 'P900493', 'P900493', 'SA', 'P000009', 7298000, 0, 1, 1, 7298000, 0, 'Piutang SA', 7298000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900494', '2019-02-25', '12/018', 'P900494', 'P900494', 'SA', 'P000009', 10933250, 0, 1, 1, 10933250, 0, 'Piutang SA', 10933250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900495', '2019-02-25', '12/035', 'P900495', 'P900495', 'SA', 'P000009', 12577500, 0, 1, 1, 12577500, 0, 'Piutang SA', 12577500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900496', '2019-02-25', '12/039', 'P900496', 'P900496', 'SA', 'P000009', 3969000, 0, 1, 1, 3969000, 0, 'Piutang SA', 3969000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900497', '2019-02-25', '12/048', 'P900497', 'P900497', 'SA', 'P000009', 168000, 0, 1, 1, 168000, 0, 'Piutang SA', 168000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900498', '2019-02-25', '12/057', 'P900498', 'P900498', 'SA', 'P000009', 5666750, 0, 1, 1, 5666750, 0, 'Piutang SA', 5666750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900499', '2019-02-25', '12/060', 'P900499', 'P900499', 'SA', 'P000009', 61026750, 0, 1, 1, 61026750, 0, 'Piutang SA', 61026750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900500', '2019-02-25', '12/061', 'P900500', 'P900500', 'SA', 'P000009', 30178500, 0, 1, 1, 30178500, 0, 'Piutang SA', 30178500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900501', '2019-02-25', '12/120', 'P900501', 'P900501', 'SA', 'P000009', 2107000, 0, 1, 1, 2107000, 0, 'Piutang SA', 2107000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900502', '2019-02-25', '12/121', 'P900502', 'P900502', 'SA', 'P000009', 19762500, 0, 1, 1, 19762500, 0, 'Piutang SA', 19762500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900503', '2019-02-25', '12/134', 'P900503', 'P900503', 'SA', 'P000009', 8053500, 0, 1, 1, 8053500, 0, 'Piutang SA', 8053500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900504', '2019-02-25', '12/152', 'P900504', 'P900504', 'SA', 'P000009', 323000, 0, 1, 1, 323000, 0, 'Piutang SA', 323000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900505', '2019-02-25', '12/168', 'P900505', 'P900505', 'SA', 'P000009', 86000, 0, 1, 1, 86000, 0, 'Piutang SA', 86000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900506', '2019-02-25', '01/026', 'P900506', 'P900506', 'SA', 'P000009', 13650000, 0, 1, 1, 13650000, 0, 'Piutang SA', 13650000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900507', '2019-02-25', '01/039', 'P900507', 'P900507', 'SA', 'P000009', 6833750, 0, 1, 1, 6833750, 0, 'Piutang SA', 6833750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900508', '2019-02-25', '01/087', 'P900508', 'P900508', 'SA', 'P000009', 14309000, 0, 1, 1, 14309000, 0, 'Piutang SA', 14309000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900509', '2019-02-25', '01/096', 'P900509', 'P900509', 'SA', 'P000009', 9781100, 0, 1, 1, 9781100, 0, 'Piutang SA', 9781100);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900510', '2019-02-25', '01/160', 'P900510', 'P900510', 'SA', 'P000009', 6320000, 0, 1, 1, 6320000, 0, 'Piutang SA', 6320000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900511', '2019-02-25', '01/174', 'P900511', 'P900511', 'SA', 'P000009', 3895000, 0, 1, 1, 3895000, 0, 'Piutang SA', 3895000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900512', '2019-02-25', '02/029', 'P900512', 'P900512', 'SA', 'P000009', 656000, 0, 1, 1, 656000, 0, 'Piutang SA', 656000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900513', '2019-02-25', '02/037', 'P900513', 'P900513', 'SA', 'P000009', 27300000, 0, 1, 1, 27300000, 0, 'Piutang SA', 27300000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900514', '2019-02-25', '02/089', 'P900514', 'P900514', 'SA', 'P000009', 4095000, 0, 1, 1, 4095000, 0, 'Piutang SA', 4095000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900515', '2019-02-25', '02/120', 'P900515', 'P900515', 'SA', 'P000009', 4114500, 0, 1, 1, 4114500, 0, 'Piutang SA', 4114500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900516', '2019-02-25', '01/122', 'P900516', 'P900516', 'SA', 'P000009', 314500, 0, 1, 1, 314500, 0, 'Piutang SA', 314500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900517', '2019-02-25', '10/174', 'P900517', 'P900517', 'SA', 'P000009', 27060000, 0, 1, 1, 27060000, 0, 'Piutang SA', 27060000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900518', '2019-02-25', '11/011', 'P900518', 'P900518', 'SA', 'P000009', 3895000, 0, 1, 1, 3895000, 0, 'Piutang SA', 3895000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900519', '2019-02-25', '02/173', 'P900519', 'P900519', 'SA', 'P000009', 1782000, 0, 1, 1, 1782000, 0, 'Piutang SA', 1782000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900520', '2019-02-25', '03/200', 'P900520', 'P900520', 'SA', 'P000009', 7329000, 0, 1, 1, 7329000, 0, 'Piutang SA', 7329000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900521', '2019-02-25', '08/038', 'P900521', 'P900521', 'SA', 'P000009', 10584000, 0, 1, 1, 10584000, 0, 'Piutang SA', 10584000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900522', '2019-02-25', '02/128', 'P900522', 'P900522', 'SA', 'P000009', 1472000, 0, 1, 1, 1472000, 0, 'Piutang SA', 1472000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900523', '2019-02-25', '07/175', 'P900523', 'P900523', 'SA', 'P000009', 272250, 0, 1, 1, 272250, 0, 'Piutang SA', 272250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900524', '2019-02-25', '09/091', 'P900524', 'P900524', 'SA', 'P000009', 25295250, 0, 1, 1, 25295250, 0, 'Piutang SA', 25295250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900525', '2019-02-25', '10/013', 'P900525', 'P900525', 'SA', 'P000009', 3610500, 0, 1, 1, 3610500, 0, 'Piutang SA', 3610500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900526', '2019-02-25', '10/136', 'P900526', 'P900526', 'SA', 'P000009', 9463500, 0, 1, 1, 9463500, 0, 'Piutang SA', 9463500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900527', '2019-02-25', '10/157', 'P900527', 'P900527', 'SA', 'P000009', 440000, 0, 1, 1, 440000, 0, 'Piutang SA', 440000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900528', '2019-02-25', '11/033', 'P900528', 'P900528', 'SA', 'P000009', 2288000, 0, 1, 1, 2288000, 0, 'Piutang SA', 2288000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900529', '2019-02-25', '11/052', 'P900529', 'P900529', 'SA', 'P000009', 240000, 0, 1, 1, 240000, 0, 'Piutang SA', 240000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900530', '2019-02-25', '12/149', 'P900530', 'P900530', 'SA', 'P000009', 6746250, 0, 1, 1, 6746250, 0, 'Piutang SA', 6746250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900531', '2019-02-25', '05/011', 'P900531', 'P900531', 'SA', 'P000009', 1290500, 0, 1, 1, 1290500, 0, 'Piutang SA', 1290500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900532', '2019-02-25', '05/043', 'P900532', 'P900532', 'SA', 'P000009', 3915000, 0, 1, 1, 3915000, 0, 'Piutang SA', 3915000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900533', '2019-02-25', '05/063', 'P900533', 'P900533', 'SA', 'P000009', 40056000, 0, 1, 1, 40056000, 0, 'Piutang SA', 40056000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900534', '2019-02-25', '05/068', 'P900534', 'P900534', 'SA', 'P000009', 139400, 0, 1, 1, 139400, 0, 'Piutang SA', 139400);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900535', '2019-02-25', '05/107', 'P900535', 'P900535', 'SA', 'P000009', 2200000, 0, 1, 1, 2200000, 0, 'Piutang SA', 2200000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900536', '2019-02-25', '05/135', 'P900536', 'P900536', 'SA', 'P000009', 15018750, 0, 1, 1, 15018750, 0, 'Piutang SA', 15018750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900537', '2019-02-25', '05/168', 'P900537', 'P900537', 'SA', 'P000009', 7104000, 0, 1, 1, 7104000, 0, 'Piutang SA', 7104000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900538', '2019-02-25', '05/195', 'P900538', 'P900538', 'SA', 'P000009', 2610000, 0, 1, 1, 2610000, 0, 'Piutang SA', 2610000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900539', '2019-02-25', '05/198', 'P900539', 'P900539', 'SA', 'P000009', 3115000, 0, 1, 1, 3115000, 0, 'Piutang SA', 3115000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900540', '2019-02-25', '05/234', 'P900540', 'P900540', 'SA', 'P000009', 13982500, 0, 1, 1, 13982500, 0, 'Piutang SA', 13982500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900541', '2019-02-25', '06/053', 'P900541', 'P900541', 'SA', 'P000009', 1360000, 0, 1, 1, 1360000, 0, 'Piutang SA', 1360000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900542', '2019-02-25', '06/068', 'P900542', 'P900542', 'SA', 'P000009', 26372500, 0, 1, 1, 26372500, 0, 'Piutang SA', 26372500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900543', '2019-02-25', '06/075', 'P900543', 'P900543', 'SA', 'P000009', 41736000, 0, 1, 1, 41736000, 0, 'Piutang SA', 41736000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900544', '2019-02-25', '07/026', 'P900544', 'P900544', 'SA', 'P000009', 249280, 0, 1, 1, 249280, 0, 'Piutang SA', 249280);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900545', '2019-02-25', '07/047', 'P900545', 'P900545', 'SA', 'P000009', 5930750, 0, 1, 1, 5930750, 0, 'Piutang SA', 5930750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900546', '2019-02-25', '07/052', 'P900546', 'P900546', 'SA', 'P000009', 129044500, 0, 1, 1, 129044500, 0, 'Piutang SA', 129044500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900547', '2019-02-25', '07/064', 'P900547', 'P900547', 'SA', 'P000009', 4050000, 0, 1, 1, 4050000, 0, 'Piutang SA', 4050000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900548', '2019-02-25', '07/070', 'P900548', 'P900548', 'SA', 'P000009', 3026000, 0, 1, 1, 3026000, 0, 'Piutang SA', 3026000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900549', '2019-02-25', '07/089', 'P900549', 'P900549', 'SA', 'P000009', 4611000, 0, 1, 1, 4611000, 0, 'Piutang SA', 4611000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900550', '2019-02-25', '07/115', 'P900550', 'P900550', 'SA', 'P000009', 1246000, 0, 1, 1, 1246000, 0, 'Piutang SA', 1246000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900551', '2019-02-25', '07/128', 'P900551', 'P900551', 'SA', 'P000009', 2016000, 0, 1, 1, 2016000, 0, 'Piutang SA', 2016000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900552', '2019-02-25', '07/131', 'P900552', 'P900552', 'SA', 'P000009', 8612500, 0, 1, 1, 8612500, 0, 'Piutang SA', 8612500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900553', '2019-02-25', '07/144', 'P900553', 'P900553', 'SA', 'P000009', 124640, 0, 1, 1, 124640, 0, 'Piutang SA', 124640);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900554', '2019-02-25', '07/169', 'P900554', 'P900554', 'SA', 'P000009', 2714500, 0, 1, 1, 2714500, 0, 'Piutang SA', 2714500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900555', '2019-02-25', '07/215', 'P900555', 'P900555', 'SA', 'P000009', 832500, 0, 1, 1, 832500, 0, 'Piutang SA', 832500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900556', '2019-02-25', '07/218', 'P900556', 'P900556', 'SA', 'P000009', 10658000, 0, 1, 1, 10658000, 0, 'Piutang SA', 10658000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900557', '2019-02-25', '08/055', 'P900557', 'P900557', 'SA', 'P000009', 42099250, 0, 1, 1, 42099250, 0, 'Piutang SA', 42099250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900558', '2019-02-25', '08/060', 'P900558', 'P900558', 'SA', 'P000009', 7575750, 0, 1, 1, 7575750, 0, 'Piutang SA', 7575750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900559', '2019-02-25', '08/079', 'P900559', 'P900559', 'SA', 'P000009', 29710250, 0, 1, 1, 29710250, 0, 'Piutang SA', 29710250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900560', '2019-02-25', '08/101', 'P900560', 'P900560', 'SA', 'P000009', 3003000, 0, 1, 1, 3003000, 0, 'Piutang SA', 3003000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900561', '2019-02-25', '08/155', 'P900561', 'P900561', 'SA', 'P000009', 287000, 0, 1, 1, 287000, 0, 'Piutang SA', 287000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900562', '2019-02-25', '08/166', 'P900562', 'P900562', 'SA', 'P000009', 8158500, 0, 1, 1, 8158500, 0, 'Piutang SA', 8158500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900563', '2019-02-25', '08/224', 'P900563', 'P900563', 'SA', 'P000009', 33957000, 0, 1, 1, 33957000, 0, 'Piutang SA', 33957000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900564', '2019-02-25', '09/023', 'P900564', 'P900564', 'SA', 'P000009', 2670000, 0, 1, 1, 2670000, 0, 'Piutang SA', 2670000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900565', '2019-02-25', '09/028', 'P900565', 'P900565', 'SA', 'P000009', 211225, 0, 1, 1, 211225, 0, 'Piutang SA', 211225);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900566', '2019-02-25', '09/035', 'P900566', 'P900566', 'SA', 'P000009', 392275, 0, 1, 1, 392275, 0, 'Piutang SA', 392275);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900567', '2019-02-25', '09/055', 'P900567', 'P900567', 'SA', 'P000009', 22825287.5, 0, 1, 1, 22825287.5, 0, 'Piutang SA', 22825287.5);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900568', '2019-02-25', '09/074', 'P900568', 'P900568', 'SA', 'P000009', 4400000, 0, 1, 1, 4400000, 0, 'Piutang SA', 4400000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900569', '2019-02-25', '10/002', 'P900569', 'P900569', 'SA', 'P000009', 8160000, 0, 1, 1, 8160000, 0, 'Piutang SA', 8160000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900570', '2019-02-25', '10/031', 'P900570', 'P900570', 'SA', 'P000009', 2937000, 0, 1, 1, 2937000, 0, 'Piutang SA', 2937000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900571', '2019-02-25', '10/056', 'P900571', 'P900571', 'SA', 'P000009', 68250, 0, 1, 1, 68250, 0, 'Piutang SA', 68250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900572', '2019-02-25', '10/058', 'P900572', 'P900572', 'SA', 'P000009', 23521750, 0, 1, 1, 23521750, 0, 'Piutang SA', 23521750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900573', '2019-02-25', '10/078', 'P900573', 'P900573', 'SA', 'P000009', 2730000, 0, 1, 1, 2730000, 0, 'Piutang SA', 2730000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900574', '2019-02-25', '10/138', 'P900574', 'P900574', 'SA', 'P000009', 1512000, 0, 1, 1, 1512000, 0, 'Piutang SA', 1512000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900575', '2019-02-25', '10/178', 'P900575', 'P900575', 'SA', 'P000009', 37430000, 0, 1, 1, 37430000, 0, 'Piutang SA', 37430000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900576', '2019-02-25', '11/013', 'P900576', 'P900576', 'SA', 'P000009', 7507500, 0, 1, 1, 7507500, 0, 'Piutang SA', 7507500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900577', '2019-02-25', '11/020', 'P900577', 'P900577', 'SA', 'P000009', 14498750, 0, 1, 1, 14498750, 0, 'Piutang SA', 14498750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900578', '2019-02-25', '11/022', 'P900578', 'P900578', 'SA', 'P000009', 2376000, 0, 1, 1, 2376000, 0, 'Piutang SA', 2376000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900579', '2019-02-25', '11/026', 'P900579', 'P900579', 'SA', 'P000009', 5328000, 0, 1, 1, 5328000, 0, 'Piutang SA', 5328000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900580', '2019-02-25', '11/044', 'P900580', 'P900580', 'SA', 'P000009', 3082500, 0, 1, 1, 3082500, 0, 'Piutang SA', 3082500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900581', '2019-02-25', '11/056', 'P900581', 'P900581', 'SA', 'P000009', 24378750, 0, 1, 1, 24378750, 0, 'Piutang SA', 24378750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900582', '2019-02-25', '11/077', 'P900582', 'P900582', 'SA', 'P000009', 131200, 0, 1, 1, 131200, 0, 'Piutang SA', 131200);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900583', '2019-02-25', '11/091', 'P900583', 'P900583', 'SA', 'P000009', 2848000, 0, 1, 1, 2848000, 0, 'Piutang SA', 2848000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900584', '2019-02-25', '11/117', 'P900584', 'P900584', 'SA', 'P000009', 1319500, 0, 1, 1, 1319500, 0, 'Piutang SA', 1319500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900585', '2019-02-25', '11/144', 'P900585', 'P900585', 'SA', 'P000009', 14331250, 0, 1, 1, 14331250, 0, 'Piutang SA', 14331250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900586', '2019-02-25', '11/163', 'P900586', 'P900586', 'SA', 'P000009', 14240000, 0, 1, 1, 14240000, 0, 'Piutang SA', 14240000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900587', '2019-02-25', '12/059', 'P900587', 'P900587', 'SA', 'P000009', 14553000, 0, 1, 1, 14553000, 0, 'Piutang SA', 14553000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900588', '2019-02-25', '12/067', 'P900588', 'P900588', 'SA', 'P000009', 25161500, 0, 1, 1, 25161500, 0, 'Piutang SA', 25161500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900589', '2019-02-25', '12/078', 'P900589', 'P900589', 'SA', 'P000009', 4737500, 0, 1, 1, 4737500, 0, 'Piutang SA', 4737500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900590', '2019-02-25', '12/084', 'P900590', 'P900590', 'SA', 'P000009', 4231500, 0, 1, 1, 4231500, 0, 'Piutang SA', 4231500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900591', '2019-02-25', '12/103', 'P900591', 'P900591', 'SA', 'P000009', 9468000, 0, 1, 1, 9468000, 0, 'Piutang SA', 9468000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900592', '2019-02-25', '12/163', 'P900592', 'P900592', 'SA', 'P000009', 7939750, 0, 1, 1, 7939750, 0, 'Piutang SA', 7939750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900593', '2019-02-25', '01/023', 'P900593', 'P900593', 'SA', 'P000009', 4361000, 0, 1, 1, 4361000, 0, 'Piutang SA', 4361000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900594', '2019-02-25', '01/101', 'P900594', 'P900594', 'SA', 'P000009', 1313500, 0, 1, 1, 1313500, 0, 'Piutang SA', 1313500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900595', '2019-02-25', '01/107', 'P900595', 'P900595', 'SA', 'P000009', 4975500, 0, 1, 1, 4975500, 0, 'Piutang SA', 4975500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900596', '2019-02-25', '01/108', 'P900596', 'P900596', 'SA', 'P000009', 9323250, 0, 1, 1, 9323250, 0, 'Piutang SA', 9323250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900597', '2019-02-25', '01/124', 'P900597', 'P900597', 'SA', 'P000009', 442800, 0, 1, 1, 442800, 0, 'Piutang SA', 442800);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900598', '2019-02-25', '01/143', 'P900598', 'P900598', 'SA', 'P000009', 8393250, 0, 1, 1, 8393250, 0, 'Piutang SA', 8393250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900599', '2019-02-25', '01/172', 'P900599', 'P900599', 'SA', 'P000009', 1366587.5, 0, 1, 1, 1366587.5, 0, 'Piutang SA', 1366587.5);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900600', '2019-02-25', '02/003', 'P900600', 'P900600', 'SA', 'P000009', 107835, 0, 1, 1, 107835, 0, 'Piutang SA', 107835);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900601', '2019-02-25', '02/027', 'P900601', 'P900601', 'SA', 'P000009', 612637.5, 0, 1, 1, 612637.5, 0, 'Piutang SA', 612637.5);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900602', '2019-02-25', '02/038', 'P900602', 'P900602', 'SA', 'P000009', 1813500, 0, 1, 1, 1813500, 0, 'Piutang SA', 1813500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900603', '2019-02-25', '02/055', 'P900603', 'P900603', 'SA', 'P000009', 8184000, 0, 1, 1, 8184000, 0, 'Piutang SA', 8184000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900604', '2019-02-25', '02/112', 'P900604', 'P900604', 'SA', 'P000009', 54167750, 0, 1, 1, 54167750, 0, 'Piutang SA', 54167750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900605', '2019-02-25', '02/121', 'P900605', 'P900605', 'SA', 'P000009', 1634500, 0, 1, 1, 1634500, 0, 'Piutang SA', 1634500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900606', '2019-02-25', '02/127', 'P900606', 'P900606', 'SA', 'P000009', 4237812.5, 0, 1, 1, 4237812.5, 0, 'Piutang SA', 4237812.5);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900607', '2019-02-25', '02/139', 'P900607', 'P900607', 'SA', 'P000009', 355725, 0, 1, 1, 355725, 0, 'Piutang SA', 355725);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900608', '2019-02-25', '02/140', 'P900608', 'P900608', 'SA', 'P000009', 9282000, 0, 1, 1, 9282000, 0, 'Piutang SA', 9282000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900609', '2019-02-25', '11/160', 'P900609', 'P900609', 'SA', 'P000009', 26201250, 0, 1, 1, 26201250, 0, 'Piutang SA', 26201250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900610', '2019-02-25', '11/165', 'P900610', 'P900610', 'SA', 'P000009', 46980000, 0, 1, 1, 46980000, 0, 'Piutang SA', 46980000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900611', '2019-02-25', '01/135', 'P900611', 'P900611', 'SA', 'P000009', 48949500, 0, 1, 1, 48949500, 0, 'Piutang SA', 48949500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900612', '2019-02-25', '02/071', 'P900612', 'P900612', 'SA', 'P000009', 25946250, 0, 1, 1, 25946250, 0, 'Piutang SA', 25946250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900613', '2019-02-25', '10/149', 'P900613', 'P900613', 'SA', 'P000009', 11041500, 0, 1, 1, 11041500, 0, 'Piutang SA', 11041500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900614', '2019-02-25', '10/165', 'P900614', 'P900614', 'SA', 'P000009', 5925000, 0, 1, 1, 5925000, 0, 'Piutang SA', 5925000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900615', '2019-02-25', '11/069', 'P900615', 'P900615', 'SA', 'P000009', 8823000, 0, 1, 1, 8823000, 0, 'Piutang SA', 8823000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900616', '2019-02-25', '11/134', 'P900616', 'P900616', 'SA', 'P000009', 8560000, 0, 1, 1, 8560000, 0, 'Piutang SA', 8560000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900617', '2019-02-25', '11/176', 'P900617', 'P900617', 'SA', 'P000009', 2830500, 0, 1, 1, 2830500, 0, 'Piutang SA', 2830500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900618', '2019-02-25', '01/088', 'P900618', 'P900618', 'SA', 'P000009', 210000, 0, 1, 1, 210000, 0, 'Piutang SA', 210000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900619', '2019-02-25', '01/132', 'P900619', 'P900619', 'SA', 'P000009', 204000, 0, 1, 1, 204000, 0, 'Piutang SA', 204000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900620', '2019-02-25', '02/104', 'P900620', 'P900620', 'SA', 'P000009', 903000, 0, 1, 1, 903000, 0, 'Piutang SA', 903000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900621', '2019-02-25', '01/188', 'P900621', 'P900621', 'SA', 'P000009', 168000000, 0, 1, 1, 168000000, 0, 'Piutang SA', 168000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900622', '2019-02-25', '02/088', 'P900622', 'P900622', 'SA', 'P000009', 79640000, 0, 1, 1, 79640000, 0, 'Piutang SA', 79640000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900623', '2019-02-25', '01/139', 'P900623', 'P900623', 'SA', 'P000009', 8702000, 0, 1, 1, 8702000, 0, 'Piutang SA', 8702000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900624', '2019-02-25', '02/034', 'P900624', 'P900624', 'SA', 'P000009', 5925000, 0, 1, 1, 5925000, 0, 'Piutang SA', 5925000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900625', '2019-02-25', '02/070', 'P900625', 'P900625', 'SA', 'P000009', 2622000, 0, 1, 1, 2622000, 0, 'Piutang SA', 2622000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900626', '2019-02-25', '09/136', 'P900626', 'P900626', 'SA', 'P000009', 4027510, 0, 1, 1, 4027510, 0, 'Piutang SA', 4027510);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900627', '2019-02-25', '11/123', 'P900627', 'P900627', 'SA', 'P000009', 110687400, 0, 1, 1, 110687400, 0, 'Piutang SA', 110687400);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900628', '2019-02-25', '11/129', 'P900628', 'P900628', 'SA', 'P000009', 3186000, 0, 1, 1, 3186000, 0, 'Piutang SA', 3186000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900629', '2019-02-25', '12/023', 'P900629', 'P900629', 'SA', 'P000009', 26163360, 0, 1, 1, 26163360, 0, 'Piutang SA', 26163360);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900630', '2019-02-25', '12/109', 'P900630', 'P900630', 'SA', 'P000009', 48770280, 0, 1, 1, 48770280, 0, 'Piutang SA', 48770280);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900631', '2019-02-25', '12/146', 'P900631', 'P900631', 'SA', 'P000009', 59597640, 0, 1, 1, 59597640, 0, 'Piutang SA', 59597640);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900632', '2019-02-25', '01/133', 'P900632', 'P900632', 'SA', 'P000009', 316098360, 0, 1, 1, 316098360, 0, 'Piutang SA', 316098360);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900633', '2019-02-25', '02/018', 'P900633', 'P900633', 'SA', 'P000009', 210852000, 0, 1, 1, 210852000, 0, 'Piutang SA', 210852000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900634', '2019-02-25', '02/028', 'P900634', 'P900634', 'SA', 'P000009', 3200040, 0, 1, 1, 3200040, 0, 'Piutang SA', 3200040);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900635', '2019-02-25', '02/105', 'P900635', 'P900635', 'SA', 'P000009', 2660000, 0, 1, 1, 2660000, 0, 'Piutang SA', 2660000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900636', '2019-02-25', '01/089', 'P900636', 'P900636', 'SA', 'P000009', 132750, 0, 1, 1, 132750, 0, 'Piutang SA', 132750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900637', '2019-02-25', '01/097', 'P900637', 'P900637', 'SA', 'P000011', 77542425, 0, 1, 1, 77542425, 0, 'Piutang SA', 77542425);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900638', '2019-02-25', '01/173', 'P900638', 'P900638', 'SA', 'P000009', 121019400, 0, 1, 1, 121019400, 0, 'Piutang SA', 121019400);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900639', '2019-02-25', '01/045', 'P900639', 'P900639', 'SA', 'P000009', 276250, 0, 1, 1, 276250, 0, 'Piutang SA', 276250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900640', '2019-02-25', '01/073', 'P900640', 'P900640', 'SA', 'P000009', 311500, 0, 1, 1, 311500, 0, 'Piutang SA', 311500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900641', '2019-02-25', '01/125', 'P900641', 'P900641', 'SA', 'P000009', 85000, 0, 1, 1, 85000, 0, 'Piutang SA', 85000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900642', '2019-02-25', '02/047', 'P900642', 'P900642', 'SA', 'P000009', 624750, 0, 1, 1, 624750, 0, 'Piutang SA', 624750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900643', '2019-02-25', '09/027', 'P900643', 'P900643', 'SA', 'P000009', 6497750, 0, 1, 1, 6497750, 0, 'Piutang SA', 6497750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900644', '2019-02-25', '09/043', 'P900644', 'P900644', 'SA', 'P000009', 11484250, 0, 1, 1, 11484250, 0, 'Piutang SA', 11484250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900645', '2019-02-25', '09/051', 'P900645', 'P900645', 'SA', 'P000009', 2946750, 0, 1, 1, 2946750, 0, 'Piutang SA', 2946750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900646', '2019-02-25', '09/054', 'P900646', 'P900646', 'SA', 'P000009', 1617000, 0, 1, 1, 1617000, 0, 'Piutang SA', 1617000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900647', '2019-02-25', '09/065', 'P900647', 'P900647', 'SA', 'P000009', 3523500, 0, 1, 1, 3523500, 0, 'Piutang SA', 3523500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900648', '2019-02-25', '09/069', 'P900648', 'P900648', 'SA', 'P000009', 3555000, 0, 1, 1, 3555000, 0, 'Piutang SA', 3555000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900649', '2019-02-25', '09/078', 'P900649', 'P900649', 'SA', 'P000009', 12495000, 0, 1, 1, 12495000, 0, 'Piutang SA', 12495000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900650', '2019-02-25', '09/142', 'P900650', 'P900650', 'SA', 'P000009', 18657000, 0, 1, 1, 18657000, 0, 'Piutang SA', 18657000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900651', '2019-02-25', '09/155', 'P900651', 'P900651', 'SA', 'P000009', 57072000, 0, 1, 1, 57072000, 0, 'Piutang SA', 57072000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900652', '2019-02-25', '10/024', 'P900652', 'P900652', 'SA', 'P000009', 10183500, 0, 1, 1, 10183500, 0, 'Piutang SA', 10183500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900653', '2019-02-25', '10/034', 'P900653', 'P900653', 'SA', 'P000009', 4740000, 0, 1, 1, 4740000, 0, 'Piutang SA', 4740000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900654', '2019-02-25', '10/135', 'P900654', 'P900654', 'SA', 'P000009', 3759000, 0, 1, 1, 3759000, 0, 'Piutang SA', 3759000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900655', '2019-02-25', '10/159', 'P900655', 'P900655', 'SA', 'P000009', 2343000, 0, 1, 1, 2343000, 0, 'Piutang SA', 2343000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900656', '2019-02-25', '10/180', 'P900656', 'P900656', 'SA', 'P000009', 13370750, 0, 1, 1, 13370750, 0, 'Piutang SA', 13370750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900657', '2019-02-25', '11/046', 'P900657', 'P900657', 'SA', 'P000009', 6720000, 0, 1, 1, 6720000, 0, 'Piutang SA', 6720000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900658', '2019-02-25', '11/076', 'P900658', 'P900658', 'SA', 'P000009', 5922000, 0, 1, 1, 5922000, 0, 'Piutang SA', 5922000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900659', '2019-02-25', '11/114', 'P900659', 'P900659', 'SA', 'P000009', 11058250, 0, 1, 1, 11058250, 0, 'Piutang SA', 11058250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900660', '2019-02-25', '11/118', 'P900660', 'P900660', 'SA', 'P000009', 10420750, 0, 1, 1, 10420750, 0, 'Piutang SA', 10420750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900661', '2019-02-25', '11/159', 'P900661', 'P900661', 'SA', 'P000009', 3208000, 0, 1, 1, 3208000, 0, 'Piutang SA', 3208000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900662', '2019-02-25', '12/030', 'P900662', 'P900662', 'SA', 'P000009', 4759750, 0, 1, 1, 4759750, 0, 'Piutang SA', 4759750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900663', '2019-02-25', '12/046', 'P900663', 'P900663', 'SA', 'P000009', 24068500, 0, 1, 1, 24068500, 0, 'Piutang SA', 24068500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900664', '2019-02-25', '12/066', 'P900664', 'P900664', 'SA', 'P000009', 1165250, 0, 1, 1, 1165250, 0, 'Piutang SA', 1165250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900665', '2019-02-25', '12/074', 'P900665', 'P900665', 'SA', 'P000009', 8981500, 0, 1, 1, 8981500, 0, 'Piutang SA', 8981500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900666', '2019-02-25', '12/080', 'P900666', 'P900666', 'SA', 'P000009', 3887250, 0, 1, 1, 3887250, 0, 'Piutang SA', 3887250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900667', '2019-02-25', '12/083', 'P900667', 'P900667', 'SA', 'P000009', 18087250, 0, 1, 1, 18087250, 0, 'Piutang SA', 18087250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900668', '2019-02-25', '12/085', 'P900668', 'P900668', 'SA', 'P000009', 11271250, 0, 1, 1, 11271250, 0, 'Piutang SA', 11271250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900669', '2019-02-25', '12/125', 'P900669', 'P900669', 'SA', 'P000009', 18140500, 0, 1, 1, 18140500, 0, 'Piutang SA', 18140500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900670', '2019-02-25', '12/141', 'P900670', 'P900670', 'SA', 'P000009', 6419000, 0, 1, 1, 6419000, 0, 'Piutang SA', 6419000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900671', '2019-02-25', '12/148', 'P900671', 'P900671', 'SA', 'P000009', 6933500, 0, 1, 1, 6933500, 0, 'Piutang SA', 6933500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900672', '2019-02-25', '12/164', 'P900672', 'P900672', 'SA', 'P000009', 4117750, 0, 1, 1, 4117750, 0, 'Piutang SA', 4117750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900673', '2019-02-25', '01/012', 'P900673', 'P900673', 'SA', 'P000009', 36803500, 0, 1, 1, 36803500, 0, 'Piutang SA', 36803500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900674', '2019-02-25', '01/038', 'P900674', 'P900674', 'SA', 'P000009', 4753000, 0, 1, 1, 4753000, 0, 'Piutang SA', 4753000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900675', '2019-02-25', '01/060', 'P900675', 'P900675', 'SA', 'P000009', 3185000, 0, 1, 1, 3185000, 0, 'Piutang SA', 3185000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900676', '2019-02-25', '01/094', 'P900676', 'P900676', 'SA', 'P000009', 3900000, 0, 1, 1, 3900000, 0, 'Piutang SA', 3900000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900677', '2019-02-25', '01/102', 'P900677', 'P900677', 'SA', 'P000009', 12173500, 0, 1, 1, 12173500, 0, 'Piutang SA', 12173500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900678', '2019-02-25', '01/109', 'P900678', 'P900678', 'SA', 'P000009', 21772250, 0, 1, 1, 21772250, 0, 'Piutang SA', 21772250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900679', '2019-02-25', '01/119', 'P900679', 'P900679', 'SA', 'P000009', 9161500, 0, 1, 1, 9161500, 0, 'Piutang SA', 9161500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900680', '2019-02-25', '01/181', 'P900680', 'P900680', 'SA', 'P000009', 3400000, 0, 1, 1, 3400000, 0, 'Piutang SA', 3400000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900681', '2019-02-25', '01/191', 'P900681', 'P900681', 'SA', 'P000009', 3412750, 0, 1, 1, 3412750, 0, 'Piutang SA', 3412750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900682', '2019-02-25', '02/057', 'P900682', 'P900682', 'SA', 'P000009', 22640000, 0, 1, 1, 22640000, 0, 'Piutang SA', 22640000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900683', '2019-02-25', '02/059', 'P900683', 'P900683', 'SA', 'P000009', 2430000, 0, 1, 1, 2430000, 0, 'Piutang SA', 2430000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900684', '2019-02-25', '02/078', 'P900684', 'P900684', 'SA', 'P000009', 18870500, 0, 1, 1, 18870500, 0, 'Piutang SA', 18870500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900685', '2019-02-25', '02/086', 'P900685', 'P900685', 'SA', 'P000009', 8942500, 0, 1, 1, 8942500, 0, 'Piutang SA', 8942500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900686', '2019-02-25', '02/097', 'P900686', 'P900686', 'SA', 'P000009', 11570500, 0, 1, 1, 11570500, 0, 'Piutang SA', 11570500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900687', '2019-02-25', '02/113', 'P900687', 'P900687', 'SA', 'P000009', 4947000, 0, 1, 1, 4947000, 0, 'Piutang SA', 4947000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900688', '2019-02-25', '02/126', 'P900688', 'P900688', 'SA', 'P000009', 2745000, 0, 1, 1, 2745000, 0, 'Piutang SA', 2745000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900689', '2019-02-25', '02/125', 'P900689', 'P900689', 'SA', 'P000009', 42322500, 0, 1, 1, 42322500, 0, 'Piutang SA', 42322500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900690', '2019-02-25', '10/057', 'P900690', 'P900690', 'SA', 'P000009', 11550000, 0, 1, 1, 11550000, 0, 'Piutang SA', 11550000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900691', '2019-02-25', '10/172', 'P900691', 'P900691', 'SA', 'P000009', 3753750, 0, 1, 1, 3753750, 0, 'Piutang SA', 3753750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900692', '2019-02-25', '01/034', 'P900692', 'P900692', 'SA', 'P000009', 4207500, 0, 1, 1, 4207500, 0, 'Piutang SA', 4207500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900693', '2019-02-25', '01/074', 'P900693', 'P900693', 'SA', 'P000009', 5524750, 0, 1, 1, 5524750, 0, 'Piutang SA', 5524750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900694', '2019-02-25', '08/090', 'P900694', 'P900694', 'SA', 'P000009', 6336000, 0, 1, 1, 6336000, 0, 'Piutang SA', 6336000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900695', '2019-02-25', '12/069', 'P900695', 'P900695', 'SA', 'P000009', 3960000, 0, 1, 1, 3960000, 0, 'Piutang SA', 3960000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900696', '2019-02-25', '02/052', 'P900696', 'P900696', 'SA', 'P000009', 1470000, 0, 1, 1, 1470000, 0, 'Piutang SA', 1470000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900697', '2019-02-25', '02/130', 'P900697', 'P900697', 'SA', 'P000009', 3995000, 0, 1, 1, 3995000, 0, 'Piutang SA', 3995000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900698', '2019-02-25', '01/071', 'P900698', 'P900698', 'SA', 'P000009', 2871000, 0, 1, 1, 2871000, 0, 'Piutang SA', 2871000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900699', '2019-02-25', '02/030', 'P900699', 'P900699', 'SA', 'P000009', 12061500, 0, 1, 1, 12061500, 0, 'Piutang SA', 12061500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900700', '2019-02-25', '02/143', 'P900700', 'P900700', 'SA', 'P000009', 1575000, 0, 1, 1, 1575000, 0, 'Piutang SA', 1575000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900701', '2019-02-25', '08/162', 'P900701', 'P900701', 'SA', 'P000009', 3152500, 0, 1, 1, 3152500, 0, 'Piutang SA', 3152500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900702', '2019-02-25', '09/002', 'P900702', 'P900702', 'SA', 'P000009', 4947000, 0, 1, 1, 4947000, 0, 'Piutang SA', 4947000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900703', '2019-02-25', '11/180', 'P900703', 'P900703', 'SA', 'P000009', 17120500, 0, 1, 1, 17120500, 0, 'Piutang SA', 17120500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900704', '2019-02-25', '02/046', 'P900704', 'P900704', 'SA', 'P000009', 11112750, 0, 1, 1, 11112750, 0, 'Piutang SA', 11112750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900705', '2019-02-25', '12/047', 'P900705', 'P900705', 'SA', 'P000009', 5654500, 0, 1, 1, 5654500, 0, 'Piutang SA', 5654500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900706', '2019-02-25', '12/104', 'P900706', 'P900706', 'SA', 'P000009', 3528000, 0, 1, 1, 3528000, 0, 'Piutang SA', 3528000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900707', '2019-02-25', '08/063', 'P900707', 'P900707', 'SA', 'P000009', 3750000, 0, 1, 1, 3750000, 0, 'Piutang SA', 3750000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900708', '2019-02-25', '10/083', 'P900708', 'P900708', 'SA', 'P000009', 4075000, 0, 1, 1, 4075000, 0, 'Piutang SA', 4075000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900709', '2019-02-25', '12/119', 'P900709', 'P900709', 'SA', 'P000009', 6500000, 0, 1, 1, 6500000, 0, 'Piutang SA', 6500000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900710', '2019-02-25', '12/167', 'P900710', 'P900710', 'SA', 'P000009', 1320000, 0, 1, 1, 1320000, 0, 'Piutang SA', 1320000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900711', '2019-02-25', '01/055', 'P900711', 'P900711', 'SA', 'P000009', 9888000, 0, 1, 1, 9888000, 0, 'Piutang SA', 9888000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900712', '2019-02-25', '01/093', 'P900712', 'P900712', 'SA', 'P000009', 1067000, 0, 1, 1, 1067000, 0, 'Piutang SA', 1067000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900713', '2019-02-25', '01/113', 'P900713', 'P900713', 'SA', 'P000009', 39645000, 0, 1, 1, 39645000, 0, 'Piutang SA', 39645000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900714', '2019-02-25', '01/100', 'P900714', 'P900714', 'SA', 'P000009', 9000000, 0, 1, 1, 9000000, 0, 'Piutang SA', 9000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900715', '2019-02-25', '12/077', 'P900715', 'P900715', 'SA', 'P000009', 1600500, 0, 1, 1, 1600500, 0, 'Piutang SA', 1600500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900716', '2019-02-25', '12/081', 'P900716', 'P900716', 'SA', 'P000009', 57048050, 0, 1, 1, 57048050, 0, 'Piutang SA', 57048050);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900717', '2019-02-25', '01/078', 'P900717', 'P900717', 'SA', 'P000009', 31646250, 0, 1, 1, 31646250, 0, 'Piutang SA', 31646250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900718', '2019-02-25', '01/138', 'P900718', 'P900718', 'SA', 'P000009', 30663750, 0, 1, 1, 30663750, 0, 'Piutang SA', 30663750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900719', '2019-02-25', '02/013', 'P900719', 'P900719', 'SA', 'P000009', 13038750, 0, 1, 1, 13038750, 0, 'Piutang SA', 13038750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900720', '2019-02-25', '01/120', 'P900720', 'P900720', 'SA', 'P000009', 17267250, 0, 1, 1, 17267250, 0, 'Piutang SA', 17267250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900721', '2019-02-25', '11/119', 'P900721', 'P900721', 'SA', 'P000009', 7707000, 0, 1, 1, 7707000, 0, 'Piutang SA', 7707000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900722', '2019-02-25', '02/101', 'P900722', 'P900722', 'SA', 'P000009', 2365000, 0, 1, 1, 2365000, 0, 'Piutang SA', 2365000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900723', '2019-02-25', '08/048', 'P900723', 'P900723', 'SA', 'P000009', 35500000, 0, 1, 1, 35500000, 0, 'Piutang SA', 35500000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900724', '2019-02-25', '08/068', 'P900724', 'P900724', 'SA', 'P000009', 91666325, 0, 1, 1, 91666325, 0, 'Piutang SA', 91666325);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900725', '2019-02-25', '08/106', 'P900725', 'P900725', 'SA', 'P000009', 35695250, 0, 1, 1, 35695250, 0, 'Piutang SA', 35695250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900726', '2019-02-25', '08/109', 'P900726', 'P900726', 'SA', 'P000009', 35872750, 0, 1, 1, 35872750, 0, 'Piutang SA', 35872750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900727', '2019-02-25', '08/140', 'P900727', 'P900727', 'SA', 'P000009', 20678750, 0, 1, 1, 20678750, 0, 'Piutang SA', 20678750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900728', '2019-02-25', '08/158', 'P900728', 'P900728', 'SA', 'P000009', 31524000, 0, 1, 1, 31524000, 0, 'Piutang SA', 31524000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900729', '2019-02-25', '08/169', 'P900729', 'P900729', 'SA', 'P000009', 13223750, 0, 1, 1, 13223750, 0, 'Piutang SA', 13223750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900730', '2019-02-25', '08/212', 'P900730', 'P900730', 'SA', 'P000009', 16170250, 0, 1, 1, 16170250, 0, 'Piutang SA', 16170250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900731', '2019-02-25', '09/003', 'P900731', 'P900731', 'SA', 'P000009', 28729750, 0, 1, 1, 28729750, 0, 'Piutang SA', 28729750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900732', '2019-02-25', '09/046', 'P900732', 'P900732', 'SA', 'P000009', 37479400, 0, 1, 1, 37479400, 0, 'Piutang SA', 37479400);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900733', '2019-02-25', '09/084', 'P900733', 'P900733', 'SA', 'P000009', 34288500, 0, 1, 1, 34288500, 0, 'Piutang SA', 34288500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900734', '2019-02-25', '10/100', 'P900734', 'P900734', 'SA', 'P000009', 51045400, 0, 1, 1, 51045400, 0, 'Piutang SA', 51045400);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900735', '2019-02-25', '11/001', 'P900735', 'P900735', 'SA', 'P000009', 65398000, 0, 1, 1, 65398000, 0, 'Piutang SA', 65398000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900736', '2019-02-25', '11/079', 'P900736', 'P900736', 'SA', 'P000009', 43092000, 0, 1, 1, 43092000, 0, 'Piutang SA', 43092000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900737', '2019-02-25', '11/097', 'P900737', 'P900737', 'SA', 'P000009', 14972000, 0, 1, 1, 14972000, 0, 'Piutang SA', 14972000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900738', '2019-02-25', '11/148', 'P900738', 'P900738', 'SA', 'P000009', 17760000, 0, 1, 1, 17760000, 0, 'Piutang SA', 17760000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900739', '2019-02-25', '12/012', 'P900739', 'P900739', 'SA', 'P000009', 46838750, 0, 1, 1, 46838750, 0, 'Piutang SA', 46838750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900740', '2019-02-25', '12/051', 'P900740', 'P900740', 'SA', 'P000009', 69008000, 0, 1, 1, 69008000, 0, 'Piutang SA', 69008000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900741', '2019-02-25', '12/063', 'P900741', 'P900741', 'SA', 'P000009', 12825000, 0, 1, 1, 12825000, 0, 'Piutang SA', 12825000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900742', '2019-02-25', '12/123', 'P900742', 'P900742', 'SA', 'P000009', 22173000, 0, 1, 1, 22173000, 0, 'Piutang SA', 22173000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900743', '2019-02-25', '12/139', 'P900743', 'P900743', 'SA', 'P000009', 12046000, 0, 1, 1, 12046000, 0, 'Piutang SA', 12046000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900744', '2019-02-25', '12/157', 'P900744', 'P900744', 'SA', 'P000009', 113328250, 0, 1, 1, 113328250, 0, 'Piutang SA', 113328250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900745', '2019-02-25', '01/013', 'P900745', 'P900745', 'SA', 'P000009', 17957250, 0, 1, 1, 17957250, 0, 'Piutang SA', 17957250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900746', '2019-02-25', '01/017', 'P900746', 'P900746', 'SA', 'P000009', 67824750, 0, 1, 1, 67824750, 0, 'Piutang SA', 67824750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900747', '2019-02-25', '01/037', 'P900747', 'P900747', 'SA', 'P000009', 36062000, 0, 1, 1, 36062000, 0, 'Piutang SA', 36062000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900748', '2019-02-25', '01/054', 'P900748', 'P900748', 'SA', 'P000009', 20256000, 0, 1, 1, 20256000, 0, 'Piutang SA', 20256000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900749', '2019-02-25', '01/130', 'P900749', 'P900749', 'SA', 'P000009', 34428000, 0, 1, 1, 34428000, 0, 'Piutang SA', 34428000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900750', '2019-02-25', '01/179', 'P900750', 'P900750', 'SA', 'P000009', 30210000, 0, 1, 1, 30210000, 0, 'Piutang SA', 30210000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900751', '2019-02-25', '02/022', 'P900751', 'P900751', 'SA', 'P000009', 77425000, 0, 1, 1, 77425000, 0, 'Piutang SA', 77425000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900752', '2019-02-25', '02/118', 'P900752', 'P900752', 'SA', 'P000009', 21859500, 0, 1, 1, 21859500, 0, 'Piutang SA', 21859500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900753', '2019-02-25', '02/123', 'P900753', 'P900753', 'SA', 'P000009', 12538500, 0, 1, 1, 12538500, 0, 'Piutang SA', 12538500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900754', '2019-02-25', '09/026', 'P900754', 'P900754', 'SA', 'P000009', 9658500, 0, 1, 1, 9658500, 0, 'Piutang SA', 9658500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900755', '2019-02-25', '09/079', 'P900755', 'P900755', 'SA', 'P000009', 19507500, 0, 1, 1, 19507500, 0, 'Piutang SA', 19507500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900756', '2019-02-25', '09/122', 'P900756', 'P900756', 'SA', 'P000009', 8062000, 0, 1, 1, 8062000, 0, 'Piutang SA', 8062000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900757', '2019-02-25', '10/038', 'P900757', 'P900757', 'SA', 'P000009', 6554000, 0, 1, 1, 6554000, 0, 'Piutang SA', 6554000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900758', '2019-02-25', '10/167', 'P900758', 'P900758', 'SA', 'P000009', 18592000, 0, 1, 1, 18592000, 0, 'Piutang SA', 18592000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900759', '2019-02-25', '11/122', 'P900759', 'P900759', 'SA', 'P000009', 15143000, 0, 1, 1, 15143000, 0, 'Piutang SA', 15143000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900760', '2019-02-25', '11/161', 'P900760', 'P900760', 'SA', 'P000009', 6923000, 0, 1, 1, 6923000, 0, 'Piutang SA', 6923000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900761', '2019-02-25', '12/042', 'P900761', 'P900761', 'SA', 'P000009', 12968000, 0, 1, 1, 12968000, 0, 'Piutang SA', 12968000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900762', '2019-02-25', '12/096', 'P900762', 'P900762', 'SA', 'P000009', 119967500, 0, 1, 1, 119967500, 0, 'Piutang SA', 119967500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900763', '2019-02-25', '12/116', 'P900763', 'P900763', 'SA', 'P000009', 6232000, 0, 1, 1, 6232000, 0, 'Piutang SA', 6232000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900764', '2019-02-25', '01/021', 'P900764', 'P900764', 'SA', 'P000009', 18357000, 0, 1, 1, 18357000, 0, 'Piutang SA', 18357000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900765', '2019-02-25', '01/080', 'P900765', 'P900765', 'SA', 'P000009', 7144000, 0, 1, 1, 7144000, 0, 'Piutang SA', 7144000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900766', '2019-02-25', '01/180', 'P900766', 'P900766', 'SA', 'P000009', 16550000, 0, 1, 1, 16550000, 0, 'Piutang SA', 16550000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900767', '2019-02-25', '09/030', 'P900767', 'P900767', 'SA', 'P000009', 10123750, 0, 1, 1, 10123750, 0, 'Piutang SA', 10123750);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900768', '2019-02-25', '10/111', 'P900768', 'P900768', 'SA', 'P000009', 9278250, 0, 1, 1, 9278250, 0, 'Piutang SA', 9278250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900769', '2019-02-25', '01/142', 'P900769', 'P900769', 'SA', 'P000009', 7462000, 0, 1, 1, 7462000, 0, 'Piutang SA', 7462000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900770', '2019-02-25', '02/098', 'P900770', 'P900770', 'SA', 'P000009', 6506500, 0, 1, 1, 6506500, 0, 'Piutang SA', 6506500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900771', '2019-02-25', '12/008', 'P900771', 'P900771', 'SA', 'P000011', 8282250, 0, 1, 1, 8282250, 0, 'Piutang SA', 8282250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900772', '2019-02-25', '12/022', 'P900772', 'P900772', 'SA', 'P000011', 9386500, 0, 1, 1, 9386500, 0, 'Piutang SA', 9386500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900773', '2019-02-25', '12/026', 'P900773', 'P900773', 'SA', 'P000011', 9345000, 0, 1, 1, 9345000, 0, 'Piutang SA', 9345000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900774', '2019-02-25', '12/038', 'P900774', 'P900774', 'SA', 'P000011', 146458400, 0, 1, 1, 146458400, 0, 'Piutang SA', 146458400);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900775', '2019-02-25', '12/055', 'P900775', 'P900775', 'SA', 'P000011', 20057262.5, 0, 1, 1, 20057262.5, 0, 'Piutang SA', 20057262.5);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900776', '2019-02-25', '12/090', 'P900776', 'P900776', 'SA', 'P000011', 10440000, 0, 1, 1, 10440000, 0, 'Piutang SA', 10440000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900777', '2019-02-25', '12/117', 'P900777', 'P900777', 'SA', 'P000011', 6675000, 0, 1, 1, 6675000, 0, 'Piutang SA', 6675000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900778', '2019-02-25', '01/009', 'P900778', 'P900778', 'SA', 'P000011', 20570000, 0, 1, 1, 20570000, 0, 'Piutang SA', 20570000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900779', '2019-02-25', '01/028', 'P900779', 'P900779', 'SA', 'P000011', 13016250, 0, 1, 1, 13016250, 0, 'Piutang SA', 13016250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900780', '2019-02-25', '01/063', 'P900780', 'P900780', 'SA', 'P000011', 23807500, 0, 1, 1, 23807500, 0, 'Piutang SA', 23807500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900781', '2019-02-25', '01/081', 'P900781', 'P900781', 'SA', 'P000011', 10327500, 0, 1, 1, 10327500, 0, 'Piutang SA', 10327500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900782', '2019-02-25', '01/164', 'P900782', 'P900782', 'SA', 'P000011', 12285000, 0, 1, 1, 12285000, 0, 'Piutang SA', 12285000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900783', '2019-02-25', '01/177', 'P900783', 'P900783', 'SA', 'P000011', 10356250, 0, 1, 1, 10356250, 0, 'Piutang SA', 10356250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900784', '2019-02-25', '01/194', 'P900784', 'P900784', 'SA', 'P000011', 18632250, 0, 1, 1, 18632250, 0, 'Piutang SA', 18632250);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900785', '2019-02-25', '02/036', 'P900785', 'P900785', 'SA', 'P000011', 9464000, 0, 1, 1, 9464000, 0, 'Piutang SA', 9464000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900786', '2019-02-25', '02/069', 'P900786', 'P900786', 'SA', 'P000011', 7982000, 0, 1, 1, 7982000, 0, 'Piutang SA', 7982000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900787', '2019-02-25', '02/075', 'P900787', 'P900787', 'SA', 'P000011', 9282000, 0, 1, 1, 9282000, 0, 'Piutang SA', 9282000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900788', '2019-02-25', '01/173', 'P900788', 'P900788', 'SA', 'P000011', 184000, 0, 1, 1, 184000, 0, 'Piutang SA', 184000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900789', '2019-02-25', '12/087', 'P900789', 'P900789', 'SA', 'P000011', 75145000, 0, 1, 1, 75145000, 0, 'Piutang SA', 75145000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900790', '2019-02-25', '12/098', 'P900790', 'P900790', 'SA', 'P000011', 3610000, 0, 1, 1, 3610000, 0, 'Piutang SA', 3610000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900791', '2019-02-25', '12/160', 'P900791', 'P900791', 'SA', 'P000011', 110200000, 0, 1, 1, 110200000, 0, 'Piutang SA', 110200000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900792', '2019-02-25', '01/029', 'P900792', 'P900792', 'SA', 'P000011', 3800000, 0, 1, 1, 3800000, 0, 'Piutang SA', 3800000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900793', '2019-02-25', '01/159', 'P900793', 'P900793', 'SA', 'P000011', 234000, 0, 1, 1, 234000, 0, 'Piutang SA', 234000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900794', '2019-02-25', '02/115', 'P900794', 'P900794', 'SA', 'P000011', 45112500, 0, 1, 1, 45112500, 0, 'Piutang SA', 45112500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900795', '2019-02-25', '02/134', 'P900795', 'P900795', 'SA', 'P000011', 2902500, 0, 1, 1, 2902500, 0, 'Piutang SA', 2902500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900796', '2019-03-06', 'BB19PMKA00003', 'P000002', 'P000003', 'BB', 'P000014', 115008500, 0, 1, 14000, 115008500, 0, 'PPH 21  RISNADI T KRISNADI TALAR  
', 115008500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900797', '2019-03-06', 'BB19PMKA00003', 'P000002', 'P000003', 'BB', 'P000004', 0, 0, 1, 14000, 0, 0, NULL, 0);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900798', '2019-03-05', 'BB19PMKA00003', 'P000003', 'P000004', 'BB', 'P000014', 115008500, 0, 1, 14000, 115008500, 0, 'PPH 21  RISNADI T KRISNADI TALAR  
', 115008500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P900799', '2019-03-05', 'BB19PMKA00003', 'P000003', 'P000004', 'BB', 'P000004', 115008500, 0, 1, 14000, 115008500, 0, NULL, 115008500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079a', '2019-03-06', 'BB19PMKA00004', 'P000004', 'P000005', 'BB', 'P000634', 0, 31328690, 1, 14000, 0, 31328690, 'KAS NEGARA / PPH 25 MASA FEB''19  PRIMA MODA
', 31328690);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079b', '2019-03-06', 'BB19PMKA00004', 'P000004', 'P000006', 'BB', 'P000632', 0, 28984004, 1, 14000, 0, 28984004, 'KAS NEGARA / PPH 21 MASA FEB''19  PRIMA MODA (GAJI KARY & KOMISI )
', 28984004);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079c', '2019-03-06', 'BB19PMKA00004', 'P000004', 'P000007', 'BB', 'P000633', 0, 509046, 1, 14000, 0, 509046, 'KAS NEGARA / PPH 23 MASA FEB''19  PRIMA MODA

', 509046);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079d', '2019-03-06', 'BB19PMKA00004', 'P000004', 'P000007', 'BB', 'P000004', 0, 60821740, 1, 14000, 0, 60821740, NULL, 60821740);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079e', '2019-04-02', 'BB19PMKA00005', 'P000005', 'P000008', 'BB', 'P000622', 0, 6195000, 1, 14000, 0, 6195000, 'PT. MANDIRI TUNAS FINANCE / BCA : EH 623146 / CICILAN KE 2
', 6195000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079f', '2019-04-02', 'BB19PMKA00005', 'P000005', 'P000008', 'BB', 'P000004', 0, 6195000, 1, 14000, 0, 6195000, NULL, 6195000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079g', '2019-04-02', 'BB19PMKA00005', 'P000005', 'P000009', 'BB', 'P000405', 6195000, 0, 1, 14000, 0, 6195000, 'PT. MANDIRI TUNAS FINANCE / BCA : EH 623146 / CICILAN KE 2
', 6195000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079h', '2019-04-02', 'BB19PMKA00005', 'P000005', 'P000009', 'BB', 'P000004', 0, 6195000, 1, 14000, 0, 6195000, NULL, 6195000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079i', '2019-04-02', 'BB19PMKA00005', 'P000005', 'P00000a', 'BB', 'P000405', 6195000, 0, 1, 14000, 0, 6195000, 'PT. MANDIRI TUNAS FINANCE / BCA : EH 623146 / CICILAN KE 2
', 6195000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079j', '2019-04-02', 'BB19PMKA00005', 'P000005', 'P00000a', 'BB', 'P000004', 0, 6195000, 1, 14000, 0, 6195000, NULL, 6195000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079k', '2019-05-07', 'PB19PMKA00001', 'P000001', 'P000001', 'PB', 'P000018', 1587500, 0, 1, 14000, 1587500, 0, '', 1587500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079l', '2019-05-07', 'PB19PMKA00001', 'P000001', 'P000001', 'PB', 'P000021', 0, 1587500, 1, 14000, 0, 1587500, '', 1587500);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079m', '2019-05-07', 'PB19PMKA00001', 'P000001', 'P000001', 'PB', 'P000018', 1651000, 0, 1, 14000, 1651000, 0, '', 1651000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079n', '2019-05-07', 'PB19PMKA00001', 'P000001', 'P000001', 'PB', 'P000021', 0, 1651000, 1, 14000, 0, 1651000, '', 1651000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079o', '2019-05-07', 'PB19PMKA00002', 'P000002', 'P000001', 'PB', 'P000018', 1730375, 0, 1, 14000, 1730375, 0, '', 1730375);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079p', '2019-05-07', 'PB19PMKA00002', 'P000002', 'P000001', 'PB', 'P000021', 0, 1730375, 1, 14000, 0, 1730375, '', 1730375);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079q', '2019-05-07', 'PB19PMKA00003', 'P000003', 'P000002', 'PB', 'P000018', 0, 0, 1, 14000, 0, 0, '', 0);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079r', '2019-05-07', 'PB19PMKA00003', 'P000003', 'P000002', 'PB', 'P000021', 0, 0, 1, 14000, 0, 0, '', 0);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079s', '2019-06-10', 'PBA1900001', 'P000001', 'P000002', 'PBA', 'P000218', 10000000, 0, 1, 14000, 10000000, 0, '-', 10000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079t', '2019-06-10', 'PBA1900001', 'P000001', 'P000002', 'PBA', 'P000218', 0, 10000000, 1, 14000, 0, 10000000, '-', 10000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079u', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079v', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079w', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079x', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079y', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P90079z', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a0', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a1', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a2', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a3', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a4', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a5', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'sdsadawd', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a6', '2019-06-10', 'INVAST1900001', 'P000001', 'P000002', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadasdwa', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a7', '2019-06-10', 'INVAST1900001', 'P000001', 'P000002', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadasdwa', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a8', '2019-06-10', 'INVAST1900001', 'P000001', 'P000003', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadasdwa', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007a9', '2019-06-10', 'INVAST1900001', 'P000001', 'P000003', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadasdwa', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007aa', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ab', '2019-06-10', 'INVAST1900001', 'P000001', 'P000001', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ac', '2019-06-10', 'INVAST1900001', 'P000001', 'P000002', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ad', '2019-06-10', 'INVAST1900001', 'P000001', 'P000002', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ae', '2019-06-10', 'INVAST1900001', 'P000001', 'P000003', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007af', '2019-06-10', 'INVAST1900001', 'P000001', 'P000003', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ag', '2019-06-10', 'INVAST1900001', 'P000001', 'P000004', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ah', '2019-06-10', 'INVAST1900001', 'P000001', 'P000004', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ai', NULL, NULL, 'P000001', NULL, 'INVAST', NULL, 0, 0, 1, 14000, 0, 0, NULL, 0);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007aj', NULL, NULL, 'P000001', NULL, 'INVAST', NULL, 0, 0, 1, 14000, 0, 0, NULL, 0);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ak', NULL, NULL, 'P000001', NULL, 'INVAST', NULL, 0, 0, 1, 14000, 0, 0, NULL, 0);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007al', NULL, NULL, 'P000001', NULL, 'INVAST', NULL, 0, 0, 1, 14000, 0, 0, NULL, 0);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007am', '2019-06-10', 'INVAST1900001', 'P000001', 'P000005', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007an', '2019-06-10', 'INVAST1900001', 'P000001', 'P000005', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ao', '2019-06-10', 'INVAST1900001', 'P000001', 'P000006', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ap', '2019-06-10', 'INVAST1900001', 'P000001', 'P000006', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007aq', '2019-06-10', 'INVAST1900001', 'P000001', 'P000007', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ar', '2019-06-10', 'INVAST1900001', 'P000001', 'P000007', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007as', '2019-06-10', 'INVAST1900001', 'P000001', 'P000008', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007at', '2019-06-10', 'INVAST1900001', 'P000001', 'P000008', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007au', '2019-06-10', 'INVAST1900001', 'P000001', 'P000009', 'INVAST', 'P000628', 0, 11000000, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007av', '2019-06-10', 'INVAST1900001', 'P000001', 'P000009', 'INVAST', 'P000218', 11000000, 0, 1, 14000, 0, 11000000, 'dsadas', 11000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007aw', '2019-06-11', 'PBA1900002', 'P000002', 'P000003', 'PBA', 'P000213', 200000000, 0, 1, 14000, 200000000, 0, '-', 200000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ax', '2019-06-11', 'PBA1900002', 'P000002', 'P000003', 'PBA', 'P000213', 0, 200000000, 1, 14000, 0, 200000000, '-', 200000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007ay', '2019-06-11', 'PBA1900002', 'P000002', 'P000004', 'PBA', 'P000218', 10000000, 0, 1, 14000, 10000000, 0, '-', 10000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P9007az', '2019-06-11', 'PBA1900002', 'P000002', 'P000004', 'PBA', 'P000218', 0, 10000000, 1, 14000, 0, 10000000, '-', 10000000);

-- ----------------------------
-- Table structure for tbl_jurnal_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal_umum";
CREATE TABLE "public"."tbl_jurnal_umum" (
  "IDJU" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_debit" float8,
  "Total_kredit" float8
)
;

-- ----------------------------
-- Records of tbl_jurnal_umum
-- ----------------------------
INSERT INTO "public"."tbl_jurnal_umum" VALUES ('P000001', '2019-03-31', 'JU19PMKA00001', 'aktif', 1001478, 1001478);
INSERT INTO "public"."tbl_jurnal_umum" VALUES ('P000002', '2019-03-31', 'JU19PMKA00002', 'aktif', 663227, 663227);

-- ----------------------------
-- Table structure for tbl_jurnal_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal_umum_detail";
CREATE TABLE "public"."tbl_jurnal_umum_detail" (
  "IDJUDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDJU" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8,
  "Kredit" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_jurnal_umum_detail
-- ----------------------------
INSERT INTO "public"."tbl_jurnal_umum_detail" VALUES ('P000001', 'P000001', 'P000032', 'Kredit', 0, 1001478, 1, 1, 'Amortisasi bulanan');
INSERT INTO "public"."tbl_jurnal_umum_detail" VALUES ('P000002', 'P000002', 'P000596', 'Debet', 663227, 0, 1, 1, 'Bunga Leasing');
INSERT INTO "public"."tbl_jurnal_umum_detail" VALUES ('P000003', 'P000002', 'P000622', 'Kredit', 0, 663227, 1, 1, 'Bunga Leasing');

-- ----------------------------
-- Table structure for tbl_kartu_stok
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kartu_stok";
CREATE TABLE "public"."tbl_kartu_stok" (
  "IDKartuStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDKartuStok_seq"'::regclass),
  "Tanggal" date,
  "Nomor_faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDFaktur_seq"'::regclass),
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDFakturDetail_seq"'::regclass),
  "IDStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDStok_seq"'::regclass),
  "Jenis_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDWarna_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDGudang_seq"'::regclass),
  "Masuk_yard" float8,
  "Masuk_meter" float8,
  "Keluar_yard" float8,
  "Keluar_meter" float8,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDMataUang_seq"'::regclass),
  "Kurs" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8
)
;

-- ----------------------------
-- Records of tbl_kartu_stok
-- ----------------------------
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000001', '2019-05-07', 'PB19PMKA00001', 'P000001', 'P000001', 'P000001', 'PB', '0419PMK03447410', 'P000004', 'P000005', 'P000052', '0', 50, 45.72, 0, 0, 'A', 'P000004', 31750, 1, '', 1587500);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000002', '2019-05-07', 'PB19PMKA00001', 'P000001', 'P000001', 'P000002', 'PB', '0419PMK03447412', 'P000004', 'P000005', 'P000052', '0', 52, 47.55, 0, 0, 'A', 'P000004', 31750, 1, '', 1651000);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000003', '2019-05-07', 'PB19PMKA00002', 'P000002', 'P000001', 'P000003', 'PB', '0419PMK03447411', 'P000004', 'P000005', 'P000052', '0', 54.5, 49.83, 0, 0, 'A', 'P000004', 31750, 1, '', 1730375);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000004', '2019-05-07', 'PB19PMKA00003', 'P000003', 'P000002', 'P000004', 'PB', '0419PMK03447412', 'P00000o', 'P000008', 'P00055h', '0', 250, 235, 0, 0, 'A', 'P000004', 0, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000005', '2019-06-10', 'PBA1900001', 'P000001', 'P000002', 'P000001', 'PBA', '-', 'P100006', '-', '-', '0', NULL, NULL, NULL, NULL, NULL, '14', 10000000, 1, '', 20000000);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000006', '2019-06-11', 'PBA1900002', 'P000002', 'P000003', 'P000002', 'PBA', '-', 'P100001', '-', '-', '0', NULL, NULL, NULL, NULL, NULL, '15', 200000000, 1, '', 200000000);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000007', '2019-06-11', 'PBA1900002', 'P000002', 'P000004', 'P000003', 'PBA', '-', 'P100006', '-', '-', '0', NULL, NULL, NULL, NULL, NULL, '16', 10000000, 1, '', 10000000);

-- ----------------------------
-- Table structure for tbl_konversi_satuan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_konversi_satuan";
CREATE TABLE "public"."tbl_konversi_satuan" (
  "IDKonversi" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDKonversi_seq"'::regclass),
  "IDSatuanBerat" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDSatuanBerat_seq"'::regclass),
  "Qty" float8,
  "IDSatuanKecil" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDSatuanKecil_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_konversi_satuan
-- ----------------------------
INSERT INTO "public"."tbl_konversi_satuan" VALUES (6, 5, 1.6757657, 5);
INSERT INTO "public"."tbl_konversi_satuan" VALUES (7, 5, 1.56462345327, 5);
INSERT INTO "public"."tbl_konversi_satuan" VALUES (14, 9, 1.323, 9);

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan";
CREATE TABLE "public"."tbl_koreksi_persediaan" (
  "IDKP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_IDKP_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_yard" float8,
  "Total_meter" float8,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_IDMataUang_seq"'::regclass),
  "Kurs" float8,
  "Jenis" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(35) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan_detail";
CREATE TABLE "public"."tbl_koreksi_persediaan_detail" (
  "IDKPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDKPDetail_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDSatuan_seq"'::regclass),
  "Barcode" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDWarna_seq"'::regclass),
  "IDKP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDKP_seq"'::regclass),
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDMataUang_seq"'::regclass),
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Kurs" float8,
  "Qty_yard" float8
)
;

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan_scan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan_scan";
CREATE TABLE "public"."tbl_koreksi_persediaan_scan" (
  "IDKPScan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_IDKPScan_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_yard" float8,
  "Total_meter" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Jenis" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan_scan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail";
CREATE TABLE "public"."tbl_koreksi_persediaan_scan_detail" (
  "IDKPScanDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"'::regclass),
  "IDKPScan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDKP_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDWarna_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"'::regclass),
  "Kurs" float8
)
;

-- ----------------------------
-- Table structure for tbl_kota
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kota";
CREATE TABLE "public"."tbl_kota" (
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kota_IDKota_seq"'::regclass),
  "Kode_Kota" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Provinsi" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kota" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_kota
-- ----------------------------
INSERT INTO "public"."tbl_kota" VALUES ('P000003', 'BKL', 'Bengkulu', 'BENGKULU', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000004', 'BKS', 'Jawa Barat', 'BEKASI', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000005', 'CLCP', 'Jawa Tengah', 'CILACAP', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000006', 'CMH', 'Jawa Barat', 'CIMAHI', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000007', 'CRB', 'Jawa Barat', 'CIREBON', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000008', 'DPK', 'Jawa Barat', 'DEPOK', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000009', 'GRNT', 'Gorontalo', 'GORONTALO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000010', 'JKT', 'DKI Jakarta', 'JAKARTA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000011', 'JMB', 'Jambi', 'JAMBI', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000012', 'LPG', 'Lampung', 'LAMPUNG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000013', 'KRW', 'Jawa Barat', 'KARAWANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000014', 'MND', 'Sulawesi Utara', 'MANADO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000015', 'MDN', 'Sumatera Utara', 'MEDAN', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000016', 'MKSR', 'Sulawesi Selatan', 'MAKASAR', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000017', 'MLG', 'Jawa Timur', 'MALANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000018', 'PDG', 'Sumatera Barat', 'PADANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000019', 'PLB', 'Sumatera Selata', 'PALEMBANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000020', 'PKBR', 'Riau', 'PEKANBARU', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000021', 'PTNK', 'Kalimantan Barat', 'PONTIANAK', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000022', 'SBY', 'Jawa TImur', 'SURABAYA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000023', 'SLO', 'Jawa Tengah', 'SOLO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000024', 'SMR', 'Jawa Tengah', 'SEMARANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000025', 'SRG', 'Banten', 'SERANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000026', 'SMRD', 'Kalimantan Timur', 'SAMARINDA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000027', 'TSK', 'Jawa Barat', 'TASIKMALAYA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000028', 'TGR', 'Banten', 'TANGERANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000029', 'TRNT', 'Maluku Utara', 'TERNATE', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000001', 'BDG', 'Jawa Barat', 'BANDUNG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P00002a', '-', '-', '-', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P00002b', 'JAI', 'MALUKU UTARA', 'JAILOLO', 'aktif');

-- ----------------------------
-- Table structure for tbl_lap_umur_persediaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_lap_umur_persediaan";
CREATE TABLE "public"."tbl_lap_umur_persediaan" (
  "IDLapPers" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDLapPers_seq"'::regclass),
  "IDBarang" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDBarang_seq"'::regclass),
  "IDCorak" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDCorak_seq"'::regclass),
  "IDMerk" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDMerk_seq"'::regclass),
  "IDWarna" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDWarna_seq"'::regclass),
  "Qty_yard1" float8,
  "Qty_meter1" float8,
  "Nilai1" float8,
  "Qty_yard2" float8,
  "Qty_meter2" float8,
  "Nilai2" float8,
  "Qty_yard3" float8,
  "Qty_meter3" float8,
  "Nilai3" float8,
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Total_nilai" float8
)
;

-- ----------------------------
-- Table structure for tbl_mata_uang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mata_uang";
CREATE TABLE "public"."tbl_mata_uang" (
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_mata_uang_IDMataUang_seq"'::regclass),
  "Kode" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Mata_uang" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Negara" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Default" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_mata_uang
-- ----------------------------
INSERT INTO "public"."tbl_mata_uang" VALUES (1, 'rp001', 'Rupiah', 'Indonesia', NULL, '140000', 'aktif');

-- ----------------------------
-- Table structure for tbl_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu";
CREATE TABLE "public"."tbl_menu" (
  "IDMenu" int8 NOT NULL DEFAULT nextval('"tbl_menu_IDMenu_seq"'::regclass),
  "Menu" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Urutan_menu" int2
)
;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
INSERT INTO "public"."tbl_menu" VALUES (17, 'Setting', NULL);
INSERT INTO "public"."tbl_menu" VALUES (1, 'Data Master', 1);
INSERT INTO "public"."tbl_menu" VALUES (16, 'Saldo Awal', 2);
INSERT INTO "public"."tbl_menu" VALUES (12, 'Pembelian', 3);
INSERT INTO "public"."tbl_menu" VALUES (19, 'Penjualan', 4);
INSERT INTO "public"."tbl_menu" VALUES (18, 'Persediaan', 5);
INSERT INTO "public"."tbl_menu" VALUES (28, 'Stok Opname', 6);
INSERT INTO "public"."tbl_menu" VALUES (20, 'Keuangan', 7);
INSERT INTO "public"."tbl_menu" VALUES (30, 'Asset', 9);

-- ----------------------------
-- Table structure for tbl_menu_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu_detail";
CREATE TABLE "public"."tbl_menu_detail" (
  "IDMenuDetail" int8 NOT NULL DEFAULT nextval('"tbl_menu_detail_IDMenuDetail_seq"'::regclass),
  "IDMenu" int8 NOT NULL DEFAULT nextval('"tbl_menu_detail_IDMenu_seq"'::regclass),
  "Menu_Detail" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Url" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Urutan" int2
)
;

-- ----------------------------
-- Records of tbl_menu_detail
-- ----------------------------
INSERT INTO "public"."tbl_menu_detail" VALUES (1, 17, 'Agen', 'Agen/tambah_agen', NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (13, 17, 'Group User', 'GroupUser', NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (19, 17, 'Menu', 'Menu', NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (20, 17, 'Menu Detail', 'MenuDetail', NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (27, 17, 'User', 'User', NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (88, 19, 'Sample Kain', 'SampleKain', 66);
INSERT INTO "public"."tbl_menu_detail" VALUES (75, 19, 'Sales Order', 'So', 57);
INSERT INTO "public"."tbl_menu_detail" VALUES (98, 20, 'Laporan AR', 'SettingLR/index_aging_schedule', 98);
INSERT INTO "public"."tbl_menu_detail" VALUES (85, 19, 'Retur Penjualan', 'ReturPenjualan', 64);
INSERT INTO "public"."tbl_menu_detail" VALUES (94, 20, 'Penerimaan Piutang', 'PenerimaanPiutang', 90);
INSERT INTO "public"."tbl_menu_detail" VALUES (99, 20, 'Buku Kas', 'BukuKas', 87);
INSERT INTO "public"."tbl_menu_detail" VALUES (100, 20, 'Buku Bank', 'BukuBank', 88);
INSERT INTO "public"."tbl_menu_detail" VALUES (91, 20, 'Laporan BS', 'SettingLR/', 101);
INSERT INTO "public"."tbl_menu_detail" VALUES (34, 12, 'Booking Order', 'BookingOrder', 26);
INSERT INTO "public"."tbl_menu_detail" VALUES (87, 19, 'Retur Penjualan Non Kain', 'ReturPenjualanNonKain', 65);
INSERT INTO "public"."tbl_menu_detail" VALUES (81, 20, 'Laporan Giro', 'Laporan_giro', 96);
INSERT INTO "public"."tbl_menu_detail" VALUES (114, 19, 'Down Payment', 'DownPayment', 67);
INSERT INTO "public"."tbl_menu_detail" VALUES (86, 20, 'Laporan Buku Bank', 'BukuBank/laporan_buku_bank', 93);
INSERT INTO "public"."tbl_menu_detail" VALUES (95, 20, 'Laporan CF', 'SettingLR/index_arus_kas', 103);
INSERT INTO "public"."tbl_menu_detail" VALUES (90, 20, 'Mutasi Giro', 'MutasiGiro', 91);
INSERT INTO "public"."tbl_menu_detail" VALUES (115, 20, 'Setting BS', 'SettingLR/tambah_setting_lr', 104);
INSERT INTO "public"."tbl_menu_detail" VALUES (105, 20, 'General Ledger', 'SettingLR/index_buku_besar', 100);
INSERT INTO "public"."tbl_menu_detail" VALUES (93, 20, 'Pembayaran Hutang', 'PembayaranHutang', 89);
INSERT INTO "public"."tbl_menu_detail" VALUES (82, 20, 'Laporan Pembayaran Hutang', 'PembayaranHutang/pencarian_lap_pembayaran_hutang', 94);
INSERT INTO "public"."tbl_menu_detail" VALUES (112, 19, 'Laporan Surat Jalan Kain', 'SuratJalan/laporan_surat_jalan_kain', 71);
INSERT INTO "public"."tbl_menu_detail" VALUES (108, 19, 'Laporan Invoice Penjualan Kain', 'InvoicePenjualan/laporan_inv_kain', 73);
INSERT INTO "public"."tbl_menu_detail" VALUES (69, 18, 'Laporan Kartu Stok', 'KartuStok', 81);
INSERT INTO "public"."tbl_menu_detail" VALUES (109, 19, 'Laporan Invoice Penjualan Non Kain', 'InvoicePenjualanNonKain/laporan_inv_nonkain', 74);
INSERT INTO "public"."tbl_menu_detail" VALUES (110, 19, 'Laporan Retur Penjualan Kain', 'ReturPenjualan/laporan_retur_kain', 75);
INSERT INTO "public"."tbl_menu_detail" VALUES (111, 19, 'Laporan Retur Penjualan Non Kain', 'ReturPenjualanNonKain/laporan_retur_nonkain', 76);
INSERT INTO "public"."tbl_menu_detail" VALUES (66, 18, 'Koreksi Persediaan', 'KoreksiPersediaan', 78);
INSERT INTO "public"."tbl_menu_detail" VALUES (67, 18, 'Koreksi Persediaan Scan', 'KoreksiPersediaanScan', 79);
INSERT INTO "public"."tbl_menu_detail" VALUES (102, 19, 'Laporan Sales Order Kain', 'So/laporan_so', 68);
INSERT INTO "public"."tbl_menu_detail" VALUES (117, 20, 'Setting CF', 'SettingLR/tambah_setting_cf', 106);
INSERT INTO "public"."tbl_menu_detail" VALUES (118, 20, 'Setting Notes', 'SettingLR/tambah_setting_notes', NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (54, 12, 'Laporan Purchase Order Celup', 'PoCelup/laporan_po', 46);
INSERT INTO "public"."tbl_menu_detail" VALUES (49, 12, 'Surat Jalan Makloon Celup', 'MakloonCelup', 35);
INSERT INTO "public"."tbl_menu_detail" VALUES (76, 19, 'Sales Order Non Kain', 'Sos', 58);
INSERT INTO "public"."tbl_menu_detail" VALUES (104, 19, 'Laporan Sales Order Non Kain', 'Sos/laporan_sos', 69);
INSERT INTO "public"."tbl_menu_detail" VALUES (116, 20, 'Setting PL', 'SettingLR/tambah_setting_pl', 105);
INSERT INTO "public"."tbl_menu_detail" VALUES (50, 12, 'Surat Jalan Makloon Jahit', 'MakloonJahit', 34);
INSERT INTO "public"."tbl_menu_detail" VALUES (73, 28, 'Laporan Stok Opname', 'StokOpName/laporan_stokopname', 85);
INSERT INTO "public"."tbl_menu_detail" VALUES (51, 12, 'Pendaftaran Asset', 'Pembelian_asset', 42);
INSERT INTO "public"."tbl_menu_detail" VALUES (43, 12, 'Penerimaan Barang Makloon Jahit', 'PenerimaanBarangJahit', 36);
INSERT INTO "public"."tbl_menu_detail" VALUES (3, 1, 'Bank', 'Bank', 1);
INSERT INTO "public"."tbl_menu_detail" VALUES (97, 20, 'Jurnal Umum', 'JurnalUmum', 86);
INSERT INTO "public"."tbl_menu_detail" VALUES (55, 12, 'Laporan PO Kain Non Trisula', 'PoUmum/laporan_po', 45);
INSERT INTO "public"."tbl_menu_detail" VALUES (68, 18, 'Laporan Umur Persediaan', 'LapUmurPersediaan', 83);
INSERT INTO "public"."tbl_menu_detail" VALUES (18, 1, 'Kota', 'Kota', 2);
INSERT INTO "public"."tbl_menu_detail" VALUES (14, 1, 'Gudang', 'Gudang', 3);
INSERT INTO "public"."tbl_menu_detail" VALUES (6, 1, 'Corak', 'Corak', 4);
INSERT INTO "public"."tbl_menu_detail" VALUES (21, 1, 'Merk', 'Merk', 5);
INSERT INTO "public"."tbl_menu_detail" VALUES (24, 1, 'Satuan', 'Satuan', 6);
INSERT INTO "public"."tbl_menu_detail" VALUES (28, 1, 'Warna', 'Warna', 7);
INSERT INTO "public"."tbl_menu_detail" VALUES (65, 1, 'Group COA', 'GroupCOA', 8);
INSERT INTO "public"."tbl_menu_detail" VALUES (5, 1, 'COA', 'Coa', 9);
INSERT INTO "public"."tbl_menu_detail" VALUES (4, 1, 'Barang', 'Barang', 11);
INSERT INTO "public"."tbl_menu_detail" VALUES (12, 1, 'Group Supplier', 'GroupSupplier', 12);
INSERT INTO "public"."tbl_menu_detail" VALUES (25, 1, 'Supplier', 'Supplier', 13);
INSERT INTO "public"."tbl_menu_detail" VALUES (11, 1, 'Group Customer', 'GroupCustomer', 14);
INSERT INTO "public"."tbl_menu_detail" VALUES (7, 1, 'Customer', 'Customer', 15);
INSERT INTO "public"."tbl_menu_detail" VALUES (9, 1, 'Group Asset', 'GroupAsset', 16);
INSERT INTO "public"."tbl_menu_detail" VALUES (2, 1, 'Asset', 'Assets', 17);
INSERT INTO "public"."tbl_menu_detail" VALUES (15, 1, 'Harga Jual Barang', 'HargaJualBarang', 19);
INSERT INTO "public"."tbl_menu_detail" VALUES (16, 16, 'Hutang', 'Hutang', 19);
INSERT INTO "public"."tbl_menu_detail" VALUES (22, 16, 'Piutang', 'Piutang', 20);
INSERT INTO "public"."tbl_menu_detail" VALUES (8, 16, 'Giro Masuk', 'Giro', 21);
INSERT INTO "public"."tbl_menu_detail" VALUES (23, 16, 'Saldo Awal Asset', 'SaldoAwalAsset', 23);
INSERT INTO "public"."tbl_menu_detail" VALUES (71, 16, 'Saldo Awal Account', 'SaldoAwalAcount', 24);
INSERT INTO "public"."tbl_menu_detail" VALUES (26, 16, 'UM Customer', 'Umcustomer', 25);
INSERT INTO "public"."tbl_menu_detail" VALUES (35, 12, 'Purchase Order', 'Po', 27);
INSERT INTO "public"."tbl_menu_detail" VALUES (36, 12, 'Purchase Order Kain Non Trisula', 'PoUmum', 28);
INSERT INTO "public"."tbl_menu_detail" VALUES (37, 12, 'Purchase Order Celup', 'PoCelup', 29);
INSERT INTO "public"."tbl_menu_detail" VALUES (38, 12, 'Purchase Order Umum', 'PoUmum2', 30);
INSERT INTO "public"."tbl_menu_detail" VALUES (39, 12, 'Penerimaan Barang', 'PenerimaanBarang', 31);
INSERT INTO "public"."tbl_menu_detail" VALUES (42, 12, 'Penerimaan Barang Kain Non Trisula', 'penerimaan_barang_umum', 33);
INSERT INTO "public"."tbl_menu_detail" VALUES (44, 12, 'Invoice Pembelian', 'InvoicePembelian', 37);
INSERT INTO "public"."tbl_menu_detail" VALUES (41, 12, 'Penerimaan Barang Umum', 'Penerimaan_barang_umum2', 35);
INSERT INTO "public"."tbl_menu_detail" VALUES (45, 12, 'Invoice Pembelian Umum', 'Invoice_pembelian_umum', 38);
INSERT INTO "public"."tbl_menu_detail" VALUES (46, 12, 'Intsruksi Pengiriman', 'InstruksiPengiriman', 39);
INSERT INTO "public"."tbl_menu_detail" VALUES (47, 12, 'Retur Pembelian', 'ReturPembelian', 40);
INSERT INTO "public"."tbl_menu_detail" VALUES (48, 12, 'Retur Pembelian Umum', 'Retur_pembelian_umum', 41);
INSERT INTO "public"."tbl_menu_detail" VALUES (52, 12, 'Laporan Booking Order', 'Laporan_bo/index', 43);
INSERT INTO "public"."tbl_menu_detail" VALUES (53, 12, 'Laporan Purchase Order', 'Po/laporan_po', 44);
INSERT INTO "public"."tbl_menu_detail" VALUES (77, 19, 'Packing List', 'PackingList', 59);
INSERT INTO "public"."tbl_menu_detail" VALUES (78, 19, 'Surat Jalan', 'SuratJalan', 60);
INSERT INTO "public"."tbl_menu_detail" VALUES (79, 19, 'Surat Jalan Non Kain', 'SuratJalanSeragam', 61);
INSERT INTO "public"."tbl_menu_detail" VALUES (80, 19, 'Invoice Penjualan', 'InvoicePenjualan', 62);
INSERT INTO "public"."tbl_menu_detail" VALUES (83, 19, 'Invoice Penjualan Non Kain', 'InvoicePenjualanNonKain', 63);
INSERT INTO "public"."tbl_menu_detail" VALUES (70, 18, 'Laporan Stok Per Corak', 'Stok', 80);
INSERT INTO "public"."tbl_menu_detail" VALUES (107, 18, 'Laporan Mutasi Persediaan', 'SettingLR/index_mutasi_persediaan', 82);
INSERT INTO "public"."tbl_menu_detail" VALUES (72, 28, 'Stok Opname', 'StokOpName', 84);
INSERT INTO "public"."tbl_menu_detail" VALUES (96, 20, 'Laporan Notes', 'SettingLR/index_catatan_lapkeu', NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (10, 1, 'Group Barang', 'GroupBarang', 10);
INSERT INTO "public"."tbl_menu_detail" VALUES (32, 16, 'Giro Keluar', 'Giro/index_giro_keluar', 22);
INSERT INTO "public"."tbl_menu_detail" VALUES (40, 12, 'Scan Penerimaan Barang', 'scan_penerimaan_barang', 32);
INSERT INTO "public"."tbl_menu_detail" VALUES (124, 12, 'Laporan Purchase Order Umum', 'PoUmum2/laporan_po', 47);
INSERT INTO "public"."tbl_menu_detail" VALUES (56, 12, 'Laporan Penerimaan Barang', 'Laporan_penerimaan_barang/index', 48);
INSERT INTO "public"."tbl_menu_detail" VALUES (129, 12, 'Laporan Scan Penerimaan Barang', 'Scan_penerimaan_barang/index_scan', 49);
INSERT INTO "public"."tbl_menu_detail" VALUES (126, 12, 'Laporan Penerimaan Barang Non Trisula', 'Laporan_penerimaan_barang/index_pbnt', 50);
INSERT INTO "public"."tbl_menu_detail" VALUES (128, 12, 'Laporan Penerimaan Makloon Jahit', 'Laporan_penerimaan_barang/index_jahit', 51);
INSERT INTO "public"."tbl_menu_detail" VALUES (123, 12, 'Laporan Penerimaan Barang Umum', 'Laporan_penerimaan_barang/index_umum', 52);
INSERT INTO "public"."tbl_menu_detail" VALUES (119, 12, 'Laporan Invoice Pembelian', 'Laporan_invoice_pembelian', 53);
INSERT INTO "public"."tbl_menu_detail" VALUES (120, 12, 'Laporan Instruksi Pengiriman', 'InstruksiPengiriman/laporan_instruksi', 54);
INSERT INTO "public"."tbl_menu_detail" VALUES (121, 12, 'Laporan Retur Pembelian', 'ReturPembelian/laporan_retur_pembelian', 55);
INSERT INTO "public"."tbl_menu_detail" VALUES (122, 12, 'Laporan Retur Pembelian Umum', 'Retur_pembelian_umum/laporan_retur_pembelian', 56);
INSERT INTO "public"."tbl_menu_detail" VALUES (125, 19, 'Laporan Packing List', 'PackingList/laporan_packing_list', 70);
INSERT INTO "public"."tbl_menu_detail" VALUES (113, 19, 'Laporan Surat Jalan Non Kain', 'SuratJalanSeragam/laporan_surat_jalan_non_kain', 72);
INSERT INTO "public"."tbl_menu_detail" VALUES (127, 19, 'Penjualan Asset', 'Penjualan_asset/tambah_penjualan_asset', 77);
INSERT INTO "public"."tbl_menu_detail" VALUES (89, 20, 'Laporan Buku Kas', 'BukuKas/laporan_buku_kas', 92);
INSERT INTO "public"."tbl_menu_detail" VALUES (84, 20, 'Laporan Penerimaan Piutang', 'PenerimaanPiutang/pencarian_lap_penerimaan_piutang', 95);
INSERT INTO "public"."tbl_menu_detail" VALUES (101, 20, 'Laporan AP', 'SettingLR/index_mutasi_utang_usaha', 97);
INSERT INTO "public"."tbl_menu_detail" VALUES (103, 20, 'Neraca Saldo', 'SettingLR/index_buku_besar_pembantu', 99);
INSERT INTO "public"."tbl_menu_detail" VALUES (92, 20, 'Laporan PL', 'SettingLR/index_laba_rugi', 102);
INSERT INTO "public"."tbl_menu_detail" VALUES (113, 30, 'Purchase Order Asset', 'Po_asset', 1);
INSERT INTO "public"."tbl_menu_detail" VALUES (114, 30, 'Terima Barang Asset', 'Penerimaan_barang_asset', 2);
INSERT INTO "public"."tbl_menu_detail" VALUES (115, 30, 'Pembelian Barang Asset', 'Pembelian_barang_asset', 3);

-- ----------------------------
-- Table structure for tbl_menu_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu_role";
CREATE TABLE "public"."tbl_menu_role" (
  "IDMenuRole" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDMenuRole_seq"'::regclass),
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDGroupUser_seq"'::regclass),
  "IDMenuDetail" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDMenuDetail_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_menu_role
-- ----------------------------
INSERT INTO "public"."tbl_menu_role" VALUES (4181, 5, 2);
INSERT INTO "public"."tbl_menu_role" VALUES (4182, 5, 3);
INSERT INTO "public"."tbl_menu_role" VALUES (4183, 5, 4);
INSERT INTO "public"."tbl_menu_role" VALUES (4184, 5, 5);
INSERT INTO "public"."tbl_menu_role" VALUES (4185, 5, 6);
INSERT INTO "public"."tbl_menu_role" VALUES (4186, 5, 7);
INSERT INTO "public"."tbl_menu_role" VALUES (4187, 5, 9);
INSERT INTO "public"."tbl_menu_role" VALUES (4188, 5, 10);
INSERT INTO "public"."tbl_menu_role" VALUES (4189, 5, 11);
INSERT INTO "public"."tbl_menu_role" VALUES (4190, 5, 12);
INSERT INTO "public"."tbl_menu_role" VALUES (4191, 5, 14);
INSERT INTO "public"."tbl_menu_role" VALUES (4192, 5, 15);
INSERT INTO "public"."tbl_menu_role" VALUES (4193, 5, 18);
INSERT INTO "public"."tbl_menu_role" VALUES (4194, 5, 21);
INSERT INTO "public"."tbl_menu_role" VALUES (4195, 5, 24);
INSERT INTO "public"."tbl_menu_role" VALUES (4196, 5, 25);
INSERT INTO "public"."tbl_menu_role" VALUES (4197, 5, 28);
INSERT INTO "public"."tbl_menu_role" VALUES (4198, 5, 65);
INSERT INTO "public"."tbl_menu_role" VALUES (4199, 5, 34);
INSERT INTO "public"."tbl_menu_role" VALUES (4200, 5, 35);
INSERT INTO "public"."tbl_menu_role" VALUES (4201, 5, 36);
INSERT INTO "public"."tbl_menu_role" VALUES (4202, 5, 37);
INSERT INTO "public"."tbl_menu_role" VALUES (4203, 5, 38);
INSERT INTO "public"."tbl_menu_role" VALUES (4204, 5, 39);
INSERT INTO "public"."tbl_menu_role" VALUES (4205, 5, 42);
INSERT INTO "public"."tbl_menu_role" VALUES (4206, 5, 40);
INSERT INTO "public"."tbl_menu_role" VALUES (4207, 5, 41);
INSERT INTO "public"."tbl_menu_role" VALUES (4208, 5, 43);
INSERT INTO "public"."tbl_menu_role" VALUES (4209, 5, 44);
INSERT INTO "public"."tbl_menu_role" VALUES (4210, 5, 45);
INSERT INTO "public"."tbl_menu_role" VALUES (4211, 5, 47);
INSERT INTO "public"."tbl_menu_role" VALUES (4212, 5, 48);
INSERT INTO "public"."tbl_menu_role" VALUES (4213, 5, 51);
INSERT INTO "public"."tbl_menu_role" VALUES (4214, 5, 56);
INSERT INTO "public"."tbl_menu_role" VALUES (4215, 5, 53);
INSERT INTO "public"."tbl_menu_role" VALUES (4216, 5, 50);
INSERT INTO "public"."tbl_menu_role" VALUES (4217, 5, 54);
INSERT INTO "public"."tbl_menu_role" VALUES (4218, 5, 55);
INSERT INTO "public"."tbl_menu_role" VALUES (4219, 5, 46);
INSERT INTO "public"."tbl_menu_role" VALUES (4220, 5, 49);
INSERT INTO "public"."tbl_menu_role" VALUES (4221, 5, 52);
INSERT INTO "public"."tbl_menu_role" VALUES (4222, 5, 119);
INSERT INTO "public"."tbl_menu_role" VALUES (4223, 5, 120);
INSERT INTO "public"."tbl_menu_role" VALUES (4224, 5, 121);
INSERT INTO "public"."tbl_menu_role" VALUES (4225, 5, 122);
INSERT INTO "public"."tbl_menu_role" VALUES (4226, 5, 123);
INSERT INTO "public"."tbl_menu_role" VALUES (4227, 5, 124);
INSERT INTO "public"."tbl_menu_role" VALUES (4228, 5, 126);
INSERT INTO "public"."tbl_menu_role" VALUES (4229, 5, 128);
INSERT INTO "public"."tbl_menu_role" VALUES (4230, 5, 129);
INSERT INTO "public"."tbl_menu_role" VALUES (4231, 5, 8);
INSERT INTO "public"."tbl_menu_role" VALUES (4232, 5, 16);
INSERT INTO "public"."tbl_menu_role" VALUES (4233, 5, 22);
INSERT INTO "public"."tbl_menu_role" VALUES (4234, 5, 23);
INSERT INTO "public"."tbl_menu_role" VALUES (4235, 5, 32);
INSERT INTO "public"."tbl_menu_role" VALUES (4236, 5, 71);
INSERT INTO "public"."tbl_menu_role" VALUES (4237, 5, 26);
INSERT INTO "public"."tbl_menu_role" VALUES (4238, 5, 1);
INSERT INTO "public"."tbl_menu_role" VALUES (4239, 5, 13);
INSERT INTO "public"."tbl_menu_role" VALUES (4240, 5, 19);
INSERT INTO "public"."tbl_menu_role" VALUES (4241, 5, 20);
INSERT INTO "public"."tbl_menu_role" VALUES (4242, 5, 27);
INSERT INTO "public"."tbl_menu_role" VALUES (4243, 5, 66);
INSERT INTO "public"."tbl_menu_role" VALUES (4244, 5, 67);
INSERT INTO "public"."tbl_menu_role" VALUES (4245, 5, 68);
INSERT INTO "public"."tbl_menu_role" VALUES (4246, 5, 69);
INSERT INTO "public"."tbl_menu_role" VALUES (4247, 5, 70);
INSERT INTO "public"."tbl_menu_role" VALUES (4248, 5, 107);
INSERT INTO "public"."tbl_menu_role" VALUES (4249, 5, 80);
INSERT INTO "public"."tbl_menu_role" VALUES (4250, 5, 75);
INSERT INTO "public"."tbl_menu_role" VALUES (4251, 5, 76);
INSERT INTO "public"."tbl_menu_role" VALUES (4252, 5, 77);
INSERT INTO "public"."tbl_menu_role" VALUES (4253, 5, 78);
INSERT INTO "public"."tbl_menu_role" VALUES (4254, 5, 79);
INSERT INTO "public"."tbl_menu_role" VALUES (4255, 5, 87);
INSERT INTO "public"."tbl_menu_role" VALUES (4256, 5, 83);
INSERT INTO "public"."tbl_menu_role" VALUES (4257, 5, 85);
INSERT INTO "public"."tbl_menu_role" VALUES (4258, 5, 88);
INSERT INTO "public"."tbl_menu_role" VALUES (4259, 5, 104);
INSERT INTO "public"."tbl_menu_role" VALUES (4260, 5, 113);
INSERT INTO "public"."tbl_menu_role" VALUES (4261, 5, 108);
INSERT INTO "public"."tbl_menu_role" VALUES (4262, 5, 109);
INSERT INTO "public"."tbl_menu_role" VALUES (4263, 5, 110);
INSERT INTO "public"."tbl_menu_role" VALUES (4264, 5, 111);
INSERT INTO "public"."tbl_menu_role" VALUES (4265, 5, 112);
INSERT INTO "public"."tbl_menu_role" VALUES (4266, 5, 114);
INSERT INTO "public"."tbl_menu_role" VALUES (4267, 5, 102);
INSERT INTO "public"."tbl_menu_role" VALUES (4268, 5, 125);
INSERT INTO "public"."tbl_menu_role" VALUES (4269, 5, 127);
INSERT INTO "public"."tbl_menu_role" VALUES (4270, 5, 81);
INSERT INTO "public"."tbl_menu_role" VALUES (4271, 5, 93);
INSERT INTO "public"."tbl_menu_role" VALUES (4272, 5, 97);
INSERT INTO "public"."tbl_menu_role" VALUES (4273, 5, 99);
INSERT INTO "public"."tbl_menu_role" VALUES (4274, 5, 105);
INSERT INTO "public"."tbl_menu_role" VALUES (4275, 5, 82);
INSERT INTO "public"."tbl_menu_role" VALUES (4276, 5, 90);
INSERT INTO "public"."tbl_menu_role" VALUES (4277, 5, 91);
INSERT INTO "public"."tbl_menu_role" VALUES (4278, 5, 94);
INSERT INTO "public"."tbl_menu_role" VALUES (4279, 5, 95);
INSERT INTO "public"."tbl_menu_role" VALUES (4280, 5, 98);
INSERT INTO "public"."tbl_menu_role" VALUES (4281, 5, 100);
INSERT INTO "public"."tbl_menu_role" VALUES (4282, 5, 86);
INSERT INTO "public"."tbl_menu_role" VALUES (4283, 5, 116);
INSERT INTO "public"."tbl_menu_role" VALUES (4284, 5, 117);
INSERT INTO "public"."tbl_menu_role" VALUES (4285, 5, 115);
INSERT INTO "public"."tbl_menu_role" VALUES (4286, 5, 92);
INSERT INTO "public"."tbl_menu_role" VALUES (4287, 5, 101);
INSERT INTO "public"."tbl_menu_role" VALUES (4288, 5, 89);
INSERT INTO "public"."tbl_menu_role" VALUES (4289, 5, 84);
INSERT INTO "public"."tbl_menu_role" VALUES (4290, 5, 103);
INSERT INTO "public"."tbl_menu_role" VALUES (4291, 5, 72);
INSERT INTO "public"."tbl_menu_role" VALUES (4292, 5, 73);

-- ----------------------------
-- Table structure for tbl_merk
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_merk";
CREATE TABLE "public"."tbl_merk" (
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_merk_IDMerk_seq"'::regclass),
  "Kode_Merk" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Merk" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_merk
-- ----------------------------
INSERT INTO "public"."tbl_merk" VALUES ('Prk0001', 'HB', 'Hugo Black Stripe', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0002', 'HBsld', 'Hugo Black Solid', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0003', 'MM', 'Maxmoda Stripe', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0004', 'Mmsld', 'Maxmoda Solid', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0005', 'MORAFILL', 'Morafill', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0006', 'MORAFILL NS', 'Morafill Non Sulam', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0007', 'PORSEUS ', 'Porseus', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0008', 'PORSEUS NS', 'Porseus Non Sulam', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0009', 'CARV', 'Carvello', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0010', 'RGIOR', 'Ralph Gior', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0011', 'CMODA', 'Classmoda', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0012', 'SIIP', 'Bellini Siip', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0013', 'CHLOU', 'Chez louis', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0014', 'ECL', 'Eclusiva Twill', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0015', 'LB', 'Libero', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0016', 'AQUAV', 'Aquaviva', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0017', 'ECL PRM', 'Eclusiva Premier', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0018', 'VIS', 'Neo Vissero', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0019', 'BW', 'Black Wool', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0020', 'ZL', 'Zibellius', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0021', 'MJB', 'Maxmoda JB', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0022', 'TR', 'Tiera', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0023', 'Mmatrix', 'Macro Matrix', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0024', 'WP', 'Wool Plaid', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0027', 'KD012', '12', 'aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0025', 'GB', 'Goldbrown', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0027', '-', '-', 'aktif');
INSERT INTO "public"."tbl_merk" VALUES ('P000001', 'CO', 'Consina', 'aktif');
INSERT INTO "public"."tbl_merk" VALUES ('P000002', 'LO', 'Lokis', 'aktif');

-- ----------------------------
-- Table structure for tbl_mutasi_giro
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mutasi_giro";
CREATE TABLE "public"."tbl_mutasi_giro" (
  "IDMG" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_mutasi_giro_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mutasi_giro_detail";
CREATE TABLE "public"."tbl_mutasi_giro_detail" (
  "IDMGDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDMG" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGiro" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_lama" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_baru" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_out
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_out";
CREATE TABLE "public"."tbl_out" (
  "IDOut" int8 NOT NULL DEFAULT nextval('"tbl_out_IDOut_seq"'::regclass),
  "Tanggal" date,
  "Buyer" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "CustDes" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "WarnaCust" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Panjang_Yard" float8,
  "Panjang_Meter" float8,
  "Grade" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_packing_list
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_packing_list";
CREATE TABLE "public"."tbl_packing_list" (
  "IDPAC" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSOK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total_pcs_grade_a" float8,
  "Total_qty_grade_a" float8,
  "Total_pcs_grade_b" float8,
  "Total_qty_grade_b" float8,
  "Total_pcs_grade_s" float8,
  "Total_qty_grade_s" float8,
  "Total_pcs_grade_e" float8,
  "Total_qty_grade_e" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_packing_list_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_packing_list_detail";
CREATE TABLE "public"."tbl_packing_list_detail" (
  "IDPACDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDPAC" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran";
CREATE TABLE "public"."tbl_pembayaran" (
  "IDFBPembayaran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDFBPembayaran_seq"'::regclass),
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDFB_seq"'::regclass),
  "Jenis_pembayaran" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDCOA_seq"'::regclass),
  "NominalPembayaran" float8,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_pembayaran_IDMataUang_seq"'::regclass),
  "Kurs" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_Giro" date
)
;

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang";
CREATE TABLE "public"."tbl_pembayaran_hutang" (
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total" float8,
  "Uang_muka" float8,
  "Selisih" float8,
  "Kompensasi_um" float8,
  "Pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kelebihan_bayar" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang_bayar
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang_bayar";
CREATE TABLE "public"."tbl_pembayaran_hutang_bayar" (
  "IDBSBayar" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nominal_pembayaran" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Status_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_giro" date
)
;

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang_detail";
CREATE TABLE "public"."tbl_pembayaran_hutang_detail" (
  "IDBSDet" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fb" date,
  "Nilai_fb" float8,
  "Telah_diterima" float8,
  "Diterima" float8,
  "UM" float8,
  "Retur" float8,
  "Saldo" float8,
  "Selisih" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "IDHutang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_pembelian
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian";
CREATE TABLE "public"."tbl_pembelian" (
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDFB_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDTBS_seq"'::regclass),
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDSupplier_seq"'::regclass),
  "No_sj_supplier" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_jatuh_tempo" date,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_pembelian_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Discount" float8,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_asset";
CREATE TABLE "public"."tbl_pembelian_asset" (
  "IDFBA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_IDFBA_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_pembelian_asset_IDMataUang_seq"'::regclass),
  "Kurs" varchar COLLATE "pg_catalog"."default",
  "Total_nilai_perolehan" float8,
  "Total_akumulasi_penyusutan" float8,
  "Total_nilai_buku" float8,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_asset_detail";
CREATE TABLE "public"."tbl_pembelian_asset_detail" (
  "IDFBADetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDFBADetail_seq"'::regclass),
  "IDFBA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDFBA_seq"'::regclass),
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDAsset_seq"'::regclass),
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDGroupAsset_seq"'::regclass),
  "Nilai_perolehan" float8,
  "Akumulasi_penyusutan" float8,
  "Nilai_buku" float8,
  "Metode_penyusutan" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_penyusutan" date,
  "Umur" float8
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_detail";
CREATE TABLE "public"."tbl_pembelian_detail" (
  "IDFBDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDFBDetail_seq"'::regclass),
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDFB_seq"'::regclass),
  "IDTBSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDTBSDetail_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDCorak_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDMerk_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDWarna_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_grand_total";
CREATE TABLE "public"."tbl_pembelian_grand_total" (
  "IDFBGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_grand_total_IDFBGrandTotal_seq"'::regclass),
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8,
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_grand_total_IDFB_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum";
CREATE TABLE "public"."tbl_pembelian_umum" (
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status_ppn" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Persen_disc" float8,
  "Disc" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total_qty" float8,
  "Saldo_qty" float8,
  "Grand_total" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jatuh_tempo" int8,
  "Tanggal_jatuh_tempo" date
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_detail";
CREATE TABLE "public"."tbl_pembelian_umum_detail" (
  "IDFBUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBSUmumDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Saldo" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_grand_total";
CREATE TABLE "public"."tbl_pembelian_umum_grand_total" (
  "IDFBUmumGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_pembayaran";
CREATE TABLE "public"."tbl_pembelian_umum_pembayaran" (
  "IDFBUmumPembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalPembayaran" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Tanggal_giro" date
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang";
CREATE TABLE "public"."tbl_penerimaan_piutang" (
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total" float8,
  "Uang_muka" float8,
  "Selisih" float8,
  "Kompensasi_um" float8,
  "Pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kelebihan_bayar" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang_bayar
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang_bayar";
CREATE TABLE "public"."tbl_penerimaan_piutang_bayar" (
  "IDTPBayar" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nominal_pembayaran" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Status_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_giro" date
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang_detail";
CREATE TABLE "public"."tbl_penerimaan_piutang_detail" (
  "IDTPDet" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFJ" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fj" date,
  "Nilai_fj" float8,
  "Telah_diterima" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Diterima" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "UM" float8,
  "Retur" float8,
  "Saldo" float8,
  "Selisih" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "IDPiutang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_asset";
CREATE TABLE "public"."tbl_penjualan_asset" (
  "IDFJA" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal" varbit(20) DEFAULT NULL::bit varying,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_di_faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int2,
  "Kurs" float8,
  "Total_qty" float8,
  "Saldo_qty" float8,
  "Discount" float8,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_asset_detail";
CREATE TABLE "public"."tbl_penjualan_asset_detail" (
  "IDFJADetail" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFJA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBADetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Harga" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_kain
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_kain";
CREATE TABLE "public"."tbl_penjualan_kain" (
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_di_faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSJCK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" int8,
  "Tanggal_jatuh_tempo" date,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total_pieces" float8,
  "Total_yard" float8,
  "Saldo_pieces" float8,
  "Saldo_yard" float8,
  "Discount" float8,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_harga" float8
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_kain_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_kain_detail";
CREATE TABLE "public"."tbl_penjualan_kain_detail" (
  "IDFJKDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_roll" float8,
  "Saldo_qty_roll" float8,
  "Qty_yard" float8,
  "Saldo_yard" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_kain_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_kain_grand_total";
CREATE TABLE "public"."tbl_penjualan_kain_grand_total" (
  "IDFJKGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_kain_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_kain_pembayaran";
CREATE TABLE "public"."tbl_penjualan_kain_pembayaran" (
  "IDFJKPembayaran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalPembayaran" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Tanggal_giro" date
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_sample
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_sample";
CREATE TABLE "public"."tbl_penjualan_sample" (
  "IDFJSP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_sample_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_sample_detail";
CREATE TABLE "public"."tbl_penjualan_sample_detail" (
  "IDFJSPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJSP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_seragam
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_seragam";
CREATE TABLE "public"."tbl_penjualan_seragam" (
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_di_faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSJCS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" int8,
  "Tanggal_jatuh_tempo" date,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total_pieces" float8,
  "Total_yard" float8,
  "Saldo_pieces" float8,
  "Saldo_yard" float8,
  "Discount" float8,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_seragam_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_seragam_detail";
CREATE TABLE "public"."tbl_penjualan_seragam_detail" (
  "IDFJSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_roll" float8,
  "Saldo_qty_roll" float8,
  "Qty_yard" float8,
  "Saldo_yard" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_seragam_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_seragam_grand_total";
CREATE TABLE "public"."tbl_penjualan_seragam_grand_total" (
  "IDFJSGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_seragam_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_seragam_pembayaran";
CREATE TABLE "public"."tbl_penjualan_seragam_pembayaran" (
  "IDFJSPembayaran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalPembayaran" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Tanggal_giro" date
)
;

-- ----------------------------
-- Table structure for tbl_piutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_piutang";
CREATE TABLE "public"."tbl_piutang" (
  "Tanggal_Piutang" date,
  "Jatuh_Tempo" date,
  "No_Faktur" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDCustomer_seq"'::regclass),
  "Jenis_Faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_Piutang" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Awal" numeric(100) DEFAULT NULL::numeric,
  "Pembayaran" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Akhir" numeric(100) DEFAULT NULL::numeric,
  "IDPiutang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDPiutang_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDFaktur_seq"'::regclass),
  "UM" float8 DEFAULT 0,
  "DISC" float8 DEFAULT 0,
  "Retur" float8 DEFAULT 0,
  "Selisih" float8 DEFAULT 0,
  "Kontra_bon" float8 DEFAULT 0,
  "Saldo_kontra_bon" float8 DEFAULT 0
)
;

-- ----------------------------
-- Records of tbl_piutang
-- ----------------------------
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-01', '2019-01-15', '11/010', 'P100010', 'SA', 13431000, 13431000, 0, 13431000, 'P900001', 'P900001', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/041', 'P100010', 'SA', 21726500, 21726500, 0, 21726500, 'P900002', 'P900002', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-17', '2019-03-02', '12/093', 'P100011', 'SA', 34983000, 34983000, 0, 34983000, 'P900003', 'P900003', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-18', '2019-05-04', '02/081', 'P100011', 'SA', 35788750, 35788750, 0, 35788750, 'P900004', 'P900004', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-13', '2018-11-27', '09/062', 'P100012', 'SA', 14413000, 14413000, 0, 14413000, 'P900005', 'P900005', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-12', '2018-12-26', '10/091', 'P100012', 'SA', 33889000, 33889000, 0, 33889000, 'P900006', 'P900006', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-29', '2019-01-12', '10/171', 'P100012', 'SA', 2760000, 2760000, 0, 2760000, 'P900007', 'P900007', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-08', '2019-01-22', '11/051', 'P100012', 'SA', 3290000, 3290000, 0, 3290000, 'P900008', 'P900008', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/127', 'P100012', 'SA', 5264000, 5264000, 0, 5264000, 'P900012', 'P900012', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-18', '2019-04-03', '01/112', 'P100012', 'SA', 5900000, 5900000, 0, 5900000, 'P900013', 'P900013', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-21', '2019-04-06', '01/121', 'P100012', 'SA', 9316000, 9316000, 0, 9316000, 'P900014', 'P900014', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-11', '2019-04-27', '02/044', 'P100012', 'SA', 7661000, 7661000, 0, 7661000, 'P900015', 'P900015', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-21', '2019-05-07', '02/095', 'P100013', 'SA', 17823000, 17823000, 0, 17823000, 'P900016', 'P900016', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-03', '2019-02-16', '12/003', 'P100014', 'SA', 14090250, 14090250, 0, 14090250, 'P900017', 'P900017', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-03', '2019-02-16', '12/004', 'P100014', 'SA', 1260000, 1260000, 0, 1260000, 'P900018', 'P900018', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-05', '2019-02-18', '12/019', 'P100014', 'SA', 8820000, 8820000, 0, 8820000, 'P900019', 'P900019', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-11', '2019-02-24', '12/058', 'P100014', 'SA', 5610500, 5610500, 0, 5610500, 'P900021', 'P900021', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/101', 'P100014', 'SA', 4545000, 4545000, 0, 4545000, 'P900022', 'P900022', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-19', '2019-03-04', '12/115', 'P100014', 'SA', 12600000, 12600000, 0, 12600000, 'P900023', 'P900023', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-21', '2019-03-06', '12/128', 'P100014', 'SA', 7647500, 7647500, 0, 7647500, 'P900024', 'P900024', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-21', '2019-03-06', '12/129', 'P100014', 'SA', 3146000, 3146000, 0, 3146000, 'P900025', 'P900025', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/145', 'P100014', 'SA', 1488000, 1488000, 0, 1488000, 'P900026', 'P900026', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/006', 'P100014', 'SA', 7866000, 7866000, 0, 7866000, 'P900027', 'P900027', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/014', 'P100014', 'SA', 54558000, 54558000, 0, 54558000, 'P900028', 'P900028', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-04', '2019-03-20', '01/019', 'P100014', 'SA', 7560000, 7560000, 0, 7560000, 'P900029', 'P900029', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-10', '2019-03-26', '01/053', 'P100014', 'SA', 1232000, 1232000, 0, 1232000, 'P900031', 'P900031', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/075', 'P100014', 'SA', 8477000, 8477000, 0, 8477000, 'P900032', 'P900032', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/076', 'P100014', 'SA', 1800000, 1800000, 0, 1800000, 'P900033', 'P900033', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', '01/095', 'P100014', 'SA', 3230000, 3230000, 0, 3230000, 'P900034', 'P900034', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-21', '2019-04-06', '01/123', 'P100014', 'SA', 4128500, 4128500, 0, 4128500, 'P900036', 'P900036', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-26', '2019-04-11', '01/156', 'P100014', 'SA', 3146000, 3146000, 0, 3146000, 'P900037', 'P900037', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-01', '2019-04-17', '02/002', 'P100014', 'SA', 4312000, 4312000, 0, 4312000, 'P900038', 'P900038', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-08', '2019-04-24', '02/033', 'P100014', 'SA', 3150000, 3150000, 0, 3150000, 'P900040', 'P900040', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-11', '2019-04-27', '02/042', 'P100014', 'SA', 2970000, 2970000, 0, 2970000, 'P900041', 'P900041', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-12', '2019-04-28', '02/048', 'P100014', 'SA', 2880000, 2880000, 0, 2880000, 'P900042', 'P900042', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-14', '2019-04-30', '02/063', 'P100014', 'SA', 11157250, 11157250, 0, 11157250, 'P900043', 'P900043', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-19', '2019-05-05', '02/084', 'P100014', 'SA', 11686500, 11686500, 0, 11686500, 'P900044', 'P900044', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2017-05-23', '03/083', 'P100081', 'SA', 234000, 234000, 0, 234000, 'P900045', 'P900045', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-02', '2019-03-18', '01/002', 'P100016', 'SA', 621000, 621000, 0, 621000, 'P900047', 'P900047', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-05', '2018-12-19', '10/041', 'P100017', 'SA', 7904000, 7904000, 0, 7904000, 'P900048', 'P900048', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-05', '2018-12-19', '10/043', 'P100017', 'SA', 4140500, 4140500, 0, 4140500, 'P900049', 'P900049', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-09', '2018-12-23', '10/064', 'P100017', 'SA', 2898000, 2898000, 0, 2898000, 'P900050', 'P900050', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-10', '2018-12-24', '10/072', 'P100017', 'SA', 3665500, 3665500, 0, 3665500, 'P900051', 'P900051', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-18', '2019-01-01', '10/122', 'P100017', 'SA', 3725000, 3725000, 0, 3725000, 'P900052', 'P900052', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-24', '2019-01-07', '10/154', 'P100017', 'SA', 9702000, 9702000, 0, 9702000, 'P900054', 'P900054', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-25', '2019-01-08', '10/160', 'P100017', 'SA', 9800000, 9800000, 0, 9800000, 'P900055', 'P900055', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-29', '2019-01-12', '10/173', 'P100017', 'SA', 7400000, 7400000, 0, 7400000, 'P900056', 'P900056', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-01', '2019-01-15', '11/003', 'P100017', 'SA', 6669000, 6669000, 0, 6669000, 'P900057', 'P900057', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-13', '2019-01-27', '11/072', 'P100017', 'SA', 6762000, 6762000, 0, 6762000, 'P900060', 'P900060', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-13', '2019-01-27', '11/075', 'P100017', 'SA', 1764000, 1764000, 0, 1764000, 'P900061', 'P900061', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-14', '2019-01-28', '11/089', 'P100017', 'SA', 7657000, 7657000, 0, 7657000, 'P900062', 'P900062', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-15', '2019-01-29', '11/093', 'P100017', 'SA', 6946500, 6946500, 0, 6946500, 'P900063', 'P900063', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-16', '2019-01-30', '11/105', 'P100017', 'SA', 3430000, 3430000, 0, 3430000, 'P900064', 'P900064', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-21', '2019-02-04', '11/125', 'P100017', 'SA', 2606250, 2606250, 0, 2606250, 'P900065', 'P900065', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-22', '2019-02-05', '11/131', 'P100017', 'SA', 3501000, 3501000, 0, 3501000, 'P900066', 'P900066', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-26', '2019-02-09', '11/154', 'P100017', 'SA', 13060000, 13060000, 0, 13060000, 'P900067', 'P900067', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-03', '2019-02-16', '12/011', 'P100017', 'SA', 3590000, 3590000, 0, 3590000, 'P900068', 'P900068', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-04', '2019-02-17', '12/016', 'P100017', 'SA', 2220000, 2220000, 0, 2220000, 'P900069', 'P900069', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-06', '2019-02-19', '12/028', 'P100017', 'SA', 4250000, 4250000, 0, 4250000, 'P900070', 'P900070', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2017-06-12', '03/221', 'P100015', 'SA', 3162000, 3162000, 0, 3162000, 'P900046', 'P900046', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-17', '2019-03-02', '12/099', 'P100012', 'SA', 2944000, 2944000, 0, 2944000, 'P900011', 'P900011', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-19', '2019-03-04', '12/113', 'P100017', 'SA', 6296500, 6296500, 0, 6296500, 'P900074', 'P900074', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/150', 'P100017', 'SA', 14626500, 14626500, 0, 14626500, 'P900076', 'P900076', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', '12/158', 'P100017', 'SA', 4165000, 4165000, 0, 4165000, 'P900077', 'P900077', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-12-29', '2020-03-13', '12/169', 'P100017', 'SA', 1254000, 1254000, 0, 1254000, 'P900078', 'P900078', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-02', '2019-03-18', '01/004', 'P100017', 'SA', 4047000, 4047000, 0, 4047000, 'P900079', 'P900079', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-07', '2019-03-23', '01/032', 'P100017', 'SA', 8109500, 8109500, 0, 8109500, 'P900080', 'P900080', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/048', 'P100017', 'SA', 2951000, 2951000, 0, 2951000, 'P900081', 'P900081', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-17', '2019-04-02', '01/105', 'P100017', 'SA', 36752000, 36752000, 0, 36752000, 'P900083', 'P900083', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-18', '2019-04-03', '01/114', 'P100017', 'SA', 60819000, 60819000, 0, 60819000, 'P900084', 'P900084', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-19', '2019-04-04', '01/118', 'P100017', 'SA', 66348000, 66348000, 0, 66348000, 'P900085', 'P900085', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '12/016', 'P100017', 'SA', -2220000, -2220000, 0, -2220000, 'P900086', 'P900086', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '12/088', 'P100017', 'SA', -925000, -925000, 0, -925000, 'P900087', 'P900087', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-24', '2019-04-09', '01/146', 'P100017', 'SA', 25470000, 25470000, 0, 25470000, 'P900088', 'P900088', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/161', 'P100017', 'SA', 2618000, 2618000, 0, 2618000, 'P900089', 'P900089', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/168', 'P100017', 'SA', 179303000, 179303000, 0, 179303000, 'P900090', 'P900090', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/169', 'P100017', 'SA', 3780000, 3780000, 0, 3780000, 'P900091', 'P900091', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-30', '2019-04-15', '01/182', 'P100017', 'SA', 9625500, 9625500, 0, 9625500, 'P900092', 'P900092', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-31', '2019-04-16', '01/192', 'P100017', 'SA', 2866500, 2866500, 0, 2866500, 'P900093', 'P900093', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-01', '2019-04-17', '02/006', 'P100017', 'SA', 4008000, 4008000, 0, 4008000, 'P900094', 'P900094', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-18', '2019-05-04', '02/082', 'P100017', 'SA', 3525000, 3525000, 0, 3525000, 'P900097', 'P900097', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-19', '2019-05-05', '02/085', 'P100017', 'SA', 3430000, 3430000, 0, 3430000, 'P900098', 'P900098', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-23', '2019-05-09', '02/110', 'P100017', 'SA', 11052000, 11052000, 0, 11052000, 'P900099', 'P900099', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-27', '2019-05-13', '02/135', 'P100017', 'SA', 11837000, 11837000, 0, 11837000, 'P900100', 'P900100', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-28', '2019-05-14', '02/141', 'P100017', 'SA', 1470000, 1470000, 0, 1470000, 'P900101', 'P900101', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-08-18', '06/026', 'P100018', 'SA', 6014000, 6014000, 0, 6014000, 'P900103', 'P900103', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-08-24', '06/063', 'P100018', 'SA', 24343500, 24343500, 0, 24343500, 'P900104', 'P900104', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-08-28', '06/086', 'P100018', 'SA', 17250000, 17250000, 0, 17250000, 'P900105', 'P900105', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-09-06', '06/152', 'P100018', 'SA', 3564000, 3564000, 0, 3564000, 'P900107', 'P900107', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-09-26', '07/008', 'P100018', 'SA', 8946000, 8946000, 0, 8946000, 'P900108', 'P900108', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-02', '07/051', 'P100018', 'SA', 3571500, 3571500, 0, 3571500, 'P900109', 'P900109', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-04', '07/082', 'P100018', 'SA', 13026000, 13026000, 0, 13026000, 'P900110', 'P900110', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-04', '07/086', 'P100018', 'SA', 5611500, 5611500, 0, 5611500, 'P900111', 'P900111', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-05', '07/091', 'P100018', 'SA', 5334000, 5334000, 0, 5334000, 'P900113', 'P900113', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-06', '07/102', 'P100018', 'SA', 4234000, 4234000, 0, 4234000, 'P900114', 'P900114', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-05', '07/090', 'P100018', 'SA', 9288000, 9288000, 0, 9288000, 'P900112', 'P900112', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-08', '07/114', 'P100018', 'SA', 7488000, 7488000, 0, 7488000, 'P900116', 'P900116', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-09', '07/127', 'P100018', 'SA', 7263500, 7263500, 0, 7263500, 'P900117', 'P900117', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-10', '07/144', 'P100018', 'SA', 11180000, 11180000, 0, 11180000, 'P900119', 'P900119', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-13', '07/170', 'P100018', 'SA', 129500, 129500, 0, 129500, 'P900120', 'P900120', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-15', '08/014', 'P100018', 'SA', 11514250, 11514250, 0, 11514250, 'P900121', 'P900121', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-16', '08/021', 'P100018', 'SA', 2300500, 2300500, 0, 2300500, 'P900122', 'P900122', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-19', '08/064', 'P100018', 'SA', 35030000, 35030000, 0, 35030000, 'P900123', 'P900123', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-20', '08/079', 'P100018', 'SA', 1470000, 1470000, 0, 1470000, 'P900124', 'P900124', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-23', '08/099', 'P100018', 'SA', 22063750, 22063750, 0, 22063750, 'P900125', 'P900125', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-23', '08/109', 'P100018', 'SA', 6825000, 6825000, 0, 6825000, 'P900126', 'P900126', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-24', '08/113', 'P100018', 'SA', 2520000, 2520000, 0, 2520000, 'P900127', 'P900127', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-29', '08/153', 'P100018', 'SA', 7200000, 7200000, 0, 7200000, 'P900128', 'P900128', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-11-05', '08/199', 'P100018', 'SA', 12343250, 12343250, 0, 12343250, 'P900129', 'P900129', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-11-08', '08/227', 'P100018', 'SA', 14742000, 14742000, 0, 14742000, 'P900131', 'P900131', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-11-12', '08/257', 'P100018', 'SA', 10824000, 10824000, 0, 10824000, 'P900132', 'P900132', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-08-15', 'RETUR', 'P100018', 'SA', -1054750, -1054750, 0, -1054750, 'P900102', 'P900102', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-14', '2019-02-27', '12/088', 'P100017', 'SA', 13408000, 13408000, 0, 13408000, 'P900072', 'P900072', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/107', 'P100017', 'SA', 2660000, 2660000, 0, 2660000, 'P900073', 'P900073', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-12-14', 'RETUR', 'P100018', 'SA', -6784500, -6784500, 0, -6784500, 'P900139', 'P900139', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2017-05-02', '02/132', 'P100018', 'SA', 13920500, 13920500, 0, 13920500, 'P900140', 'P900140', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-03', '2018-11-17', '09/005', 'P100019', 'SA', 1520000, 1520000, 0, 1520000, 'P900141', 'P900141', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-20', '2018-12-04', '09/097', 'P100019', 'SA', 1242000, 1242000, 0, 1242000, 'P900142', 'P900142', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-02', '2018-12-16', '10/016', 'P100019', 'SA', 2415000, 2415000, 0, 2415000, 'P900143', 'P900143', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-25', '2019-01-08', '10/163', 'P100019', 'SA', 13196250, 13196250, 0, 13196250, 'P900144', 'P900144', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-13', '2019-01-27', '11/074', 'P100019', 'SA', 3168000, 3168000, 0, 3168000, 'P900145', 'P900145', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-16', '2019-01-30', '11/103', 'P100019', 'SA', 11540250, 11540250, 0, 11540250, 'P900146', 'P900146', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-17', '2019-03-02', '12/092', 'P100019', 'SA', 2346000, 2346000, 0, 2346000, 'P900147', 'P900147', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-01', '2018-12-15', '10/001', 'P100020', 'SA', 89550000, 89550000, 0, 89550000, 'P900149', 'P900149', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-05', '2018-12-19', '10/042', 'P100020', 'SA', 50456250, 50456250, 0, 50456250, 'P900150', 'P900150', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-05', '2018-12-19', '10/044', 'P100020', 'SA', 32516750, 32516750, 0, 32516750, 'P900151', 'P900151', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-12', '2018-12-26', '10/089', 'P100020', 'SA', 667344000, 667344000, 0, 667344000, 'P900153', 'P900153', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-15', '2018-12-29', '10/101', 'P100020', 'SA', 62558000, 62558000, 0, 62558000, 'P900154', 'P900154', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-18', '2019-01-01', '10/125', 'P100020', 'SA', 14652000, 14652000, 0, 14652000, 'P900155', 'P900155', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-07', '2019-01-21', '11/045', 'P100020', 'SA', 28548000, 28548000, 0, 28548000, 'P900156', 'P900156', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-12', '2019-01-26', '11/070', 'P100020', 'SA', 13754000, 13754000, 0, 13754000, 'P900157', 'P900157', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-23', '2019-02-06', '11/141', 'P100020', 'SA', 75994500, 75994500, 0, 75994500, 'P900158', 'P900158', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-11', '2019-02-24', '12/064', 'P100020', 'SA', 15312000, 15312000, 0, 15312000, 'P900159', 'P900159', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/010', 'P100020', 'SA', 12361500, 12361500, 0, 12361500, 'P900161', 'P900161', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '01/131', 'P100020', 'SA', 43788000, 43788000, 0, 43788000, 'P900162', 'P900162', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-24', '2019-04-09', '01/144', 'P100020', 'SA', 22770000, 22770000, 0, 22770000, 'P900163', 'P900163', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/167', 'P100020', 'SA', 19973250, 19973250, 0, 19973250, 'P900164', 'P900164', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-30', '2019-04-15', '01/184', 'P100020', 'SA', 49919750, 49919750, 0, 49919750, 'P900165', 'P900165', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-31', '2019-04-16', '01/193', 'P100020', 'SA', 11561000, 11561000, 0, 11561000, 'P900166', 'P900166', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-12', '2019-04-28', '02/053', 'P100020', 'SA', 59010750, 59010750, 0, 59010750, 'P900167', 'P900167', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-22', '2019-05-08', '02/106', 'P100020', 'SA', 13005000, 13005000, 0, 13005000, 'P900168', 'P900168', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-28', '2019-05-14', '02/142', 'P100020', 'SA', 9450000, 9450000, 0, 9450000, 'P900169', 'P900169', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-03-05', '2018-05-19', '03/028', 'P100021', 'SA', 5347500, 5347500, 0, 5347500, 'P900170', 'P900170', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-04-02', '2018-06-16', '04/002', 'P100021', 'SA', 8659500, 8659500, 0, 8659500, 'P900171', 'P900171', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-09', '2018-07-23', '05/072', 'P100021', 'SA', 53199000, 53199000, 0, 53199000, 'P900174', 'P900174', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-18', '2018-08-01', '05/136', 'P100021', 'SA', 4157250, 4157250, 0, 4157250, 'P900175', 'P900175', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-21', '2018-08-04', '05/155', 'P100021', 'SA', 2498500, 2498500, 0, 2498500, 'P900176', 'P900176', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-24', '2018-08-07', '05/200', 'P100021', 'SA', 3122250, 3122250, 0, 3122250, 'P900177', 'P900177', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-26', '2018-08-09', '05/215', 'P100021', 'SA', 3915750, 3915750, 0, 3915750, 'P900178', 'P900178', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-30', '2018-08-13', '05/230', 'P100021', 'SA', 2932500, 2932500, 0, 2932500, 'P900179', 'P900179', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-06-06', '2018-08-20', '06/042', 'P100021', 'SA', 3760500, 3760500, 0, 3760500, 'P900180', 'P900180', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-06-28', '2018-09-11', '06/081', 'P100021', 'SA', 2984250, 2984250, 0, 2984250, 'P900181', 'P900181', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-04', '2018-09-17', '07/022', 'P100021', 'SA', 25978500, 25978500, 0, 25978500, 'P900182', 'P900182', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-16', '2018-09-29', '07/109', 'P100021', 'SA', 15939000, 15939000, 0, 15939000, 'P900183', 'P900183', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-30', '2018-10-13', '07/216', 'P100021', 'SA', 2415000, 2415000, 0, 2415000, 'P900185', 'P900185', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-27', '2018-11-10', '08/203', 'P100021', 'SA', 4260000, 4260000, 0, 4260000, 'P900186', 'P900186', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-24', '2018-12-08', '09/125', 'P100021', 'SA', 5325000, 5325000, 0, 5325000, 'P900187', 'P900187', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-16', '2018-12-30', '10/109', 'P100021', 'SA', 9514000, 9514000, 0, 9514000, 'P900188', 'P900188', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-25', '2019-01-08', '10/164', 'P100021', 'SA', 5918000, 5918000, 0, 5918000, 'P900189', 'P900189', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-06', '2019-01-20', '11/037', 'P100021', 'SA', 48599500, 48599500, 0, 48599500, 'P900190', 'P900190', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-14', '2019-01-28', '11/086', 'P100021', 'SA', 22649000, 22649000, 0, 22649000, 'P900191', 'P900191', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-12', '2019-02-25', '12/068', 'P100021', 'SA', 6993500, 6993500, 0, 6993500, 'P900193', 'P900193', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-21', '2019-03-06', '12/133', 'P100021', 'SA', 4828000, 4828000, 0, 4828000, 'P900194', 'P900194', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-11-23', '09/075', 'P100018', 'SA', 6720000, 6720000, 0, 6720000, 'P900134', 'P900134', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-11-24', '09/086', 'P100018', 'SA', 5670000, 5670000, 0, 5670000, 'P900135', 'P900135', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-12-12', '09/217', 'P100018', 'SA', 7048000, 7048000, 0, 7048000, 'P900138', 'P900138', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-29', '2019-04-14', '01/175', 'P100021', 'SA', 9654250, 9654250, 0, 9654250, 'P900199', 'P900199', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-08', '2019-04-24', '02/032', 'P100021', 'SA', 3157250, 3157250, 0, 3157250, 'P900200', 'P900200', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-14', '2019-04-30', '02/065', 'P100021', 'SA', 15768000, 15768000, 0, 15768000, 'P900201', 'P900201', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-19', '2019-05-05', '02/087', 'P100021', 'SA', 9417000, 9417000, 0, 9417000, 'P900202', 'P900202', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-23', '2019-05-09', '02/109', 'P100021', 'SA', 2372500, 2372500, 0, 2372500, 'P900203', 'P900203', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-28', '2019-05-14', '02/144', 'P100021', 'SA', 9380500, 9380500, 0, 9380500, 'P900204', 'P900204', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-02', '2018-12-16', '10/015', 'P100022', 'SA', 3162000, 3162000, 0, 3162000, 'P900205', 'P900205', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-06', '2019-02-19', '12/025', 'P100022', 'SA', 5225000, 5225000, 0, 5225000, 'P900206', 'P900206', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/084', 'P100022', 'SA', 17274000, 17274000, 0, 17274000, 'P900207', 'P900207', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-30', '2018-10-13', '07/213', 'P100023', 'SA', 3898500, 3898500, 0, 3898500, 'P900210', 'P900210', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-07', '2018-10-21', '08/053', 'P100023', 'SA', 24717000, 24717000, 0, 24717000, 'P900211', 'P900211', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-13', '2018-10-27', '08/108', 'P100023', 'SA', 11715000, 11715000, 0, 11715000, 'P900212', 'P900212', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-15', '2018-10-29', '08/135', 'P100023', 'SA', 141162000, 141162000, 0, 141162000, 'P900213', 'P900213', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-16', '2018-10-30', '08/150', 'P100023', 'SA', 23047500, 23047500, 0, 23047500, 'P900214', 'P900214', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-27', '2018-11-10', '08/193', 'P100023', 'SA', 4934500, 4934500, 0, 4934500, 'P900215', 'P900215', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-12', '2018-11-26', '09/058', 'P100023', 'SA', 2414000, 2414000, 0, 2414000, 'P900216', 'P900216', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-21', '2018-12-05', '09/106', 'P100023', 'SA', 9656000, 9656000, 0, 9656000, 'P900217', 'P900217', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-15', '2018-12-29', '10/099', 'P100023', 'SA', 11076000, 11076000, 0, 11076000, 'P900218', 'P900218', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-29', '2019-01-12', '10/175', 'P100023', 'SA', 19045750, 19045750, 0, 19045750, 'P900219', 'P900219', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-26', '2019-02-09', '11/157', 'P100023', 'SA', 12318500, 12318500, 0, 12318500, 'P900221', 'P900221', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-19', '2019-03-04', '12/112', 'P100023', 'SA', 15637750, 15637750, 0, 15637750, 'P900222', 'P900222', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/050', 'P100023', 'SA', 7891250, 7891250, 0, 7891250, 'P900223', 'P900223', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-11', '2019-03-27', '01/064', 'P100023', 'SA', 1828250, 1828250, 0, 1828250, 'P900224', 'P900224', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-25', '2019-04-10', '01/150', 'P100023', 'SA', 8979000, 8979000, 0, 8979000, 'P900225', 'P900225', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-01', '2019-04-17', '02/007', 'P100023', 'SA', 3230250, 3230250, 0, 3230250, 'P900226', 'P900226', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-04-27', '2018-07-11', '04/181', 'P100024', 'SA', 6364000, 6364000, 0, 6364000, 'P900227', 'P900227', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-14', '2018-07-28', '05/098', 'P100024', 'SA', 558000, 558000, 0, 558000, 'P900228', 'P900228', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-18', '2018-10-01', '07/127', 'P100024', 'SA', 976500, 976500, 0, 976500, 'P900230', 'P900230', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-10', '2018-11-24', '09/042', 'P100024', 'SA', 617500, 617500, 0, 617500, 'P900231', 'P900231', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-28', '2018-12-12', '09/154', 'P100024', 'SA', 261800, 261800, 0, 261800, 'P900233', 'P900233', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-05', '2019-01-19', '11/024', 'P100024', 'SA', 1034000, 1034000, 0, 1034000, 'P900234', 'P900234', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-19', '2019-02-02', '11/109', 'P100024', 'SA', 1448750, 1448750, 0, 1448750, 'P900235', 'P900235', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-19', '2019-02-02', '11/111', 'P100024', 'SA', 1452000, 1452000, 0, 1452000, 'P900236', 'P900236', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-22', '2019-02-05', '11/130', 'P100024', 'SA', 378750, 378750, 0, 378750, 'P900237', 'P900237', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-30', '2019-02-13', '11/181', 'P100024', 'SA', 2185000, 2185000, 0, 2185000, 'P900238', 'P900238', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-03', '2019-02-16', '12/007', 'P100024', 'SA', 1232000, 1232000, 0, 1232000, 'P900239', 'P900239', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-17', '2019-04-02', '01/098', 'P100024', 'SA', 388000, 388000, 0, 388000, 'P900240', 'P900240', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-11', '2019-04-27', '02/040', 'P100024', 'SA', 2109750, 2109750, 0, 2109750, 'P900241', 'P900241', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-18', '2019-05-04', '02/080', 'P100024', 'SA', 630000, 630000, 0, 630000, 'P900242', 'P900242', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-01', '2019-01-15', '11/011', 'P100025', 'SA', 20440000, 20440000, 0, 20440000, 'P900243', 'P900243', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/011', 'P100025', 'SA', 18000000, 18000000, 0, 18000000, 'P900245', 'P900245', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-27', '2019-05-13', '02/132', 'P100025', 'SA', 12730500, 12730500, 0, 12730500, 'P900246', 'P900246', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-14', '2019-03-30', '01/066', 'P100026', 'SA', 323000, 323000, 0, 323000, 'P900247', 'P900247', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-01', '2019-01-15', '11/002', 'P100027', 'SA', 13397000, 13397000, 0, 13397000, 'P900249', 'P900249', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-28', '2019-02-11', '11/168', 'P100027', 'SA', 72261000, 72261000, 0, 72261000, 'P900251', 'P900251', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-28', '2019-02-11', '11/169', 'P100027', 'SA', 72224000, 72224000, 0, 72224000, 'P900252', 'P900252', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-03', '2019-02-16', '12/009', 'P100027', 'SA', 11761000, 11761000, 0, 11761000, 'P900253', 'P900253', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-05', '2019-02-18', '12/021', 'P100027', 'SA', 9744750, 9744750, 0, 9744750, 'P900254', 'P900254', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-07', '2019-03-23', '01/031', 'P100027', 'SA', 9404750, 9404750, 0, 9404750, 'P900256', 'P900256', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-21', '2019-05-07', '02/099', 'P100026', 'SA', 142392250, 129447500, 0, 142392250, 'P900248', 'P900248', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-04', '2019-03-20', '01/016', 'P100021', 'SA', 17040000, 17040000, 0, 17040000, 'P900197', 'P900197', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-10', '2019-03-26', '01/052', 'P100021', 'SA', 7011250, 7011250, 0, 7011250, 'P900198', 'P900198', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/166', 'P100027', 'SA', 13924250, 13924250, 0, 13924250, 'P900261', 'P900261', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-01', '2019-04-17', '02/008', 'P100027', 'SA', 9139500, 9139500, 0, 9139500, 'P900262', 'P900262', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-04', '2019-04-20', '02/012', 'P100027', 'SA', 29099850, 29099850, 0, 29099850, 'P900263', 'P900263', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-04', '2019-04-20', '02/015', 'P100027', 'SA', 11232000, 11232000, 0, 11232000, 'P900264', 'P900264', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-18', '2019-05-04', '02/079', 'P100027', 'SA', 11657500, 11657500, 0, 11657500, 'P900265', 'P900265', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-27', '2019-05-13', '02/133', 'P100027', 'SA', 12622500, 12622500, 0, 12622500, 'P900266', 'P900266', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-02-15', '2018-05-01', '02/095', 'P100028', 'SA', 131080, 131080, 0, 131080, 'P900267', 'P900267', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-04-24', '2018-07-08', '04/157', 'P100028', 'SA', 59040, 59040, 0, 59040, 'P900268', 'P900268', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-26', '2018-10-09', '07/204', 'P100028', 'SA', 446500, 446500, 0, 446500, 'P900269', 'P900269', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-10', '2018-11-24', '09/045', 'P100029', 'SA', 8494000, 8494000, 0, 8494000, 'P900272', 'P900272', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-10', '2018-11-24', '09/050', 'P100029', 'SA', 4949000, 4949000, 0, 4949000, 'P900273', 'P900273', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-20', '2018-12-04', '09/098', 'P100029', 'SA', 9809500, 9809500, 0, 9809500, 'P900274', 'P900274', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-22', '2018-12-06', '09/114', 'P100029', 'SA', 5439500, 5439500, 0, 5439500, 'P900275', 'P900275', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-22', '2018-12-06', '09/117', 'P100029', 'SA', 7052000, 7052000, 0, 7052000, 'P900276', 'P900276', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-24', '2018-12-08', '09/121', 'P100029', 'SA', 5342750, 5342750, 0, 5342750, 'P900277', 'P900277', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-27', '2018-12-11', '09/143', 'P100029', 'SA', 15824000, 15824000, 0, 15824000, 'P900278', 'P900278', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-01', '2018-12-15', '10/003', 'P100029', 'SA', 18608500, 18608500, 0, 18608500, 'P900279', 'P900279', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-03', '2018-12-17', '10/023', 'P100029', 'SA', 5257000, 5257000, 0, 5257000, 'P900280', 'P900280', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-04', '2018-12-18', '10/033', 'P100029', 'SA', 8060500, 8060500, 0, 8060500, 'P900281', 'P900281', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-10', '2018-12-24', '10/067', 'P100029', 'SA', 9549500, 9549500, 0, 9549500, 'P900283', 'P900283', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-12', '2018-12-26', '10/085', 'P100029', 'SA', 5904500, 5904500, 0, 5904500, 'P900284', 'P900284', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-12', '2018-12-26', '10/087', 'P100029', 'SA', 7073500, 7073500, 0, 7073500, 'P900285', 'P900285', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-12', '2018-12-26', '10/090', 'P100029', 'SA', 9460000, 9460000, 0, 9460000, 'P900286', 'P900286', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-13', '2018-12-27', '10/092', 'P100029', 'SA', 10275000, 10275000, 0, 10275000, 'P900287', 'P900287', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-22', '2019-01-05', '10/134', 'P100029', 'SA', 8514000, 8514000, 0, 8514000, 'P900288', 'P900288', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-23', '2019-01-06', '10/142', 'P100029', 'SA', 9116000, 9116000, 0, 9116000, 'P900289', 'P900289', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-05', '2019-01-19', '11/021', 'P100029', 'SA', 8131000, 8131000, 0, 8131000, 'P900291', 'P900291', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-08', '2019-01-22', '11/047', 'P100029', 'SA', 3139000, 3139000, 0, 3139000, 'P900292', 'P900292', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-08', '2019-01-22', '11/053', 'P100029', 'SA', 20038000, 20038000, 0, 20038000, 'P900293', 'P900293', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-12', '2019-01-26', '11/064', 'P100029', 'SA', 4300000, 4300000, 0, 4300000, 'P900295', 'P900295', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-12', '2019-01-26', '11/067', 'P100029', 'SA', 5145000, 5145000, 0, 5145000, 'P900296', 'P900296', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-14', '2019-01-28', '11/087', 'P100029', 'SA', 8987000, 8987000, 0, 8987000, 'P900297', 'P900297', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-15', '2019-01-29', '11/096', 'P100029', 'SA', 7453750, 7453750, 0, 7453750, 'P900298', 'P900298', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-19', '2019-02-02', '11/115', 'P100029', 'SA', 8428000, 8428000, 0, 8428000, 'P900299', 'P900299', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-23', '2019-02-06', '11/139', 'P100029', 'SA', 9554750, 9554750, 0, 9554750, 'P900300', 'P900300', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-29', '2019-02-12', '11/177', 'P100029', 'SA', 5719000, 5719000, 0, 5719000, 'P900301', 'P900301', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-03', '2019-02-16', '12/006', 'P100029', 'SA', 4816000, 4816000, 0, 4816000, 'P900302', 'P900302', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/052', 'P100029', 'SA', 8643000, 8643000, 0, 8643000, 'P900303', 'P900303', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-12', '2019-02-25', '12/070', 'P100029', 'SA', 2408000, 2408000, 0, 2408000, 'P900304', 'P900304', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-12', '2019-02-25', '12/072', 'P100029', 'SA', 4851000, 4851000, 0, 4851000, 'P900305', 'P900305', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-02', '2019-03-18', '01/001', 'P100029', 'SA', 82089500, 82089500, 0, 82089500, 'P900307', 'P900307', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-02', '2019-03-18', '01/005', 'P100029', 'SA', 12068000, 12068000, 0, 12068000, 'P900308', 'P900308', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-04', '2019-03-20', '01/020', 'P100029', 'SA', 4753000, 4753000, 0, 4753000, 'P900309', 'P900309', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-08', '2019-03-24', '01/035', 'P100029', 'SA', 13824500, 13824500, 0, 13824500, 'P900311', 'P900311', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/043', 'P100029', 'SA', 5332000, 5332000, 0, 5332000, 'P900312', 'P900312', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/082', 'P100029', 'SA', 10900500, 10900500, 0, 10900500, 'P900313', 'P900313', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-25', '2019-04-10', '01/149', 'P100029', 'SA', 2684000, 2684000, 0, 2684000, 'P900314', 'P900314', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/165', 'P100029', 'SA', 5038000, 5038000, 0, 5038000, 'P900315', 'P900315', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/170', 'P100029', 'SA', 4444000, 4444000, 0, 4444000, 'P900316', 'P900316', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-06', '2019-04-22', '02/023', 'P100029', 'SA', 9086000, 9086000, 0, 9086000, 'P900318', 'P900318', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-10', '2019-03-26', '01/058', 'P100027', 'SA', 3648000, 3648000, 0, 3648000, 'P900258', 'P900258', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-18', '2019-04-03', '01/111', 'P100027', 'SA', 10881000, 10881000, 0, 10881000, 'P900260', 'P900260', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/007', 'P100031', 'SA', 1305000, 1305000, 0, 1305000, 'P900323', 'P900323', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/040', 'P100031', 'SA', 18821750, 18821750, 0, 18821750, 'P900324', 'P900324', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/044', 'P100031', 'SA', 470050, 470050, 0, 470050, 'P900325', 'P900325', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-02', '2019-04-18', '02/009', 'P100031', 'SA', 21964000, 21964000, 0, 21964000, 'P900326', 'P900326', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-02', '2019-04-18', '02/009', 'P100031', 'SA', 872100, 872100, 0, 872100, 'P900327', 'P900327', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-02', '2019-04-18', '02/009', 'P100031', 'SA', 193800, 193800, 0, 193800, 'P900328', 'P900328', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-25', '2019-05-11', '02/116', 'P100031', 'SA', 161500, 161500, 0, 161500, 'P900329', 'P900329', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-18', '2019-01-01', '10/117', 'P100032', 'SA', 1277500, 1277500, 0, 1277500, 'P900330', 'P900330', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-25', '2019-01-08', '10/156', 'P100032', 'SA', 5483500, 5483500, 0, 5483500, 'P900331', 'P900331', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-23', '2019-02-06', '11/137', 'P100032', 'SA', 1380000, 1380000, 0, 1380000, 'P900332', 'P900332', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-06', '2019-02-19', '12/032', 'P100032', 'SA', 2750000, 2750000, 0, 2750000, 'P900333', 'P900333', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', '12/154', 'P100032', 'SA', 1241000, 1241000, 0, 1241000, 'P900335', 'P900335', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', '01/086', 'P100032', 'SA', 1275000, 1275000, 0, 1275000, 'P900336', 'P900336', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-01', '2019-04-17', '02/001', 'P100032', 'SA', 4471500, 4471500, 0, 4471500, 'P900337', 'P900337', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-11', '2019-02-24', '12/065', 'P100033', 'SA', 28736250, 28736250, 0, 28736250, 'P900339', 'P900339', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '01/128', 'P100033', 'SA', 18079000, 18079000, 0, 18079000, 'P900340', 'P900340', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-25', '2019-04-10', '01/151', 'P100034', 'SA', 7090000, 7090000, 0, 7090000, 'P900341', 'P900341', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-25', '2019-04-10', '01/152', 'P100034', 'SA', 825000, 825000, 0, 825000, 'P900342', 'P900342', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-20', '2019-05-06', '02/092', 'P100035', 'SA', 214598500, 214598500, 0, 214598500, 'P900343', 'P900343', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/079', 'P100036', 'SA', 3773000, 3773000, 0, 3773000, 'P900344', 'P900344', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-01', '2019-04-17', '02/004', 'P100036', 'SA', 21111000, 21111000, 0, 21111000, 'P900345', 'P900345', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-08', '2019-04-24', '02/031', 'P100037', 'SA', 1632000, 1632000, 0, 1632000, 'P900347', 'P900347', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-06-02', '2018-08-16', '06/010', 'P100038', 'SA', 11818750, 11818750, 0, 11818750, 'P900348', 'P900348', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-04', '2018-09-17', '07/019', 'P100038', 'SA', 9037500, 9037500, 0, 9037500, 'P900349', 'P900349', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-05', '2018-09-18', '07/032', 'P100038', 'SA', 6964000, 6964000, 0, 6964000, 'P900350', 'P900350', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-06', '2018-09-19', '07/034', 'P100038', 'SA', 1680000, 1680000, 0, 1680000, 'P900351', 'P900351', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-30', '2018-10-13', '07/207', 'P100038', 'SA', 2475000, 2475000, 0, 2475000, 'P900352', 'P900352', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-01', '2018-10-15', '08/005', 'P100038', 'SA', 1125000, 1125000, 0, 1125000, 'P900353', 'P900353', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-01', '2018-10-15', '08/011', 'P100038', 'SA', 1350000, 1350000, 0, 1350000, 'P900354', 'P900354', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-03', '2018-10-17', '08/033', 'P100038', 'SA', 1343000, 1343000, 0, 1343000, 'P900355', 'P900355', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-06', '2018-10-20', '08/043', 'P100038', 'SA', 43489500, 43489500, 0, 43489500, 'P900356', 'P900356', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-08', '2018-10-22', '08/062', 'P100038', 'SA', 1181250, 1181250, 0, 1181250, 'P900357', 'P900357', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-04', '2018-11-18', '09/013', 'P100038', 'SA', 9141250, 9141250, 0, 9141250, 'P900360', 'P900360', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-05', '2018-11-19', '09/022', 'P100038', 'SA', 6497750, 6497750, 0, 6497750, 'P900361', 'P900361', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-06', '2018-11-20', '09/032', 'P100038', 'SA', 18609250, 18609250, 0, 18609250, 'P900362', 'P900362', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-20', '2018-12-04', '09/101', 'P100038', 'SA', 4393500, 4393500, 0, 4393500, 'P900363', 'P900363', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-21', '2018-12-05', '09/111', 'P100038', 'SA', 3653750, 3653750, 0, 3653750, 'P900364', 'P900364', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-27', '2018-12-11', '09/145', 'P100038', 'SA', 5000500, 5000500, 0, 5000500, 'P900365', 'P900365', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-05', '2018-12-19', '10/045', 'P100038', 'SA', 2942750, 2942750, 0, 2942750, 'P900366', 'P900366', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-11', '2018-12-25', '10/081', 'P100038', 'SA', 20856000, 20856000, 0, 20856000, 'P900367', 'P900367', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-15', '2018-12-29', '10/098', 'P100038', 'SA', 2250000, 2250000, 0, 2250000, 'P900368', 'P900368', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-19', '2019-01-02', '10/130', 'P100038', 'SA', 34894250, 34894250, 0, 34894250, 'P900369', 'P900369', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-29', '2019-01-12', '10/177', 'P100038', 'SA', 46730500, 46730500, 0, 46730500, 'P900371', 'P900371', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-30', '2019-01-13', '10/179', 'P100038', 'SA', 13557000, 13557000, 0, 13557000, 'P900372', 'P900372', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-15', '2019-01-29', '11/095', 'P100038', 'SA', 11081250, 11081250, 0, 11081250, 'P900373', 'P900373', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-26', '2019-02-09', '11/153', 'P100038', 'SA', 3862500, 3862500, 0, 3862500, 'P900374', 'P900374', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/053', 'P100038', 'SA', 7766750, 7766750, 0, 7766750, 'P900375', 'P900375', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/111', 'P100038', 'SA', 26149000, 26149000, 0, 26149000, 'P900376', 'P900376', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-21', '2019-03-06', '12/132', 'P100038', 'SA', 2625000, 2625000, 0, 2625000, 'P900377', 'P900377', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-28', '2019-03-13', '12/165', 'P100038', 'SA', 2550000, 2550000, 0, 2550000, 'P900378', 'P900378', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-11', '2019-03-27', '01/062', 'P100038', 'SA', 6754500, 6754500, 0, 6754500, 'P900380', 'P900380', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-27', '2019-05-13', '02/131', 'P100065', 'SA', 4920000, 312000, 0, 4920000, 'P900321', 'P900321', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/171', 'P100038', 'SA', 5120500, 5120500, 0, 5120500, 'P900384', 'P900384', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-29', '2019-04-14', '01/176', 'P100038', 'SA', 13282500, 13282500, 0, 13282500, 'P900385', 'P900385', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-31', '2019-04-16', '01/190', 'P100038', 'SA', 40581000, 40581000, 0, 40581000, 'P900386', 'P900386', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-01', '2019-04-17', '02/005', 'P100038', 'SA', 4995500, 4995500, 0, 4995500, 'P900387', 'P900387', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-25', '2019-05-11', '02/114', 'P100038', 'SA', 6726500, 6726500, 0, 6726500, 'P900389', 'P900389', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-19', '2018-12-03', '09/094', 'P100039', 'SA', 3557500, 3557500, 0, 3557500, 'P900391', 'P900391', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-10', '2018-12-24', '10/069', 'P100039', 'SA', 5937750, 5937750, 0, 5937750, 'P900392', 'P900392', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-17', '2018-12-31', '10/112', 'P100039', 'SA', 3001500, 3001500, 0, 3001500, 'P900393', 'P900393', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-24', '2019-01-07', '10/151', 'P100039', 'SA', 7395000, 7395000, 0, 7395000, 'P900394', 'P900394', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-01', '2019-01-15', '11/007', 'P100039', 'SA', 52961250, 52961250, 0, 52961250, 'P900395', 'P900395', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-02', '2019-01-16', '11/017', 'P100039', 'SA', 8308500, 8308500, 0, 8308500, 'P900396', 'P900396', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-14', '2019-01-28', '11/088', 'P100039', 'SA', 9048000, 9048000, 0, 9048000, 'P900397', 'P900397', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-06', '2019-02-19', '12/027', 'P100039', 'SA', 3306000, 3306000, 0, 3306000, 'P900398', 'P900398', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/054', 'P100039', 'SA', 9113250, 9113250, 0, 9113250, 'P900399', 'P900399', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/108', 'P100039', 'SA', 6958000, 6958000, 0, 6958000, 'P900400', 'P900400', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/110', 'P100039', 'SA', 9352500, 9352500, 0, 9352500, 'P900401', 'P900401', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-15', '2019-05-01', '02/073', 'P100039', 'SA', 1846750, 1846750, 0, 1846750, 'P900403', 'P900403', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-23', '2019-02-06', '11/135', 'P100040', 'SA', 1468500, 1468500, 0, 1468500, 'P900404', 'P900404', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', '12/159', 'P100040', 'SA', 3293000, 3293000, 0, 3293000, 'P900405', 'P900405', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-23', '2019-04-08', '01/141', 'P100040', 'SA', 6450000, 6450000, 0, 6450000, 'P900406', 'P900406', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-16', '2018-12-30', '10/106', 'P100041', 'SA', 6450000, 6450000, 0, 6450000, 'P900407', 'P900407', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-28', '2019-02-11', '11/164', 'P100041', 'SA', 8300000, 8300000, 0, 8300000, 'P900408', 'P900408', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-02', '2018-12-16', '10/011', 'P100042', 'SA', 2673000, 2673000, 0, 2673000, 'P900410', 'P900410', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-03', '2018-12-17', '10/020', 'P100042', 'SA', 74088000, 74088000, 0, 74088000, 'P900411', 'P900411', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-03', '2018-12-17', '10/028', 'P100042', 'SA', 2546250, 2546250, 0, 2546250, 'P900412', 'P900412', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-10', '2018-12-24', '10/065', 'P100042', 'SA', 7157500, 7157500, 0, 7157500, 'P900413', 'P900413', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-18', '2019-01-01', '10/118', 'P100042', 'SA', 5044500, 5044500, 0, 5044500, 'P900415', 'P900415', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-23', '2019-01-06', '10/139', 'P100042', 'SA', 3071250, 3071250, 0, 3071250, 'P900416', 'P900416', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-25', '2019-01-08', '10/161', 'P100042', 'SA', 7210000, 7210000, 0, 7210000, 'P900417', 'P900417', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-26', '2019-01-09', '10/168', 'P100042', 'SA', 3500000, 3500000, 0, 3500000, 'P900418', 'P900418', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-06', '2019-01-20', '11/035', 'P100042', 'SA', 5670000, 5670000, 0, 5670000, 'P900419', 'P900419', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-06', '2019-01-20', '11/038', 'P100042', 'SA', 3118500, 3118500, 0, 3118500, 'P900420', 'P900420', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-07', '2019-01-21', '11/043', 'P100042', 'SA', 10850000, 10850000, 0, 10850000, 'P900421', 'P900421', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-10', '2019-01-24', '11/058', 'P100042', 'SA', 13073750, 13073750, 0, 13073750, 'P900422', 'P900422', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-12', '2019-01-26', '11/061', 'P100042', 'SA', 2025000, 2025000, 0, 2025000, 'P900423', 'P900423', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-13', '2019-01-27', '11/073', 'P100042', 'SA', 3010000, 3010000, 0, 3010000, 'P900424', 'P900424', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-14', '2019-01-28', '11/090', 'P100042', 'SA', 1470000, 1470000, 0, 1470000, 'P900425', 'P900425', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-16', '2019-01-30', '11/106', 'P100042', 'SA', 2673000, 2673000, 0, 2673000, 'P900428', 'P900428', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-19', '2019-02-02', '11/112', 'P100042', 'SA', 4914675, 4914675, 0, 4914675, 'P900429', 'P900429', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-24', '2019-02-07', '11/146', 'P100042', 'SA', 92592500, 92592500, 0, 92592500, 'P900430', 'P900430', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-26', '2019-02-09', '11/156', 'P100042', 'SA', 5775000, 5775000, 0, 5775000, 'P900431', 'P900431', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-01', '2019-02-14', '12/001', 'P100042', 'SA', 4242000, 4242000, 0, 4242000, 'P900432', 'P900432', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/045', 'P100042', 'SA', 2905000, 2905000, 0, 2905000, 'P900433', 'P900433', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-14', '2019-02-27', '12/082', 'P100042', 'SA', 8522500, 8522500, 0, 8522500, 'P900434', 'P900434', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-15', '2019-02-28', '12/089', 'P100042', 'SA', 6892500, 6892500, 0, 6892500, 'P900435', 'P900435', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/106', 'P100042', 'SA', 1470000, 1470000, 0, 1470000, 'P900436', 'P900436', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/124', 'P100042', 'SA', 2940000, 2940000, 0, 2940000, 'P900437', 'P900437', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/140', 'P100042', 'SA', 1620000, 1620000, 0, 1620000, 'P900439', 'P900439', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-28', '2019-03-13', '12/166', 'P100042', 'SA', 1215000, 1215000, 0, 1215000, 'P900440', 'P900440', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/008', 'P100042', 'SA', 2730000, 2730000, 0, 2730000, 'P900441', 'P900441', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-04', '2019-03-20', '01/024', 'P100042', 'SA', 1923750, 1923750, 0, 1923750, 'P900442', 'P900442', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-17', '2019-04-02', '01/099', 'P100038', 'SA', 5690250, 5690250, 0, 5690250, 'P900382', 'P900382', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-25', '2019-04-10', '01/154', 'P100038', 'SA', 6058250, 6058250, 0, 6058250, 'P900383', 'P900383', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '01/127', 'P100042', 'SA', 5792500, 5792500, 0, 5792500, 'P900447', 'P900447', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-23', '2019-04-08', '01/136', 'P100042', 'SA', 2614500, 2614500, 0, 2614500, 'P900448', 'P900448', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-24', '2019-04-09', '01/147', 'P100042', 'SA', 2520000, 2520000, 0, 2520000, 'P900449', 'P900449', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-25', '2019-04-10', '01/153', 'P100042', 'SA', 6702500, 6702500, 0, 6702500, 'P900450', 'P900450', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-26', '2019-04-11', '01/155', 'P100042', 'SA', 1826000, 1826000, 0, 1826000, 'P900451', 'P900451', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/162', 'P100042', 'SA', 55449250, 55449250, 0, 55449250, 'P900452', 'P900452', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/163', 'P100042', 'SA', 17937500, 17937500, 0, 17937500, 'P900453', 'P900453', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-30', '2019-04-15', '01/183', 'P100042', 'SA', 6622000, 6622000, 0, 6622000, 'P900454', 'P900454', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-31', '2019-04-16', '01/189', 'P100042', 'SA', 2232500, 2232500, 0, 2232500, 'P900455', 'P900455', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-04', '2019-04-20', '02/016', 'P100042', 'SA', 3745000, 3745000, 0, 3745000, 'P900456', 'P900456', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-07', '2019-04-23', '02/025', 'P100042', 'SA', 6422500, 6422500, 0, 6422500, 'P900457', 'P900457', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-14', '2019-04-30', '02/067', 'P100042', 'SA', 3852000, 3852000, 0, 3852000, 'P900459', 'P900459', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-14', '2019-04-30', '02/068', 'P100042', 'SA', 3727500, 3727500, 0, 3727500, 'P900460', 'P900460', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-16', '2019-05-02', '02/077', 'P100042', 'SA', 3195500, 3195500, 0, 3195500, 'P900461', 'P900461', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-26', '2019-05-12', '02/122', 'P100042', 'SA', 5145000, 5145000, 0, 5145000, 'P900463', 'P900463', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-15', '2019-05-01', '02/076', 'P100043', 'SA', 33853750, 33853750, 0, 33853750, 'P900464', 'P900464', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-21', '2019-05-07', '02/102', 'P100043', 'SA', 29930000, 29930000, 0, 29930000, 'P900465', 'P900465', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-04', '2019-04-20', '02/011', 'P100044', 'SA', 1617000, 1617000, 0, 1617000, 'P900466', 'P900466', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-04', '2019-04-20', '02/014', 'P100044', 'SA', 2737500, 2737500, 0, 2737500, 'P900467', 'P900467', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-06', '2019-04-22', '02/017', 'P100044', 'SA', 758500, 758500, 0, 758500, 'P900468', 'P900468', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-06', '2019-04-22', '02/021', 'P100044', 'SA', 2517900, 2517900, 0, 2517900, 'P900469', 'P900469', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-15', '2019-05-01', '02/072', 'P100044', 'SA', 1278000, 1278000, 0, 1278000, 'P900471', 'P900471', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-20', '2019-05-06', '02/091', 'P100044', 'SA', 2013000, 2013000, 0, 2013000, 'P900472', 'P900472', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-21', '2019-05-07', '02/096', 'P100044', 'SA', 5493250, 5493250, 0, 5493250, 'P900473', 'P900473', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-21', '2019-05-07', '02/100', 'P100044', 'SA', 1914000, 1914000, 0, 1914000, 'P900474', 'P900474', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-28', '2019-05-14', '02/138', 'P100044', 'SA', 4890000, 4890000, 0, 4890000, 'P900476', 'P900476', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '1971-07-12', '10/066', 'P100045', 'SA', 3861000, 3861000, 0, 3861000, 'P900477', 'P900477', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-12', '2019-04-28', '02/049', 'P100045', 'SA', 44596500, 44596500, 0, 44596500, 'P900478', 'P900478', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-30', '2019-02-13', '11/184', 'P100046', 'SA', 7718500, 7718500, 0, 7718500, 'P900479', 'P900479', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-08', '2019-02-21', '12/041', 'P100046', 'SA', 11996500, 11996500, 0, 11996500, 'P900480', 'P900480', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/126', 'P100048', 'SA', 28500000, 28500000, 0, 28500000, 'P900483', 'P900483', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-03-16', '2018-05-30', '03/119', 'P100049', 'SA', 1155000, 1155000, 0, 1155000, 'P900484', 'P900484', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-03-27', '2018-06-10', '03/188', 'P100049', 'SA', 6487250, 6487250, 0, 6487250, 'P900485', 'P900485', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-03-27', '2018-06-10', '03/189', 'P100049', 'SA', 2700000, 2700000, 0, 2700000, 'P900486', 'P900486', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-03-27', '2018-06-10', '03/190', 'P100049', 'SA', 22100750, 22100750, 0, 22100750, 'P900487', 'P900487', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-03-28', '2018-06-11', '03/197', 'P100049', 'SA', 1405250, 1405250, 0, 1405250, 'P900488', 'P900488', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-04-06', '2018-06-20', '04/041', 'P100049', 'SA', 14052500, 14052500, 0, 14052500, 'P900489', 'P900489', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-06', '2019-01-20', '11/030', 'P100049', 'SA', 46429500, 46429500, 0, 46429500, 'P900490', 'P900490', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-07', '2019-01-21', '11/042', 'P100049', 'SA', 1615000, 1615000, 0, 1615000, 'P900491', 'P900491', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-16', '2019-01-30', '11/100', 'P100049', 'SA', 14000000, 14000000, 0, 14000000, 'P900492', 'P900492', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-21', '2019-02-04', '11/120', 'P100049', 'SA', 7298000, 7298000, 0, 7298000, 'P900493', 'P900493', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-07', '2019-02-20', '12/035', 'P100049', 'SA', 12577500, 12577500, 0, 12577500, 'P900495', 'P900495', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-07', '2019-02-20', '12/039', 'P100049', 'SA', 3969000, 3969000, 0, 3969000, 'P900496', 'P900496', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/048', 'P100049', 'SA', 168000, 168000, 0, 168000, 'P900497', 'P900497', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-11', '2019-02-24', '12/057', 'P100049', 'SA', 5666750, 5666750, 0, 5666750, 'P900498', 'P900498', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-11', '2019-02-24', '12/060', 'P100049', 'SA', 61026750, 61026750, 0, 61026750, 'P900499', 'P900499', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-11', '2019-02-24', '12/061', 'P100049', 'SA', 30178500, 30178500, 0, 30178500, 'P900500', 'P900500', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/120', 'P100049', 'SA', 2107000, 2107000, 0, 2107000, 'P900501', 'P900501', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-22', '2019-03-07', '12/134', 'P100049', 'SA', 8053500, 8053500, 0, 8053500, 'P900503', 'P900503', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', '12/152', 'P100049', 'SA', 323000, 323000, 0, 323000, 'P900504', 'P900504', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-26', '2019-05-12', '02/124', 'P100044', 'SA', 3048750, 2887500, 0, 3048750, 'P900475', 'P900475', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', '01/092', 'P100042', 'SA', 4420000, 4420000, 0, 4420000, 'P900445', 'P900445', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', '01/087', 'P100049', 'SA', 14309000, 14309000, 0, 14309000, 'P900508', 'P900508', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/160', 'P100049', 'SA', 6320000, 6320000, 0, 6320000, 'P900510', 'P900510', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-07', '2019-04-23', '02/029', 'P100049', 'SA', 656000, 656000, 0, 656000, 'P900512', 'P900512', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-09', '2019-04-25', '02/037', 'P100049', 'SA', 27300000, 27300000, 0, 27300000, 'P900513', 'P900513', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-19', '2019-05-05', '02/089', 'P100049', 'SA', 4095000, 4095000, 0, 4095000, 'P900514', 'P900514', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-26', '2019-05-12', '02/120', 'P100049', 'SA', 4114500, 4114500, 0, 4114500, 'P900515', 'P900515', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-21', '2019-04-06', '01/122', 'P100050', 'SA', 314500, 314500, 0, 314500, 'P900516', 'P900516', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2018-01-15', '11/011', 'P100051', 'SA', 3895000, 3895000, 0, 3895000, 'P900518', 'P900518', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-02-26', '2018-05-12', '02/173', 'P100051', 'SA', 1782000, 1782000, 0, 1782000, 'P900519', 'P900519', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-03-28', '2018-06-11', '03/200', 'P100051', 'SA', 7329000, 7329000, 0, 7329000, 'P900520', 'P900520', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-06', '2018-10-20', '08/038', 'P100051', 'SA', 10584000, 10584000, 0, 10584000, 'P900521', 'P900521', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-27', '2019-05-13', '02/128', 'P100051', 'SA', 1472000, 1472000, 0, 1472000, 'P900522', 'P900522', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-19', '2018-12-03', '09/091', 'P100052', 'SA', 25295250, 25295250, 0, 25295250, 'P900524', 'P900524', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-02', '2018-12-16', '10/013', 'P100052', 'SA', 3610500, 3610500, 0, 3610500, 'P900525', 'P900525', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-22', '2019-01-05', '10/136', 'P100052', 'SA', 9463500, 9463500, 0, 9463500, 'P900526', 'P900526', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-25', '2019-01-08', '10/157', 'P100052', 'SA', 440000, 440000, 0, 440000, 'P900527', 'P900527', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-06', '2019-01-20', '11/033', 'P100052', 'SA', 2288000, 2288000, 0, 2288000, 'P900528', 'P900528', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-08', '2019-01-22', '11/052', 'P100052', 'SA', 240000, 240000, 0, 240000, 'P900529', 'P900529', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-02', '2018-07-16', '05/011', 'P100053', 'SA', 1290500, 1290500, 0, 1290500, 'P900531', 'P900531', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-07', '2018-07-21', '05/043', 'P100053', 'SA', 3915000, 3915000, 0, 3915000, 'P900532', 'P900532', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-09', '2018-07-23', '05/063', 'P100053', 'SA', 40056000, 40056000, 0, 40056000, 'P900533', 'P900533', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-15', '2018-07-29', '05/107', 'P100053', 'SA', 2200000, 2200000, 0, 2200000, 'P900535', 'P900535', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-18', '2018-08-01', '05/135', 'P100053', 'SA', 15018750, 15018750, 0, 15018750, 'P900536', 'P900536', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-22', '2018-08-05', '05/168', 'P100053', 'SA', 7104000, 7104000, 0, 7104000, 'P900537', 'P900537', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-24', '2018-08-07', '05/195', 'P100053', 'SA', 2610000, 2610000, 0, 2610000, 'P900538', 'P900538', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-24', '2018-08-07', '05/198', 'P100053', 'SA', 3115000, 3115000, 0, 3115000, 'P900539', 'P900539', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-31', '2018-08-14', '05/234', 'P100053', 'SA', 13982500, 13982500, 0, 13982500, 'P900540', 'P900540', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-06-08', '2018-08-22', '06/053', 'P100053', 'SA', 1360000, 1360000, 0, 1360000, 'P900541', 'P900541', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-06-26', '2018-09-09', '06/068', 'P100053', 'SA', 26372500, 26372500, 0, 26372500, 'P900542', 'P900542', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-06-28', '2018-09-11', '06/075', 'P100053', 'SA', 41736000, 41736000, 0, 41736000, 'P900543', 'P900543', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-05', '2018-09-18', '07/026', 'P100053', 'SA', 249280, 249280, 0, 249280, 'P900544', 'P900544', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-09', '2018-09-22', '07/047', 'P100053', 'SA', 5930750, 5930750, 0, 5930750, 'P900545', 'P900545', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-11', '2018-09-24', '07/064', 'P100053', 'SA', 4050000, 4050000, 0, 4050000, 'P900547', 'P900547', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-11', '2018-09-24', '07/070', 'P100053', 'SA', 3026000, 3026000, 0, 3026000, 'P900548', 'P900548', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-17', '2018-09-30', '07/115', 'P100053', 'SA', 1246000, 1246000, 0, 1246000, 'P900550', 'P900550', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-18', '2018-10-01', '07/128', 'P100053', 'SA', 2016000, 2016000, 0, 2016000, 'P900551', 'P900551', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-18', '2018-10-01', '07/131', 'P100053', 'SA', 8612500, 8612500, 0, 8612500, 'P900552', 'P900552', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-19', '2018-10-02', '07/144', 'P100053', 'SA', 124640, 124640, 0, 124640, 'P900553', 'P900553', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-23', '2018-10-06', '07/169', 'P100053', 'SA', 2714500, 2714500, 0, 2714500, 'P900554', 'P900554', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-30', '2018-10-13', '07/215', 'P100053', 'SA', 832500, 832500, 0, 832500, 'P900555', 'P900555', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-31', '2018-10-14', '07/218', 'P100053', 'SA', 10658000, 10658000, 0, 10658000, 'P900556', 'P900556', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-07', '2018-10-21', '08/055', 'P100053', 'SA', 42099250, 42099250, 0, 42099250, 'P900557', 'P900557', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-09', '2018-10-23', '08/079', 'P100053', 'SA', 29710250, 29710250, 0, 29710250, 'P900559', 'P900559', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-11', '2018-10-25', '08/101', 'P100053', 'SA', 3003000, 3003000, 0, 3003000, 'P900560', 'P900560', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-18', '2018-11-01', '08/155', 'P100053', 'SA', 287000, 287000, 0, 287000, 'P900561', 'P900561', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-21', '2018-11-04', '08/166', 'P100053', 'SA', 8158500, 8158500, 0, 8158500, 'P900562', 'P900562', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-31', '2018-11-14', '08/224', 'P100053', 'SA', 33957000, 33957000, 0, 33957000, 'P900563', 'P900563', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-05', '2018-11-19', '09/023', 'P100053', 'SA', 2670000, 2670000, 0, 2670000, 'P900564', 'P900564', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-06', '2018-11-20', '09/028', 'P100053', 'SA', 211225, 211225, 0, 211225, 'P900565', 'P900565', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-07', '2018-11-21', '09/035', 'P100053', 'SA', 392275, 392275, 0, 392275, 'P900566', 'P900566', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2018-01-06', '10/174', 'P100051', 'SA', 27060000, 27060000, 0, 27060000, 'P900517', 'P900517', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-07', '2019-03-23', '01/026', 'P100049', 'SA', 13650000, 13650000, 0, 13650000, 'P900506', 'P900506', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/039', 'P100049', 'SA', 6833750, 6833750, 0, 6833750, 'P900507', 'P900507', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-04', '2018-12-18', '10/031', 'P100053', 'SA', 2937000, 2937000, 0, 2937000, 'P900570', 'P900570', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-09', '2018-12-23', '10/056', 'P100053', 'SA', 68250, 68250, 0, 68250, 'P900571', 'P900571', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-11', '2018-12-25', '10/078', 'P100053', 'SA', 2730000, 2730000, 0, 2730000, 'P900573', 'P900573', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-23', '2019-01-06', '10/138', 'P100053', 'SA', 1512000, 1512000, 0, 1512000, 'P900574', 'P900574', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-30', '2019-01-13', '10/178', 'P100053', 'SA', 37430000, 37430000, 0, 37430000, 'P900575', 'P900575', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-02', '2019-01-16', '11/013', 'P100053', 'SA', 7507500, 7507500, 0, 7507500, 'P900576', 'P900576', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-05', '2019-01-19', '11/020', 'P100053', 'SA', 14498750, 14498750, 0, 14498750, 'P900577', 'P900577', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-05', '2019-01-19', '11/022', 'P100053', 'SA', 2376000, 2376000, 0, 2376000, 'P900578', 'P900578', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-05', '2019-01-19', '11/026', 'P100053', 'SA', 5328000, 5328000, 0, 5328000, 'P900579', 'P900579', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-07', '2019-01-21', '11/044', 'P100053', 'SA', 3082500, 3082500, 0, 3082500, 'P900580', 'P900580', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-09', '2019-01-23', '11/056', 'P100053', 'SA', 24378750, 24378750, 0, 24378750, 'P900581', 'P900581', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-13', '2019-01-27', '11/077', 'P100053', 'SA', 131200, 131200, 0, 131200, 'P900582', 'P900582', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-21', '2019-02-04', '11/117', 'P100053', 'SA', 1319500, 1319500, 0, 1319500, 'P900584', 'P900584', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-23', '2019-02-06', '11/144', 'P100053', 'SA', 14331250, 14331250, 0, 14331250, 'P900585', 'P900585', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-28', '2019-02-11', '11/163', 'P100053', 'SA', 14240000, 14240000, 0, 14240000, 'P900586', 'P900586', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-12', '2019-02-25', '12/067', 'P100053', 'SA', 25161500, 25161500, 0, 25161500, 'P900588', 'P900588', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-13', '2019-02-26', '12/078', 'P100053', 'SA', 4737500, 4737500, 0, 4737500, 'P900589', 'P900589', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-14', '2019-02-27', '12/084', 'P100053', 'SA', 4231500, 4231500, 0, 4231500, 'P900590', 'P900590', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/103', 'P100053', 'SA', 9468000, 9468000, 0, 9468000, 'P900591', 'P900591', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-28', '2019-03-13', '12/163', 'P100053', 'SA', 7939750, 7939750, 0, 7939750, 'P900592', 'P900592', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-04', '2019-03-20', '01/023', 'P100053', 'SA', 4361000, 4361000, 0, 4361000, 'P900593', 'P900593', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-17', '2019-04-02', '01/101', 'P100053', 'SA', 1313500, 1313500, 0, 1313500, 'P900594', 'P900594', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-18', '2019-04-03', '01/108', 'P100053', 'SA', 9323250, 9323250, 0, 9323250, 'P900596', 'P900596', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-21', '2019-04-06', '01/124', 'P100053', 'SA', 442800, 442800, 0, 442800, 'P900597', 'P900597', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-24', '2019-04-09', '01/143', 'P100053', 'SA', 8393250, 8393250, 0, 8393250, 'P900598', 'P900598', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-29', '2019-04-14', '01/172', 'P100053', 'SA', 1366588, 1366588, 0, 1366588, 'P900599', 'P900599', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-01', '2019-04-17', '02/003', 'P100053', 'SA', 107835, 107835, 0, 107835, 'P900600', 'P900600', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-07', '2019-04-23', '02/027', 'P100053', 'SA', 612638, 612638, 0, 612638, 'P900601', 'P900601', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-09', '2019-04-25', '02/038', 'P100053', 'SA', 1813500, 1813500, 0, 1813500, 'P900602', 'P900602', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-13', '2019-04-29', '02/055', 'P100053', 'SA', 8184000, 8184000, 0, 8184000, 'P900603', 'P900603', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-25', '2019-05-11', '02/112', 'P100053', 'SA', 54167750, 54167750, 0, 54167750, 'P900604', 'P900604', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-26', '2019-05-12', '02/121', 'P100053', 'SA', 1634500, 1634500, 0, 1634500, 'P900605', 'P900605', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-28', '2019-05-14', '02/140', 'P100053', 'SA', 9282000, 9282000, 0, 9282000, 'P900608', 'P900608', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-27', '2019-02-10', '11/160', 'P100054', 'SA', 26201250, 26201250, 0, 26201250, 'P900609', 'P900609', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-28', '2019-02-11', '11/165', 'P100054', 'SA', 46980000, 46980000, 0, 46980000, 'P900610', 'P900610', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-23', '2019-04-08', '01/135', 'P100054', 'SA', 48949500, 48949500, 0, 48949500, 'P900611', 'P900611', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-14', '2019-04-30', '02/071', 'P100054', 'SA', 25946250, 25946250, 0, 25946250, 'P900612', 'P900612', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-24', '2019-01-07', '10/149', 'P100055', 'SA', 11041500, 11041500, 0, 11041500, 'P900613', 'P900613', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-25', '2019-01-08', '10/165', 'P100055', 'SA', 5925000, 5925000, 0, 5925000, 'P900614', 'P900614', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-12', '2019-01-26', '11/069', 'P100055', 'SA', 8823000, 8823000, 0, 8823000, 'P900615', 'P900615', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-23', '2019-02-06', '11/134', 'P100055', 'SA', 8560000, 8560000, 0, 8560000, 'P900616', 'P900616', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-29', '2019-02-12', '11/176', 'P100055', 'SA', 2830500, 2830500, 0, 2830500, 'P900617', 'P900617', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', '01/088', 'P100055', 'SA', 210000, 210000, 0, 210000, 'P900618', 'P900618', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-22', '2019-05-08', '02/104', 'P100055', 'SA', 903000, 903000, 0, 903000, 'P900620', 'P900620', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-31', '2019-04-16', '01/188', 'P100056', 'SA', 168000000, 168000000, 0, 168000000, 'P900621', 'P900621', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-19', '2019-05-05', '02/088', 'P100056', 'SA', 79640000, 79640000, 0, 79640000, 'P900622', 'P900622', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-23', '2019-04-08', '01/139', 'P100057', 'SA', 8702000, 8702000, 0, 8702000, 'P900623', 'P900623', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-08', '2019-04-24', '02/034', 'P100057', 'SA', 5925000, 5925000, 0, 5925000, 'P900624', 'P900624', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-14', '2019-04-30', '02/070', 'P100057', 'SA', 2622000, 2622000, 0, 2622000, 'P900625', 'P900625', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-21', '2019-02-04', '11/123', 'P100058', 'SA', 110687400, 110687400, 0, 110687400, 'P900627', 'P900627', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-22', '2019-02-05', '11/129', 'P100058', 'SA', 3186000, 3186000, 0, 3186000, 'P900628', 'P900628', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-17', '2018-12-01', '09/074', 'P100053', 'SA', 4400000, 4400000, 0, 4400000, 'P900568', 'P900568', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-07', '2019-04-23', '02/028', 'P100058', 'SA', 3200040, 3200040, 0, 3200040, 'P900634', 'P900634', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-22', '2019-05-08', '02/105', 'P100058', 'SA', 2660000, 2660000, 0, 2660000, 'P900635', 'P900635', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', '01/089', 'P100059', 'SA', 132750, 132750, 0, 132750, 'P900636', 'P900636', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-17', '2019-04-02', '01/097', 'P100060', 'SA', 77542425, 77542425, 0, 77542425, 'P900637', 'P900637', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-29', '2019-04-14', '01/173', 'P100060', 'SA', 121019400, 121019400, 0, 121019400, 'P900638', 'P900638', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/045', 'P100061', 'SA', 276250, 276250, 0, 276250, 'P900639', 'P900639', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-14', '2019-03-30', '01/073', 'P100061', 'SA', 311500, 311500, 0, 311500, 'P900640', 'P900640', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '01/125', 'P100061', 'SA', 85000, 85000, 0, 85000, 'P900641', 'P900641', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-11', '2019-04-27', '02/047', 'P100061', 'SA', 624750, 624750, 0, 624750, 'P900642', 'P900642', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-06', '2018-11-20', '09/027', 'P100062', 'SA', 6497750, 6497750, 0, 6497750, 'P900643', 'P900643', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-10', '2018-11-24', '09/043', 'P100062', 'SA', 11484250, 11484250, 0, 11484250, 'P900644', 'P900644', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-12', '2018-11-26', '09/054', 'P100062', 'SA', 1617000, 1617000, 0, 1617000, 'P900646', 'P900646', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-13', '2018-11-27', '09/065', 'P100062', 'SA', 3523500, 3523500, 0, 3523500, 'P900647', 'P900647', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-14', '2018-11-28', '09/069', 'P100062', 'SA', 3555000, 3555000, 0, 3555000, 'P900648', 'P900648', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-17', '2018-12-01', '09/078', 'P100062', 'SA', 12495000, 12495000, 0, 12495000, 'P900649', 'P900649', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-28', '2018-12-12', '09/155', 'P100062', 'SA', 57072000, 57072000, 0, 57072000, 'P900651', 'P900651', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-03', '2018-12-17', '10/024', 'P100062', 'SA', 10183500, 10183500, 0, 10183500, 'P900652', 'P900652', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-04', '2018-12-18', '10/034', 'P100062', 'SA', 4740000, 4740000, 0, 4740000, 'P900653', 'P900653', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-22', '2019-01-05', '10/135', 'P100062', 'SA', 3759000, 3759000, 0, 3759000, 'P900654', 'P900654', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-25', '2019-01-08', '10/159', 'P100062', 'SA', 2343000, 2343000, 0, 2343000, 'P900655', 'P900655', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-30', '2019-01-13', '10/180', 'P100062', 'SA', 13370750, 13370750, 0, 13370750, 'P900656', 'P900656', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-07', '2019-01-21', '11/046', 'P100062', 'SA', 6720000, 6720000, 0, 6720000, 'P900657', 'P900657', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-19', '2019-02-02', '11/114', 'P100062', 'SA', 11058250, 11058250, 0, 11058250, 'P900659', 'P900659', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-21', '2019-02-04', '11/118', 'P100062', 'SA', 10420750, 10420750, 0, 10420750, 'P900660', 'P900660', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-27', '2019-02-10', '11/159', 'P100062', 'SA', 3208000, 3208000, 0, 3208000, 'P900661', 'P900661', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-06', '2019-02-19', '12/030', 'P100062', 'SA', 4759750, 4759750, 0, 4759750, 'P900662', 'P900662', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/046', 'P100062', 'SA', 24068500, 24068500, 0, 24068500, 'P900663', 'P900663', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-11', '2019-02-24', '12/066', 'P100062', 'SA', 1165250, 1165250, 0, 1165250, 'P900664', 'P900664', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-13', '2019-02-26', '12/074', 'P100062', 'SA', 8981500, 8981500, 0, 8981500, 'P900665', 'P900665', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-13', '2019-02-26', '12/080', 'P100062', 'SA', 3887250, 3887250, 0, 3887250, 'P900666', 'P900666', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-14', '2019-02-27', '12/083', 'P100062', 'SA', 18087250, 18087250, 0, 18087250, 'P900667', 'P900667', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-14', '2019-02-27', '12/085', 'P100062', 'SA', 11271250, 11271250, 0, 11271250, 'P900668', 'P900668', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/148', 'P100062', 'SA', 6933500, 6933500, 0, 6933500, 'P900671', 'P900671', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-28', '2019-03-13', '12/164', 'P100062', 'SA', 4117750, 4117750, 0, 4117750, 'P900672', 'P900672', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/012', 'P100062', 'SA', 36803500, 36803500, 0, 36803500, 'P900673', 'P900673', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-08', '2019-03-24', '01/038', 'P100062', 'SA', 4753000, 4753000, 0, 4753000, 'P900674', 'P900674', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-10', '2019-03-26', '01/060', 'P100062', 'SA', 3185000, 3185000, 0, 3185000, 'P900675', 'P900675', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', '01/094', 'P100062', 'SA', 3900000, 3900000, 0, 3900000, 'P900676', 'P900676', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-17', '2019-04-02', '01/102', 'P100062', 'SA', 12173500, 12173500, 0, 12173500, 'P900677', 'P900677', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-18', '2019-04-03', '01/109', 'P100062', 'SA', 21772250, 21772250, 0, 21772250, 'P900678', 'P900678', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-19', '2019-04-04', '01/119', 'P100062', 'SA', 9161500, 9161500, 0, 9161500, 'P900679', 'P900679', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-30', '2019-04-15', '01/181', 'P100062', 'SA', 3400000, 3400000, 0, 3400000, 'P900680', 'P900680', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-13', '2019-04-29', '02/057', 'P100062', 'SA', 22640000, 22640000, 0, 22640000, 'P900682', 'P900682', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-13', '2019-04-29', '02/059', 'P100062', 'SA', 2430000, 2430000, 0, 2430000, 'P900683', 'P900683', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-16', '2019-05-02', '02/078', 'P100062', 'SA', 18870500, 18870500, 0, 18870500, 'P900684', 'P900684', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-19', '2019-05-05', '02/086', 'P100062', 'SA', 8942500, 8942500, 0, 8942500, 'P900685', 'P900685', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-21', '2019-05-07', '02/097', 'P100062', 'SA', 11570500, 11570500, 0, 11570500, 'P900686', 'P900686', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-25', '2019-05-11', '02/113', 'P100062', 'SA', 4947000, 4947000, 0, 4947000, 'P900687', 'P900687', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-26', '2019-05-12', '02/126', 'P100062', 'SA', 2745000, 2745000, 0, 2745000, 'P900688', 'P900688', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-09', '2018-12-23', '10/057', 'P100064', 'SA', 11550000, 11550000, 0, 11550000, 'P900690', 'P900690', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/109', 'P100058', 'SA', 48770280, 48770280, 0, 48770280, 'P900630', 'P900630', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/146', 'P100058', 'SA', 59597640, 59597640, 0, 59597640, 'P900631', 'P900631', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-12', '2019-02-25', '12/069', 'P100065', 'SA', 3960000, 3960000, 0, 3960000, 'P900695', 'P900695', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-27', '2019-05-13', '02/130', 'P100067', 'SA', 3995000, 3995000, 0, 3995000, 'P900697', 'P900697', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-14', '2019-03-30', '01/071', 'P100068', 'SA', 2871000, 2871000, 0, 2871000, 'P900698', 'P900698', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-08', '2019-04-24', '02/030', 'P100068', 'SA', 12061500, 12061500, 0, 12061500, 'P900699', 'P900699', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-28', '2019-05-14', '02/143', 'P100068', 'SA', 1575000, 1575000, 0, 1575000, 'P900700', 'P900700', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-20', '2018-11-03', '08/162', 'P100069', 'SA', 3152500, 3152500, 0, 3152500, 'P900701', 'P900701', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-03', '2018-11-17', '09/002', 'P100069', 'SA', 4947000, 4947000, 0, 4947000, 'P900702', 'P900702', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-30', '2019-02-13', '11/180', 'P100069', 'SA', 17120500, 17120500, 0, 17120500, 'P900703', 'P900703', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-11', '2019-04-27', '02/046', 'P100069', 'SA', 11112750, 11112750, 0, 11112750, 'P900704', 'P900704', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/047', 'P100070', 'SA', 5654500, 5654500, 0, 5654500, 'P900705', 'P900705', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/104', 'P100071', 'SA', 3528000, 3528000, 0, 3528000, 'P900706', 'P900706', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-11', '2018-12-25', '10/083', 'P100073', 'SA', 4075000, 4075000, 0, 4075000, 'P900708', 'P900708', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/119', 'P100073', 'SA', 6500000, 6500000, 0, 6500000, 'P900709', 'P900709', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-29', '2019-03-14', '12/167', 'P100073', 'SA', 1320000, 1320000, 0, 1320000, 'P900710', 'P900710', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', '01/093', 'P100073', 'SA', 1067000, 1067000, 0, 1067000, 'P900712', 'P900712', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-18', '2019-04-03', '01/113', 'P100073', 'SA', 39645000, 39645000, 0, 39645000, 'P900713', 'P900713', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-17', '2019-04-02', '01/100', 'P100074', 'SA', 9000000, 9000000, 0, 9000000, 'P900714', 'P900714', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-13', '2019-02-26', '12/077', 'P100075', 'SA', 1600500, 1600500, 0, 1600500, 'P900715', 'P900715', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-13', '2019-02-26', '12/081', 'P100075', 'SA', 57048050, 57048050, 0, 57048050, 'P900716', 'P900716', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/078', 'P100075', 'SA', 31646250, 31646250, 0, 31646250, 'P900717', 'P900717', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-04', '2019-04-20', '02/013', 'P100075', 'SA', 13038750, 13038750, 0, 13038750, 'P900719', 'P900719', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-21', '2019-04-06', '01/120', 'P100076', 'SA', 17267250, 17267250, 0, 17267250, 'P900720', 'P900720', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-21', '2019-02-04', '11/119', 'P100077', 'SA', 7707000, 7707000, 0, 7707000, 'P900721', 'P900721', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-21', '2019-05-07', '02/101', 'P100077', 'SA', 2365000, 2365000, 0, 2365000, 'P900722', 'P900722', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-06', '2018-10-20', '08/048', 'P100078', 'SA', 35500000, 35500000, 0, 35500000, 'P900723', 'P900723', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-08', '2018-10-22', '08/068', 'P100078', 'SA', 91666325, 91666325, 0, 91666325, 'P900724', 'P900724', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-13', '2018-10-27', '08/106', 'P100078', 'SA', 35695250, 35695250, 0, 35695250, 'P900725', 'P900725', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-13', '2018-10-27', '08/109', 'P100078', 'SA', 35872750, 35872750, 0, 35872750, 'P900726', 'P900726', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-16', '2018-10-30', '08/140', 'P100078', 'SA', 20678750, 20678750, 0, 20678750, 'P900727', 'P900727', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-20', '2018-11-03', '08/158', 'P100078', 'SA', 31524000, 31524000, 0, 31524000, 'P900728', 'P900728', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-03', '2018-11-17', '09/003', 'P100078', 'SA', 28729750, 28729750, 0, 28729750, 'P900731', 'P900731', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-10', '2018-11-24', '09/046', 'P100078', 'SA', 37479400, 37479400, 0, 37479400, 'P900732', 'P900732', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-18', '2018-12-02', '09/084', 'P100078', 'SA', 34288500, 34288500, 0, 34288500, 'P900733', 'P900733', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-15', '2018-12-29', '10/100', 'P100078', 'SA', 51045400, 51045400, 0, 51045400, 'P900734', 'P900734', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-01', '2019-01-15', '11/001', 'P100078', 'SA', 65398000, 65398000, 0, 65398000, 'P900735', 'P900735', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-13', '2019-01-27', '11/079', 'P100078', 'SA', 43092000, 43092000, 0, 43092000, 'P900736', 'P900736', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-15', '2019-01-29', '11/097', 'P100078', 'SA', 14972000, 14972000, 0, 14972000, 'P900737', 'P900737', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-24', '2019-02-07', '11/148', 'P100078', 'SA', 17760000, 17760000, 0, 17760000, 'P900738', 'P900738', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-03', '2019-02-16', '12/012', 'P100078', 'SA', 46838750, 46838750, 0, 46838750, 'P900739', 'P900739', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/051', 'P100078', 'SA', 69008000, 69008000, 0, 69008000, 'P900740', 'P900740', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/123', 'P100078', 'SA', 22173000, 22173000, 0, 22173000, 'P900742', 'P900742', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/139', 'P100078', 'SA', 12046000, 12046000, 0, 12046000, 'P900743', 'P900743', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', '12/157', 'P100078', 'SA', 113328250, 113328250, 0, 113328250, 'P900744', 'P900744', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/013', 'P100078', 'SA', 17957250, 17957250, 0, 17957250, 'P900745', 'P900745', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-04', '2019-03-20', '01/017', 'P100078', 'SA', 67824750, 67824750, 0, 67824750, 'P900746', 'P900746', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-08', '2019-03-24', '01/037', 'P100078', 'SA', 36062000, 36062000, 0, 36062000, 'P900747', 'P900747', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-10', '2019-03-26', '01/054', 'P100078', 'SA', 20256000, 20256000, 0, 20256000, 'P900748', 'P900748', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '01/130', 'P100078', 'SA', 34428000, 34428000, 0, 34428000, 'P900749', 'P900749', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-30', '2019-04-15', '01/179', 'P100078', 'SA', 30210000, 30210000, 0, 30210000, 'P900750', 'P900750', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-25', '2019-05-11', '02/118', 'P100078', 'SA', 21859500, 21859500, 0, 21859500, 'P900752', 'P900752', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-04', '2019-03-20', '01/022', 'P100065', 'SA', 5280000, 6336000, 0, 5280000, 'P900694', 'P900694', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-14', '2019-03-30', '01/074', 'P100064', 'SA', 5524750, 5524750, 0, 5524750, 'P900693', 'P900693', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-24', '2018-12-08', '09/122', 'P100079', 'SA', 8062000, 8062000, 0, 8062000, 'P900756', 'P900756', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-26', '2019-01-09', '10/167', 'P100079', 'SA', 18592000, 18592000, 0, 18592000, 'P900758', 'P900758', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-21', '2019-02-04', '11/122', 'P100079', 'SA', 15143000, 15143000, 0, 15143000, 'P900759', 'P900759', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-27', '2019-02-10', '11/161', 'P100079', 'SA', 6923000, 6923000, 0, 6923000, 'P900760', 'P900760', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/042', 'P100079', 'SA', 12968000, 12968000, 0, 12968000, 'P900761', 'P900761', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-17', '2019-03-02', '12/096', 'P100079', 'SA', 119967500, 119967500, 0, 119967500, 'P900762', 'P900762', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/116', 'P100079', 'SA', 6232000, 6232000, 0, 6232000, 'P900763', 'P900763', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-04', '2019-03-20', '01/021', 'P100079', 'SA', 18357000, 18357000, 0, 18357000, 'P900764', 'P900764', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/080', 'P100079', 'SA', 7144000, 7144000, 0, 7144000, 'P900765', 'P900765', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-30', '2019-04-15', '01/180', 'P100079', 'SA', 16550000, 16550000, 0, 16550000, 'P900766', 'P900766', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-06', '2018-11-20', '09/030', 'P100080', 'SA', 10123750, 10123750, 0, 10123750, 'P900767', 'P900767', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-17', '2018-12-31', '10/111', 'P100080', 'SA', 9278250, 9278250, 0, 9278250, 'P900768', 'P900768', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-21', '2019-05-07', '02/098', 'P100080', 'SA', 6506500, 6506500, 0, 6506500, 'P900770', 'P900770', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-03', '2019-02-16', '12/008', 'P100001', 'SA', 8282250, 8282250, 0, 8282250, 'P900771', 'P900771', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-06', '2019-02-19', '12/026', 'P100001', 'SA', 9345000, 9345000, 0, 9345000, 'P900773', 'P900773', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-07', '2019-02-20', '12/038', 'P100001', 'SA', 146458400, 146458400, 0, 146458400, 'P900774', 'P900774', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/055', 'P100001', 'SA', 20057263, 20057263, 0, 20057263, 'P900775', 'P900775', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-15', '2019-02-28', '12/090', 'P100001', 'SA', 10440000, 10440000, 0, 10440000, 'P900776', 'P900776', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/117', 'P100001', 'SA', 6675000, 6675000, 0, 6675000, 'P900777', 'P900777', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-03', '2019-03-19', '01/009', 'P100001', 'SA', 20570000, 20570000, 0, 20570000, 'P900778', 'P900778', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-07', '2019-03-23', '01/028', 'P100001', 'SA', 13016250, 13016250, 0, 13016250, 'P900779', 'P900779', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-11', '2019-03-27', '01/063', 'P100001', 'SA', 23807500, 23807500, 0, 23807500, 'P900780', 'P900780', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-28', '2019-04-13', '01/164', 'P100001', 'SA', 12285000, 12285000, 0, 12285000, 'P900782', 'P900782', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-30', '2019-04-15', '01/177', 'P100001', 'SA', 10356250, 10356250, 0, 10356250, 'P900783', 'P900783', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-31', '2019-04-16', '01/194', 'P100001', 'SA', 18632250, 18632250, 0, 18632250, 'P900784', 'P900784', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-08', '2019-04-24', '02/036', 'P100001', 'SA', 9464000, 9464000, 0, 9464000, 'P900785', 'P900785', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-14', '2019-04-30', '02/069', 'P100001', 'SA', 7982000, 7982000, 0, 7982000, 'P900786', 'P900786', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-15', '2019-05-01', '02/075', 'P100001', 'SA', 9282000, 9282000, 0, 9282000, 'P900787', 'P900787', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-24', '2018-04-09', '01/173', 'P100002', 'SA', 184000, 184000, 0, 184000, 'P900788', 'P900788', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-14', '2019-02-27', '12/087', 'P100006', 'SA', 75145000, 75145000, 0, 75145000, 'P900789', 'P900789', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-17', '2019-03-02', '12/098', 'P100006', 'SA', 3610000, 3610000, 0, 3610000, 'P900790', 'P900790', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', '12/160', 'P100006', 'SA', 110200000, 110200000, 0, 110200000, 'P900791', 'P900791', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-07', '2019-03-23', '01/029', 'P100006', 'SA', 3800000, 3800000, 0, 3800000, 'P900792', 'P900792', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-27', '2019-05-13', '02/134', 'P100008', 'SA', 2902500, 2902500, 0, 2902500, 'P900795', 'P900795', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-08-30', '06/110', 'P100018', 'SA', 24768000, 24768000, 0, 24768000, 'P900106', 'P900106', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-10-10', '07/142', 'P100018', 'SA', 6096500, 6096500, 0, 6096500, 'P900118', 'P900118', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-11-07', '08/215', 'P100018', 'SA', 15506250, 15506250, 0, 15506250, 'P900130', 'P900130', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-16', '2019-01-30', '11/102', 'P100012', 'SA', 9405000, 9405000, 0, 9405000, 'P900009', 'P900009', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-08', '2019-03-24', '01/036', 'P100014', 'SA', 6101000, 6101000, 0, 6101000, 'P900030', 'P900030', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-23', '2019-01-06', '10/145', 'P100017', 'SA', 3283000, 3283000, 0, 3283000, 'P900053', 'P900053', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-13', '2019-02-26', '12/076', 'P100017', 'SA', 2737500, 2737500, 0, 2737500, 'P900071', 'P900071', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-14', '2019-03-30', '01/072', 'P100017', 'SA', 10573250, 10573250, 0, 10573250, 'P900082', 'P900082', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-08', '2019-04-24', '02/035', 'P100017', 'SA', 54435000, 54435000, 0, 54435000, 'P900095', 'P900095', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-11-19', '09/032', 'P100018', 'SA', 4860000, 4860000, 0, 4860000, 'P900133', 'P900133', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-01-01', '2016-11-27', '09/087', 'P100018', 'SA', 12134000, 12134000, 0, 12134000, 'P900136', 'P900136', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-14', '2019-04-30', '02/066', 'P100019', 'SA', 2343000, 2343000, 0, 2343000, 'P900148', 'P900148', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-15', '2019-02-28', '12/091', 'P100020', 'SA', 10578000, 10578000, 0, 10578000, 'P900160', 'P900160', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-04-11', '2018-06-25', '04/064', 'P100021', 'SA', 4485000, 4485000, 0, 4485000, 'P900172', 'P900172', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/138', 'P100021', 'SA', 4167750, 4167750, 0, 4167750, 'P900195', 'P900195', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/147', 'P100021', 'SA', 12425000, 12425000, 0, 12425000, 'P900196', 'P900196', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-06', '2018-11-20', '09/026', 'P100079', 'SA', 9658500, 9658500, 0, 9658500, 'P900754', 'P900754', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-17', '2018-12-01', '09/079', 'P100079', 'SA', 19507500, 19507500, 0, 19507500, 'P900755', 'P900755', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-02', '2019-01-16', '11/018', 'P100025', 'SA', 22080000, 22080000, 0, 22080000, 'P900244', 'P900244', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-13', '2019-02-26', '12/079', 'P100027', 'SA', 16777000, 16777000, 0, 16777000, 'P900255', 'P900255', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-09', '2019-03-25', '01/042', 'P100027', 'SA', 18503000, 18503000, 0, 18503000, 'P900257', 'P900257', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/083', 'P100027', 'SA', 12744500, 12744500, 0, 12744500, 'P900259', 'P900259', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-06', '2018-11-20', '09/029', 'P100029', 'SA', 16117500, 16117500, 0, 16117500, 'P900271', 'P900271', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-08', '2018-12-22', '10/052', 'P100029', 'SA', 5702000, 5702000, 0, 5702000, 'P900282', 'P900282', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-09', '2019-01-23', '11/057', 'P100029', 'SA', 4369000, 4369000, 0, 4369000, 'P900294', 'P900294', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-18', '2019-03-03', '12/105', 'P100029', 'SA', 15050000, 15050000, 0, 15050000, 'P900306', 'P900306', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-06', '2019-04-22', '02/019', 'P100029', 'SA', 3450000, 3450000, 0, 3450000, 'P900317', 'P900317', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-13', '2019-04-29', '02/062', 'P100029', 'SA', 3300000, 3300000, 0, 3300000, 'P900319', 'P900319', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-04', '2019-02-17', '12/015', 'P100031', 'SA', 78500, 78500, 0, 78500, 'P900322', 'P900322', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-17', '2019-03-02', '12/095', 'P100032', 'SA', 27251325, 27251325, 0, 27251325, 'P900334', 'P900334', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-19', '2019-01-02', '10/131', 'P100037', 'SA', 168000, 168000, 0, 168000, 'P900346', 'P900346', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-08', '2018-10-22', '08/067', 'P100038', 'SA', 4187000, 4187000, 0, 4187000, 'P900358', 'P900358', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-23', '2019-01-06', '10/141', 'P100038', 'SA', 2531250, 2531250, 0, 2531250, 'P900370', 'P900370', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-14', '2019-03-30', '01/067', 'P100038', 'SA', 7437000, 7437000, 0, 7437000, 'P900381', 'P900381', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-18', '2019-04-03', '01/110', 'P100039', 'SA', 10190500, 10190500, 0, 10190500, 'P900402', 'P900402', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-11', '2018-12-25', '10/079', 'P100042', 'SA', 7262500, 7262500, 0, 7262500, 'P900414', 'P900414', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-16', '2019-01-30', '11/101', 'P100042', 'SA', 7875000, 7875000, 0, 7875000, 'P900426', 'P900426', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-21', '2019-03-06', '12/131', 'P100042', 'SA', 2430000, 2430000, 0, 2430000, 'P900438', 'P900438', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-14', '2019-03-30', '01/069', 'P100042', 'SA', 8662500, 8662500, 0, 8662500, 'P900443', 'P900443', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-19', '2019-04-04', '01/117', 'P100042', 'SA', 454750, 454750, 0, 454750, 'P900446', 'P900446', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-12', '2019-04-28', '02/050', 'P100042', 'SA', 5845000, 5845000, 0, 5845000, 'P900458', 'P900458', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-11', '2019-04-27', '02/045', 'P100044', 'SA', 1342000, 1342000, 0, 1342000, 'P900470', 'P900470', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-05', '2019-02-18', '12/018', 'P100049', 'SA', 10933250, 10933250, 0, 10933250, 'P900494', 'P900494', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-12-29', '2020-03-13', '12/168', 'P100049', 'SA', 86000, 86000, 0, 86000, 'P900505', 'P900505', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-29', '2019-04-14', '01/174', 'P100049', 'SA', 3895000, 3895000, 0, 3895000, 'P900511', 'P900511', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-23', '2018-10-06', '07/175', 'P100052', 'SA', 272250, 272250, 0, 272250, 'P900523', 'P900523', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-05-09', '2018-07-23', '05/068', 'P100053', 'SA', 139400, 139400, 0, 139400, 'P900534', 'P900534', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-09', '2018-09-22', '07/052', 'P100053', 'SA', 129044500, 129044500, 0, 129044500, 'P900546', 'P900546', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-07', '2018-10-21', '08/060', 'P100053', 'SA', 7575750, 7575750, 0, 7575750, 'P900558', 'P900558', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-12', '2018-11-26', '09/055', 'P100053', 'SA', 22825288, 22825288, 0, 22825288, 'P900567', 'P900567', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-09', '2018-12-23', '10/058', 'P100053', 'SA', 23521750, 23521750, 0, 23521750, 'P900572', 'P900572', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-15', '2019-01-29', '11/091', 'P100053', 'SA', 2848000, 2848000, 0, 2848000, 'P900583', 'P900583', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-28', '2019-05-14', '02/139', 'P100053', 'SA', 355725, 355725, 0, 355725, 'P900607', 'P900607', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '01/132', 'P100055', 'SA', 204000, 204000, 0, 204000, 'P900619', 'P900619', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-05', '2019-02-18', '12/023', 'P100058', 'SA', 26163360, 26163360, 0, 26163360, 'P900629', 'P900629', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-22', '2019-04-07', '01/133', 'P100058', 'SA', 316098360, 316098360, 0, 316098360, 'P900632', 'P900632', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-10', '2018-11-24', '09/051', 'P100062', 'SA', 2946750, 2946750, 0, 2946750, 'P900645', 'P900645', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-13', '2019-01-27', '11/076', 'P100062', 'SA', 5922000, 5922000, 0, 5922000, 'P900658', 'P900658', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-20', '2019-03-05', '12/125', 'P100062', 'SA', 18140500, 18140500, 0, 18140500, 'P900669', 'P900669', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-31', '2019-04-16', '01/191', 'P100062', 'SA', 3412750, 3412750, 0, 3412750, 'P900681', 'P900681', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-29', '2019-01-12', '10/172', 'P100064', 'SA', 3753750, 3753750, 0, 3753750, 'P900691', 'P900691', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-12', '2019-04-28', '02/052', 'P100066', 'SA', 1470000, 1470000, 0, 1470000, 'P900696', 'P900696', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-08', '2018-10-22', '08/063', 'P100072', 'SA', 3750000, 3750000, 0, 3750000, 'P900707', 'P900707', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-23', '2019-04-08', '01/138', 'P100075', 'SA', 30663750, 30663750, 0, 30663750, 'P900718', 'P900718', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-21', '2018-11-04', '08/169', 'P100078', 'SA', 13223750, 13223750, 0, 13223750, 'P900729', 'P900729', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-11', '2019-02-24', '12/063', 'P100078', 'SA', 12825000, 12825000, 0, 12825000, 'P900741', 'P900741', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-26', '2019-05-12', '02/123', 'P100078', 'SA', 12538500, 12538500, 0, 12538500, 'P900753', 'P900753', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-05', '2018-12-19', '10/038', 'P100079', 'SA', 6554000, 6554000, 0, 6554000, 'P900757', 'P900757', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-23', '2019-04-08', '01/142', 'P100080', 'SA', 7462000, 7462000, 0, 7462000, 'P900769', 'P900769', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', '01/081', 'P100001', 'SA', 10327500, 10327500, 0, 10327500, 'P900781', 'P900781', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-01', '2019-02-14', '12/002', 'P100012', 'SA', 2961000, 2961000, 0, 2961000, 'P900010', 'P900010', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/043', 'P100014', 'SA', 2887500, 2887500, 0, 2887500, 'P900020', 'P900020', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-19', '2019-04-04', '01/116', 'P100014', 'SA', 3285000, 3285000, 0, 3285000, 'P900035', 'P900035', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-07', '2019-04-23', '02/024', 'P100014', 'SA', 1567500, 1567500, 0, 1567500, 'P900039', 'P900039', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-05', '2019-01-19', '11/027', 'P100017', 'SA', 6993500, 6993500, 0, 6993500, 'P900058', 'P900058', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-08', '2019-01-22', '11/049', 'P100017', 'SA', 3528000, 3528000, 0, 3528000, 'P900059', 'P900059', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-26', '2019-03-11', '12/142', 'P100017', 'SA', 7254000, 7254000, 0, 7254000, 'P900075', 'P900075', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-13', '2019-04-29', '02/061', 'P100017', 'SA', 5304000, 5304000, 0, 5304000, 'P900096', 'P900096', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-10-09', '2018-12-23', '10/062', 'P100020', 'SA', 14894000, 14894000, 0, 14894000, 'P900152', 'P900152', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-04-27', '2018-07-11', '04/176', 'P100021', 'SA', 4605750, 4605750, 0, 4605750, 'P900173', 'P900173', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-07-17', '2018-09-30', '07/121', 'P100021', 'SA', 2893250, 2893250, 0, 2893250, 'P900184', 'P900184', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-10', '2019-02-23', '12/049', 'P100021', 'SA', 9531750, 9531750, 0, 9531750, 'P900192', 'P900192', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-06', '2019-04-22', '02/020', 'P100022', 'SA', 25256000, 25256000, 0, 25256000, 'P900208', 'P900208', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-22', '2019-05-08', '02/108', 'P100022', 'SA', 5081750, 5081750, 0, 5081750, 'P900209', 'P900209', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-08', '2019-01-22', '11/050', 'P100023', 'SA', 5112000, 5112000, 0, 5112000, 'P900220', 'P900220', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-06-05', '2018-08-19', '06/031', 'P100024', 'SA', 838500, 838500, 0, 838500, 'P900229', 'P900229', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-26', '2018-12-10', '09/134', 'P100024', 'SA', 1232000, 1232000, 0, 1232000, 'P900232', 'P900232', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-12', '2019-01-26', '11/068', 'P100027', 'SA', 14522000, 14522000, 0, 14522000, 'P900250', 'P900250', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-09-01', '2018-11-15', '09/001', 'P100029', 'SA', 4515000, 4515000, 0, 4515000, 'P900270', 'P900270', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-11-03', '2019-01-17', '11/019', 'P100029', 'SA', 6514500, 6514500, 0, 6514500, 'P900290', 'P900290', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-07', '2019-03-23', '01/030', 'P100029', 'SA', 4321500, 4321500, 0, 4321500, 'P900310', 'P900310', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-18', '2019-05-04', '02/083', 'P100029', 'SA', 15042000, 15042000, 0, 15042000, 'P900320', 'P900320', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-02-27', '2019-05-13', '02/129', 'P100032', 'SA', 5398750, 5398750, 0, 5398750, 'P900338', 'P900338', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-08-14', '2018-10-28', '08/118', 'P100038', 'SA', 7141600, 7141600, 0, 7141600, 'P900359', 'P900359', 0, 0, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for tbl_po_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_po_asset";
CREATE TABLE "public"."tbl_po_asset" (
  "IDPOAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default",
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default",
  "PPN" float8,
  "Status_PPN" varchar(64) COLLATE "pg_catalog"."default",
  "Percen_Disc" float8,
  "Disc" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Total_Qty" float8,
  "Saldo_Qty" float8,
  "Grand_Total" float8,
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "Batal" varchar(53) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Jenis_PO" varchar(64) COLLATE "pg_catalog"."default",
  "Status" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_po_asset
-- ----------------------------
INSERT INTO "public"."tbl_po_asset" VALUES ('POA1900001', '2019-06-10', 'POA1900001', 'P000017', NULL, NULL, NULL, NULL, 1, 14000, 1, 1, 10000000, 'dsadas', 'aktif', 'P000001', '2019-06-10 20:15:19', NULL, NULL, 'PO Asset', 'aktif');
INSERT INTO "public"."tbl_po_asset" VALUES ('POA1900002', '2019-06-11', 'POA1900002', 'P000017', NULL, NULL, NULL, NULL, 1, 14000, 2, 2, 210000000, '-', 'aktif', 'P000001', '2019-06-11 12:05:43', 'P000001', '2019-06-11 12:06:35', 'PO Asset', 'aktif');

-- ----------------------------
-- Table structure for tbl_po_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_po_asset_detail";
CREATE TABLE "public"."tbl_po_asset_detail" (
  "IDPOAssetDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDPOAsset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Saldo" float8,
  "Harga_Satuan" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Subtotal" float8
)
;

-- ----------------------------
-- Records of tbl_po_asset_detail
-- ----------------------------
INSERT INTO "public"."tbl_po_asset_detail" VALUES ('P000001', 'POA1900001', 'P100006', 2, 2, 10000000, 1, 14000, 20000000);
INSERT INTO "public"."tbl_po_asset_detail" VALUES ('P000002', 'POA1900002', 'P100001', 1, 1, 200000000, 1, 14000, 200000000);
INSERT INTO "public"."tbl_po_asset_detail" VALUES ('P000003', 'POA1900002', 'P100006', 1, 1, 10000000, 1, 14000, 10000000);

-- ----------------------------
-- Table structure for tbl_pu_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pu_asset";
CREATE TABLE "public"."tbl_pu_asset" (
  "IDFBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default",
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default",
  "IDTBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "PPN" float8,
  "Status_PPN" varchar(100) COLLATE "pg_catalog"."default",
  "Percen_Disc" float8,
  "Disc" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Total_Qty" float8,
  "Saldo_Qty" float8,
  "Grand_Total" float8,
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "Batal" varchar(64) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Jatuh_Tempo" varchar(64) COLLATE "pg_catalog"."default",
  "Tanggal_Jatuh_Tempo" date
)
;

-- ----------------------------
-- Records of tbl_pu_asset
-- ----------------------------
INSERT INTO "public"."tbl_pu_asset" VALUES ('P000001', '2019-06-10', 'INVAST1900001', 'P000004', 'P000001', 2000000, 'Include', 0, 0, 1, 14000, 2, 2, 20000000, 'dsadas', 'aktif', 'P000001', '2019-06-10 16:04:20', NULL, NULL, '75', '2019-08-24');

-- ----------------------------
-- Table structure for tbl_pu_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pu_asset_detail";
CREATE TABLE "public"."tbl_pu_asset_detail" (
  "IDFBAssetDetail" varchar(64) COLLATE "pg_catalog"."default",
  "IDFBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "IDTBAssetDetail" varchar(64) COLLATE "pg_catalog"."default",
  "Qty" float8,
  "Saldo" float8,
  "Harga_Satuan" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Subtotal" float8,
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_pu_asset_detail
-- ----------------------------
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000001', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000002', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000003', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000004', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000005', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000006', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000007', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000008', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');
INSERT INTO "public"."tbl_pu_asset_detail" VALUES ('P000009', 'P000001', '1', 2, 2, 10000000, 1, 14000, 20000000, 'P100006');

-- ----------------------------
-- Table structure for tbl_pu_asset_grandtotal
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pu_asset_grandtotal";
CREATE TABLE "public"."tbl_pu_asset_grandtotal" (
  "IDFBAssetGrandTotal" varchar(64) COLLATE "pg_catalog"."default",
  "IDFBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Pembayaran" varchar(255) COLLATE "pg_catalog"."default",
  "IDMataUang" float8,
  "Kurs" float8,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_Total" float8,
  "Sisa" float8
)
;

-- ----------------------------
-- Records of tbl_pu_asset_grandtotal
-- ----------------------------
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000001', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000002', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000003', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000004', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000005', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000006', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000007', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000008', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);
INSERT INTO "public"."tbl_pu_asset_grandtotal" VALUES ('P000009', 'P000001', 'Asset', 1, 14000, 18000000, 0, 2000000, 20000000, 20000000);

-- ----------------------------
-- Table structure for tbl_purchase_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order";
CREATE TABLE "public"."tbl_purchase_order" (
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDPO_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_selesai" date,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDSupplier_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDCorak_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDMerk_seq"'::regclass),
  "IDAgen" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDAgen_seq"'::regclass),
  "Lot" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_Pesanan" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_purchase_order_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty" float8,
  "Saldo" float8,
  "PIC" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Prioritas" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Bentuk" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Panjang" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Point" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kirim" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Stamping" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi1" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Album" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Jenis_PO" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Batal" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Grand_total" float8,
  "M10" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kain" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lembaran" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_purchase_order
-- ----------------------------
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000001', '2019-12-29', 'PO19PMKA00001', '2019-01-15', 'P000004', 'P00000k', 'P000019', 'Prk0019', 'P000001', '-', 'stok', 2, '-', 4800, 4800, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', '-', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 171600000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000002', '2018-11-05', 'PO19PMKA00002', '2018-11-17', 'P000004', 'P00000g', 'P000013', 'Prk0013', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', 'Gulungan standar. Kecuali warna 20/87 50yds', 'PO TTI', 'aktif', 33300000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000n', '2019-02-11', 'PO19PMKA00023', '2019-02-25', 'P000004', 'P00000o', 'P000008', 'Prk0008', 'P000001', '-', 'seragam', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', 'Terpisah', 'Agen', 'Tanpa Cap', '-', '-', '-', '-', 'PO TTI', 'aktif', 35700000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000003', '2019-01-02', 'PO19PMKA00003', '2019-01-18', 'P000004', 'P00000s', 'P000010', 'Prk0010', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', '-', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 38280000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000004', '2019-01-11', 'PO19PMKA00004', '2019-01-20', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'stok', 2, '-', 2400, 2400, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 72720000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000005', '2019-01-11', 'PO19PMKA00005', '2019-01-20', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 33000000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000007', '2019-01-15', 'PO19PMKA00007', '2019-03-26', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 36360000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000o', '2019-02-12', 'PO19PMKA00024', '2019-02-26', 'P000004', 'P00000b', 'P000007', 'Prk0007', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 36900000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000p', '2019-02-13', 'PO19PMKA00025', '2019-02-27', 'P000004', 'P00000o', 'P000008', 'Prk0008', 'P000001', '-', 'seragam', 2, '-', 4200, 4200, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Tanpa Cap', '-', '-', '-', '-', 'PO TTI', 'aktif', 124950000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000q', '2019-02-13', 'PO19PMKA00026', '2019-03-26', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'stok', 2, '-', 3000, 3000, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 82500000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000006', '2019-01-11', 'PO19PMKA00006', '2019-01-20', 'P000004', 'P00000o', 'P000027', 'Prk0002', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 88035960, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000008', '2019-01-19', 'PO19PMKA00008', '2019-01-25', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'stok', 2, '-', 8400, 8400, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 254520000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000009', '2019-01-19', 'PO19PMKA00009', '2019-01-25', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'stok', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 16500000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000a', '2019-01-24', 'PO19PMKA00010', '2019-01-31', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'stok', 2, '-', 500, 500, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 15150000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000b', '2019-01-24', 'PO19PMKA00011', '2019-01-31', 'P000004', 'P00000s', 'P000010', 'Prk0010', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Terpisah', 'Agen', 'Tanpa Cap', '-', '-', '-', '-', 'PO TTI', 'aktif', 39480000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000c', '2019-01-24', 'PO19PMKA00012', '2019-01-31', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 16290000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000d', '2019-01-24', 'PO19PMKA00013', '2019-01-31', 'P000004', 'P00000b', 'P000007', 'Prk0007', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 36900000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000e', '2019-01-24', 'PO19PMKA00014', '2019-03-27', 'P000004', 'P000004', 'P000005', 'Prk0005', 'P000001', '-', 'stok', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', 'Hati-Hati, warnanya bukan brown tapi green', 'PO TTI', 'aktif', 19050000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000f', '2019-01-24', 'PO19PMKA00015', '2019-01-31', 'P000004', 'P00000n', 'P000004', 'Prk0004', 'P000001', '-', 'stok', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 22020000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000g', '2019-01-24', 'PO19PMKA00016', '2019-01-24', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 2400, 2400, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 65160000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000h', '2019-01-24', 'PO19PMKA00017', '2019-02-01', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 7920, 7920, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', 'Dalam satuan Meter (dikali 1,1)', 'PO TTI', 'aktif', 213840000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000i', '2019-01-28', 'PO19PMKA00018', '2019-02-06', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 3080, 3080, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', 'Tambahan PO 018. Satuan dalam meter (dikali 1,1)', 'PO TTI', 'aktif', 83160000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000j', '2019-01-28', 'PO19PMKA00019', '2019-01-31', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 6000, 6000, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 162900000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000k', '2019-02-06', 'PO19PMKA00020', '2019-02-18', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'seragam', 2, '-', 3600, 3600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 99000000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000l', '2019-02-06', 'PO19PMKA00021', '2019-02-19', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 1000, 1000, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', '-', 'Agen', 'Tanpa Cap', '-', '-', '-', '-', 'PO TTI', 'aktif', 27150000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000m', '2019-02-07', 'PO19PMKA00022', '2019-02-22', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 3600, 3600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 97740000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000r', '2019-02-14', 'PO19PMKA00027', '2019-02-28', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'seragam', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 32580000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000s', '2019-02-16', 'PO19PMKA00028', '2019-03-27', 'P000004', 'P00000c', 'P000009', 'Prk0009', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 39960000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000t', '2019-02-16', 'PO19PMKA00029', '2019-02-28', 'P000004', 'P00000n', 'P000004', 'Prk0004', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 44040000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000u', '2019-02-16', 'PO19PMKA00030', '2019-02-28', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 1800, 1800, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 48870000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000v', '2019-02-18', 'PO19PMKA00031', '2019-03-04', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'stok', 2, '-', 3000, 3000, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 90900000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000w', '2019-02-19', 'PO19PMKA00032', '2019-03-04', 'P000004', 'P00000g', 'P000013', 'Prk0013', 'P000001', '-', 'stok', 2, '-', 3600, 3600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', 'Pjg Gulungan standar kecuali warna 20/87 50yds (besar).', 'PO TTI', 'aktif', 103500000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000x', '2019-02-12', 'PO19PMKA00033', '2019-02-26', 'P000004', 'P000004', 'P000005', 'Prk0005', 'P000001', '-', 'stok', 2, '-', 1800, 1800, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 57150000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000y', '2019-02-19', 'PO19PMKA00034', '2019-02-27', 'P000004', 'P000004', 'P000005', 'Prk0005', 'P000001', '-', 'stok', 2, '-', 3600, 3600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 114300000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00000z', '2019-02-20', 'PO19PMKA00035', '2019-03-08', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'seragam', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 18180000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000010', '2019-02-21', 'PO19PMKA00036', '2019-03-11', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'seragam', 2, '-', 1800, 1800, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 49500000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000011', '2019-02-25', 'PO19PMKA00037', '2019-03-01', 'P000004', 'P000004', 'P000005', 'Prk0005', 'P000001', '-', 'seragam', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 19050000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000012', '2019-02-25', 'PO19PMKA00038', '2019-02-28', 'P000004', 'P000007', 'P000003', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', '-', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 21450000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000013', '2019-02-26', 'PO19PMKA00039', '2019-03-12', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'seragam', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 16290000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000014', '2019-03-01', 'PO19PMKA00040', '2019-03-15', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'seragam', 2, '-', 6000, 6000, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', '-', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 181800000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000015', '2019-03-05', 'PO19PMKA00041', '2019-03-16', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'seragam', 2, '-', 6600, 6600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', 'Satuan dalam meter', 'PO TTI', 'aktif', 198990000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000016', '2019-03-05', 'PO19PMKA00042', '2019-03-16', 'P000004', 'P00000s', 'P000010', 'Prk0010', 'P000001', '-', 'stok', 2, '-', 3000, 3000, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 98700000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000017', '2019-03-06', 'PO19PMKA00043', '2019-03-22', 'P000004', 'P000004', 'P000005', 'Prk0005', 'P000001', '-', 'seragam', 2, '-', 3000, 3000, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 95250000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000018', '2019-03-08', 'PO19PMKA00044', '2019-03-20', 'P000004', 'P00000s', 'P000010', 'Prk0010', 'P000001', '-', 'stok', 2, '-', 600, 600, 'Irwan', 'normal', 'Piece (Double Fold)', 'Standar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 19740000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000019', '2019-03-09', 'PO19PMKA00045', '2019-03-22', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'seragam', 2, '-', 6000, 6000, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', '-', 'Agen', 'Tanpa Cap', '-', '-', '-', '-', 'PO TTI', 'aktif', 165000000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00001a', '2019-03-11', 'PO19PMKA00046', '2019-03-26', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'seragam', 2, '-', 7200, 7200, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', 'Terpisah', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 198000000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00001b', '2019-03-11', 'PO19PMKA00047', '2019-03-26', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'seragam', 2, '-', 9000, 9000, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Tanpa Cap', '-', '-', '-', '-', 'PO TTI', 'aktif', 247500000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00001c', '2019-03-13', 'PO19PMKA00048', '2019-03-28', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'seragam', 2, '-', 4800, 4800, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 145440000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00001d', '2019-03-13', 'PO19PMKA00049', '2019-03-22', 'P000004', 'P00000l', 'P000018', 'Prk0018', 'P000001', '-', 'seragam', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', 'Terpisah', 'Agen', 'Tanpa Cap', '-', '-', '-', '-', 'PO TTI', 'aktif', 53400000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00001e', '2019-03-13', 'PO19PMKA00050', '2019-03-26', 'P000004', 'P000006', 'P000015', 'Prk0015', 'P000001', '-', 'stok', 2, '-', 600, 600, 'Irwan', 'normal', 'Roll (Single Fold)', 'Besar', 'Terpisah', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 16500000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00001f', '2019-03-20', 'PO19PMKA00051', '2019-04-05', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'seragam', 2, '-', 2400, 2400, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 65160000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00001g', '2019-01-24', 'PO19PMKA00052', '2019-01-31', 'P000004', 'P000007', 'P000012', 'Prk0012', 'P000001', '-', 'stok', 2, '-', 1200, 1200, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 32580000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P00001h', '2018-12-20', 'PO19PMKA00053', '2019-01-08', 'P000004', 'P000005', 'P000011', 'Prk0011', 'P000001', '-', 'stok', 2, '-', 12000, 12000, 'Irwan', 'normal', 'Piece (Double Fold)', 'Besar', 'Ikut piece lain', 'Agen', 'Cap', 'Selvedge', 'Face', '-', '-', 'PO TTI', 'aktif', 351600000, '-', '-', '-');

-- ----------------------------
-- Table structure for tbl_satuan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_satuan";
CREATE TABLE "public"."tbl_satuan" (
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_satuan_IDSatuan_seq"'::regclass),
  "Kode_Satuan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_satuan
-- ----------------------------
INSERT INTO "public"."tbl_satuan" VALUES ('P000004', 'Yard', 'Yard', 'aktif');
INSERT INTO "public"."tbl_satuan" VALUES ('P000002', 'Mtr', 'Meter', 'aktif');
INSERT INTO "public"."tbl_satuan" VALUES ('P000003', 'Pcs', 'Piece', 'aktif');

-- ----------------------------
-- Table structure for tbl_stok
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_stok";
CREATE TABLE "public"."tbl_stok" (
  "IDStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDStok_seq"'::regclass),
  "Tanggal" date,
  "Nomor_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDFaktur_seq"'::regclass),
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDFakturDetail_seq"'::regclass),
  "Jenis_faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDWarna_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDGudang_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_stok_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8
)
;

-- ----------------------------
-- Records of tbl_stok
-- ----------------------------
INSERT INTO "public"."tbl_stok" VALUES ('P000001', '2019-06-10', 'PBA1900001', 'P000001', 'P000002', 'PBA', '-', 'P100006', '-', '-', '0', NULL, NULL, NULL, NULL, NULL, '28', 10000000, 1, '', 20000000);
INSERT INTO "public"."tbl_stok" VALUES ('P000002', '2019-06-11', 'PBA1900002', 'P000002', 'P000003', 'PBA', '-', 'P100001', '-', '-', '0', NULL, NULL, NULL, NULL, NULL, '29', 200000000, 1, '', 200000000);
INSERT INTO "public"."tbl_stok" VALUES ('P000003', '2019-06-11', 'PBA1900002', 'P000002', 'P000004', 'PBA', '-', 'P100006', '-', '-', '0', NULL, NULL, NULL, NULL, NULL, '30', 10000000, 1, '', 10000000);

-- ----------------------------
-- Table structure for tbl_suplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_suplier";
CREATE TABLE "public"."tbl_suplier" (
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_suplier_IDSupplier_seq"'::regclass),
  "IDGroupSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_suplier_IDGroupSupplier_seq"'::regclass),
  "Kode_Suplier" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default",
  "No_Telpon" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Fax" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(35) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_KTP" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_suplier
-- ----------------------------
INSERT INTO "public"."tbl_suplier" VALUES ('P000006', 'P000001', 'JJ', 'JUST JAIT INDONESIA', 'Komp. Batununggal Indah I no. 12', '022-95056000/7502022', 'P000001', '022-95056000/7502022', NULL, 'BDG', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000008', 'P000001', 'TTL', 'TRIKARYA TEXTILINDO', 'JL. MANGGA DUA RAYA BLOK. C4/12', '021-6019122', 'P000010', '021-6013908', NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000009', 'P000002', 'mu', 'MODERA UTAMA TEXTILE', 'RUKO ITC BARANANGSIANG', '022-4222065', 'P000001', NULL, NULL, 'BDG', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000010', 'P000002', 'HM', 'HEGAR MULYA', 'JL. Hegar Lokasi II Cibaligo', '022-6030240', 'P000006', '022-6031222', NULL, 'CIMAHI', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000012', 'P000002', 'RMJ', 'REMAJA', 'JL. RAYA RAGUNAN NO. 44 PASAR MINGGU', '021-7802952', 'P000010', '021-7802952', NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000002', 'P000001', 'SRLT-02', 'PT. TRICITRA BUSANA MAS', 'JL. MANGGA DUA RAYA BLOK. C2/6', '021-6018210', 'P000010', '021-6016855', 'tricitrabm@gmail.com', '013394598046000', '0000', NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000007', 'P000001', 'SRLT-03', 'PT. SINAR ABADI CITRANUSA', 'JL. MANGGA DUA RAYA BLOK. C2/31', '021-6013301', 'P000010', '021-6019747', 'lukman@sinarabadi-citranusa.com', '017814310044000', '0000', NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000003', 'P000001', 'SRLT-04', 'PERMATA BUSANAMAS', 'JL. MANGGA DUA RAYA BLOK. D2/22', '021-6128064', 'P000010', '0000', 'maryanawijaya66@gmail.com', '021863733044000', '0000', NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000005', 'P000001', 'SRLT-05', 'PT. CAKRA KENCANA', 'JL. MANGGA DUA RAYA BLOK. C6/12', '021-6016055', 'P000010', '021-6015441', 'ck@gmail.com', '031253511044000', '0000', NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000001', 'P000001', 'SRLT-06', 'PT. SAVANA LESTARI', 'JL. MANGGA DUA RAYA BLOK. C4/12', '021-6019122', 'P000010', '021-6110971', 'savanalestari@yahoo.com', '031253503044000', '0000', NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000011', 'P000002', 'S3RD-01', 'PT. SUMBER GUNUNG LESTARI', 'JL. PASAR PAGI NO. 17', '021-6907216', 'P000010', '0000', 'sgl@gmail.com', '013394507033000', '0000', NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000004', 'P000001', 'SRLT-01', 'PT. TRISULA TEXTILE INDUSTRIES, TBK', 'JL. LEUWI GAJAH NO. 170 ', '022-6613333', 'P000006', '022-6613377', 'TTI@gmail.com', '011185642441000', '76757557', NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000013', 'P000002', 'S3RD-03', 'MODATEX', 'PASAR PAGI, JAKARTA', '000', 'P000010', '0000', 'modatex@gmail.com', '000000000000', '0000', 'aktif');
INSERT INTO "public"."tbl_suplier" VALUES ('P000014', 'P000002', 'GAP', 'PT GAJAH ANGKASA PERKASA', 'JL. Jend. Sudirman No. 823 ', NULL, 'P000001', NULL, NULL, 'BDG', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000015', 'P000002', 'CN', 'CANDRATEX', 'JL. CISIRUNG NO. 48', NULL, 'P000001', NULL, NULL, 'BDG', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000016', 'P000002', 'NI', 'NAGA INTAN', 'JL. PINTU KECIL NO. 16', NULL, 'P000010', NULL, NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000017', 'P000002', 'BMA', 'BERKAT MULIA ABADI', 'JL. DULATIF', NULL, 'P000001', NULL, NULL, 'BDG', NULL, NULL);

-- ----------------------------
-- Table structure for tbl_surat_jalan_makloon_celup
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_makloon_celup";
CREATE TABLE "public"."tbl_surat_jalan_makloon_celup" (
  "IDSJM" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_IDSJM_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_IDSupplier_seq"'::regclass),
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_IDPO_seq"'::regclass),
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_surat_jalan_makloon_celup_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail";
CREATE TABLE "public"."tbl_surat_jalan_makloon_celup_detail" (
  "IDSJMDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq"'::regclass),
  "IDSJM" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDSJM_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(15) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_surat_jalan_makloon_jahit
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit";
CREATE TABLE "public"."tbl_surat_jalan_makloon_jahit" (
  "IDSJH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_IDSJH_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_IDSupplier_seq"'::regclass),
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_surat_jalan_makloon_jahit
-- ----------------------------
INSERT INTO "public"."tbl_surat_jalan_makloon_jahit" VALUES ('00001', NULL, '00001', 'P000004', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_surat_jalan_makloon_jahit_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail";
CREATE TABLE "public"."tbl_surat_jalan_makloon_jahit_detail" (
  "IDSJHDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq"'::regclass),
  "IDSJH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_surat_jalan_makloon_jahit_detail
-- ----------------------------
INSERT INTO "public"."tbl_surat_jalan_makloon_jahit_detail" VALUES ('123456', '00001', '3481248162486184', '3213', 'P000008', 'P000001', 'Prk0001', 121231, 1312, 31312313, 3213123123, '3123', '131231', 'P000004');
INSERT INTO "public"."tbl_surat_jalan_makloon_jahit_detail" VALUES ('123457', '00001', '3481248162486184', '3213', 'P000008', 'P000001', 'Prk0001', 12, 2, NULL, NULL, NULL, NULL, 'P000004');

-- ----------------------------
-- Table structure for tbl_tampungan1
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_tampungan1";
CREATE TABLE "public"."tbl_tampungan1" (
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoPO" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Corak" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Warna" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(9) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(18) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Total_harga" float8,
  "IDT" int8 NOT NULL DEFAULT nextval('"tbl_tampungan_IDT_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier";
CREATE TABLE "public"."tbl_terima_barang_supplier" (
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDTBS_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDSupplier_seq"'::regclass),
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDPO_seq"'::regclass),
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Jenis_TBS" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Batal" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_harga" float8
)
;

-- ----------------------------
-- Records of tbl_terima_barang_supplier
-- ----------------------------
INSERT INTO "public"."tbl_terima_barang_supplier" VALUES ('P000001', '2019-05-07', 'PB19PMKA00001', 'P000004', '801001465', 'P000001', 'O19PMK034', 102, 93.27, 102, 93.27, '', 'PB', 'Aktif', 63500);
INSERT INTO "public"."tbl_terima_barang_supplier" VALUES ('P000002', '2019-05-07', 'PB19PMKA00002', 'P000004', '801001466', 'P000001', 'O19PMK034', 54.5, 49.83, 54.5, 49.83, '', 'PB', 'Aktif', 31750);
INSERT INTO "public"."tbl_terima_barang_supplier" VALUES ('P000003', '2019-05-07', 'PB19PMKA00003', 'P000004', '801001467', 'P00001j', 'O19PMK035', 250, 235, 250, 235, '', 'PB', 'Aktif', 0);

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_asset";
CREATE TABLE "public"."tbl_terima_barang_supplier_asset" (
  "IDTBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default",
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default",
  "IDPOAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Nomor_Supplier" varchar(64) COLLATE "pg_catalog"."default",
  "IDMataUang" float8,
  "Kurs" float8,
  "Total_Qty" float8,
  "Saldo" float8,
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "Batal" varchar(64) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0)
)
;

-- ----------------------------
-- Records of tbl_terima_barang_supplier_asset
-- ----------------------------
INSERT INTO "public"."tbl_terima_barang_supplier_asset" VALUES ('P000001', '2019-06-10', 'PBA1900001', 'P000004', 'POA1900001', NULL, 1, 14000, 2, 2, 'dsdasd', 'aktif', 'P000001', '2019-06-10 20:32:11', NULL, NULL);
INSERT INTO "public"."tbl_terima_barang_supplier_asset" VALUES ('P000002', '2019-06-11', 'PBA1900002', 'P000004', 'POA1900002', NULL, 1, 14000, 2, 2, '-', 'aktif', 'P000001', '2019-06-11 12:16:12', NULL, NULL);

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_asset_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_asset_detail" (
  "IDTBAssetDetail" varchar(64) COLLATE "pg_catalog"."default",
  "IDTBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "IDPOAssetDetail" varchar(64) COLLATE "pg_catalog"."default",
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Qty" float8,
  "Saldo" float8,
  "Harga_Satuan" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Subtotal" float8
)
;

-- ----------------------------
-- Records of tbl_terima_barang_supplier_asset_detail
-- ----------------------------
INSERT INTO "public"."tbl_terima_barang_supplier_asset_detail" VALUES ('P000002', 'P000001', 'P000001', 'P100006', 2, 2, 10000000, 1, 14000, 20000000);
INSERT INTO "public"."tbl_terima_barang_supplier_asset_detail" VALUES ('P000003', 'P000002', 'P000002', 'P100001', 1, 1, 200000000, 1, 14000, 200000000);
INSERT INTO "public"."tbl_terima_barang_supplier_asset_detail" VALUES ('P000004', 'P000002', 'P000003', 'P100006', 1, 1, 10000000, 1, 14000, 10000000);

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_detail" (
  "IDTBSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDTBSDetail_seq"'::regclass),
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDTBS_seq"'::regclass),
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(5) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDSatuan_seq"'::regclass),
  "Harga" float8
)
;

-- ----------------------------
-- Records of tbl_terima_barang_supplier_detail
-- ----------------------------
INSERT INTO "public"."tbl_terima_barang_supplier_detail" VALUES ('P000001', 'P000002', '0419PMK03447411', 'O19PMK034', '474', '', 'P000004', 'P000005', 'P000052', 'Prk0005', 54.5, 49.83, 54.5, 49.83, 'A', 'PESANAN', '154', 'P000004', 31750);

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_jahit
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_jahit";
CREATE TABLE "public"."tbl_terima_barang_supplier_jahit" (
  "IDTBSH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDTBSH_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDSupplier_seq"'::regclass),
  "IDSJH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDSJH_seq"'::regclass),
  "Nomor_supplier" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty" float8,
  "Saldo" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_terima_barang_supplier_jahit
-- ----------------------------
INSERT INTO "public"."tbl_terima_barang_supplier_jahit" VALUES ('P00001', '2019-06-11', NULL, 'P000006', '11', NULL, NULL, NULL, NULL, 'aktif');
INSERT INTO "public"."tbl_terima_barang_supplier_jahit" VALUES ('P000002', '2019-06-12', 'PBMJ19PMKA00001', 'P000004', '00001', '23819389173', 2, 2, '', 'aktif');
INSERT INTO "public"."tbl_terima_barang_supplier_jahit" VALUES ('P000002', '2019-06-12', 'PBMJ19PMKA00001', 'P000004', '00001', '23819389173', 2, 2, 'dsad', 'aktif');
INSERT INTO "public"."tbl_terima_barang_supplier_jahit" VALUES ('P000002', '2019-06-12', 'PBMJ19PMKA00001', 'P000004', '00001', '23819389173', 2, 2, 'dsad', 'aktif');
INSERT INTO "public"."tbl_terima_barang_supplier_jahit" VALUES ('P000002', '2019-06-12', 'PBMJ19PMKA00001', 'P000004', '00001', '23819389173', 2, 2, 'dsad', 'aktif');
INSERT INTO "public"."tbl_terima_barang_supplier_jahit" VALUES ('P000002', '2019-06-12', 'PBMJ19PMKA00001', 'P000004', '00001', '23819389173', 2, 2, 'dsad', 'aktif');

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_jahit_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_jahit_detail" (
  "IDTBSHDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq"'::regclass),
  "IDTBSH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"'::regclass),
  "Qty" float8,
  "Saldo" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "Total_harga" float8
)
;

-- ----------------------------
-- Records of tbl_terima_barang_supplier_jahit_detail
-- ----------------------------
INSERT INTO "public"."tbl_terima_barang_supplier_jahit_detail" VALUES ('p00001', 'p00001', '', '', 31231, 321, '', 100000, 10000000);
INSERT INTO "public"."tbl_terima_barang_supplier_jahit_detail" VALUES ('p00002', 'p00001', '', '', 2, 11, '', 2000000, 20000000);

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_umum";
CREATE TABLE "public"."tbl_terima_barang_supplier_umum" (
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPOUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Total_qty" float8,
  "Saldo" float8,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_harga" float8
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_umum_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_umum_detail" (
  "IDTBSUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "IDPOUmumDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Saldo" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_um_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_um_customer";
CREATE TABLE "public"."tbl_um_customer" (
  "IDUMCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_um_customer_IDUMCustomer_seq"'::regclass),
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_um_customer_IDCustomer_seq"'::regclass),
  "Tanggal_UM" date,
  "Nomor_Faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_UM" numeric(100) DEFAULT NULL::numeric,
  "Saldo_UM" numeric(100) DEFAULT NULL::numeric,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_um_customer_IDFaktur_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_um_customer
-- ----------------------------
INSERT INTO "public"."tbl_um_customer" VALUES ('P000001', 'P100030', '2019-01-16', '01/090', 9688000, 9688000, '35');
INSERT INTO "public"."tbl_um_customer" VALUES ('P000002', 'P100082', '2019-02-28', '02/137', 7983500, 7983500, '36');
INSERT INTO "public"."tbl_um_customer" VALUES ('P000003', 'P100084', '2019-02-28', 'DP/01', 6600000, 6600000, '37');
INSERT INTO "public"."tbl_um_customer" VALUES ('P000004', 'P100083', '2019-02-28', 'DP/02', 31000000, 31000000, '38');

-- ----------------------------
-- Table structure for tbl_um_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_um_supplier";
CREATE TABLE "public"."tbl_um_supplier" (
  "IDUMSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Nomor_Faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nominal" float8,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_UM" date
)
;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_user";
CREATE TABLE "public"."tbl_user" (
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_user_IDUser_seq"'::regclass),
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Username" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Password" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_user_IDGroupUser_seq"'::regclass),
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO "public"."tbl_user" VALUES ('P000001', 'advess', 'advess', 'advess@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 5, 'Aktif');

-- ----------------------------
-- Table structure for tbl_warna
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_warna";
CREATE TABLE "public"."tbl_warna" (
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_warna_IDWarna_seq"'::regclass),
  "Kode_Warna" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Warna" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_warna
-- ----------------------------
INSERT INTO "public"."tbl_warna" VALUES ('P000001', '01/35', '01/35', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000002', '02/04', '02/04', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000003', '03/05', '03/05', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000004', '04/06', '04/06', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000005', '05/26', '05/26', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000006', '06/02', '06/02', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000007', '07/03', '07/03', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000008', '08/21', '08/21', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000009', '09/31', '09/31', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000010', '10/43', '10/43', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000011', '11/41', '11/41', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000012', '12/13', '12/13', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000013', '13/29', '13/29', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000014', '14/27', '14/27', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000015', '15/09', '15/09', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000016', '16/32', '16/32', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000017', '17/24', '17/24', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000018', '18/19', '18/19', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000019', '19/76', '19/76', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000020', '20/86', '20/86', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000021', '21/156', '21/156', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000022', '22/39', '22/39', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000023', '23/152', '23/152', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000024', '24/149', '24/149', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000025', '25/16', '25/16', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000026', '27/858', '27/858', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000027', '28/230', '28/230', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000028', '36/1111', '36/1111', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000029', '38/114', '38/114', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000030', '40/1135', '40/1135', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000031', '41/1123', '41/1123', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000040', '09/107', '09/107', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000041', '10/43', '10/43', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000042', '11/38', '11/38', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000043', '12/110', '12/110', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000044', '13/86', '13/86', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000045', '14/117', '14/117', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000046', '15/118', '15/118', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000047', '16/19', '16/19', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000049', '18/72', '18/72', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000050', '22/204', '22/204', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000051', '23/266', '23/266', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000052', '01/06', '01/06', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000053', '02/08', '02/08', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000054', '03/09', '03/09', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000055', '04/22', '04/22', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000056', '05/03', '05/03', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000057', '06/22', '06/22', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000058', '07/80', '07/80', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000059', '08/25', '08/25', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000060', '09/17', '09/17', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000061', '10/83', '10/83', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000062', '11/99', '11/99', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000063', '12/72', '12/72', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000064', '13/77', '13/77', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000065', '14/18', '14/18', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000066', '15/30', '15/30', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000067', '16/36', '16/36', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000068', '17/93', '17/93', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000069', '18/34', '18/34', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000070', '19/119', '19/119', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000071', '20/131', '20/131', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000072', '21/134', '21/134', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000073', '22/137', '22/137', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000074', '01/36', '01/36', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000075', '02/76', '02/76', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000076', '03/28', '03/28', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000077', '04/29', '04/29', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000078', '05/24', '05/24', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000079', '06/67', '06/67', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000080', '07/38', '07/38', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000081', '08/40', '08/40', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000082', '09/26', '09/26', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000083', '10/41', '10/41', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000084', '11/81', '11/81', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000085', '12/22', '12/22', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000086', '13/11', '13/11', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000087', '14/08', '14/08', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000088', '15/10', '15/10', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000089', '16/02', '16/02', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000090', '17/50', '17/50', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000091', '18/55', '18/55', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000092', '19/116', '19/116', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000093', '22/63', '22/63', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000094', '23/60', '23/60', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000095', '24/130', '24/130', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000096', '25/C', '25/C', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000097', '26/C', '26/C', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000098', '01/681', '01/681', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000099', '02/477', '02/477', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000100', '03/482', '03/482', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000101', '04/42', '04/42', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000102', '05/634', '05/634', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000103', '06/497', '06/497', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000104', '07/188', '07/188', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000105', '08/50', '08/50', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000106', '09/711', '09/711', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000107', '10/676', '10/676', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000108', '11/40', '11/40', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000109', '12/663', '12/663', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000110', '13/666', '13/666', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000111', '14/38', '14/38', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000112', '15/43', '15/43', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000113', '16/689', '16/689', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000114', '17/687', '17/687', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000115', '18/670', '18/670', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000116', '19/483', '19/483', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000117', '20/44', '20/44', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000118', '21/593', '21/593', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000119', '22/153', '22/153', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000120', '23/147', '23/147', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000034', '03/120', '03/120', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P000036', '05/129', '05/129', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P000035', '04/26', '04/26', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P000037', '06/31', '06/31', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P000038', '07/114', '07/114', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P000039', '08/76', '08/76', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P000121', '24/488', '24/488', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000122', '25/181', '25/181', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000123', '26/179', '26/179', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000124', '27/49', '27/49', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000125', '28/46', '28/46', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000126', '29/485', '29/485', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000127', '30/66', '30/66', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000128', '31/57', '31/57', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000129', '32/55', '32/55', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000130', '33/672', '33/672', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000131', '34/68', '34/68', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000132', '35/31', '35/31', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000133', '36/70', '36/70', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000134', '901/1838', '901/1838', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000135', '39/2312', '39/2312', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000136', '41/C', '41/C', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000137', '01/V1', '01/V1', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000138', '02/31', '02/31', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000139', '03/16', '03/16', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000140', '04/37', '04/37', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000141', '05/K1', '05/K1', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000142', '06/Z2', '06/Z2', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000143', '07/01', '07/01', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000144', '08/J2', '08/J2', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000145', '09/G1', '09/G1', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000146', '10/91', '10/91', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000147', '11/29', '11/29', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000148', '12/43', '12/43', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000149', '13/11', '13/11', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000150', '14/15', '14/15', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000151', '15/120', '15/120', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000152', '16/148', '16/148', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000153', '17/I', '17/I', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000154', '18/C1', '18/C1', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000155', '19/D', '19/D', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000156', '20/C', '20/C', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000157', '21/100', '21/100', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000158', '22/105', '22/105', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000159', '23/172', '23/172', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000160', '24/181', '24/181', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000161', '25/190', '25/190', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000162', '26/206', '26/206', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000163', '27/210', '27/210', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000164', '28/213', '28/213', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000165', '29/218', '29/218', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000166', '31/242', '31/242', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000167', '01/71', '01/71', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000168', '02/49', '02/49', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000169', '03/43', '03/43', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000170', '04/77', '04/77', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000171', '05/83', '05/83', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000172', '06/84', '06/84', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000173', '07/64', '07/64', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000174', '08/73', '08/73', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000175', '09/62', '09/62', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000176', '10/81', '10/81', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000177', '11/55', '11/55', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000178', '12/115', '12/115', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000179', '13/66', '13/66', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000180', '14/51', '14/51', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000181', '15/63', '15/63', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000182', '16/67', '16/67', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000183', '17/87', '17/87', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000184', '18/93', '18/93', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000185', '19/60', '19/60', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000186', '20/91', '20/91', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000187', '21/302', '21/302', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000188', '22/305', '22/305', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000189', '23/311', '23/311', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000190', '24/312', '24/312', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000191', '25/512', '25/512', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000192', '01/114', '01/114', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000193', '02/115', '02/115', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000194', '03/116', '03/116', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000195', '04/117', '04/117', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000196', '05/204', '05/204', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000197', '06/58', '06/58', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000198', '07/69', '07/69', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000199', '08/119', '08/119', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000200', '09/120', '09/120', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000201', '10/121', '10/121', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000202', '11/122', '11/122', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000203', '12/123', '12/123', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000204', '13/124', '13/124', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000205', '14/125', '14/125', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000206', '15/87', '15/87', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000207', '16/59', '16/59', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000208', '17/97', '17/97', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000209', '18/99', '18/99', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000210', '19/126', '19/126', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000211', '20/127', '20/127', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000212', '21/102', '21/102', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000213', '22/128', '22/128', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000214', '23/129', '23/129', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000215', '24/53', '24/53', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000216', '25/17', '25/17', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000217', '26/85', '26/85', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000218', '27/31', '27/31', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000219', '28/15', '28/15', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000220', '29/130', '29/130', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000221', '30/01', '30/01', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000222', '31/132', '31/132', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000223', '32/148', '32/148', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000224', '33/157', '33/157', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000225', '34/138', '34/138', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000226', '35/142', '35/142', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000227', '36/192', '36/192', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000228', '37/196', '37/196', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000229', '38/205', '38/205', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000230', '39/211', '39/211', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000231', '40/326', '40/326', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000232', '41/340', '41/340', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000233', '42/342', '42/342', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000234', '43/348', '43/348', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000235', '47/457', '47/457', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000236', '48/460', '48/460', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000237', '50/393', '50/393', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000238', '51/396', '51/396', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000239', '52/408', '52/408', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000240', '53/428', '53/428', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000241', '54/453', '54/453', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000242', '01/331', '01/331', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000243', '02/387', '02/387', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000244', '03/324', '03/324', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000245', '04/437', '04/437', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000246', '05/422', '05/422', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000247', '06/466', '06/466', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000248', '07/446', '07/446', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000249', '08/440', '08/440', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000250', '09/418', '09/418', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000251', '10/405', '10/405', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000252', '11/391', '11/391', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000253', '12/475', '12/475', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000254', '13/641', '13/641', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000255', '14/400', '14/400', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000256', '15/432', '15/432', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000257', '16/358', '16/358', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000258', '17/409', '17/409', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000259', '18/354', '18/354', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000260', '19/503', '19/503', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000261', '20/318', '20/318', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000262', '21/461', '21/461', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000263', '22/436', '22/436', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000264', '23/384', '23/384', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000265', '29/677', '29/677', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000266', '30/659', '30/659', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000267', '31/811', '31/811', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000268', '32/734', '32/734', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000269', '33/768', '33/768', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000270', '35/832', '35/832', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000271', '36/844', '36/844', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000272', '37/836', '37/836', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000273', '38/862', '38/862', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000274', '39/863', '39/863', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000275', '40/126', '40/126', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000276', '01/147', '01/147', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000277', '02/166', '02/166', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000278', '03/265', '03/265', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000279', '04/271', '04/271', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000280', '05/171', '05/171', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000281', '06/174', '06/174', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000282', '07/191', '07/191', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000283', '08/175', '08/175', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000284', '09/295', '09/295', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000285', '10/290', '10/290', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000286', '11/189', '11/189', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000287', '12/194', '12/194', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000288', '13/46', '13/46', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000289', '14/39', '14/39', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000290', '15/163', '15/163', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000291', '16/168', '16/168', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000292', '17/123', '17/123', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000293', '18/142', '18/142', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000294', '19/133', '19/133', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000295', '20/135', '20/135', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000296', '21/289', '21/289', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000297', '22/284', '22/284', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000298', '23/179', '23/179', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000299', '24/178', '24/178', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000300', '25/279', '25/279', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000301', '26/181', '26/181', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000302', '27/273', '27/273', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000303', '28/275', '28/275', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000304', '29/298', '29/298', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000305', '30/359', '30/359', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000306', '31/413', '31/413', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000307', '32/414', '32/414', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000308', '33/415', '33/415', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000309', '01/97', '01/97', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000310', '02/67', '02/67', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000311', '03/52', '03/52', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000312', '04/41', '04/41', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000313', '05/66', '05/66', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000314', '06/31', '06/31', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000315', '07/34', '07/34', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000316', '08/70', '08/70', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000317', '09/101', '09/101', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000318', '10/42', '10/42', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000319', '11/78', '11/78', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000320', '12/68', '12/68', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000321', '13/51', '13/51', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000322', '14/54', '14/54', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000323', '15/66', '15/66', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000324', '16/80', '16/80', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000325', '17/61', '17/61', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000326', '18/63', '18/63', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000327', '19/150', '19/150', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000328', '20/153', '20/153', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000329', '21/157', '21/157', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000330', '22/152', '22/152', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000331', '23/162', '23/162', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000332', '24/127', '24/127', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000333', '25/171', '25/171', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000334', '26/282', '26/282', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000335', '28/270', '28/270', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000336', '30/556', '30/556', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000337', '36/694', '36/694', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000338', '37/605', '37/605', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000339', '38/562', '38/562', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000340', '45/31', '45/31', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000341', '18/1051', '18/1051', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000342', '50/1067', '50/1067', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000343', '01/110', '01/110', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000344', '02/102', '02/102', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000345', '03/70', '03/70', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000346', '04/72', '04/72', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000347', '05/189', '05/189', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000348', '06/186', '06/186', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000349', '07/193', '07/193', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000350', '08/82', '08/82', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000351', '09/47', '09/47', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000352', '10/48', '10/48', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000353', '11/04', '11/04', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000354', '12/40', '12/40', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000355', '13/09', '13/09', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000356', '14/80', '14/80', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000357', '15/183', '15/183', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000358', '16/18', '16/18', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000359', '17/147', '17/147', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000360', '18/26', '18/26', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000361', '19/91', '19/91', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000362', '20/87', '20/87', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000363', '21/169', '21/169', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000364', '22/291', '22/291', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000365', '23/212', '23/212', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000366', '24/305', '24/305', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000367', '25/385', '25/385', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000368', '41/631', '41/631', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000369', '50/680', '50/680', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000370', '51/683', '51/683', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000371', '40/393', '40/393', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000372', '31/503', '31/503', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000373', '47/50', '47/50', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000374', '31/55', '31/55', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000375', '42/105', '42/105', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000376', '43/107', '43/107', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000377', '44/112', '44/112', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000378', '45/149', '45/149', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000379', '38/92', '38/92', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000380', '32/449', '32/449', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000381', '52/689', '52/689', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000382', '01/24', '01/24', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000383', '02/18', '02/18', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000384', '03/19', '03/19', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000385', '04/30', '04/30', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000386', '05/31', '05/31', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000387', '06/39', '06/39', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000388', '07/40', '07/40', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000389', '08/36', '08/36', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000390', '09/28', '09/28', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000391', '10/25', '10/25', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000392', '11/21', '11/21', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000393', '12/41', '12/41', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000394', '13/38', '13/38', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000395', '14/33', '14/33', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000396', '15/50', '15/50', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000397', '16/150', '16/150', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000398', '18/67', '18/67', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000399', '17/71', '17/71', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000400', '19/78', '19/78', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000401', '25/138', '25/138', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000402', '22/139', '22/139', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000403', '01/78', '01/78', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000404', '02/85', '02/85', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000405', '03/30', '03/30', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000406', '04/95', '04/95', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000407', '05/02', '05/02', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000408', '06/75', '06/75', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000409', '07/21', '07/21', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000410', '08/145', '08/145', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000411', '09/32', '09/32', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000412', '10/41', '10/41', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000413', '11/08', '11/08', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000414', '12/11', '12/11', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000415', '13/66', '13/66', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000416', '14/148', '14/148', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000417', '15/136', '15/136', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000418', '16/134', '16/134', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000419', '17/150', '17/150', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000420', '18/119', '18/119', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000421', '19/143', '19/143', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000422', '20/128', '20/128', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000423', '21/154', '21/154', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000424', '22/295', '22/295', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000425', '23/293', '23/293', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000426', '24/304', '24/304', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000427', '200/150A', '200/150A', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000428', '230', '230', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000429', '233', '233', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000430', '670', '670', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000431', '01/13', '01/13', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000432', '02/17', '02/17', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000433', '03/02', '03/02', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000434', '04/07', '04/07', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000435', '05/08', '05/08', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000436', '06/24', '06/24', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000437', '07/29', '07/29', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000438', '08/28', '08/28', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000439', '09/03', '09/03', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000440', '10/237', '10/237', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000441', '11/40', '11/40', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000442', '13494', '13494', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000443', '13/23', '13/23', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000444', '14/21/268', '14/21/268', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000445', '15/22', '15/22', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000446', '16/32', '16/32', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000447', '17/27', '17/27', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000448', '18/199', '18/199', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000449', '19/225', '19/225', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000450', '20/191', '20/191', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000451', '21/65/152', '21/65/152', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000452', '22/39', '22/39', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000453', '23/11', '23/11', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000454', '24/31', '24/31', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000455', '25/231', '25/231', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000456', '26/229', '26/229', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000457', '27/267', '27/267', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000458', '28/18', '28/18', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000459', '29/33', '29/33', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000460', '30/34', '30/34', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000461', '31/05', '31/05', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000462', '32/06', '32/06', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000463', '33/26', '33/26', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000464', '34/09', '34/09', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000465', '35/286', '35/286', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000466', '36/328', '36/328', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000467', '37/327', '37/327', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000468', '38/336', '38/336', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000469', '39/332', '39/332', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000470', '40/337', '40/337', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000471', '41/15/343', '41/15/343', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000472', '42/349', '42/349', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000473', '43/376', '43/376', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000474', '45/C', '45/C', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000475', '46/B', '46/B', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000476', 'VSR 01', 'VSR 01', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000477', 'VSR 06', 'VSR 06', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000478', 'VSR 30', 'VSR 30', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000479', 'VSR 78', 'VSR 78', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000480', '01/04', '01/04', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000481', '02/20', '02/20', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000482', '03/30', '03/30', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000483', '04/24', '04/24', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000484', '05/16', '05/16', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000485', '06/26', '06/26', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000486', '07/C', '07/C', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000487', '08/09', '08/09', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000488', '09/10', '09/10', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000489', '10/13', '10/13', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000490', '11/18', '11/18', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000491', '12/43', '12/43', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000492', '13/A', '13/A', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000493', '14/92', '14/92', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000494', '01 JB', '01 JB', 'aktif', 'P000020');
INSERT INTO "public"."tbl_warna" VALUES ('P000495', '01 M.JB', '01 M.JB', 'aktif', 'P000021');
INSERT INTO "public"."tbl_warna" VALUES ('P000496', '01/96', '01/96', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000497', '02/26', '02/26', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000498', '03/80', '03/80', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000499', '04/31', '04/31', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000500', '05/79', '05/79', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000501', '06/114', '06/114', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000502', '07/76', '07/76', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000503', '08/34', '08/34', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000504', '09/58', '09/58', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000505', '10/90', '10/90', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000506', '11/88', '11/88', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000507', '12/119', '12/119', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000508', '13/107', '13/107', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000509', '14/110', '14/110', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000510', '15/38', '15/38', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000511', '16/43', '16/43', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000512', '17/86', '17/86', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000513', '18/19', '18/19', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000514', '19/124', '19/124', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000515', '20/72', '20/72', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000516', '21/257', '21/257', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000517', '22/A', '22/A', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000518', '24/277', '24/277', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000519', '48/33', '48/33', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000520', '01/826', '01/826', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000521', '02/782', '02/782', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000522', '03/829', '03/829', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000523', '04/864', '04/864', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000524', '05/867', '05/867', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000525', '06/832', '06/832', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000526', '07/763', '07/763', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000527', '08/846', '08/846', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000528', '09/830', '09/830', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000529', '10/837', '10/837', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000530', '11/834', '11/834', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000531', '12/821', '12/821', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000532', '13/876', '13/876', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000533', '14/872', '14/872', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000534', '15/900', '15/900', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000535', '16/848', '16/848', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000536', '17/758', '17/758', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000537', '18/879', '18/879', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000538', '19/839', '19/839', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000539', '20/851', '20/851', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000540', '21/775', '21/775', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000541', '22/894', '22/894', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000542', '23/777', '23/777', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000543', '24/844', '24/844', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000544', '25/790', '25/790', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000545', '26/1012', '26/1012', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000546', '27/1038', '27/1038', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000547', '32/1270', '32/1270', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000548', '1030', '1030', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000549', '1063', '1063', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000550', '01/001 Black', '01/001 Black', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000551', '02/B Navy', '02/B Navy', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000552', '03/C Brown', '03/C Brown', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000553', '04/H D.Brown', '04/H D.Brown', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000554', '05/01 D.Charcoal', '05/01 D.Charcoal', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000555', '06/P L.Brown', '06/P L.Brown', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000556', '07/S Cream', '07/S Cream', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000557', '08/A Khaky', '08/A Khaky', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P00055e', '864', 'Charcoal', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055g', '37/836', 'Orange Red', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055h', '12/22', 'Grey', 'aktif', 'P000008');
INSERT INTO "public"."tbl_warna" VALUES ('P00055i', '01', 'White', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P00055j', '17/124', '-', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P000048', '17/124', '17/124', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P00055k', '01/112', '01/112', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P00055l', '02/122', '02/122', 'aktif', 'P000006');
INSERT INTO "public"."tbl_warna" VALUES ('P00055m', '55/466', '55/466', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P00055n', '38/862', 'Maroon', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055o', '19', 'Light Grey', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055p', '13', 'White', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055q', '39', 'Green', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055r', '03', 'Dark Brown', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055s', '10', 'Deep Khaki', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055f', '35/832', 'Gray', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055t', '15', 'Midnight Blue', 'aktif', 'P000012');
INSERT INTO "public"."tbl_warna" VALUES ('P00055u', 'Jet Black', 'Jet Black', 'aktif', 'P000018');

-- ----------------------------
-- Function structure for ap
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ap"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_pembayaran_hutang."Total"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier"  AND date_part('month',tbl_pembayaran_hutang."Tanggal") = '1' GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_pembelian ON tbl_pembelian."Nomor"=tbl_jurnal."Nomor" right join tbl_suplier on tbl_suplier."IDSupplier"=tbl_pembelian."IDSupplier" AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ap2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ap2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m0."Tanggal", m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV'  AND date_part('month',tbl_pembelian."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB'  AND date_part('month',tbl_pembelian."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_pembayaran_hutang."Total"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier"  AND date_part('month',tbl_pembayaran_hutang."Tanggal") = '1' GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_jurnal."Tanggal", tbl_suplier."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_pembelian ON tbl_pembelian."Nomor"=tbl_jurnal."Nomor" right join tbl_suplier on tbl_suplier."IDSupplier"=tbl_pembelian."IDSupplier" AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_jurnal."Tanggal", tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ap_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8);
CREATE OR REPLACE FUNCTION "public"."ap_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV' AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m1
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_retur_pembelian right join tbl_suplier on tbl_retur_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_retur_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB' AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembayaran_hutang."Nomor" AND tbl_jurnal."Jenis_faktur"='PH' AND date_part('month',tbl_pembayaran_hutang."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_pembelian ON tbl_pembelian."Nomor"=tbl_jurnal."Nomor" right join tbl_suplier on tbl_suplier."IDSupplier"=tbl_pembelian."IDSupplier" AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan-1 GROUP BY tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ap_cari2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap_cari2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8);
CREATE OR REPLACE FUNCTION "public"."ap_cari2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m0."Tanggal", m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV'  AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_retur_pembelian right join tbl_suplier on tbl_retur_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_retur_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB'  AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembayaran_hutang."Nomor" AND tbl_jurnal."Jenis_faktur"='PH' AND date_part('month',tbl_pembayaran_hutang."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_jurnal."Tanggal", tbl_suplier."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_pembelian ON tbl_pembelian."Nomor"=tbl_jurnal."Nomor" right join tbl_suplier on tbl_suplier."IDSupplier"=tbl_pembelian."IDSupplier" AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan-1 GROUP BY tbl_jurnal."Tanggal", tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ar"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_penerimaan_piutang."Total"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer"  AND date_part('month',tbl_penerimaan_piutang."Tanggal") = '1' GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_penjualan_kain ON tbl_penjualan_kain."Nomor"=tbl_jurnal."Nomor" right join tbl_customer on tbl_customer."IDCustomer"=tbl_penjualan_kain."IDCustomer" AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama") m0 USING("Nama")
								
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ar2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m0."Tanggal", m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_penerimaan_piutang."Total"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer"  AND date_part('month',tbl_penerimaan_piutang."Tanggal") = '1' GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_jurnal."Tanggal", tbl_customer."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_penjualan_kain ON tbl_penjualan_kain."Nomor"=tbl_jurnal."Nomor" right join tbl_customer on tbl_customer."IDCustomer"=tbl_penjualan_kain."IDCustomer" AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_jurnal."Tanggal", tbl_customer."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."ar_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP' AND date_part('month',tbl_penjualan_kain."Tanggal") = caribulan GROUP BY tbl_customer."Nama") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ' AND date_part('month',tbl_penjualan_kain."Tanggal") = caribulan GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penerimaan_piutang."Nomor" AND tbl_jurnal."Jenis_faktur"='PP' AND date_part('month',tbl_penerimaan_piutang."Tanggal") = caribulan GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_penjualan_kain ON tbl_penjualan_kain."Nomor"=tbl_jurnal."Nomor" right join tbl_customer on tbl_customer."IDCustomer"=tbl_penjualan_kain."IDCustomer" AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_customer."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar_cari2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar_cari2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."ar_cari2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m0."Tanggal", m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP' AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ' AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penerimaan_piutang."Nomor" AND tbl_jurnal."Jenis_faktur"='PP' AND date_part('month',tbl_penerimaan_piutang."Tanggal") = caribulan GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_jurnal."Tanggal", tbl_customer."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_penjualan_kain ON tbl_penjualan_kain."Nomor"=tbl_jurnal."Nomor" right join tbl_customer on tbl_customer."IDCustomer"=tbl_penjualan_kain."IDCustomer" AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_jurnal."Tanggal", tbl_customer."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for average_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."average_kain"();
CREATE OR REPLACE FUNCTION "public"."average_kain"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
declare harga_po integer;
declare idpo varchar;
declare idbarang varchar;
declare idcoark varchar;
declare harga_grade integer;
declare harga_stok integer;
declare saldo_stok float;
declare harga_hpp float;
declare hpp float;
begin

Select idpo = OLD.IDPO From tbl_terima_barang_suppllier where idtbs = NEW.idtbs;

select idbarang = OLD.IDBarang, idcorak = OLD.IDCorak from tbl_purchase_order where IDPO = idpo;

select harga_po = OLD.Harga from tbl_purchase_order_detail where IDPO = idpo and IDWarna = NEW.IDWarna;

IF (NEW.Grade <> Ä) THEN
	harga_grade =(harga_po * 15) /100;
	harga_po = harga_po - harga_grade;
END IF;

select harga_stok = SUM(Saldo_yard * harga) from tbl_stok where IDBarang = idbarang and IDCorak = idcorak and idwarna = NEW.IDWarna and Grade = NEW.Grade;

harga_hpp = harga_po + harga_stok;

select saldo_stok = sum(saldo_yard) from tbl_stok where IDBarang = idbarang and IDCorak = idcorak and idwarna = NEW.IDWarna and Grade = NEW.Grade;

hpp = harga_hpp / saldo_stok;

UPDATE tbl_stok set "Harga" = hpp where IDBarang = idbarang and IDCorak = idcorak and idwarna = NEW.IDWarna and Grade = NEW.Grade; 

return new;
end $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for bs
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar);
CREATE OR REPLACE FUNCTION "public"."bs"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_bulan_lalu, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_lalu, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_lalu, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan" ORDER BY tbl_setting_lr."IDSettingLR" ASC) m1
LEFT JOIN (
SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for bs_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8);
CREATE OR REPLACE FUNCTION "public"."bs_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_bulan_lalu, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_lalu, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_lalu, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = caribulan-1 AND date_part('year',tbl_jurnal."Tanggal") = caritahun
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan" ORDER BY tbl_setting_lr."IDSettingLR" ASC) m1
LEFT JOIN (
SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan AND date_part('year',tbl_jurnal."Tanggal") = caritahun
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC
)m2	USING ("IDSettingLR")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for bs_cari2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs_cari2"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "debet_bulan_ini" float8, OUT "kredit_bulan_ini" float8, OUT "debet_bulan_lalu" float8, OUT "kredit_bulan_lalu" float8, OUT "perhitungan" varchar, OUT "normal_balance" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."bs_cari2"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "debet_bulan_ini" float8, OUT "kredit_bulan_ini" float8, OUT "debet_bulan_lalu" float8, OUT "kredit_bulan_lalu" float8, OUT "perhitungan" varchar, OUT "normal_balance" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Debet_bulan_ini, Kredit_bulan_ini, Debet_bulan_lalu, Kredit_bulan_lalu, Perhitungan, Normal_balance IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Debet_bulan_lalu, m1.Kredit_bulan_lalu, m2.Debet_bulan_ini, m2.Kredit_bulan_ini, m1."Perhitungan", m2."Normal_Balance"
FROM (SELECT tbl_setting_lr."IDSettingLR", tbl_group_coa."Normal_Balance", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet") AS Debet_bulan_lalu, SUM(tbl_jurnal."Kredit") AS Kredit_bulan_lalu, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" LEFT JOIN tbl_coa ON tbl_coa."IDCoa"=tbl_jurnal."IDCOA" LEFT JOIN tbl_group_coa ON tbl_group_coa."IDGroupCOA" = tbl_coa."IDGroupCOA"
AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1
GROUP BY tbl_setting_lr."IDSettingLR", tbl_group_coa."Normal_Balance" , tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC) m1
LEFT JOIN (
-- SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
-- RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" 
-- AND date_part('month',tbl_jurnal."Tanggal") = caribulan
-- GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC

SELECT tbl_setting_lr."IDSettingLR", tbl_group_coa."Normal_Balance", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet") AS Debet_bulan_ini, SUM(tbl_jurnal."Kredit") AS Kredit_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" LEFT JOIN tbl_coa ON tbl_coa."IDCoa"=tbl_jurnal."IDCOA" LEFT JOIN tbl_group_coa ON tbl_group_coa."IDGroupCOA" = tbl_coa."IDGroupCOA"
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_lr."IDSettingLR", tbl_group_coa."Normal_Balance" , tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC
)m2	USING ("IDSettingLR")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCOA, Nama_COA,Tanggal, Nomor, Saldo_awal, Debet, Kredit, Saldo_akhir IN
       
SELECT m1."IDCOA", m1."Nama_COA", m1."Tanggal", m1."Nomor", m2."saldo_awal", m1."Debet", m1."Kredit", m2."saldo_awal"+m1."Debet"-m1."Kredit" AS saldo_akhir  FROM (SELECT tbl_jurnal."IDCOA", tbl_coa."Nama_COA",tbl_jurnal."Tanggal", tbl_jurnal."Nomor", tbl_jurnal."Debet", tbl_jurnal."Kredit" FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" WHERE tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan) m1
LEFT JOIN
(SELECT tbl_jurnal."IDCOA", tbl_jurnal."Tanggal", tbl_jurnal."Nomor", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"),0) AS Saldo_awal FROM tbl_jurnal JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_jurnal."IDCOA", tbl_jurnal."Tanggal", tbl_jurnal."Nomor") m2 USING("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar_cari"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar_cari"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCOA, Nama_COA, Tanggal, Nomor, Saldo_awal, Debet, Kredit, Saldo_akhir IN
       
SELECT m1."Nomor",m1."Nomor", m1."Tanggal", m1."Nomor", m2."saldo_awal", m1."debet", m1."kredit", m1."debet"-m1."kredit" AS saldo_akhir  FROM (SELECT tbl_jurnal."Tanggal", tbl_jurnal."Nomor", coalesce(SUM(tbl_jurnal."Debet"),0) AS debet, coalesce(SUM(tbl_jurnal."Kredit"),0) AS kredit FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" WHERE tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_jurnal."Nomor", tbl_jurnal."Tanggal" ORDER BY tbl_jurnal."Nomor" DESC) m1
LEFT JOIN
(SELECT tbl_jurnal."Tanggal", tbl_jurnal."Nomor", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"),0) AS Saldo_awal FROM tbl_jurnal JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_jurnal."Nomor", tbl_jurnal."Tanggal" ORDER BY tbl_jurnal."Nomor" DESC) m2 USING("Nomor")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar_pembantu_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar_pembantu_cari"(OUT "idcoa" varchar, OUT "kode_coa" varchar, OUT "nama_coa" varchar, OUT "saldo_awal" float8, OUT "debit" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar_pembantu_cari"(OUT "idcoa" varchar, OUT "kode_coa" varchar, OUT "nama_coa" varchar, OUT "saldo_awal" float8, OUT "debit" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCoa, Kode_COA, Nama_COA, Saldo_awal, Debit, Kredit, Saldo_akhir IN
       
SELECT m1."IDCoa", m1."Kode_COA", m1."Nama_COA", m2."saldo_awal", m1."debit", m1."kredit", m2."saldo_awal"+m1."debit"-m1."kredit" AS saldo_akhir  FROM (SELECT tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Debit, coalesce(SUM(tbl_jurnal."Kredit"), 0) AS Kredit FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA") m1
LEFT JOIN
(select tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"), 0) AS Saldo_awal FROM tbl_jurnal RIGHT JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA") m2 USING("IDCoa")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_ip
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_ip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_ip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal, Barcode, NoSO, Party, Nama_Barang, Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party", g."Nama_Barang" AS "Nama_Barang", c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar" 
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
							INNER JOIN tbl_barang AS "g" 
							ON c."IDBarang" = g."IDBarang"
								
								WHERE a."Tanggal" >= datein
								AND a."Tanggal" <= dateuntil
								AND a."Nomor" like keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_ip_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_ip_like"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_ip_like"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal , Barcode, NoSO, Party, Nama_Barang, Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party", g."Nama_Barang" AS "Nama_Barang", c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar" 
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
							INNER JOIN tbl_barang AS "g" 
							ON c."IDBarang" = g."IDBarang"
								
								WHERE a."Nomor" like keywords 					
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_bo
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	
	WHERE BO."Tanggal" >= datein
								AND BO."Tanggal" <= dateuntil
								AND BO."Nomor" like keywords 					
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_bo_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_bo_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_bo_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	
	WHERE BO."Nomor" like keywords 				
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_giro
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_giro"(OUT "idmg" varchar, OUT "nomor" varchar, IN "tanggal_mulai" date, IN "tanggal_selesai" date, IN "jenis_giro" varchar, IN "status_giro" varchar, IN "keyword" varchar, IN "detail_keyword" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_giro"(OUT "idmg" varchar, OUT "nomor" varchar, IN "tanggal_mulai" date, IN "tanggal_selesai" date, IN "jenis_giro" varchar, IN "status_giro" varchar, IN "keyword" varchar, IN "detail_keyword" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDMG, Nomor IN
        SELECT tbl_mutasi_giro."IDMG", tbl_mutasi_giro."Nomor" FROM tbl_mutasi_giro 
				JOIN tbl_mutasi_giro_detail on tbl_mutasi_giro."IDMG"=tbl_mutasi_giro_detail."IDMG" 
				JOIN tbl_giro on tbl_mutasi_giro_detail."IDGiro"=tbl_giro."IDGiro"
				WHERE tbl_mutasi_giro."Tanggal" >= tanggal_mulai
				AND tbl_mutasi_giro."Tanggal" <= tanggal_selesai
				AND tbl_mutasi_giro_detail."Jenis_giro" = jenis_giro
				AND tbl_giro."Status_Giro" = status_giro 
 				AND keyword = detail_keyword
				
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_giro_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_giro_like"(OUT "idmg" varchar, OUT "nomor" varchar, IN "tanggal_mulai" date, IN "tanggal_selesai" date, IN "jenis_giro" varchar, IN "status_giro" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_giro_like"(OUT "idmg" varchar, OUT "nomor" varchar, IN "tanggal_mulai" date, IN "tanggal_selesai" date, IN "jenis_giro" varchar, IN "status_giro" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDMG, Nomor IN
        SELECT tbl_mutasi_giro."IDMG", tbl_mutasi_giro."Nomor" FROM tbl_mutasi_giro 
				JOIN tbl_mutasi_giro_detail on tbl_mutasi_giro."IDMG"=tbl_mutasi_giro_detail."IDMG" 
				JOIN tbl_giro on tbl_mutasi_giro_detail."IDGiro"=tbl_giro."IDGiro"
				WHERE tbl_mutasi_giro."Tanggal" >= tanggal_mulai
				AND tbl_mutasi_giro."Tanggal" <= tanggal_selesai
				AND tbl_mutasi_giro_detail."Jenis_giro" = jenis_giro
				AND tbl_giro."Status_Giro" = status_giro 
-- 				AND keyword LIKE detail_keyword
				
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, IN "datein" date, IN "dateuntil" date, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, IN "datein" date, IN "dateuntil" date, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total"
FROM
	tbl_pembelian
  JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
	JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Tanggal" >= datein
								AND tbl_pembelian."Tanggal" <= dateuntil
								AND tbl_pembelian."Nomor" like keywords 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_pembelian_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total"
FROM
	tbl_pembelian
  JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
	JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Nomor" like keywords 				 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor, SJCK."Nomor" As "Nomor2", CUS."Nama" As "Nama", PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PK."Tanggal" >= datein
								AND PK."Tanggal" <= dateuntil
								AND PK."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_kain_like"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_kain_like"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor, SJCK."Nomor" As "Nomor2", CUS."Nama" As "Nama", PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PK."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjcs, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, SJCS."Nomor" As "Nomor2", CUS."Nama" As "Nama", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PSD."Qty_roll" as "Qty_roll", PSD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Tanggal" >= datein
								AND PS."Tanggal" <= dateuntil
								AND PS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_seragam_like"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_seragam_like"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjcs, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, SJCS."Nomor" As "Nomor2", CUS."Nama" As "Nama", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PSD."Qty_roll" as "Qty_roll", PSD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_invoice_pembelian_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_invoice_pembelian_umum"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_invoice_pembelian_umum"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						FBU."IDFBUmum" as "IDFBUmum", FBU."Tanggal" As Tanggal, FBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", FBUD."Qty" AS "Qty", FBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", FBUD."Harga_satuan" AS "Harga_satuan", FBUD."Sub_total" AS "Sub_total"
							FROM tbl_pembelian_umum AS FBU
							INNER JOIN tbl_pembelian_umum_detail AS FBUD
								ON FBU."IDFBUmum" = FBUD."IDFBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON FBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON FBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON FBUD."IDBarang" = BAR."IDBarang"
							WHERE FBU."Tanggal" >= datein
								AND FBU."Tanggal" <= dateuntil
								AND FBU."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_invoice_pembelian_umum_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_invoice_pembelian_umum_like"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_invoice_pembelian_umum_like"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						FBU."IDFBUmum" as "IDFBUmum", FBU."Tanggal" As Tanggal, FBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", FBUD."Qty" AS "Qty", FBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", FBUD."Harga_satuan" AS "Harga_satuan", FBUD."Sub_total" AS "Sub_total"
							FROM tbl_pembelian_umum AS FBU
							INNER JOIN tbl_pembelian_umum_detail AS FBUD
								ON FBU."IDFBUmum" = FBUD."IDFBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON FBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON FBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON FBUD."IDBarang" = BAR."IDBarang"
							WHERE FBU."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_packing_list
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
							WHERE PL."Tanggal" >= datein
								AND PL."Tanggal" <= dateuntil
								AND PL."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_packing_list_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_packing_list_like"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_packing_list_like"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
							WHERE PL."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_pembelian_asset
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	
	WHERE T1."Tanggal"::DATE >= datein
								AND T1."Tanggal"::DATE <= dateuntil
								AND T1."Nomor" like keywords 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_pembelian_asset_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_pembelian_asset_like"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_pembelian_asset_like"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	
	WHERE T1."Nomor" like keywords 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_barang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar, IN "jenis_tbs" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar, IN "jenis_tbs" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga
FROM
	tbl_terima_barang_supplier AS T1
	JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Tanggal" >= datein
								AND T1."Tanggal" <= dateuntil
								AND T1."Nomor" like keywords 
								AND T1."Jenis_TBS" = jenis_tbs
ORDER BY T1."IDTBS" DESC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_barang_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_barang_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar, IN "jenis_tbs" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_barang_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar, IN "jenis_tbs" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga
FROM
	tbl_terima_barang_supplier AS T1
	JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Nomor" like keywords AND T1."Jenis_TBS"=jenis_tbs
ORDER BY T1."IDTBS" DESC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_jahit
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_jahit"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_jahit"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSH, Tanggal, Nomor, NamaSup, Nomor_supplier, Nama_Barang, Merk, Saldo, Satuan, Harga, Total_harga IN
        SELECT 
						MJ."IDTBSH" as "IDTBSH", MJ."Tanggal" As Tanggal, MJ."Nomor" As Nomor, SUP."Nama" As NamaSup, MJ."Nomor_supplier" As "Nomor_supplier", BAR."Nama_Barang" AS "Nama_Barang", MER."Merk" AS "Merk", MJD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", MJD."Harga" AS "Harga", MJD."Total_harga" AS "Total_harga"
							FROM tbl_terima_barang_supplier_jahit AS MJ
							INNER JOIN tbl_terima_barang_supplier_jahit_detail AS MJD
								ON MJ."IDTBSH" = MJD."IDTBSH"
							INNER JOIN tbl_suplier AS SUP
								ON MJ."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON MJD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJD."IDBarang" = BAR."IDBarang"
								INNER JOIN tbl_merk AS MER
								ON MJD."IDMerk" = MER."IDMerk"
							WHERE MJ."Tanggal" >= datein
								AND MJ."Tanggal" <= dateuntil
								AND MJ."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_jahit_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_jahit_like"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_jahit_like"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSH, Tanggal, Nomor, NamaSup, Nomor_supplier, Nama_Barang, Merk, Saldo, Satuan, Harga, Total_harga IN
        SELECT 
						MJ."IDTBSH" as "IDTBSH", MJ."Tanggal" As Tanggal, MJ."Nomor" As Nomor, SUP."Nama" As NamaSup, MJ."Nomor_supplier" As "Nomor_supplier", BAR."Nama_Barang" AS "Nama_Barang", MER."Merk" AS "Merk", MJD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", MJD."Harga" AS "Harga", MJD."Total_harga" AS "Total_harga"
							FROM tbl_terima_barang_supplier_jahit AS MJ
							INNER JOIN tbl_terima_barang_supplier_jahit_detail AS MJD
								ON MJ."IDTBSH" = MJD."IDTBSH"
							INNER JOIN tbl_suplier AS SUP
								ON MJ."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON MJD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJD."IDBarang" = BAR."IDBarang"
								INNER JOIN tbl_merk AS MER
								ON MJD."IDMerk" = MER."IDMerk"
							WHERE MJ."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_umum"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_umum"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						TBS."IDTBSUmum" as "IDTBSUmum", TBS."Tanggal" As Tanggal, TBS."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", TBSD."Qty" AS "Qty", TBSD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", TBSD."Harga_satuan" AS "Harga_satuan", TBSD."Sub_total" AS "Sub_total"
							FROM tbl_terima_barang_supplier_umum AS TBS
							INNER JOIN tbl_terima_barang_supplier_umum_detail AS TBSD
								ON TBS."IDTBSUmum" = TBSD."IDTBSUmum"
							INNER JOIN tbl_suplier AS SUP
								ON TBS."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON TBSD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON TBSD."IDBarang" = BAR."IDBarang"
							WHERE TBS."Tanggal" >= datein
								AND TBS."Tanggal" <= dateuntil
								AND TBS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_umum_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_umum_like"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_umum_like"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						TBS."IDTBSUmum" as "IDTBSUmum", TBS."Tanggal" As Tanggal, TBS."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", TBSD."Qty" AS "Qty", TBSD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", TBSD."Harga_satuan" AS "Harga_satuan", TBSD."Sub_total" AS "Sub_total"
							FROM tbl_terima_barang_supplier_umum AS TBS
							INNER JOIN tbl_terima_barang_supplier_umum_detail AS TBSD
								ON TBS."IDTBSUmum" = TBSD."IDTBSUmum"
							INNER JOIN tbl_suplier AS SUP
								ON TBS."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON TBSD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON TBSD."IDBarang" = BAR."IDBarang"
							WHERE 
								TBS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	
	WHERE tbl_retur_pembelian."Tanggal" >= datein
				AND tbl_retur_pembelian."Tanggal" <= dateuntil
				AND tbl_retur_pembelian."Nomor" like keywords 	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	
	WHERE tbl_retur_pembelian."Nomor" like keywords 		
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian_umum"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian_umum"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						RBU."IDRBUmum" as "IDRBUmum", RBU."Tanggal" As Tanggal, RBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", RBUD."Qty" AS "Qty", RBUD."Saldo" AS "Saldo", RBUD."IDSatuan" AS "Satuan", RBUD."Harga_satuan" AS "Harga_satuan", RBUD."Sub_total" AS "Sub_total"
							FROM tbl_retur_pembelian_umum AS RBU
							INNER JOIN tbl_retur_pembelian_umum_detail AS RBUD
								ON RBU."IDRBUmum" = RBUD."IDRBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON RBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON RBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON RBUD."IDBarang" = BAR."IDBarang"
							WHERE RBU."Tanggal" >= datein
								AND RBU."Tanggal" <= dateuntil
								AND RBU."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian_umum_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian_umum_like"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian_umum_like"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						RBU."IDRBUmum" as "IDRBUmum", RBU."Tanggal" As Tanggal, RBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", RBUD."Qty" AS "Qty", RBUD."Saldo" AS "Saldo", RBUD."IDSatuan" AS "Satuan", RBUD."Harga_satuan" AS "Harga_satuan", RBUD."Sub_total" AS "Sub_total"
							FROM tbl_retur_pembelian_umum AS RBU
							INNER JOIN tbl_retur_pembelian_umum_detail AS RBUD
								ON RBU."IDRBUmum" = RBUD."IDRBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON RBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON RBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON RBUD."IDBarang" = BAR."IDBarang"
							WHERE RBU."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Tanggal" >= datein
	AND RPK."Tanggal" <= dateuntil
	AND RPK."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_kain_like"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_kain_like"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPSD."Qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPS."Tanggal" >= datein
				AND RPS."Tanggal" <= dateuntil
				AND RPS."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_seragam_like"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_seragam_like"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPSD."Qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPS."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							INNER JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							INNER JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
							WHERE SOK."Tanggal" >= datein
								AND SOK."Tanggal" <= dateuntil
								AND SOK."Nomor" like keywords 
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_kain_like"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_kain_like"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							INNER JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							INNER JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
							WHERE SOK."Nomor" like keywords 				 
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON SOSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SOSD."IDWarna" = WAR."IDWarna"
-- 							INNER JOIN tbl_merk AS MERK
-- 								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
								WHERE SOS."Tanggal" >= datein
								AND SOS."Tanggal" <= dateuntil
								AND SOS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_seragam_like"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_seragam_like"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON SOSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SOSD."IDWarna" = WAR."IDWarna"
-- 							INNER JOIN tbl_merk AS MERK
-- 								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
								WHERE SOS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_scan
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_scan"(OUT "idin" varchar, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "panjang_yard" float8, OUT "panjang_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_scan"(OUT "idin" varchar, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "panjang_yard" float8, OUT "panjang_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDIn, Barcode, Nama_Barang, Corak, Warna, Merk, Panjang_yard, Panjang_meter, Grade, Satuan IN
        SELECT 
						MJ."IDIn" as "IDIn", MJ."Barcode" As Barcode, BAR."Nama_Barang" AS "Nama_Barang", COR."Corak" AS "Corak", WAR."Warna" AS "Warna", MER."Merk" AS "Merk", MJ."Panjang_yard" AS "Panjang_yard", MJ."Panjang_meter" AS "Panjang_meter", MJ."Grade" AS "Grade", SAT."Satuan" AS "Satuan"
							FROM tbl_in AS MJ
							INNER JOIN tbl_satuan AS SAT
								ON MJ."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJ."IDBarang" = BAR."IDBarang"
								INNER JOIN tbl_merk AS MER
								ON MJ."IDMerk" = MER."IDMerk"
								INNER JOIN tbl_warna AS WAR
								ON MJ."IDWarna" = WAR."IDWarna"
								INNER JOIN tbl_corak AS COR
								ON MJ."IDCorak" = COR."IDCorak"
								WHERE MJ."Barcode" LIKE keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_stokopname
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Tanggal" >= datein
								AND T1."Tanggal" <= dateuntil
								AND (T1."Barcode" like keywords OR T3."Corak" like keywords OR T4."Warna" like keywords)
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_stokopname_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_stokopname_like"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_stokopname_like"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Barcode" like keywords OR T3."Corak" like keywords OR T4."Warna" like keywords
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Warna, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
							WHERE SJCK."Tanggal" >= datein
								AND SJCK."Tanggal" <= dateuntil
								AND SJCK."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_kain_like"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_kain_like"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
							WHERE SJCK."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk",
SJCSD."Saldo_qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SJCSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SJCSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE SJCS."Tanggal" >= datein
								AND SJCS."Tanggal" <= dateuntil
								AND SJCS."Nomor" like keywords
								
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_seragam_like"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_seragam_like"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk",
SJCSD."Saldo_qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SJCSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SJCSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE SJCS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Album, M10, Kain, Lembaran, Qty IN
					 SELECT 
						PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Album" as "Album", PO."M10" as "M10", PO."Kain" as "Kain", PO."Lembaran" as "Lembaran", POD."Qty" As "Qty"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
								
								WHERE PO."Jenis_PO" = jenis_po_in

								AND PO."Tanggal" >= datein
								AND PO."Tanggal" <= dateuntil
								AND PO."Nomor" like keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po_like"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po_like"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Album, M10, Kain, Lembaran, Qty IN
					 SELECT 
					PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Album" as "Album", PO."M10" as "M10", PO."Kain" as "Kain", PO."Lembaran" as "Lembaran", POD."Qty" As "Qty"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
								
								WHERE PO."Jenis_PO" = jenis_po_in
								AND PO."Nomor" like keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po_umum"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po_umum"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						PO."IDPOUmum" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", POD."Qty" AS "Qty", POD."Saldo" AS "Saldo", POD."IDSatuan" AS "Satuan", POD."Harga_satuan" AS "Harga_satuan", POD."Sub_total" AS "Sub_total"
							FROM tbl_purchase_order_umum AS PO
							INNER JOIN tbl_purchase_order_umum_detail AS POD
								ON PO."IDPOUmum" = POD."IDPOUmum"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON POD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON POD."IDBarang" = BAR."IDBarang"
								WHERE PO."Tanggal" >= datein
								AND PO."Tanggal" <= dateuntil
								AND PO."Nomor" like keywords 							
								AND PO."Batal" = 'aktif'
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po_umum_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po_umum_like"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po_umum_like"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						PO."IDPOUmum" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", POD."Qty" AS "Qty", POD."Saldo" AS "Saldo", POD."IDSatuan" AS "Satuan", POD."Harga_satuan" AS "Harga_satuan", POD."Sub_total" AS "Sub_total"
							FROM tbl_purchase_order_umum AS PO
							INNER JOIN tbl_purchase_order_umum_detail AS POD
								ON PO."IDPOUmum" = POD."IDPOUmum"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON POD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON POD."IDBarang" = BAR."IDBarang"
								WHERE PO."Nomor" like keywords
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cf_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cf_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_tahun_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8);
CREATE OR REPLACE FUNCTION "public"."cf_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_tahun_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_tahun_lalu, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_tahun_lalu, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_setting_cf."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_tahun_lalu, tbl_setting_cf."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_cf ON tbl_jurnal."IDCOA"=tbl_setting_cf."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = '12' AND date_part('years',tbl_jurnal."Tanggal") = date_part('years',current_date)-1 AND date_part('years',tbl_jurnal."Tanggal")=caritahun
GROUP BY tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_jurnal."IDCOA", tbl_setting_cf."Keterangan",tbl_setting_cf."Perhitungan" ORDER BY tbl_setting_cf."IDSettingCF" ASC) m1
LEFT JOIN 
(SELECT tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_setting_cf."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_cf."Perhitungan" FROM tbl_jurnal RIGHT JOIN tbl_setting_cf ON tbl_jurnal."IDCOA"=tbl_setting_cf."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan AND date_part('years',tbl_jurnal."Tanggal")=caritahun GROUP BY tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_jurnal."IDCOA", tbl_setting_cf."Keterangan",tbl_setting_cf."Perhitungan"  ORDER BY tbl_setting_cf."IDSettingCF" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for getip
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."getip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar);
CREATE OR REPLACE FUNCTION "public"."getip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal, Barcode, NoSO, Party, Nama_Barang, Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party", g."Nama_Barang" AS "Nama_Barang", c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar"
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
							INNER JOIN tbl_barang AS "g" 
							ON c."IDBarang" = g."IDBarang"
			WHERE a."Batal"='Aktif'
    LOOP
	
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for getpo2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."getpo2"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, OUT "batal" varchar, IN "jenis_po_in" varchar);
CREATE OR REPLACE FUNCTION "public"."getpo2"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, OUT "batal" varchar, IN "jenis_po_in" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Album, M10, Kain, Lembaran, Qty, Batal IN
        SELECT 
						PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Album" as "Album", PO."M10" as "M10", PO."Kain" as "Kain", PO."Lembaran" as "Lembaran", POD."Qty" As "Qty", PO."Batal" AS "Batal"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
							WHERE PO."Jenis_PO" = jenis_po_in 
							AND PO."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for getpoumum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."getpoumum"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."getpoumum"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						PO."IDPOUmum" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", POD."Qty" AS "Qty", POD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", POD."Harga_satuan" AS "Harga_satuan", POD."Sub_total" AS "Sub_total"
							FROM tbl_purchase_order_umum AS PO
							INNER JOIN tbl_purchase_order_umum_detail AS POD
								ON PO."IDPOUmum" = POD."IDPOUmum"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON POD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON POD."IDBarang" = BAR."IDBarang"
							AND PO."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_bo
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty,
	Batal IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty",
	BO."Batal" AS "Batal"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	WHERE BO."Batal" = 'Aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total,
	Batal
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total",
	tbl_pembelian."Batal"
FROM
	tbl_pembelian
  JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
	JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Batal" = 'aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor,  CUS."Nama" As "Nama", PK."Nama_di_faktur" As "Nama_di_faktur", SJCK."Nomor" As "Nomor2",PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PKD."Harga" as "Harga", PKD."Sub_total" as "Sub_total"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Tanggal_jatuh_tempo,Nama_Barang, Corak, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, CUS."Nama" As "Nama", PS."Nama_di_faktur" As "Nama_di_faktur", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak",PSD."Saldo_qty_roll" as "Qty_roll", PSD."Saldo_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PSD."Harga" As "Harga", PSD."Sub_total" As "Sub_total"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Tanggal" >= datein
								AND PS."Tanggal" <= dateuntil
								AND PS."Nomor" like keywords
    LOOP
		
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Tanggal_jatuh_tempo,Nama_Barang, Corak, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, CUS."Nama" As "Nama", PS."Nama_di_faktur" As "Nama_di_faktur", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak",PSD."Saldo_qty_roll" as "Qty_roll", PSD."Saldo_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PSD."Harga" As "Harga", PSD."Sub_total" As "Sub_total"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Tanggal_jatuh_tempo,Nama_Barang, Corak, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, CUS."Nama" As "Nama", PS."Nama_di_faktur" As "Nama_di_faktur", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak",PSD."Saldo_qty_roll" as "Qty_roll", PSD."Saldo_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PSD."Harga" As "Harga", PSD."Sub_total" As "Sub_total"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_invoice_pembelian_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_invoice_pembelian_umum"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_invoice_pembelian_umum"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						FBU."IDFBUmum" as "IDFBUmum", FBU."Tanggal" As Tanggal, FBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", FBUD."Qty" AS "Qty", FBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", FBUD."Harga_satuan" AS "Harga_satuan", FBUD."Sub_total" AS "Sub_total"
							FROM tbl_pembelian_umum AS FBU
							INNER JOIN tbl_pembelian_umum_detail AS FBUD
								ON FBU."IDFBUmum" = FBUD."IDFBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON FBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON FBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON FBUD."IDBarang" = BAR."IDBarang"
							WHERE FBU."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_packing_list
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_pembelian_asset
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur,
	Batal
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur",
	T1."Batal"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	WHERE T1."Batal"='aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_penerimaan_barang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "batal" varchar, IN "jenis_tbs" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "batal" varchar, IN "jenis_tbs" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,
	Batal
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga,
	T1. "Batal" AS Batal
FROM
	tbl_terima_barang_supplier AS T1
	JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
	WHERE T1."Batal" = 'Aktif' AND T1."Jenis_TBS" = jenis_tbs
ORDER BY T1."IDTBS" DESC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_penerimaan_jahit
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_penerimaan_jahit"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_penerimaan_jahit"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSH, Tanggal, Nomor, NamaSup, Nomor_supplier, Nama_Barang, Merk, Saldo, Satuan, Harga, Total_harga IN
        SELECT 
						MJ."IDTBSH" as "IDTBSH", MJ."Tanggal" As Tanggal, MJ."Nomor" As Nomor, SUP."Nama" As NamaSup, MJ."Nomor_supplier" As "Nomor_supplier", BAR."Nama_Barang" AS "Nama_Barang", MER."Merk" AS "Merk", MJD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", MJD."Harga" AS "Harga", MJD."Total_harga" AS "Total_harga"
							FROM tbl_terima_barang_supplier_jahit AS MJ
							INNER JOIN tbl_terima_barang_supplier_jahit_detail AS MJD
								ON MJ."IDTBSH" = MJD."IDTBSH"
							INNER JOIN tbl_suplier AS SUP
								ON MJ."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON MJD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJD."IDBarang" = BAR."IDBarang"
								INNER JOIN tbl_merk AS MER
								ON MJD."IDMerk" = MER."IDMerk"
							WHERE MJ."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_penerimaan_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_penerimaan_umum"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_penerimaan_umum"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						TBS."IDTBSUmum" as "IDTBSUmum", TBS."Tanggal" As Tanggal, TBS."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", TBSD."Qty" AS "Qty", TBSD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", TBSD."Harga_satuan" AS "Harga_satuan", TBSD."Sub_total" AS "Sub_total"
							FROM tbl_terima_barang_supplier_umum AS TBS
							INNER JOIN tbl_terima_barang_supplier_umum_detail AS TBSD
								ON TBS."IDTBSUmum" = TBSD."IDTBSUmum"
							INNER JOIN tbl_suplier AS SUP
								ON TBS."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON TBSD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON TBSD."IDBarang" = BAR."IDBarang"
							WHERE TBS."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal,
	Batal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal",
	tbl_retur_pembelian."Batal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	WHERE tbl_retur_pembelian."Batal"='aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_pembelian_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_pembelian_umum"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_pembelian_umum"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						RBU."IDRBUmum" as "IDRBUmum", RBU."Tanggal" As Tanggal, RBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", RBUD."Qty" AS "Qty", RBUD."Saldo" AS "Saldo", RBUD."IDSatuan" AS "Satuan", RBUD."Harga_satuan" AS "Harga_satuan", RBUD."Sub_total" AS "Sub_total"
							FROM tbl_retur_pembelian_umum AS RBU
							INNER JOIN tbl_retur_pembelian_umum_detail AS RBUD
								ON RBU."IDRBUmum" = RBUD."IDRBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON RBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON RBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON RBUD."IDBarang" = BAR."IDBarang"
							WHERE RBU."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPS."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPS."Tanggal" >= datein
				AND RPS."Tanggal" <= dateuntil
				AND RPS."Nomor" like keywords 	
				
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
-- 	WAR."Warna" AS "Warna",
	RPSD."Saldo_qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
-- 	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_sales_order_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							INNER JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							INNER JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_sales_order_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON SOSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SOSD."IDWarna" = WAR."IDWarna"
-- 							INNER JOIN tbl_merk AS MERK
-- 								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_scan
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_scan"(OUT "idin" varchar, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "panjang_yard" float8, OUT "panjang_meter" float8, OUT "grade" varchar, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_scan"(OUT "idin" varchar, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "panjang_yard" float8, OUT "panjang_meter" float8, OUT "grade" varchar, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDIn, Barcode, Nama_Barang, Corak, Warna, Merk, Panjang_yard, Panjang_meter, Grade, Satuan IN
        SELECT 
						MJ."IDIn" as "IDIn", MJ."Barcode" As Barcode, BAR."Nama_Barang" AS "Nama_Barang", COR."Corak" AS "Corak", WAR."Warna" AS "Warna", MER."Merk" AS "Merk", MJ."Panjang_yard" AS "Panjang_yard", MJ."Panjang_meter" AS "Panjang_meter", MJ."Grade" AS "Grade", SAT."Satuan" AS "Satuan"
							FROM tbl_in AS MJ
							INNER JOIN tbl_satuan AS SAT
								ON MJ."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJ."IDBarang" = BAR."IDBarang"
								INNER JOIN tbl_merk AS MER
								ON MJ."IDMerk" = MER."IDMerk"
								INNER JOIN tbl_warna AS WAR
								ON MJ."IDWarna" = WAR."IDWarna"
								INNER JOIN tbl_corak AS COR
								ON MJ."IDCorak" = COR."IDCorak"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_stokopname
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_surat_jalan_customer_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Warna, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_surat_jalan_customer_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk",
SJCSD."Saldo_qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SJCSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SJCSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for notes
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."notes"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar);
CREATE OR REPLACE FUNCTION "public"."notes"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_setting_notes."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_notes."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_notes ON tbl_jurnal."IDCOA"=tbl_setting_notes."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_jurnal."IDCOA", tbl_setting_notes."Keterangan",tbl_setting_notes."Perhitungan"  ORDER BY tbl_setting_notes."IDSettingNotes" ASC
)m1
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for notes_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."notes_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."notes_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_setting_notes."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_notes."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_notes ON tbl_jurnal."IDCOA"=tbl_setting_notes."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_jurnal."IDCOA", tbl_setting_notes."Keterangan",tbl_setting_notes."Perhitungan"  ORDER BY tbl_setting_notes."IDSettingNotes" ASC
)m1
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for pl
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pl"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar);
CREATE OR REPLACE FUNCTION "public"."pl"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Bulan_sd_bulan, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Bulan_sd_bulan, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Bulan_sd_bulan, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") BETWEEN '1' AND '1'
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan" ORDER BY tbl_setting_pl."IDSettingPL" ASC) m1
LEFT JOIN (
SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan"  ORDER BY tbl_setting_pl."IDSettingPL" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for pl_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8);
CREATE OR REPLACE FUNCTION "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Bulan_sd_bulan, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m2.Total_bulan_ini, m1.Bulan_sd_bulan,m1."Perhitungan"
FROM (SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Bulan_sd_bulan, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" AND
(date_part('month',tbl_jurnal."Tanggal") BETWEEN '1' AND caribulan) AND date_part('year',tbl_jurnal."Tanggal")=caritahun
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan" ORDER BY tbl_setting_pl."IDSettingPL" ASC) m1
LEFT JOIN (
SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan AND date_part('year',tbl_jurnal."Tanggal")=caritahun
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan"  ORDER BY tbl_setting_pl."IDSettingPL" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for pl_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Bulan_sd_bulan, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m2.Total_bulan_ini, m1.Bulan_sd_bulan,m1."Perhitungan"
FROM (SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Bulan_sd_bulan, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") BETWEEN '1' AND caribulan
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan" ORDER BY tbl_setting_pl."IDSettingPL" ASC) m1
LEFT JOIN (
SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan"  ORDER BY tbl_setting_pl."IDSettingPL" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for stock
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."stock"(OUT "corak" varchar, OUT "grade" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "persediaan_awal_qty" float8, OUT "persediaan_awal_nilai" float8, OUT "beli_qty" float8, OUT "beli_nilai" float8, OUT "retur_beli_qty" float8, OUT "retur_beli_nilai" float8, OUT "koreksi_qty" float8, OUT "koreksi_nilai" float8, OUT "koreksi_scan_qty" float8, OUT "koreksi_scan_nilai" float8, OUT "hpp_qty" float8, OUT "hpp_nilai" float8, OUT "sample_qty" float8, OUT "sample_nilai" float8, OUT "retur_jual_qty" float8, OUT "retur_jual_nilai" float8, OUT "persediaan_akhir_qty" float8, OUT "persediaan_akhir_nilai" float8, IN "tanggal_mulai" date, IN "tanggal_selesai" date);
CREATE OR REPLACE FUNCTION "public"."stock"(OUT "corak" varchar, OUT "grade" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "persediaan_awal_qty" float8, OUT "persediaan_awal_nilai" float8, OUT "beli_qty" float8, OUT "beli_nilai" float8, OUT "retur_beli_qty" float8, OUT "retur_beli_nilai" float8, OUT "koreksi_qty" float8, OUT "koreksi_nilai" float8, OUT "koreksi_scan_qty" float8, OUT "koreksi_scan_nilai" float8, OUT "hpp_qty" float8, OUT "hpp_nilai" float8, OUT "sample_qty" float8, OUT "sample_nilai" float8, OUT "retur_jual_qty" float8, OUT "retur_jual_nilai" float8, OUT "persediaan_akhir_qty" float8, OUT "persediaan_akhir_nilai" float8, IN "tanggal_mulai" date, IN "tanggal_selesai" date)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Corak, Grade, Warna, Merk, Satuan, Harga, Persediaan_awal_qty, Persediaan_awal_nilai, Beli_qty, Beli_nilai, Retur_beli_qty, Retur_beli_nilai, Koreksi_qty, Koreksi_nilai, Koreksi_scan_qty, Koreksi_scan_nilai, Hpp_qty, Hpp_nilai, Sample_qty, Sample_nilai, Retur_jual_qty, Retur_jual_nilai, Persediaan_akhir_qty, Persediaan_akhir_nilai IN
       
SELECT m1."Corak", m1."Grade", m1."Warna", m1."Merk", m1."Satuan", m1."Harga", m1."persediaan_awal_qty", m1."persediaan_awal_nilai", m2."beli_qty", m2."beli_nilai", m3."retur_beli_qty", m3."retur_beli_nilai", m4."koreksi_qty", m4."koreksi_nilai", m5."koreksi_scan_qty", m5."koreksi_scan_nilai", m6."hpp_qty", m6."hpp_nilai", m7."sample_qty", m7."sample_nilai", m8."retur_jual_qty", m8."retur_jual_nilai", (m1."persediaan_awal_qty"+m2."beli_qty"-m3."retur_beli_qty"+m4."koreksi_qty"-m5."koreksi_scan_qty"-m6."hpp_qty"-m7."sample_qty"+m8."retur_jual_qty") AS persediaan_akhir_qty, (m1."persediaan_awal_nilai"+m2."beli_nilai"-m3."retur_beli_nilai"+m4."koreksi_nilai"-m5."koreksi_scan_nilai"-m6."hpp_nilai"-m7."sample_nilai"+m8."retur_jual_nilai") AS persediaan_akhir_nilai FROM
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS persediaan_awal_qty, coalesce(SUM(tbl_stok."Harga"),0) AS persediaan_awal_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='SA' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m1
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS beli_qty, coalesce(SUM(tbl_stok."Harga"),0) AS beli_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='TBS' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m2 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS retur_beli_qty, coalesce(SUM(tbl_stok."Harga"),0) AS retur_beli_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='RB' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m3 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS koreksi_qty, coalesce(SUM(tbl_stok."Harga"),0) AS koreksi_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='KP' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m4 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS koreksi_scan_qty, coalesce(SUM(tbl_stok."Harga"),0) AS koreksi_scan_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='KPS' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m5 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS hpp_qty, coalesce(SUM(tbl_stok."Harga"),0) AS hpp_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='SJ' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m6 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS sample_qty, coalesce(SUM(tbl_stok."Harga"),0) AS sample_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='SP' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m7 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS retur_jual_qty, coalesce(SUM(tbl_stok."Harga"),0) AS retur_jual_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='RJ' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m8 USING("Corak")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for testing_insert
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."testing_insert"();
CREATE OR REPLACE FUNCTION "public"."testing_insert"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
         INSERT INTO tbl_coa_saldo_awal(idcoasaldoawal,idcoa,idgroupcoa, nilaisaldoawal, aktif)
         VALUES(NEW.IDCOASaldoAwal,NEW.IDCoa,NEW.IDGroupCOA,NEW.Nilai_Saldo_Awal, NEW.Aktif);
 
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for update_stok
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."update_stok"();
CREATE OR REPLACE FUNCTION "public"."update_stok"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
begin
update stokbarang set stok_ketersediaan=stok_ketersediaan-new.jumlah_beli where kode_barang=new.kode_barang;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."tbl_agen_IDAgen_seq"
OWNED BY "public"."tbl_agen"."IDAgen";
SELECT setval('"public"."tbl_agen_IDAgen_seq"', 29, true);
ALTER SEQUENCE "public"."tbl_asset_IDAsset_seq"
OWNED BY "public"."tbl_asset"."IDAsset";
SELECT setval('"public"."tbl_asset_IDAsset_seq"', 292, true);
ALTER SEQUENCE "public"."tbl_asset_IDGroupAsset_seq"
OWNED BY "public"."tbl_asset"."IDGroupAsset";
SELECT setval('"public"."tbl_asset_IDGroupAsset_seq"', 269, true);
ALTER SEQUENCE "public"."tbl_bank_IDBank_seq"
OWNED BY "public"."tbl_bank"."IDBank";
SELECT setval('"public"."tbl_bank_IDBank_seq"', 44, true);
ALTER SEQUENCE "public"."tbl_bank_IDCoa_seq"
OWNED BY "public"."tbl_bank"."IDCoa";
SELECT setval('"public"."tbl_bank_IDCoa_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_barang_IDBarang_seq"
OWNED BY "public"."tbl_barang"."IDBarang";
SELECT setval('"public"."tbl_barang_IDBarang_seq"', 25, true);
ALTER SEQUENCE "public"."tbl_barang_IDCorak_seq"
OWNED BY "public"."tbl_barang"."IDCorak";
SELECT setval('"public"."tbl_barang_IDCorak_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_barang_IDGroupBarang_seq"
OWNED BY "public"."tbl_barang"."IDGroupBarang";
SELECT setval('"public"."tbl_barang_IDGroupBarang_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_barang_IDMerk_seq"
OWNED BY "public"."tbl_barang"."IDMerk";
SELECT setval('"public"."tbl_barang_IDMerk_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_barang_IDSatuan_seq"
OWNED BY "public"."tbl_barang"."IDSatuan";
SELECT setval('"public"."tbl_barang_IDSatuan_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_barcode_print_IDBarcodePrint_seq"
OWNED BY "public"."tbl_barcode_print"."IDBarcodePrint";
SELECT setval('"public"."tbl_barcode_print_IDBarcodePrint_seq"', 8, true);
ALTER SEQUENCE "public"."tbl_booking_order_IDBO_seq"
OWNED BY "public"."tbl_booking_order"."IDBO";
SELECT setval('"public"."tbl_booking_order_IDBO_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_booking_order_IDCorak_seq"
OWNED BY "public"."tbl_booking_order"."IDCorak";
SELECT setval('"public"."tbl_booking_order_IDCorak_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_booking_order_IDMataUang_seq"
OWNED BY "public"."tbl_booking_order"."IDMataUang";
SELECT setval('"public"."tbl_booking_order_IDMataUang_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_coa_IDCoa_seq"
OWNED BY "public"."tbl_coa"."IDCoa";
SELECT setval('"public"."tbl_coa_IDCoa_seq"', 29, true);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDCOASaldoAwal";
SELECT setval('"public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDCoa_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDCoa";
SELECT setval('"public"."tbl_coa_saldo_awal_IDCoa_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDGroupCOA_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDGroupCOA";
SELECT setval('"public"."tbl_coa_saldo_awal_IDGroupCOA_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_corak_IDCorak_seq"
OWNED BY "public"."tbl_corak"."IDCorak";
SELECT setval('"public"."tbl_corak_IDCorak_seq"', 20, true);
ALTER SEQUENCE "public"."tbl_corak_IDMerk_seq"
OWNED BY "public"."tbl_corak"."IDMerk";
SELECT setval('"public"."tbl_corak_IDMerk_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_customer_IDCustomer_seq"
OWNED BY "public"."tbl_customer"."IDCustomer";
SELECT setval('"public"."tbl_customer_IDCustomer_seq"', 99, true);
ALTER SEQUENCE "public"."tbl_customer_IDGroupCustomer_seq"
OWNED BY "public"."tbl_customer"."IDGroupCustomer";
SELECT setval('"public"."tbl_customer_IDGroupCustomer_seq"', 91, true);
ALTER SEQUENCE "public"."tbl_giro_IDFaktur_seq"
OWNED BY "public"."tbl_giro"."IDFaktur";
SELECT setval('"public"."tbl_giro_IDFaktur_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_giro_IDGiro_seq"
OWNED BY "public"."tbl_giro"."IDGiro";
SELECT setval('"public"."tbl_giro_IDGiro_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_giro_IDPerusahaan_seq"
OWNED BY "public"."tbl_giro"."IDPerusahaan";
SELECT setval('"public"."tbl_giro_IDPerusahaan_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_group_asset_IDGroupAsset_seq"
OWNED BY "public"."tbl_group_asset"."IDGroupAsset";
SELECT setval('"public"."tbl_group_asset_IDGroupAsset_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_group_barang_IDGroupBarang_seq"
OWNED BY "public"."tbl_group_barang"."IDGroupBarang";
SELECT setval('"public"."tbl_group_barang_IDGroupBarang_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_group_coa_IDGroupCOA_seq"
OWNED BY "public"."tbl_group_coa"."IDGroupCOA";
SELECT setval('"public"."tbl_group_coa_IDGroupCOA_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_group_customer_IDGroupCustomer_seq"
OWNED BY "public"."tbl_group_customer"."IDGroupCustomer";
SELECT setval('"public"."tbl_group_customer_IDGroupCustomer_seq"', 25, true);
ALTER SEQUENCE "public"."tbl_group_supplier_IDGroupSupplier_seq"
OWNED BY "public"."tbl_group_supplier"."IDGroupSupplier";
SELECT setval('"public"."tbl_group_supplier_IDGroupSupplier_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_group_user_IDGroupUser_seq"
OWNED BY "public"."tbl_group_user"."IDGroupUser";
SELECT setval('"public"."tbl_group_user_IDGroupUser_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_gudang_IDGudang_seq"
OWNED BY "public"."tbl_gudang"."IDGudang";
SELECT setval('"public"."tbl_gudang_IDGudang_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDBarang_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDBarang";
SELECT setval('"public"."tbl_harga_jual_barang_IDBarang_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDGroupCustomer_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDGroupCustomer";
SELECT setval('"public"."tbl_harga_jual_barang_IDGroupCustomer_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDHargaJual_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDHargaJual";
SELECT setval('"public"."tbl_harga_jual_barang_IDHargaJual_seq"', 34, true);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDSatuan_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDSatuan";
SELECT setval('"public"."tbl_harga_jual_barang_IDSatuan_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_hutang_IDFaktur_seq"
OWNED BY "public"."tbl_hutang"."IDFaktur";
SELECT setval('"public"."tbl_hutang_IDFaktur_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_hutang_IDHutang_seq"
OWNED BY "public"."tbl_hutang"."IDHutang";
SELECT setval('"public"."tbl_hutang_IDHutang_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_hutang_IDSupplier_seq"
OWNED BY "public"."tbl_hutang"."IDSupplier";
SELECT setval('"public"."tbl_hutang_IDSupplier_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_in_IDBarang_seq"
OWNED BY "public"."tbl_in"."IDBarang";
SELECT setval('"public"."tbl_in_IDBarang_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_in_IDCorak_seq"
OWNED BY "public"."tbl_in"."IDCorak";
SELECT setval('"public"."tbl_in_IDCorak_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_in_IDIn_seq"
OWNED BY "public"."tbl_in"."IDIn";
SELECT setval('"public"."tbl_in_IDIn_seq"', 92, true);
ALTER SEQUENCE "public"."tbl_in_IDMerk_seq"
OWNED BY "public"."tbl_in"."IDMerk";
SELECT setval('"public"."tbl_in_IDMerk_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_in_IDSatuan_seq"
OWNED BY "public"."tbl_in"."IDSatuan";
SELECT setval('"public"."tbl_in_IDSatuan_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_in_IDWarna_seq"
OWNED BY "public"."tbl_in"."IDWarna";
SELECT setval('"public"."tbl_in_IDWarna_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDIP_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDIP";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDIP_seq"', 56, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDPO_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDPO";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDPO_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDSupplier_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDSupplier";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDSupplier_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDBarang";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDBarang_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDCorak";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDCorak_seq"', 18, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDIPDetail";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq"', 97, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIP_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDIP";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDIP_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDMerk";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDMerk_seq"', 18, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDSatuan";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq"', 18, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDWarna";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDWarna_seq"', 18, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDBarang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDBarang";
SELECT setval('"public"."tbl_kartu_stok_IDBarang_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDCorak_seq"
OWNED BY "public"."tbl_kartu_stok"."IDCorak";
SELECT setval('"public"."tbl_kartu_stok_IDCorak_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDFakturDetail_seq"
OWNED BY "public"."tbl_kartu_stok"."IDFakturDetail";
SELECT setval('"public"."tbl_kartu_stok_IDFakturDetail_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDFaktur_seq"
OWNED BY "public"."tbl_kartu_stok"."IDFaktur";
SELECT setval('"public"."tbl_kartu_stok_IDFaktur_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDGudang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDGudang";
SELECT setval('"public"."tbl_kartu_stok_IDGudang_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDKartuStok_seq"
OWNED BY "public"."tbl_kartu_stok"."IDKartuStok";
SELECT setval('"public"."tbl_kartu_stok_IDKartuStok_seq"', 211, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDMataUang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDMataUang";
SELECT setval('"public"."tbl_kartu_stok_IDMataUang_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDSatuan_seq"
OWNED BY "public"."tbl_kartu_stok"."IDSatuan";
SELECT setval('"public"."tbl_kartu_stok_IDSatuan_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDStok_seq"
OWNED BY "public"."tbl_kartu_stok"."IDStok";
SELECT setval('"public"."tbl_kartu_stok_IDStok_seq"', 13, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDWarna_seq"
OWNED BY "public"."tbl_kartu_stok"."IDWarna";
SELECT setval('"public"."tbl_kartu_stok_IDWarna_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDKonversi_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDKonversi";
SELECT setval('"public"."tbl_konversi_satuan_IDKonversi_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDSatuanBerat_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDSatuanBerat";
SELECT setval('"public"."tbl_konversi_satuan_IDSatuanBerat_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDSatuanKecil_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDSatuanKecil";
SELECT setval('"public"."tbl_konversi_satuan_IDSatuanKecil_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_IDKP_seq"
OWNED BY "public"."tbl_koreksi_persediaan"."IDKP";
SELECT setval('"public"."tbl_koreksi_persediaan_IDKP_seq"', 63, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_IDMataUang_seq"
OWNED BY "public"."tbl_koreksi_persediaan"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_IDMataUang_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDBarang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDBarang";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDBarang_seq"', 21, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDCorak_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDCorak";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDCorak_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDKPDetail";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq"', 81, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKP_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDKP";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDKP_seq"', 16, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDMataUang_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDSatuan";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDSatuan_seq"', 21, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDWarna_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDWarna";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDWarna_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan"."IDKPScan";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_IDKPScan_seq"', 36, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDBarang";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDCorak";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDKPScanDetail";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"', 40, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDKPScan";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDSatuan";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDWarna";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_kota_IDKota_seq"
OWNED BY "public"."tbl_kota"."IDKota";
SELECT setval('"public"."tbl_kota_IDKota_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDBarang_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDBarang";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDBarang_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDCorak_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDCorak";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDCorak_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDLapPers_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDLapPers";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDLapPers_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDMerk_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDMerk";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDMerk_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDWarna_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDWarna";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDWarna_seq"', 10, false);
ALTER SEQUENCE "public"."tbl_mata_uang_IDMataUang_seq"
OWNED BY "public"."tbl_mata_uang"."IDMataUang";
SELECT setval('"public"."tbl_mata_uang_IDMataUang_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_menu_IDMenu_seq"
OWNED BY "public"."tbl_menu"."IDMenu";
SELECT setval('"public"."tbl_menu_IDMenu_seq"', 31, true);
ALTER SEQUENCE "public"."tbl_menu_detail_IDMenuDetail_seq"
OWNED BY "public"."tbl_menu_detail"."IDMenuDetail";
SELECT setval('"public"."tbl_menu_detail_IDMenuDetail_seq"', 116, true);
ALTER SEQUENCE "public"."tbl_menu_detail_IDMenu_seq"
OWNED BY "public"."tbl_menu_detail"."IDMenu";
SELECT setval('"public"."tbl_menu_detail_IDMenu_seq"', 89, true);
ALTER SEQUENCE "public"."tbl_menu_role_IDGroupUser_seq"
OWNED BY "public"."tbl_menu_role"."IDGroupUser";
SELECT setval('"public"."tbl_menu_role_IDGroupUser_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_menu_role_IDMenuDetail_seq"
OWNED BY "public"."tbl_menu_role"."IDMenuDetail";
SELECT setval('"public"."tbl_menu_role_IDMenuDetail_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_menu_role_IDMenuRole_seq"
OWNED BY "public"."tbl_menu_role"."IDMenuRole";
SELECT setval('"public"."tbl_menu_role_IDMenuRole_seq"', 4294, true);
ALTER SEQUENCE "public"."tbl_merk_IDMerk_seq"
OWNED BY "public"."tbl_merk"."IDMerk";
SELECT setval('"public"."tbl_merk_IDMerk_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_out_IDOut_seq"
OWNED BY "public"."tbl_out"."IDOut";
SELECT setval('"public"."tbl_out_IDOut_seq"', 37, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDCOA_seq"
OWNED BY "public"."tbl_pembayaran"."IDCOA";
SELECT setval('"public"."tbl_pembayaran_IDCOA_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDFBPembayaran_seq"
OWNED BY "public"."tbl_pembayaran"."IDFBPembayaran";
SELECT setval('"public"."tbl_pembayaran_IDFBPembayaran_seq"', 35, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDFB_seq"
OWNED BY "public"."tbl_pembayaran"."IDFB";
SELECT setval('"public"."tbl_pembayaran_IDFB_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_pembayaran_IDMataUang_seq"
OWNED BY "public"."tbl_pembayaran"."IDMataUang";
SELECT setval('"public"."tbl_pembayaran_IDMataUang_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDFB_seq"
OWNED BY "public"."tbl_pembelian"."IDFB";
SELECT setval('"public"."tbl_pembelian_IDFB_seq"', 114, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDMataUang_seq"
OWNED BY "public"."tbl_pembelian"."IDMataUang";
SELECT setval('"public"."tbl_pembelian_IDMataUang_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDSupplier_seq"
OWNED BY "public"."tbl_pembelian"."IDSupplier";
SELECT setval('"public"."tbl_pembelian_IDSupplier_seq"', 13, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDTBS_seq"
OWNED BY "public"."tbl_pembelian"."IDTBS";
SELECT setval('"public"."tbl_pembelian_IDTBS_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_IDFBA_seq"
OWNED BY "public"."tbl_pembelian_asset"."IDFBA";
SELECT setval('"public"."tbl_pembelian_asset_IDFBA_seq"', 95, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_IDMataUang_seq"
OWNED BY "public"."tbl_pembelian_asset"."IDMataUang";
SELECT setval('"public"."tbl_pembelian_asset_IDMataUang_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDAsset_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDAsset";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDAsset_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBADetail_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDFBADetail";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDFBADetail_seq"', 185, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBA_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDFBA";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDFBA_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDGroupAsset";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDGroupAsset_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDBarang_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDBarang";
SELECT setval('"public"."tbl_pembelian_detail_IDBarang_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDCorak_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDCorak";
SELECT setval('"public"."tbl_pembelian_detail_IDCorak_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDFBDetail_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDFBDetail";
SELECT setval('"public"."tbl_pembelian_detail_IDFBDetail_seq"', 138, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDFB_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDFB";
SELECT setval('"public"."tbl_pembelian_detail_IDFB_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDMerk_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDMerk";
SELECT setval('"public"."tbl_pembelian_detail_IDMerk_seq"', 28, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDSatuan_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDSatuan";
SELECT setval('"public"."tbl_pembelian_detail_IDSatuan_seq"', 38, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDTBSDetail_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDTBSDetail";
SELECT setval('"public"."tbl_pembelian_detail_IDTBSDetail_seq"', 12, false);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDWarna_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDWarna";
SELECT setval('"public"."tbl_pembelian_detail_IDWarna_seq"', 28, true);
SELECT setval('"public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq"', 53, true);
SELECT setval('"public"."tbl_pembelian_grand_total_IDFB_seq"', 18, true);
SELECT setval('"public"."tbl_piutang_IDCustomer_seq"', 806, true);
SELECT setval('"public"."tbl_piutang_IDFaktur_seq"', 817, true);
SELECT setval('"public"."tbl_piutang_IDPiutang_seq"', 817, true);
SELECT setval('"public"."tbl_purchase_order_IDAgen_seq"', 11, false);
SELECT setval('"public"."tbl_purchase_order_IDBarang_seq"', 11, false);
SELECT setval('"public"."tbl_purchase_order_IDCorak_seq"', 11, false);
SELECT setval('"public"."tbl_purchase_order_IDMataUang_seq"', 11, false);
SELECT setval('"public"."tbl_purchase_order_IDMerk_seq"', 11, false);
SELECT setval('"public"."tbl_purchase_order_IDPO_seq"', 88, true);
SELECT setval('"public"."tbl_purchase_order_IDSupplier_seq"', 11, false);
SELECT setval('"public"."tbl_purchase_order_detail_IDPODetail_seq"', 92, true);
SELECT setval('"public"."tbl_purchase_order_detail_IDPO_seq"', 11, false);
SELECT setval('"public"."tbl_purchase_order_detail_IDWarna_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_IDFB_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_IDRB_seq"', 29, true);
SELECT setval('"public"."tbl_retur_pembelian_IDSupplier_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDBarang_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDCorak_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDMerk_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDRBDetail_seq"', 44, true);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDRB_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDSatuan_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDWarna_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDMataUang_seq"', 11, false);
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq"', 27, true);
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDRB_seq"', 11, false);
SELECT setval('"public"."tbl_saldo_awal_asset_IDAsset_seq"', 138, true);
SELECT setval('"public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq"', 149, true);
SELECT setval('"public"."tbl_satuan_IDSatuan_seq"', 20, true);
SELECT setval('"public"."tbl_stok_IDBarang_seq"', 28, true);
SELECT setval('"public"."tbl_stok_IDCorak_seq"', 28, true);
SELECT setval('"public"."tbl_stok_IDFakturDetail_seq"', 33, true);
SELECT setval('"public"."tbl_stok_IDFaktur_seq"', 33, true);
SELECT setval('"public"."tbl_stok_IDGudang_seq"', 28, true);
SELECT setval('"public"."tbl_stok_IDMataUang_seq"', 28, true);
SELECT setval('"public"."tbl_stok_IDSatuan_seq"', 31, true);
SELECT setval('"public"."tbl_stok_IDStok_seq"', 172, true);
SELECT setval('"public"."tbl_stok_IDWarna_seq"', 28, true);
SELECT setval('"public"."tbl_stok_history_AsalIDFakturDetail_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_AsalIDFaktur_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDBarang_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDCorak_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDFakturDetail_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDFaktur_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDGudang_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDHistoryStok_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDMataUang_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDSatuan_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDStok_seq"', 9, false);
SELECT setval('"public"."tbl_stok_history_IDWarna_seq"', 9, false);
SELECT setval('"public"."tbl_stok_opname_IDBarang_seq"', 9, true);
SELECT setval('"public"."tbl_stok_opname_IDCorak_seq"', 10, true);
SELECT setval('"public"."tbl_stok_opname_IDGudang_seq"', 11, true);
SELECT setval('"public"."tbl_stok_opname_IDSatuan_seq"', 13, true);
SELECT setval('"public"."tbl_stok_opname_IDStokOpname_seq"', 16, true);
SELECT setval('"public"."tbl_stok_opname_IDWarna_seq"', 10, true);
SELECT setval('"public"."tbl_suplier_IDGroupSupplier_seq"', 11, false);
SELECT setval('"public"."tbl_suplier_IDSupplier_seq"', 17, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDPO_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDSJM_seq"', 23, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq"', 26, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq"', 24, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq"', 11, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq"', 11, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq"', 11, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq"', 11, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq"', 27, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq"', 11, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq"', 11, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq"', 11, true);
SELECT setval('"public"."tbl_tampungan_IDT_seq"', 505, true);
SELECT setval('"public"."tbl_terima_barang_supplier_IDPO_seq"', 55, true);
SELECT setval('"public"."tbl_terima_barang_supplier_IDSupplier_seq"', 45, true);
SELECT setval('"public"."tbl_terima_barang_supplier_IDTBS_seq"', 104, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDBarang_seq"', 16, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDCorak_seq"', 20, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDMerk_seq"', 31, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDSatuan_seq"', 33, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq"', 265, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDTBS_seq"', 13, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDWarna_seq"', 30, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDSJH_seq"', 12, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq"', 12, false);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq"', 44, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"', 12, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"', 12, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq"', 12, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq"', 50, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq"', 12, true);
SELECT setval('"public"."tbl_um_customer_IDCustomer_seq"', 13, true);
SELECT setval('"public"."tbl_um_customer_IDFaktur_seq"', 40, true);
SELECT setval('"public"."tbl_um_customer_IDUMCustomer_seq"', 18, true);
SELECT setval('"public"."tbl_user_IDGroupUser_seq"', 12, false);
SELECT setval('"public"."tbl_user_IDUser_seq"', 27, true);
SELECT setval('"public"."tbl_warna_IDWarna_seq"', 20, true);

-- ----------------------------
-- Primary Key structure for table tbl_po_asset
-- ----------------------------
ALTER TABLE "public"."tbl_po_asset" ADD CONSTRAINT "tbl_po_asset_pkey" PRIMARY KEY ("IDPOAsset");
