-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 10, 2019 at 09:13 AM
-- Server version: 5.6.43
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `advess_lushentex`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(255) NOT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `lebar` varchar(50) DEFAULT NULL,
  `gramasi` varchar(50) DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT 'aktif',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `lebar`, `gramasi`, `status`, `created_at`) VALUES
('1A', 'BABYTERRY PE 30\'S', '36', '180-190', 'aktif', '2019-03-09 18:03:03'),
('1B', 'BABYTERRY PE 30\'S', '36', '190-200', 'aktif', '2019-03-09 18:03:20'),
('2A', 'PE 20\'S SK', '36', '190-200', 'aktif', '2019-03-09 18:03:46'),
('3A', 'PE 24\'S SK', '36', '170-180', 'aktif', '2019-03-09 18:04:07'),
('4A', 'PE 28\'S SK', '36', '150-160', 'aktif', '2019-03-09 18:04:40'),
('5A', 'PE 30\'S SK', '36', '140-150', 'aktif', '2019-03-09 18:05:47'),
('6A', 'SPANDEX PE', '72', '170-180', 'aktif', '2019-03-09 18:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `bkkb`
--

CREATE TABLE `bkkb` (
  `no_bkkb` varchar(50) NOT NULL,
  `id_sub_perusahaan` int(11) DEFAULT NULL,
  `kode_supplier` varchar(15) DEFAULT NULL,
  `no_rek_tujuan` varchar(30) DEFAULT NULL,
  `atas_nama` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status_pembulatan` enum('+','-') DEFAULT '+',
  `pembulatan` double DEFAULT '0',
  `total_bayar` double DEFAULT NULL,
  `bayar` double DEFAULT '0',
  `selisih` double DEFAULT '0',
  `status_print` enum('ya','tidak') DEFAULT 'tidak',
  `total_debet` double DEFAULT '0',
  `keterangan` text,
  `tambah_bayar` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkkb`
--

INSERT INTO `bkkb` (`no_bkkb`, `id_sub_perusahaan`, `kode_supplier`, `no_rek_tujuan`, `atas_nama`, `tanggal`, `status_pembulatan`, `pembulatan`, `total_bayar`, `bayar`, `selisih`, `status_print`, `total_debet`, `keterangan`, `tambah_bayar`) VALUES
('BKKB-00001', 3, 'CDR-0004', '-', '-', '2019-03-18', '+', 0, 18640000, 0, 18640000, 'tidak', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bpb_pembelian`
--

CREATE TABLE `bpb_pembelian` (
  `no_bpb` varchar(255) NOT NULL,
  `no_po` varchar(255) DEFAULT NULL,
  `no_retur_beli` varchar(255) DEFAULT NULL,
  `no_sj_beli` varchar(255) DEFAULT NULL,
  `jatuh_tempo` varchar(100) DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bpb_pembelian`
--

INSERT INTO `bpb_pembelian` (`no_bpb`, `no_po`, `no_retur_beli`, `no_sj_beli`, `jatuh_tempo`, `status`, `tanggal`, `created_at`) VALUES
('BPB00001', 'CDRPO-00002', NULL, 'CDR-0001', '2019-06-08', 'aktif', '2019-03-10', '2019-03-09 18:22:32');

-- --------------------------------------------------------

--
-- Table structure for table `bpkb`
--

CREATE TABLE `bpkb` (
  `no_bpkb` varchar(50) NOT NULL,
  `id_sub_perusahaan` int(11) DEFAULT NULL,
  `id_langganan` bigint(10) DEFAULT NULL,
  `no_rek_tujuan` varchar(30) DEFAULT NULL,
  `atas_nama` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status_pembulatan` enum('+','-') DEFAULT '+',
  `pembulatan` double DEFAULT '0',
  `total_bayar` double DEFAULT NULL,
  `bayar` double DEFAULT '0',
  `selisih` double DEFAULT '0',
  `status_print` enum('ya','tidak') DEFAULT 'tidak',
  `total_kredit` double DEFAULT '0',
  `keterangan` text,
  `tambah_bayar` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bpkb`
--

INSERT INTO `bpkb` (`no_bpkb`, `id_sub_perusahaan`, `id_langganan`, `no_rek_tujuan`, `atas_nama`, `tanggal`, `status_pembulatan`, `pembulatan`, `total_bayar`, `bayar`, `selisih`, `status_print`, `total_kredit`, `keterangan`, `tambah_bayar`) VALUES
('BPKB-00001', 3, 170, '76r5888', '', '2019-03-18', '+', 700000, 4214000, 0, 4214000, 'tidak', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_sales_order`
--

CREATE TABLE `detail_sales_order` (
  `id_detail_sales_order` bigint(20) NOT NULL,
  `no_so` varchar(255) DEFAULT NULL,
  `warna` varchar(50) DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `keterangan` text,
  `kode_warna` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_sales_order`
--

INSERT INTO `detail_sales_order` (`id_detail_sales_order`, `no_so`, `warna`, `jumlah`, `harga`, `keterangan`, `kode_warna`) VALUES
(102, 'SO00001', 'PUTIH', 40, 42000, '-', 'PUTIHNETRAL');

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `id_jabatan` int(11) NOT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `salesman_view` enum('ya','tidak') DEFAULT 'tidak',
  `salesman_input` enum('ya','tidak') DEFAULT 'tidak',
  `salesman_update` enum('ya','tidak') DEFAULT 'tidak',
  `salesman_delete` enum('ya','tidak') DEFAULT 'tidak',
  `barang_view` enum('ya','tidak') DEFAULT 'tidak',
  `barang_input` enum('ya','tidak') DEFAULT 'tidak',
  `barang_update` enum('ya','tidak') DEFAULT 'tidak',
  `barang_delete` enum('ya','tidak') DEFAULT 'tidak',
  `supplier_view` enum('ya','tidak') DEFAULT 'tidak',
  `supplier_input` enum('ya','tidak') DEFAULT 'tidak',
  `supplier_update` enum('ya','tidak') DEFAULT 'tidak',
  `supplier_delete` enum('ya','tidak') DEFAULT 'tidak',
  `customer_view` enum('ya','tidak') DEFAULT 'tidak',
  `customer_input` enum('ya','tidak') DEFAULT 'tidak',
  `customer_update` enum('ya','tidak') DEFAULT 'tidak',
  `customer_delete` enum('ya','tidak') DEFAULT 'tidak',
  `po_view` enum('ya','tidak') DEFAULT 'tidak',
  `bpb_view` enum('ya','tidak') DEFAULT 'tidak',
  `retur_beli_view` enum('ya','tidak') DEFAULT 'tidak',
  `so_view` enum('ya','tidak') DEFAULT 'tidak',
  `sj_view` enum('ya','tidak') DEFAULT 'tidak',
  `retur_jual_view` enum('ya','tidak') DEFAULT 'tidak',
  `pakunting_view` enum('ya','tidak') DEFAULT 'tidak',
  `pbarang_view` enum('ya','tidak') DEFAULT 'tidak',
  `debet_view` enum('ya','tidak') DEFAULT 'tidak',
  `kredit_view` enum('ya','tidak') DEFAULT 'tidak',
  `kontra_beli_view` enum('ya','tidak') DEFAULT 'tidak',
  `kontra_jual_view` enum('ya','tidak') DEFAULT 'tidak',
  `bkkb_view` enum('ya','tidak') DEFAULT 'tidak',
  `bpkb_view` enum('ya','tidak') DEFAULT 'tidak',
  `komisi_view` enum('ya','tidak') DEFAULT 'tidak'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`id_jabatan`, `jabatan`, `keterangan`, `salesman_view`, `salesman_input`, `salesman_update`, `salesman_delete`, `barang_view`, `barang_input`, `barang_update`, `barang_delete`, `supplier_view`, `supplier_input`, `supplier_update`, `supplier_delete`, `customer_view`, `customer_input`, `customer_update`, `customer_delete`, `po_view`, `bpb_view`, `retur_beli_view`, `so_view`, `sj_view`, `retur_jual_view`, `pakunting_view`, `pbarang_view`, `debet_view`, `kredit_view`, `kontra_beli_view`, `kontra_jual_view`, `bkkb_view`, `bpkb_view`, `komisi_view`) VALUES
(5, 'admin', 'admin', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya'),
(7, 'tes', 'hn', 'ya', 'tidak', 'tidak', 'tidak', 'ya', 'tidak', 'tidak', 'tidak', 'ya', 'tidak', 'tidak', 'tidak', 'ya', 'tidak', 'tidak', 'tidak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hasil_komisi`
--

CREATE TABLE `hasil_komisi` (
  `id_hasil_komisi` bigint(20) NOT NULL,
  `no_sj` varchar(255) DEFAULT NULL,
  `no_retur_jual` varchar(255) DEFAULT NULL,
  `id_sales` bigint(20) DEFAULT NULL,
  `nama_sales` varchar(100) DEFAULT NULL,
  `komisi_sales` double DEFAULT NULL,
  `up_sales` double DEFAULT NULL,
  `sj_komisi_sales` double DEFAULT NULL,
  `sj_up_sales` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `r_komisi` double DEFAULT NULL,
  `r_up_sales` double DEFAULT NULL,
  `minus_komisi` double DEFAULT NULL,
  `status` enum('lunas','belum lunas') DEFAULT 'belum lunas',
  `tgl_cair` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_komisi`
--

INSERT INTO `hasil_komisi` (`id_hasil_komisi`, `no_sj`, `no_retur_jual`, `id_sales`, `nama_sales`, `komisi_sales`, `up_sales`, `sj_komisi_sales`, `sj_up_sales`, `total`, `r_komisi`, `r_up_sales`, `minus_komisi`, `status`, `tgl_cair`) VALUES
(2, 'LST-00002', NULL, 66, 'BUBUN', 250, 1000, 11987.5, 47950, 59937.5, NULL, NULL, NULL, 'belum lunas', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `komisi`
--

CREATE TABLE `komisi` (
  `id_komisi` bigint(20) NOT NULL,
  `id_sales` bigint(20) DEFAULT NULL,
  `komisi` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komisi`
--

INSERT INTO `komisi` (`id_komisi`, `id_sales`, `komisi`) VALUES
(191, 65, 100),
(192, 65, 150),
(193, 66, 200),
(194, 66, 250),
(195, 67, 300),
(196, 67, 350),
(197, 68, 400),
(198, 68, 450),
(199, 69, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontra_beli`
--

CREATE TABLE `kontra_beli` (
  `no_kontra_beli` varchar(255) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `kode_supplier` varchar(15) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `no_kontra_sup` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontra_beli`
--

INSERT INTO `kontra_beli` (`no_kontra_beli`, `tanggal`, `kode_supplier`, `total`, `no_kontra_sup`) VALUES
('KBB-00001', '2019-03-18', 'CDR-0004', 18640000, '');

-- --------------------------------------------------------

--
-- Table structure for table `kontra_jual`
--

CREATE TABLE `kontra_jual` (
  `no_kontra_jual` varchar(255) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `id_langganan` bigint(20) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `id_sub_perusahaan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontra_jual`
--

INSERT INTO `kontra_jual` (`no_kontra_jual`, `tanggal`, `id_langganan`, `total`, `id_sub_perusahaan`) VALUES
('KBJ-00001', '2019-03-10', 170, 4214000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `langganan`
--

CREATE TABLE `langganan` (
  `id_langganan` bigint(20) NOT NULL,
  `nama_langganan` varchar(100) DEFAULT NULL,
  `limit_kredit` double DEFAULT NULL,
  `limit_kredit_stop` double DEFAULT NULL,
  `jatuh_tempo` varchar(50) DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT 'aktif',
  `penggunaan` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `langganan`
--

INSERT INTO `langganan` (`id_langganan`, `nama_langganan`, `limit_kredit`, `limit_kredit_stop`, `jatuh_tempo`, `status`, `penggunaan`) VALUES
(169, 'TOKO BUSANA', 100000000, 200000000, '15', 'aktif', 0),
(170, 'GO GREEN', 300000000, 400000000, '30', 'aktif', 2150310),
(171, 'LST', 10000000000, 10000000000, '300', 'aktif', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nota_debet`
--

CREATE TABLE `nota_debet` (
  `no_nota_debet` varchar(255) NOT NULL,
  `no_po` varchar(255) DEFAULT NULL,
  `kode_supplier` varchar(15) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `keterangan` text,
  `tanggal` date DEFAULT NULL,
  `status_print` enum('sudah','belum') DEFAULT 'belum',
  `adjustment` enum('ya','tidak') DEFAULT 'tidak',
  `status_pakai` enum('pakai','belum') DEFAULT 'belum'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nota_kredit`
--

CREATE TABLE `nota_kredit` (
  `no_nota_kredit` varchar(255) NOT NULL,
  `no_sj` varchar(255) DEFAULT NULL,
  `id_langganan` bigint(20) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `keterangan` text,
  `tanggal` date DEFAULT NULL,
  `status_print` enum('sudah','belum') DEFAULT 'belum',
  `adjustment` enum('ya','tidak') DEFAULT 'tidak',
  `status_pakai` enum('pakai','belum') DEFAULT 'belum'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` bigint(20) NOT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `id_jabatan`, `nama`, `username`, `email`, `password`) VALUES
(10, 5, 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id_perusahaan` int(11) NOT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  `alamat` text,
  `kota` varchar(100) DEFAULT NULL,
  `no_telepon` varchar(20) DEFAULT NULL,
  `no_fax` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id_perusahaan`, `nama_perusahaan`, `owner`, `alamat`, `kota`, `no_telepon`, `no_fax`) VALUES
(1, 'LUSHENTEX', '-', 'JALAN ANYAR RAYA NO. 8\r\nRT 07 / RW 10\r\nJELAMBAR - JAKARTA BARAT', 'DKI JAKARTA', '021', '021');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `no_po` varchar(255) NOT NULL,
  `kode_supplier` varchar(10) DEFAULT NULL,
  `no_so` varchar(255) DEFAULT NULL,
  `id_langganan` bigint(20) DEFAULT NULL,
  `id_sub_langganan` bigint(20) DEFAULT NULL,
  `kode_barang` varchar(255) DEFAULT NULL,
  `no_sc` varchar(100) DEFAULT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `lebar` varchar(50) DEFAULT NULL,
  `gramasi` varchar(50) DEFAULT NULL,
  `hand_feel` varchar(50) DEFAULT NULL,
  `jumlah_roll` double DEFAULT NULL,
  `jumlah_kg` double DEFAULT NULL,
  `with_so` enum('ya','tidak') DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `no_rekening` varchar(50) DEFAULT NULL,
  `atas_nama` varchar(50) DEFAULT NULL,
  `id_sub_supplier` bigint(20) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `no_adjustment` bigint(20) NOT NULL,
  `adjustment` double DEFAULT NULL,
  `alasan_adjustment` text,
  `umur` int(11) DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`no_po`, `kode_supplier`, `no_so`, `id_langganan`, `id_sub_langganan`, `kode_barang`, `no_sc`, `nama_barang`, `lebar`, `gramasi`, `hand_feel`, `jumlah_roll`, `jumlah_kg`, `with_so`, `tanggal`, `status`, `bank`, `no_rekening`, `atas_nama`, `id_sub_supplier`, `quantity`, `no_adjustment`, `adjustment`, `alasan_adjustment`, `umur`, `jatuh_tempo`, `created_at`) VALUES
('CDRPO-00002', 'CDR-0004', 'SO00001', NULL, NULL, '2A', '', 'PE 20\'S SK', '36', '190-200', 'seperti contoh', 1, 25, 'ya', '2019-03-10', 'aktif', 'BCA', '00003333', 'CHANDRA', 26, 40, 91, NULL, NULL, 30, '2019-04-09', '2019-03-09 18:20:17'),
('MBTPO-00003', 'MBT-0003', '', 170, 201, '5A', '', 'PE 30\'S SK', '36', '140-150', 'soft', 1, 25, 'tidak', '2019-03-10', 'aktif', 'BCA', '00002222', 'MBTEX', 25, 24, 92, NULL, NULL, 30, '2019-04-09', '2019-03-09 18:21:21'),
('MMTPO-00001', 'MMT-0002', '', 171, 202, '6A', '', 'SPANDEX PE', '72', '170-180', 'A', 1, 25, 'tidak', '2019-03-10', 'aktif', 'BCA', '00001111', 'LIEM LIENA', 24, 72, 90, NULL, NULL, 30, '2019-04-09', '2019-03-09 18:19:30');

-- --------------------------------------------------------

--
-- Table structure for table `retur_pembelian`
--

CREATE TABLE `retur_pembelian` (
  `no_retur_beli` varchar(255) NOT NULL,
  `no_bpb` varchar(255) DEFAULT NULL,
  `kode_barang` varchar(255) DEFAULT NULL,
  `jenis_retur` enum('perbaikan','putus') DEFAULT NULL,
  `packing` varchar(50) DEFAULT NULL,
  `gudang` varchar(50) DEFAULT NULL,
  `no_pol` varchar(15) DEFAULT NULL,
  `supir` varchar(50) DEFAULT NULL,
  `nota_angkutan` varchar(50) DEFAULT NULL,
  `alasan_retur` text,
  `kendaraan_pengirim` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `gramasi` varchar(50) DEFAULT NULL,
  `lebar` varchar(50) DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jatuh_tempo` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retur_pembelian`
--

INSERT INTO `retur_pembelian` (`no_retur_beli`, `no_bpb`, `kode_barang`, `jenis_retur`, `packing`, `gudang`, `no_pol`, `supir`, `nota_angkutan`, `alasan_retur`, `kendaraan_pengirim`, `keterangan`, `gramasi`, `lebar`, `status`, `tanggal`, `jatuh_tempo`, `created_at`) VALUES
('RTCDR00001', 'BPB00001', '2A', 'perbaikan', '-', '-', '-', '-', '-', 'kotor', '-', 'kotor', '190-200', '36', 'aktif', '2019-03-10', '30', '2019-03-09 18:30:57');

-- --------------------------------------------------------

--
-- Table structure for table `retur_penjualan`
--

CREATE TABLE `retur_penjualan` (
  `no_retur_jual` varchar(255) NOT NULL,
  `no_sj` varchar(255) DEFAULT NULL,
  `komisi` double DEFAULT NULL,
  `up_sales` double DEFAULT NULL,
  `n_barang` varchar(50) DEFAULT NULL,
  `a_retur` varchar(50) DEFAULT NULL,
  `lebar` varchar(50) DEFAULT NULL,
  `gramasi` varchar(50) DEFAULT NULL,
  `gudang` varchar(50) DEFAULT NULL,
  `packing` varchar(50) DEFAULT NULL,
  `supir` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `tanggal` date DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT 'aktif',
  `kendaraan` char(50) DEFAULT NULL,
  `nopol` char(10) DEFAULT NULL,
  `nota_angkutan` char(50) DEFAULT NULL,
  `status_print` enum('ya','tidak') DEFAULT 'tidak',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retur_penjualan`
--

INSERT INTO `retur_penjualan` (`no_retur_jual`, `no_sj`, `komisi`, `up_sales`, `n_barang`, `a_retur`, `lebar`, `gramasi`, `gudang`, `packing`, `supir`, `keterangan`, `tanggal`, `status`, `kendaraan`, `nopol`, `nota_angkutan`, `status_print`, `created_at`) VALUES
('RT00001', 'LST-00001', 0, 0, 'PE 20\'S SK', '', '36', '190-200', '', '', '', '', '2019-03-29', 'aktif', '', '', '', 'ya', '2019-03-28 19:42:24');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id_sales` bigint(20) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `telepon` varchar(20) DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT 'aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id_sales`, `nama`, `alamat`, `telepon`, `status`) VALUES
(65, 'AHMAD', 'JAKANRA', '-', 'aktif'),
(66, 'BUBUN', 'JAKARTA', '-', 'aktif'),
(67, 'CEPI', 'JAKARTA', '-', 'aktif'),
(68, 'DUDUNG', 'JAKARTA', '-', 'aktif'),
(69, 'LST', 'JAKARTA', '-', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE `sales_order` (
  `no_so` varchar(255) NOT NULL,
  `id_sub_langganan` bigint(20) DEFAULT NULL,
  `id_sales` bigint(20) DEFAULT NULL,
  `kode_barang` varchar(50) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `komisi` double DEFAULT NULL,
  `up_sales` double DEFAULT NULL,
  `jumlah_roll` int(11) DEFAULT NULL,
  `lebar` varchar(50) DEFAULT NULL,
  `gramasi` varchar(50) DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT 'aktif',
  `limit_kredit` double DEFAULT NULL,
  `status_print` enum('ya','tidak') DEFAULT 'tidak'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_order`
--

INSERT INTO `sales_order` (`no_so`, `id_sub_langganan`, `id_sales`, `kode_barang`, `tanggal`, `komisi`, `up_sales`, `jumlah_roll`, `lebar`, `gramasi`, `status`, `limit_kredit`, `status_print`) VALUES
('SO00001', 200, 65, '2A', '2019-03-10', 100, 325, 40, '36', '190-200', 'aktif', 200000000, 'ya');

-- --------------------------------------------------------

--
-- Table structure for table `sj_penjualan`
--

CREATE TABLE `sj_penjualan` (
  `no_sj` varchar(255) NOT NULL,
  `id_sub_langganan` bigint(20) DEFAULT NULL,
  `packing` varchar(50) DEFAULT NULL,
  `gudang` varchar(50) DEFAULT NULL,
  `no_pol` varchar(15) DEFAULT NULL,
  `supir` varchar(50) DEFAULT NULL,
  `kendaraan_pengirim` varchar(50) DEFAULT NULL,
  `nota_angkutan` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `tanggal` date DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT NULL,
  `id_sales` bigint(20) DEFAULT NULL,
  `nama_sales` varchar(100) DEFAULT NULL,
  `komisi_sales` double DEFAULT NULL,
  `up_sales` double DEFAULT NULL,
  `lebar` varchar(50) DEFAULT NULL,
  `gramasi` varchar(50) DEFAULT NULL,
  `foot_total_roll` double DEFAULT NULL,
  `foot_total_kg` double DEFAULT NULL,
  `foot_total_harga` varchar(20) DEFAULT NULL,
  `dengan_sales` enum('ya','tidak') DEFAULT NULL,
  `print` enum('ya','tidak') DEFAULT 'tidak',
  `nama_barang` varchar(255) DEFAULT NULL,
  `limit_kredit` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sj_penjualan`
--

INSERT INTO `sj_penjualan` (`no_sj`, `id_sub_langganan`, `packing`, `gudang`, `no_pol`, `supir`, `kendaraan_pengirim`, `nota_angkutan`, `keterangan`, `tanggal`, `jatuh_tempo`, `status`, `id_sales`, `nama_sales`, `komisi_sales`, `up_sales`, `lebar`, `gramasi`, `foot_total_roll`, `foot_total_kg`, `foot_total_harga`, `dengan_sales`, `print`, `nama_barang`, `limit_kredit`) VALUES
('LST-00001', 201, '-', '-', '-', '-', '-', '-', '-', '2019-03-10', '2019-04-09', 'aktif', 68, 'DUDUNG', 450, 0, '36', '190-200', 4, 98, '4214000', 'ya', 'ya', 'PE 20\'S SK', 400000000),
('LST-00002', 201, '-', '-', '-', '-', '-', '-', '', '2019-03-12', '2019-04-11', 'aktif', 66, 'BUBUN', 250, 1000, '36', '190-200', 2, 47, '86310', 'ya', 'tidak', 'PE 20\'S SK', 395786000);

-- --------------------------------------------------------

--
-- Table structure for table `sub_bkkb`
--

CREATE TABLE `sub_bkkb` (
  `id_sub_bkkb` bigint(20) NOT NULL,
  `no_bkkb` varchar(50) DEFAULT NULL,
  `status_input` enum('kbb','bkkb') DEFAULT NULL,
  `no_kontra_beli` varchar(255) DEFAULT NULL,
  `no_bkkb_foreign` varchar(50) DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `total` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_bkkb`
--

INSERT INTO `sub_bkkb` (`id_sub_bkkb`, `no_bkkb`, `status_input`, `no_kontra_beli`, `no_bkkb_foreign`, `jatuh_tempo`, `total`) VALUES
(16, 'BKKB-00001', 'kbb', 'KBB-00001', NULL, '2019-06-08', 18640000);

-- --------------------------------------------------------

--
-- Table structure for table `sub_bkkb_nota`
--

CREATE TABLE `sub_bkkb_nota` (
  `no_nota_debet` varchar(255) DEFAULT NULL,
  `no_bkkb` varchar(50) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `no_po` varchar(255) DEFAULT NULL,
  `nama_supplier` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_bpb_pembelian`
--

CREATE TABLE `sub_bpb_pembelian` (
  `no_bpb` varchar(255) DEFAULT NULL,
  `warna` varchar(255) DEFAULT NULL,
  `p_roll` double DEFAULT NULL,
  `p_kg` double DEFAULT NULL,
  `t_roll` double DEFAULT NULL,
  `t_kg` double DEFAULT NULL,
  `s_roll` double DEFAULT NULL,
  `s_kg` double DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `id_sub_po` bigint(20) DEFAULT NULL,
  `saldo_roll` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_bpb_pembelian`
--

INSERT INTO `sub_bpb_pembelian` (`no_bpb`, `warna`, `p_roll`, `p_kg`, `t_roll`, `t_kg`, `s_roll`, `s_kg`, `harga`, `total`, `id_sub_po`, `saldo_roll`) VALUES
('BPB00001', 'PUTIH', 40, 1000, 20, 490, 20, 0, 40000, 19600000, 246, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_bpkb`
--

CREATE TABLE `sub_bpkb` (
  `id_sub_bpkb` bigint(20) NOT NULL,
  `no_bpkb` varchar(50) DEFAULT NULL,
  `status_input` enum('kbj','bpkb') DEFAULT NULL,
  `no_kontra_jual` varchar(255) DEFAULT NULL,
  `no_bpkb_foreign` varchar(50) DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `total` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_bpkb`
--

INSERT INTO `sub_bpkb` (`id_sub_bpkb`, `no_bpkb`, `status_input`, `no_kontra_jual`, `no_bpkb_foreign`, `jatuh_tempo`, `total`) VALUES
(20, 'BPKB-00001', 'kbj', 'KBJ-00001', NULL, '2019-04-09', 4214000);

-- --------------------------------------------------------

--
-- Table structure for table `sub_bpkb_nota`
--

CREATE TABLE `sub_bpkb_nota` (
  `no_nota_kredit` varchar(255) DEFAULT NULL,
  `no_bpkb` varchar(50) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `no_sj` varchar(255) DEFAULT NULL,
  `nama_langganan` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_kontra_beli`
--

CREATE TABLE `sub_kontra_beli` (
  `no_kontra_beli` varchar(255) DEFAULT NULL,
  `no_po` varchar(255) DEFAULT NULL,
  `no_bpb` varchar(255) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `no_sj_beli` varchar(255) DEFAULT NULL,
  `jatuh_tempo` varchar(255) DEFAULT NULL,
  `nama_barang` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_kontra_beli`
--

INSERT INTO `sub_kontra_beli` (`no_kontra_beli`, `no_po`, `no_bpb`, `total`, `no_sj_beli`, `jatuh_tempo`, `nama_barang`) VALUES
('KBB-00001', 'CDRPO-00002', 'BPB00001', 18640000, 'CDR-0001', '2019-06-08', 'PE 20');

-- --------------------------------------------------------

--
-- Table structure for table `sub_kontra_jual`
--

CREATE TABLE `sub_kontra_jual` (
  `no_kontra_jual` varchar(255) DEFAULT NULL,
  `id_sub_sj_penjualan` bigint(20) DEFAULT NULL,
  `no_bpb` varchar(255) DEFAULT NULL,
  `id_sales` bigint(20) DEFAULT NULL,
  `nama_sales` varchar(255) DEFAULT NULL,
  `komisi_sales` double DEFAULT NULL,
  `up_sales` double DEFAULT NULL,
  `jumlah_roll` double DEFAULT NULL,
  `jumlah_kg` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `jatuh_tempo` varchar(255) DEFAULT NULL,
  `tgl_sj` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_kontra_jual`
--

INSERT INTO `sub_kontra_jual` (`no_kontra_jual`, `id_sub_sj_penjualan`, `no_bpb`, `id_sales`, `nama_sales`, `komisi_sales`, `up_sales`, `jumlah_roll`, `jumlah_kg`, `total`, `jatuh_tempo`, `tgl_sj`) VALUES
('KBJ-00001', 83, 'BPB00001', 68, 'DUDUNG', 450, 0, 4, 2500, 4214000, '2019-04-09', '2019-03-10');

-- --------------------------------------------------------

--
-- Table structure for table `sub_langganan`
--

CREATE TABLE `sub_langganan` (
  `id_sub_langganan` bigint(20) NOT NULL,
  `id_langganan` bigint(20) DEFAULT NULL,
  `nama_pic` varchar(100) DEFAULT NULL,
  `alamat` text,
  `kota` varchar(50) DEFAULT NULL,
  `no_telepon` varchar(30) DEFAULT NULL,
  `nama_cmt` varchar(100) DEFAULT NULL,
  `alamat_cmt` text,
  `ppn` enum('ya','tidak') DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_langganan`
--

INSERT INTO `sub_langganan` (`id_sub_langganan`, `id_langganan`, `nama_pic`, `alamat`, `kota`, `no_telepon`, `nama_cmt`, `alamat_cmt`, `ppn`, `keterangan`) VALUES
(200, 169, 'GUNAWAN', 'JAKARTA', 'JAKARTA', '-', '-', '-', 'tidak', '-'),
(201, 170, 'GO GREEN', 'JAKARTA', 'JAKARTA', '-', '-', '-', 'tidak', '-'),
(202, 171, 'LST', 'DUMMY', 'DUMMY', '-', '-', '-', 'tidak', '-');

-- --------------------------------------------------------

--
-- Table structure for table `sub_perusahaan`
--

CREATE TABLE `sub_perusahaan` (
  `id_sub_perusahaan` int(11) NOT NULL,
  `id_perusahaan` int(11) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `no_rekening` varchar(30) DEFAULT NULL,
  `atas_nama` varchar(100) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_perusahaan`
--

INSERT INTO `sub_perusahaan` (`id_sub_perusahaan`, `id_perusahaan`, `bank`, `no_rekening`, `atas_nama`, `keterangan`) VALUES
(3, 1, 'BCA', '5920737333', 'IKE YOVITA', '');

-- --------------------------------------------------------

--
-- Table structure for table `sub_po`
--

CREATE TABLE `sub_po` (
  `id_sub_po` bigint(20) NOT NULL,
  `no_po` varchar(255) DEFAULT NULL,
  `warna` varchar(50) DEFAULT NULL,
  `kode_warna` varchar(50) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `jumlah_roll` double DEFAULT NULL,
  `jumlah_kg` double DEFAULT NULL,
  `keterangan` text,
  `sisa_roll` double DEFAULT NULL,
  `adjustment` double DEFAULT NULL,
  `stok` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_po`
--

INSERT INTO `sub_po` (`id_sub_po`, `no_po`, `warna`, `kode_warna`, `harga`, `jumlah_roll`, `jumlah_kg`, `keterangan`, `sisa_roll`, `adjustment`, `stok`) VALUES
(240, 'MMTPO-00001', 'PUTIH', '-', 45000, 12, 300, '-', 12, NULL, NULL),
(241, 'MMTPO-00001', 'HITAM', '-', 47000, 12, 300, '-', 12, NULL, NULL),
(242, 'MMTPO-00001', 'NAVY', '-', 47000, 12, 300, '-', 12, NULL, NULL),
(243, 'MMTPO-00001', 'KUBUS', '-', 46000, 12, 300, '-', 12, NULL, NULL),
(244, 'MMTPO-00001', 'PINK', '-', 45000, 12, 300, '-', 12, NULL, NULL),
(245, 'MMTPO-00001', 'SALEM', '-', 45000, 12, 300, '-', 12, NULL, NULL),
(246, 'CDRPO-00002', 'PUTIH', 'PUTIHNETRAL', 40000, 40, 1000, '-', 20, NULL, 15),
(247, 'MBTPO-00003', 'M71', 'm71', 32500, 12, 300, '-', 12, NULL, NULL),
(248, 'MBTPO-00003', 'm81', 'm81', 34500, 12, 300, '-', 12, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_retur_pembelian`
--

CREATE TABLE `sub_retur_pembelian` (
  `id_sub_po` bigint(20) DEFAULT NULL,
  `no_retur_beli` varchar(255) DEFAULT NULL,
  `warna` varchar(255) DEFAULT NULL,
  `t_roll` double DEFAULT NULL,
  `t_kg` double DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `roll_retur` double DEFAULT NULL,
  `kg_retur` varchar(100) DEFAULT NULL,
  `total_kg_retur` double DEFAULT NULL,
  `total_harga_retur` double DEFAULT NULL,
  `id_sales` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_retur_pembelian`
--

INSERT INTO `sub_retur_pembelian` (`id_sub_po`, `no_retur_beli`, `warna`, `t_roll`, `t_kg`, `harga`, `roll_retur`, `kg_retur`, `total_kg_retur`, `total_harga_retur`, `id_sales`) VALUES
(246, 'RTCDR00001', 'PUTIH', 16, 490, 40000, 1, '2400', 24, 960000, 65);

-- --------------------------------------------------------

--
-- Table structure for table `sub_retur_penjualan`
--

CREATE TABLE `sub_retur_penjualan` (
  `warna` char(50) DEFAULT NULL,
  `no_retur_jual` varchar(255) DEFAULT NULL,
  `no_po` varchar(255) DEFAULT NULL,
  `id_sub_po` bigint(20) DEFAULT NULL,
  `jumlah_roll` double DEFAULT NULL,
  `jumlah_kg` double DEFAULT NULL,
  `retur_roll` double DEFAULT NULL,
  `retur_kg` text,
  `total_kg_retur` double DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `total` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_retur_penjualan`
--

INSERT INTO `sub_retur_penjualan` (`warna`, `no_retur_jual`, `no_po`, `id_sub_po`, `jumlah_roll`, `jumlah_kg`, `retur_roll`, `retur_kg`, `total_kg_retur`, `harga`, `total`) VALUES
('PUTIH', 'RT00001', 'CDRPO-00002', 246, 4, 2500, 2, '50', 0, 43000, 2150000);

-- --------------------------------------------------------

--
-- Table structure for table `sub_sales`
--

CREATE TABLE `sub_sales` (
  `id_sub_sales` bigint(20) NOT NULL,
  `id_sales` bigint(20) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `no_rekening` varchar(30) DEFAULT NULL,
  `atas_nama` varchar(100) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_sales`
--

INSERT INTO `sub_sales` (`id_sub_sales`, `id_sales`, `bank`, `no_rekening`, `atas_nama`, `keterangan`) VALUES
(69, 65, 'BCA', '11111111', 'AHMAD', '-'),
(70, 66, 'BCA', '22222222', 'BUBUN', '-'),
(71, 67, 'BCA', '33333333', 'CECEP', '-'),
(72, 68, 'BCA', '44444444', 'DUDUNG', '-'),
(73, 69, '-', '0', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sj_penjualan`
--

CREATE TABLE `sub_sj_penjualan` (
  `id_sub_sj_penjualan` bigint(20) NOT NULL,
  `no_sj` varchar(255) DEFAULT NULL,
  `no_po` varchar(255) DEFAULT NULL,
  `no_bpb` varchar(255) DEFAULT NULL,
  `no_sj_beli` varchar(255) DEFAULT NULL,
  `warna` varchar(100) DEFAULT NULL,
  `jumlah_roll` double DEFAULT NULL,
  `jumlah_kg` text,
  `total_kg` double DEFAULT NULL,
  `harga_jual` double DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `id_sub_po` bigint(20) DEFAULT NULL,
  `total_sisa_roll` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_sj_penjualan`
--

INSERT INTO `sub_sj_penjualan` (`id_sub_sj_penjualan`, `no_sj`, `no_po`, `no_bpb`, `no_sj_beli`, `warna`, `jumlah_roll`, `jumlah_kg`, `total_kg`, `harga_jual`, `harga_beli`, `total`, `id_sub_po`, `total_sisa_roll`) VALUES
(83, 'LST-00001', 'CDRPO-00002', 'BPB00001', 'CDR-0001', 'PUTIH', 4, '2500 2400 2500 2400', 98, 43000, 40000, 4214000, 246, 16),
(84, 'LST-00002', 'CDRPO-00002', 'BPB00001', 'CDR-0001', 'PUTIH', 2, '2290 2505', 47.95, 1800, 40000, 86310, 246, 13);

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_langganan`
--

CREATE TABLE `sub_sub_langganan` (
  `id_sub_sub` bigint(20) NOT NULL,
  `id_sub_langganan` bigint(20) DEFAULT NULL,
  `komisi` double DEFAULT NULL,
  `id_sales` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_sub_langganan`
--

INSERT INTO `sub_sub_langganan` (`id_sub_sub`, `id_sub_langganan`, `komisi`, `id_sales`) VALUES
(553, 202, 0, 69),
(554, 200, 300, 67),
(555, 200, 200, 66),
(556, 200, 100, 65),
(557, 201, 450, 68),
(558, 201, 250, 66);

-- --------------------------------------------------------

--
-- Table structure for table `sub_supplier`
--

CREATE TABLE `sub_supplier` (
  `id_sub_supplier` bigint(20) NOT NULL,
  `kode_supplier` varchar(15) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `no_rekening` varchar(50) DEFAULT NULL,
  `atas_nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_supplier`
--

INSERT INTO `sub_supplier` (`id_sub_supplier`, `kode_supplier`, `bank`, `no_rekening`, `atas_nama`) VALUES
(24, 'MMT-0002', 'BCA', '00001111', 'LIEM LIENA'),
(25, 'MBT-0003', 'BCA', '00002222', 'MBTEX'),
(26, 'CDR-0004', 'BCA', '00003333', 'CHANDRA');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(15) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` text,
  `no_telepon` varchar(30) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `pic` varchar(50) DEFAULT NULL,
  `no_telepon_pic` varchar(30) DEFAULT NULL,
  `ppn` enum('PPn','Tidak') DEFAULT NULL,
  `jatuh_tempo` varchar(50) DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT 'aktif',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama`, `alamat`, `no_telepon`, `kota`, `pic`, `no_telepon_pic`, `ppn`, `jatuh_tempo`, `status`, `created_at`) VALUES
('AD-0000', 'EGGY SUDIANTO', 'JALAN HOLIS GANG CIBUNTU BARAT BLOK TOGE NO 242', '087821948861', 'BANDUNG', 'WEWQ', 'QWE', 'Tidak', '30', 'tidak aktif', '2019-03-03 19:53:50'),
('CDR-0004', 'CITRA DAMAI REJEKITEX', 'BANDUNG', '022-44444444', 'BANDUNG', 'KO CHANDRA', '022-44444444', 'Tidak', '90', 'aktif', '2019-03-09 18:12:11'),
('MBT-0003', 'MB-TEX', 'BANDUNG', '022-33333333', 'BANDUNG', 'CIK MAIDA', '022-33333333', 'Tidak', '90', 'aktif', '2019-03-09 18:11:24'),
('MMT-0002', 'MENTARI MANDIRI', 'BANDUNG', '022-22222222', 'BANDUNG', 'EBET', '022-22222222', 'Tidak', '90', 'aktif', '2019-03-09 18:10:36'),
('QW-0001', 'EGGY SUDIANTO', 'JALAN HOLIS GANG CIBUNTU BARAT BLOK TOGE NO 242', '087821948861', 'BANDUNG', 'WEWQ', 'QWE', 'Tidak', '30', 'tidak aktif', '2019-03-03 19:54:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `bkkb`
--
ALTER TABLE `bkkb`
  ADD PRIMARY KEY (`no_bkkb`),
  ADD KEY `id_sub_perusahaan` (`id_sub_perusahaan`),
  ADD KEY `kode_supplier` (`kode_supplier`);

--
-- Indexes for table `bpb_pembelian`
--
ALTER TABLE `bpb_pembelian`
  ADD PRIMARY KEY (`no_bpb`),
  ADD KEY `bpb_pembelian_ibfk_1` (`no_po`);

--
-- Indexes for table `bpkb`
--
ALTER TABLE `bpkb`
  ADD PRIMARY KEY (`no_bpkb`),
  ADD KEY `id_sub_perusahaan` (`id_sub_perusahaan`),
  ADD KEY `kode_supplier` (`id_langganan`);

--
-- Indexes for table `detail_sales_order`
--
ALTER TABLE `detail_sales_order`
  ADD PRIMARY KEY (`id_detail_sales_order`),
  ADD KEY `no_so` (`no_so`);

--
-- Indexes for table `hak_akses`
--
ALTER TABLE `hak_akses`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `hasil_komisi`
--
ALTER TABLE `hasil_komisi`
  ADD PRIMARY KEY (`id_hasil_komisi`),
  ADD KEY `no_sj` (`no_sj`),
  ADD KEY `no_retur_jual` (`no_retur_jual`),
  ADD KEY `id_sales` (`id_sales`);

--
-- Indexes for table `komisi`
--
ALTER TABLE `komisi`
  ADD PRIMARY KEY (`id_komisi`),
  ADD KEY `id_sales` (`id_sales`);

--
-- Indexes for table `kontra_beli`
--
ALTER TABLE `kontra_beli`
  ADD PRIMARY KEY (`no_kontra_beli`),
  ADD KEY `kode_supplier` (`kode_supplier`);

--
-- Indexes for table `kontra_jual`
--
ALTER TABLE `kontra_jual`
  ADD PRIMARY KEY (`no_kontra_jual`),
  ADD KEY `id_langganan` (`id_langganan`),
  ADD KEY `id_sub_perusahaan` (`id_sub_perusahaan`);

--
-- Indexes for table `langganan`
--
ALTER TABLE `langganan`
  ADD PRIMARY KEY (`id_langganan`);

--
-- Indexes for table `nota_debet`
--
ALTER TABLE `nota_debet`
  ADD PRIMARY KEY (`no_nota_debet`),
  ADD KEY `nota_debet_ibfk_1` (`no_po`),
  ADD KEY `kode_supplier` (`kode_supplier`);

--
-- Indexes for table `nota_kredit`
--
ALTER TABLE `nota_kredit`
  ADD PRIMARY KEY (`no_nota_kredit`),
  ADD KEY `nota_kredit_ibfk_1` (`no_sj`),
  ADD KEY `nota_kredit_ibfk_2` (`id_langganan`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`no_po`),
  ADD KEY `purchase_order_ibfk_4` (`kode_barang`),
  ADD KEY `purchase_order_ibfk_5` (`no_so`),
  ADD KEY `kode_supplier` (`kode_supplier`),
  ADD KEY `no_adjustment` (`no_adjustment`);

--
-- Indexes for table `retur_pembelian`
--
ALTER TABLE `retur_pembelian`
  ADD PRIMARY KEY (`no_retur_beli`),
  ADD KEY `retur_pembelian_ibfk_1` (`no_bpb`),
  ADD KEY `retur_pembelian_ibfk_2` (`kode_barang`);

--
-- Indexes for table `retur_penjualan`
--
ALTER TABLE `retur_penjualan`
  ADD PRIMARY KEY (`no_retur_jual`),
  ADD KEY `retur_penjualan_ibfk_1` (`no_sj`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id_sales`);

--
-- Indexes for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`no_so`),
  ADD KEY `sales_order_ibfk_2` (`id_sales`),
  ADD KEY `sales_order_ibfk_3` (`kode_barang`),
  ADD KEY `id_sub_langganan` (`id_sub_langganan`);

--
-- Indexes for table `sj_penjualan`
--
ALTER TABLE `sj_penjualan`
  ADD PRIMARY KEY (`no_sj`),
  ADD KEY `sj_penjualan_ibfk_1` (`id_sub_langganan`),
  ADD KEY `id_sales` (`id_sales`);

--
-- Indexes for table `sub_bkkb`
--
ALTER TABLE `sub_bkkb`
  ADD PRIMARY KEY (`id_sub_bkkb`),
  ADD KEY `no_kontra_beli` (`no_kontra_beli`),
  ADD KEY `no_bkkb_foreign` (`no_bkkb_foreign`),
  ADD KEY `sub_bkkb_ibfk_1` (`no_bkkb`);

--
-- Indexes for table `sub_bkkb_nota`
--
ALTER TABLE `sub_bkkb_nota`
  ADD KEY `nota_bkkb_ibfk_1` (`no_nota_debet`),
  ADD KEY `nota_bkkb_ibfk_2` (`no_bkkb`),
  ADD KEY `nama_supplier` (`nama_supplier`);

--
-- Indexes for table `sub_bpb_pembelian`
--
ALTER TABLE `sub_bpb_pembelian`
  ADD KEY `no_bpb` (`no_bpb`),
  ADD KEY `id_sub_po` (`id_sub_po`);

--
-- Indexes for table `sub_bpkb`
--
ALTER TABLE `sub_bpkb`
  ADD PRIMARY KEY (`id_sub_bpkb`),
  ADD KEY `no_kontra_beli` (`no_kontra_jual`),
  ADD KEY `no_bkkb_foreign` (`no_bpkb_foreign`),
  ADD KEY `sub_bkkb_ibfk_1` (`no_bpkb`);

--
-- Indexes for table `sub_bpkb_nota`
--
ALTER TABLE `sub_bpkb_nota`
  ADD KEY `nota_bkkb_ibfk_1` (`no_nota_kredit`),
  ADD KEY `nota_bkkb_ibfk_2` (`no_bpkb`),
  ADD KEY `nama_supplier` (`nama_langganan`),
  ADD KEY `sub_bpkb_nota_ibfk_3` (`no_sj`);

--
-- Indexes for table `sub_kontra_beli`
--
ALTER TABLE `sub_kontra_beli`
  ADD KEY `no_kontra_beli` (`no_kontra_beli`),
  ADD KEY `no_bpb` (`no_bpb`);

--
-- Indexes for table `sub_kontra_jual`
--
ALTER TABLE `sub_kontra_jual`
  ADD KEY `no_kontra_jual` (`no_kontra_jual`),
  ADD KEY `no_bpb` (`no_bpb`),
  ADD KEY `id_sub_sj_penjualan` (`id_sub_sj_penjualan`),
  ADD KEY `sub_kontra_jual_ibfk_4` (`id_sales`);

--
-- Indexes for table `sub_langganan`
--
ALTER TABLE `sub_langganan`
  ADD PRIMARY KEY (`id_sub_langganan`),
  ADD KEY `id_langganan` (`id_langganan`);

--
-- Indexes for table `sub_perusahaan`
--
ALTER TABLE `sub_perusahaan`
  ADD PRIMARY KEY (`id_sub_perusahaan`),
  ADD KEY `id_perusahaan` (`id_perusahaan`);

--
-- Indexes for table `sub_po`
--
ALTER TABLE `sub_po`
  ADD PRIMARY KEY (`id_sub_po`),
  ADD KEY `no_po` (`no_po`);

--
-- Indexes for table `sub_retur_pembelian`
--
ALTER TABLE `sub_retur_pembelian`
  ADD KEY `no_retur_beli` (`no_retur_beli`),
  ADD KEY `id_sub_po` (`warna`),
  ADD KEY `id_sales` (`id_sales`);

--
-- Indexes for table `sub_retur_penjualan`
--
ALTER TABLE `sub_retur_penjualan`
  ADD KEY `no_retur_jual` (`no_retur_jual`),
  ADD KEY `no_bpb` (`no_po`);

--
-- Indexes for table `sub_sales`
--
ALTER TABLE `sub_sales`
  ADD PRIMARY KEY (`id_sub_sales`),
  ADD KEY `id_sales` (`id_sales`);

--
-- Indexes for table `sub_sj_penjualan`
--
ALTER TABLE `sub_sj_penjualan`
  ADD PRIMARY KEY (`id_sub_sj_penjualan`),
  ADD KEY `no_sj` (`no_sj`),
  ADD KEY `no_bpb` (`no_po`),
  ADD KEY `no_bpb_2` (`no_bpb`);

--
-- Indexes for table `sub_sub_langganan`
--
ALTER TABLE `sub_sub_langganan`
  ADD PRIMARY KEY (`id_sub_sub`),
  ADD KEY `sub_sub_langganan_ibfk_1` (`id_sub_langganan`),
  ADD KEY `id_sales` (`id_sales`);

--
-- Indexes for table `sub_supplier`
--
ALTER TABLE `sub_supplier`
  ADD PRIMARY KEY (`id_sub_supplier`),
  ADD KEY `kode_supplier` (`kode_supplier`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_sales_order`
--
ALTER TABLE `detail_sales_order`
  MODIFY `id_detail_sales_order` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `hak_akses`
--
ALTER TABLE `hak_akses`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hasil_komisi`
--
ALTER TABLE `hasil_komisi`
  MODIFY `id_hasil_komisi` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `komisi`
--
ALTER TABLE `komisi`
  MODIFY `id_komisi` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT for table `langganan`
--
ALTER TABLE `langganan`
  MODIFY `id_langganan` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `no_adjustment` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id_sales` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `sub_bkkb`
--
ALTER TABLE `sub_bkkb`
  MODIFY `id_sub_bkkb` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sub_bpkb`
--
ALTER TABLE `sub_bpkb`
  MODIFY `id_sub_bpkb` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `sub_langganan`
--
ALTER TABLE `sub_langganan`
  MODIFY `id_sub_langganan` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `sub_perusahaan`
--
ALTER TABLE `sub_perusahaan`
  MODIFY `id_sub_perusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_po`
--
ALTER TABLE `sub_po`
  MODIFY `id_sub_po` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `sub_sales`
--
ALTER TABLE `sub_sales`
  MODIFY `id_sub_sales` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `sub_sj_penjualan`
--
ALTER TABLE `sub_sj_penjualan`
  MODIFY `id_sub_sj_penjualan` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `sub_sub_langganan`
--
ALTER TABLE `sub_sub_langganan`
  MODIFY `id_sub_sub` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=559;

--
-- AUTO_INCREMENT for table `sub_supplier`
--
ALTER TABLE `sub_supplier`
  MODIFY `id_sub_supplier` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bkkb`
--
ALTER TABLE `bkkb`
  ADD CONSTRAINT `bkkb_ibfk_1` FOREIGN KEY (`id_sub_perusahaan`) REFERENCES `sub_perusahaan` (`id_sub_perusahaan`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `bkkb_ibfk_2` FOREIGN KEY (`kode_supplier`) REFERENCES `supplier` (`kode_supplier`) ON UPDATE NO ACTION;

--
-- Constraints for table `bpb_pembelian`
--
ALTER TABLE `bpb_pembelian`
  ADD CONSTRAINT `bpb_pembelian_ibfk_1` FOREIGN KEY (`no_po`) REFERENCES `purchase_order` (`no_po`) ON UPDATE CASCADE;

--
-- Constraints for table `bpkb`
--
ALTER TABLE `bpkb`
  ADD CONSTRAINT `bpkb_ibfk_1` FOREIGN KEY (`id_langganan`) REFERENCES `langganan` (`id_langganan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bpkb_ibfk_2` FOREIGN KEY (`id_sub_perusahaan`) REFERENCES `sub_perusahaan` (`id_sub_perusahaan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `detail_sales_order`
--
ALTER TABLE `detail_sales_order`
  ADD CONSTRAINT `detail_sales_order_ibfk_1` FOREIGN KEY (`no_so`) REFERENCES `sales_order` (`no_so`);

--
-- Constraints for table `hasil_komisi`
--
ALTER TABLE `hasil_komisi`
  ADD CONSTRAINT `hasil_komisi_ibfk_1` FOREIGN KEY (`no_sj`) REFERENCES `sj_penjualan` (`no_sj`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hasil_komisi_ibfk_2` FOREIGN KEY (`no_retur_jual`) REFERENCES `retur_penjualan` (`no_retur_jual`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hasil_komisi_ibfk_3` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON UPDATE CASCADE;

--
-- Constraints for table `komisi`
--
ALTER TABLE `komisi`
  ADD CONSTRAINT `komisi_ibfk_1` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kontra_beli`
--
ALTER TABLE `kontra_beli`
  ADD CONSTRAINT `kontra_beli_ibfk_2` FOREIGN KEY (`kode_supplier`) REFERENCES `supplier` (`kode_supplier`) ON UPDATE CASCADE;

--
-- Constraints for table `kontra_jual`
--
ALTER TABLE `kontra_jual`
  ADD CONSTRAINT `kontra_jual_ibfk_2` FOREIGN KEY (`id_langganan`) REFERENCES `langganan` (`id_langganan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `kontra_jual_ibfk_3` FOREIGN KEY (`id_sub_perusahaan`) REFERENCES `sub_perusahaan` (`id_sub_perusahaan`) ON UPDATE CASCADE;

--
-- Constraints for table `nota_debet`
--
ALTER TABLE `nota_debet`
  ADD CONSTRAINT `nota_debet_ibfk_1` FOREIGN KEY (`no_po`) REFERENCES `purchase_order` (`no_po`) ON UPDATE CASCADE,
  ADD CONSTRAINT `nota_debet_ibfk_2` FOREIGN KEY (`kode_supplier`) REFERENCES `supplier` (`kode_supplier`) ON UPDATE CASCADE;

--
-- Constraints for table `nota_kredit`
--
ALTER TABLE `nota_kredit`
  ADD CONSTRAINT `nota_kredit_ibfk_1` FOREIGN KEY (`no_sj`) REFERENCES `sj_penjualan` (`no_sj`) ON UPDATE CASCADE,
  ADD CONSTRAINT `nota_kredit_ibfk_2` FOREIGN KEY (`id_langganan`) REFERENCES `langganan` (`id_langganan`) ON UPDATE CASCADE;

--
-- Constraints for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD CONSTRAINT `pengguna_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `hak_akses` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD CONSTRAINT `purchase_order_ibfk_4` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_ibfk_6` FOREIGN KEY (`kode_supplier`) REFERENCES `supplier` (`kode_supplier`) ON UPDATE CASCADE;

--
-- Constraints for table `retur_pembelian`
--
ALTER TABLE `retur_pembelian`
  ADD CONSTRAINT `retur_pembelian_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`) ON UPDATE CASCADE,
  ADD CONSTRAINT `retur_pembelian_ibfk_3` FOREIGN KEY (`no_bpb`) REFERENCES `bpb_pembelian` (`no_bpb`) ON UPDATE CASCADE;

--
-- Constraints for table `retur_penjualan`
--
ALTER TABLE `retur_penjualan`
  ADD CONSTRAINT `retur_penjualan_ibfk_1` FOREIGN KEY (`no_sj`) REFERENCES `sj_penjualan` (`no_sj`) ON UPDATE CASCADE;

--
-- Constraints for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD CONSTRAINT `sales_order_ibfk_2` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_order_ibfk_3` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_order_ibfk_4` FOREIGN KEY (`id_sub_langganan`) REFERENCES `sub_langganan` (`id_sub_langganan`) ON UPDATE CASCADE;

--
-- Constraints for table `sj_penjualan`
--
ALTER TABLE `sj_penjualan`
  ADD CONSTRAINT `sj_penjualan_ibfk_3` FOREIGN KEY (`id_sub_langganan`) REFERENCES `sub_langganan` (`id_sub_langganan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sj_penjualan_ibfk_4` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_bkkb`
--
ALTER TABLE `sub_bkkb`
  ADD CONSTRAINT `sub_bkkb_ibfk_1` FOREIGN KEY (`no_bkkb`) REFERENCES `bkkb` (`no_bkkb`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_bkkb_ibfk_2` FOREIGN KEY (`no_kontra_beli`) REFERENCES `kontra_beli` (`no_kontra_beli`),
  ADD CONSTRAINT `sub_bkkb_ibfk_3` FOREIGN KEY (`no_bkkb_foreign`) REFERENCES `bkkb` (`no_bkkb`);

--
-- Constraints for table `sub_bkkb_nota`
--
ALTER TABLE `sub_bkkb_nota`
  ADD CONSTRAINT `sub_bkkb_nota_ibfk_1` FOREIGN KEY (`no_nota_debet`) REFERENCES `nota_debet` (`no_nota_debet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `sub_bkkb_nota_ibfk_2` FOREIGN KEY (`no_bkkb`) REFERENCES `bkkb` (`no_bkkb`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_bpb_pembelian`
--
ALTER TABLE `sub_bpb_pembelian`
  ADD CONSTRAINT `sub_bpb_pembelian_ibfk_1` FOREIGN KEY (`no_bpb`) REFERENCES `bpb_pembelian` (`no_bpb`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_bpb_pembelian_ibfk_2` FOREIGN KEY (`id_sub_po`) REFERENCES `sub_po` (`id_sub_po`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_bpkb`
--
ALTER TABLE `sub_bpkb`
  ADD CONSTRAINT `sub_bpkb_ibfk_1` FOREIGN KEY (`no_bpkb`) REFERENCES `bpkb` (`no_bpkb`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_bpkb_ibfk_2` FOREIGN KEY (`no_kontra_jual`) REFERENCES `kontra_jual` (`no_kontra_jual`),
  ADD CONSTRAINT `sub_bpkb_ibfk_3` FOREIGN KEY (`no_bpkb_foreign`) REFERENCES `bpkb` (`no_bpkb`);

--
-- Constraints for table `sub_bpkb_nota`
--
ALTER TABLE `sub_bpkb_nota`
  ADD CONSTRAINT `sub_bpkb_nota_ibfk_1` FOREIGN KEY (`no_nota_kredit`) REFERENCES `nota_kredit` (`no_nota_kredit`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `sub_bpkb_nota_ibfk_2` FOREIGN KEY (`no_bpkb`) REFERENCES `bpkb` (`no_bpkb`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_bpkb_nota_ibfk_3` FOREIGN KEY (`no_sj`) REFERENCES `sj_penjualan` (`no_sj`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sub_kontra_beli`
--
ALTER TABLE `sub_kontra_beli`
  ADD CONSTRAINT `sub_kontra_beli_ibfk_1` FOREIGN KEY (`no_kontra_beli`) REFERENCES `kontra_beli` (`no_kontra_beli`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_kontra_beli_ibfk_2` FOREIGN KEY (`no_bpb`) REFERENCES `bpb_pembelian` (`no_bpb`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_kontra_jual`
--
ALTER TABLE `sub_kontra_jual`
  ADD CONSTRAINT `sub_kontra_jual_ibfk_1` FOREIGN KEY (`no_kontra_jual`) REFERENCES `kontra_jual` (`no_kontra_jual`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_kontra_jual_ibfk_3` FOREIGN KEY (`no_bpb`) REFERENCES `bpb_pembelian` (`no_bpb`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_kontra_jual_ibfk_5` FOREIGN KEY (`id_sub_sj_penjualan`) REFERENCES `sub_sj_penjualan` (`id_sub_sj_penjualan`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_langganan`
--
ALTER TABLE `sub_langganan`
  ADD CONSTRAINT `sub_langganan_ibfk_1` FOREIGN KEY (`id_langganan`) REFERENCES `langganan` (`id_langganan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_perusahaan`
--
ALTER TABLE `sub_perusahaan`
  ADD CONSTRAINT `sub_perusahaan_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `perusahaan` (`id_perusahaan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_po`
--
ALTER TABLE `sub_po`
  ADD CONSTRAINT `sub_po_ibfk_1` FOREIGN KEY (`no_po`) REFERENCES `purchase_order` (`no_po`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_retur_pembelian`
--
ALTER TABLE `sub_retur_pembelian`
  ADD CONSTRAINT `sub_retur_pembelian_ibfk_1` FOREIGN KEY (`no_retur_beli`) REFERENCES `retur_pembelian` (`no_retur_beli`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_retur_pembelian_ibfk_2` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_retur_penjualan`
--
ALTER TABLE `sub_retur_penjualan`
  ADD CONSTRAINT `sub_retur_penjualan_ibfk_1` FOREIGN KEY (`no_retur_jual`) REFERENCES `retur_penjualan` (`no_retur_jual`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_retur_penjualan_ibfk_2` FOREIGN KEY (`no_po`) REFERENCES `purchase_order` (`no_po`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_sales`
--
ALTER TABLE `sub_sales`
  ADD CONSTRAINT `sub_sales_ibfk_1` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_sj_penjualan`
--
ALTER TABLE `sub_sj_penjualan`
  ADD CONSTRAINT `sub_sj_penjualan_ibfk_1` FOREIGN KEY (`no_sj`) REFERENCES `sj_penjualan` (`no_sj`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_sj_penjualan_ibfk_2` FOREIGN KEY (`no_po`) REFERENCES `purchase_order` (`no_po`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_sj_penjualan_ibfk_3` FOREIGN KEY (`no_bpb`) REFERENCES `bpb_pembelian` (`no_bpb`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_sub_langganan`
--
ALTER TABLE `sub_sub_langganan`
  ADD CONSTRAINT `sub_sub_langganan_ibfk_1` FOREIGN KEY (`id_sub_langganan`) REFERENCES `sub_langganan` (`id_sub_langganan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_sub_langganan_ibfk_2` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_supplier`
--
ALTER TABLE `sub_supplier`
  ADD CONSTRAINT `sub_supplier_ibfk_1` FOREIGN KEY (`kode_supplier`) REFERENCES `supplier` (`kode_supplier`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
