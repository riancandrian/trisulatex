/*
 Navicat Premium Data Transfer

 Source Server         : erp_trisula
 Source Server Type    : PostgreSQL
 Source Server Version : 90514
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90514
 File Encoding         : 65001

 Date: 21/01/2019 09:38:45
*/


-- ----------------------------
-- Sequence structure for tbl_agen_IDAgen_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_agen_IDAgen_seq";
CREATE SEQUENCE "public"."tbl_agen_IDAgen_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_agen_IDAgen_seq"', 25, true);

-- ----------------------------
-- Sequence structure for tbl_asset_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_asset_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_asset_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_asset_IDAsset_seq"', 30, true);

-- ----------------------------
-- Sequence structure for tbl_asset_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_asset_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_asset_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_asset_IDGroupAsset_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_bank_IDBank_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_bank_IDBank_seq";
CREATE SEQUENCE "public"."tbl_bank_IDBank_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_bank_IDBank_seq"', 40, true);

-- ----------------------------
-- Sequence structure for tbl_bank_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_bank_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_bank_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_bank_IDCoa_seq"', 11, true);

-- ----------------------------
-- Sequence structure for tbl_barang_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_barang_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_barang_IDBarang_seq"', 21, true);

-- ----------------------------
-- Sequence structure for tbl_barang_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_barang_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_barang_IDCorak_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_barang_IDGroupBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDGroupBarang_seq";
CREATE SEQUENCE "public"."tbl_barang_IDGroupBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_barang_IDGroupBarang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_barang_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_barang_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_barang_IDMerk_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_barang_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barang_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_barang_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_barang_IDSatuan_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDBO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDBO_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDBO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_booking_order_IDBO_seq"', 23, true);

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_booking_order_IDCorak_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_booking_order_IDMataUang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_coa_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_coa_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_coa_IDCoa_seq"', 25, true);

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDCOASaldoAwal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_coa_saldo_awal_IDCoa_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDGroupCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDGroupCOA_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDGroupCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_coa_saldo_awal_IDGroupCOA_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_corak_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_corak_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_corak_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_corak_IDCorak_seq"', 16, true);

-- ----------------------------
-- Sequence structure for tbl_corak_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_corak_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_corak_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_corak_IDMerk_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_customer_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_customer_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_customer_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_customer_IDCustomer_seq"', 15, true);

-- ----------------------------
-- Sequence structure for tbl_customer_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_customer_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_customer_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_customer_IDGroupCustomer_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_giro_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_giro_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_giro_IDFaktur_seq"', 23, true);

-- ----------------------------
-- Sequence structure for tbl_giro_IDGiro_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDGiro_seq";
CREATE SEQUENCE "public"."tbl_giro_IDGiro_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_giro_IDGiro_seq"', 23, true);

-- ----------------------------
-- Sequence structure for tbl_giro_IDPerusahaan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDPerusahaan_seq";
CREATE SEQUENCE "public"."tbl_giro_IDPerusahaan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_giro_IDPerusahaan_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_group_asset_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_asset_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_group_asset_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_group_asset_IDGroupAsset_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_group_barang_IDGroupBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_barang_IDGroupBarang_seq";
CREATE SEQUENCE "public"."tbl_group_barang_IDGroupBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_group_barang_IDGroupBarang_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_group_coa_IDGroupCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_coa_IDGroupCOA_seq";
CREATE SEQUENCE "public"."tbl_group_coa_IDGroupCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_group_coa_IDGroupCOA_seq"', 8, true);

-- ----------------------------
-- Sequence structure for tbl_group_customer_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_customer_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_group_customer_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_group_customer_IDGroupCustomer_seq"', 21, true);

-- ----------------------------
-- Sequence structure for tbl_group_supplier_IDGroupSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_supplier_IDGroupSupplier_seq";
CREATE SEQUENCE "public"."tbl_group_supplier_IDGroupSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_group_supplier_IDGroupSupplier_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_group_user_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_user_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_group_user_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_group_user_IDGroupUser_seq"', 13, true);

-- ----------------------------
-- Sequence structure for tbl_gudang_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_gudang_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_gudang_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_gudang_IDGudang_seq"', 13, true);

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_harga_jual_barang_IDBarang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_harga_jual_barang_IDGroupCustomer_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDHargaJual_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDHargaJual_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDHargaJual_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_harga_jual_barang_IDHargaJual_seq"', 30, true);

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_harga_jual_barang_IDSatuan_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_hutang_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_hutang_IDFaktur_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_hutang_IDHutang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDHutang_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDHutang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_hutang_IDHutang_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_hutang_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_hutang_IDSupplier_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_in_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_in_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_in_IDBarang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_in_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_in_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_in_IDCorak_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_in_IDIn_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDIn_seq";
CREATE SEQUENCE "public"."tbl_in_IDIn_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_in_IDIn_seq"', 61, true);

-- ----------------------------
-- Sequence structure for tbl_in_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_in_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_in_IDMerk_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_in_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_in_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_in_IDSatuan_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_in_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_in_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_in_IDWarna_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDIP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDIP_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDIP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_IDIP_seq"', 52, true);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDPO_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_IDPO_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_IDSupplier_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDBarang_seq"', 13, true);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDCorak_seq"', 14, true);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDIPDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq"', 93, true);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDIP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDIP_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDIP_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDMerk_seq"', 14, true);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq"', 14, true);

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDWarna_seq"', 14, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDBarang_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDCorak_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDFakturDetail_seq"', 8, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDFaktur_seq"', 8, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDGudang_seq"', 11, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDKartuStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDKartuStok_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDKartuStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDKartuStok_seq"', 207, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDMataUang_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDSatuan_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDStok_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDStok_seq"', 9, true);

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kartu_stok_IDWarna_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDKonversi_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDKonversi_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDKonversi_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_konversi_satuan_IDKonversi_seq"', 19, true);

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDSatuanBerat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDSatuanBerat_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDSatuanBerat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_konversi_satuan_IDSatuanBerat_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDSatuanKecil_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDSatuanKecil_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDSatuanKecil_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_konversi_satuan_IDSatuanKecil_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_IDKP_seq"', 59, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_IDMataUang_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDBarang_seq"', 17, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDCorak_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDKPDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq"', 77, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDKP_seq"', 12, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDMataUang_seq"', 11, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDSatuan_seq"', 17, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDWarna_seq"', 11, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_IDKPScan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_scan_IDKPScan_seq"', 32, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"', 36, true);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_kota_IDKota_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kota_IDKota_seq";
CREATE SEQUENCE "public"."tbl_kota_IDKota_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_kota_IDKota_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_lap_umur_persediaan_IDBarang_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_lap_umur_persediaan_IDCorak_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDLapPers_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDLapPers_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDLapPers_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_lap_umur_persediaan_IDLapPers_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_lap_umur_persediaan_IDMerk_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_lap_umur_persediaan_IDWarna_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_mata_uang_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_mata_uang_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_mata_uang_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_mata_uang_IDMataUang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_menu_IDMenu_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_IDMenu_seq";
CREATE SEQUENCE "public"."tbl_menu_IDMenu_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_menu_IDMenu_seq"', 26, true);

-- ----------------------------
-- Sequence structure for tbl_menu_detail_IDMenuDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_detail_IDMenuDetail_seq";
CREATE SEQUENCE "public"."tbl_menu_detail_IDMenuDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_menu_detail_IDMenuDetail_seq"', 108, true);

-- ----------------------------
-- Sequence structure for tbl_menu_detail_IDMenu_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_detail_IDMenu_seq";
CREATE SEQUENCE "public"."tbl_menu_detail_IDMenu_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_menu_detail_IDMenu_seq"', 12, true);

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_menu_role_IDGroupUser_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDMenuDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDMenuDetail_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDMenuDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_menu_role_IDMenuDetail_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDMenuRole_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDMenuRole_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDMenuRole_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_menu_role_IDMenuRole_seq"', 3016, true);

-- ----------------------------
-- Sequence structure for tbl_merk_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_merk_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_merk_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_merk_IDMerk_seq"', 15, true);

-- ----------------------------
-- Sequence structure for tbl_out_IDOut_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_out_IDOut_seq";
CREATE SEQUENCE "public"."tbl_out_IDOut_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_out_IDOut_seq"', 33, true);

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDCOA_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembayaran_IDCOA_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDFBPembayaran_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDFBPembayaran_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDFBPembayaran_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembayaran_IDFBPembayaran_seq"', 31, true);

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembayaran_IDFB_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembayaran_IDMataUang_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_IDFB_seq"', 110, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_IDMataUang_seq"', 13, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_IDSupplier_seq"', 9, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_IDTBS_seq"', 8, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_IDFBA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_IDFBA_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_IDFBA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_asset_IDFBA_seq"', 91, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_asset_IDMataUang_seq"', 13, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_asset_detail_IDAsset_seq"', 8, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDFBADetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDFBADetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBADetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_asset_detail_IDFBADetail_seq"', 181, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDFBA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDFBA_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_asset_detail_IDFBA_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_asset_detail_IDGroupAsset_seq"', 8, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_detail_IDBarang_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_detail_IDCorak_seq"', 19, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDFBDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDFBDetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDFBDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_detail_IDFBDetail_seq"', 134, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_detail_IDFB_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_detail_IDMerk_seq"', 24, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_detail_IDSatuan_seq"', 34, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDTBSDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDTBSDetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDTBSDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_detail_IDTBSDetail_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_detail_IDWarna_seq"', 24, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_grand_total_IDFBGrandTotal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq";
CREATE SEQUENCE "public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq"', 49, true);

-- ----------------------------
-- Sequence structure for tbl_pembelian_grand_total_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_grand_total_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_grand_total_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_pembelian_grand_total_IDFB_seq"', 14, true);

-- ----------------------------
-- Sequence structure for tbl_piutang_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_piutang_IDCustomer_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_piutang_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_piutang_IDFaktur_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_piutang_IDPiutang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDPiutang_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDPiutang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_piutang_IDPiutang_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDAgen_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDAgen_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDAgen_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_IDAgen_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_IDBarang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_IDCorak_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_IDMataUang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_IDMerk_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDPO_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_IDPO_seq"', 85, true);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_IDSupplier_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDPODetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDPODetail_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDPODetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_detail_IDPODetail_seq"', 89, true);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDPO_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_detail_IDPO_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_purchase_order_detail_IDWarna_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDFB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_IDFB_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_IDRB_seq"', 26, true);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_IDSupplier_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_detail_IDBarang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_detail_IDCorak_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_detail_IDMerk_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDRBDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDRBDetail_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDRBDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_detail_IDRBDetail_seq"', 41, true);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_detail_IDRB_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_detail_IDSatuan_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_detail_IDWarna_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDMataUang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq"', 24, true);

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDRB_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_saldo_awal_asset_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_saldo_awal_asset_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_saldo_awal_asset_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_saldo_awal_asset_IDAsset_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_saldo_awal_asset_IDSaldoAwalAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq";
CREATE SEQUENCE "public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq"', 18, true);

-- ----------------------------
-- Sequence structure for tbl_satuan_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_satuan_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_satuan_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_satuan_IDSatuan_seq"', 17, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDBarang_seq"', 22, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDCorak_seq"', 22, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDFakturDetail_seq"', 27, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDFaktur_seq"', 27, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDGudang_seq"', 22, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDMataUang_seq"', 22, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDSatuan_seq"', 22, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDStok_seq";
CREATE SEQUENCE "public"."tbl_stok_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDStok_seq"', 169, true);

-- ----------------------------
-- Sequence structure for tbl_stok_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_IDWarna_seq"', 22, true);

-- ----------------------------
-- Sequence structure for tbl_stok_history_AsalIDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_AsalIDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_history_AsalIDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_AsalIDFakturDetail_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_AsalIDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_AsalIDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_history_AsalIDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_AsalIDFaktur_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDBarang_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDCorak_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDFakturDetail_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDFaktur_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDGudang_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDHistoryStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDHistoryStok_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDHistoryStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDHistoryStok_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDMataUang_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDSatuan_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDStok_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDStok_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_history_IDWarna_seq"', 6, false);

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_opname_IDBarang_seq"', 6, true);

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_opname_IDCorak_seq"', 7, true);

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_opname_IDGudang_seq"', 8, true);

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_opname_IDSatuan_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDStokOpname_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDStokOpname_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDStokOpname_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_opname_IDStokOpname_seq"', 13, true);

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_stok_opname_IDWarna_seq"', 7, true);

-- ----------------------------
-- Sequence structure for tbl_suplier_IDGroupSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_suplier_IDGroupSupplier_seq";
CREATE SEQUENCE "public"."tbl_suplier_IDGroupSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_suplier_IDGroupSupplier_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_suplier_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_suplier_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_suplier_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_suplier_IDSupplier_seq"', 14, true);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDPO_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDPO_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDSJM_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDSJM_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDSJM_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDSJM_seq"', 20, true);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq"', 23, true);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSJM_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq"', 21, true);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq"', 24, true);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_tampungan_IDT_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_tampungan_IDT_seq";
CREATE SEQUENCE "public"."tbl_tampungan_IDT_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_tampungan_IDT_seq"', 10, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDPO_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_IDPO_seq"', 52, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_IDSupplier_seq"', 42, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_IDTBS_seq"', 101, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDBarang_seq"', 13, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDCorak_seq"', 16, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDMerk_seq"', 27, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDSatuan_seq"', 29, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDTBSDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq"', 261, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDTBS_seq"', 9, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDWarna_seq"', 26, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDSJH_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDTBSH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq"', 40, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq"', 46, true);

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_um_customer_IDCustomer_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_um_customer_IDFaktur_seq"', 15, true);

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDUMCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDUMCustomer_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDUMCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_um_customer_IDUMCustomer_seq"', 14, true);

-- ----------------------------
-- Sequence structure for tbl_user_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_user_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_user_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_user_IDGroupUser_seq"', 8, false);

-- ----------------------------
-- Sequence structure for tbl_user_IDUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_user_IDUser_seq";
CREATE SEQUENCE "public"."tbl_user_IDUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_user_IDUser_seq"', 23, true);

-- ----------------------------
-- Sequence structure for tbl_warna_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_warna_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_warna_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."tbl_warna_IDWarna_seq"', 16, true);

-- ----------------------------
-- Table structure for tbl_agen
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_agen";
CREATE TABLE "public"."tbl_agen" (
  "IDAgen" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_agen_IDAgen_seq"'::regclass),
  "Kode_Perusahaan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Perusahaan" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Initial" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_Tlp" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Image" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(70) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_agen
-- ----------------------------
INSERT INTO "public"."tbl_agen" VALUES ('P000001', 'PMK123', 'PMK', 'PMAK', 'jl. cimahi', '8', '+62543353', '1535334789_1044348767.png', 'aktif', '2342342');

-- ----------------------------
-- Table structure for tbl_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_asset";
CREATE TABLE "public"."tbl_asset" (
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_asset_IDAsset_seq"'::regclass),
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_asset_IDGroupAsset_seq"'::regclass),
  "Kode_Asset" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Asset" varchar(200) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_asset
-- ----------------------------
INSERT INTO "public"."tbl_asset" VALUES ('P000001', 'P000002', 'ASST0001', ' RUKO ITC BARANANGSIANG BLOK E ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000002', 'P000002', 'ASST0002', ' RUKO ITC BARANANGSIANG BLOK F ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000003', 'P000002', 'ASST0003', ' RUKO ITC BARANANGSIANG BLOK F ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000004', 'P000002', 'ASST0004', ' APARTEMEN BEKASI EX KIM ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000005', 'P000003', 'ASST0005', ' KIJANG INOVA DIESEL 2014 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000006', 'P000003', 'ASST0006', ' KIJANG INOVA DIESEL 2012 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000007', 'P000003', 'ASST0007', ' MOTOR VARIO ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000008', 'P000004', 'ASST0008', ' ISTANA ALUMUNIUM ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000009', 'P000004', 'ASST0009', ' FAXIMILLE ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000010', 'P000004', 'ASST0010', ' AC AOLIA ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000011', 'P000004', 'ASST0011', ' DISPENSER ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000012', 'P000004', 'ASST0012', ' TERALIS ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000013', 'P000004', 'ASST0013', ' MESIN POTONG ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000014', 'P000004', 'ASST0014', ' BOR MAKTEC & DUDUKAN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000015', 'P000004', 'ASST0015', ' EXHAUS FAN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000016', 'P000004', 'ASST0016', ' KACA FILM ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000017', 'P000004', 'ASST0017', ' RAK BUKU 4 SUSUN BK 504 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000018', 'P000004', 'ASST0018', ' MEJA KANTOR HP I OD 032 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000019', 'P000004', 'ASST0019', ' LACI SORONG HP I MB 135 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000020', 'P000004', 'ASST0020', ' LEMARI ARSIP HP I ST 270 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000021', 'P000004', 'ASST0021', ' MEJA KANTOR HP V HOD 5055 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000022', 'P000004', 'ASST0022', ' LEMARI ARSIP KK ST 700 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000023', 'P000004', 'ASST0023', ' PINTU LEMARI HP I ST 701 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000024', 'P000004', 'ASST0024', ' KURSI MANAGER HP 62 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000025', 'P000004', 'ASST0025', ' RAK BUKU 3 SUSUN BK 503 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000026', 'P000004', 'ASST0026', ' MEJA KANTOR HP I OD 300 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000027', 'P000004', 'ASST0027', ' MEJA KSB BIDADARI + KACA ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000028', 'P000004', 'ASST0028', ' MEJA TULIS SOLID ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000029', 'P000004', 'ASST0029', ' MEJA TULIS EXPO ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000030', 'P000004', 'ASST0030', ' MEJA TULIS 1/2 BIRO SOLID ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000031', 'P000004', 'ASST0031', ' KURSI MULTI YUKI SILVER ABU Y2 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000032', 'P000004', 'ASST0032', ' RAK BUKU 3 SUSUN BK 503 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000033', 'P000004', 'ASST0033', ' LEMARI ARSIP ST 230A HP 1 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000034', 'P000004', 'ASST0034', ' PINTU LEMARI ST 701A HP 1 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000035', 'P000004', 'ASST0035', ' KK.L.ARSIP ST 700/HST5100 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000036', 'P000004', 'ASST0036', ' RAK BUKU 3 SUSUN BK 503 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000037', 'P000004', 'ASST0037', ' RAK BUKU 4 SUSUN BK 504 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000038', 'P000004', 'ASST0038', ' SPEAKER SIMBADA CST Z100 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000039', 'P000004', 'ASST0039', ' STAPLER SDI 1143 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000040', 'P000004', 'ASST0040', ' RAK BESI + TRIPLEKS ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000041', 'P000004', 'ASST0041', ' MEJA KOMPUTER  ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000042', 'P000004', 'ASST0042', ' MEJA KOMPUTER  ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000043', 'P000004', 'ASST0043', ' RAK BESI + TRIPLEKS ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000044', 'P000004', 'ASST0044', ' RAK BESI + TRIPLEKS ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000045', 'P000004', 'ASST0045', ' RAK BESI UNTUK KAIN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000046', 'P000004', 'ASST0046', ' MEJA KANTOR HP 1 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000047', 'P000004', 'ASST0047', ' LEMARI ARSIP HP 1 + KK L. ARSIP ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000048', 'P000004', 'ASST0048', ' PINTU LEMARI HP 1 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000049', 'P000004', 'ASST0049', ' AIR CONDITIONER ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000050', 'P000004', 'ASST0050', ' NOTEBOOK TOSHIBA-2 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000051', 'P000004', 'ASST0051', ' PARTISI KAYU ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000052', 'P000004', 'ASST0052', ' HARD DISK 40 GB + MOUSE ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000053', 'P000004', 'ASST0053', ' RAK DAN TRALIS RUKO E-9 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000054', 'P000004', 'ASST0054', ' MESIN POTONG ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000055', 'P000004', 'ASST0055', ' RAK BESI + MULTIPLEK ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000056', 'P000004', 'ASST0056', ' CANOPY E-9 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000057', 'P000004', 'ASST0057', ' SEKAT ALUMUNIUM ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000058', 'P000004', 'ASST0058', ' KURSI ADMINISTRASI ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000059', 'P000004', 'ASST0059', ' RAK BESI ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000060', 'P000004', 'ASST0060', ' HANDTRUCK TROLLEY ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000061', 'P000004', 'ASST0061', ' RAK BUKU EXPO 3183 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000062', 'P000004', 'ASST0062', ' KURSI PUTAR LUFO ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000063', 'P000004', 'ASST0063', ' TV LG 21 FL 4RG ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000064', 'P000004', 'ASST0064', ' AC PANASONIC ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000065', 'P000004', 'ASST0065', ' MEJA KANTOR ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000066', 'P000004', 'ASST0066', ' JOINT TABLE ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000067', 'P000004', 'ASST0067', ' MEJA KOMPUTER  ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000068', 'P000004', 'ASST0068', ' LACI SORONG  ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000069', 'P000004', 'ASST0069', ' HEKTER MAX 12 L ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000070', 'P000004', 'ASST0070', ' RENGH KERTAS PRIMO ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000071', 'P000004', 'ASST0071', ' PISAU POTONG POLYTEX 50CM-5MM ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000072', 'P000004', 'ASST0072', ' CANOPY BESI HOLLOW F10-11 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000073', 'P000004', 'ASST0073', ' TANGGA ALUMUNIUM ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000074', 'P000004', 'ASST0074', ' KOPER SAMPEL DAN PAKAIAN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000075', 'P000004', 'ASST0075', ' KOMPUTER LCD E-9 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000076', 'P000004', 'ASST0076', ' RAK DAN LEMARI ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000077', 'P000004', 'ASST0077', ' MESIN POTONG & PISAU ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000078', 'P000004', 'ASST0078', ' PRINTER XEROX PHASER 3124 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000079', 'P000004', 'ASST0079', ' KOMPUTER SERVER ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000080', 'P000004', 'ASST0080', ' FILTER POMPA AIR ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000081', 'P000004', 'ASST0081', ' TANGKI AIR ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000082', 'P000004', 'ASST0082', ' RAK UNTUK M20 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000083', 'P000004', 'ASST0083', ' PRINTER EPSON T20E ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000084', 'P000004', 'ASST0084', ' MEJA KOMPUTER + RAK ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000085', 'P000004', 'ASST0085', ' RAK ARSIP ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000086', 'P000004', 'ASST0086', ' MEJA KANTOR (HP I -300) ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000087', 'P000004', 'ASST0087', ' MEJA KANTOR (HP I -302) ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000088', 'P000004', 'ASST0088', ' PALET F8 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000089', 'P000004', 'ASST0089', ' TANGKI AIR ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000090', 'P000004', 'ASST0090', ' CPU & LAN SETTING ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000091', 'P000004', 'ASST0091', ' MONITOR LCD LG 15,6" ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000092', 'P000004', 'ASST0092', ' MESIN KETOKAN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000093', 'P000004', 'ASST0093', ' LEMARI BESI ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000094', 'P000004', 'ASST0094', ' HARD DISK EXTERNAL TOSHIBA 500 GB ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000095', 'P000004', 'ASST0095', ' DVD RW EXTERNAL ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000096', 'P000004', 'ASST0096', ' LEMARI RAK ARSIP ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000097', 'P000004', 'ASST0097', ' POMPA SHIMIZU PC 250 BIT ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000098', 'P000004', 'ASST0098', ' LEMARI ARSIP ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000099', 'P000004', 'ASST0099', ' MESIN CUTTER TSC ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000100', 'P000004', 'ASST0100', ' ALAT POTONG KAIN GRAMASI + KLEM ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000101', 'P000004', 'ASST0101', ' DISPENSER ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000102', 'P000004', 'ASST0102', ' PEMADAM KEBAKARAN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000103', 'P000004', 'ASST0103', ' PRINTER EPSON R230 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000104', 'P000004', 'ASST0104', ' RAK KAIN BARU ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000105', 'P000004', 'ASST0105', ' LEMARI ARSIP VCL 492 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000106', 'P000004', 'ASST0106', ' BESI SIKU LOBANG 4X4X4MTR ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000107', 'P000004', 'ASST0107', ' RAK HANGER ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000108', 'P000004', 'ASST0108', ' CONFERENCE TABLE CT 3B HIGHPOINT ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000109', 'P000004', 'ASST0109', ' OFFICE DESK HOD 5052 HIGHPOINT ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000110', 'P000004', 'ASST0110', ' MONITOR LCD ACER 16" ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000111', 'P000004', 'ASST0111', ' KEYBOARD ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000112', 'P000004', 'ASST0112', ' AC SHARP PLASMA ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000113', 'P000004', 'ASST0113', ' RAK ALUMUNIUM ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000114', 'P000004', 'ASST0114', ' TRALIS DAN CANOPY ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000115', 'P000004', 'ASST0115', ' LOCKER ACE HARDWARE ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000116', 'P000004', 'ASST0116', ' LEMARI ARSIP 2 SET ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000117', 'P000004', 'ASST0117', ' OD +DRAWERS CENTRAL LOCK HP ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000118', 'P000004', 'ASST0118', ' TABLE FAN INDUSTRIAL ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000119', 'P000004', 'ASST0119', ' BIG WHEEL F/HAND TROLLEY + PULLER ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000120', 'P000004', 'ASST0120', ' KURSI CHITOSE ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000121', 'P000004', 'ASST0121', ' NEW CPU  ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000122', 'P000004', 'ASST0122', ' NEW CPU 2 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000123', 'P000004', 'ASST0123', ' POWER SUPPLY 420 W ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000124', 'P000004', 'ASST0124', ' RAK BESI UNTUK KAIN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000125', 'P000004', 'ASST0125', ' UPS 600 VA ENLIGHT ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000126', 'P000004', 'ASST0126', ' UPS 1200 VA PROLINK  ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000127', 'P000004', 'ASST0127', ' NEW CPU 3 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000128', 'P000004', 'ASST0128', ' SETRIKA PHILLIP GC9105 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000129', 'P000004', 'ASST0129', ' HP SAMSUNG BARU ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000130', 'P000004', 'ASST0130', ' PENGAMAN LIFT ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000131', 'P000004', 'ASST0131', ' MESIN ELECTRONIC SELVAGE LETTER JACK ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000132', 'P000004', 'ASST0132', ' CANOPY ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000133', 'P000004', 'ASST0133', ' POMPA SHIMIZU U/ E9 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000134', 'P000004', 'ASST0134', ' PRINTER HP LASERJET M1132 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000135', 'P000004', 'ASST0135', ' ALAT PEL LANTAI ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000136', 'P000004', 'ASST0136', ' KOMPUTER ADMIN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000137', 'P000004', 'ASST0137', ' UPS + POWER SUPPLY ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000138', 'P000004', 'ASST0138', ' PISAU POTONG ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000139', 'P000004', 'ASST0139', ' UPS PROLINK 700 VA ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000140', 'P000004', 'ASST0140', ' PRINTER EPSON T13 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000141', 'P000004', 'ASST0141', ' FAX PANASONIC ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000142', 'P000004', 'ASST0142', ' NOTEBOOK LENOVO ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000143', 'P000004', 'ASST0143', ' LEMARI BESI ADMIN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000144', 'P000004', 'ASST0144', ' UPS PROLINK 700 VA ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000145', 'P000004', 'ASST0145', ' MESIN ELECTRONIC SELVAGE LETTER JACK ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000146', 'P000004', 'ASST0146', ' RAK BESI BARU ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000147', 'P000004', 'ASST0147', ' SETRIKA UAP PHILIIP BARU ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000148', 'P000004', 'ASST0148', ' KURSI KANTOR ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000149', 'P000004', 'ASST0149', ' RAK ADMIN ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000150', 'P000004', 'ASST0150', ' UPS ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000151', 'P000004', 'ASST0151', ' POWER SUPPLY 420 W ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000152', 'P000004', 'ASST0152', ' POMPA AIR ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000153', 'P000004', 'ASST0153', ' RAK BESI F11 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000154', 'P000004', 'ASST0154', ' CPU baru ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000155', 'P000004', 'ASST0155', ' ACER Notebook ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000156', 'P000004', 'ASST0156', ' RAK BESI 2015 ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000157', 'P000004', 'ASST0157', ' UPS ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000158', 'P000004', 'ASST0158', ' KOMPUTER + PRINTER ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000159', 'P000004', 'ASST0159', ' FURNITURE ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000160', 'P000004', 'ASST0160', ' TELEPON PABX ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000161', 'P000004', 'ASST0161', ' LISTRIK ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000162', 'P000004', 'ASST0162', ' TELEPON PABX ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000163', 'P000004', 'ASST0163', ' PERALATAN CCTV ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000164', 'P000004', 'ASST0164', ' PERALATAN CCTV ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000165', 'P000004', 'ASST0165', ' PERALATAN CCTV ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000166', 'P000004', 'ASST0166', ' SOFTWARE ACCURATE ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000167', 'P000004', 'ASST0167', ' MEJA + KURSI ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000168', 'P000004', 'ASST0168', ' CPU UNIT ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000169', 'P000004', 'ASST0169', ' TELEPON PANASONIC ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000170', 'P000004', 'ASST0170', ' KURSI KANTOR ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000171', 'P000004', 'ASST0171', ' RAK ARSIP BARU ', 'Aktif');
INSERT INTO "public"."tbl_asset" VALUES ('P000172', 'P000004', 'ASST0172', ' PRINTER CANON LBP 6030 ', 'Aktif');

-- ----------------------------
-- Table structure for tbl_bank
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_bank";
CREATE TABLE "public"."tbl_bank" (
  "IDBank" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_bank_IDBank_seq"'::regclass),
  "Nomor_Rekening" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_bank_IDCoa_seq"'::regclass),
  "Atas_Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_bank
-- ----------------------------
INSERT INTO "public"."tbl_bank" VALUES ('P000001', '800150490700', 'P000008', 'PRIMA MODA KREASINDO', 'aktif');
INSERT INTO "public"."tbl_bank" VALUES ('P000002', '1563087777', 'P000004', 'PRIMA MODA KREASINDO', 'aktif');
INSERT INTO "public"."tbl_bank" VALUES ('P000003', '1561554477', 'P000004', 'PRIMA MODA KREASINDO', 'aktif');
INSERT INTO "public"."tbl_bank" VALUES ('P000004', '00000100772', 'P000012', 'PRIMA MODA KREASINDO', 'aktif');

-- ----------------------------
-- Table structure for tbl_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_barang";
CREATE TABLE "public"."tbl_barang" (
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDBarang_seq"'::regclass),
  "IDGroupBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDGroupBarang_seq"'::regclass),
  "Kode_Barang" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Barang" varchar(200) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDMerk_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDSatuan_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_barang_IDCorak_seq"'::regclass),
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Warna_Cust" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_barang
-- ----------------------------
INSERT INTO "public"."tbl_barang" VALUES ('P000001', 'GB00001', 'KB0001', '03131 Hugo Black Stripe', 'Prk0001', 'ST0001', 'P000001', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P000002', 'GB00002', 'SR001', 'Seragam Bank BCA', 'P000001', 'PC0002', 'P000001', 'aktif', NULL);
INSERT INTO "public"."tbl_barang" VALUES ('P000003', 'GB00001', 'KB00123', '93063 Hugo Black Solid', 'Prk0002', 'ST0001', 'P000002', 'aktif', NULL);

-- ----------------------------
-- Table structure for tbl_booking_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_booking_order";
CREATE TABLE "public"."tbl_booking_order" (
  "IDBO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_booking_order_IDBO_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_selesai" date DEFAULT NULL,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_booking_order_IDCorak_seq"'::regclass),
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_booking_order_IDMataUang_seq"'::regclass),
  "Kurs" varchar COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_coa
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_coa";
CREATE TABLE "public"."tbl_coa" (
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_IDCoa_seq"'::regclass),
  "Kode_COA" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_COA" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Modal" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_coa
-- ----------------------------
INSERT INTO "public"."tbl_coa" VALUES ('P000138', '-', '-', 'anak', 'aktif', 'P000001', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000002', '1110110', 'Kas Kecil', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000003', '1110211', 'Kas Besar', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000004', '1120051', 'Bank Central Asia', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000001', '1110000', 'Kas & Setara Kas', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000005', '1120071', 'Bank Permata', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000006', '1120081', 'Bank Mandiri', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000007', '1120091', 'Bank Nasional Indonesia', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000008', '1120101', 'Bank CIMB Niaga', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000009', '1120282', 'Bank Danamon', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000010', '1120283', 'Bank Internasional Indonesia (Maybank)', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000011', '1120284', 'Bank Nasional Nobu', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000012', '1120061', 'Bank Nusantara Parahyangan IDR', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000013', '1130200', 'Deposito', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000014', '1126000', 'Ayat Silang', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000015', '1210000', 'Piutang Usaha', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000016', '1210100', 'Piutang Usaha - Giro Mundur', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000017', '1221000', 'Piutang Usaha - Pihak Ketiga', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000018', '1222000', 'Piutang Usaha - Pihak Berelasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000019', '1223001', 'Piutang Lain - Lain', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000020', '1223000', 'Piutang Lain - Lain - Pihak Ketiga', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000021', '1224000', 'Piutang Lain - Lain - Pihak Berelasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000022', '1330100', 'Persediaan Barang Dagang', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000023', '1429990', 'Suspense', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000024', '1512001', 'Uang Muka Pembelian', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000025', '1512000', 'Uang Muka Pembelian - Pihak Ketiga', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000026', '1513000', 'Uang Muka Pembelian - lain-lain', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000027', '1520011', 'Pajak Dibayar Dimuka', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000028', '1520010', 'PPN Masukan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000029', '1520020', 'PPh 22 - Dibayar Dimuka', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000030', '1520030', 'PPh 23 - Dibayar Dimuka', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000031', '1520040', 'PPh 25 - Dibayar Dimuka', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000032', '1520041', 'PPh 29 - Dibayar Dimuka', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000033', '1530101', 'Beban Dibayar Dimuka', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000034', '1530100', 'Beban Dibayar Dimuka - Sewa', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000035', '1530200', 'Beban Dibayar Dimuka - Asuransi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000036', '1530300', 'Beban Dibayar Dimuka - lainnya', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000037', '1613401', 'Investasi Dalam Entitas Asosiasi', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000038', '1613400', 'Investasi - Gracia Multi Moda', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000039', '1613210', 'investasi - Bintang Cipta Sejahtera', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000040', '1613110', 'investasi - Bina Citra Sentosa', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000041', '1612910', 'investasi - Rajawali Citranusa', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000042', '1701001', 'Aset Tetap', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000043', '1701000', 'Tanah', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000044', '1702500', 'Bangunan Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000045', '1703000', 'Mesin Pabrik', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000046', '1704500', 'Kendaraan Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000047', '1705000', 'Perabot dan peralatan Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000048', '1710500', 'Kendaraan leasing kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000049', '1706500', 'Instalasi Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000050', '1802500', 'Akumulasi Penyusutan Bangunan Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000051', '1803000', 'Akumulasi Penyusutan  Mesin', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000052', '1804500', 'Akumulasi Penyusutan Kendaraan Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000053', '1805000', 'Akumulasi Penyusutan Perabot dan Peralatan Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000054', '1810500', 'Akumulasi Penyusutan Kendaraan leasing Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000055', '1806500', 'Akumulasi Amortisasi Instalasi Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000056', '2120100', 'Utang Bank Jangka Pendek', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000057', '2210101', 'Utang Usaha', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000058', '2210100', 'Utang Usaha - Giro Mundur', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000059', '2221000', 'Utang Usaha - Pihak Ketiga', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000060', '2222000', 'Utang Usaha - Pihak Berelasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000061', '2223001', 'Utang Lain - Lain', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000062', '2223000', 'Utang lain-lain - Pihak Ketiga', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000063', '2224000', 'Utang lain-lain - Pihak Berelasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000064', '2320100', 'Hutang jangka panjang Bank yg akan JT', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000065', '2410101', 'Pajak Yang Masih Harus Dibayar', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000066', '2410100', 'PPN - Keluaran', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000067', '2410200', 'PPh 4 (2) - Terutang', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000068', '2410400', 'PPh 21 - Terutang', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000069', '2410500', 'PPh 23 - Terutang', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000070', '2410600', 'PPh 25 - Terutang', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000071', '2410800', 'PPh 29 - Terutang', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000072', '2340000', 'Utang Leasing', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000073', '2510201', 'Beban Yang Masih Harus Dibayar', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000074', '2510200', 'BYMH - Bunga Yang Masih Harus Dibayar', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000075', '2519990', 'BYMH - Lainnya', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000076', '2610000', 'Pendapatan Yang Masih Harus Diterima', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000077', '2620101', 'Uang Muka Pelanggan', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000078', '2620100', 'Uang Muka Pelanggan - Pihak Ketiga', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000079', '2620190', 'Uang Muka Pelanggan - Lainnya', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000080', '2620200', 'Uang Muka Pelanggan - Pihak Berelasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000081', '2710100', 'Utang Bank Jangka Panjang', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000082', '2910101', 'Ekuitas', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000083', '2910100', 'Modal Saham Disetor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000084', '2920100', 'Agio / Disagio Saham', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000085', '2950100', 'Saldo Laba Ditahan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000086', '2950110', 'Laba (Rugi) Tahun Berjalan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000087', '3110201', 'Penjualan', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000088', '3110200', 'Penjualan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000089', '3210200', 'Retur Penjualan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000090', '3310200', 'Potongan Penjualan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000091', '4110100', 'Harga Pokok Penjualan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000092', '6110101', 'Beban Penjualan', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000093', '6110100', 'B. Penjualan - Sample & Promosi / Iklan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000094', '6110110', 'B. Penjualan - Percetakan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000095', '6111110', 'B. Penjualan - Angkutan & Kuli', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000096', '6112200', 'B. Penjualan - Komisi Penjualan Lokal', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000097', '6114000', 'B. Penjualan - Makloon', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000098', '6115000', 'B. Penjualan - Pengepakan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000099', '6113000', 'B. Penjualan - Klaim', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000100', '6111200', 'B. Penjualan - Uji Lab', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000101', '6120111', 'Beban Administrasi Umum & Penjualan', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000102', '6120110', 'B. Adm & Penjualan - Gaji Dan Upah', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000103', '6120120', 'B. Adm & Penjualan - Lembur', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000104', '6120210', 'B. Adm & Penjualan - Tunjangan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000105', '6120220', 'B. Adm & Penjualan - Kesejahteraan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000106', '6120221', 'B. Adm & Penjualan - Asuransi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000107', '6120310', 'B. Adm & Penjualan - Pemeliharaan Gedung', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000108', '6120410', 'B. Adm & Penjualan - Pemeliharaan Kendaraan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000109', '6120510', 'B. Adm & Penjualan - Jamuan & Representasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000110', '6120550', 'B. Adm & Penjualan - Perizinan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000111', '6120560', 'B. Adm & Penjualan - Keanggotaan & Sumbangan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000112', '6120610', 'B. Adm & Penjualan - Rumah Tangga Kantor', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000113', '6120620', 'B. Adm & Penjualan - Alat Tulis Kantor & Fotocopy', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000114', '6120710', 'B. Adm & Penjualan - Teknologi Informasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000115', '6120810', 'B. Adm & Penjualan - Biaya Bank', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000116', '6120910', 'B. Adm & Penjualan - Biaya Sewa', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000117', '6121010', 'B. Adm & Penjualan - Komunikasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000118', '6121110', 'B. Adm & Penjualan - Perjalanan dan akomodasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000119', '6121120', 'B. Adm & Penjualan - Transportasi', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000120', '6121210', 'B. Adm & Penjualan - Konsultan dan manajemen', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000121', '6121310', 'B. Adm & Penjualan - SDM', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000122', '6121410', 'B. Adm & Penjualan - Penyusutan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000123', '6121710', 'B. Adm & Penjualan - Air & Listrik', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000124', '6121910', 'B. Adm & Penjualan - Pajak', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000125', '6122010', 'B. Adm & Penjualan - Asuransi gedung', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000126', '6122020', 'B. Adm & Penjualan - Asuransi kendaraan', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000127', '6122030', 'B. Adm & Penjualan - Lainnya', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000128', '7110121', 'Pendapatan Lain Lain', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000129', '7110120', 'Pendapatan Jasa Giro', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000130', '7790400', 'Pendapatan Lainnya', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000131', '7120111', 'Beban Lain Lain', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000132', '7120110', 'Beban Bunga Pinjaman Pihak Ketiga', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000133', '7120120', 'Beban Bunga Kredit Lokal', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000134', '7129990', 'Beban Lainnya', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000135', '7210100', 'Beban ( Keuntungan ) Penjualan Aset Tetap', 'anak', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000136', '7810100', 'Beban Pajak Penghasilan', 'kepala', 'aktif', 'P000002', 0);
INSERT INTO "public"."tbl_coa" VALUES ('P000137', '7910100', 'NCI', 'kepala', 'aktif', 'P000002', 0);

-- ----------------------------
-- Table structure for tbl_coa_saldo_awal
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_coa_saldo_awal";
CREATE TABLE "public"."tbl_coa_saldo_awal" (
  "IDCOASaldoAwal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDCOASaldoAwal_seq"'::regclass),
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDCoa_seq"'::regclass),
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDGroupCOA_seq"'::regclass),
  "Nilai_Saldo_Awal" float8 DEFAULT NULL,
  "Aktif" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_corak
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_corak";
CREATE TABLE "public"."tbl_corak" (
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_corak_IDCorak_seq"'::regclass),
  "Kode_Corak" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Corak" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_corak_IDMerk_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_corak
-- ----------------------------
INSERT INTO "public"."tbl_corak" VALUES ('P000001', '03131', '03131', 'aktif', 'Prk0001');
INSERT INTO "public"."tbl_corak" VALUES ('P000002', '93063', '93063', 'aktif', 'Prk0002');
INSERT INTO "public"."tbl_corak" VALUES ('P000003', '03128', '03128', 'aktif', 'Prk0003');
INSERT INTO "public"."tbl_corak" VALUES ('P000004', '93055', '93055', 'aktif', 'Prk0004');
INSERT INTO "public"."tbl_corak" VALUES ('P000005', '03430', '03430', 'aktif', 'Prk0005');
INSERT INTO "public"."tbl_corak" VALUES ('P000006', '03424', '03424', 'aktif', 'Prk0006');
INSERT INTO "public"."tbl_corak" VALUES ('P000007', '03422', '03422', 'aktif', 'Prk0007');
INSERT INTO "public"."tbl_corak" VALUES ('P000008', '03425', '03425', 'aktif', 'Prk0008');
INSERT INTO "public"."tbl_corak" VALUES ('P000009', '03643', '03643', 'aktif', 'Prk0009');
INSERT INTO "public"."tbl_corak" VALUES ('P000010', '03202', '03202', 'aktif', 'Prk0010');
INSERT INTO "public"."tbl_corak" VALUES ('P000011', '93609', '93609', 'aktif', 'Prk0011');
INSERT INTO "public"."tbl_corak" VALUES ('P000012', '26297', '26297', 'aktif', 'Prk0012');
INSERT INTO "public"."tbl_corak" VALUES ('P000013', '21472', '21472', 'aktif', 'Prk0013');
INSERT INTO "public"."tbl_corak" VALUES ('P000014', '21453', '21453', 'aktif', 'Prk0014');
INSERT INTO "public"."tbl_corak" VALUES ('P000015', '26427', '26427', 'aktif', 'Prk0015');
INSERT INTO "public"."tbl_corak" VALUES ('P000016', '21455', '21455', 'aktif', 'Prk0016');
INSERT INTO "public"."tbl_corak" VALUES ('P000017', '21526', '21526', 'aktif', 'Prk0017');
INSERT INTO "public"."tbl_corak" VALUES ('P000018', '77308', '77308', 'aktif', 'Prk0018');
INSERT INTO "public"."tbl_corak" VALUES ('P000019', '70604', '70604', 'aktif', 'Prk0019');
INSERT INTO "public"."tbl_corak" VALUES ('P000020', '88210', '88210', 'aktif', 'Prk0020');
INSERT INTO "public"."tbl_corak" VALUES ('P000021', '88280', '88280', 'aktif', 'Prk0021');
INSERT INTO "public"."tbl_corak" VALUES ('P000022', '68345', '68345', 'aktif', 'Prk0022');
INSERT INTO "public"."tbl_corak" VALUES ('P000023', '11431', '11431', 'aktif', 'Prk0023');
INSERT INTO "public"."tbl_corak" VALUES ('P000024', '61288', '61288', 'aktif', 'Prk0024');
INSERT INTO "public"."tbl_corak" VALUES ('P000025', '93067', '93067', 'aktif', 'Prk0025');
INSERT INTO "public"."tbl_corak" VALUES ('P000026', '03131G', '03131G', 'aktif', 'Prk0001');

-- ----------------------------
-- Table structure for tbl_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_customer";
CREATE TABLE "public"."tbl_customer" (
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_customer_IDCustomer_seq"'::regclass),
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_customer_IDGroupCustomer_seq"'::regclass),
  "Kode_Customer" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "No_Telpon" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Fax" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_KTP" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_customer
-- ----------------------------
INSERT INTO "public"."tbl_customer" VALUES ('P000004', 'P000002', '3RD04', 'BP. ADONG', 'BP. ADONG', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000005', 'P000002', '3RD05', 'BP. AGUNG', 'BP. AGUNG', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000006', 'P000002', '3RD06', 'BP. AGUS KUSWARA', 'BP. AGUS KUSWARA', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000007', 'P000002', '3RD07', 'BP. AGUSTON', 'BP. AGUSTON', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000008', 'P000002', '3RD08', 'BP. AHMAD', 'BP. AHMAD', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000009', 'P000002', '3RD09', 'BP. AKBAR', 'BP. AKBAR', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000010', 'P000002', '3RD10', 'BP. ALAN', 'BP. ALAN', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000011', 'P000002', '3RD11', 'BP. ANDRI / PMK', 'BP. ANDRI / PMK', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000012', 'P000002', '3RD12', 'BP. BAYU / BRI', 'BP. BAYU / BRI', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000013', 'P000002', '3RD13', 'BP. CARMUN', 'BP. CARMUN', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000014', 'P000002', '3RD14', 'BP. CHRIS PERMANA', 'BP. CHRIS PERMANA', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000015', 'P000002', '3RD15', 'BP. DAHYAN', 'BP. DAHYAN', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000016', 'P000002', '3RD16', 'BP. DEDI', 'BP. DEDI', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000017', 'P000002', '3RD17', 'BP. DONI', 'BP. DONI', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000018', 'P000002', '3RD18', 'BP. ERWIN', 'BP. ERWIN', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000019', 'P000002', '3RD19', 'BP. FERI', 'BP. FERI', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000020', 'P000002', '3RD20', 'BP. HARNO', 'BP. HARNO', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000021', 'P000002', '3RD21', 'BP. IRZAL', 'BP. IRZAL', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000022', 'P000002', '3RD22', 'BP. JALALUDIN / JATAYU TAYLOR', 'BP. JALALUDIN / JATAYU TAYLOR', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000023', 'P000002', '3RD23', 'BP. JHONNY EFFENDY SH', 'BP. JHONNY EFFENDY SH', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000024', 'P000002', '3RD24', 'BP. KASTO', 'BP. KASTO', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000025', 'P000002', '3RD25', 'BP. MAULANA', 'BP. MAULANA', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000026', 'P000002', '3RD26', 'BP. OEN SOENG SEN', 'BP. OEN SOENG SEN', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000027', 'P000002', '3RD27', 'BP. SAN SAN', 'BP. SAN SAN', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000028', 'P000002', '3RD28', 'BP. SUDRA', 'BP. SUDRA', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000029', 'P000002', '3RD29', 'BP. SURYA', 'BP. SURYA', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000030', 'P000002', '3RD30', 'BP. TONI', 'BP. TONI', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000031', 'P000002', '3RD31', 'BP. TONI MULYADI', 'BP. TONI MULYADI', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000032', 'P000002', '3RD32', 'BP. UNTUNG', 'BP. UNTUNG', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000033', 'P000002', '3RD33', 'BP. WADO', 'BP. WADO', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000034', 'P000002', '3RD34', 'BP. WANDA', 'BP. WANDA', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000035', 'P000002', '3RD35', 'BP. WANDA / CV. REMAJA', 'BP. WANDA / CV. REMAJA', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000036', 'P000002', '3RD36', 'BP. WAWA', 'BP. WAWA', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000037', 'P000002', '3RD37', 'BP. YUSUF', 'BP. YUSUF', '21', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000038', 'P000002', '3RD38', 'CI WAWA', 'CI WAWA', '08170280103', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000039', 'P000002', '3RD39', 'CV. APRIYANTI MULYA', 'CV. APRIYANTI MULYA', '30', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000040', 'P000002', '3RD40', 'CV. BAYU MANDIRI', 'CV. BAYU MANDIRI', '30', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000041', 'P000002', '3RD41', 'CV. Lintang Lazuardi', 'CV. Lintang Lazuardi', '30', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000042', 'P000002', '3RD42', 'CV. RAPIH JAYA', 'CV. RAPIH JAYA', '30', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000043', 'P000002', '3RD43', 'CV. REMAJA / BP. WANDA', 'CV. REMAJA / BP. WANDA', '30', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000044', 'P000002', '3RD44', 'CV. ZITEX', 'CV. ZITEX', '30', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000045', 'P000002', '3RD45', 'ELDORADO', 'ELDORADO', '2011666', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000046', 'P000002', '3RD46', 'IBU AGUSTINE', 'IBU AGUSTINE', '30', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000047', 'P000002', '3RD47', 'IBU AY LAN', 'IBU AY LAN', '81220071725', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000048', 'P000002', '3RD48', 'IBU DEWI MULYATI', 'IBU DEWI MULYATI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000049', 'P000002', '3RD49', 'IBU DIDI', 'IBU DIDI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000050', 'P000002', '3RD50', 'IBU EKA', 'IBU EKA', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000051', 'P000002', '3RD51', 'IBU ERNA', 'IBU ERNA', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000052', 'P000002', '3RD52', 'IBU HERLINA', 'IBU HERLINA', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000053', 'P000002', '3RD53', 'IBU INDRA', 'IBU INDRA', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000054', 'P000002', '3RD54', 'IBU INEZ', 'IBU INEZ', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000055', 'P000002', '3RD55', 'IBU NGATMI', 'IBU NGATMI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000056', 'P000002', '3RD56', 'IBU NIA', 'IBU NIA', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000057', 'P000002', '3RD57', 'IBU NURBAETI', 'IBU NURBAETI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000058', 'P000002', '3RD58', 'IBU SARI KUSMAWATI', 'IBU SARI KUSMAWATI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000059', 'P000002', '3RD59', 'IBU SIU LAN', 'IBU SIU LAN', '022-5207623', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000060', 'P000002', '3RD60', 'IBU SUKAWI', 'IBU SUKAWI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000061', 'P000002', '3RD61', 'IBU SUKAWI / IBU ANE', 'IBU SUKAWI / IBU ANE', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000062', 'P000002', '3RD62', 'IBU SUKMA', 'IBU SUKMA', '022-4234838', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000001', 'P000002', '3RD01', 'AGUS KUSWARA', 'AGUS KUSWARA', '21', 'P000001', '22', 'dan@g,ail.com', '44444', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000002', 'P000002', '3RD02', 'BOZETO', 'BOZETO', '21', 'P000001', '22', 'dan@g,ail.com', '44444', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000003', 'P000002', '3RD03', 'BP. ADE', 'BP. ADE', '21', 'P000001', '22', 'dan@g,ail.com', '4444444', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000063', 'P000002', '3RD63', 'IBU TESDA', 'IBU TESDA', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000064', 'P000002', '3RD64', 'IBU TINY', 'IBU TINY', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000065', 'P000002', '3RD65', 'IBU TUTY', 'IBU TUTY', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000066', 'P000002', '3RD66', 'IBU YANTI', 'IBU YANTI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000067', 'P000002', '3RD67', 'IBU YATI / IBU NELLY', 'IBU YATI / IBU NELLY', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000068', 'P000002', '3RD68', 'IBU YENNY', 'IBU YENNY', '022-4222065', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000069', 'P000002', '3RD69', 'JATAYU TAYLOR / JALALUDIN', 'JATAYU TAYLOR / JALALUDIN', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000070', 'P000002', '3RD70', 'KRIDA NUSANTARA', 'KRIDA NUSANTARA', '022-7806125', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000071', 'P000002', '3RD71', 'MAHKOTA SAHABAT', 'MAHKOTA SAHABAT', '022-4238165', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000072', 'P000002', '3RD72', 'MENTARI JAYA', 'MENTARI JAYA', '022-4209052', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000073', 'P000002', '3RD73', 'MENTARI JAYA / YASSIN WIJAYA', 'MENTARI JAYA / YASSIN WIJAYA', '022-4209052', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000074', 'P000002', '3RD74', 'MODERA UTAMA', 'MODERA UTAMA', '022-4222065', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000075', 'P000002', '3RD75', 'MODERA UTAMA / YENNI', 'MODERA UTAMA / YENNI', '022-4222065', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000076', 'P000002', '3RD76', 'OEN SOENG SEN', 'OEN SOENG SEN', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000077', 'P000002', '3RD77', 'PD. BINTANG', 'PD. BINTANG', '022 - 420 80 55', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000078', 'P000002', '3RD78', 'PD. SLAMET LESTARI', 'PD. SLAMET LESTARI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000079', 'P000002', '3RD79', 'PT. ACA ASURANSI', 'PT. ACA ASURANSI', '022-4236766', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000080', 'P000002', '3RD80', 'PT. BERSAMA ZATTA JAYA ( ELZATTA )', 'PT. BERSAMA ZATTA JAYA ( ELZATTA )', '(022)-86065356 ', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000081', 'P000002', '3RD81', 'PT. DEKATAMA CENTRA', 'PT. DEKATAMA CENTRA', '022-7800605', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000082', 'P000002', '3RD82', 'PT. ELIZABETH HANJAYA', 'PT. ELIZABETH HANJAYA', '022-5201125', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000083', 'P000002', '3RD83', 'PT. GAYA HIDUP MASA KINI', 'PT. GAYA HIDUP MASA KINI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000084', 'P000002', '3RD84', 'PT. JUST JAIT INDONESIA', 'PT. JUST JAIT INDONESIA', '022-95056000/7502022', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000085', 'P000002', '3RD85', 'PT. MASCOTINDO', 'PT. MASCOTINDO', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000086', 'P000002', '3RD86', 'PT. MULTI GARMENT JAYA', 'PT. MULTI GARMENT JAYA', '022-5200433', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000087', 'P000002', '3RD87', 'PT. NUSANTARA C', 'PT. NUSANTARA C', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000088', 'P000002', '3RD88', 'PT. NUSANTARA CEMERLANG', 'PT. NUSANTARA CEMERLANG', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000089', 'P000002', '3RD89', 'PT. PROGRESSIO INDONESIA', 'PT. PROGRESSIO INDONESIA', '022-4221871', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000090', 'P000002', '3RD90', 'PT. RESTU IBU MANDIRI', 'PT. RESTU IBU MANDIRI', '022 - 8778 7278', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000091', 'P000002', '3RD91', 'PT. SEIKOU', 'PT. SEIKOU', '022-5892003', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000092', 'P000002', '3RD92', 'PT. SHAFIRA LARAS PERSADA', 'PT. SHAFIRA LARAS PERSADA', '022 - 7833250', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000093', 'P000002', '3RD93', 'PT. TRIMAS SARANA GARMENT', 'PT. TRIMAS SARANA GARMENT', '022-5400488', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000094', 'P000002', '3RD94', 'PT. TRIMAS SARANA INDUSTRY', 'PT. TRIMAS SARANA INDUSTRY', '022-5400488', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000095', 'P000002', '3RD95', 'PT. TRIMUDA TUNGGAL SEJAHTERA', 'PT. TRIMUDA TUNGGAL SEJAHTERA', '022 - 7202131', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000096', 'P000002', '3RD96', 'PT. TRISCO TAILORRED APPAREL', 'PT. TRISCO TAILORRED APPAREL', '022-5897183', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000097', 'P000002', '3RD97', 'REJEKI BARU', 'REJEKI BARU', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000098', 'P000002', '3RD98', 'REKATAMA INTI ANUGRAH', 'REKATAMA INTI ANUGRAH', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000099', 'P000002', '3RD99', 'SARI KUSMAWATI', 'SARI KUSMAWATI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000100', 'P000002', '3RD100', 'SARI KUSMAWATI / IBU DIDI', 'SARI KUSMAWATI / IBU DIDI', NULL, 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000101', 'P000002', '3RD101', 'SINAR ABADI', 'SINAR ABADI', '022-6040287', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000102', 'P000002', '3RD102', 'TOKO ELIZABETH', 'TOKO ELIZABETH', '022-5201125', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000103', 'P000002', '3RD103', 'TOKO LIBERTY', 'TOKO LIBERTY', '022-4232188', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000104', 'P000002', '3RD104', 'TOKO SELAMAT ABADI', 'TOKO SELAMAT ABADI', '022 4263696', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000105', 'P000002', '3RD105', 'YAYASAN KRIDA NUSANTARA', 'YAYASAN KRIDA NUSANTARA', '022-7806125', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000106', 'P000002', '3RD106', 'YUNI SARASWATI', 'YUNI SARASWATI', '082119492266', 'P000001', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000107', 'P000002', '3RD107', 'SETIA KAWAN', 'JL. Bengkulu No. 40, Tanjung Karang', '(0721) 252434', 'P000003', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000108', 'P000002', '3RD108', 'CAHAYA TEXTILE', 'Jl. Pengadilan (Ruko Pengadilan) No. 3F-G', '0251-314795', 'P000002', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000109', 'P000002', '3RD109', 'CAHAYATEX BGR', 'Jl. Pengadilan (Ruko Pengadilan) No. 3F-G', '0251-314795', 'P000002', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000110', 'P000002', '3RD110', 'PT. TERATAI WIJAYA', 'Jl. Raya Simpang Tiga Karadenan No. 35 Cibinong', '021-68314545', 'P000002', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000111', 'P000002', '3RD111', 'BP. BUDI', 'Perumahan Vinifera, Jl. Bina Asih 2 Blok B-11 RT.005/RW.009', NULL, 'P000004', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000112', 'P000002', '3RD112', 'CV. AMILA JAYA MANDIRI', 'Taman Wisma Asri, Jl. Nangka Raya No. 3', NULL, 'P000004', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000113', 'P000002', '3RD113', 'HARI HARI', 'Jl.Ir. H. Juanda Pertokoan Bekasi Blok 2 no. 31-32', '021-8809261', 'P000004', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000114', 'P000002', '3RD114', 'HARI HARI / AMING', 'Jl.Ir. H. Juanda Pertokoan Bekasi Blok 2 no. 31-32', '021-8809261', 'P000004', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000115', 'P000002', '3RD115', 'TOKO MUDA RIA', 'Jl. Suprapto no. 124', '0736-22363', 'P000003', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000116', 'P000002', '3RD116', 'BP. SUNARNO " TINO COLLECTION" ', 'Jl. Kendeng 376', NULL, 'P000005', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000117', 'P000002', '3RD117', 'AKPER DUSTIRA', 'Jl. Dustira No. 1', '022-6632358', 'P000006', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000118', 'P000002', '3RD118', 'BP. TATAN', '-', NULL, 'P000006', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000119', 'P000002', '3RD119', 'BP. TONI / CIMINDI SUBUR', 'Jalan Raya Cimindi, Jl. Cemp. No.88, Cigugur Tengah, Andir, Kota ', '022-6010698', 'P000006', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000120', 'P000002', '3RD120', 'IBU NUR KANDAR', 'Harapan Baru Taman Bunga Blok. B3 No. 7 Sukatani Tapos', '8181129581', 'P000008', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000121', 'P000002', '3RD121', 'IBU YANI FASHION', 'Jl. Pesantren Permai II No. 155', NULL, 'P000006', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000122', 'P000002', '3RD122', 'PT. TRISULATEX', 'Jl. Leuwigajah No. 170 Cimahi', '022-6613333', 'P000006', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000123', 'P000002', '3RD123', 'ANEKA TEXILE / AKMAL MALIK OEI', 'Tembusan R.S. Akademis kav. 8-9', '0411-3625089', 'P000016', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000124', 'P000002', '3RD124', 'BP. HERBIN', 'Jl. Bukit Barisan Dalam No. 20', '061 - 88815887', 'P000015', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000125', 'P000002', '3RD125', 'CAHAYA MAS MDN', 'Jl. Kumango No. 60', NULL, 'P000015', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000126', 'P000002', '3RD126', 'INDONESIA EXCLUSIV TEXTILE', 'Jl. Gatot Subroto No. 68', '0741-7553590', 'P000011', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000127', 'P000002', '3RD127', 'JAKARTATEX', 'Jl. Perniagaan Baru No. 36 C (Kesawan) Medan', '061-4511136', 'P000015', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000128', 'P000002', '3RD128', 'JAKARTATEX / AKUN', 'Jl. Perniagaan Baru No. 36 C (Kesawan) Medan', '061-4511136', 'P000015', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000129', 'P000002', '3RD129', 'PT. GRACIA MULTI MODA', 'Jl. Bukit Barisan Dalam No. 20', '061 - 88815887', 'P000015', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000130', 'P000002', '3RD130', 'SANDANG SARI', 'Jl. Jendr. Soeprapto No. 78', '0435-824229', 'P000009', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000131', 'P000002', '3RD131', 'SINAR RAYA', 'Jl. Ahmad Yani III No. 28', '061-4513781', 'P000015', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000132', 'P000002', '3RD132', 'SUMATRA JAYA', 'Jl. Mr. Asaad No. 23', '0741-23323', 'P000011', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000133', 'P000002', '3RD133', 'TETAP JAYA', 'Jl. Kumango No. 60', '061-451460', 'P000015', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000134', 'P000002', '3RD134', 'TETAP JAYA / AWAN', 'Jl. Kumango No. 60', '061-451460', 'P000015', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000135', 'P000002', '3RD135', 'TOKO 19', 'JL. KALIMANTAN NO. 49 ', '0411-315761', 'P000016', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000136', 'P000002', '3RD136', 'ALGI MAULANA', 'Jl. Alianyang ASR.P Hidayat Blok CPM No. 25, RT/RW 004/025 Kel.  SEI BANGKONG', '0812 2035 3445', 'P000021', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000137', 'P000002', '3RD137', 'BANK SULUTGO', 'Jl. Sam Ratulangi No. 9', '0431-851451/861765', 'P000014', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000138', 'P000002', '3RD138', 'CV. AMARTA WISESA', 'Jl. Besar Ijen No. 81', NULL, 'P000017', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000139', 'P000002', '3RD139', 'EKA JAYA', 'JL. TENGKURUK PERMAI NO. 42-43', NULL, 'P000019', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000140', 'P000002', '3RD140', 'EKA JAYA / HUSIN HANAFIAH', 'JL. TENGKURUK PERMAI NO. 42-43', NULL, 'P000019', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000141', 'P000002', '3RD141', 'GUNUNG LANGIT', 'JL. SAM RATULANGI NO. 24-28', '0431-859338', 'P000014', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000142', 'P000002', '3RD142', 'REMAJA  PDG', 'Komp. Nusantara Building A10', '0751-20678', 'P000018', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000143', 'P000002', '3RD143', 'REMAJA PDG / AZNIL ANWAR', 'Komp. Nusantara Building A10', '0751-20678', 'P000018', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000144', 'P000002', '3RD144', 'SALIM', 'Komp. Pasar Bertingkat Fase 1D/55', '0751-32202', 'P000018', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000145', 'P000002', '3RD145', 'AZALEA', 'PERTOKOAN BONGKARAN MEGA NO. 24', '031-3554456', 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000146', 'P000002', '3RD146', 'BAHAGIA BARU', 'JL. WOT GANDUL DALAM NO. 121', '024-3545754', 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000147', 'P000002', '3RD147', 'BAHAGIA BARU / CI NOVI', 'JL. WOT GANDUL DALAM NO. 121', '024-3545754', 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000148', 'P000002', '3RD148', 'BAHAGIA BARU / CV. BAHAGIA BARU', 'JL. WOT GANDUL DALAM NO. 121', '024-3545754', 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000149', 'P000002', '3RD149', 'BAHAGIA BARU / HADI SUFSANTO', 'JL. WOT GANDUL DALAM NO. 121', '024-3545754', 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000150', 'P000002', '3RD150', 'BAHAGIA BARU / LIONG TJU', 'JL. WOT GANDUL DALAM NO. 121', '024-3545754', 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000151', 'P000002', '3RD151', 'BP. JONGGO WIJAYA / CAHAYA MAS', 'JL. SLOMPRETAN NO. 82 ', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000152', 'P000002', '3RD152', 'BUSANA MAS', 'JL. SLOMPRETAN NO. 98', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000153', 'P000002', '3RD153', 'BUSANA MAS / KCML', 'JL. SLOMPRETAN NO. 98', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000154', 'P000002', '3RD154', 'BUSANA MAS SBY', 'JL. SLOMPRETAN NO. 98', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000155', 'P000002', '3RD155', 'CAHAYA MAS / JONGGO WIJAYA', 'JL. SLOMPRETAN NO. 82 ', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000156', 'P000002', '3RD156', 'CAHAYA MAS SBY', 'JL. SLOMPRETAN NO. 82 ', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000157', 'P000002', '3RD157', 'CEMPAKA SBY', 'Pertokoan Bongkaran Mega NO. 24', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000158', 'P000002', '3RD158', 'CEMPAKA SMR', 'JL. GANG WARUNG NO. 58', NULL, 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000159', 'P000002', '3RD159', 'CITRA BUSANA MAS', 'JL. SLOMPRETAN NO. 84/III', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000160', 'P000002', '3RD160', 'CITRA BUSANA MAS / IRWAN SUSANTO', 'JL. SLOMPRETAN NO. 84/III', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000161', 'P000002', '3RD161', 'CV. ARTHA SENA', 'Perumahan Puri Surya Jaya Taman Vancouver Blok J7/14 Rt.02/Rw.09', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000162', 'P000002', '3RD162', 'CV. ARTHA SENA  / BP. ROBY KUSUMA', 'Perumahan Puri Surya Jaya Taman Vancouver Blok J7/14 Rt.02/Rw.09', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000163', 'P000002', '3RD163', 'CV. MELATI', 'JL. GANG WARUNG NO. 43', NULL, 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000164', 'P000002', '3RD164', 'CV. MELATI / CEMPAKA SBY', 'Pertokoan Bongkaran Mega NO. 24', NULL, 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000165', 'P000002', '3RD165', 'CV. SUMINAR KENCANA', 'JL. KAPTEN MULYADI NO. 165', NULL, 'P000023', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000166', 'P000002', '3RD166', 'NATA 23', 'Jl. Abu Hasan No. 34', '0541-747675', 'P000026', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000167', 'P000002', '3RD167', 'NATA 23 / BIKIE', 'Jl. Abu Hasan No. 34', '0541-747675', 'P000026', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000168', 'P000002', '3RD168', 'PT. GOLDEN FLOWER', 'Jl. Karimun Jawa Desa Gedang Anak', NULL, 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000169', 'P000002', '3RD169', 'PT. KHARISMA CEMERLANG', 'RUKO PENGAMPON SQUARE BLOK G NO. 3 JL. SEMUT BARU ', '031-3548719', 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000170', 'P000002', '3RD170', 'TOKO SASAMI', 'Jl. Dr. Rajiman No. 165', '0271-647115', 'P000023', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000171', 'P000002', '3RD171', 'AGUNG JAYA', 'Jl. Dewi Sartika 26 - 27 Ciputat', NULL, 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000172', 'P000002', '3RD172', 'BP. ABDUL', 'Jl. Raya Mauk KM.14 No. 45, Rt/Rw 06/02 Desa Gintung', '816960642', 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000173', 'P000002', '3RD173', 'CV. INTAN PUTERA', 'Green Serpong Bintaro ( GSB ) Cluster La Jardin Blok 11 Jl. Lengkong Wetan Raya', '82136111853', 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000174', 'P000002', '3RD174', 'HERMAWAN INDARTO', 'Jl. Gatot Subroto No. 57 RT.001/RW.002 Kemlayan - Serengan Surakarta', NULL, 'P000023', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000175', 'P000002', '3RD175', 'IBU SUJIATI', 'Pamulang Permai 1 blok A28 No. 9 Pamulang Barat', NULL, 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000176', 'P000002', '3RD176', 'LUHUR MAKMUR', 'JL. MERDEKA Gg. ARENG NO.2', '85692037500', 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000177', 'P000002', '3RD177', 'LUHUR MAKMUR / YUDI', 'JL. MERDEKA Gg. ARENG NO.2', '021-5521440', 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000178', 'P000002', '3RD178', 'PLARA TEXTILE', 'JAILOLO', '021-5521440', 'P000029', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000179', 'P000002', '3RD179', 'PLARA TEXTILE / BANDUNG TAYLOR', 'JAILOLO', '085 240 460 057', 'P000029', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000180', 'P000002', '3RD180', 'PT. AXA SETARA APPAREL', 'RUKO PROMINENCE 38 G / 45 ALAM SUTRA PANUNGGANGAN TIMUR PINANG ', '085 240 460 057', 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000181', 'P000002', '3RD181', 'PT. PUTERA JAYA LESTARI KURNIA', 'Jl. MH. Thamrin Ruko Mahkota Mas Blok H No. 36 Kel. Cikokol Kec. Tangerang', '021-30052568', 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000182', 'P000002', '3RD182', 'SUKSES MANDIRI', 'Jl. Merdeka Gg. Areng No. 02', NULL, 'P000028', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000183', 'P000002', '3RD183', 'TOKO BANDUNG', 'JL. HASANUDIN NO.40 ', '085215959679', 'P000025', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000184', 'P000002', '3RD184', 'TOKO CEMERLANG', 'JL. LEMBONG NO. 23', '0254-200922', 'P000025', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000185', 'P000002', '3RD185', 'AGUNG ABADI', 'JL. RAYA KEBAYORAN LAMA NO. 18 ', '021-7250865', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000186', 'P000002', '3RD186', 'BP. IMAM ZHAKI', 'Jl. Raya Kebagusan no. 12', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000187', 'P000002', '3RD187', 'BP. SANDRO', 'Ruko Niaga Citragrand R 7 No. 19', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000188', 'P000002', '3RD188', 'BP. SWANBI', 'RUKO TEXTILE MANGGA DUA BLOK C2/6 JL. ARTERI MANGGA DUA RAYA', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000189', 'P000002', '3RD189', 'BP. SYAIFUL', 'Gg. Rosa Mulya No. 11 Kalibaru Timur - Kel. Bungur', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000190', 'P000002', '3RD190', 'CAHAYA JKT / LIE MEI', 'Blok 3 Lantai Dasar No. 65-68 (penampungan)', '08158717829', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000191', 'P000002', '3RD191', 'CAHAYA PUTRI', 'Jl. Pasar Senen Blok 1 LT 2 No. 84 B Proyek Senen', '021-4210044', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000192', 'P000002', '3RD192', 'CAHAYA TOKO', 'Blok 3 Lantai Dasar No. 65-68 (penampungan) Deket Fly Over Senen, Sebrang Atrium Plaza', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000193', 'P000002', '3RD193', 'CV. GARUDA TEXTILE', 'Ruko Textile Mangga Dua Blok C4/8', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000194', 'P000002', '3RD194', 'CV. GARUDA TEXTILINDO', 'Ruko Textile Mangga Dua Blok C4/8', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000195', 'P000002', '3RD195', 'CV. KANIA FASHION / IBU OTTY', 'Jl. Cawang Baru Tengah No. 52 rt/rw 005/011 ( Cawang Kauling ), Cipinang Cempedak', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000196', 'P000002', '3RD196', 'CV. KARYA CIPTA PERSADA', 'Jl. Prof. DR. Soepomo No. 231 Crown Palace Blok C 10', '021 - 83787433', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000197', 'P000002', '3RD197', 'CV. PRIMA BUANA PERSADA', 'Jl. Prof. DR. Soepomo No. 231 Crown Palace Blok C 10', '021 - 83787433', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000198', 'P000002', '3RD198', 'FIRMA INDONESIA', 'JL. GUNUNG SAHARI RAYA N0. 17 A KEL. GUNUNG SAHARI UTARA SEBELAH BRI DAN KIA MOTOR', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000199', 'P000002', '3RD199', 'GIANINA FASHION', 'ITC. Mangga Dua Lt. 3, Blok C No. 2 - 3', '021-6015219', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000200', 'P000002', '3RD200', 'HERLINA TOKO / NG BAK HONG / EDY HONG', 'Jl. Cileungsir No. 8 RT/RW 011/01, Cideng Gambir', '021-39841481', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000201', 'P000002', '3RD201', 'IBU CATHRIN', 'Jl. Prof. DR. Soepomo No. 231 Crown Palace Blok C 10', '021 - 83787433', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000202', 'P000002', '3RD202', 'IBU FENI / HERLINA MANDAI BERLIAN', '-', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000203', 'P000002', '3RD203', 'IBU JJ AGUSTIN', 'Jl. Raya Krukut No. 17 Limo', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000204', 'P000002', '3RD204', 'IBU KARTINI', 'Gang Rasa Mulya No. 11 Kalibaru Timur 3, Kel. Bungur', '021 - 424 98 42', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000205', 'P000002', '3RD205', 'IBU. NONI', '-', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000206', 'P000002', '3RD206', 'IKOBANA', 'Jl. Tebah III No. 25 Jakarta Selatan', '0811188971', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000207', 'P000002', '3RD207', 'IKOBANA / JOHANES', 'Jl. Tebah III No. 25 Jakarta Selatan', '0811188971', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000208', 'P000002', '3RD208', 'INDRA MAKMUR', 'Jl. Kaka Tua Blok Y No. 7 Cipinang Indah II Jakarta Timur', '021-8510232', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000209', 'P000002', '3RD209', 'NEW RATU', 'Jl. Pintu Kecil No. 57 E', '021-6917729', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000210', 'P000002', '3RD210', 'PT. ASURANSI CENTRAL ASIA', 'Gedung Wisma Asia Lt 15 Jl. Letjen S.Parman Kav. 79 ', '021-5638028', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000211', 'P000002', '3RD211', 'PT. BINA BUSANA INTERNUSA', 'Jl. Pulo Buaran II Blok Q No. 1 Kawasan Industri Pulogadung', '0821 1141 1534', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000212', 'P000002', '3RD212', 'PT. CENTRAL NETWORK INDONESIA', 'JL. BELAWAN NO. 1 CIDENG BARAT', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000213', 'P000002', '3RD213', 'PT. DERIFIS UTAMA DICIPTA', 'Gedung Pembina Graha Lt. 1 R 8a, Jl. D.I Panjaitan No. 45', '021 - 851 4058', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000214', 'P000002', '3RD214', 'PT. ENERGY MANDIRI SEJAHTERA B', 'Kantor Taman E.3.3 Unit D3a, Lantai 3, Jl. Dr. Ide Anak Agung Gde Agung Lot 8.6-8.7', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000215', 'P000002', '3RD215', 'PT. GUMILAR MANDIRI PERKASA', 'Jl. Sampit 1 No. 55, Kramat Pela - Kebayoran Baru', '021 - 7226066', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000216', 'P000002', '3RD216', 'PT. Indofashion Cipta Kreasi Busana', 'Jl. Raya Bogor KM.26 RT.002/005 Ciracas', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000217', 'P000002', '3RD217', 'PT. INKABIZ', 'Jl. Tarumanegara no. 47 Pisangan Ciputat', '021-98133165', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000218', 'P000002', '3RD218', 'PT. INKABIZ INDONESIA', 'Jl. Tarumanegara no. 47 Pisangan Ciputat', '021-98133165', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000219', 'P000002', '3RD219', 'PT. KUSUMA PUTRI SANTOSA', 'Jl. Cikajang Raya No. 40 Petogogan', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000220', 'P000002', '3RD220', 'PT. LESTARI SENTOSA', 'Jl. Tanjun Duren Raya no. 119 A', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000221', 'P000002', '3RD221', 'PT. LUNAR JAYA', 'Golf Lake Residence Rukan Paris Blok A no. 70', '021-29033689', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000222', 'P000002', '3RD222', 'PT. MEGA MULTI UTAMA', 'Jl. Mesjid Al-Wustho No. 33 Duren Sawit', '021-861 3118', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000223', 'P000002', '3RD223', 'PT. MUSA ATELIER', 'Kemanggisan Utama Raya No. 9 B Slipi', '021-536 75 491', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000224', 'P000002', '3RD224', 'PT. PESONA SINJANG KENCANA', 'Jl. Bumi No. 52-54 Kebayoran Baru', '021-7251705', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000225', 'P000002', '3RD225', 'PT. Radian Putra Metropolindo Prata', 'Komp. PIK BLK.B No. 109-113 Penggilingan, Cakung ', '0817 6428 836', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000226', 'P000002', '3RD226', 'PT. SAMUDRA LAUTAN BIRU', 'Sedayu Square Blok E/23-25, Ringroad Cengkareng, Depan Taman Kencana', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000227', 'P000002', '3RD227', 'PT. SANDPASIFIK JAYA ABADI/BP. SANDRO', 'Ruko Niaga Citragrand R 7 No. 19', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000228', 'P000002', '3RD228', 'PT. TRISULA INTERNATIONAL', 'Trisula Center Jl. Lingkar Luar Barat Blok A No.01 RT.014 RW.04 Cengkareng', '021-5835737', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000229', 'P000002', '3RD229', 'PT. TRISULA INTERNATIONAL Tbk', 'Trisula Center Jl. Lingkar Luar Barat Blok A No.01 RT.014 RW.04 Cengkareng', '021-5835737', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000230', 'P000002', '3RD230', 'PT. UNIMAX CIPTA BUSANA', 'Jl. Sunter Agung Barat 1 Blok A3 No. 23', '021-22065370', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000231', 'P000002', '3RD231', 'REMAJA JKT', 'Jl. Raya Rangunan No. 44 Pasar Minggu', '021-7802952', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000232', 'P000002', '3RD232', 'REMAJA JKT / HERMAN SASONGKO', 'Jl. Raya Rangunan No. 44 Pasar Minggu', '021-7802952', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000233', 'P000002', '3RD233', 'TOKO PG', 'Jl. Mangga Dua Blok C2 No. 37', '021-6009926 / 27', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000234', 'P000002', '3RD234', 'TOKO SAUDARA', 'BLOK B Basment Los C No. 103-105 Tanah Abang', NULL, 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000235', 'P000002', '3RD235', 'TOKO. PG / IE TIAT TIE ', 'Jl. Mangga Dua Blok C2 No. 37', '021-6009926 / 27', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000236', 'P000002', '3RD236', 'WAHYU OTOMO', 'Ruko Textile Mangga Dua Blok D-2 No. 26-27', '021-6128071', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000237', 'P000001', 'RLT01', 'PT. BINA CITRA SENTOSA', 'Jl. Kapten Pier Tandean No. 14', '024-3563633', 'P000024', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000238', 'P000001', 'RLT02', 'PT. BINTANG CIPTA SEJAHTERA', 'Ruko Pengampon Square Blok G No. 3 Jl. Semut Baru Bongkaran Pabean Cantikan', '031 - 355 0979', 'P000022', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000239', 'P000001', 'RLT03', 'PT. CAKRA KENCANA', 'Jl. Mangga dua raya Blok C 6/12 JL. ARTERI MANGGA DUA RAYA  ', '021-6016055', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000240', 'P000001', 'RLT04', 'PT. MIDO INDONESIA', 'JL. ABDUL WAHAB RT 001/RW 008 NO. 38 KEDAUNG SAWANGAN DEPOK', '021 - 227 942 68', 'P000008', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000241', 'P000001', 'RLT05', 'PT. SAVANA LESTARI', 'Jl. Mangga Dua Raya Blok C1/17 ', '021 - 6014534', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');
INSERT INTO "public"."tbl_customer" VALUES ('P000242', 'P000001', 'RLT06', 'PT. TRICITRA BUSANA MAS', 'RUKO TEXTILE MANGGA DUA BLOK C2/6 JL. ARTERI MANGGA DUA RAYA', '021-6018517', 'P000010', '22', 'dan@g,ail.com', '12345', '12345', 'aktif');

-- ----------------------------
-- Table structure for tbl_down_payment
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_down_payment";
CREATE TABLE "public"."tbl_down_payment" (
  "IDDownPayment" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "NoDP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalDP" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Aktif" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_giro
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_giro";
CREATE TABLE "public"."tbl_giro" (
  "IDGiro" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDGiro_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDFaktur_seq"'::regclass),
  "IDPerusahaan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDPerusahaan_seq"'::regclass),
  "Jenis_Faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_Faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBank" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_Giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_Faktur" date DEFAULT NULL,
  "Tanggal_Cair" date DEFAULT NULL,
  "Nilai" int8 DEFAULT NULL,
  "Status_Giro" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_Giro" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_group_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_asset";
CREATE TABLE "public"."tbl_group_asset" (
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_asset_IDGroupAsset_seq"'::regclass),
  "Kode_Group_Asset" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Asset" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tarif_Penyusutan" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_group_asset
-- ----------------------------
INSERT INTO "public"."tbl_group_asset" VALUES ('P000001', 'Tnh ', 'Tanah', 'Aktif', 0);
INSERT INTO "public"."tbl_group_asset" VALUES ('P000002', 'Bngn', 'Bangunan', 'Aktif', 0);
INSERT INTO "public"."tbl_group_asset" VALUES ('P000003', 'kend', 'Kendaraan', 'Aktif', 0);
INSERT INTO "public"."tbl_group_asset" VALUES ('P000004', 'perltn', 'Peralatan', 'Aktif', 0);

-- ----------------------------
-- Table structure for tbl_group_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_barang";
CREATE TABLE "public"."tbl_group_barang" (
  "IDGroupBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_barang_IDGroupBarang_seq"'::regclass),
  "Kode_Group_Barang" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Barang" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_barang
-- ----------------------------
INSERT INTO "public"."tbl_group_barang" VALUES ('GB00002', 'Sr', 'seragam', 'aktif');
INSERT INTO "public"."tbl_group_barang" VALUES ('GB00001', 'Kn', 'kain', 'aktif');
INSERT INTO "public"."tbl_group_barang" VALUES ('Pb00002', 'ak09123', 'aksesoris', 'aktif');
INSERT INTO "public"."tbl_group_barang" VALUES ('GB00004', 'TP', 'Topi', 'aktif');
INSERT INTO "public"."tbl_group_barang" VALUES ('Pb00005', 'FA', 'Aset', 'aktif');

-- ----------------------------
-- Table structure for tbl_group_coa
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_coa";
CREATE TABLE "public"."tbl_group_coa" (
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_coa_IDGroupCOA_seq"'::regclass),
  "Nama_Group" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Normal_Balance" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kode_Group_COA" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_coa
-- ----------------------------
INSERT INTO "public"."tbl_group_coa" VALUES ('P000001', 'biaya', 'debet', 'aktif', '002');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000002', 'aktivasi', 'kredit', 'aktif', 'k00143');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000001', 'Aktiva', 'Debit', 'Aktif', 'Ak');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000002', 'Biaya', 'Debit', 'Aktif', 'Bi');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000003', 'Pendapatan', 'Debit', 'Aktif', 'Pe');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000004', 'Modal', 'Debit', 'Aktif', 'Mo');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000005', 'Kewajiban', 'Debit', 'Aktif', 'Ke');

-- ----------------------------
-- Table structure for tbl_group_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_customer";
CREATE TABLE "public"."tbl_group_customer" (
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_customer_IDGroupCustomer_seq"'::regclass),
  "Kode_Group_Customer" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Group_Customer" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_customer
-- ----------------------------
INSERT INTO "public"."tbl_group_customer" VALUES ('P000002', '3RD', 'PIHAK KETIGA', 'aktif');

-- ----------------------------
-- Table structure for tbl_group_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_supplier";
CREATE TABLE "public"."tbl_group_supplier" (
  "IDGroupSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_supplier_IDGroupSupplier_seq"'::regclass),
  "Kode_Group_Supplier" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Supplier" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_supplier
-- ----------------------------
INSERT INTO "public"."tbl_group_supplier" VALUES ('P000003', '543re', 'uiuiui', 'aktif');
INSERT INTO "public"."tbl_group_supplier" VALUES ('P000004', 'da3499', 'test', 'aktif');
INSERT INTO "public"."tbl_group_supplier" VALUES ('P000001', 'da34', 'relasi', 'aktif');
INSERT INTO "public"."tbl_group_supplier" VALUES ('P000002', '65656', 'pihak ketiga', 'aktif');

-- ----------------------------
-- Table structure for tbl_group_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_user";
CREATE TABLE "public"."tbl_group_user" (
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_group_user_IDGroupUser_seq"'::regclass),
  "Kode_Group_User" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_User" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_user
-- ----------------------------
INSERT INTO "public"."tbl_group_user" VALUES (5, '123', 'admin');

-- ----------------------------
-- Table structure for tbl_gudang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_gudang";
CREATE TABLE "public"."tbl_gudang" (
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_gudang_IDGudang_seq"'::regclass),
  "Kode_Gudang" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Gudang" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_gudang
-- ----------------------------
INSERT INTO "public"."tbl_gudang" VALUES ('5', 'fe122222', 'dfgd', 'aktif');
INSERT INTO "public"."tbl_gudang" VALUES ('P000001', 'gdTTI', 'Trisulatex', 'Aktif');
INSERT INTO "public"."tbl_gudang" VALUES ('P000002', 'gdPMK', 'PMK', 'Aktif');

-- ----------------------------
-- Table structure for tbl_harga_jual_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_harga_jual_barang";
CREATE TABLE "public"."tbl_harga_jual_barang" (
  "IDHargaJual" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDHargaJual_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDBarang_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDSatuan_seq"'::regclass),
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDGroupCustomer_seq"'::regclass),
  "Modal" numeric(100) DEFAULT NULL::numeric,
  "Harga_Jual" numeric(100) DEFAULT NULL::numeric
)
;

-- ----------------------------
-- Records of tbl_harga_jual_barang
-- ----------------------------
INSERT INTO "public"."tbl_harga_jual_barang" VALUES ('P000001', 'P000001', 'ST0001', 'P000002', 40000, 50000);
INSERT INTO "public"."tbl_harga_jual_barang" VALUES ('P000002', 'P000003', 'ST0001', 'P000002', 100000, 120000);

-- ----------------------------
-- Table structure for tbl_hutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_hutang";
CREATE TABLE "public"."tbl_hutang" (
  "Tanggal_Hutang" date DEFAULT NULL,
  "Jatuh_Tempo" date DEFAULT NULL,
  "No_Faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDSupplier_seq"'::regclass),
  "Jenis_Faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_Hutang" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Awal" numeric(100) DEFAULT NULL::numeric,
  "Pembayaran" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Akhir" numeric(100) DEFAULT NULL::numeric,
  "IDHutang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDHutang_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDFaktur_seq"'::regclass),
  "UM" float8 DEFAULT 0,
  "DISC" float8 DEFAULT 0,
  "Retur" float8 DEFAULT 0,
  "Selisih" float8 DEFAULT 0,
  "Kontra_bon" float8 DEFAULT 0,
  "Saldo_kontra_bon" float8 DEFAULT 0
)
;

-- ----------------------------
-- Records of tbl_hutang
-- ----------------------------
INSERT INTO "public"."tbl_hutang" VALUES ('2019-01-18', '2019-04-13', 'INV/19/PMAK/00002', 'P000004', '', 8800000000, 8800000000, 8800000000, 0, 'P000002', 'P000002', 0, 0, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for tbl_in
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_in";
CREATE TABLE "public"."tbl_in" (
  "IDIn" int8 NOT NULL DEFAULT nextval('"tbl_in_IDIn_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDMerk_seq"'::regclass),
  "Panjang_yard" float8 DEFAULT NULL,
  "Panjang_meter" float8 DEFAULT NULL,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_in
-- ----------------------------
INSERT INTO "public"."tbl_in" VALUES (60, '88878278', NULL, NULL, NULL, 'P000003', 'P000002', 'P000001', 'Prk0002', 200, 199.5, 'A', NULL, 'ST0001');
INSERT INTO "public"."tbl_in" VALUES (61, '09012301', NULL, NULL, NULL, 'P000003', 'P000002', 'P000001', 'Prk0002', 200, 199.5, 'A', NULL, 'ST0001');

-- ----------------------------
-- Table structure for tbl_instruksi_pengiriman
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_instruksi_pengiriman";
CREATE TABLE "public"."tbl_instruksi_pengiriman" (
  "IDIP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDIP_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDSupplier_seq"'::regclass),
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDPO_seq"'::regclass),
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_kirim" date DEFAULT NULL,
  "Total_yard" float8 DEFAULT NULL,
  "Total_meter" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_instruksi_pengiriman
-- ----------------------------
INSERT INTO "public"."tbl_instruksi_pengiriman" VALUES ('P000001', '2019-01-16', 'TBS/19/PMAK/00001', 'P000004', 'S00912', 'P000001', 'SO90123', '2019-01-31', 400, 398, NULL, 'Aktif');

-- ----------------------------
-- Table structure for tbl_instruksi_pengiriman_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_instruksi_pengiriman_detail";
CREATE TABLE "public"."tbl_instruksi_pengiriman_detail" (
  "IDIPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDIPDetail_seq"'::regclass),
  "IDIP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDIP_seq"'::regclass),
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_instruksi_pengiriman_detail
-- ----------------------------
INSERT INTO "public"."tbl_instruksi_pengiriman_detail" VALUES ('P000001', 'P000001', '90881799', 'SO90123', '1', '-', 'P000001', 'P000001', 'P000001', 'Prk0001', 200, 199.5, 200, 199.5, 'A', 'test', '123', 'ST0001');
INSERT INTO "public"."tbl_instruksi_pengiriman_detail" VALUES ('P000002', 'P000001', '34657189', 'SO90123', '1', '-', 'P000001', 'P000001', 'P000001', 'Prk0001', 200, 199.5, 200, 199.5, 'A', 'test', '123', 'ST0001');

-- ----------------------------
-- Table structure for tbl_jurnal
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal";
CREATE TABLE "public"."tbl_jurnal" (
  "IDJurnal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8 DEFAULT NULL,
  "Kredit" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_debet" float8 DEFAULT NULL,
  "Total_kredit" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_jurnal
-- ----------------------------
INSERT INTO "public"."tbl_jurnal" VALUES ('P000021', NULL, NULL, 'P000001', 'P000001', 'PH', 'P000057', 8800000000, 0, 1, 14000, 0, 8800000000, 'test', 8800000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000021', NULL, NULL, 'P000001', 'P000001', 'PH', NULL, 0, 8800000000, 1, 14000, 0, 8800000000, 'test', 8800000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001f', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000005', 'TBS', 'P000022', 20000000, 0, 1, 14000, 20000000, 0, 'testasd', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001g', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000005', 'TBS', 'P000091', 0, 20000000, 1, 14000, 0, 20000000, 'testasd', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001h', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000006', 'TBS', 'P000022', 20000000, 0, 1, 14000, 20000000, 0, 'testasd', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001i', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000006', 'TBS', 'P000091', 0, 20000000, 1, 14000, 0, 20000000, 'testasd', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001j', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000007', 'TBS', 'P000022', 4000000, 0, 1, 14000, 4000000, 0, 'testasd', 4000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001k', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000007', 'TBS', 'P000091', 0, 4000000, 1, 14000, 0, 4000000, 'testasd', 4000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001l', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000008', 'TBS', 'P000022', 4000000, 0, 1, 14000, 4000000, 0, 'testasd', 4000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001m', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000008', 'TBS', 'P000091', 0, 4000000, 1, 14000, 0, 4000000, 'testasd', 4000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001n', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000009', 'TBS', 'P000022', 20000000, 0, 1, 14000, 20000000, 0, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001o', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000009', 'TBS', 'P000091', 0, 20000000, 1, 14000, 0, 20000000, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001p', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P00000a', 'TBS', 'P000022', 20000000, 0, 1, 14000, 20000000, 0, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001q', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P00000a', 'TBS', 'P000091', 0, 20000000, 1, 14000, 0, 20000000, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001r', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'TBS', 'P000022', 20000000, 0, 1, 14000, 20000000, 0, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001s', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'TBS', 'P000091', 0, 20000000, 1, 14000, 0, 20000000, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001t', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'TBS', 'P000022', 20000000, 0, 1, 14000, 20000000, 0, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001u', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'TBS', 'P000091', 0, 20000000, 1, 14000, 0, 20000000, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001v', '2019-01-18', 'INV/19/PMAK/00002', 'P000002', 'P000005', 'INV', 'P000091', 20000000, 0, 1, 14000, 20000000, 0, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001w', '2019-01-18', 'INV/19/PMAK/00002', 'P000002', 'P000005', 'INV', 'P000066', 2000000, 0, 1, 14000, 2000000, 0, 'test', 2000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001x', '2019-01-18', 'INV/19/PMAK/00002', 'P000002', 'P000005', 'INV', 'P000059', 0, 20000000, 1, 14000, 0, 20000000, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001y', '2019-01-18', 'INV/19/PMAK/00002', 'P000002', 'P000006', 'INV', 'P000091', 20000000, 0, 1, 14000, 20000000, 0, 'test', 20000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P00001z', '2019-01-18', 'INV/19/PMAK/00002', 'P000002', 'P000006', 'INV', 'P000066', 2000000, 0, 1, 14000, 2000000, 0, 'test', 2000000);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000020', '2019-01-18', 'INV/19/PMAK/00002', 'P000002', 'P000006', 'INV', 'P000059', 0, 20000000, 1, 14000, 0, 20000000, 'test', 20000000);

-- ----------------------------
-- Table structure for tbl_jurnal_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal_umum";
CREATE TABLE "public"."tbl_jurnal_umum" (
  "IDJU" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_jurnal_umum
-- ----------------------------
INSERT INTO "public"."tbl_jurnal_umum" VALUES ('P000001', '2019-01-12', 'JU19PMAK00001', 'P000003', 'Kredit', 'fgdgd', 'aktif');

-- ----------------------------
-- Table structure for tbl_jurnal_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal_umum_detail";
CREATE TABLE "public"."tbl_jurnal_umum_detail" (
  "IDJUDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDJU" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8 DEFAULT NULL,
  "Kredit" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_jurnal_umum_detail
-- ----------------------------
INSERT INTO "public"."tbl_jurnal_umum_detail" VALUES ('P000001', 'P000001', 'P000006', 'Kredit', 0, 50000, 1, 1, 'dgfd');

-- ----------------------------
-- Table structure for tbl_kartu_stok
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kartu_stok";
CREATE TABLE "public"."tbl_kartu_stok" (
  "IDKartuStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDKartuStok_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor_faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDFaktur_seq"'::regclass),
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDFakturDetail_seq"'::regclass),
  "IDStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDStok_seq"'::regclass),
  "Jenis_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDWarna_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDGudang_seq"'::regclass),
  "Masuk_yard" float8 DEFAULT NULL,
  "Masuk_meter" float8 DEFAULT NULL,
  "Keluar_yard" float8 DEFAULT NULL,
  "Keluar_meter" float8 DEFAULT NULL,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDSatuan_seq"'::regclass),
  "Harga" float8 DEFAULT NULL,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDMataUang_seq"'::regclass),
  "Kurs" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_kartu_stok
-- ----------------------------
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000001', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '9987718', 'P000001', 'P000001', 'P000005', '0', 400, 399.8, 0, 0, 'A', '0', 400000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000002', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '3491911', 'P000001', 'P000001', 'P000001', '0', 300, 299.8, 0, 0, 'A', '0', 200000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000003', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000003', 'P000003', 'SA', '8878819', 'P000001', 'P000001', 'P000001', '0', 300, 299.8, 0, 0, 'A', '0', 200000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000004', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000004', 'P000004', 'SA', '0999918', 'P000001', 'P000001', 'P000001', '0', 300, 299.8, 0, 0, 'A', '0', 200000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000005', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000005', 'P000005', 'SA', '9091231', 'P000001', 'P000001', 'P000005', '0', 6000, 5999.9, 0, 0, 'A', '0', 6000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000006', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000006', 'P000006', 'SA', '9091231', 'P000001', 'P000001', 'P000005', '0', 400, 399.8, 0, 0, 'A', '0', 400000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000007', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000007', 'P000007', 'SA', '9887189', 'P000001', 'P000001', 'P000001', '0', 300, 299.8, 0, 0, 'A', '0', 200000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000g', '2018-12-31', 'PB/18/PMAK/00002', 'P000002', 'P000008', 'P000009', 'SA', '845851234615', '0', 'P000026', 'P000558', '0', 500, 0, 0, 0, '1', 'ST0001', 15000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000009', 'SA', '9981918', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P00000a', 'SA', '1090928', 'P000026', 'P000001', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P00000b', 'SA', '981289', 'P000026', 'P000001', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P00000c', 'SA', '57151571', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-15', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '90881799', 'P000026', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-15', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '34657189', 'P000026', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-15', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P000003', 'SA', '77656786', 'P000026', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-15', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P000004', 'SA', '43566777', 'P000026', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000s', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000005', 'P000001', 'SA', '90881799', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000s', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000006', 'P000002', 'SA', '34657189', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000s', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000007', 'P000003', 'SA', '77656786', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000s', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000008', 'P000004', 'SA', '43566777', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-17', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '90881799', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-17', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '34657189', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-17', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P000003', 'SA', '77656786', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-17', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P000004', 'SA', '43566777', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000w', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '88878278', 'P000003', 'P000002', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 20000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000w', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '09012301', 'P000003', 'P000002', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 20000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000008', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '9987718', 'P000001', 'P000001', 'P000005', '0', 400, 399.8, 0, 0, 'A', 'ST0001', 400000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P000009', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '3491911', 'P000001', 'P000001', 'P000001', '0', 300, 299.8, 0, 0, 'A', 'ST0001', 200000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000a', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000003', 'P000003', 'SA', '8878819', 'P000001', 'P000001', 'P000001', '0', 300, 299.8, 0, 0, 'A', 'ST0001', 200000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000b', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000004', 'P000004', 'SA', '0999918', 'P000001', 'P000001', 'P000001', '0', 300, 299.8, 0, 0, 'A', 'ST0001', 200000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000c', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000005', 'P000005', 'SA', '3456091', 'P000001', 'P000001', 'P000005', '0', 6000, 5999.9, 0, 0, 'A', 'ST0001', 6000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000d', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000006', 'P000006', 'SA', '9091231', 'P000001', 'P000001', 'P000005', '0', 400, 399.8, 0, 0, 'A', 'ST0001', 400000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000e', '2018-12-27', 'PB/18/PMAK/00001', 'P000001', 'P000007', 'P000007', 'SA', '9887189', 'P000001', 'P000001', 'P000001', '0', 300, 299.8, 0, 0, 'A', 'ST0001', 200000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000g', '2019-01-11', 'PB/19/PMAK/00001', 'P000003', 'P000001', 'P000001', 'SA', '9981918', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000g', '2019-01-11', 'PB/19/PMAK/00001', 'P000003', 'P000002', 'P000002', 'SA', '1090928', 'P000002', 'P000002', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000g', '2019-01-11', 'PB/19/PMAK/00001', 'P000003', 'P000003', 'P000003', 'SA', '981289', 'P000002', 'P000002', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000g', '2019-01-11', 'PB/19/PMAK/00001', 'P000003', 'P000004', 'P000004', 'SA', '57151571', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P00000d', 'SA', '9981918', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P00000e', 'SA', '1090928', 'P000026', 'P000001', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P00000f', 'SA', '981289', 'P000026', 'P000001', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P00000g', 'SA', '57151571', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-15', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '34657189', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-15', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '90881799', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-15', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P000003', 'SA', '77656786', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-15', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P000004', 'SA', '43566777', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000s', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '34657189', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000s', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '90881799', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000s', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P000003', 'SA', '77656786', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000s', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P000004', 'SA', '43566777', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000005', 'P000005', 'SA', '88878278', '0', 'P000002', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 20000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000006', 'P000006', 'SA', '09012301', '0', 'P000002', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 20000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000007', 'P000007', 'SA', '77656786', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-18', 'PB/19/PMAK/00002', 'P000002', 'P000008', 'P000008', 'SA', '43566777', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000f', '2018-12-27', 'PBUU/18/PMAK/00001', 'P000001', 'P000001', 'P000008', '-', '-', '-', '-', '-', '0', 10000, 10000, 0, 0, '-', 'ST0001', 20000, 1, '', 200000000);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000g', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000005', 'SA', '9981918', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000h', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P000006', 'SA', '1090928', 'P000002', 'P000002', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000i', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P000007', 'SA', '981289', 'P000002', 'P000002', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000j', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P000008', 'SA', '57151571', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000k', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '9981918', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000l', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '1090928', 'P000026', 'P000001', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000m', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P000003', 'SA', '981289', 'P000026', 'P000001', 'P000002', '0', 100, 98.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000n', '2019-01-11', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P000004', 'SA', '57151571', 'P000026', 'P000001', 'P000001', '0', 75, 72.8, 0, 0, 'A', 'ST0001', 2250000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000o', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'P000001', 'SA', '90881799', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000p', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'P000002', 'SA', '34657189', 'P000001', 'P000001', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 12000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000q', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000003', 'P000003', 'SA', '77656786', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000r', '2019-01-16', 'PB/19/PMAK/00001', 'P000001', 'P000004', 'P000004', 'SA', '43566777', 'P000001', 'P000001', 'P000002', '0', 100, 99.5, 0, 0, 'A', 'ST0001', 4000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000t', '2019-01-16', 'PBU/19/PMAK/00002', 'P000002', 'P000005', 'P000005', 'SA', '7788000912', '0', 'P000002', 'P000002', '0', 1000, 914.4, 0, 0, 'A', 'ST0001', 0, 1, '14000', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000u', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000009', 'P000005', 'SA', '88878278', 'P000003', 'P000002', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 20000000, 1, '', 0);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('P00000v', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P00000a', 'P000006', 'SA', '09012301', 'P000003', 'P000002', 'P000001', '0', 200, 199.5, 0, 0, 'A', 'ST0001', 20000000, 1, '', 0);

-- ----------------------------
-- Table structure for tbl_konversi_satuan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_konversi_satuan";
CREATE TABLE "public"."tbl_konversi_satuan" (
  "IDKonversi" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDKonversi_seq"'::regclass),
  "IDSatuanBerat" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDSatuanBerat_seq"'::regclass),
  "Qty" float8 DEFAULT NULL,
  "IDSatuanKecil" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDSatuanKecil_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_konversi_satuan
-- ----------------------------
INSERT INTO "public"."tbl_konversi_satuan" VALUES (6, 5, 1.6757657, 5);
INSERT INTO "public"."tbl_konversi_satuan" VALUES (7, 5, 1.56462345327, 5);
INSERT INTO "public"."tbl_konversi_satuan" VALUES (14, 9, 1.323, 9);

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan";
CREATE TABLE "public"."tbl_koreksi_persediaan" (
  "IDKP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_IDKP_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_yard" float8 DEFAULT NULL,
  "Total_meter" float8 DEFAULT NULL,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_IDMataUang_seq"'::regclass),
  "Kurs" float8 DEFAULT NULL,
  "Jenis" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(35) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan_detail";
CREATE TABLE "public"."tbl_koreksi_persediaan_detail" (
  "IDKPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDKPDetail_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDSatuan_seq"'::regclass),
  "Barcode" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDWarna_seq"'::regclass),
  "IDKP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDKP_seq"'::regclass),
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDMataUang_seq"'::regclass),
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Qty_yard" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan_scan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan_scan";
CREATE TABLE "public"."tbl_koreksi_persediaan_scan" (
  "IDKPScan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_IDKPScan_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_yard" float8 DEFAULT NULL,
  "Total_meter" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Jenis" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan_scan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail";
CREATE TABLE "public"."tbl_koreksi_persediaan_scan_detail" (
  "IDKPScanDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"'::regclass),
  "IDKPScan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDKP_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDWarna_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"'::regclass),
  "Kurs" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_kota
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kota";
CREATE TABLE "public"."tbl_kota" (
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kota_IDKota_seq"'::regclass),
  "Kode_Kota" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Provinsi" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kota" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_kota
-- ----------------------------
INSERT INTO "public"."tbl_kota" VALUES ('P000001', 'BDG', 'Jawa Barat', 'BANDUNG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000002', 'BGR', 'Jawa Barat', 'BOGOR', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000003', 'BKL', 'Bengkulu', 'BENGKULU', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000004', 'BKS', 'Jawa Barat', 'BEKASI', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000005', 'CLCP', 'Jawa Tengah', 'CILACAP', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000006', 'CMH', 'Jawa Barat', 'CIMAHI', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000007', 'CRB', 'Jawa Barat', 'CIREBON', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000008', 'DPK', 'Jawa Barat', 'DEPOK', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000009', 'GRNT', 'Gorontalo', 'GORONTALO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000010', 'JKT', 'DKI Jakarta', 'JAKARTA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000011', 'JMB', 'Jambi', 'JAMBI', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000012', 'LPG', 'Lampung', 'LAMPUNG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000013', 'KRW', 'Jawa Barat', 'KARAWANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000014', 'MND', 'Sulawesi Utara', 'MANADO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000015', 'MDN', 'Sumatera Utara', 'MEDAN', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000016', 'MKSR', 'Sulawesi Selatan', 'MAKASAR', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000017', 'MLG', 'Jawa Timur', 'MALANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000018', 'PDG', 'Sumatera Barat', 'PADANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000019', 'PLB', 'Sumatera Selata', 'PALEMBANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000020', 'PKBR', 'Riau', 'PEKANBARU', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000021', 'PTNK', 'Kalimantan Barat', 'PONTIANAK', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000022', 'SBY', 'Jawa TImur', 'SURABAYA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000023', 'SLO', 'Jawa Tengah', 'SOLO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000024', 'SMR', 'Jawa Tengah', 'SEMARANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000025', 'SRG', 'Banten', 'SERANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000026', 'SMRD', 'Kalimantan Timur', 'SAMARINDA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000027', 'TSK', 'Jawa Barat', 'TASIKMALAYA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000028', 'TGR', 'Banten', 'TANGERANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000029', 'TRNT', 'Maluku Utara', 'TERNATE', 'aktif');

-- ----------------------------
-- Table structure for tbl_lap_umur_persediaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_lap_umur_persediaan";
CREATE TABLE "public"."tbl_lap_umur_persediaan" (
  "IDLapPers" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDLapPers_seq"'::regclass),
  "IDBarang" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDBarang_seq"'::regclass),
  "IDCorak" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDCorak_seq"'::regclass),
  "IDMerk" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDMerk_seq"'::regclass),
  "IDWarna" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDWarna_seq"'::regclass),
  "Qty_yard1" float8 DEFAULT NULL,
  "Qty_meter1" float8 DEFAULT NULL,
  "Nilai1" float8 DEFAULT NULL,
  "Qty_yard2" float8 DEFAULT NULL,
  "Qty_meter2" float8 DEFAULT NULL,
  "Nilai2" float8 DEFAULT NULL,
  "Qty_yard3" float8 DEFAULT NULL,
  "Qty_meter3" float8 DEFAULT NULL,
  "Nilai3" float8 DEFAULT NULL,
  "Total_qty_yard" float8 DEFAULT NULL,
  "Total_qty_meter" float8 DEFAULT NULL,
  "Total_nilai" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_mata_uang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mata_uang";
CREATE TABLE "public"."tbl_mata_uang" (
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_mata_uang_IDMataUang_seq"'::regclass),
  "Kode" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Mata_uang" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Negara" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Default" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_mata_uang
-- ----------------------------
INSERT INTO "public"."tbl_mata_uang" VALUES (1, 'rp001', 'Rupiah', 'Indonesia', NULL, '140000', 'aktif');

-- ----------------------------
-- Table structure for tbl_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu";
CREATE TABLE "public"."tbl_menu" (
  "IDMenu" int8 NOT NULL DEFAULT nextval('"tbl_menu_IDMenu_seq"'::regclass),
  "Menu" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
INSERT INTO "public"."tbl_menu" VALUES (1, 'Data Master');
INSERT INTO "public"."tbl_menu" VALUES (12, 'Pembelian');
INSERT INTO "public"."tbl_menu" VALUES (16, 'Saldo Awal');
INSERT INTO "public"."tbl_menu" VALUES (17, 'Setting');
INSERT INTO "public"."tbl_menu" VALUES (18, 'Persediaan');
INSERT INTO "public"."tbl_menu" VALUES (19, 'Penjualan');
INSERT INTO "public"."tbl_menu" VALUES (20, 'Keuangan');
INSERT INTO "public"."tbl_menu" VALUES (15, 'Laporan Pembelian dan Penjualan');
INSERT INTO "public"."tbl_menu" VALUES (26, 'Setting Keuangan');

-- ----------------------------
-- Table structure for tbl_menu_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu_detail";
CREATE TABLE "public"."tbl_menu_detail" (
  "IDMenuDetail" int8 NOT NULL DEFAULT nextval('"tbl_menu_detail_IDMenuDetail_seq"'::regclass),
  "IDMenu" int8 NOT NULL DEFAULT nextval('"tbl_menu_detail_IDMenu_seq"'::regclass),
  "Menu_Detail" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Url" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_menu_detail
-- ----------------------------
INSERT INTO "public"."tbl_menu_detail" VALUES (15, 1, 'Harga Jual Barang', 'HargaJualBarang');
INSERT INTO "public"."tbl_menu_detail" VALUES (67, 18, 'Koreksi Persediaan Scan', 'KoreksiPersediaanScan');
INSERT INTO "public"."tbl_menu_detail" VALUES (69, 18, 'Laporan Kartu Stok', 'KartuStok');
INSERT INTO "public"."tbl_menu_detail" VALUES (70, 18, 'Laporan Stok', 'Stok');
INSERT INTO "public"."tbl_menu_detail" VALUES (63, 15, 'Laporan Intruksi Pengiriman', 'InstruksiPengiriman/laporan_instruksi');
INSERT INTO "public"."tbl_menu_detail" VALUES (71, 16, 'Saldo Awal Account', 'SaldoAwalAcount');
INSERT INTO "public"."tbl_menu_detail" VALUES (2, 1, 'Asset', 'Assets');
INSERT INTO "public"."tbl_menu_detail" VALUES (3, 1, 'Bank', 'Bank');
INSERT INTO "public"."tbl_menu_detail" VALUES (4, 1, 'Barang', 'Barang');
INSERT INTO "public"."tbl_menu_detail" VALUES (5, 1, 'COA', 'Coa');
INSERT INTO "public"."tbl_menu_detail" VALUES (6, 1, 'Corak', 'Corak');
INSERT INTO "public"."tbl_menu_detail" VALUES (7, 1, 'Customer', 'Customer');
INSERT INTO "public"."tbl_menu_detail" VALUES (9, 1, 'Group Asset', 'GroupAsset');
INSERT INTO "public"."tbl_menu_detail" VALUES (10, 1, 'Group Barang', 'GroupBarang');
INSERT INTO "public"."tbl_menu_detail" VALUES (11, 1, 'Group Customer', 'GroupCustomer');
INSERT INTO "public"."tbl_menu_detail" VALUES (12, 1, 'Group Supplier', 'GroupSupplier');
INSERT INTO "public"."tbl_menu_detail" VALUES (14, 1, 'Gudang', 'Gudang');
INSERT INTO "public"."tbl_menu_detail" VALUES (21, 1, 'Merk', 'Merk');
INSERT INTO "public"."tbl_menu_detail" VALUES (24, 1, 'Satuan', 'Satuan');
INSERT INTO "public"."tbl_menu_detail" VALUES (25, 1, 'Supplier', 'Supplier');
INSERT INTO "public"."tbl_menu_detail" VALUES (68, 18, 'Laporan Umur Persediaan', 'LapUmurPersediaan');
INSERT INTO "public"."tbl_menu_detail" VALUES (28, 1, 'Warna', 'Warna');
INSERT INTO "public"."tbl_menu_detail" VALUES (34, 12, 'Booking Order', 'BookingOrder');
INSERT INTO "public"."tbl_menu_detail" VALUES (75, 19, 'Sales Order', 'So');
INSERT INTO "public"."tbl_menu_detail" VALUES (76, 19, 'Sales Order Non Kain', 'Sos');
INSERT INTO "public"."tbl_menu_detail" VALUES (77, 19, 'Packing List', 'PackingList');
INSERT INTO "public"."tbl_menu_detail" VALUES (78, 19, 'Surat Jalan', 'SuratJalan');
INSERT INTO "public"."tbl_menu_detail" VALUES (35, 12, 'Purchase Order', 'Po');
INSERT INTO "public"."tbl_menu_detail" VALUES (60, 15, 'Laporan Invoice Pembelian', 'Laporan_invoice_pembelian/index');
INSERT INTO "public"."tbl_menu_detail" VALUES (62, 15, 'Laporan Pembelian Asset', 'Laporan_pembelian_asset/index');
INSERT INTO "public"."tbl_menu_detail" VALUES (59, 15, 'Laporan Penerimaan Barang', 'Laporan_penerimaan_barang/index');
INSERT INTO "public"."tbl_menu_detail" VALUES (56, 15, 'Laporan PO', 'PO/laporan_po');
INSERT INTO "public"."tbl_menu_detail" VALUES (57, 15, 'Laporan PO Celup', 'POCelup/laporan_po');
INSERT INTO "public"."tbl_menu_detail" VALUES (58, 15, 'Laporan PO Umum', 'POUmum/laporan_po');
INSERT INTO "public"."tbl_menu_detail" VALUES (98, 15, 'Laporan Surat Jalan Kain', 'SuratJalan/laporan_surat_jalan_kain');
INSERT INTO "public"."tbl_menu_detail" VALUES (64, 15, 'Laporan Retur Pembelian', 'ReturPembelian/laporan_retur_pembelian');
INSERT INTO "public"."tbl_menu_detail" VALUES (16, 16, 'Hutang', 'Hutang');
INSERT INTO "public"."tbl_menu_detail" VALUES (22, 16, 'Piutang', 'Piutang');
INSERT INTO "public"."tbl_menu_detail" VALUES (8, 16, 'Giro Masuk', 'Giro');
INSERT INTO "public"."tbl_menu_detail" VALUES (32, 16, 'Giro Keluar', 'Giro/index_giro_keluar');
INSERT INTO "public"."tbl_menu_detail" VALUES (26, 16, 'UM Customer', 'UMCustomer');
INSERT INTO "public"."tbl_menu_detail" VALUES (23, 16, 'Saldo Awal Asset', 'SaldoAwalAsset');
INSERT INTO "public"."tbl_menu_detail" VALUES (55, 15, 'Laporan BO', 'Laporan_bo/index');
INSERT INTO "public"."tbl_menu_detail" VALUES (80, 19, 'Invoice Penjualan', 'InvoicePenjualan');
INSERT INTO "public"."tbl_menu_detail" VALUES (37, 12, 'Purchase Order Celup', 'PoCelup');
INSERT INTO "public"."tbl_menu_detail" VALUES (38, 12, 'Penerimaan Barang', 'PenerimaanBarang');
INSERT INTO "public"."tbl_menu_detail" VALUES (39, 12, 'Scan Penerimaan Barang', 'scan_penerimaan_barang');
INSERT INTO "public"."tbl_menu_detail" VALUES (42, 12, 'Invoice Pembelian', 'InvoicePembelian');
INSERT INTO "public"."tbl_menu_detail" VALUES (43, 12, 'Intruksi Pengiriman', 'InstruksiPengiriman');
INSERT INTO "public"."tbl_menu_detail" VALUES (44, 12, 'Retur Pembelian', 'ReturPembelian');
INSERT INTO "public"."tbl_menu_detail" VALUES (41, 12, 'Penerimaan Barang Makloon Jahit', 'PenerimaanBarangJahit');
INSERT INTO "public"."tbl_menu_detail" VALUES (45, 12, 'Surat Jalan Makloon Celup', 'MakloonCelup');
INSERT INTO "public"."tbl_menu_detail" VALUES (46, 12, 'Surat Jalan Makloon Jahit', 'MakloonJahit');
INSERT INTO "public"."tbl_menu_detail" VALUES (1, 17, 'Agen', 'Agen/tambah_agen');
INSERT INTO "public"."tbl_menu_detail" VALUES (27, 17, 'User', 'User');
INSERT INTO "public"."tbl_menu_detail" VALUES (13, 17, 'Group User', 'GroupUser');
INSERT INTO "public"."tbl_menu_detail" VALUES (20, 17, 'Menu Detail', 'MenuDetail');
INSERT INTO "public"."tbl_menu_detail" VALUES (65, 1, 'Group COA', 'GroupCOA');
INSERT INTO "public"."tbl_menu_detail" VALUES (66, 18, 'Koreksi Persediaan', 'KoreksiPersediaan');
INSERT INTO "public"."tbl_menu_detail" VALUES (81, 19, 'Invoice Penjualan Non Kain', 'InvoicePenjualanNonKain');
INSERT INTO "public"."tbl_menu_detail" VALUES (82, 19, 'Retur Penjualan', 'ReturPenjualan');
INSERT INTO "public"."tbl_menu_detail" VALUES (83, 19, 'Retur Penjualan Non Kain', 'ReturPenjualanNonKain');
INSERT INTO "public"."tbl_menu_detail" VALUES (84, 19, 'Sample Kain', 'SampleKain');
INSERT INTO "public"."tbl_menu_detail" VALUES (79, 19, 'Surat Jalan Non Kain', 'SuratJalanSeragam');
INSERT INTO "public"."tbl_menu_detail" VALUES (85, 20, 'Mutasi Giro', 'MutasiGiro');
INSERT INTO "public"."tbl_menu_detail" VALUES (86, 20, 'Pembayaran Hutang', 'PembayaranHutang');
INSERT INTO "public"."tbl_menu_detail" VALUES (87, 20, 'Penerimaan Piutang', 'PenerimaanPiutang');
INSERT INTO "public"."tbl_menu_detail" VALUES (88, 20, 'Jurnal Umum', 'JurnalUmum');
INSERT INTO "public"."tbl_menu_detail" VALUES (89, 20, 'Buku Kas', 'BukuKas');
INSERT INTO "public"."tbl_menu_detail" VALUES (90, 20, 'Buku Bank', 'BukuBank');
INSERT INTO "public"."tbl_menu_detail" VALUES (91, 15, 'Laporan SO Kain', 'So/laporan_so');
INSERT INTO "public"."tbl_menu_detail" VALUES (92, 15, 'Laporan SO Non Kain', 'Sos/laporan_sos');
INSERT INTO "public"."tbl_menu_detail" VALUES (93, 15, 'Laporan Packing List', 'PackingList/laporan_packing_list');
INSERT INTO "public"."tbl_menu_detail" VALUES (94, 15, 'Laporan Invoice Penjualan Kain', 'InvoicePenjualan/laporan_inv_kain');
INSERT INTO "public"."tbl_menu_detail" VALUES (95, 15, 'Laporan Invoice Penjualan Non Kain', 'InvoicePenjualanNonKain/laporan_inv_nonkain');
INSERT INTO "public"."tbl_menu_detail" VALUES (96, 15, 'Laporan Retur Penjualan Kain', 'ReturPenjualan/laporan_retur_kain');
INSERT INTO "public"."tbl_menu_detail" VALUES (97, 15, 'Laporan Retur Penjualan Non Kain', 'ReturPenjualanNonKain/laporan_retur_nonkain');
INSERT INTO "public"."tbl_menu_detail" VALUES (99, 15, 'Laporan Surat Jalan Non Kain', 'SuratJalanSeragam/laporan_surat_jalan_non_kain');
INSERT INTO "public"."tbl_menu_detail" VALUES (71, 16, 'Saldo Awal Account', 'SaldoAwalAcount');
INSERT INTO "public"."tbl_menu_detail" VALUES (2, 1, 'Asset', 'Assets');
INSERT INTO "public"."tbl_menu_detail" VALUES (3, 1, 'Bank', 'Bank');
INSERT INTO "public"."tbl_menu_detail" VALUES (4, 1, 'Barang', 'Barang');
INSERT INTO "public"."tbl_menu_detail" VALUES (5, 1, 'COA', 'Coa');
INSERT INTO "public"."tbl_menu_detail" VALUES (6, 1, 'Corak', 'Corak');
INSERT INTO "public"."tbl_menu_detail" VALUES (7, 1, 'Customer', 'Customer');
INSERT INTO "public"."tbl_menu_detail" VALUES (9, 1, 'Group Asset', 'GroupAsset');
INSERT INTO "public"."tbl_menu_detail" VALUES (10, 1, 'Group Barang', 'GroupBarang');
INSERT INTO "public"."tbl_menu_detail" VALUES (11, 1, 'Group Customer', 'GroupCustomer');
INSERT INTO "public"."tbl_menu_detail" VALUES (12, 1, 'Group Supplier', 'GroupSupplier');
INSERT INTO "public"."tbl_menu_detail" VALUES (14, 1, 'Gudang', 'Gudang');
INSERT INTO "public"."tbl_menu_detail" VALUES (21, 1, 'Merk', 'Merk');
INSERT INTO "public"."tbl_menu_detail" VALUES (24, 1, 'Satuan', 'Satuan');
INSERT INTO "public"."tbl_menu_detail" VALUES (25, 1, 'Supplier', 'Supplier');
INSERT INTO "public"."tbl_menu_detail" VALUES (68, 18, 'Laporan Umur Persediaan', 'LapUmurPersediaan');
INSERT INTO "public"."tbl_menu_detail" VALUES (28, 1, 'Warna', 'Warna');
INSERT INTO "public"."tbl_menu_detail" VALUES (36, 12, 'Purchase Order Kain Non Trisula', 'PoUmum');
INSERT INTO "public"."tbl_menu_detail" VALUES (34, 12, 'Booking Order', 'BookingOrder');
INSERT INTO "public"."tbl_menu_detail" VALUES (75, 19, 'Sales Order', 'So');
INSERT INTO "public"."tbl_menu_detail" VALUES (19, 17, 'Menu', 'Menu');
INSERT INTO "public"."tbl_menu_detail" VALUES (73, 18, 'Laporan Stok Opname', 'StokOpName/laporan_stokopname');
INSERT INTO "public"."tbl_menu_detail" VALUES (72, 18, 'Stok Opname', 'StokOpName');
INSERT INTO "public"."tbl_menu_detail" VALUES (72, 18, 'Stok Opname', 'StokOpName');
INSERT INTO "public"."tbl_menu_detail" VALUES (76, 19, 'Sales Order Non Kain', 'Sos');
INSERT INTO "public"."tbl_menu_detail" VALUES (77, 19, 'Packing List', 'PackingList');
INSERT INTO "public"."tbl_menu_detail" VALUES (78, 19, 'Surat Jalan', 'SuratJalan');
INSERT INTO "public"."tbl_menu_detail" VALUES (35, 12, 'Purchase Order', 'Po');
INSERT INTO "public"."tbl_menu_detail" VALUES (60, 15, 'Laporan Invoice Pembelian', 'Laporan_invoice_pembelian/index');
INSERT INTO "public"."tbl_menu_detail" VALUES (62, 15, 'Laporan Pembelian Asset', 'Laporan_pembelian_asset/index');
INSERT INTO "public"."tbl_menu_detail" VALUES (59, 15, 'Laporan Penerimaan Barang', 'Laporan_penerimaan_barang/index');
INSERT INTO "public"."tbl_menu_detail" VALUES (56, 15, 'Laporan PO', 'PO/laporan_po');
INSERT INTO "public"."tbl_menu_detail" VALUES (57, 15, 'Laporan PO Celup', 'POCelup/laporan_po');
INSERT INTO "public"."tbl_menu_detail" VALUES (58, 15, 'Laporan PO Umum', 'POUmum/laporan_po');
INSERT INTO "public"."tbl_menu_detail" VALUES (98, 15, 'Laporan Surat Jalan Kain', 'SuratJalan/laporan_surat_jalan_kain');
INSERT INTO "public"."tbl_menu_detail" VALUES (64, 15, 'Laporan Retur Pembelian', 'ReturPembelian/laporan_retur_pembelian');
INSERT INTO "public"."tbl_menu_detail" VALUES (16, 16, 'Hutang', 'Hutang');
INSERT INTO "public"."tbl_menu_detail" VALUES (22, 16, 'Piutang', 'Piutang');
INSERT INTO "public"."tbl_menu_detail" VALUES (8, 16, 'Giro Masuk', 'Giro');
INSERT INTO "public"."tbl_menu_detail" VALUES (32, 16, 'Giro Keluar', 'Giro/index_giro_keluar');
INSERT INTO "public"."tbl_menu_detail" VALUES (26, 16, 'UM Customer', 'UMCustomer');
INSERT INTO "public"."tbl_menu_detail" VALUES (23, 16, 'Saldo Awal Asset', 'SaldoAwalAsset');
INSERT INTO "public"."tbl_menu_detail" VALUES (55, 15, 'Laporan BO', 'Laporan_bo/index');
INSERT INTO "public"."tbl_menu_detail" VALUES (80, 19, 'Invoice Penjualan', 'InvoicePenjualan');
INSERT INTO "public"."tbl_menu_detail" VALUES (37, 12, 'Purchase Order Celup', 'PoCelup');
INSERT INTO "public"."tbl_menu_detail" VALUES (38, 12, 'Penerimaan Barang', 'PenerimaanBarang');
INSERT INTO "public"."tbl_menu_detail" VALUES (39, 12, 'Scan Penerimaan Barang', 'scan_penerimaan_barang');
INSERT INTO "public"."tbl_menu_detail" VALUES (42, 12, 'Invoice Pembelian', 'InvoicePembelian');
INSERT INTO "public"."tbl_menu_detail" VALUES (43, 12, 'Intruksi Pengiriman', 'InstruksiPengiriman');
INSERT INTO "public"."tbl_menu_detail" VALUES (44, 12, 'Retur Pembelian', 'ReturPembelian');
INSERT INTO "public"."tbl_menu_detail" VALUES (41, 12, 'Penerimaan Barang Makloon Jahit', 'PenerimaanBarangJahit');
INSERT INTO "public"."tbl_menu_detail" VALUES (45, 12, 'Surat Jalan Makloon Celup', 'MakloonCelup');
INSERT INTO "public"."tbl_menu_detail" VALUES (46, 12, 'Surat Jalan Makloon Jahit', 'MakloonJahit');
INSERT INTO "public"."tbl_menu_detail" VALUES (1, 17, 'Agen', 'Agen/tambah_agen');
INSERT INTO "public"."tbl_menu_detail" VALUES (27, 17, 'User', 'User');
INSERT INTO "public"."tbl_menu_detail" VALUES (13, 17, 'Group User', 'GroupUser');
INSERT INTO "public"."tbl_menu_detail" VALUES (20, 17, 'Menu Detail', 'MenuDetail');
INSERT INTO "public"."tbl_menu_detail" VALUES (65, 1, 'Group COA', 'GroupCOA');
INSERT INTO "public"."tbl_menu_detail" VALUES (66, 18, 'Koreksi Persediaan', 'KoreksiPersediaan');
INSERT INTO "public"."tbl_menu_detail" VALUES (81, 19, 'Invoice Penjualan Non Kain', 'InvoicePenjualanNonKain');
INSERT INTO "public"."tbl_menu_detail" VALUES (82, 19, 'Retur Penjualan', 'ReturPenjualan');
INSERT INTO "public"."tbl_menu_detail" VALUES (83, 19, 'Retur Penjualan Non Kain', 'ReturPenjualanNonKain');
INSERT INTO "public"."tbl_menu_detail" VALUES (84, 19, 'Sample Kain', 'SampleKain');
INSERT INTO "public"."tbl_menu_detail" VALUES (79, 19, 'Surat Jalan Non Kain', 'SuratJalanSeragam');
INSERT INTO "public"."tbl_menu_detail" VALUES (85, 20, 'Mutasi Giro', 'MutasiGiro');
INSERT INTO "public"."tbl_menu_detail" VALUES (86, 20, 'Pembayaran Hutang', 'PembayaranHutang');
INSERT INTO "public"."tbl_menu_detail" VALUES (87, 20, 'Penerimaan Piutang', 'PenerimaanPiutang');
INSERT INTO "public"."tbl_menu_detail" VALUES (88, 20, 'Jurnal Umum', 'JurnalUmum');
INSERT INTO "public"."tbl_menu_detail" VALUES (89, 20, 'Buku Kas', 'BukuKas');
INSERT INTO "public"."tbl_menu_detail" VALUES (90, 20, 'Buku Bank', 'BukuBank');
INSERT INTO "public"."tbl_menu_detail" VALUES (91, 15, 'Laporan SO Kain', 'So/laporan_so');
INSERT INTO "public"."tbl_menu_detail" VALUES (92, 15, 'Laporan SO Non Kain', 'Sos/laporan_sos');
INSERT INTO "public"."tbl_menu_detail" VALUES (93, 15, 'Laporan Packing List', 'PackingList/laporan_packing_list');
INSERT INTO "public"."tbl_menu_detail" VALUES (94, 15, 'Laporan Invoice Penjualan Kain', 'InvoicePenjualan/laporan_inv_kain');
INSERT INTO "public"."tbl_menu_detail" VALUES (95, 15, 'Laporan Invoice Penjualan Non Kain', 'InvoicePenjualanNonKain/laporan_inv_nonkain');
INSERT INTO "public"."tbl_menu_detail" VALUES (96, 15, 'Laporan Retur Penjualan Kain', 'ReturPenjualan/laporan_retur_kain');
INSERT INTO "public"."tbl_menu_detail" VALUES (97, 15, 'Laporan Retur Penjualan Non Kain', 'ReturPenjualanNonKain/laporan_retur_nonkain');
INSERT INTO "public"."tbl_menu_detail" VALUES (99, 15, 'Laporan Surat Jalan Non Kain', 'SuratJalanSeragam/laporan_surat_jalan_non_kain');
INSERT INTO "public"."tbl_menu_detail" VALUES (104, 19, 'Down Payment', 'DownPayment');
INSERT INTO "public"."tbl_menu_detail" VALUES (47, 12, 'Pendaftaran Asset', 'Pembelian_asset');
INSERT INTO "public"."tbl_menu_detail" VALUES (47, 12, 'Pendaftaran Asset', 'Pembelian_asset');
INSERT INTO "public"."tbl_menu_detail" VALUES (36, 12, 'Purchase Order Kain Non Trisula', 'PoUmum');
INSERT INTO "public"."tbl_menu_detail" VALUES (105, 12, 'Purchase Order Umum', 'PoUmum2');
INSERT INTO "public"."tbl_menu_detail" VALUES (40, 12, 'Penerimaan Barang Kain Non Trisula', 'penerimaan_barang_umum');
INSERT INTO "public"."tbl_menu_detail" VALUES (40, 12, 'Penerimaan Barang Kain Non Trisula', 'penerimaan_barang_umum');
INSERT INTO "public"."tbl_menu_detail" VALUES (106, 12, 'Penerimaan Barang Umum', 'Penerimaan_barang_umum2');
INSERT INTO "public"."tbl_menu_detail" VALUES (107, 12, 'Invoice Pembelian Umum', 'Invoice_pembelian_umum');
INSERT INTO "public"."tbl_menu_detail" VALUES (111, 20, 'Laporan Notes', 'SettingLR/index_catatan_lapkeu');
INSERT INTO "public"."tbl_menu_detail" VALUES (112, 20, 'Laporan AR', 'SettingLR/index_aging_schedule');
INSERT INTO "public"."tbl_menu_detail" VALUES (113, 20, 'Laporan Dibayar Dimuka', 'SettingLR/index_dibayar_dimuka');
INSERT INTO "public"."tbl_menu_detail" VALUES (114, 20, 'Laporan Aset Tetap', 'SettingLR/index_asset_tetap');
INSERT INTO "public"."tbl_menu_detail" VALUES (115, 20, 'Laporan AP', 'SettingLR/index_mutasi_utang_usaha');
INSERT INTO "public"."tbl_menu_detail" VALUES (19, 17, 'Menu', 'Menu');
INSERT INTO "public"."tbl_menu_detail" VALUES (19, 17, 'Menu', 'Menu');
INSERT INTO "public"."tbl_menu_detail" VALUES (19, 17, 'Menu', 'Menu');
INSERT INTO "public"."tbl_menu_detail" VALUES (73, 18, 'Laporan Stok Opname', 'StokOpName/laporan_stokopname');
INSERT INTO "public"."tbl_menu_detail" VALUES (119, 26, 'Setting Neraca', 'SettingLR/tambah_setting_lr');
INSERT INTO "public"."tbl_menu_detail" VALUES (108, 20, 'Laporan Neraca', 'SettingLR/');
INSERT INTO "public"."tbl_menu_detail" VALUES (116, 20, 'Laporan BIUS', 'SettingLR/index_beban_operasional');
INSERT INTO "public"."tbl_menu_detail" VALUES (117, 20, 'Laporan Buku Besar Pembantu', 'SettingLR/index_buku_besar_pembantu');
INSERT INTO "public"."tbl_menu_detail" VALUES (109, 20, 'Laporan PL', 'SettingLR/index_laba_rugi');
INSERT INTO "public"."tbl_menu_detail" VALUES (110, 20, 'Laporan CF', 'SettingLR/index_arus_kas');
INSERT INTO "public"."tbl_menu_detail" VALUES (118, 20, 'Laporan Buku Besar', 'SettingLR/index_buku_besar');
INSERT INTO "public"."tbl_menu_detail" VALUES (120, 26, 'Setting PL', 'SettingLR/tambah_setting_pl');
INSERT INTO "public"."tbl_menu_detail" VALUES (121, 26, 'Setting CF', 'SettingLR/tambah_setting_cf');
INSERT INTO "public"."tbl_menu_detail" VALUES (122, 26, 'Setting Notes', 'SettingLR/tambah_setting_notes');

-- ----------------------------
-- Table structure for tbl_menu_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu_role";
CREATE TABLE "public"."tbl_menu_role" (
  "IDMenuRole" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDMenuRole_seq"'::regclass),
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDGroupUser_seq"'::regclass),
  "IDMenuDetail" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDMenuDetail_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_menu_role
-- ----------------------------
INSERT INTO "public"."tbl_menu_role" VALUES (2833, 5, 15);
INSERT INTO "public"."tbl_menu_role" VALUES (2834, 5, 2);
INSERT INTO "public"."tbl_menu_role" VALUES (2835, 5, 3);
INSERT INTO "public"."tbl_menu_role" VALUES (2836, 5, 4);
INSERT INTO "public"."tbl_menu_role" VALUES (2837, 5, 5);
INSERT INTO "public"."tbl_menu_role" VALUES (2838, 5, 6);
INSERT INTO "public"."tbl_menu_role" VALUES (2839, 5, 7);
INSERT INTO "public"."tbl_menu_role" VALUES (2840, 5, 9);
INSERT INTO "public"."tbl_menu_role" VALUES (2841, 5, 10);
INSERT INTO "public"."tbl_menu_role" VALUES (2842, 5, 11);
INSERT INTO "public"."tbl_menu_role" VALUES (2843, 5, 12);
INSERT INTO "public"."tbl_menu_role" VALUES (2844, 5, 14);
INSERT INTO "public"."tbl_menu_role" VALUES (2845, 5, 21);
INSERT INTO "public"."tbl_menu_role" VALUES (2846, 5, 24);
INSERT INTO "public"."tbl_menu_role" VALUES (2847, 5, 25);
INSERT INTO "public"."tbl_menu_role" VALUES (2848, 5, 28);
INSERT INTO "public"."tbl_menu_role" VALUES (2849, 5, 65);
INSERT INTO "public"."tbl_menu_role" VALUES (2850, 5, 2);
INSERT INTO "public"."tbl_menu_role" VALUES (2851, 5, 3);
INSERT INTO "public"."tbl_menu_role" VALUES (2852, 5, 4);
INSERT INTO "public"."tbl_menu_role" VALUES (2853, 5, 5);
INSERT INTO "public"."tbl_menu_role" VALUES (2854, 5, 6);
INSERT INTO "public"."tbl_menu_role" VALUES (2855, 5, 7);
INSERT INTO "public"."tbl_menu_role" VALUES (2856, 5, 9);
INSERT INTO "public"."tbl_menu_role" VALUES (2857, 5, 10);
INSERT INTO "public"."tbl_menu_role" VALUES (2858, 5, 11);
INSERT INTO "public"."tbl_menu_role" VALUES (2859, 5, 12);
INSERT INTO "public"."tbl_menu_role" VALUES (2860, 5, 14);
INSERT INTO "public"."tbl_menu_role" VALUES (2861, 5, 21);
INSERT INTO "public"."tbl_menu_role" VALUES (2862, 5, 24);
INSERT INTO "public"."tbl_menu_role" VALUES (2863, 5, 25);
INSERT INTO "public"."tbl_menu_role" VALUES (2864, 5, 28);
INSERT INTO "public"."tbl_menu_role" VALUES (2865, 5, 65);
INSERT INTO "public"."tbl_menu_role" VALUES (2866, 5, 34);
INSERT INTO "public"."tbl_menu_role" VALUES (2867, 5, 35);
INSERT INTO "public"."tbl_menu_role" VALUES (2868, 5, 37);
INSERT INTO "public"."tbl_menu_role" VALUES (2869, 5, 38);
INSERT INTO "public"."tbl_menu_role" VALUES (2870, 5, 39);
INSERT INTO "public"."tbl_menu_role" VALUES (2871, 5, 42);
INSERT INTO "public"."tbl_menu_role" VALUES (2872, 5, 43);
INSERT INTO "public"."tbl_menu_role" VALUES (2873, 5, 44);
INSERT INTO "public"."tbl_menu_role" VALUES (2874, 5, 41);
INSERT INTO "public"."tbl_menu_role" VALUES (2875, 5, 45);
INSERT INTO "public"."tbl_menu_role" VALUES (2876, 5, 46);
INSERT INTO "public"."tbl_menu_role" VALUES (2877, 5, 36);
INSERT INTO "public"."tbl_menu_role" VALUES (2878, 5, 34);
INSERT INTO "public"."tbl_menu_role" VALUES (2879, 5, 35);
INSERT INTO "public"."tbl_menu_role" VALUES (2880, 5, 37);
INSERT INTO "public"."tbl_menu_role" VALUES (2881, 5, 38);
INSERT INTO "public"."tbl_menu_role" VALUES (2882, 5, 39);
INSERT INTO "public"."tbl_menu_role" VALUES (2883, 5, 42);
INSERT INTO "public"."tbl_menu_role" VALUES (2884, 5, 43);
INSERT INTO "public"."tbl_menu_role" VALUES (2885, 5, 44);
INSERT INTO "public"."tbl_menu_role" VALUES (2886, 5, 41);
INSERT INTO "public"."tbl_menu_role" VALUES (2887, 5, 45);
INSERT INTO "public"."tbl_menu_role" VALUES (2888, 5, 46);
INSERT INTO "public"."tbl_menu_role" VALUES (2889, 5, 47);
INSERT INTO "public"."tbl_menu_role" VALUES (2890, 5, 47);
INSERT INTO "public"."tbl_menu_role" VALUES (2891, 5, 36);
INSERT INTO "public"."tbl_menu_role" VALUES (2892, 5, 105);
INSERT INTO "public"."tbl_menu_role" VALUES (2893, 5, 40);
INSERT INTO "public"."tbl_menu_role" VALUES (2894, 5, 40);
INSERT INTO "public"."tbl_menu_role" VALUES (2895, 5, 106);
INSERT INTO "public"."tbl_menu_role" VALUES (2896, 5, 107);
INSERT INTO "public"."tbl_menu_role" VALUES (2897, 5, 71);
INSERT INTO "public"."tbl_menu_role" VALUES (2898, 5, 16);
INSERT INTO "public"."tbl_menu_role" VALUES (2899, 5, 22);
INSERT INTO "public"."tbl_menu_role" VALUES (2900, 5, 8);
INSERT INTO "public"."tbl_menu_role" VALUES (2901, 5, 32);
INSERT INTO "public"."tbl_menu_role" VALUES (2902, 5, 26);
INSERT INTO "public"."tbl_menu_role" VALUES (2903, 5, 23);
INSERT INTO "public"."tbl_menu_role" VALUES (2904, 5, 71);
INSERT INTO "public"."tbl_menu_role" VALUES (2905, 5, 16);
INSERT INTO "public"."tbl_menu_role" VALUES (2906, 5, 22);
INSERT INTO "public"."tbl_menu_role" VALUES (2907, 5, 8);
INSERT INTO "public"."tbl_menu_role" VALUES (2908, 5, 32);
INSERT INTO "public"."tbl_menu_role" VALUES (2909, 5, 26);
INSERT INTO "public"."tbl_menu_role" VALUES (2910, 5, 23);
INSERT INTO "public"."tbl_menu_role" VALUES (2911, 5, 1);
INSERT INTO "public"."tbl_menu_role" VALUES (2912, 5, 27);
INSERT INTO "public"."tbl_menu_role" VALUES (2913, 5, 13);
INSERT INTO "public"."tbl_menu_role" VALUES (2914, 5, 20);
INSERT INTO "public"."tbl_menu_role" VALUES (2915, 5, 19);
INSERT INTO "public"."tbl_menu_role" VALUES (2916, 5, 1);
INSERT INTO "public"."tbl_menu_role" VALUES (2917, 5, 27);
INSERT INTO "public"."tbl_menu_role" VALUES (2918, 5, 13);
INSERT INTO "public"."tbl_menu_role" VALUES (2919, 5, 20);
INSERT INTO "public"."tbl_menu_role" VALUES (2920, 5, 19);
INSERT INTO "public"."tbl_menu_role" VALUES (2921, 5, 19);
INSERT INTO "public"."tbl_menu_role" VALUES (2922, 5, 19);
INSERT INTO "public"."tbl_menu_role" VALUES (2923, 5, 67);
INSERT INTO "public"."tbl_menu_role" VALUES (2924, 5, 69);
INSERT INTO "public"."tbl_menu_role" VALUES (2925, 5, 70);
INSERT INTO "public"."tbl_menu_role" VALUES (2926, 5, 68);
INSERT INTO "public"."tbl_menu_role" VALUES (2927, 5, 66);
INSERT INTO "public"."tbl_menu_role" VALUES (2928, 5, 68);
INSERT INTO "public"."tbl_menu_role" VALUES (2929, 5, 73);
INSERT INTO "public"."tbl_menu_role" VALUES (2930, 5, 72);
INSERT INTO "public"."tbl_menu_role" VALUES (2931, 5, 72);
INSERT INTO "public"."tbl_menu_role" VALUES (2932, 5, 66);
INSERT INTO "public"."tbl_menu_role" VALUES (2933, 5, 73);
INSERT INTO "public"."tbl_menu_role" VALUES (2934, 5, 75);
INSERT INTO "public"."tbl_menu_role" VALUES (2935, 5, 76);
INSERT INTO "public"."tbl_menu_role" VALUES (2936, 5, 77);
INSERT INTO "public"."tbl_menu_role" VALUES (2937, 5, 78);
INSERT INTO "public"."tbl_menu_role" VALUES (2938, 5, 80);
INSERT INTO "public"."tbl_menu_role" VALUES (2939, 5, 81);
INSERT INTO "public"."tbl_menu_role" VALUES (2940, 5, 82);
INSERT INTO "public"."tbl_menu_role" VALUES (2941, 5, 83);
INSERT INTO "public"."tbl_menu_role" VALUES (2942, 5, 84);
INSERT INTO "public"."tbl_menu_role" VALUES (2943, 5, 79);
INSERT INTO "public"."tbl_menu_role" VALUES (2944, 5, 75);
INSERT INTO "public"."tbl_menu_role" VALUES (2945, 5, 76);
INSERT INTO "public"."tbl_menu_role" VALUES (2946, 5, 77);
INSERT INTO "public"."tbl_menu_role" VALUES (2947, 5, 78);
INSERT INTO "public"."tbl_menu_role" VALUES (2948, 5, 80);
INSERT INTO "public"."tbl_menu_role" VALUES (2949, 5, 81);
INSERT INTO "public"."tbl_menu_role" VALUES (2950, 5, 82);
INSERT INTO "public"."tbl_menu_role" VALUES (2951, 5, 83);
INSERT INTO "public"."tbl_menu_role" VALUES (2952, 5, 84);
INSERT INTO "public"."tbl_menu_role" VALUES (2953, 5, 79);
INSERT INTO "public"."tbl_menu_role" VALUES (2954, 5, 104);
INSERT INTO "public"."tbl_menu_role" VALUES (2955, 5, 85);
INSERT INTO "public"."tbl_menu_role" VALUES (2956, 5, 86);
INSERT INTO "public"."tbl_menu_role" VALUES (2957, 5, 87);
INSERT INTO "public"."tbl_menu_role" VALUES (2958, 5, 88);
INSERT INTO "public"."tbl_menu_role" VALUES (2959, 5, 89);
INSERT INTO "public"."tbl_menu_role" VALUES (2960, 5, 90);
INSERT INTO "public"."tbl_menu_role" VALUES (2961, 5, 85);
INSERT INTO "public"."tbl_menu_role" VALUES (2962, 5, 86);
INSERT INTO "public"."tbl_menu_role" VALUES (2963, 5, 87);
INSERT INTO "public"."tbl_menu_role" VALUES (2964, 5, 88);
INSERT INTO "public"."tbl_menu_role" VALUES (2965, 5, 89);
INSERT INTO "public"."tbl_menu_role" VALUES (2966, 5, 90);
INSERT INTO "public"."tbl_menu_role" VALUES (2967, 5, 111);
INSERT INTO "public"."tbl_menu_role" VALUES (2968, 5, 112);
INSERT INTO "public"."tbl_menu_role" VALUES (2969, 5, 113);
INSERT INTO "public"."tbl_menu_role" VALUES (2970, 5, 114);
INSERT INTO "public"."tbl_menu_role" VALUES (2971, 5, 115);
INSERT INTO "public"."tbl_menu_role" VALUES (2972, 5, 116);
INSERT INTO "public"."tbl_menu_role" VALUES (2973, 5, 117);
INSERT INTO "public"."tbl_menu_role" VALUES (2974, 5, 108);
INSERT INTO "public"."tbl_menu_role" VALUES (2975, 5, 109);
INSERT INTO "public"."tbl_menu_role" VALUES (2976, 5, 110);
INSERT INTO "public"."tbl_menu_role" VALUES (2977, 5, 118);
INSERT INTO "public"."tbl_menu_role" VALUES (2978, 5, 63);
INSERT INTO "public"."tbl_menu_role" VALUES (2979, 5, 60);
INSERT INTO "public"."tbl_menu_role" VALUES (2980, 5, 62);
INSERT INTO "public"."tbl_menu_role" VALUES (2981, 5, 59);
INSERT INTO "public"."tbl_menu_role" VALUES (2982, 5, 56);
INSERT INTO "public"."tbl_menu_role" VALUES (2983, 5, 57);
INSERT INTO "public"."tbl_menu_role" VALUES (2984, 5, 58);
INSERT INTO "public"."tbl_menu_role" VALUES (2985, 5, 98);
INSERT INTO "public"."tbl_menu_role" VALUES (2986, 5, 64);
INSERT INTO "public"."tbl_menu_role" VALUES (2987, 5, 55);
INSERT INTO "public"."tbl_menu_role" VALUES (2988, 5, 91);
INSERT INTO "public"."tbl_menu_role" VALUES (2989, 5, 92);
INSERT INTO "public"."tbl_menu_role" VALUES (2990, 5, 93);
INSERT INTO "public"."tbl_menu_role" VALUES (2991, 5, 94);
INSERT INTO "public"."tbl_menu_role" VALUES (2992, 5, 95);
INSERT INTO "public"."tbl_menu_role" VALUES (2993, 5, 96);
INSERT INTO "public"."tbl_menu_role" VALUES (2994, 5, 97);
INSERT INTO "public"."tbl_menu_role" VALUES (2995, 5, 99);
INSERT INTO "public"."tbl_menu_role" VALUES (2996, 5, 60);
INSERT INTO "public"."tbl_menu_role" VALUES (2997, 5, 62);
INSERT INTO "public"."tbl_menu_role" VALUES (2998, 5, 59);
INSERT INTO "public"."tbl_menu_role" VALUES (2999, 5, 56);
INSERT INTO "public"."tbl_menu_role" VALUES (3000, 5, 57);
INSERT INTO "public"."tbl_menu_role" VALUES (3001, 5, 58);
INSERT INTO "public"."tbl_menu_role" VALUES (3002, 5, 98);
INSERT INTO "public"."tbl_menu_role" VALUES (3003, 5, 64);
INSERT INTO "public"."tbl_menu_role" VALUES (3004, 5, 55);
INSERT INTO "public"."tbl_menu_role" VALUES (3005, 5, 91);
INSERT INTO "public"."tbl_menu_role" VALUES (3006, 5, 92);
INSERT INTO "public"."tbl_menu_role" VALUES (3007, 5, 93);
INSERT INTO "public"."tbl_menu_role" VALUES (3008, 5, 94);
INSERT INTO "public"."tbl_menu_role" VALUES (3009, 5, 95);
INSERT INTO "public"."tbl_menu_role" VALUES (3010, 5, 96);
INSERT INTO "public"."tbl_menu_role" VALUES (3011, 5, 97);
INSERT INTO "public"."tbl_menu_role" VALUES (3012, 5, 99);
INSERT INTO "public"."tbl_menu_role" VALUES (3013, 5, 119);
INSERT INTO "public"."tbl_menu_role" VALUES (3014, 5, 120);
INSERT INTO "public"."tbl_menu_role" VALUES (3015, 5, 121);
INSERT INTO "public"."tbl_menu_role" VALUES (3016, 5, 122);

-- ----------------------------
-- Table structure for tbl_merk
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_merk";
CREATE TABLE "public"."tbl_merk" (
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_merk_IDMerk_seq"'::regclass),
  "Kode_Merk" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Merk" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_merk
-- ----------------------------
INSERT INTO "public"."tbl_merk" VALUES ('P000001', 'ds', 'consina', 'aktif');
INSERT INTO "public"."tbl_merk" VALUES ('P000002', 'dsd', 'lokis', 'aktif');
INSERT INTO "public"."tbl_merk" VALUES ('P000003', '9j', 'mosi', 'aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0001', 'HB', 'Hugo Black Stripe', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0002', 'HBsld', 'Hugo Black Solid', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0003', 'MM', 'Maxmoda Stripe', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0004', 'Mmsld', 'Maxmoda Solid', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0005', 'MORAFILL', 'Morafill', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0006', 'MORAFILL NS', 'Morafill Non Sulam', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0007', 'PORSEUS ', 'Porseus', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0008', 'PORSEUS NS', 'Porseus Non Sulam', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0009', 'CARV', 'Carvello', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0010', 'RGIOR', 'Ralph Gior', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0011', 'CMODA', 'Classmoda', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0012', 'SIIP', 'Bellini Siip', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0013', 'CHLOU', 'Chez louis', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0014', 'ECL', 'Eclusiva Twill', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0015', 'LB', 'Libero', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0016', 'AQUAV', 'Aquaviva', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0017', 'ECL PRM', 'Eclusiva Premier', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0018', 'VIS', 'Neo Vissero', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0019', 'BW', 'Black Wool', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0020', 'ZL', 'Zibellius', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0021', 'MJB', 'Maxmoda JB', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0022', 'TR', 'Tiera', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0023', 'Mmatrix', 'Macro Matrix', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0024', 'WP', 'Wool Plaid', 'Aktif');
INSERT INTO "public"."tbl_merk" VALUES ('Prk0025', 'GB', 'Goldbrown', 'Aktif');

-- ----------------------------
-- Table structure for tbl_mutasi_giro
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mutasi_giro";
CREATE TABLE "public"."tbl_mutasi_giro" (
  "IDMG" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_mutasi_giro_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mutasi_giro_detail";
CREATE TABLE "public"."tbl_mutasi_giro_detail" (
  "IDMGDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDMG" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGiro" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_lama" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_baru" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_out
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_out";
CREATE TABLE "public"."tbl_out" (
  "IDOut" int8 NOT NULL DEFAULT nextval('"tbl_out_IDOut_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Buyer" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "CustDes" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "WarnaCust" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Panjang_Yard" float8 DEFAULT NULL,
  "Panjang_Meter" float8 DEFAULT NULL,
  "Grade" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_packing_list
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_packing_list";
CREATE TABLE "public"."tbl_packing_list" (
  "IDPAC" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSOK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_pcs_grade_a" float8 DEFAULT NULL,
  "Total_qty_grade_a" float8 DEFAULT NULL,
  "Total_pcs_grade_b" float8 DEFAULT NULL,
  "Total_qty_grade_b" float8 DEFAULT NULL,
  "Total_pcs_grade_s" float8 DEFAULT NULL,
  "Total_qty_grade_s" float8 DEFAULT NULL,
  "Total_pcs_grade_e" float8 DEFAULT NULL,
  "Total_qty_grade_e" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_packing_list
-- ----------------------------
INSERT INTO "public"."tbl_packing_list" VALUES ('P000001', '2019-01-16', 'PL19PMAK00001', 'P000001', 'P000004', 1, 1, 2, 400, 0, 0, 0, 0, 0, 0, 'test', 'aktif');

-- ----------------------------
-- Table structure for tbl_packing_list_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_packing_list_detail";
CREATE TABLE "public"."tbl_packing_list_detail" (
  "IDPACDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDPAC" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_packing_list_detail
-- ----------------------------
INSERT INTO "public"."tbl_packing_list_detail" VALUES ('P000001', 'P000001', 'P000001', '34657189', 'P000001', 'Prk0001', 'P000001', 200, 199.5, 200, 199.5, 'A', 'ST0001');
INSERT INTO "public"."tbl_packing_list_detail" VALUES ('P000002', 'P000001', 'P000001', '90881799', 'P000001', 'Prk0001', 'P000001', 200, 199.5, 200, 199.5, 'A', 'ST0001');

-- ----------------------------
-- Table structure for tbl_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran";
CREATE TABLE "public"."tbl_pembayaran" (
  "IDFBPembayaran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDFBPembayaran_seq"'::regclass),
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDFB_seq"'::regclass),
  "Jenis_pembayaran" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDCOA_seq"'::regclass),
  "NominalPembayaran" float8 DEFAULT NULL,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_pembayaran_IDMataUang_seq"'::regclass),
  "Kurs" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_Giro" date DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang";
CREATE TABLE "public"."tbl_pembayaran_hutang" (
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total" float8 DEFAULT NULL,
  "Uang_muka" float8 DEFAULT NULL,
  "Selisih" float8 DEFAULT NULL,
  "Kompensasi_um" float8 DEFAULT NULL,
  "Pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kelebihan_bayar" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_pembayaran_hutang
-- ----------------------------
INSERT INTO "public"."tbl_pembayaran_hutang" VALUES ('P000001', '2019-01-31', 'PH19PMAK00001', 'P000004', 1, 14000, 8800000000, 0, NULL, 0, '200000000', 0, 'test', 'aktif');

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang_bayar
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang_bayar";
CREATE TABLE "public"."tbl_pembayaran_hutang_bayar" (
  "IDBSBayar" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nominal_pembayaran" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Status_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_giro" date DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_pembayaran_hutang_bayar
-- ----------------------------
INSERT INTO "public"."tbl_pembayaran_hutang_bayar" VALUES ('P000001', 'P000001', 'Transfer', 'P000008', NULL, NULL, 1, 14000, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang_detail";
CREATE TABLE "public"."tbl_pembayaran_hutang_detail" (
  "IDBSDet" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fb" date DEFAULT NULL,
  "Nilai_fb" float8 DEFAULT NULL,
  "Telah_diterima" float8 DEFAULT NULL,
  "Diterima" float8 DEFAULT NULL,
  "UM" float8 DEFAULT NULL,
  "Retur" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "Selisih" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "IDHutang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_pembayaran_hutang_detail
-- ----------------------------
INSERT INTO "public"."tbl_pembayaran_hutang_detail" VALUES ('P000001', 'P000001', 'P000002', 'INV/19/PMAK/00002', '2019-01-18', NULL, 8800000000, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'P000002');

-- ----------------------------
-- Table structure for tbl_pembelian
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian";
CREATE TABLE "public"."tbl_pembelian" (
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDFB_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDTBS_seq"'::regclass),
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDSupplier_seq"'::regclass),
  "No_sj_supplier" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_jatuh_tempo" date DEFAULT NULL,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_pembelian_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty_yard" float8 DEFAULT NULL,
  "Total_qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_pembelian
-- ----------------------------
INSERT INTO "public"."tbl_pembelian" VALUES ('P000001', '2019-01-17', 'INV/19/PMAK/00001', 'P000001', 'P000004', '023', '75', '2019-04-02', 6, '-', 600, 596, 600, 596, 0, 'Exclude', '-', 'aktif');
INSERT INTO "public"."tbl_pembelian" VALUES ('P000002', '2019-01-18', 'INV/19/PMAK/00002', 'P000001', 'P000004', '8878', '70', '2019-03-29', 6, '-', 400, 398, 400, 398, 0, 'Exclude', 'test', 'aktif');

-- ----------------------------
-- Table structure for tbl_pembelian_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_asset";
CREATE TABLE "public"."tbl_pembelian_asset" (
  "IDFBA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_IDFBA_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_pembelian_asset_IDMataUang_seq"'::regclass),
  "Kurs" varchar COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Total_nilai_perolehan" float8 DEFAULT NULL,
  "Total_akumulasi_penyusutan" float8 DEFAULT NULL,
  "Total_nilai_buku" float8 DEFAULT NULL,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_asset_detail";
CREATE TABLE "public"."tbl_pembelian_asset_detail" (
  "IDFBADetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDFBADetail_seq"'::regclass),
  "IDFBA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDFBA_seq"'::regclass),
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDAsset_seq"'::regclass),
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDGroupAsset_seq"'::regclass),
  "Nilai_perolehan" float8 DEFAULT NULL,
  "Akumulasi_penyusutan" float8 DEFAULT NULL,
  "Nilai_buku" float8 DEFAULT NULL,
  "Metode_penyusutan" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_penyusutan" date DEFAULT NULL,
  "Umur" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_detail";
CREATE TABLE "public"."tbl_pembelian_detail" (
  "IDFBDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDFBDetail_seq"'::regclass),
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDFB_seq"'::regclass),
  "IDTBSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDTBSDetail_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDCorak_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDMerk_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDWarna_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDSatuan_seq"'::regclass),
  "Harga" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_pembelian_detail
-- ----------------------------
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('P000001', 'P000001', '1', '77656786', 'SO90123', '1', '-', 'P000001', 'P000001', 'Prk0001', 'P000002', 100, 99.5, 100, 99.5, 'A', 'test', '123', 'ST0001', 4000000, 400000000);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('P000002', 'P000001', '1', '43566777', 'SO90123', '1', '-', 'P000001', 'P000001', 'Prk0001', 'P000002', 100, 99.5, 100, 99.5, 'A', 'test', '123', 'ST0001', 4000000, 400000000);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('P000003', 'P000001', '1', '90881799', 'SO90123', '1', '-', 'P000001', 'P000001', 'Prk0001', 'P000001', 200, 199.5, 200, 199.5, 'A', 'test', '123', 'ST0001', 12000000, 2400000000);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('P000004', 'P000001', '1', '34657189', 'SO90123', '1', '-', 'P000001', 'P000001', 'Prk0001', 'P000001', 200, 199.5, 200, 199.5, 'A', 'test', '123', 'ST0001', 12000000, 2400000000);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('P000005', 'P000002', '1', '88878278', 'SO90124', '1', '-', 'P000003', 'P000002', 'Prk0002', 'P000001', 200, 199.5, 200, 199.5, 'A', 'test', '123', 'ST0001', 20000000, 4000000000);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('P000006', 'P000002', '1', '09012301', 'SO90124', '1', '-', 'P000003', 'P000002', 'Prk0002', 'P000001', 200, 199.5, 200, 199.5, 'A', 'test', '123', 'ST0001', 20000000, 4000000000);

-- ----------------------------
-- Table structure for tbl_pembelian_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_grand_total";
CREATE TABLE "public"."tbl_pembelian_grand_total" (
  "IDFBGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_grand_total_IDFBGrandTotal_seq"'::regclass),
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "DPP" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Sisa" float8 DEFAULT NULL,
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_grand_total_IDFB_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_pembelian_grand_total
-- ----------------------------
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000001', '-', 32904000000, 0, 3656000000, 36560000000, 36560000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000002', '-', 32904000000, 0, 3656000000, 36560000000, 36560000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000003', '-', 1023750000, 0, 113750000, 1137500000, 1137500000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000004', '-', 1023750000, 0, 113750000, 1137500000, 1137500000, 'P000002');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000005', '-', 1023750000, 0, 113750000, 1137500000, 1137500000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000006', '-', 1023750000, 0, 113750000, 1137500000, 1137500000, 'P000002');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000007', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000008', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P000009', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000002');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000a', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000003');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000b', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000004');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000c', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000d', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000e', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000002');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000f', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000003');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000g', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000h', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000i', '-', 0, 0, 0, 0, 0, 'P000002');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000j', '-', 5600000000, 0, 560000000, 6160000000, 6160000000, 'P000001');
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('P00000k', '-', 8000000000, 20, 800000000, 8800000000, 8800000000, 'P000002');

-- ----------------------------
-- Table structure for tbl_pembelian_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum";
CREATE TABLE "public"."tbl_pembelian_umum" (
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status_ppn" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Persen_disc" float8 DEFAULT NULL,
  "Disc" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_detail";
CREATE TABLE "public"."tbl_pembelian_umum_detail" (
  "IDFBUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBSUmumDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_grand_total";
CREATE TABLE "public"."tbl_pembelian_umum_grand_total" (
  "IDFBUmumGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "DPP" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Sisa" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_pembelian_umum_grand_total
-- ----------------------------
INSERT INTO "public"."tbl_pembelian_umum_grand_total" VALUES ('P000001', 'P000001', 'Giro', 1, 14000, 180000000, 20, 20000000, 220000000, 220000000);

-- ----------------------------
-- Table structure for tbl_pembelian_umum_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_pembayaran";
CREATE TABLE "public"."tbl_pembelian_umum_pembayaran" (
  "IDFBUmumPembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalPembayaran" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Tanggal_giro" date DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang";
CREATE TABLE "public"."tbl_penerimaan_piutang" (
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total" float8 DEFAULT NULL,
  "Uang_muka" float8 DEFAULT NULL,
  "Selisih" float8 DEFAULT NULL,
  "Kompensasi_um" float8 DEFAULT NULL,
  "Pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kelebihan_bayar" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang_bayar
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang_bayar";
CREATE TABLE "public"."tbl_penerimaan_piutang_bayar" (
  "IDTPBayar" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nominal_pembayaran" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Status_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_giro" date DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang_detail";
CREATE TABLE "public"."tbl_penerimaan_piutang_detail" (
  "IDTPDet" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFJ" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fj" date DEFAULT NULL,
  "Nilai_fj" float8 DEFAULT NULL,
  "Telah_diterima" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Diterima" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "UM" float8 DEFAULT NULL,
  "Retur" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "Selisih" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "IDPiutang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_kain
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_kain";
CREATE TABLE "public"."tbl_penjualan_kain" (
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_di_faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSJCK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" int8 DEFAULT NULL,
  "Tanggal_jatuh_tempo" date DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_pieces" float8 DEFAULT NULL,
  "Total_yard" float8 DEFAULT NULL,
  "Saldo_pieces" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_penjualan_kain
-- ----------------------------
INSERT INTO "public"."tbl_penjualan_kain" VALUES ('P000001', '2019-01-16', 'INVPK19PMAK00001', 'P000004', 'dadang', 'P000001', NULL, '2019-04-01', 1, 14000, 400, 400, 400, 400, 0, 'Include', 'po', 'aktif');
INSERT INTO "public"."tbl_penjualan_kain" VALUES ('P000002', '2019-01-16', 'INVPK19PMAK00002', 'P000004', 'dada', 'P000001', NULL, '2019-04-01', 1, 14000, 400, 400, 400, 400, 0, 'Include', 'po', 'aktif');

-- ----------------------------
-- Table structure for tbl_penjualan_kain_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_kain_detail";
CREATE TABLE "public"."tbl_penjualan_kain_detail" (
  "IDFJKDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_roll" float8 DEFAULT NULL,
  "Saldo_qty_roll" float8 DEFAULT NULL,
  "Qty_yard" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_penjualan_kain_detail
-- ----------------------------
INSERT INTO "public"."tbl_penjualan_kain_detail" VALUES ('P000001', 'P000001', 'P000001', 'P000001', 'P000001', 400, 400, 400, 400, 'ST0001', 50000, 20000000);
INSERT INTO "public"."tbl_penjualan_kain_detail" VALUES ('P000002', 'P000002', 'P000001', 'P000001', 'P000001', 400, 400, 400, 400, 'ST0001', 50000, 20000000);

-- ----------------------------
-- Table structure for tbl_penjualan_kain_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_kain_grand_total";
CREATE TABLE "public"."tbl_penjualan_kain_grand_total" (
  "IDFJKGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "DPP" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Sisa" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_penjualan_kain_grand_total
-- ----------------------------
INSERT INTO "public"."tbl_penjualan_kain_grand_total" VALUES ('P000001', 'P000002', '-', 1, 14000, 18000000, 0, 2000000, 20000000, 0);

-- ----------------------------
-- Table structure for tbl_penjualan_kain_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_kain_pembayaran";
CREATE TABLE "public"."tbl_penjualan_kain_pembayaran" (
  "IDFJKPembayaran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalPembayaran" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Tanggal_giro" date DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_sample
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_sample";
CREATE TABLE "public"."tbl_penjualan_sample" (
  "IDFJSP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_sample_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_sample_detail";
CREATE TABLE "public"."tbl_penjualan_sample_detail" (
  "IDFJSPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJSP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_seragam
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_seragam";
CREATE TABLE "public"."tbl_penjualan_seragam" (
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_di_faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSJCS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" int8 DEFAULT NULL,
  "Tanggal_jatuh_tempo" date DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_pieces" float8 DEFAULT NULL,
  "Total_yard" float8 DEFAULT NULL,
  "Saldo_pieces" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_seragam_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_seragam_detail";
CREATE TABLE "public"."tbl_penjualan_seragam_detail" (
  "IDFJSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_roll" float8 DEFAULT NULL,
  "Saldo_qty_roll" float8 DEFAULT NULL,
  "Qty_yard" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_penjualan_seragam_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_seragam_grand_total";
CREATE TABLE "public"."tbl_penjualan_seragam_grand_total" (
  "IDFJSGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "DPP" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Sisa" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_penjualan_seragam_grand_total
-- ----------------------------
INSERT INTO "public"."tbl_penjualan_seragam_grand_total" VALUES ('P000001', 'P000001', '-', 1, 14000, 36000000, 0, 4000000, 40000000, 0);

-- ----------------------------
-- Table structure for tbl_penjualan_seragam_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_seragam_pembayaran";
CREATE TABLE "public"."tbl_penjualan_seragam_pembayaran" (
  "IDFJSPembayaran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalPembayaran" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Tanggal_giro" date DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_piutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_piutang";
CREATE TABLE "public"."tbl_piutang" (
  "Tanggal_Piutang" date DEFAULT NULL,
  "Jatuh_Tempo" date DEFAULT NULL,
  "No_Faktur" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDCustomer_seq"'::regclass),
  "Jenis_Faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_Piutang" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Awal" numeric(100) DEFAULT NULL::numeric,
  "Pembayaran" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Akhir" numeric(100) DEFAULT NULL::numeric,
  "IDPiutang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDPiutang_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDFaktur_seq"'::regclass),
  "UM" float8 DEFAULT 0,
  "DISC" float8 DEFAULT 0,
  "Retur" float8 DEFAULT 0,
  "Selisih" float8 DEFAULT 0,
  "Kontra_bon" float8 DEFAULT 0,
  "Saldo_kontra_bon" float8 DEFAULT 0
)
;

-- ----------------------------
-- Records of tbl_piutang
-- ----------------------------
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', 'INVPK18PMAK00001', 'P000005', '', 1200000, 1200000, 0, 1200000, 'P000001', 'P000001', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', 'INVPK18PMAK00002', 'P000006', '', 900000, 900000, 900000, 900000, 'P000002', 'P000002', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2018-12-27', '2019-03-12', 'INVPK18PMAK00003', 'P000006', '', 900000, 900000, 0, 900000, 'P000003', 'P000003', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', 'INVPK19PMAK00001', 'P000004', '', 10000000, 10000000, 0, 10000000, 'P000003', 'P000001', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-15', '2019-03-31', 'INVPK19PMAK00001', 'P000004', '', 10000000, 10000000, 0, 10000000, 'P000003', 'P000001', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', 'INVPK19PMAK00001', 'P000004', '', 20000000, 20000000, 0, 20000000, 'P000003', 'P000001', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', 'INVPK19PMAK00002', 'P000004', '', 20000000, 20000000, 0, 20000000, 'P000003', 'P000002', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', 'INVPK19PMAK00001', 'P000004', '', 20000000, 20000000, 0, 20000000, 'P000003', 'P000001', 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."tbl_piutang" VALUES ('2019-01-16', '2019-04-01', 'INVPK19PMAK00002', 'P000004', '', 20000000, 20000000, 0, 20000000, 'P000003', 'P000002', 0, 0, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for tbl_purchase_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order";
CREATE TABLE "public"."tbl_purchase_order" (
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDPO_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_selesai" date DEFAULT NULL,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDSupplier_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDCorak_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDMerk_seq"'::regclass),
  "IDAgen" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDAgen_seq"'::regclass),
  "Lot" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_Pesanan" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_purchase_order_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "PIC" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Prioritas" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Bentuk" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Panjang" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Point" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kirim" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Stamping" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi1" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Album" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Jenis_PO" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Batal" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Grand_total" float8 DEFAULT NULL,
  "M10" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Kain" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Lembaran" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_purchase_order
-- ----------------------------
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000002', '2019-01-18', 'PO/19/PMAK/00002', '2019-01-18', 'P000004', 'P000001', 'P000002', 'Prk0001', 'P000001', '1', 'seragam', 2, '-', 400, 400, 'test', 'urgent', 'Piece (Double Fold)', 'Standar', 'Terpisah', 'Agen', 'Cap', 'Selvedge', 'Face', '-', 'tetsasd', 'PO TTI', 'aktif', 40000000, '-', '-', '-');
INSERT INTO "public"."tbl_purchase_order" VALUES ('P000001', '2019-01-17', 'PO/19/PMAK/00001', '2019-01-17', 'P000004', 'P000001', 'P000001', 'Prk0001', 'P000001', '1', 'seragam', 2, '-', 600, 600, 'aseo', 'urgent', 'Piece (Double Fold)', 'Standar', 'Terpisah', 'Agen', 'Cap', 'Selvedge', 'Face', '10', 'testing', 'PO TTI', 'aktif', 24000000, '-', '-', '-');

-- ----------------------------
-- Table structure for tbl_purchase_order_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order_detail";
CREATE TABLE "public"."tbl_purchase_order_detail" (
  "IDPODetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_detail_IDPODetail_seq"'::regclass),
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_detail_IDPO_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_detail_IDWarna_seq"'::regclass),
  "Qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "Harga" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_purchase_order_detail
-- ----------------------------
INSERT INTO "public"."tbl_purchase_order_detail" VALUES ('P000001', 'P000001', 'P000001', 400, 400, 40000);
INSERT INTO "public"."tbl_purchase_order_detail" VALUES ('P000002', 'P000001', 'P000002', 200, 200, 40000);
INSERT INTO "public"."tbl_purchase_order_detail" VALUES ('P000003', 'P000002', 'P000001', 400, 400, 100000);

-- ----------------------------
-- Table structure for tbl_purchase_order_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order_umum";
CREATE TABLE "public"."tbl_purchase_order_umum" (
  "IDPOUmum" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_selesai" date DEFAULT NULL,
  "IDAgen" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty" float8 DEFAULT NULL,
  "Catatan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "PPN" float8 DEFAULT NULL,
  "Status_ppn" float8 DEFAULT NULL,
  "Persen_disc" float8 DEFAULT NULL,
  "Disc" float8 DEFAULT NULL,
  "IDMataUang" int2 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_purchase_order_umum
-- ----------------------------
INSERT INTO "public"."tbl_purchase_order_umum" VALUES ('P000001', '2019-01-18', 'POU/19/PMAK/00001', 'P000016', NULL, 'P000001', 10, 'test', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "public"."tbl_purchase_order_umum" VALUES ('P000002', '2019-01-18', 'POU/19/PMAK/00001', 'P000016', NULL, 'P000001', 10, 'test', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "public"."tbl_purchase_order_umum" VALUES ('P000003', '2019-01-18', 'POU/19/PMAK/00001', 'P000016', NULL, 'P000001', 10, 'test', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "public"."tbl_purchase_order_umum" VALUES ('P000004', '2019-01-18', 'POU/19/PMAK/00001', 'P000016', NULL, 'P000001', 10, 'test', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL, 10);

-- ----------------------------
-- Table structure for tbl_purchase_order_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order_umum_detail";
CREATE TABLE "public"."tbl_purchase_order_umum_detail" (
  "IDPOUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDPOUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian";
CREATE TABLE "public"."tbl_retur_pembelian" (
  "IDRB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_IDRB_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_IDFB_seq"'::regclass),
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_IDSupplier_seq"'::regclass),
  "Tanggal_fb" date DEFAULT NULL,
  "Total_qty_yard" float8 DEFAULT NULL,
  "Total_qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_detail";
CREATE TABLE "public"."tbl_retur_pembelian_detail" (
  "IDRBDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDRBDetail_seq"'::regclass),
  "IDRB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDRB_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDCorak_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDMerk_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDWarna_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(15) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDSatuan_seq"'::regclass),
  "Harga" float8 DEFAULT NULL,
  "Subtotal" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_grand_total";
CREATE TABLE "public"."tbl_retur_pembelian_grand_total" (
  "IDRBGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq"'::regclass),
  "IDRB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_grand_total_IDRB_seq"'::regclass),
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_retur_pembelian_grand_total_IDMataUang_seq"'::regclass),
  "Kurs" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "DPP" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Sisa" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_umum";
CREATE TABLE "public"."tbl_retur_pembelian_umum" (
  "IDRBUmum" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fb" date DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Status_ppn" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Persen_disc" float8 DEFAULT NULL,
  "Disc" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_umum_detail";
CREATE TABLE "public"."tbl_retur_pembelian_umum_detail" (
  "IDRBUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBUmumDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_umum_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_umum_grand_total";
CREATE TABLE "public"."tbl_retur_pembelian_umum_grand_total" (
  "IDRBUmumGrandToal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "DPP" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Sisa" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_retur_penjualan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_penjualan_detail";
CREATE TABLE "public"."tbl_retur_penjualan_detail" (
  "IDRPSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRPS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_retur_penjualan_kain
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_penjualan_kain";
CREATE TABLE "public"."tbl_retur_penjualan_kain" (
  "IDRPK" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFJK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fj" date DEFAULT NULL,
  "Total_qty_yard" float8 DEFAULT NULL,
  "Total_qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_retur_penjualan_kain_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_penjualan_kain_detail";
CREATE TABLE "public"."tbl_retur_penjualan_kain_detail" (
  "IDRPKDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRPK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_retur_penjualan_kain_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_penjualan_kain_grand_total";
CREATE TABLE "public"."tbl_retur_penjualan_kain_grand_total" (
  "IDRPKGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRPK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "DPP" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Sisa" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_retur_penjualan_seragam
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_penjualan_seragam";
CREATE TABLE "public"."tbl_retur_penjualan_seragam" (
  "IDRPS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFJS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fj" date DEFAULT NULL,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_retur_penjualan_seragam_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_penjualan_seragam_grand_total";
CREATE TABLE "public"."tbl_retur_penjualan_seragam_grand_total" (
  "IDRPSGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRPS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "DPP" float8 DEFAULT NULL,
  "Discount" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL,
  "Grand_total" float8 DEFAULT NULL,
  "Sisa" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_saldo_awal_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_saldo_awal_asset";
CREATE TABLE "public"."tbl_saldo_awal_asset" (
  "IDSaldoAwalAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_saldo_awal_asset_IDSaldoAwalAsset_seq"'::regclass),
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_saldo_awal_asset_IDAsset_seq"'::regclass),
  "Tanggal_Perolehan" date DEFAULT NULL,
  "Qty" int4 DEFAULT NULL,
  "Umur" int4 DEFAULT NULL,
  "Nilai_Penyusutan_Asset" numeric(100) DEFAULT NULL::numeric,
  "Nilai_Saldo_awal" numeric(100) DEFAULT NULL::numeric,
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_Penyusutan" date DEFAULT NULL,
  "Metode" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_sales_order_kain
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_sales_order_kain";
CREATE TABLE "public"."tbl_sales_order_kain" (
  "IDSOK" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" int8 DEFAULT NULL,
  "Tanggal_jatuh_tempo" date DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" int8 DEFAULT NULL,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "No_po_customer" varchar(18) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Grand_total" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status_ppn" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "DPP" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_sales_order_kain
-- ----------------------------
INSERT INTO "public"."tbl_sales_order_kain" VALUES ('P000001', '2019-01-16', 'SOK1900001', 'P000004', 75, '2019-04-01', 1, 1, 400, 400, 'PO09123', 20000000, 'test', 'aktif', 'Include', 18000000, 2000000);
INSERT INTO "public"."tbl_sales_order_kain" VALUES ('P000002', '2019-01-20', 'SOK1900002', 'P000004', 80, '2019-04-10', 1, 1, 200, 200, 'PO0923121t', 10000000, 'gsad', 'aktif', 'Exclude', 10000000, 1000000);

-- ----------------------------
-- Table structure for tbl_sales_order_kain_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_sales_order_kain_detail";
CREATE TABLE "public"."tbl_sales_order_kain_detail" (
  "IDSOKDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDSOK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_sales_order_kain_detail
-- ----------------------------
INSERT INTO "public"."tbl_sales_order_kain_detail" VALUES ('P000001', 'P000001', 'P000001', 'P000001', 'Prk0001', 'P000001', 400, 'ST0001', 50000, 20000000);
INSERT INTO "public"."tbl_sales_order_kain_detail" VALUES ('P000002', 'P000002', 'P000001', 'P000001', 'Prk0001', 'P000001', 200, 'ST0001', 50000, 10000000);

-- ----------------------------
-- Table structure for tbl_sales_order_seragam
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_sales_order_seragam";
CREATE TABLE "public"."tbl_sales_order_seragam" (
  "IDSOS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" int8 DEFAULT NULL,
  "Tanggal_jatuh_tempo" date DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "No_po_customer" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Grand_total" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status_ppn" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "DPP" float8 DEFAULT NULL,
  "PPN" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_sales_order_seragam_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_sales_order_seragam_detail";
CREATE TABLE "public"."tbl_sales_order_seragam_detail" (
  "IDSOSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDSOS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_satuan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_satuan";
CREATE TABLE "public"."tbl_satuan" (
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_satuan_IDSatuan_seq"'::regclass),
  "Kode_Satuan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_satuan
-- ----------------------------
INSERT INTO "public"."tbl_satuan" VALUES ('ST0001', 'yrd', 'Yard', 'Aktif');
INSERT INTO "public"."tbl_satuan" VALUES ('PC0002', 'pcs', 'Pcs', 'Aktif');
INSERT INTO "public"."tbl_satuan" VALUES ('MT0003', 'mtr', 'Meter', 'Aktif');

-- ----------------------------
-- Table structure for tbl_setting_cf
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_setting_cf";
CREATE TABLE "public"."tbl_setting_cf" (
  "IDSettingCF" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDCoa" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Kategori" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Perhitungan" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group" int8 DEFAULT NULL,
  "Urutan" int8 DEFAULT NULL,
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_setting_cf
-- ----------------------------
INSERT INTO "public"."tbl_setting_cf" VALUES ('P000001', 'P000138', '  ', 'spasi', 'tidak ada', 1, 1, 'P000001');

-- ----------------------------
-- Table structure for tbl_setting_lr
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_setting_lr";
CREATE TABLE "public"."tbl_setting_lr" (
  "IDSettingLR" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Kategori" varchar(70) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Perhitungan" varchar(70) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Group" varchar(70) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Urutan" int4 DEFAULT NULL,
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_setting_lr
-- ----------------------------
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000001', 'P000138', 'Aset', 'judul', 'tidak ada', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000002', 'P000138', 'Aset Lancar', 'judul', 'tidak ada', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000003', 'P000001', 'Kas & Setara Kas', 'data', 'tambah', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000004', 'P000015', 'Piutang Usaha', 'data', 'tambah', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000005', 'P000019', 'Piutang Lain-lain', 'data', 'tambah', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000006', 'P000022', 'Persediaan Barang Dagang', 'data', 'tambah', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000007', 'P000023', 'Suspense', 'data', 'tambah', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000008', 'P000024', 'Uang Muka Pembelian', 'data', 'tambah', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P000009', 'P000027', 'Pajak Dibayar Dimuka', 'data', 'tambah', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000a', 'P000138', 'Total Aset Lancar', 'hasil', 'tambah', '1', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000b', 'P000138', 'Aset Tidak Lancar', 'judul', 'tidak ada', '2', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000c', 'P000037', 'Investasi Dalam Entitas Asosiasi', 'data', 'tidak ada', '2', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000d', 'P000042', 'Aset Tetap', 'data', 'tidak ada', '2', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000e', 'P000138', 'Total Aset Tidak Lancar', 'hasil', 'tambah', '2', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000f', 'P000138', 'Total Aset', 'total', 'tambah', '1, 2', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000g', 'P000138', 'Liabilitas Ekuitas', 'judul', 'tidak ada', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000h', 'P000138', 'Liabilitas Jangka Pendek', 'judul', 'tidak ada', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000i', 'P000056', 'Utang Bank Jangka Pendek', 'data', 'tidak ada', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000j', 'P000057', 'Utang Usaha', 'data', 'tidak ada', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000k', 'P000061', 'Utang Lain-lain', 'data', 'tidak ada', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000l', 'P000072', 'Utang Leasing', 'data', 'tidak ada', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000m', 'P000074', 'Biaya Yang Masih Harus Dibayar', 'data', 'tidak ada', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000n', 'P000077', 'Uang Muka Pelanggan', 'data', 'tidak ada', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000o', 'P000138', 'Total Liabilitas Jangka Pendek', 'hasil', 'tambah', '3', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000p', 'P000138', 'Ekuitas', 'judul', 'tidak ada', '4', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000q', 'P000083', 'Modal Saham', 'data', 'tidak ada', '4', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000r', 'P000085', 'Saldo Laba Ditahan', 'data', 'tidak ada', '4', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000s', 'P000086', 'Laba (Rugi) Tahun Berjalan', 'data', 'tidak ada', '4', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000t', 'P000138', 'Total Ekuitas', 'hasil', 'tambah', '4', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000u', 'P000138', 'Total Liabilitas dan Ekuitas', 'hasil', 'tambah', '3, 4', 1, 'P000001');
INSERT INTO "public"."tbl_setting_lr" VALUES ('P00000v', 'P000138', '-', 'judul', 'tidak ada', '-', 1, 'P000001');

-- ----------------------------
-- Table structure for tbl_setting_notes
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_setting_notes";
CREATE TABLE "public"."tbl_setting_notes" (
  "IDSettingNotes" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDCoa" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Kategori" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Perhitungan" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group" int8 DEFAULT NULL,
  "Urutan" int8 DEFAULT NULL,
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_setting_notes
-- ----------------------------
INSERT INTO "public"."tbl_setting_notes" VALUES ('P000001', 'P000016', '  piutang', 'judul', 'tidak ada', 1, 1, 'P000001');

-- ----------------------------
-- Table structure for tbl_setting_pl
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_setting_pl";
CREATE TABLE "public"."tbl_setting_pl" (
  "IDSettingPL" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Kategori" varchar(90) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Perhitungan" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group" int8 DEFAULT NULL,
  "Urutan" int8 DEFAULT NULL,
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_setting_pl
-- ----------------------------
INSERT INTO "public"."tbl_setting_pl" VALUES ('P000001', 'P000138', '  ', 'spasi', 'tidak ada', 1, 1, 'P000001');

-- ----------------------------
-- Table structure for tbl_stok
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_stok";
CREATE TABLE "public"."tbl_stok" (
  "IDStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDStok_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDFaktur_seq"'::regclass),
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDFakturDetail_seq"'::regclass),
  "Jenis_faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDWarna_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDGudang_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDSatuan_seq"'::regclass),
  "Harga" float8 DEFAULT NULL,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_stok_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_stok
-- ----------------------------
INSERT INTO "public"."tbl_stok" VALUES ('P000001', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000001', 'SA', '88878278', 'P000003', 'P000002', 'P000001', '1', 200, 199.5, 200, 199.5, 'A', 'ST0001', 20000000, 1, '', 0);
INSERT INTO "public"."tbl_stok" VALUES ('P000002', '2019-01-18', 'PB/19/PMAK/00001', 'P000001', 'P000002', 'SA', '09012301', 'P000003', 'P000002', 'P000001', '1', 200, 199.5, 200, 199.5, 'A', 'ST0001', 20000000, 1, '', 0);

-- ----------------------------
-- Table structure for tbl_stok_history
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_stok_history";
CREATE TABLE "public"."tbl_stok_history" (
  "IDBarang" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDBarang_seq"'::regclass),
  "Asal_jenis_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor_faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDFaktur_seq"'::regclass),
  "IDFakturDetail" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDFakturDetail_seq"'::regclass),
  "Jenis_faktur" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDHistoryStok" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDHistoryStok_seq"'::regclass),
  "IDStok" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDStok_seq"'::regclass),
  "Asal_faktur" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "AsalIDFaktur" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_AsalIDFaktur_seq"'::regclass),
  "AsalIDFakturDetail" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_AsalIDFakturDetail_seq"'::regclass),
  "Barcode" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDCorak_seq"'::regclass),
  "IDWarna" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDWarna_seq"'::regclass),
  "IDGudang" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDGudang_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDSatuan_seq"'::regclass),
  "Harga" float8 DEFAULT NULL,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDMataUang_seq"'::regclass),
  "Kurs" float8 DEFAULT NULL,
  "Total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_stok_opname
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_stok_opname";
CREATE TABLE "public"."tbl_stok_opname" (
  "IDStokOpname" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDStokOpname_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Barcode" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDWarna_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDGudang_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Grade" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDSatuan_seq"'::regclass),
  "Harga" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_suplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_suplier";
CREATE TABLE "public"."tbl_suplier" (
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_suplier_IDSupplier_seq"'::regclass),
  "IDGroupSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_suplier_IDGroupSupplier_seq"'::regclass),
  "Kode_Suplier" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "No_Telpon" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Fax" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(35) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_KTP" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_suplier
-- ----------------------------
INSERT INTO "public"."tbl_suplier" VALUES ('P000001', 'P000001', 'SL', 'SAVANA LESTARI', 'JL. MANGGA DUA RAYA BLOK. C4/12', '021-6019122', 'P000010', '021-6110971', NULL, 'JKT', 'P000002', NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000002', 'P000001', 'TBM', 'TRICITRA BUSANAMAS', 'JL. MANGGA DUA RAYA BLOK. C2/6', '021-6018210', 'P000010', '021-6016855', NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000003', 'P000001', 'PBM', 'PERMATA BUSANAMAS', 'JL. MANGGA DUA RAYA BLOK. D2/22', '021-6128064', 'P000010', NULL, NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000004', 'P000001', 'TTI', 'TRISULA TEXTILE INDUSTRIES', 'JL. LEUWI GAJAH NO. 170 ', '022-6613333', 'P000006', '022-6613377', NULL, 'CIMAHI', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000005', 'P000001', 'CK', 'CAKRA KENCANA', 'JL. MANGGA DUA RAYA BLOK. C6/12', '021-6016055', 'P000010', '021-6015441', NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000006', 'P000001', 'JJ', 'JUST JAIT INDONESIA', 'Komp. Batununggal Indah I no. 12', '022-95056000/7502022', 'P000001', '022-95056000/7502022', NULL, 'BDG', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000007', 'P000001', 'SACN', 'SINAR ABADI CITRA NUSA', 'JL. MANGGA DUA RAYA BLOK. C2/31', '021-6013301', 'P000010', '021-6019747', NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000008', 'P000001', 'TTL', 'TRIKARYA TEXTILINDO', 'JL. MANGGA DUA RAYA BLOK. C4/12', '021-6019122', 'P000010', '021-6013908', NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000009', 'P000002', 'mu', 'MODERA UTAMA TEXTILE', 'RUKO ITC BARANANGSIANG', '022-4222065', 'P000001', NULL, NULL, 'BDG', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000010', 'P000002', 'HM', 'HEGAR MULYA', 'JL. Hegar Lokasi II Cibaligo', '022-6030240', 'P000006', '022-6031222', NULL, 'CIMAHI', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000011', 'P000002', 'SGL', 'SUMBER GUNUNG LESTARI', 'JL. PASAR PAGI NO. 17', '021-6907216', 'P000010', NULL, NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000012', 'P000002', 'RMJ', 'REMAJA', 'JL. RAYA RAGUNAN NO. 44 PASAR MINGGU', '021-7802952', 'P000010', '021-7802952', NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000013', 'P000002', 'GAP', 'PT GAJAH ANGKASA PERKASA', 'JL. Jend. Sudirman No. 823 ', NULL, 'P000001', NULL, NULL, 'BDG', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000014', 'P000002', 'CN', 'CANDRATEX', 'JL. CISIRUNG NO. 48', NULL, 'P000001', NULL, NULL, 'BDG', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000015', 'P000002', 'NI', 'NAGA INTAN', 'JL. PINTU KECIL NO. 16', NULL, 'P000010', NULL, NULL, 'JKT', NULL, NULL);
INSERT INTO "public"."tbl_suplier" VALUES ('P000016', 'P000002', 'BMA', 'BERKAT MULIA ABADI', 'JL. DULATIF', NULL, 'P000001', NULL, NULL, 'BDG', NULL, NULL);

-- ----------------------------
-- Table structure for tbl_surat_jalan_customer_kain
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_customer_kain";
CREATE TABLE "public"."tbl_surat_jalan_customer_kain" (
  "IDSJCK" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPAC" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_kirim" date DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_surat_jalan_customer_kain
-- ----------------------------
INSERT INTO "public"."tbl_surat_jalan_customer_kain" VALUES ('P000001', '2019-01-16', 'SJCK1900001', 'P000001', 'P000004', '2019-01-16', 1, 1, 400, 400, 'test', 'aktif');

-- ----------------------------
-- Table structure for tbl_surat_jalan_customer_kain_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_customer_kain_detail";
CREATE TABLE "public"."tbl_surat_jalan_customer_kain_detail" (
  "IDSJCKDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDSJCK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_roll" float8 DEFAULT NULL,
  "Saldo_roll" float8 DEFAULT NULL,
  "Qty_yard" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Grade" varchar(12) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_surat_jalan_customer_kain_detail
-- ----------------------------
INSERT INTO "public"."tbl_surat_jalan_customer_kain_detail" VALUES ('P000001', 'P000001', 'P000001', 'P000001', 'Prk0001', 'P000001', 400, 400, 400, 400, 'ST0001', 'A');

-- ----------------------------
-- Table structure for tbl_surat_jalan_customer_seragam
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_customer_seragam";
CREATE TABLE "public"."tbl_surat_jalan_customer_seragam" (
  "IDSJCS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSOS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_kirim" date DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_surat_jalan_customer_seragam_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_customer_seragam_detail";
CREATE TABLE "public"."tbl_surat_jalan_customer_seragam_detail" (
  "IDSJCSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDSJCS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8 DEFAULT NULL,
  "Saldo_qty" float8 DEFAULT NULL,
  "Qty_roll" float8 DEFAULT NULL,
  "Saldo_roll" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_surat_jalan_makloon_celup
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_makloon_celup";
CREATE TABLE "public"."tbl_surat_jalan_makloon_celup" (
  "IDSJM" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_IDSJM_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_IDSupplier_seq"'::regclass),
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_IDPO_seq"'::regclass),
  "Total_qty_yard" float8 DEFAULT NULL,
  "Total_qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_surat_jalan_makloon_celup_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail";
CREATE TABLE "public"."tbl_surat_jalan_makloon_celup_detail" (
  "IDSJMDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq"'::regclass),
  "IDSJM" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDSJM_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(15) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_surat_jalan_makloon_jahit
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit";
CREATE TABLE "public"."tbl_surat_jalan_makloon_jahit" (
  "IDSJH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_IDSJH_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_IDSupplier_seq"'::regclass),
  "Total_qty_yard" float8 DEFAULT NULL,
  "Total_qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_surat_jalan_makloon_jahit_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail";
CREATE TABLE "public"."tbl_surat_jalan_makloon_jahit_detail" (
  "IDSJHDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq"'::regclass),
  "IDSJH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_tampungan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_tampungan";
CREATE TABLE "public"."tbl_tampungan" (
  "IDT" int8 NOT NULL DEFAULT nextval('"tbl_tampungan_IDT_seq"'::regclass),
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoPO" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Corak" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Warna" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(9) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(18) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_tampungan
-- ----------------------------
INSERT INTO "public"."tbl_tampungan" VALUES (5, 'S00913', 'PO/19/PMAK/00002', '09012301', 'SO90124', '1', '-', '93063', '01/35', 200, 199.5, 0, 0, 'A', 'test', '123', 'YARD', 20000000);
INSERT INTO "public"."tbl_tampungan" VALUES (2, 'S00912', 'PO/19/PMAK/00001', '90881799', 'SO90123', '1', '-', '03131', '01/35', 200, 199.5, 0, 0, 'A', 'test', '123', 'YARD', 12000000);
INSERT INTO "public"."tbl_tampungan" VALUES (1, 'S00912', 'PO/19/PMAK/00001', '34657189', 'SO90123', '1', '-', '03131', '01/35', 200, 199.5, 0, 0, 'A', 'test', '123', 'YARD', 12000000);
INSERT INTO "public"."tbl_tampungan" VALUES (3, 'S00912', 'PO/19/PMAK/00001', '77656786', 'SO90123', '1', '-', '03131', '02/04', 100, 99.5, 0, 0, 'A', 'test', '123', 'YARD', 4000000);
INSERT INTO "public"."tbl_tampungan" VALUES (4, 'S00912', 'PO/19/PMAK/00001', '43566777', 'SO90123', '1', '-', '03131', '02/04', 100, 99.5, 0, 0, 'A', 'test', '123', 'YARD', 4000000);
INSERT INTO "public"."tbl_tampungan" VALUES (6, 'S00913', 'PO/19/PMAK/00002', '88878278', 'SO90124', '1', '-', '93063', '01/35', 200, 199.5, 0, 0, 'A', 'test', '123', 'YARD', 20000000);

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier";
CREATE TABLE "public"."tbl_terima_barang_supplier" (
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDTBS_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDSupplier_seq"'::regclass),
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDPO_seq"'::regclass),
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty_yard" float8 DEFAULT NULL,
  "Total_qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Jenis_TBS" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Batal" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_terima_barang_supplier
-- ----------------------------
INSERT INTO "public"."tbl_terima_barang_supplier" VALUES ('P000001', '2019-01-18', 'PB/19/PMAK/00001', 'P000004', 'S00913', 'P000002', 'SO90124', 400, 398, 400, 398, 'test', 'PB', 'Aktif');

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_detail" (
  "IDTBSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDTBSDetail_seq"'::regclass),
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDTBS_seq"'::regclass),
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8 DEFAULT NULL,
  "Qty_meter" float8 DEFAULT NULL,
  "Saldo_yard" float8 DEFAULT NULL,
  "Saldo_meter" float8 DEFAULT NULL,
  "Grade" varchar(5) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDSatuan_seq"'::regclass),
  "Harga" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_terima_barang_supplier_detail
-- ----------------------------
INSERT INTO "public"."tbl_terima_barang_supplier_detail" VALUES ('P000001', 'P000001', '88878278', 'SO90124', '1', '-', 'P000003', 'P000002', 'P000001', 'Prk0002', 200, 199.5, 200, 199.5, 'A', 'test', '123', 'ST0001', 20000000);
INSERT INTO "public"."tbl_terima_barang_supplier_detail" VALUES ('P000002', 'P000001', '09012301', 'SO90124', '1', '-', 'P000003', 'P000002', 'P000001', 'Prk0002', 200, 199.5, 200, 199.5, 'A', 'test', '123', 'ST0001', 20000000);

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_jahit
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_jahit";
CREATE TABLE "public"."tbl_terima_barang_supplier_jahit" (
  "IDTBSH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDTBSH_seq"'::regclass),
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDSupplier_seq"'::regclass),
  "IDSJH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDSJH_seq"'::regclass),
  "Nomor_supplier" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_jahit_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_jahit_detail" (
  "IDTBSHDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq"'::regclass),
  "IDTBSH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"'::regclass),
  "Qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_umum";
CREATE TABLE "public"."tbl_terima_barang_supplier_umum" (
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date DEFAULT NULL,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPOUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Keterangan" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Total_qty" float8 DEFAULT NULL,
  "Saldo" float8 DEFAULT NULL,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_umum_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_umum_detail" (
  "IDTBSUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8 DEFAULT NULL,
  "IDPOUmumDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Saldo" float8 DEFAULT NULL,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8 DEFAULT NULL,
  "IDMataUang" int8 DEFAULT NULL,
  "Kurs" float8 DEFAULT NULL,
  "Sub_total" float8 DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for tbl_um_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_um_customer";
CREATE TABLE "public"."tbl_um_customer" (
  "IDUMCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_um_customer_IDUMCustomer_seq"'::regclass),
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_um_customer_IDCustomer_seq"'::regclass),
  "Tanggal_UM" date DEFAULT NULL,
  "Nomor_Faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_UM" numeric(100) DEFAULT NULL::numeric,
  "Saldo_UM" numeric(100) DEFAULT NULL::numeric,
  "IDFaktur" int8 NOT NULL DEFAULT nextval('"tbl_um_customer_IDFaktur_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_um_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_um_supplier";
CREATE TABLE "public"."tbl_um_supplier" (
  "IDUMSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "Nomor_Faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Nominal" float8 DEFAULT NULL,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "Tanggal_UM" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;

-- ----------------------------
-- Records of tbl_um_supplier
-- ----------------------------
INSERT INTO "public"."tbl_um_supplier" VALUES ('P000001', 'PH19PMAK00001', 'P000004', 6160000000, 'P000001', '2019-01-16');
INSERT INTO "public"."tbl_um_supplier" VALUES ('P000002', 'PH19PMAK00001', 'P000004', 8800000000, 'P000001', '2019-01-31');

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_user";
CREATE TABLE "public"."tbl_user" (
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_user_IDUser_seq"'::regclass),
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Username" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Password" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_user_IDGroupUser_seq"'::regclass),
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO "public"."tbl_user" VALUES ('P000001', 'advess', 'advess', 'advess@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 5, 'Aktif');

-- ----------------------------
-- Table structure for tbl_warna
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_warna";
CREATE TABLE "public"."tbl_warna" (
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_warna_IDWarna_seq"'::regclass),
  "Kode_Warna" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Warna" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_warna
-- ----------------------------
INSERT INTO "public"."tbl_warna" VALUES ('P000001', '01/35', '01/35', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000002', '02/04', '02/04', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000003', '03/05', '03/05', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000004', '04/06', '04/06', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000005', '05/26', '05/26', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000006', '06/02', '06/02', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000007', '07/03', '07/03', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000008', '08/21', '08/21', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000009', '09/31', '09/31', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000010', '10/43', '10/43', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000011', '11/41', '11/41', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000012', '12/13', '12/13', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000013', '13/29', '13/29', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000014', '14/27', '14/27', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000015', '15/09', '15/09', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000016', '16/32', '16/32', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000017', '17/24', '17/24', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000018', '18/19', '18/19', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000019', '19/76', '19/76', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000020', '20/86', '20/86', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000021', '21/156', '21/156', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000022', '22/39', '22/39', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000023', '23/152', '23/152', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000024', '24/149', '24/149', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000025', '25/16', '25/16', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000026', '27/858', '27/858', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000027', '28/230', '28/230', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000028', '36/1111', '36/1111', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000029', '38/114', '38/114', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000030', '40/1135', '40/1135', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000031', '41/1123', '41/1123', 'aktif', 'P000001');
INSERT INTO "public"."tbl_warna" VALUES ('P000032', '01/112', '01/112', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000033', '02/122', '02/122', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000034', '03/120', '03/120', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000035', '04/26', '04/26', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000036', '05/129', '05/129', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000037', '06/31', '06/31', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000038', '07/114', '07/114', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000039', '08/76', '08/76', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000040', '09/107', '09/107', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000041', '10/43', '10/43', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000042', '11/38', '11/38', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000043', '12/110', '12/110', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000044', '13/86', '13/86', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000045', '14/117', '14/117', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000046', '15/118', '15/118', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000047', '16/19', '16/19', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000048', '17/124', '17/124', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000049', '18/72', '18/72', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000050', '22/204', '22/204', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000051', '23/266', '23/266', 'aktif', 'P000002');
INSERT INTO "public"."tbl_warna" VALUES ('P000052', '01/06', '01/06', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000053', '02/08', '02/08', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000054', '03/09', '03/09', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000055', '04/22', '04/22', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000056', '05/03', '05/03', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000057', '06/22', '06/22', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000058', '07/80', '07/80', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000059', '08/25', '08/25', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000060', '09/17', '09/17', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000061', '10/83', '10/83', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000062', '11/99', '11/99', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000063', '12/72', '12/72', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000064', '13/77', '13/77', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000065', '14/18', '14/18', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000066', '15/30', '15/30', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000067', '16/36', '16/36', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000068', '17/93', '17/93', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000069', '18/34', '18/34', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000070', '19/119', '19/119', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000071', '20/131', '20/131', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000072', '21/134', '21/134', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000073', '22/137', '22/137', 'aktif', 'P000005');
INSERT INTO "public"."tbl_warna" VALUES ('P000074', '01/36', '01/36', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000075', '02/76', '02/76', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000076', '03/28', '03/28', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000077', '04/29', '04/29', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000078', '05/24', '05/24', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000079', '06/67', '06/67', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000080', '07/38', '07/38', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000081', '08/40', '08/40', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000082', '09/26', '09/26', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000083', '10/41', '10/41', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000084', '11/81', '11/81', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000085', '12/22', '12/22', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000086', '13/11', '13/11', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000087', '14/08', '14/08', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000088', '15/10', '15/10', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000089', '16/02', '16/02', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000090', '17/50', '17/50', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000091', '18/55', '18/55', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000092', '19/116', '19/116', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000093', '22/63', '22/63', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000094', '23/60', '23/60', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000095', '24/130', '24/130', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000096', '25/C', '25/C', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000097', '26/C', '26/C', 'aktif', 'P000007');
INSERT INTO "public"."tbl_warna" VALUES ('P000098', '01/681', '01/681', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000099', '02/477', '02/477', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000100', '03/482', '03/482', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000101', '04/42', '04/42', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000102', '05/634', '05/634', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000103', '06/497', '06/497', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000104', '07/188', '07/188', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000105', '08/50', '08/50', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000106', '09/711', '09/711', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000107', '10/676', '10/676', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000108', '11/40', '11/40', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000109', '12/663', '12/663', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000110', '13/666', '13/666', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000111', '14/38', '14/38', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000112', '15/43', '15/43', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000113', '16/689', '16/689', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000114', '17/687', '17/687', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000115', '18/670', '18/670', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000116', '19/483', '19/483', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000117', '20/44', '20/44', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000118', '21/593', '21/593', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000119', '22/153', '22/153', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000120', '23/147', '23/147', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000121', '24/488', '24/488', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000122', '25/181', '25/181', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000123', '26/179', '26/179', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000124', '27/49', '27/49', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000125', '28/46', '28/46', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000126', '29/485', '29/485', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000127', '30/66', '30/66', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000128', '31/57', '31/57', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000129', '32/55', '32/55', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000130', '33/672', '33/672', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000131', '34/68', '34/68', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000132', '35/31', '35/31', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000133', '36/70', '36/70', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000134', '901/1838', '901/1838', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000135', '39/2312', '39/2312', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000136', '41/C', '41/C', 'aktif', 'P000009');
INSERT INTO "public"."tbl_warna" VALUES ('P000137', '01/V1', '01/V1', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000138', '02/31', '02/31', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000139', '03/16', '03/16', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000140', '04/37', '04/37', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000141', '05/K1', '05/K1', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000142', '06/Z2', '06/Z2', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000143', '07/01', '07/01', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000144', '08/J2', '08/J2', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000145', '09/G1', '09/G1', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000146', '10/91', '10/91', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000147', '11/29', '11/29', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000148', '12/43', '12/43', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000149', '13/11', '13/11', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000150', '14/15', '14/15', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000151', '15/120', '15/120', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000152', '16/148', '16/148', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000153', '17/I', '17/I', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000154', '18/C1', '18/C1', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000155', '19/D', '19/D', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000156', '20/C', '20/C', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000157', '21/100', '21/100', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000158', '22/105', '22/105', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000159', '23/172', '23/172', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000160', '24/181', '24/181', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000161', '25/190', '25/190', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000162', '26/206', '26/206', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000163', '27/210', '27/210', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000164', '28/213', '28/213', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000165', '29/218', '29/218', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000166', '31/242', '31/242', 'aktif', 'P000010');
INSERT INTO "public"."tbl_warna" VALUES ('P000167', '01/71', '01/71', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000168', '02/49', '02/49', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000169', '03/43', '03/43', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000170', '04/77', '04/77', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000171', '05/83', '05/83', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000172', '06/84', '06/84', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000173', '07/64', '07/64', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000174', '08/73', '08/73', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000175', '09/62', '09/62', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000176', '10/81', '10/81', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000177', '11/55', '11/55', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000178', '12/115', '12/115', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000179', '13/66', '13/66', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000180', '14/51', '14/51', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000181', '15/63', '15/63', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000182', '16/67', '16/67', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000183', '17/87', '17/87', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000184', '18/93', '18/93', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000185', '19/60', '19/60', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000186', '20/91', '20/91', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000187', '21/302', '21/302', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000188', '22/305', '22/305', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000189', '23/311', '23/311', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000190', '24/312', '24/312', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000191', '25/512', '25/512', 'aktif', 'P000003');
INSERT INTO "public"."tbl_warna" VALUES ('P000192', '01/114', '01/114', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000193', '02/115', '02/115', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000194', '03/116', '03/116', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000195', '04/117', '04/117', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000196', '05/204', '05/204', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000197', '06/58', '06/58', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000198', '07/69', '07/69', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000199', '08/119', '08/119', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000200', '09/120', '09/120', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000201', '10/121', '10/121', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000202', '11/122', '11/122', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000203', '12/123', '12/123', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000204', '13/124', '13/124', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000205', '14/125', '14/125', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000206', '15/87', '15/87', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000207', '16/59', '16/59', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000208', '17/97', '17/97', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000209', '18/99', '18/99', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000210', '19/126', '19/126', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000211', '20/127', '20/127', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000212', '21/102', '21/102', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000213', '22/128', '22/128', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000214', '23/129', '23/129', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000215', '24/53', '24/53', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000216', '25/17', '25/17', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000217', '26/85', '26/85', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000218', '27/31', '27/31', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000219', '28/15', '28/15', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000220', '29/130', '29/130', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000221', '30/01', '30/01', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000222', '31/132', '31/132', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000223', '32/148', '32/148', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000224', '33/157', '33/157', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000225', '34/138', '34/138', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000226', '35/142', '35/142', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000227', '36/192', '36/192', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000228', '37/196', '37/196', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000229', '38/205', '38/205', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000230', '39/211', '39/211', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000231', '40/326', '40/326', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000232', '41/340', '41/340', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000233', '42/342', '42/342', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000234', '43/348', '43/348', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000235', '47/457', '47/457', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000236', '48/460', '48/460', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000237', '50/393', '50/393', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000238', '51/396', '51/396', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000239', '52/408', '52/408', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000240', '53/428', '53/428', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000241', '54/453', '54/453', 'aktif', 'P000011');
INSERT INTO "public"."tbl_warna" VALUES ('P000242', '01/331', '01/331', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000243', '02/387', '02/387', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000244', '03/324', '03/324', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000245', '04/437', '04/437', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000246', '05/422', '05/422', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000247', '06/466', '06/466', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000248', '07/446', '07/446', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000249', '08/440', '08/440', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000250', '09/418', '09/418', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000251', '10/405', '10/405', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000252', '11/391', '11/391', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000253', '12/475', '12/475', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000254', '13/641', '13/641', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000255', '14/400', '14/400', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000256', '15/432', '15/432', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000257', '16/358', '16/358', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000258', '17/409', '17/409', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000259', '18/354', '18/354', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000260', '19/503', '19/503', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000261', '20/318', '20/318', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000262', '21/461', '21/461', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000263', '22/436', '22/436', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000264', '23/384', '23/384', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000265', '29/677', '29/677', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000266', '30/659', '30/659', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000267', '31/811', '31/811', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000268', '32/734', '32/734', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000269', '33/768', '33/768', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000270', '35/832', '35/832', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000271', '36/844', '36/844', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000272', '37/836', '37/836', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000273', '38/862', '38/862', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000274', '39/863', '39/863', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000275', '40/126', '40/126', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000276', '01/147', '01/147', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000277', '02/166', '02/166', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000278', '03/265', '03/265', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000279', '04/271', '04/271', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000280', '05/171', '05/171', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000281', '06/174', '06/174', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000282', '07/191', '07/191', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000283', '08/175', '08/175', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000284', '09/295', '09/295', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000285', '10/290', '10/290', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000286', '11/189', '11/189', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000287', '12/194', '12/194', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000288', '13/46', '13/46', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000289', '14/39', '14/39', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000290', '15/163', '15/163', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000291', '16/168', '16/168', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000292', '17/123', '17/123', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000293', '18/142', '18/142', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000294', '19/133', '19/133', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000295', '20/135', '20/135', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000296', '21/289', '21/289', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000297', '22/284', '22/284', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000298', '23/179', '23/179', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000299', '24/178', '24/178', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000300', '25/279', '25/279', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000301', '26/181', '26/181', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000302', '27/273', '27/273', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000303', '28/275', '28/275', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000304', '29/298', '29/298', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000305', '30/359', '30/359', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000306', '31/413', '31/413', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000307', '32/414', '32/414', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000308', '33/415', '33/415', 'aktif', 'P000015');
INSERT INTO "public"."tbl_warna" VALUES ('P000309', '01/97', '01/97', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000310', '02/67', '02/67', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000311', '03/52', '03/52', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000312', '04/41', '04/41', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000313', '05/66', '05/66', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000314', '06/31', '06/31', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000315', '07/34', '07/34', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000316', '08/70', '08/70', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000317', '09/101', '09/101', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000318', '10/42', '10/42', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000319', '11/78', '11/78', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000320', '12/68', '12/68', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000321', '13/51', '13/51', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000322', '14/54', '14/54', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000323', '15/66', '15/66', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000324', '16/80', '16/80', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000325', '17/61', '17/61', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000326', '18/63', '18/63', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000327', '19/150', '19/150', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000328', '20/153', '20/153', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000329', '21/157', '21/157', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000330', '22/152', '22/152', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000331', '23/162', '23/162', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000332', '24/127', '24/127', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000333', '25/171', '25/171', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000334', '26/282', '26/282', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000335', '28/270', '28/270', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000336', '30/556', '30/556', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000337', '36/694', '36/694', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000338', '37/605', '37/605', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000339', '38/562', '38/562', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000340', '45/31', '45/31', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000341', '18/1051', '18/1051', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000342', '50/1067', '50/1067', 'aktif', 'P000014');
INSERT INTO "public"."tbl_warna" VALUES ('P000343', '01/110', '01/110', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000344', '02/102', '02/102', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000345', '03/70', '03/70', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000346', '04/72', '04/72', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000347', '05/189', '05/189', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000348', '06/186', '06/186', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000349', '07/193', '07/193', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000350', '08/82', '08/82', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000351', '09/47', '09/47', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000352', '10/48', '10/48', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000353', '11/04', '11/04', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000354', '12/40', '12/40', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000355', '13/09', '13/09', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000356', '14/80', '14/80', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000357', '15/183', '15/183', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000358', '16/18', '16/18', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000359', '17/147', '17/147', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000360', '18/26', '18/26', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000361', '19/91', '19/91', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000362', '20/87', '20/87', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000363', '21/169', '21/169', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000364', '22/291', '22/291', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000365', '23/212', '23/212', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000366', '24/305', '24/305', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000367', '25/385', '25/385', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000368', '41/631', '41/631', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000369', '50/680', '50/680', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000370', '51/683', '51/683', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000371', '40/393', '40/393', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000372', '31/503', '31/503', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000373', '47/50', '47/50', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000374', '31/55', '31/55', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000375', '42/105', '42/105', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000376', '43/107', '43/107', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000377', '44/112', '44/112', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000378', '45/149', '45/149', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000379', '38/92', '38/92', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000380', '32/449', '32/449', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000381', '52/689', '52/689', 'aktif', 'P000013');
INSERT INTO "public"."tbl_warna" VALUES ('P000382', '01/24', '01/24', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000383', '02/18', '02/18', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000384', '03/19', '03/19', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000385', '04/30', '04/30', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000386', '05/31', '05/31', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000387', '06/39', '06/39', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000388', '07/40', '07/40', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000389', '08/36', '08/36', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000390', '09/28', '09/28', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000391', '10/25', '10/25', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000392', '11/21', '11/21', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000393', '12/41', '12/41', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000394', '13/38', '13/38', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000395', '14/33', '14/33', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000396', '15/50', '15/50', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000397', '16/150', '16/150', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000398', '18/67', '18/67', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000399', '17/71', '17/71', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000400', '19/78', '19/78', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000401', '25/138', '25/138', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000402', '22/139', '22/139', 'aktif', 'P000017');
INSERT INTO "public"."tbl_warna" VALUES ('P000403', '01/78', '01/78', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000404', '02/85', '02/85', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000405', '03/30', '03/30', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000406', '04/95', '04/95', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000407', '05/02', '05/02', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000408', '06/75', '06/75', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000409', '07/21', '07/21', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000410', '08/145', '08/145', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000411', '09/32', '09/32', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000412', '10/41', '10/41', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000413', '11/08', '11/08', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000414', '12/11', '12/11', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000415', '13/66', '13/66', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000416', '14/148', '14/148', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000417', '15/136', '15/136', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000418', '16/134', '16/134', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000419', '17/150', '17/150', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000420', '18/119', '18/119', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000421', '19/143', '19/143', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000422', '20/128', '20/128', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000423', '21/154', '21/154', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000424', '22/295', '22/295', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000425', '23/293', '23/293', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000426', '24/304', '24/304', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000427', '200/150A', '200/150A', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000428', '230', '230', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000429', '233', '233', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000430', '670', '670', 'aktif', 'P000016');
INSERT INTO "public"."tbl_warna" VALUES ('P000431', '01/13', '01/13', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000432', '02/17', '02/17', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000433', '03/02', '03/02', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000434', '04/07', '04/07', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000435', '05/08', '05/08', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000436', '06/24', '06/24', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000437', '07/29', '07/29', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000438', '08/28', '08/28', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000439', '09/03', '09/03', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000440', '10/237', '10/237', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000441', '11/40', '11/40', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000442', '13494', '13494', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000443', '13/23', '13/23', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000444', '14/21/268', '14/21/268', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000445', '15/22', '15/22', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000446', '16/32', '16/32', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000447', '17/27', '17/27', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000448', '18/199', '18/199', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000449', '19/225', '19/225', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000450', '20/191', '20/191', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000451', '21/65/152', '21/65/152', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000452', '22/39', '22/39', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000453', '23/11', '23/11', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000454', '24/31', '24/31', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000455', '25/231', '25/231', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000456', '26/229', '26/229', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000457', '27/267', '27/267', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000458', '28/18', '28/18', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000459', '29/33', '29/33', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000460', '30/34', '30/34', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000461', '31/05', '31/05', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000462', '32/06', '32/06', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000463', '33/26', '33/26', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000464', '34/09', '34/09', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000465', '35/286', '35/286', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000466', '36/328', '36/328', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000467', '37/327', '37/327', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000468', '38/336', '38/336', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000469', '39/332', '39/332', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000470', '40/337', '40/337', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000471', '41/15/343', '41/15/343', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000472', '42/349', '42/349', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000473', '43/376', '43/376', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000474', '45/C', '45/C', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000475', '46/B', '46/B', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000476', 'VSR 01', 'VSR 01', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000477', 'VSR 06', 'VSR 06', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000478', 'VSR 30', 'VSR 30', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000479', 'VSR 78', 'VSR 78', 'aktif', 'P000018');
INSERT INTO "public"."tbl_warna" VALUES ('P000480', '01/04', '01/04', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000481', '02/20', '02/20', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000482', '03/30', '03/30', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000483', '04/24', '04/24', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000484', '05/16', '05/16', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000485', '06/26', '06/26', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000486', '07/C', '07/C', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000487', '08/09', '08/09', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000488', '09/10', '09/10', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000489', '10/13', '10/13', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000490', '11/18', '11/18', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000491', '12/43', '12/43', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000492', '13/A', '13/A', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000493', '14/92', '14/92', 'aktif', 'P000019');
INSERT INTO "public"."tbl_warna" VALUES ('P000494', '01 JB', '01 JB', 'aktif', 'P000020');
INSERT INTO "public"."tbl_warna" VALUES ('P000495', '01 M.JB', '01 M.JB', 'aktif', 'P000021');
INSERT INTO "public"."tbl_warna" VALUES ('P000496', '01/96', '01/96', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000497', '02/26', '02/26', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000498', '03/80', '03/80', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000499', '04/31', '04/31', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000500', '05/79', '05/79', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000501', '06/114', '06/114', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000502', '07/76', '07/76', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000503', '08/34', '08/34', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000504', '09/58', '09/58', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000505', '10/90', '10/90', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000506', '11/88', '11/88', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000507', '12/119', '12/119', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000508', '13/107', '13/107', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000509', '14/110', '14/110', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000510', '15/38', '15/38', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000511', '16/43', '16/43', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000512', '17/86', '17/86', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000513', '18/19', '18/19', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000514', '19/124', '19/124', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000515', '20/72', '20/72', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000516', '21/257', '21/257', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000517', '22/A', '22/A', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000518', '24/277', '24/277', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000519', '48/33', '48/33', 'aktif', 'P000004');
INSERT INTO "public"."tbl_warna" VALUES ('P000520', '01/826', '01/826', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000521', '02/782', '02/782', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000522', '03/829', '03/829', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000523', '04/864', '04/864', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000524', '05/867', '05/867', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000525', '06/832', '06/832', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000526', '07/763', '07/763', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000527', '08/846', '08/846', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000528', '09/830', '09/830', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000529', '10/837', '10/837', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000530', '11/834', '11/834', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000531', '12/821', '12/821', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000532', '13/876', '13/876', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000533', '14/872', '14/872', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000534', '15/900', '15/900', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000535', '16/848', '16/848', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000536', '17/758', '17/758', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000537', '18/879', '18/879', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000538', '19/839', '19/839', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000539', '20/851', '20/851', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000540', '21/775', '21/775', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000541', '22/894', '22/894', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000542', '23/777', '23/777', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000543', '24/844', '24/844', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000544', '25/790', '25/790', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000545', '26/1012', '26/1012', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000546', '27/1038', '27/1038', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000547', '32/1270', '32/1270', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000548', '1030', '1030', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000549', '1063', '1063', 'aktif', 'P000022');
INSERT INTO "public"."tbl_warna" VALUES ('P000550', '01/001 Black', '01/001 Black', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000551', '02/B Navy', '02/B Navy', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000552', '03/C Brown', '03/C Brown', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000553', '04/H D.Brown', '04/H D.Brown', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000554', '05/01 D.Charcoal', '05/01 D.Charcoal', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000555', '06/P L.Brown', '06/P L.Brown', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000556', '07/S Cream', '07/S Cream', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000557', '08/A Khaky', '08/A Khaky', 'aktif', 'P000024');
INSERT INTO "public"."tbl_warna" VALUES ('P000558', 'GREIGE', 'GREIGE', 'aktif', 'P000026');

-- ----------------------------
-- Function structure for ap
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ap"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV'  AND date_part('month',tbl_pembelian."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB'  AND date_part('month',tbl_pembelian."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_pembayaran_hutang."Total"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier"  AND date_part('month',tbl_pembayaran_hutang."Tanggal") = '1' GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_pembelian_detail."Sub_total"),0) AS Saldo_awal from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_pembelian_detail on tbl_pembelian_detail."IDFB"=tbl_pembelian."IDFB" AND date_part('month',tbl_pembelian."Tanggal") = '1' GROUP BY tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ap_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8);
CREATE OR REPLACE FUNCTION "public"."ap_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV'  AND date_part('month',tbl_pembelian."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB'  AND date_part('month',tbl_pembelian."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_pembayaran_hutang."Total"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier"  AND date_part('month',tbl_pembayaran_hutang."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_pembelian_detail."Sub_total"),0) AS Saldo_awal from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_pembelian_detail on tbl_pembelian_detail."IDFB"=tbl_pembelian."IDFB" AND date_part('month',tbl_pembelian."Tanggal") = cari_bulan-1 GROUP BY tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ar"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP'  AND date_part('month',tbl_penjualan_kain."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ'  AND date_part('month',tbl_penjualan_kain."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_penerimaan_piutang."Total"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer"  AND date_part('month',tbl_penerimaan_piutang."Tanggal") = '1' GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_penjualan_kain_detail."Sub_total"),0) AS Saldo_awal from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_penjualan_kain_detail on tbl_penjualan_kain_detail."IDFJK"=tbl_penjualan_kain."IDFJK" AND date_part('month',tbl_penjualan_kain."Tanggal") = '1' GROUP BY tbl_customer."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."ar_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP' AND date_part('month',tbl_penjualan_kain."Tanggal") = caribulan GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ' AND date_part('month',tbl_penjualan_kain."Tanggal") = caribulan GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_penerimaan_piutang."Total"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer" AND date_part('month',tbl_penerimaan_piutang."Tanggal") = caribulan GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_penjualan_kain_detail."Sub_total"),0) AS Saldo_awal from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_penjualan_kain_detail on tbl_penjualan_kain_detail."IDFJK"=tbl_penjualan_kain."IDFJK" AND date_part('month',tbl_penjualan_kain."Tanggal") = caribulan-1 GROUP BY tbl_customer."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for bs
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar);
CREATE OR REPLACE FUNCTION "public"."bs"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_bulan_lalu, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_lalu, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_lalu, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan" ORDER BY tbl_setting_lr."IDSettingLR" ASC) m1
LEFT JOIN (
SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for bs_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."bs_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_bulan_lalu, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_lalu, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_lalu, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = caribulan-1
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan" ORDER BY tbl_setting_lr."IDSettingLR" ASC) m1
LEFT JOIN (
SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCOA, Nama_COA,Tanggal, Nomor, Saldo_awal, Debet, Kredit, Saldo_akhir IN
       
SELECT m1."IDCOA", m1."Nama_COA", m1."Tanggal", m1."Nomor", m2."saldo_awal", m1."Debet", m1."Kredit", m2."saldo_awal"+m1."Debet"-m1."Kredit" AS saldo_akhir  FROM (SELECT tbl_jurnal."IDCOA", tbl_coa."Nama_COA",tbl_jurnal."Tanggal", tbl_jurnal."Nomor", tbl_jurnal."Debet", tbl_jurnal."Kredit" FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" WHERE tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan) m1
LEFT JOIN
(SELECT tbl_jurnal."IDCOA", tbl_jurnal."Tanggal", tbl_jurnal."Nomor", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"),0) AS Saldo_awal FROM tbl_jurnal JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_jurnal."IDCOA", tbl_jurnal."Tanggal", tbl_jurnal."Nomor") m2 USING("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar_cari"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar_cari"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCOA, Nama_COA, Tanggal, Nomor, Saldo_awal, Debet, Kredit, Saldo_akhir IN
       
SELECT m1."IDCOA", m1."Nama_COA", m1."Tanggal", m1."Nomor", m2."saldo_awal", m1."Debet", m1."Kredit", m2."saldo_awal"+m1."Debet"+m1."Kredit" AS saldo_akhir  FROM (SELECT tbl_coa."Nama_COA", tbl_jurnal."IDCOA",tbl_jurnal."Tanggal", tbl_jurnal."Nomor", tbl_jurnal."Debet", tbl_jurnal."Kredit" FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" WHERE tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan) m1
LEFT JOIN
(SELECT tbl_jurnal."IDCOA", tbl_jurnal."Tanggal", tbl_jurnal."Nomor", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"),0) AS Saldo_awal FROM tbl_jurnal JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_jurnal."IDCOA", tbl_jurnal."Tanggal", tbl_jurnal."Nomor") m2 USING("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar_pembantu_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar_pembantu_cari"(OUT "idcoa" varchar, OUT "kode_coa" varchar, OUT "nama_coa" varchar, OUT "saldo_awal" float8, OUT "debit" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar_pembantu_cari"(OUT "idcoa" varchar, OUT "kode_coa" varchar, OUT "nama_coa" varchar, OUT "saldo_awal" float8, OUT "debit" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCoa, Kode_COA, Nama_COA, Saldo_awal, Debit, Kredit, Saldo_akhir IN
       
SELECT m1."IDCoa", m1."Kode_COA", m1."Nama_COA", m2."saldo_awal", m1."debit", m1."kredit", m2."saldo_awal"+m1."debit"-m1."kredit" AS saldo_akhir  FROM (SELECT tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Debit, coalesce(SUM(tbl_jurnal."Kredit"), 0) AS Kredit FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA") m1
LEFT JOIN
(select tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"), 0) AS Saldo_awal FROM tbl_jurnal RIGHT JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA") m2 USING("IDCoa")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_ip
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_ip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_ip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal, Barcode, NoSO, Party, Nama_Barang, Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party", g."Nama_Barang" AS "Nama_Barang", c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar" 
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
							INNER JOIN tbl_barang AS "g" 
							ON c."IDBarang" = g."IDBarang"
								
								WHERE a."Tanggal" >= datein
								AND a."Tanggal" <= dateuntil
								AND a."Nomor" like keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_ip_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_ip_like"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_ip_like"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal , Barcode, NoSO, Party, Nama_Barang, Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party", g."Nama_Barang" AS "Nama_Barang", c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar" 
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
							INNER JOIN tbl_barang AS "g" 
							ON c."IDBarang" = g."IDBarang"
								
								WHERE a."Nomor" like keywords 					
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_bo
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	
	WHERE BO."Tanggal" >= datein
								AND BO."Tanggal" <= dateuntil
								AND BO."Nomor" like keywords 					
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_bo_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_bo_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_bo_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	
	WHERE BO."Nomor" like keywords 				
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, IN "datein" date, IN "dateuntil" date, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, IN "datein" date, IN "dateuntil" date, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total"
FROM
	tbl_pembelian
  JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
	JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Tanggal" >= datein
								AND tbl_pembelian."Tanggal" <= dateuntil
								AND tbl_pembelian."Nomor" like keywords 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_pembelian_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total"
FROM
	tbl_pembelian
  JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
	JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Nomor" like keywords 				 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor, SJCK."Nomor" As "Nomor2", CUS."Nama" As "Nama", PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PK."Tanggal" >= datein
								AND PK."Tanggal" <= dateuntil
								AND PK."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_kain_like"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_kain_like"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor, SJCK."Nomor" As "Nomor2", CUS."Nama" As "Nama", PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PK."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjcs, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, SJCS."Nomor" As "Nomor2", CUS."Nama" As "Nama", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PSD."Qty_roll" as "Qty_roll", PSD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Tanggal" >= datein
								AND PS."Tanggal" <= dateuntil
								AND PS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_seragam_like"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_seragam_like"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjcs, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, SJCS."Nomor" As "Nomor2", CUS."Nama" As "Nama", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PSD."Qty_roll" as "Qty_roll", PSD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_packing_list
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
							WHERE PL."Tanggal" >= datein
								AND PL."Tanggal" <= dateuntil
								AND PL."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_packing_list_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_packing_list_like"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_packing_list_like"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
							WHERE PL."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_pembelian_asset
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	
	WHERE T1."Tanggal"::DATE >= datein
								AND T1."Tanggal"::DATE <= dateuntil
								AND T1."Nomor" like keywords 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_pembelian_asset_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_pembelian_asset_like"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_pembelian_asset_like"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	
	WHERE T1."Nomor" like keywords 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_barang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga
FROM
	tbl_terima_barang_supplier AS T1
	JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Tanggal" >= datein
								AND T1."Tanggal" <= dateuntil
								AND T1."Nomor" like keywords 
ORDER BY T1."IDTBS" DESC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_barang_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_barang_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_barang_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga
FROM
	tbl_terima_barang_supplier AS T1
	JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Nomor" like keywords 
ORDER BY T1."IDTBS" DESC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	
	WHERE tbl_retur_pembelian."Tanggal" >= datein
				AND tbl_retur_pembelian."Tanggal" <= dateuntil
				AND tbl_retur_pembelian."Nomor" like keywords 	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	
	WHERE tbl_retur_pembelian."Nomor" like keywords 		
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Tanggal" >= datein
	AND RPK."Tanggal" <= dateuntil
	AND RPK."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_kain_like"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_kain_like"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPSD."Qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPS."Tanggal" >= datein
				AND RPS."Tanggal" <= dateuntil
				AND RPS."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_seragam_like"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_seragam_like"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPSD."Qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPS."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							INNER JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							INNER JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
							WHERE SOK."Tanggal" >= datein
								AND SOK."Tanggal" <= dateuntil
								AND SOK."Nomor" like keywords 
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_kain_like"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_kain_like"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							INNER JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							INNER JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
							WHERE SOK."Nomor" like keywords 				 
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOSD."Qty" as "Qty", SOSD."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SOSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOSD."IDMerk" = MERK."IDMerk"
-- 							INNER JOIN tbl_satuan AS SATUAN
-- 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
								WHERE SOS."Tanggal" >= datein
								AND SOS."Tanggal" <= dateuntil
								AND SOS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_seragam_like"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_seragam_like"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOSD."Qty" as "Qty", SOSD."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SOSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOSD."IDMerk" = MERK."IDMerk"
-- 							INNER JOIN tbl_satuan AS SATUAN
-- 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
								WHERE SOS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_stokopname
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Tanggal" >= datein
								AND T1."Tanggal" <= dateuntil
								AND (T1."Barcode" like keywords OR T3."Corak" like keywords OR T4."Warna" like keywords)
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_stokopname_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_stokopname_like"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_stokopname_like"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Barcode" like keywords OR T3."Corak" like keywords OR T4."Warna" like keywords
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Warna, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
							WHERE SJCK."Tanggal" >= datein
								AND SJCK."Tanggal" <= dateuntil
								AND SJCK."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_kain_like"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_kain_like"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Warna, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
							WHERE SJCK."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Warna, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCSD."Qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SJCSD."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SJCSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							WHERE SJCS."Tanggal" >= datein
								AND SJCS."Tanggal" <= dateuntil
								AND SJCS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_seragam_like"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_seragam_like"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Warna, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCSD."Qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SJCSD."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SJCSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							WHERE SJCS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po"(OUT "idpo" int4, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "sample" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po"(OUT "idpo" int4, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "sample" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Sample, Qty IN
					 SELECT 
						PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Sample" as "Sample", POD."Qty" As "Qty"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
								
								WHERE PO."Jenis_PO" = jenis_po_in

								AND PO."Tanggal" >= datein
								AND PO."Tanggal" <= dateuntil
								AND PO."Nomor" like keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po_like"(OUT "idpo" int4, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "sample" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po_like"(OUT "idpo" int4, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "sample" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Sample, Qty IN
					 SELECT 
					PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Sample" as "Sample", POD."Qty" As "Qty"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
								
								WHERE PO."Jenis_PO" = jenis_po_in
								AND PO."Nomor" like keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cf_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cf_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_tahun_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."cf_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_tahun_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_tahun_lalu, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_tahun_lalu, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_setting_cf."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_tahun_lalu, tbl_setting_cf."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_cf ON tbl_jurnal."IDCOA"=tbl_setting_cf."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = '12' AND date_part('years',tbl_jurnal."Tanggal") = date_part('years',current_date)-1
GROUP BY tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_jurnal."IDCOA", tbl_setting_cf."Keterangan",tbl_setting_cf."Perhitungan" ORDER BY tbl_setting_cf."IDSettingCF" ASC) m1
LEFT JOIN 
(SELECT tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_setting_cf."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_cf."Perhitungan" FROM tbl_jurnal RIGHT JOIN tbl_setting_cf ON tbl_jurnal."IDCOA"=tbl_setting_cf."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_jurnal."IDCOA", tbl_setting_cf."Keterangan",tbl_setting_cf."Perhitungan"  ORDER BY tbl_setting_cf."IDSettingCF" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for getip
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."getip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar);
CREATE OR REPLACE FUNCTION "public"."getip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal, Barcode, NoSO, Party, Nama_Barang, Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party", g."Nama_Barang" AS "Nama_Barang", c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar"
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
							INNER JOIN tbl_barang AS "g" 
							ON c."IDBarang" = g."IDBarang"
			WHERE a."Batal"='Aktif'
    LOOP
	
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for getpo2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."getpo2"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "sample" varchar, OUT "qty" float8, OUT "batal" varchar, IN "jenis_po_in" varchar);
CREATE OR REPLACE FUNCTION "public"."getpo2"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "sample" varchar, OUT "qty" float8, OUT "batal" varchar, IN "jenis_po_in" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Sample, Qty, Batal IN
        SELECT 
						PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Sample" as "Sample", POD."Qty" As "Qty", PO."Batal" AS "Batal"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
							WHERE PO."Jenis_PO" = jenis_po_in 
							AND PO."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_bo
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty,
	Batal IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty",
	BO."Batal" AS "Batal"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	WHERE BO."Batal" = 'Aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total,
	Batal
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total",
	tbl_pembelian."Batal"
FROM
	tbl_pembelian
  JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
	JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Batal" = 'aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor,  CUS."Nama" As "Nama", PK."Nama_di_faktur" As "Nama_di_faktur", SJCK."Nomor" As "Nomor2",PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjcs, Tanggal_jatuh_tempo,Nama_Barang, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, SJCS."Nomor" As "Nomor2", CUS."Nama" As "Nama", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PSD."Qty_roll" as "Qty_roll", PSD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_packing_list
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_pembelian_asset
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur,
	Batal
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur",
	T1."Batal"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	WHERE T1."Batal"='aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_penerimaan_barang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,
	Batal
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga,
	T1. "Batal" AS Batal
FROM
	tbl_terima_barang_supplier AS T1
	JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
	WHERE T1."Batal" = 'Aktif'
ORDER BY T1."IDTBS" DESC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal,
	Batal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal",
	tbl_retur_pembelian."Batal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	WHERE tbl_retur_pembelian."Batal"='aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPSD."Qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_sales_order_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							INNER JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							INNER JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_sales_order_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SOSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_stokopname
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_surat_jalan_customer_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Warna, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_surat_jalan_customer_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Corak, Merk, Warna, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCSD."Qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
							INNER JOIN tbl_corak AS COR
								ON SJCSD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for notes
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."notes"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar);
CREATE OR REPLACE FUNCTION "public"."notes"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_setting_notes."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_notes."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_notes ON tbl_jurnal."IDCOA"=tbl_setting_notes."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_jurnal."IDCOA", tbl_setting_notes."Keterangan",tbl_setting_notes."Perhitungan"  ORDER BY tbl_setting_notes."IDSettingNotes" ASC
)m1
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for notes_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."notes_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."notes_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_setting_notes."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_notes."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_notes ON tbl_jurnal."IDCOA"=tbl_setting_notes."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_jurnal."IDCOA", tbl_setting_notes."Keterangan",tbl_setting_notes."Perhitungan"  ORDER BY tbl_setting_notes."IDSettingNotes" ASC
)m1
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for pl
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pl"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar);
CREATE OR REPLACE FUNCTION "public"."pl"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Bulan_sd_bulan, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Bulan_sd_bulan, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Bulan_sd_bulan, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") BETWEEN '1' AND '1'
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan" ORDER BY tbl_setting_pl."IDSettingPL" ASC) m1
LEFT JOIN (
SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan"  ORDER BY tbl_setting_pl."IDSettingPL" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for pl_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Bulan_sd_bulan, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m2.Total_bulan_ini, m1.Bulan_sd_bulan,m1."Perhitungan"
FROM (SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Bulan_sd_bulan, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") BETWEEN '1' AND caribulan
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan" ORDER BY tbl_setting_pl."IDSettingPL" ASC) m1
LEFT JOIN (
SELECT tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_setting_pl."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_pl."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_pl ON tbl_jurnal."IDCOA"=tbl_setting_pl."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_pl."IDSettingPL", tbl_setting_pl."Kategori",tbl_jurnal."IDCOA", tbl_setting_pl."Keterangan",tbl_setting_pl."Perhitungan"  ORDER BY tbl_setting_pl."IDSettingPL" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for testing_insert
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."testing_insert"();
CREATE OR REPLACE FUNCTION "public"."testing_insert"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
         INSERT INTO tbl_coa_saldo_awal(idcoasaldoawal,idcoa,idgroupcoa, nilaisaldoawal, aktif)
         VALUES(NEW.IDCOASaldoAwal,NEW.IDCoa,NEW.IDGroupCOA,NEW.Nilai_Saldo_Awal, NEW.Aktif);
 
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for update_stok
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."update_stok"();
CREATE OR REPLACE FUNCTION "public"."update_stok"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
begin
update stokbarang set stok_ketersediaan=stok_ketersediaan-new.jumlah_beli where kode_barang=new.kode_barang;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."tbl_agen_IDAgen_seq"
OWNED BY "public"."tbl_agen"."IDAgen";
SELECT setval('"public"."tbl_agen_IDAgen_seq"', 26, true);
ALTER SEQUENCE "public"."tbl_asset_IDAsset_seq"
OWNED BY "public"."tbl_asset"."IDAsset";
SELECT setval('"public"."tbl_asset_IDAsset_seq"', 31, true);
ALTER SEQUENCE "public"."tbl_asset_IDGroupAsset_seq"
OWNED BY "public"."tbl_asset"."IDGroupAsset";
SELECT setval('"public"."tbl_asset_IDGroupAsset_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_bank_IDBank_seq"
OWNED BY "public"."tbl_bank"."IDBank";
SELECT setval('"public"."tbl_bank_IDBank_seq"', 41, true);
ALTER SEQUENCE "public"."tbl_bank_IDCoa_seq"
OWNED BY "public"."tbl_bank"."IDCoa";
SELECT setval('"public"."tbl_bank_IDCoa_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_barang_IDBarang_seq"
OWNED BY "public"."tbl_barang"."IDBarang";
SELECT setval('"public"."tbl_barang_IDBarang_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_barang_IDCorak_seq"
OWNED BY "public"."tbl_barang"."IDCorak";
SELECT setval('"public"."tbl_barang_IDCorak_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_barang_IDGroupBarang_seq"
OWNED BY "public"."tbl_barang"."IDGroupBarang";
SELECT setval('"public"."tbl_barang_IDGroupBarang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_barang_IDMerk_seq"
OWNED BY "public"."tbl_barang"."IDMerk";
SELECT setval('"public"."tbl_barang_IDMerk_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_barang_IDSatuan_seq"
OWNED BY "public"."tbl_barang"."IDSatuan";
SELECT setval('"public"."tbl_barang_IDSatuan_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_booking_order_IDBO_seq"
OWNED BY "public"."tbl_booking_order"."IDBO";
SELECT setval('"public"."tbl_booking_order_IDBO_seq"', 24, true);
ALTER SEQUENCE "public"."tbl_booking_order_IDCorak_seq"
OWNED BY "public"."tbl_booking_order"."IDCorak";
SELECT setval('"public"."tbl_booking_order_IDCorak_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_booking_order_IDMataUang_seq"
OWNED BY "public"."tbl_booking_order"."IDMataUang";
SELECT setval('"public"."tbl_booking_order_IDMataUang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_coa_IDCoa_seq"
OWNED BY "public"."tbl_coa"."IDCoa";
SELECT setval('"public"."tbl_coa_IDCoa_seq"', 26, true);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDCOASaldoAwal";
SELECT setval('"public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDCoa_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDCoa";
SELECT setval('"public"."tbl_coa_saldo_awal_IDCoa_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDGroupCOA_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDGroupCOA";
SELECT setval('"public"."tbl_coa_saldo_awal_IDGroupCOA_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_corak_IDCorak_seq"
OWNED BY "public"."tbl_corak"."IDCorak";
SELECT setval('"public"."tbl_corak_IDCorak_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_corak_IDMerk_seq"
OWNED BY "public"."tbl_corak"."IDMerk";
SELECT setval('"public"."tbl_corak_IDMerk_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_customer_IDCustomer_seq"
OWNED BY "public"."tbl_customer"."IDCustomer";
SELECT setval('"public"."tbl_customer_IDCustomer_seq"', 16, true);
ALTER SEQUENCE "public"."tbl_customer_IDGroupCustomer_seq"
OWNED BY "public"."tbl_customer"."IDGroupCustomer";
SELECT setval('"public"."tbl_customer_IDGroupCustomer_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_giro_IDFaktur_seq"
OWNED BY "public"."tbl_giro"."IDFaktur";
SELECT setval('"public"."tbl_giro_IDFaktur_seq"', 24, true);
ALTER SEQUENCE "public"."tbl_giro_IDGiro_seq"
OWNED BY "public"."tbl_giro"."IDGiro";
SELECT setval('"public"."tbl_giro_IDGiro_seq"', 24, true);
ALTER SEQUENCE "public"."tbl_giro_IDPerusahaan_seq"
OWNED BY "public"."tbl_giro"."IDPerusahaan";
SELECT setval('"public"."tbl_giro_IDPerusahaan_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_group_asset_IDGroupAsset_seq"
OWNED BY "public"."tbl_group_asset"."IDGroupAsset";
SELECT setval('"public"."tbl_group_asset_IDGroupAsset_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_group_barang_IDGroupBarang_seq"
OWNED BY "public"."tbl_group_barang"."IDGroupBarang";
SELECT setval('"public"."tbl_group_barang_IDGroupBarang_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_group_coa_IDGroupCOA_seq"
OWNED BY "public"."tbl_group_coa"."IDGroupCOA";
SELECT setval('"public"."tbl_group_coa_IDGroupCOA_seq"', 9, true);
ALTER SEQUENCE "public"."tbl_group_customer_IDGroupCustomer_seq"
OWNED BY "public"."tbl_group_customer"."IDGroupCustomer";
SELECT setval('"public"."tbl_group_customer_IDGroupCustomer_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_group_supplier_IDGroupSupplier_seq"
OWNED BY "public"."tbl_group_supplier"."IDGroupSupplier";
SELECT setval('"public"."tbl_group_supplier_IDGroupSupplier_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_group_user_IDGroupUser_seq"
OWNED BY "public"."tbl_group_user"."IDGroupUser";
SELECT setval('"public"."tbl_group_user_IDGroupUser_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_gudang_IDGudang_seq"
OWNED BY "public"."tbl_gudang"."IDGudang";
SELECT setval('"public"."tbl_gudang_IDGudang_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDBarang_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDBarang";
SELECT setval('"public"."tbl_harga_jual_barang_IDBarang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDGroupCustomer_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDGroupCustomer";
SELECT setval('"public"."tbl_harga_jual_barang_IDGroupCustomer_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDHargaJual_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDHargaJual";
SELECT setval('"public"."tbl_harga_jual_barang_IDHargaJual_seq"', 31, true);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDSatuan_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDSatuan";
SELECT setval('"public"."tbl_harga_jual_barang_IDSatuan_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_hutang_IDFaktur_seq"
OWNED BY "public"."tbl_hutang"."IDFaktur";
SELECT setval('"public"."tbl_hutang_IDFaktur_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_hutang_IDHutang_seq"
OWNED BY "public"."tbl_hutang"."IDHutang";
SELECT setval('"public"."tbl_hutang_IDHutang_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_hutang_IDSupplier_seq"
OWNED BY "public"."tbl_hutang"."IDSupplier";
SELECT setval('"public"."tbl_hutang_IDSupplier_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_in_IDBarang_seq"
OWNED BY "public"."tbl_in"."IDBarang";
SELECT setval('"public"."tbl_in_IDBarang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_in_IDCorak_seq"
OWNED BY "public"."tbl_in"."IDCorak";
SELECT setval('"public"."tbl_in_IDCorak_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_in_IDIn_seq"
OWNED BY "public"."tbl_in"."IDIn";
SELECT setval('"public"."tbl_in_IDIn_seq"', 62, true);
ALTER SEQUENCE "public"."tbl_in_IDMerk_seq"
OWNED BY "public"."tbl_in"."IDMerk";
SELECT setval('"public"."tbl_in_IDMerk_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_in_IDSatuan_seq"
OWNED BY "public"."tbl_in"."IDSatuan";
SELECT setval('"public"."tbl_in_IDSatuan_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_in_IDWarna_seq"
OWNED BY "public"."tbl_in"."IDWarna";
SELECT setval('"public"."tbl_in_IDWarna_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDIP_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDIP";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDIP_seq"', 53, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDPO_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDPO";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDPO_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDSupplier_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDSupplier";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDSupplier_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDBarang";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDBarang_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDCorak";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDCorak_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDIPDetail";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq"', 94, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIP_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDIP";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDIP_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDMerk";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDMerk_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDSatuan";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDWarna";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDWarna_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDBarang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDBarang";
SELECT setval('"public"."tbl_kartu_stok_IDBarang_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDCorak_seq"
OWNED BY "public"."tbl_kartu_stok"."IDCorak";
SELECT setval('"public"."tbl_kartu_stok_IDCorak_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDFakturDetail_seq"
OWNED BY "public"."tbl_kartu_stok"."IDFakturDetail";
SELECT setval('"public"."tbl_kartu_stok_IDFakturDetail_seq"', 9, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDFaktur_seq"
OWNED BY "public"."tbl_kartu_stok"."IDFaktur";
SELECT setval('"public"."tbl_kartu_stok_IDFaktur_seq"', 9, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDGudang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDGudang";
SELECT setval('"public"."tbl_kartu_stok_IDGudang_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDKartuStok_seq"
OWNED BY "public"."tbl_kartu_stok"."IDKartuStok";
SELECT setval('"public"."tbl_kartu_stok_IDKartuStok_seq"', 208, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDMataUang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDMataUang";
SELECT setval('"public"."tbl_kartu_stok_IDMataUang_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDSatuan_seq"
OWNED BY "public"."tbl_kartu_stok"."IDSatuan";
SELECT setval('"public"."tbl_kartu_stok_IDSatuan_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDStok_seq"
OWNED BY "public"."tbl_kartu_stok"."IDStok";
SELECT setval('"public"."tbl_kartu_stok_IDStok_seq"', 10, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDWarna_seq"
OWNED BY "public"."tbl_kartu_stok"."IDWarna";
SELECT setval('"public"."tbl_kartu_stok_IDWarna_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDKonversi_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDKonversi";
SELECT setval('"public"."tbl_konversi_satuan_IDKonversi_seq"', 20, true);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDSatuanBerat_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDSatuanBerat";
SELECT setval('"public"."tbl_konversi_satuan_IDSatuanBerat_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDSatuanKecil_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDSatuanKecil";
SELECT setval('"public"."tbl_konversi_satuan_IDSatuanKecil_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_IDKP_seq"
OWNED BY "public"."tbl_koreksi_persediaan"."IDKP";
SELECT setval('"public"."tbl_koreksi_persediaan_IDKP_seq"', 60, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_IDMataUang_seq"
OWNED BY "public"."tbl_koreksi_persediaan"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_IDMataUang_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDBarang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDBarang";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDBarang_seq"', 18, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDCorak_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDCorak";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDCorak_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDKPDetail";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq"', 78, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKP_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDKP";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDKP_seq"', 13, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDMataUang_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDSatuan";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDSatuan_seq"', 18, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDWarna_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDWarna";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDWarna_seq"', 12, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan"."IDKPScan";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_IDKPScan_seq"', 33, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDBarang";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDCorak";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDCorak_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDKPScanDetail";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"', 37, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDKPScan";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDSatuan";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq"
OWNED BY "public"."tbl_koreksi_persediaan_scan_detail"."IDWarna";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDWarna_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_kota_IDKota_seq"
OWNED BY "public"."tbl_kota"."IDKota";
SELECT setval('"public"."tbl_kota_IDKota_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDBarang_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDBarang";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDBarang_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDCorak_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDCorak";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDCorak_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDLapPers_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDLapPers";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDLapPers_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDMerk_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDMerk";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDMerk_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDWarna_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDWarna";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDWarna_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_mata_uang_IDMataUang_seq"
OWNED BY "public"."tbl_mata_uang"."IDMataUang";
SELECT setval('"public"."tbl_mata_uang_IDMataUang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_menu_IDMenu_seq"
OWNED BY "public"."tbl_menu"."IDMenu";
SELECT setval('"public"."tbl_menu_IDMenu_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_menu_detail_IDMenuDetail_seq"
OWNED BY "public"."tbl_menu_detail"."IDMenuDetail";
SELECT setval('"public"."tbl_menu_detail_IDMenuDetail_seq"', 109, true);
ALTER SEQUENCE "public"."tbl_menu_detail_IDMenu_seq"
OWNED BY "public"."tbl_menu_detail"."IDMenu";
SELECT setval('"public"."tbl_menu_detail_IDMenu_seq"', 13, true);
ALTER SEQUENCE "public"."tbl_menu_role_IDGroupUser_seq"
OWNED BY "public"."tbl_menu_role"."IDGroupUser";
SELECT setval('"public"."tbl_menu_role_IDGroupUser_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_menu_role_IDMenuDetail_seq"
OWNED BY "public"."tbl_menu_role"."IDMenuDetail";
SELECT setval('"public"."tbl_menu_role_IDMenuDetail_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_menu_role_IDMenuRole_seq"
OWNED BY "public"."tbl_menu_role"."IDMenuRole";
SELECT setval('"public"."tbl_menu_role_IDMenuRole_seq"', 3017, true);
ALTER SEQUENCE "public"."tbl_merk_IDMerk_seq"
OWNED BY "public"."tbl_merk"."IDMerk";
SELECT setval('"public"."tbl_merk_IDMerk_seq"', 16, true);
ALTER SEQUENCE "public"."tbl_out_IDOut_seq"
OWNED BY "public"."tbl_out"."IDOut";
SELECT setval('"public"."tbl_out_IDOut_seq"', 34, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDCOA_seq"
OWNED BY "public"."tbl_pembayaran"."IDCOA";
SELECT setval('"public"."tbl_pembayaran_IDCOA_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDFBPembayaran_seq"
OWNED BY "public"."tbl_pembayaran"."IDFBPembayaran";
SELECT setval('"public"."tbl_pembayaran_IDFBPembayaran_seq"', 32, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDFB_seq"
OWNED BY "public"."tbl_pembayaran"."IDFB";
SELECT setval('"public"."tbl_pembayaran_IDFB_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_pembayaran_IDMataUang_seq"
OWNED BY "public"."tbl_pembayaran"."IDMataUang";
SELECT setval('"public"."tbl_pembayaran_IDMataUang_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDFB_seq"
OWNED BY "public"."tbl_pembelian"."IDFB";
SELECT setval('"public"."tbl_pembelian_IDFB_seq"', 111, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDMataUang_seq"
OWNED BY "public"."tbl_pembelian"."IDMataUang";
SELECT setval('"public"."tbl_pembelian_IDMataUang_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDSupplier_seq"
OWNED BY "public"."tbl_pembelian"."IDSupplier";
SELECT setval('"public"."tbl_pembelian_IDSupplier_seq"', 10, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDTBS_seq"
OWNED BY "public"."tbl_pembelian"."IDTBS";
SELECT setval('"public"."tbl_pembelian_IDTBS_seq"', 9, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_IDFBA_seq"
OWNED BY "public"."tbl_pembelian_asset"."IDFBA";
SELECT setval('"public"."tbl_pembelian_asset_IDFBA_seq"', 92, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_IDMataUang_seq"
OWNED BY "public"."tbl_pembelian_asset"."IDMataUang";
SELECT setval('"public"."tbl_pembelian_asset_IDMataUang_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDAsset_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDAsset";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDAsset_seq"', 9, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBADetail_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDFBADetail";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDFBADetail_seq"', 182, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBA_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDFBA";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDFBA_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDGroupAsset";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDGroupAsset_seq"', 9, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDBarang_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDBarang";
SELECT setval('"public"."tbl_pembelian_detail_IDBarang_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDCorak_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDCorak";
SELECT setval('"public"."tbl_pembelian_detail_IDCorak_seq"', 20, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDFBDetail_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDFBDetail";
SELECT setval('"public"."tbl_pembelian_detail_IDFBDetail_seq"', 135, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDFB_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDFB";
SELECT setval('"public"."tbl_pembelian_detail_IDFB_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDMerk_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDMerk";
SELECT setval('"public"."tbl_pembelian_detail_IDMerk_seq"', 25, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDSatuan_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDSatuan";
SELECT setval('"public"."tbl_pembelian_detail_IDSatuan_seq"', 35, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDTBSDetail_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDTBSDetail";
SELECT setval('"public"."tbl_pembelian_detail_IDTBSDetail_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDWarna_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDWarna";
SELECT setval('"public"."tbl_pembelian_detail_IDWarna_seq"', 25, true);
ALTER SEQUENCE "public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq"
OWNED BY "public"."tbl_pembelian_grand_total"."IDFBGrandTotal";
SELECT setval('"public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq"', 50, true);
ALTER SEQUENCE "public"."tbl_pembelian_grand_total_IDFB_seq"
OWNED BY "public"."tbl_pembelian_grand_total"."IDFB";
SELECT setval('"public"."tbl_pembelian_grand_total_IDFB_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_piutang_IDCustomer_seq"
OWNED BY "public"."tbl_piutang"."IDCustomer";
SELECT setval('"public"."tbl_piutang_IDCustomer_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_piutang_IDFaktur_seq"
OWNED BY "public"."tbl_piutang"."IDFaktur";
SELECT setval('"public"."tbl_piutang_IDFaktur_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_piutang_IDPiutang_seq"
OWNED BY "public"."tbl_piutang"."IDPiutang";
SELECT setval('"public"."tbl_piutang_IDPiutang_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_purchase_order_IDAgen_seq"
OWNED BY "public"."tbl_purchase_order"."IDAgen";
SELECT setval('"public"."tbl_purchase_order_IDAgen_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_purchase_order_IDBarang_seq"
OWNED BY "public"."tbl_purchase_order"."IDBarang";
SELECT setval('"public"."tbl_purchase_order_IDBarang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_purchase_order_IDCorak_seq"
OWNED BY "public"."tbl_purchase_order"."IDCorak";
SELECT setval('"public"."tbl_purchase_order_IDCorak_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_purchase_order_IDMataUang_seq"
OWNED BY "public"."tbl_purchase_order"."IDMataUang";
SELECT setval('"public"."tbl_purchase_order_IDMataUang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_purchase_order_IDMerk_seq"
OWNED BY "public"."tbl_purchase_order"."IDMerk";
SELECT setval('"public"."tbl_purchase_order_IDMerk_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_purchase_order_IDPO_seq"
OWNED BY "public"."tbl_purchase_order"."IDPO";
SELECT setval('"public"."tbl_purchase_order_IDPO_seq"', 86, true);
ALTER SEQUENCE "public"."tbl_purchase_order_IDSupplier_seq"
OWNED BY "public"."tbl_purchase_order"."IDSupplier";
SELECT setval('"public"."tbl_purchase_order_IDSupplier_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_purchase_order_detail_IDPODetail_seq"
OWNED BY "public"."tbl_purchase_order_detail"."IDPODetail";
SELECT setval('"public"."tbl_purchase_order_detail_IDPODetail_seq"', 90, true);
ALTER SEQUENCE "public"."tbl_purchase_order_detail_IDPO_seq"
OWNED BY "public"."tbl_purchase_order_detail"."IDPO";
SELECT setval('"public"."tbl_purchase_order_detail_IDPO_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_purchase_order_detail_IDWarna_seq"
OWNED BY "public"."tbl_purchase_order_detail"."IDWarna";
SELECT setval('"public"."tbl_purchase_order_detail_IDWarna_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_IDFB_seq"
OWNED BY "public"."tbl_retur_pembelian"."IDFB";
SELECT setval('"public"."tbl_retur_pembelian_IDFB_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_IDRB_seq"
OWNED BY "public"."tbl_retur_pembelian"."IDRB";
SELECT setval('"public"."tbl_retur_pembelian_IDRB_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_retur_pembelian_IDSupplier_seq"
OWNED BY "public"."tbl_retur_pembelian"."IDSupplier";
SELECT setval('"public"."tbl_retur_pembelian_IDSupplier_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_detail_IDBarang_seq"
OWNED BY "public"."tbl_retur_pembelian_detail"."IDBarang";
SELECT setval('"public"."tbl_retur_pembelian_detail_IDBarang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_detail_IDCorak_seq"
OWNED BY "public"."tbl_retur_pembelian_detail"."IDCorak";
SELECT setval('"public"."tbl_retur_pembelian_detail_IDCorak_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_detail_IDMerk_seq"
OWNED BY "public"."tbl_retur_pembelian_detail"."IDMerk";
SELECT setval('"public"."tbl_retur_pembelian_detail_IDMerk_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_detail_IDRBDetail_seq"
OWNED BY "public"."tbl_retur_pembelian_detail"."IDRBDetail";
SELECT setval('"public"."tbl_retur_pembelian_detail_IDRBDetail_seq"', 42, true);
ALTER SEQUENCE "public"."tbl_retur_pembelian_detail_IDRB_seq"
OWNED BY "public"."tbl_retur_pembelian_detail"."IDRB";
SELECT setval('"public"."tbl_retur_pembelian_detail_IDRB_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_detail_IDSatuan_seq"
OWNED BY "public"."tbl_retur_pembelian_detail"."IDSatuan";
SELECT setval('"public"."tbl_retur_pembelian_detail_IDSatuan_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_detail_IDWarna_seq"
OWNED BY "public"."tbl_retur_pembelian_detail"."IDWarna";
SELECT setval('"public"."tbl_retur_pembelian_detail_IDWarna_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDMataUang_seq"
OWNED BY "public"."tbl_retur_pembelian_grand_total"."IDMataUang";
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDMataUang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq"
OWNED BY "public"."tbl_retur_pembelian_grand_total"."IDRBGrandTotal";
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq"', 25, true);
ALTER SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDRB_seq"
OWNED BY "public"."tbl_retur_pembelian_grand_total"."IDRB";
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDRB_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_saldo_awal_asset_IDAsset_seq"
OWNED BY "public"."tbl_saldo_awal_asset"."IDAsset";
SELECT setval('"public"."tbl_saldo_awal_asset_IDAsset_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq"
OWNED BY "public"."tbl_saldo_awal_asset"."IDSaldoAwalAsset";
SELECT setval('"public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_satuan_IDSatuan_seq"
OWNED BY "public"."tbl_satuan"."IDSatuan";
SELECT setval('"public"."tbl_satuan_IDSatuan_seq"', 18, true);
ALTER SEQUENCE "public"."tbl_stok_IDBarang_seq"
OWNED BY "public"."tbl_stok"."IDBarang";
SELECT setval('"public"."tbl_stok_IDBarang_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_stok_IDCorak_seq"
OWNED BY "public"."tbl_stok"."IDCorak";
SELECT setval('"public"."tbl_stok_IDCorak_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_stok_IDFakturDetail_seq"
OWNED BY "public"."tbl_stok"."IDFakturDetail";
SELECT setval('"public"."tbl_stok_IDFakturDetail_seq"', 28, true);
ALTER SEQUENCE "public"."tbl_stok_IDFaktur_seq"
OWNED BY "public"."tbl_stok"."IDFaktur";
SELECT setval('"public"."tbl_stok_IDFaktur_seq"', 28, true);
ALTER SEQUENCE "public"."tbl_stok_IDGudang_seq"
OWNED BY "public"."tbl_stok"."IDGudang";
SELECT setval('"public"."tbl_stok_IDGudang_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_stok_IDMataUang_seq"
OWNED BY "public"."tbl_stok"."IDMataUang";
SELECT setval('"public"."tbl_stok_IDMataUang_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_stok_IDSatuan_seq"
OWNED BY "public"."tbl_stok"."IDSatuan";
SELECT setval('"public"."tbl_stok_IDSatuan_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_stok_IDStok_seq"
OWNED BY "public"."tbl_stok"."IDStok";
SELECT setval('"public"."tbl_stok_IDStok_seq"', 170, true);
ALTER SEQUENCE "public"."tbl_stok_IDWarna_seq"
OWNED BY "public"."tbl_stok"."IDWarna";
SELECT setval('"public"."tbl_stok_IDWarna_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_stok_history_AsalIDFakturDetail_seq"
OWNED BY "public"."tbl_stok_history"."AsalIDFakturDetail";
SELECT setval('"public"."tbl_stok_history_AsalIDFakturDetail_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_AsalIDFaktur_seq"
OWNED BY "public"."tbl_stok_history"."AsalIDFaktur";
SELECT setval('"public"."tbl_stok_history_AsalIDFaktur_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDBarang_seq"
OWNED BY "public"."tbl_stok_history"."IDBarang";
SELECT setval('"public"."tbl_stok_history_IDBarang_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDCorak_seq"
OWNED BY "public"."tbl_stok_history"."IDCorak";
SELECT setval('"public"."tbl_stok_history_IDCorak_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDFakturDetail_seq"
OWNED BY "public"."tbl_stok_history"."IDFakturDetail";
SELECT setval('"public"."tbl_stok_history_IDFakturDetail_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDFaktur_seq"
OWNED BY "public"."tbl_stok_history"."IDFaktur";
SELECT setval('"public"."tbl_stok_history_IDFaktur_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDGudang_seq"
OWNED BY "public"."tbl_stok_history"."IDGudang";
SELECT setval('"public"."tbl_stok_history_IDGudang_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDHistoryStok_seq"
OWNED BY "public"."tbl_stok_history"."IDHistoryStok";
SELECT setval('"public"."tbl_stok_history_IDHistoryStok_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDMataUang_seq"
OWNED BY "public"."tbl_stok_history"."IDMataUang";
SELECT setval('"public"."tbl_stok_history_IDMataUang_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDSatuan_seq"
OWNED BY "public"."tbl_stok_history"."IDSatuan";
SELECT setval('"public"."tbl_stok_history_IDSatuan_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDStok_seq"
OWNED BY "public"."tbl_stok_history"."IDStok";
SELECT setval('"public"."tbl_stok_history_IDStok_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_history_IDWarna_seq"
OWNED BY "public"."tbl_stok_history"."IDWarna";
SELECT setval('"public"."tbl_stok_history_IDWarna_seq"', 7, false);
ALTER SEQUENCE "public"."tbl_stok_opname_IDBarang_seq"
OWNED BY "public"."tbl_stok_opname"."IDBarang";
SELECT setval('"public"."tbl_stok_opname_IDBarang_seq"', 7, true);
ALTER SEQUENCE "public"."tbl_stok_opname_IDCorak_seq"
OWNED BY "public"."tbl_stok_opname"."IDCorak";
SELECT setval('"public"."tbl_stok_opname_IDCorak_seq"', 8, true);
ALTER SEQUENCE "public"."tbl_stok_opname_IDGudang_seq"
OWNED BY "public"."tbl_stok_opname"."IDGudang";
SELECT setval('"public"."tbl_stok_opname_IDGudang_seq"', 9, true);
ALTER SEQUENCE "public"."tbl_stok_opname_IDSatuan_seq"
OWNED BY "public"."tbl_stok_opname"."IDSatuan";
SELECT setval('"public"."tbl_stok_opname_IDSatuan_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_stok_opname_IDStokOpname_seq"
OWNED BY "public"."tbl_stok_opname"."IDStokOpname";
SELECT setval('"public"."tbl_stok_opname_IDStokOpname_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_stok_opname_IDWarna_seq"
OWNED BY "public"."tbl_stok_opname"."IDWarna";
SELECT setval('"public"."tbl_stok_opname_IDWarna_seq"', 8, true);
ALTER SEQUENCE "public"."tbl_suplier_IDGroupSupplier_seq"
OWNED BY "public"."tbl_suplier"."IDGroupSupplier";
SELECT setval('"public"."tbl_suplier_IDGroupSupplier_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_suplier_IDSupplier_seq"
OWNED BY "public"."tbl_suplier"."IDSupplier";
SELECT setval('"public"."tbl_suplier_IDSupplier_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDPO_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup"."IDPO";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDPO_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDSJM_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup"."IDSJM";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDSJM_seq"', 21, true);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup"."IDSupplier";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup_detail"."IDBarang";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup_detail"."IDCorak";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup_detail"."IDMerk";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup_detail"."IDSJMDetail";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq"', 24, true);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup_detail"."IDSJM";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup_detail"."IDSatuan";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_celup_detail"."IDWarna";
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit"."IDSJH";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit"."IDSupplier";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit_detail"."IDBarang";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit_detail"."IDCorak";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit_detail"."IDMerk";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit_detail"."IDSJHDetail";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq"', 25, true);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit_detail"."IDSJH";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit_detail"."IDSatuan";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq"
OWNED BY "public"."tbl_surat_jalan_makloon_jahit_detail"."IDWarna";
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_tampungan_IDT_seq"
OWNED BY "public"."tbl_tampungan"."IDT";
SELECT setval('"public"."tbl_tampungan_IDT_seq"', 11, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_IDPO_seq"
OWNED BY "public"."tbl_terima_barang_supplier"."IDPO";
SELECT setval('"public"."tbl_terima_barang_supplier_IDPO_seq"', 53, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_IDSupplier_seq"
OWNED BY "public"."tbl_terima_barang_supplier"."IDSupplier";
SELECT setval('"public"."tbl_terima_barang_supplier_IDSupplier_seq"', 43, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_IDTBS_seq"
OWNED BY "public"."tbl_terima_barang_supplier"."IDTBS";
SELECT setval('"public"."tbl_terima_barang_supplier_IDTBS_seq"', 102, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDBarang_seq"
OWNED BY "public"."tbl_terima_barang_supplier_detail"."IDBarang";
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDBarang_seq"', 14, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDCorak_seq"
OWNED BY "public"."tbl_terima_barang_supplier_detail"."IDCorak";
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDCorak_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDMerk_seq"
OWNED BY "public"."tbl_terima_barang_supplier_detail"."IDMerk";
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDMerk_seq"', 28, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDSatuan_seq"
OWNED BY "public"."tbl_terima_barang_supplier_detail"."IDSatuan";
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDSatuan_seq"', 30, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq"
OWNED BY "public"."tbl_terima_barang_supplier_detail"."IDTBSDetail";
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq"', 262, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDTBS_seq"
OWNED BY "public"."tbl_terima_barang_supplier_detail"."IDTBS";
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDTBS_seq"', 10, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDWarna_seq"
OWNED BY "public"."tbl_terima_barang_supplier_detail"."IDWarna";
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDWarna_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDSJH_seq"
OWNED BY "public"."tbl_terima_barang_supplier_jahit"."IDSJH";
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDSJH_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq"
OWNED BY "public"."tbl_terima_barang_supplier_jahit"."IDSupplier";
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq"
OWNED BY "public"."tbl_terima_barang_supplier_jahit"."IDTBSH";
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq"', 41, true);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"
OWNED BY "public"."tbl_terima_barang_supplier_jahit_detail"."IDBarang";
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"', 9, false);
ALTER SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"
OWNED BY "public"."tbl_terima_barang_supplier_jahit_detail"."IDMerk";
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"', 9, false);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq"', 9, false);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq"', 47, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq"', 9, false);
SELECT setval('"public"."tbl_um_customer_IDCustomer_seq"', 9, false);
SELECT setval('"public"."tbl_um_customer_IDFaktur_seq"', 16, true);
SELECT setval('"public"."tbl_um_customer_IDUMCustomer_seq"', 15, true);
SELECT setval('"public"."tbl_user_IDGroupUser_seq"', 9, false);
SELECT setval('"public"."tbl_user_IDUser_seq"', 24, true);
SELECT setval('"public"."tbl_warna_IDWarna_seq"', 17, true);

-- ----------------------------
-- Primary Key structure for table tbl_down_payment
-- ----------------------------
ALTER TABLE "public"."tbl_down_payment" ADD CONSTRAINT "tbl_down_payment_pkey" PRIMARY KEY ("IDDownPayment");

-- ----------------------------
-- Primary Key structure for table tbl_pembelian_umum_detail
-- ----------------------------
ALTER TABLE "public"."tbl_pembelian_umum_detail" ADD CONSTRAINT "tbl_pembelian_umum_detail_pkey" PRIMARY KEY ("IDFBUmumDetail");

-- ----------------------------
-- Primary Key structure for table tbl_pembelian_umum_grand_total
-- ----------------------------
ALTER TABLE "public"."tbl_pembelian_umum_grand_total" ADD CONSTRAINT "tbl_pembelian_umum_grnad_total_pkey" PRIMARY KEY ("IDFBUmumGrandTotal");

-- ----------------------------
-- Primary Key structure for table tbl_purchase_order_umum
-- ----------------------------
ALTER TABLE "public"."tbl_purchase_order_umum" ADD CONSTRAINT "tbl_purchase_order_umum_pkey" PRIMARY KEY ("IDPOUmum");

-- ----------------------------
-- Primary Key structure for table tbl_purchase_order_umum_detail
-- ----------------------------
ALTER TABLE "public"."tbl_purchase_order_umum_detail" ADD CONSTRAINT "tbl_purchase_order_umum_detail_pkey" PRIMARY KEY ("IDPOUmumDetail");

-- ----------------------------
-- Primary Key structure for table tbl_retur_pembelian_umum
-- ----------------------------
ALTER TABLE "public"."tbl_retur_pembelian_umum" ADD CONSTRAINT "tbl_retur_pembelian_umum_pkey" PRIMARY KEY ("IDRBUmum");

-- ----------------------------
-- Primary Key structure for table tbl_retur_pembelian_umum_detail
-- ----------------------------
ALTER TABLE "public"."tbl_retur_pembelian_umum_detail" ADD CONSTRAINT "tbl_retur_pembelian_umum_detail_pkey" PRIMARY KEY ("IDRBUmumDetail");

-- ----------------------------
-- Primary Key structure for table tbl_retur_pembelian_umum_grand_total
-- ----------------------------
ALTER TABLE "public"."tbl_retur_pembelian_umum_grand_total" ADD CONSTRAINT "tbl_retur_pembelian_umum_grand_total_pkey" PRIMARY KEY ("IDRBUmumGrandToal");

-- ----------------------------
-- Primary Key structure for table tbl_setting_cf
-- ----------------------------
ALTER TABLE "public"."tbl_setting_cf" ADD CONSTRAINT "tbl_setting_cf_pkey" PRIMARY KEY ("IDSettingCF");

-- ----------------------------
-- Primary Key structure for table tbl_setting_lr
-- ----------------------------
ALTER TABLE "public"."tbl_setting_lr" ADD CONSTRAINT "tbl_setting_lr_pkey" PRIMARY KEY ("IDSettingLR");

-- ----------------------------
-- Primary Key structure for table tbl_setting_notes
-- ----------------------------
ALTER TABLE "public"."tbl_setting_notes" ADD CONSTRAINT "tbl_setting_notes_pkey" PRIMARY KEY ("IDSettingNotes");

-- ----------------------------
-- Primary Key structure for table tbl_setting_pl
-- ----------------------------
ALTER TABLE "public"."tbl_setting_pl" ADD CONSTRAINT "tbl_setting_pl_pkey" PRIMARY KEY ("IDSettingPL");

-- ----------------------------
-- Primary Key structure for table tbl_terima_barang_supplier_umum
-- ----------------------------
ALTER TABLE "public"."tbl_terima_barang_supplier_umum" ADD CONSTRAINT "tbl_terima_barang_supplier_umum_pkey" PRIMARY KEY ("IDTBSUmum");

-- ----------------------------
-- Primary Key structure for table tbl_terima_barang_supplier_umum_detail
-- ----------------------------
ALTER TABLE "public"."tbl_terima_barang_supplier_umum_detail" ADD CONSTRAINT "tbl_terima_barang_supplier_umum_detail_pkey" PRIMARY KEY ("IDTBSUmumDetail");

-- ----------------------------
-- Primary Key structure for table tbl_um_supplier
-- ----------------------------
ALTER TABLE "public"."tbl_um_supplier" ADD CONSTRAINT "tbl_um_supplier_pkey" PRIMARY KEY ("IDUMSupplier");
