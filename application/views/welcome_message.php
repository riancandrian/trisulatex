<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width"/>
    <title>PT Trisula Textile Industries Tbk</title>
    <link href="<?php echo base_url() ?>asset/css/reset.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/shCore.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/jquery.jqplot.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/data-table.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/form.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/styles-admin.css" rel="stylesheet" type="text/css" media="screen">
</head>
<body id="theme-default" class="full_block">
    <div id="login_page">
        <div class="login_container">
            <!-- <div class="login_header blue_lgel">
                <ul class="login_branding">
                    <li>
                        <div class="logo_small">
                            <img src="<?php //echo base_url() ?>asset/images/logo-bingo.png" width="99" height="35" alt="bingo">
                        </div>
                        <span>PT Trisula Textile Industries Tbk</span>
                    </li>
                </ul>
            </div> -->
            <form action="<?php echo base_url() ?>Login/aksi_login" method="post">
                <div class="login_form">
                    <h3 class="blue_d">Login</h3>
                    <ul>
                        <li class="login_user">
                            <input id="username" name="username" type="text" placeholder="Username">
                        </li>
                        <li class="login_pass">
                            <input id="pass" name="password" type="password" placeholder="Password">
                        </li>
                    </ul>
                </div>
                <input class="login_btn blue_lgel" name="" value="Login" type="submit">
                <ul class="login_opt_link">
                    <!-- <li><a href="forgot-pass.html">Forgot Password?</a></li> -->
                   <!--  <li class="remember_me right">
                        <input name="apdiv" class="rem_me" type="checkbox" value="checked">
                    Remember Me</li> -->
                </ul>
            </form>
        </div>
    </div>


    <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery-ui-1.8.18.custom.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.ui.touch-punch.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chosen.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/uniform.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/bootstrap-dropdown.js"></script>
    <script src="<?php echo base_url() ?>asset/js/bootstrap-colorpicker.js"></script>
    <script src="<?php echo base_url() ?>asset/js/sticky.full.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.noty.js"></script>
    <script src="<?php echo base_url() ?>asset/js/selectToUISlider.jQuery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/fg.menu.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.cleditor.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.tipsy.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.peity.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.simplemodal.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.jBreadCrumb.1.1.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.colorbox-min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.idTabs.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.multiFieldExtender.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.confirm.js"></script>
    <script src="<?php echo base_url() ?>asset/js/elfinder.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/accordion.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/autogrow.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/check-all.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/data-table.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/ZeroClipboard.js"></script>
    <script src="<?php echo base_url() ?>asset/js/TableTools.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jeditable.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/duallist.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/easing.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/full-calendar.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/input-limiter.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/inputmask.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/iphone-style-checkbox.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/meta-data.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/quicksand.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/raty.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/smart-wizard.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/stepy.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/treeview.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/ui-accordion.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/vaidation.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/js/mosaic.1.0.1.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.collapse.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.cookie.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.autocomplete.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/localdata.js"></script>
    <script src="<?php echo base_url() ?>asset/js/excanvas.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/jquery.jqplot.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.dateAxisRenderer.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.cursor.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.logAxisRenderer.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.canvasTextRenderer.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.highlighter.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.pieRenderer.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.barRenderer.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.categoryAxisRenderer.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.pointLabels.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.meterGaugeRenderer.min.js"></script>
    <script src="<?php echo base_url() ?>asset/js/custom-scripts.js"></script>

    <script type="text/javascript">
        $(function(){
            $(window).resize(function(){
                $('.login_container').css({
                    position:'absolute',
                    left: ($(window).width() - $('.login_container').outerWidth())/2,
                    top: ($(window).height() - $('.login_container').outerHeight())/2
                });
            });
            // To initially run the function:
            $(window).resize();
        });
    </script>
    <!-- <body class="hold-transition fixed sidebar-mini"> -->

        <!-- Content Wrapper -->
        <!-- <div class="login-wrapper">
            <div class="container-center">
                <div class="panel panel-bd">
                    <div class="panel-heading">
                        <div class="view-header">
                            <div class="header-icon">
                                <i class="pe-7s-unlock"></i>
                            </div>
                            <div class="header-title">
                                <h4>Login</h4>
                                <small><strong>Enterprise Resource Planning PT Trisula Textile Industries Tbk – Quality, Care And Commitment</strong></small>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo base_url() ?>Login/aksi_login" method="post"> -->
                            <!--Social Buttons--> 
                            <!-- <?php echo $this->session->flashdata('pesan');?>
                            <div class="form-group">
                                <label class="control-label">Username</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="username" type="text" class="form-control" name="username" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input id="pass" type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                            </div>
                            <div>
                                <button class="btn btn-base pull-right">Login</button>

                                <a href="<?php echo base_url() ?>Forgot">Forgot password?</a>

                            </div>
                        </form>


                    </div>
                </div>
                <div id="bottom_text">
                    <a href="#">PT Trisula Textile Industries Tbk</a>
                </div>
            </div>
        </div> -->  <!-- /.content-wrapper -->

        
        <!-- START CORE PLUGINS -->
       <!--  <script src="<?php echo base_url() ?>asset/assets/plugins/jQuery/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>asset/assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mV9NBwzoSAQVtJocZ40nWiPpq7NyUtEF4xpCl09f6OIaQch77PuT9Pop51bc44hQrkOAVTYmPrOp2BnVwzOrWUwrpPOtXkneidmQ0EqvKaB8GgAjRI5AaWjODS1yoifQWpjPqiAjY2SiCJLIEYyvh8bqVCwaZssv0OZAZPml%2f%2btojdRHSuVYcO8ZTylABwgiEFFnxF%2fE5zPHIlrCBvRYn2nJLmLqb8uDq%2felnitWYb5IC4LDj8Mx5MS6ykNtMJnXVWs8WMl6nCyIa9YrcXDim70ZLTsfeTcQjfYbtAxsujFDP2h%2f2nPMkLWdCqcwjRJ4L6WjSohZq%2bp8Tmfh3cq%2bK7GJVr5yBPcvC7fIxRbe5kvehs%2f3VSgM199iLTTBXnvJMnacqMICAV8%2f5eHBV8k1Nc6L%2bVcbcYjK9Hhv43%2f3c0Etsmyiaq7idibvkTLfX44Dxn5msvf9rjh4kv7BIhb7ySVcukTc8mwCU%2fWaa4TLMbYY%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body> -->

        <!-- Mirrored from adminpix.thememinister.com/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jul 2018 14:06:37 GMT -->
    </body>
    </html>