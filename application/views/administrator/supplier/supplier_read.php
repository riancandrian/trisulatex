<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Supplier</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('Supplier/index')?>">Data Supplier</a></li>
                            <li style="text-transform: capitalize;"> Detail Supplier - <b><?php echo $Nama?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Group Supplier</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?= 'Kode Group : '.$Kode_Group_Supplier.' - Group : '.$Group_Suplier ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Kode Supplier</td>
                                    <td style="background-color: #ffffff;"> <?php echo $Kode_Suplier; ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Nama</td>
                                    <td style="background-color: #e5eff0;"><?php echo $Nama; ?>  </tr>  
                                     <tr>         
                                        <td style="background-color: #ffffff;">Alamat</td>
                                        <td style="background-color: #ffffff;"><?php echo $Alamat ?></td>
                                    </tr> 
                                    <tr>         
                                        <td style="background-color: #e5eff0;">No Telpon</td>
                                        <td style="background-color: #e5eff0;"><?php echo $No_Telpon ?></td>
                                    </tr>  
                                    <tr>         
                                        <td style="background-color: #ffffff;">Kota</td>
                                        <td style="background-color: #ffffff;"><?php echo $Kota ?></td>
                                    </tr>  
                                    <tr>         
                                        <td style="background-color: #e5eff0;">Fax</td>
                                        <td style="background-color: #e5eff0;"><?php echo $Fax ?></td>
                                    </tr>  
                                    <tr>         
                                        <td style="background-color: #ffffff;">Email</td>
                                        <td style="background-color: #ffffff;"><?php echo $Email ?></td>
                                    </tr>  
                                    <tr>         
                                        <td style="background-color: #e5eff0;">NPWP</td>
                                        <td style="background-color: #e5eff0;"><?php echo $NPWP?></td>
                                    </tr>  
                                    <tr>         
                                        <td style="background-color: #ffffff;">No KTP</td>
                                        <td style="background-color: #ffffff;"><?php echo $No_KTP ?></td>
                                    </tr>  
                                    <tr>         
                                        <td style="background-color: #e5eff0;">Status</td>
                                        <td style="background-color: #e5eff0;"><?php echo $Aktif ?></td>
                                    </tr>  
                                </tbody>
                                <tfoot></tfoot>
                            </table>

                            <div class="widget_content py-4 text-center">
                                <div class="form_grid_12">
                                    <div class="btn_30_light">
                                        <span> <a href="<?php echo base_url('Supplier/index')?>" name="simpan">Kembali</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('administrator/footer') ; ?>