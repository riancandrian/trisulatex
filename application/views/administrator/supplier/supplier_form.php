<?php $this->load->view('administrator/header') ; 
?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3><?= $title_master ?></h3>
 
 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Supplier">Data <?= $title ?></a></li>
              <li class="active">Tambah Supplier</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6><?= $title ?></h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="<?php echo $action; ?>" class="form_container left_label">
            <ul>
              <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title">Group Supplier</label>
                  <div class="form_input">
                    <div class="grid_12 w-100 alpha">

                     <?php
                      if($this->uri->segment(2)=='create'){
                      ?>
                      <div class="grid_9 ml">
                        <?php
                      }else{
                        ?>
                          <div class="grid_12 ml">
                            <?php
                      }
                      ?>

                        <select data-placeholder="Pilih Group Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDGroupSupplier" required oninvalid="this.setCustomValidity('Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                         <option value=""></option>
                         <?php 
                         foreach ($DataGroup as $key ) { 
                          if ($key->IDGroupSupplier == $IDGroupSupplier) { ?>
                          <option value="<?= $key->IDGroupSupplier ?>" selected> <?= 'Kode Group : '.$key->Kode_Group_Supplier.' - Group : '.$key->Group_Supplier ?></option> 
                          <!-- Bila Data Sudah isi sebelumnya -->
                          <?php }
                          ?>
                          <option value="<?= $key->IDGroupSupplier ?>"> <?= 'Kode Group : '.$key->Kode_Group_Supplier.' - Group '.$key->Group_Supplier ?></option> 

                          <?php }
                          ?>
                        </select>
                      </div>
                      <?php
                      if($this->uri->segment(2)=='create'){
                      ?>
                      <div class="form_grid_3 mr">
                        <div class="btn_24_blue flot-right">
                         <span><a href="#open-modal">Tambah Group Supplier</a></span>
                       </div>
                     </div>
                     <?php }else{
                      echo "";
                     } ?>
                   </div>
                   <div class="clear"></div>
                 </div>

                 <label class="field_title">Kode Supplier</label>
                 <div class="form_input">
                  <?php
                      if($Kode_Suplier!='SRLT-01'){
                      ?>
                   <input type="text" class="form-control" name="Kode_Suplier" id="Kode_Suplier" placeholder="Kode_Suplier" value="<?php echo $Kode_Suplier; ?>" required oninvalid="this.setCustomValidity('Kode Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                   <?php
                      }else{
                        ?>
                       <input type="text" class="form-control" name="Kode_Suplier" id="Kode_Suplier" placeholder="Kode_Suplier" value="<?php echo $Kode_Suplier; ?>" required oninvalid="this.setCustomValidity('Kode Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')" disabled>
                   <span class="clear"></span>
                        <?php
                      }
                      ?>
                 </div>

                 <label class="field_title">Nama</label>
                 <div class="form_input">
                   <input type="text" class="form-control" name="Nama" id="Nama" placeholder="Nama" value="<?php echo $Nama; ?>" required oninvalid="this.setCustomValidity('Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Alamat</label>
                 <div class="form_input">
                   <input type="text" class="form-control" name="Alamat" id="Alamat" placeholder="Alamat" value="<?php echo $Alamat; ?>" required oninvalid="this.setCustomValidity('Alamat Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">No Telpon</label>
                 <div class="form_input">
                  <input type="text" class="form-control" name="No_Telpon" id="No_Telpon" placeholder="0821xxxxxx" value="<?php echo $No_Telpon; ?>" required oninvalid="this.setCustomValidity('No Telpon Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                  <span class="clear"></span>
                </div>

                <label class="field_title">Kota</label>
                <div class="form_input">
                  <div class="grid_12 w-100 alpha">


                     <?php
                      if($this->uri->segment(2)=='create'){
                      ?>
                      <div class="grid_9 ml">
                        <?php
                      }else{
                        ?>
                          <div class="grid_12 ml">
                            <?php
                      }
                      ?>
                      <select data-placeholder="Pilih Kota" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDKota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                       <option value=""></option>
                       <?php 
                       foreach ($DataKota as $key ) { 
                        if ($key->IDKota == $IDKota) { ?>
                        <option value="<?= $key->IDKota ?>" selected> <?= $key->Kota ?></option> 
                        <!-- Bila Data Sudah isi sebelumnya -->
                        <?php }
                        ?>
                        <option value="<?= $key->IDKota ?>"> <?= $key->Kota ?></option> 

                        <?php }
                        ?>
                      </select>
                    </div>
                    <?php
                      if($this->uri->segment(2)=='create'){
                      ?>
                    <div class="form_grid_3 mr">
                      <div class="btn_24_blue flot-right">
                       <span><a href="#open-modal-kota">Tambah Kota</a></span>
                     </div>
                   </div>
                   <?php }else{
                    echo "";
                   } ?>
                 </div>
                 <span class="clear"></span>
               </div>

               <label class="field_title">Fax</label>
               <div class="form_input">
                <input type="text" class="form-control" name="Fax" id="Fax" placeholder=" Fax" value="<?php echo $Fax; ?>" required oninvalid="this.setCustomValidity('Fax Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <span class="clear"></span>
              </div>

              <label class="field_title">Email</label>
              <div class="form_input">
               <input type="email" class="form-control" name="Email" id="Email" placeholder=" Email" value="<?php echo $Email; ?>" required oninvalid="this.setCustomValidity('Email Tidak Boleh Kosong')" oninput="setCustomValidity('')">
               <span class="clear"></span>
             </div>

             <label class="field_title">NPWP</label>
             <div class="form_input">
              <input type="text" class="form-control" name="NPWP" id="NPWP" placeholder=" NPWP" value="<?php echo $NPWP; ?>" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
              <span class="clear"></span>
            </div>

            <label class="field_title">No KTP</label>
            <div class="form_input">
             <input type="text" class="form-control" name="No_KTP" id="No_KTP" placeholder=" No_KTP" value="<?php echo $No_KTP; ?>" required oninvalid="this.setCustomValidity('No KTP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
             <span class="clear"></span>
           </div>
         </div>
       </li>
       <li>
        <div class="form_grid_12">
          <div class="form_input">
            <div class="btn_30_light">
             <input type="hidden" name="IDSupplier" value="<?php echo $IDSupplier; ?>" /> 
             <span> <a href="<?= $action_back ?>" class="btn btn-primary">Kembali</a></span>
           </div>
           <div class="btn_30_blue">
            <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
          </div>
        </div>
      </div>
    </li>
  </ul>
</form>
</div>
</div>
</div>
</div>
</div>
<div id="open-modal" class="modal-window">
 <div>
   <form method="post" action="<?php echo base_url() ?>groupsupplier/simpan" enctype="multipart/form-data" class="form_container left_label">
    <a href="#modal-close" title="Close" class="modal-close">X</a>
    <h1>Tambah Group Customer</h1>
    <div class="modal-body">

      <ul>
        <li>
          <div class="form_grid_12 multiline">
            <label class="field_title">Kode Group Supplier</label>
            <div class="form_input">

              <input type="text" class="form-control" name="Kode_Group_Supplier" placeholder="Kode Group Supplier" required oninvalid="this.setCustomValidity('Kode Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
              <span class="clear"></span>
            </div>
            <label class="field_title">Group Supplier</label>
            <div class="form_input">
             <input type="text" name="Group_Supplier" class="form-control" placeholder="Nama Group Supplier" required oninvalid="this.setCustomValidity('Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
             <span class="clear"></span>
           </div>
         </div>
       </li>
     </ul>

   </div>
   <div class="modal-foot">
                  <!--   <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                      </div> -->
                      <div class="btn_30_blue">
                       <input type="submit" name="simbar" value="Simpan">
                     </div>
                   </div>
                 </form>
               </div>
             </div>

             <div id="open-modal-kota" class="modal-window">
               <div>
                 <form method="post" action="<?php echo base_url() ?>Kota/proses_kota" enctype="multipart/form-data" class="form_container left_label">
                  <a href="#modal-close" title="Close" class="modal-close">X</a>
                  <h1>Tambah Kota</h1>
                  <div class="modal-body">

                    <ul>
                      <li>
                        <div class="form_grid_12 multiline">

                          <label class="field_title">Kode Kota</label>
                          <div class="form_input">

                            <input type="text" class="form-control" placeholder="Kode Kota" name="kode" required oninvalid="this.setCustomValidity('Kode Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                            <span class="clear"></span>
                          </div>
                          <label class="field_title">Provinsi</label>
                          <div class="form_input">
                           <input type="text" class="form-control" placeholder="Provinsi" name="provinsi" required oninvalid="this.setCustomValidity('Provinsi Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                           <span class="clear"></span>
                         </div>
                         <label class="field_title">Kota</label>
                         <div class="form_input">
                          <input type="text" class="form-control" placeholder="Kota" name="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <span class="clear"></span>
                        </div>
                      </div>
                    </li>
                  </ul>

                </div>
                <div class="modal-foot">
                  <!--   <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                      </div> -->
                      <div class="btn_30_blue">
                       <input type="submit" name="simsup" value="Simpan">
                     </div>
                   </div>
                 </form>
               </div>
             </div>
             <?php $this->load->view('administrator/footer') ; ?>