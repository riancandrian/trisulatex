<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Agen</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Agen </li>
                        </ul>
                    </div>
                </div>

                <!-- search table via button -->
                <div class="widget_content">
                    <form action="<?php echo base_url() ?>Agen/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                                <div class="form_grid_12 w-100 mx">
                                    <div class="form_grid_4">
                                        <div class="btn_24_blue flot-left">
                                            <a href="<?php echo base_url('Agen/tambah_agen')?>"><span>Tambah Data</span></a>
                                        </div>
                                    </div>
                                    <div class="form_grid_2">
                                        <select name="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="Kode_Perusahaan">Kode Perusahaan</option>
                                            <option value="Nama_Perusahaan">Nama Perusahaan</option>
                                            <option value="Initial">Initial</option>
                                            <option value="Alamat">Alamat</option>
                                            <option value="Kota">Kota</option>
                                            <option value="No_Tlp">No Telpon</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_2">
                                        <select name="statuspencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_3">
                                        <input name="keyword" type="text" placeholder="Masukkan Keyword">

                                        <!-- <span class=" label_intro">Last Name</span> -->
                                    </div>
                                    <div class="form_grid_1">
                                        <div class="btn_24_blue flot-right">
                                            <input type="submit" name="" value="search">
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>
                            </li>
                        </form>
                    </div>

                    <!-- table -->
                    <div class="widget_content">
                        <table class="display data_tbl">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Perusahaan</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Initial</th>
                                    <th>Kota</th>
                                    <th>Aktif</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($agen)){
                                    ?>

                                    <?php
                                }else{
                                    $i=1;
                                    foreach ($agen as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $data->Kode_Perusahaan; ?></td>
                                            <td><?php echo $data->Nama_Perusahaan; ?></td>
                                            <td><?php echo $data->Initial; ?></td>
                                            <td><?php echo $data->Kota; ?></td>
                                            <td><?php echo $data->Aktif; ?></td>
                                            <td class="text-center ukuran-logo">
                                                <span><a class="action-icons c-Detail" href="<?php echo base_url('Agen/show/'.$data->IDAgen)?>" title="Detail Data">Detail</a></span>
                                                <span><a class="action-icons c-edit" href="<?php echo base_url()?>Agen/edit_agen/<?php echo $data->IDAgen ?>" title="Ubah Data">Edit</a></span>
                                                <span>
                                                    <a class="action-icons c-delete confirm" href="#open-modal<?php echo $data->IDAgen ?>" title="Hapus Data">Delete</a>
                                                </span>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>

                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
    if ($agen=="" || $agen==null) {

    }else{
        foreach ($agen as $data) {
            ?>
            <div id="open-modal<?php echo $data->IDAgen;?>" class="modal-window">
                <div>
                    <a href="#modal-close" title="Close" class="modal-close">close ×</a>
                    <h1>Konfirmasi Hapus</h1>
                    <div>Apakah anda yakin akan menghapus data ini ?</div>
                    <div>
                        <a href="<?php echo base_url() ?>Agen/hapus_agen/<?php echo $data->IDAgen; ?>"><button class="btn btn-primary">Ya</button></a>
                        <a href="#modal-close" title="Close" class="btn btn-link">Batal</a>
                    </div>
                </div>
            </div>
            <?php 
        }
    } 
    ?> 
    <?php $this->load->view('administrator/footer') ; ?>