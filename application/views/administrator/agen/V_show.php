<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Agen</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="<?php echo base_url('Agen/index')?>">Data Agen</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li style="text-transform: capitalize;"> Detail Agen - <b><?php echo $agen->Nama_Perusahaan?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody> 
                                <tr class="odd gradeA">         
                                    <td style="background-color: #e5eff0;">Kode Perusahaan</td>
                                    <td style="background-color: #e5eff0;"><?php echo $agen->Kode_Perusahaan ?></td>
                                </tr>
                                <tr class="odd gradeA">         
                                    <td style="background-color: #ffffff;">Nama Perusahaan</td>
                                    <td style="background-color: #ffffff;"><?php echo $agen->Nama_Perusahaan?></td>
                                </tr>  
                                <tr class="odd gradeA">         
                                    <td style="background-color: #e5eff0;">Initial</td>
                                    <td style="background-color: #e5eff0;"><?php echo $agen->Initial ?></td>
                                </tr>  
                                <tr class="odd gradeA">         
                                    <td style="background-color: #ffffff;">Alamat</td>
                                    <td style="background-color: #ffffff;"><?php echo $agen->Alamat ?></td>
                                </tr> 
                                <tr class="odd gradeA">         
                                    <td style="background-color: #e5eff0;">Kota</td>
                                    <td style="background-color: #e5eff0;"><?php echo $agen->Kota ?></td>
                                </tr>  
                                <tr class="odd gradeA">         
                                    <td style="background-color: #ffffff;">No Telpon</td>
                                    <td style="background-color: #ffffff;"><?php echo $agen->No_Tlp ?></td>
                                </tr>  
                                <tr class="odd gradeA">         
                                    <td style="background-color: #e5eff0;">Image</td>
                                    <td style="background-color: #e5eff0;"><img src="<?php echo base_url() ?>upload/<?php echo $agen->Image ?>" style="width: 100px;"></td>
                                </tr>  
                                <tr class="odd gradeA">         
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $agen->Aktif ?></td>
                                </tr>  
                            </tbody>
                            <tfoot></tfoot>
                        </table>

                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('Agen/index')?>" name="simpan" title=".classname">Kembali</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ========= -->

<!-- <div class="content-wrapper">
    <div class="content">
        <div class="content-header">
            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1>Detail Agen</h1>
                <ol class="breadcrumb">
                    <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                    <li><a href="#">Agen</a></li>
                    <li class="active">Detail Agen</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="panel panel-bd">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="user" class="table table-bordered table-striped">
                                <tbody> 
                                    <tr>         
                                        <td width="35%">Kode Perusahaan</td>
                                        <td width="65%"><?php echo $agen->Kode_Perusahaan ?></td>
                                    </tr>
                                    <tr>         
                                        <td>Nama Perusahaan</td>
                                        <td><?php echo $agen->Nama_Perusahaan?></td>
                                    </tr>  
                                    <tr>         
                                        <td>Initial</td>
                                        <td><?php echo $agen->Initial ?></td>
                                    </tr>  
                                    <tr>         
                                        <td>Alamat</td>
                                        <td><?php echo $agen->Alamat ?></td>
                                    </tr> 
                                    <tr>         
                                        <td>Kota</td>
                                        <td><?php echo $agen->Kota ?></td>
                                    </tr>  
                                    <tr>         
                                        <td>No Telpon</td>
                                        <td><?php echo $agen->No_Tlp ?></td>
                                    </tr>  
                                    <tr>         
                                        <td>Image</td>
                                        <td><img src="<?php echo base_url() ?>upload/<?php echo $agen->Image ?>" style="width: 100px;"></td>
                                    </tr>  
                                    <tr>         
                                        <td>Status</td>
                                        <td><?php echo $agen->Aktif ?></td>
                                    </tr>  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-body__title">
                    <center>
                        <a href="<?php echo base_url('Agen/index')?>"><button type="button" class="btn btn-primary">Kembali</button></a>
                    </center>
                </div>
            </div>
        </div>
    </div> 
</div> -->
<?php $this->load->view('administrator/footer') ; ?>