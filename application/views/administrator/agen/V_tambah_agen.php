<?php $this->load->view('administrator/header') ; 

?>
<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Tambah Data Agen</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>agen">Data Agen</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Tambah Agen </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Agen</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Agen/proses_agen" enctype="multipart/form-data" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                 
                                    <!-- Kode Perusahaan -->
                                    <label class="field_title">Kode Perusahaan</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Kode Perusahaan" name="kode" required oninvalid="this.setCustomValidity('Kode Perusahaan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Nama Persusahaan -->
                                    <label class="field_title">Nama Perusahaan</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Nama Perusahaan" name="nama" required oninvalid="this.setCustomValidity('Nama Perusahaan Tidak Boleh Kosong')" oninput="setCustomValidity('')">    
                                    </div>

                                    <!-- NPWP -->
                                    <label class="field_title">NPWP</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="NPWP" name="npwp" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Initial -->
                                    <label class="field_title">Initial</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Initial" name="initial" required oninvalid="this.setCustomValidity('Initial Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Alamat -->
                                    <label class="field_title">Alamat</label>
                                    <div class="form_input">
                                        <textarea class="form-control" placeholder="Alamat" name="alamat" required oninvalid="this.setCustomValidity('Alamat Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="resize: none;" rows="6"></textarea>
                                    </div>

                                    <!-- Kota -->
                                    <label class="field_title">Kota</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Kota" name="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>    
                                            <?php foreach($kota as $kot)
                                            {
                                                echo "<option value='$kot->IDKota'>$kot->Kota</option>";
                                            } 
                                            ?>
                                        </select>
                                    </div>

                                    <!-- No Telepon -->
                                    <label class="field_title">No Telepon</label>
                                    <div class="form_input">
                                    <input type="text" class="form-control" placeholder="No Telpon" name="telp" required oninvalid="this.setCustomValidity('No Telpon Tidak Boleh Kosong')" value="+62" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Foto -->
                                    <label class="field_title">Image</label>
                                    <div class="form_input">
                                        <div class="text-center">
                                            <img src="https://www.howardhead.org/custom/images/blank-profile-hi.png" border=1 id="addimage" style="object-fit: cover;width: 200px;height: 200px;">
                                        </div>
                                        <div class="text-center mt-2">
                                            <input type="file" class="form-control " placeholder="Image" id="image" title="Choose file" onchange="readURL(this);" name="image" required oninvalid="this.setCustomValidity('Image Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                    <!-- Status -->
                                    <label class="field_title">Status</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Status" name="status" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================== -->

<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#addimage')
        .attr('src', e.target.result)
        .width(200)
        .height(200);
    };

    reader.readAsDataURL(input.files[0]);
}
}
</script>
<?php $this->load->view('administrator/footer') ; ?>