<?php $this->load->view('administrator/header') ; ?>
<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Edit Data Agen</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>agen">Data Agen</a></li>
                            <li> Edit Agen </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Agen</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Agen/proses_edit_agen/<?php echo $edit->IDAgen ?>" enctype="multipart/form-data" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">

                                    <!-- Kode Perusahaan -->
                                    <label class="field_title">Kode Perusahaan</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Kode Perusahaan" name="kode" value="<?php echo $edit->Kode_Perusahaan ?>">
                                    </div>

                                    <!-- Nama Persusahaan -->
                                    <label class="field_title">Nama Perusahaan</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Nama Perusahaan" name="nama" value="<?php echo $edit->Nama_Perusahaan?>">  
                                    </div>

                                    <!-- NPWP -->
                                    <!-- <label class="field_title">NPWP</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="NPWP" name="npwp" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php //echo $edit->NPWP?>">
                                    </div> -->

                                    <!-- Initial -->
                                    <label class="field_title">Initial</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Initial" name="initial" value="<?php echo $edit->Initial?>">
                                    </div>

                                    <!-- Alamat -->
                                    <label class="field_title">Alamat</label>
                                    <div class="form_input">
                                        <textarea class="form-control" placeholder="Alamat" name="alamat" style="resize: none;" rows="6"><?php echo $edit->Kode_Perusahaan ?></textarea>
                                    </div>

                                    <!-- Kota -->
                                    <label class="field_title">Kota</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Kota" name="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""> </option>
                                            <?php
                                            foreach ($kota as $kot) {
                                                if ($edit->IDKota==$kot->IDKota) {
                                                    ?>
                                                    <option value="<?php echo $kot->IDKota ?>" selected><?php echo $kot->Kota; ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $kot->IDKota ?>"><?php echo $kot->Kota; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <!-- No Telepon -->
                                    <label class="field_title">No Telepon</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="No Telpon" name="telp" value="<?php echo $edit->No_Tlp ?>">
                                    </div>

                                    <!-- Foto -->
                                    <label class="field_title">Ganti Image</label>
                                    <div class="form_input">
                                        <div class="text-center">
                                            <img src="<?php echo base_url() ?>upload/<?php echo $edit->Image ?>" style="width: 200px;height: 200px;">
                                        </div>
                                        <div class="text-center mt-2">
                                            <input type="file" class="form-control " placeholder="Image" id="image" title="Choose file" onchange="readURL(this);" name="image" required oninvalid="this.setCustomValidity('Image Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                    <!-- Status -->
                                    <label class="field_title">Kota</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Kota" name="status" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php if($edit->Aktif=="aktif"){ ?>
                                            <option value="aktif" selected>Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                            <?php }elseif($edit->Aktif=="tidak aktif"){ ?>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif" selected>Tidak Aktif</option>
                                            <?php }else{ ?>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                            <span> <a href="<?php echo base_url() ?>Agen" name="simpan" title=".classname">Kembali</a></span>
                                        </div>
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('administrator/footer') ; ?>