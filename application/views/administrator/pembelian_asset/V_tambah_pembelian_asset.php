<?php $this->load->view('administrator/header') ; 


if ($kodenoasset->curr_number == null) {
  $number = 1;
} else {
  $number = $kodenoasset->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_no_asset = 'PA'.'/'.date('y').'/'.str_replace(' ', '',$agen).'/'.$kodemax;
?>
<div class="page_title">
    <div id="alert"></div>
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Pendaftaran Asset</h3>

    <div class="top_search">
    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Pembelian_asset">Data Pendaftaran Asset</a></li>
                            <li class="active">Tambah Pendaftaran Asset</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Pendaftaran Asset</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Pembelian_asset/simpan_pembelian_asset" class="form_container left_label">
                        <ul>
                            <li class="body-search-data">
                                <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_6">
                                       
                                        <label class="field_title mt-dot2">Tanggal</label>
                                        <div class="form_input">
                                            <input id="Tanggal" type="date" name="Tanggal" value="<?php echo date('Y-m-d'); ?>" class="input-date-padding form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        </div>
                                    </div>
                                    <div class="form_grid_6">
                                        <label class="field_title mt-dot2">No Asset</label>
                                        <div class="form_input input-not-focus">
                                            <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $code_no_asset; ?>" readonly>
                                            <!-- <span class="clear"></span> -->
                                            <!-- <span class=" label_intro">Last Name</span> -->
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12 w-100 alpha">

                                    <!-- form 1 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2">Group Asset</label>
                                        <div class="form_input">
                                            <select data-placeholder="Cari Group Asset" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDGroupAsset" id="IDGroupAsset" required oninvalid="this.setCustomValidity('Group Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')" onchange="cari_groupasset()">
                                                <?php 
                                                foreach ($group_asset as $data) {
                                                    ?>
                                                    <option value="<?php echo $data->Group_Asset ?>"><?php echo $data->Group_Asset; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <!-- form 2 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title">Metode Penyusutan</label>
                                        <div class="form_input">
                                            <select data-placeholder="Cari Metode Penyusutan" style=" width:100%;" class="chzn-select" tabindex="13"  name="Metode_penyusutan" id="Metode_penyusutan" required oninvalid="this.setCustomValidity('Metode Penyusutan Tidak Boleh Kosong')" oninput="setCustomValidity('')" onchange="metodepenyusutan()">
                                                <option value="">Pilih Metode</option>
                                                <option value="double decline">Double Decline</option>
                                                <option value="stright line">Stright Line</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <span class="clear"></span> -->

                                    <!-- form 3 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2">Nama Asset</label>
                                        <div class="form_input">
                                           <select name="IDAsset" id="IDAsset" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" class="form-control">
                                            <option value="">Pilih Group Asset Terlebih Dahulu</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- form 4 -->
                                    <div class="form_grid_6 mb-1">

                                        <label class="field_title pt">Tanggal Penyusutan</label>
                                        <div class="form_input input-not-focus">
                                            <input type="date" id="Tanggal_penyusutan" name="Tanggal_penyusutan" class="form-control" placeholder="Tanggal Penyusutan" required oninvalid="this.setCustomValidity('Tanggal Penyusutan Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tgl_terakhir ?>" readonly>
                                            <!-- <span class="clear"></span> -->
                                        </div>
                                    </div>
                                    
                                    <!-- form 5 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title">Nilai Perolehan</label>
                                        <div class="form_input">
                                            <input type="text" id="harga-123" class="form-control nilai" name="Nilai_perolehan" placeholder="Nilai Perolehan" required oninvalid="this.setCustomValidity('Nilai Perolehan Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup ="nilaimetode()" />
                                        </div>
                                    </div>

                                    <!-- form 6 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2">Umur</label>
                                        <div class="form_input input-not-focus">
                                            <input type="text" name="Umur" id="Umur" class="form-control" placeholder="Umur" required oninvalid="this.setCustomValidity('Umur Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                            <span class="clear"></span>
                                        </div>
                                    </div>
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2">Tarif Penyusutan</label>
                                        <div class="form_input input-not-focus">
                                            <input type="text" name="Tarif" id="Tarif" class="form-control" placeholder="Tarif" required oninvalid="this.setCustomValidity('Tarif Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                            <span class="clear"></span>
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>

                                <!-- form 7 -->
                                  <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2">Nilai Buku</label>
                                        <div class="form_input input-not-focus">
                                            <input type="text" id="Nilai_buku" class="form-control" name="Nilai_buku" placeholder="Nilai Buku" required oninvalid="this.setCustomValidity('Nilai Buku Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly/>
                                            <span class="clear"></span>
                                        </div>
                                    </div>

                                    <span class="clear"></span>
                                </div>
                               

                              
                                
                                <!-- form 8 -->
                                 <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title">Akumulasi Penyusutan</label>
                                        <div class="form_input input-not-focus">
                                            <input type="text" id="Akumulasi_penyusutan" class="form-control" name="Akumulasi_penyusutan" placeholder="Nilai Akumulasi Penyusutan" required oninvalid="this.setCustomValidity('Nilai Akumulasi Penyusutan Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly/>
                                            <span class="clear"></span>
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>
                                  <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_6 mb-1">
                                        <label id="akumulasiselanjutnya" class="field_title"></label>
                                         <div class="form_input input-not-focus">
                                           <input type="text" id="nilaiakumulasiselanjutnya" readonly>
                                            <span class="clear"></span>
                                        </div> 
                                    </div>
                                    <span class="clear"></span>
                                </div>
                                
                            </li>
                       <!--      <li>
                                <div class="form_grid_12">
                                    <div class="form_input ml text-center w-100">
    
                                        <button class="btn_24_blue" type="button" id="add-to-cart">
                                            Tambah Data
                                        </button>


                                    </div>
                                </div>
                            </li> -->
                            
                        </ul>
                    <!--     <div>
                            <table class="display">
                                <thead>
                                    <tr>
                                        <th class="center" style="width: 40px">No</th>
                                        <th>Nama Asset</th>
                                        <th class="hidden-xs">Group Asset</th>
                                        <th>Nilai Perolehan</th>
                                        <th>Akumulasi Penyusutan</th>
                                        <th>Nilai Buku</th>
                                        <th>Metode Penyusutan</th>
                                        <th>Tanggal Penyusutan</th>
                                        <th>Umur</th>
                                        <th style="width: 50px">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tabel-cart-asset">
                                    
                                </tbody>
                            </table>
                            <ul>
                                <li class="body-search-data total-accumulation">
                                    <div class="form_grid_12 w-100">
                     
                                        <div class="form_grid_4">
                                            <input type="text" id="Total_nilai_perolehan" name="Total_nilai_perolehan" class="form-control" placeholder="Total Nilai Perolehan" readonly>
                                        </div>
                                        <div class="form_grid_4">
                                            <input type="text" id="Total_akumulasi_penyusutan" name="Total_akumulasi_penyusutan" class="form-control" placeholder="Total Akumulasi Penyusutan" readonly>
                                        </div>
                                        <div class="form_grid_4">
                                            <input type="text" id="Total_nilai_buku" name="Total_nilai_buku" class="form-control" placeholder="Total Nilai Buku" readonly>
                                        </div>
                                        <span class="clear"></span>
                                    </div>
                                </li>
                            </ul>
                        </div> -->


                    
                    <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2">
                          <!--   <div class="btn_30_blue">
                          <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                      </div> -->
                      
                      <div class="btn_30_blue">
                        <span><input type="submit" class="btn_24_blue" id="simpan" value="Simpan">
                        </span>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>

<div id="basic-modal-content">
    <h3>Ubah Detail Pendaftaran Asset</h3>
    <form class="form_container left_label">
     
        <div class="clear"></div><br>
        <div class="form_grid_12">
            <div class="form_grid_3">
                <label class="field_title">Asset</label>
            </div>
            <div class="form_grid_9">
                <div class="form_input">
                    <select data-placeholder="Cari Asset" style="width:100%;" class="" tabindex="13" name="vasset" id="vasset"></select>
                </div>
            </div>
        </div>
        <div class="clear"></div><br>

        <div class="form_grid_12">
            <div class="form_grid_3">
                <label class="field_title">Group Asset</label>
            </div>
            <div class="form_grid_9">
                <div class="form_input">
                    <select data-placeholder="Cari Group Asset" style="width:100%;" class="" tabindex="13" name="vgroupasset" id="vgroupasset"></select>
                </div>
            </div>
        </div>
        <div class="clear"></div><br>
        
        <div class="form_grid_12">
            <div class="form_grid_3">
                <label class="field_title">Nilai Perolehan</label>
            </div>
            <div class="form_input form_grid_9">
                <input type="text" class="form-control" name="vnilai" id="vnilai" required>
            </div>
        </div>
        <div class="clear"></div><br>
        <div class="form_grid_12">
            <div class="form_grid_3">
                <label class="field_title">Akumulasi Penyusutan</label>
            </div>
            <div class="form_input form_grid_9">
                <input type="text" class="form-control" name="vpenyusutan" id="vpenyusutan" required>
            </div>
        </div>
        <div class="clear"></div><br>
        <div class="form_grid_12">
            <div class="form_grid_3">
                <label class="field_title">Nilai Buku</label>
            </div>
            <div class="form_input form_grid_9">
                <input type="text" class="form-control" name="vbuku" id="vbuku" required>
            </div>
        </div>
        <div class="clear"></div><br>
        <div class="form_grid_12">
            <div class="form_grid_3">
                <label class="field_title">Metode Penyusutan</label>
            </div>
            <div class="form_input form_grid_9">
                <input type="text" class="form-control" name="vmetode" id="vmetode" required>
            </div>
        </div>
        <div class="clear"></div><br>
        <div class="form_grid_12">
            <div class="form_grid_3">
                <label class="field_title">Tanggal Penyusutan</label>
            </div>
            <div class="form_input form_grid_9">
                <input type="text" class="form-control" name="vtanggal" id="vtanggal" required>
            </div>
        </div>
        <div class="clear"></div><br>
        <div class="form_grid_12">
            <div class="form_grid_3">
                <label class="field_title">Umur</label>
            </div>
            <div class="form_input form_grid_9">
                <input type="text" class="form-control" name="vumur" id="vumur" required>
            </div>
        </div>
        <div class="clear"></div><br>
        <input type="hidden" name="vindex" id="vindex">
        <div class="btn_24_blue">
            <a href="#" onclick="save_tmp_create()" class="simplemodal-close"><span>Simpan</span></a>
        </div>
    </form>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/pembelian_asset.js"></script>

<?php $this->load->view('administrator/footer') ; ?>