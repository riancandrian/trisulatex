<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Pendaftaran Asset</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('Pembelian_asset')?>">Data Pendaftaran Asset</a></li>
                            <li style="text-transform: capitalize;"> Detail Pendaftaran Asset </li>
                        </ul>

                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Tanggal ?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">Nomor</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #ffffff;">Total Nilai Perolehan</td>
                                    <td  style="background-color: #ffffff;"><?php echo convert_currency($data->Total_nilai_perolehan) ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">Total Akumulasi Penyusutan</td>
                                    <td style="background-color: #e5eff0;"><?php echo convert_currency($data->Total_akumulasi_penyusutan) ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Total Nilai Buku</td>
                                    <td style="background-color: #ffffff;"><?php echo convert_currency($data->Total_nilai_buku) ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Status</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $data->Batal ?></td>
                                </tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table><br><br>

                        <div class="widget_content">

                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Asset</th>
                      <th>Group Asset</th>
                      <th>Nilai Perolehan</th>
                      <th>Akumulasi Penyusutan</th>
                      <th>Nilai Buku</th>
                      <th>Metode Penyusutan</th>
                      <th>Tanggal Penyusutan</th>
                      <th>Umur</th>
                      <th>Tarif Penyusutan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($datadetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($datadetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Nama_Asset; ?></td>
                          <td><?php echo $data2->Group_Asset; ?></td>
                          <td><?php echo convert_currency($data2->Nilai_perolehan); ?></td>
                          <td><?php echo convert_currency($data2->Akumulasi_penyusutan); ?></td>
                          <td><?php echo convert_currency($data2->Nilai_buku); ?></td>
                          <td><?php echo $data2->Metode_penyusutan; ?></td>
                          <td><?php echo $data2->Tanggal_penyusutan; ?></td>
                          <td><?php echo $data2->Umur; ?></td>
                          <td><?php echo $data2->Tarif_Penyusutan*100; ?> %</td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>

                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('Pembelian_asset')?>">Kembali</a></span>
                                </div>
                        <!--   <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>Pembelian_asset/print/<?php echo $data->IDFBA ?>">Print Data</a></span>
                        </div> -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>