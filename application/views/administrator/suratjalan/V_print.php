<!DOCTYPE HTML>
<html>

<head>
    <title>PT Trisula Textile Industries Tbk</title>
    <link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen,print">
    <style type="text/css">
    @page {
        size: F4;
        margin: 0;

    }

</style>
</head>
<body>
    <div id="content">
        <div class="grid_container">
            <div class="grid_12">
                <div class="widget_wrap">
                    <div class="grid_12 invoice_num" style="margin-bottom: .5rem">
                        <table border="0" width="100%">
                            <tr><td>PT. PRIMA MODA KREASINDO</td></tr>
                            <tr><td> CIMAHI - BANDUNG</td></tr>
                        </table>
                    </div>
                    
                    <div class="grid_12 invoice_num" style="margin-bottom: .5rem; text-align: center;">
                        <h4 style="margin-top: 0; border-bottom: 1px solid #000; padding-bottom: .5rem; margin-bottom: .5rem">SURAT JALAN</h4>


                    </div>
                    <div class="grid_12 invoice_num" style="margin-bottom: .5rem;">
                        <div class="grid_6 invoice_num" style="margin-bottom: .5rem">
                            <table border="0" width="100%">
                                <tr><td>Customer</td></tr>
                                <tr><td style="height: 105px"><?php echo $print->Nama ?> <br> <?php echo $print->Alamat ?><br><br><?php echo $print->Kota ?><br><?php echo $print->No_Telpon ?> Attn : <?php echo $print->Nama ?></td></tr>
                            </table>
                        </div>
                        <div class="grid_6" style="margin: .125rem .125rem">
                            <table border="0" width="100%">
                                 <tr>
                                    <td>Delivery No : <?php echo $print->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td>Delivery Date : <?php echo $print->Tanggal ?></td>
                                </tr>
                            </table>


                        </div>
                        <!-- <div class="grid_6" style="margin: .125rem .125rem">
                            <table border="1" width="100%">
                                <tr>
                                    <td>No. SJ</td>
                                </tr>
                                <tr>
                                    <td><?php //echo $print->Nomor ?></td>
                                </tr>
                            </table>
                        </div>

                        <div class="grid_6" style="margin: .125rem .125rem">
                            <table border="1" width="100%">
                                <tr>
                                    <td>Dikirim Via</td>
                                </tr>
                                <tr>
                                    <td><?php //echo "-" ?></td>
                                </tr>
                            </table>
                        </div>

                        <div class="grid_6" style="margin: .125rem .125rem">
                            <table border="1" width="100%">
                                <tr>
                                    <td>PO. NO</td>
                                </tr>
                                <tr>
                                    <td><?php //echo "-" ?></td>
                                </tr>
                            </table>
                        </div> -->

                    </div>

                    <div class="widget_content">
                        <div class=" page_content">
                            <span class="clear"></span>
                            <div class="grid_12 invoice_details" style="margin: 0;width: 99%">
                                <div class="invoice_tbl">
                                    <table style="border: none;">
                                        <thead>
                                            <tr class=" gray_sai">
                                                <th>
                                                    Item
                                                </th>
                                                <th>
                                                    Material Description
                                                </th>
                                                <th>
                                                    Quantity
                                                </th>
                                                <th>
                                                    Remark
                                                </th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                        <?php if(!empty($printdetail)){
                                            $i=1;   
                                            foreach ($printdetail as $data2) {
                                                ?>
                                                <tr class="odd gradeA">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $data2->Corak. ', Grade.'.$data2->Grade .', Color.'.$data2->Warna?></td>
                                                    <td><?php echo $data2->Qty_roll; ?> Pcs / <?php echo $data2->Qty_yard; ?> <?php echo $data2->Satuan; ?></td>
                                                    <td><?php echo $data2->Nomor; ?></td>
                        
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                       <!--  <div class="grid_12" style="padding: 0; margin: 2rem 0; width: 100%;">

                            <table border="1" width="100%">
                                <tr >
                                    <td style="text-align: left;">Keterangan</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;"><?php// echo $print->Keterangan; ?></td>
                                </tr>
                            </table>
                        </div> -->
                        <br><br>
                        <div class="grid_2">
                            <h5 class="notes" style="margin-top: .5rem">Approved By, </h5>
                            <br><br><br><br><br>
                            <p style="border-top: 1px solid #000">
                            
                            </p>
                        </div>
                        <div class="grid_2">
                            <h5 class="notes" style="margin-top: .5rem">Received By, </h5>
                            <br><br><br><br><br>
                            <p style="border-top: 1px solid #000">
                            </p>
                        </div>
                    </div>
                    <span class="clear"></span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<span class="clear"></span>
</div>
</body>
</html>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        window.print();
        window.location.href = "<?php echo base_url() ?>SuratJalan/index";
    });
</script>  