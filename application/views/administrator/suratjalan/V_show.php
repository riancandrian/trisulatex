<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Sales Order</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('SuratJalan/index')?>">Data Surat Jalan</a></li>
                            <li style="text-transform: capitalize;"> Detail Sales Order - <b><?php echo $suratjalan->Nomor ?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">
                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $suratjalan->Tanggal ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Nomor SJ</td>
                                    <td style="background-color: #ffffff;"><?php echo $suratjalan->Nomor?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Customer</td>
                                    <td style="background-color: #e5eff0;"><?php echo $suratjalan->Nama ?></td>
                                </tr> 
                                <tr>         
                                    <td  style="background-color: #ffffff;">Tanggal Kirim</td>
                                    <td  style="background-color: #ffffff;"><?php echo $suratjalan->Tanggal_kirim ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Keterangan</td>
                                    <td style="background-color: #e5eff0;"><?php echo $suratjalan->Keterangan?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $suratjalan->Batal?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Total Qty</td>
                                    <td style="background-color: #e5eff0;"><?php echo $suratjalan->Total_qty?></td>
                                </tr> 
                               
                                
                             

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                                 <div class="widget_content">

                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Corak</th>
                      <th>Deskripsi Barang</th>
                      <th>Merk</th>
                      <th>Warna</th>
                    
                      <th>Qty Yard</th>
                      <th>Qty Meter</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($suratjalandetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($suratjalandetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Corak; ?></td>
                          <td><?php echo $data2->Nama_Barang; ?></td>
                          <td><?php echo $data2->Merk; ?></td>
                          <td><?php echo $data2->Warna; ?></td>
                        
                          <td><?php echo $data2->Qty_yard; ?></td>
                          <td><?php echo $data2->Qty_roll; ?></td>
                          
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('SuratJalan/index')?>" name="simpan">Kembali</a></span>
                                </div>
                                <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>SuratJalan/print_data_SuratJalan/<?php echo $suratjalan->IDSJCK ?>">Print Data</a></span>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>