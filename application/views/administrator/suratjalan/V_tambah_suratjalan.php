<?php $this->load->view('administrator/header') ; 


if ($kodesuratjalan->curr_number == null) {
  $number = 1;
} else {
  $number = $kodesuratjalan->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_po = 'SJK'.date('y').$agen.$kodemax;
?>
<style type="text/css" scoped>
  /* Style the tab */
  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
  }
  .tab a {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
    color:black;
  }
  .tab a:hover {
    background-color: #ddd;
  }
  .tab a.active {
    background-color: #ccc;
  }
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }
  .chzn-container {
    width: 103.5% !important;
  }
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Surat Jalan</h3>
 <div class="top_search">`
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>SuratJalan">Data Surat Jalan</a></li>
              <li class="active">Tambah Surat Jalan</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Surat Jalan</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_6">
                    <!-- form 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">No SJ</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $code_po ?>" readonly>
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tanggal SJ</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Tanggal Kirim</label>
                      <div class="form_input">
                        <input id="Tanggal_kirim" type="date" name="Tanggal_kirim" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                   
                  </div>
                  
                  <div class="form_grid_6">

                    <!-- radio 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">No PL</label>
                      <div class="form_input">
                      <select style="width:100%;" class="chzn-select" placeholder="--Pilih Packing List--" name="IDPAC" id="IDPAC" required oninvalid="this.setCustomValidity('Packing List Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <option value="">--Pilih Packing List--</option>
                          <?php foreach($packinglist as $row) {
                            echo "<option value='$row->IDPAC' data-idcustomer='$row->IDCustomer' data-namacustomer='$row->Nama' >$row->Nomor</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Nama Customer</label>
                      <div class="form_input">
                        <input type="text" name="IDCustomer" id="IDCustomer"  data-idcustomer="" class="form-control" placeholder="Silahkan Pilih Packing List Dahulu" readonly>
                      </div>
                    </div>

                    <!-- radio 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="30" rows="10"></textarea>
                        <input type="hidden" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly>
                        <input type="hidden" name="Saldo_qty" id="Saldo_qty" class="form-control" placeholder="Grand Total" readonly>
                      </div>
                    </div>

                    <!-- radio 3 -->
                   
                  </div>

                  <!-- clear content -->
                  <span class="clear"></span>
                </div> 
                <!-- <div class="form_input">
                  <div class="btn_24_blue">
                    <button class="btn_24_blue" type="button" id="tambah_sementara">
                      Tambah Data
                    </button>
                  </div>
                </div> -->
                <hr>
              
              </li>
              <div class="widget_content">
                <table class="display data_tbl" id="tabelfsdfsf">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Corak</th>
                      <th>Deskripsi Barang</th>
                      <th>Merk</th>
                      <th>Warna</th>
                      <th>Grade</th>
                      <!-- <th>Qty Pcs</th> -->
                      <th>Qty Yard</th>
                      <th>Qty Meter</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>
              </div>

              <li>
                <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                    <div class="btn_30_blue">
                     <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                     <!-- <span><a href="<?php //echo site_url('SuratJalan'); ?>"> Batal</a></span> -->
                  </div>
                  <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save()" type="button" value="Simpan Data" title="Simpan Data"></span>
                  </div>
                </div>
              </li>
             
            </ul>

           
          </div>
          <!-- end tab & pane -->

        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/suratjalan.js"></script>
<?php $this->load->view('administrator/footer') ; ?>