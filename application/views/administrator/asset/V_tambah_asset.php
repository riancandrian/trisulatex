<?php $this->load->view('administrator/header') ; 
//   if($id_asset!=""){
// foreach ($id_asset as $value) {
//   $urutan= substr($value->IDAsset, 1);
// }
// $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
// $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
// }else{
//   $urutan_id = 'P000001';
// }
?>
<div class="page_title">
  <?php echo $this->session->flashdata('Pesan');?>
  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Data Asset</h3>

  <div class="top_search">
  </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Assets">Data Asset</a></li>
              <li class="active">Tambah Data Asset</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Asset</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="<?php echo base_url() ?>Assets/simpan" class="form_container left_label">
            <ul>
              <li>
                <div class="form_grid_12 multiline">
                 <!--  <input type="hidden" class="form-control" value="<?php //echo $urutan_id ?>" name="id_asset" required> -->
                 <label class="field_title">Kode Asset</label>
                 <div class="form_input">

                  <input type="text" class="form-control" name="kode_asset" placeholder="Kode Asset" required oninvalid="this.setCustomValidity('Kode Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                  <span class="clear"></span>
                </div>
                <label class="field_title">Nama Asset</label>
                <div class="form_input">
                 <input type="text" class="form-control" name="nama_asset" placeholder="Nama Asset" required oninvalid="this.setCustomValidity('Nama Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                 <span class="clear"></span>
               </div>

               <label class="field_title">Group Asset</label>
               <div class="form_input">
                <div class="grid_12 w-100 alpha">


                  <div class="grid_9 ml">
                    <select data-placeholder="Cari Berdasarkan Group Asset" style=" width:100%;" class="chzn-select" tabindex="13"  name="group_asset" required oninvalid="this.setCustomValidity('Group Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      <option value=""></option>
                      <?php foreach ($group_asset as $row) : ?>
                        <option value="<?php echo $row->IDGroupAsset ?>"><?php echo $row->Group_Asset;?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="form_grid_3 mr">
                    <div class="btn_24_blue flot-right">
                      <span><a href="#open-modal">Tambah Group Asset</a></span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
                                    <label class="field_title">COA Asset</label>
                      <div class="form_input">
                        <select data-placeholder="Pilih COA Asset" name="coa_asset" id="coa_asset" required oninvalid="this.setCustomValidity('COA Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                          <option value=""></option>
                          <?php foreach($coa_asset as $coa_assets)
                          {
                            echo "<option value='$coa_assets->IDCoa'>$coa_assets->Nama_COA</option>";
                          } 
                          ?>
                        </select>
                      </div>

                      <label class="field_title">COA Akumulasi</label>
                      <div class="form_input">
                        <select data-placeholder="Pilih COA Akumulasi" name="coa_akumulasi" id="coa_akumulasi" required oninvalid="this.setCustomValidity('COA Akumulasi Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                          <option value=""></option>
                          <?php foreach($coa_akumulasi as $coa_aks)
                          {
                            echo "<option value='$coa_aks->IDCoa'>$coa_aks->Nama_COA</option>";
                          } 
                          ?>
                        </select>
                      </div>

                      <label class="field_title">COA Beban Depresiasi</label>
                      <div class="form_input">
                        <select data-placeholder="Pilih COA Beban Depresiasi" name="coa_beban" id="coa_beban" required oninvalid="this.setCustomValidity('COA Beban Depresiasi Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                          <option value=""></option>
                          <?php foreach($coa_beban as $coa_bebans)
                          {
                            echo "<option value='$coa_bebans->IDCoa'>$coa_bebans->Nama_COA</option>";
                          } 
                          ?>
                        </select>
                      </div>
                            <!-- <label class="field_title">Status</label>
                            <div class="form_input">
                                <select data-placeholder="Pilih Status" name="status" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                    <option value=""></option>
                                    <option value="aktif">Aktif</option>
                                    <option value="tidak aktif">Tidak Aktif</option>
                                </select>
                              </div> -->
                            </div>
                          </li>
                          <li>
                            <div class="form_grid_12">
                              <div class="form_input">
                                <div class="btn_30_light">
                                 <span> <a href="<?php echo base_url() ?>Assets" name="simpan">Kembali</a></span>
                               </div>
                               <div class="btn_30_blue">
                                <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="open-modal" class="modal-window">
           <div>
             <form method="post" action="<?php echo base_url() ?>groupasset/simpan" enctype="multipart/form-data" class="form_container left_label">
              <a href="#modal-close" title="Close" class="modal-close">X</a>
              <h1>Tambah Group Asset</h1>
              <div class="modal-body">

                <ul>
                  <li>
                    <div class="form_grid_12 multiline">

                      <!-- Kode Barang -->
                      <label class="field_title">Kode Group Asset</label>
                      <div class="form_input">
                        <input type="text" class="form-control" placeholder="Kode Group Barang" name="Kode_Asset" required oninvalid="this.setCustomValidity('Kode Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      </div>

                      <!-- Nama Barang -->
                      <label class="field_title">Group Asset</label>
                      <div class="form_input">
                        <input type="text" class="form-control" placeholder="Nama Group Barang" name="Group_Asset" required oninvalid="this.setCustomValidity('Nama Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      </div>

                      <label class="field_title">Tarif Penyusutan</label>
                      <div class="form_input">
                        <span class="input-group-addon" style="">Rp. </span>
                        <input name="tarif" type="text" id="tanpa-rupiah-debit" class="form-control" style="margin-left: 32.5px;
                        width: 87% !important;" required oninvalid="this.setCustomValidity('Tarif Penyusutan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      </div>
                    </div>
                  </li>
                </ul>

              </div>
              <div class="modal-foot">
                  <!--   <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                      </div> -->
                      <div class="btn_30_blue">
                       <input type="submit" name="simbar" value="Simpan">
                     </div>
                   </div>
                 </form>
               </div>
             </div>
             <?php $this->load->view('administrator/footer') ; ?>