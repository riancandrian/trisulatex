<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
  /*  .table_top{
        display: none !important;
        }*/
    </style>
    <!-- tittle data -->
    <div class="page_title">
       <?php echo $this->session->flashdata('Pesan');?>
       <span class="title_icon"><span class="blocks_images"></span></span>
       <h3>Data Asset</h3>
       <div class="top_search">

       </div>
   </div>
   <!-- ======================= -->

   <!-- body data -->
   <div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Asset </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Assets/pencarian" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                              <div class="form_grid_12 w-100 px">
                                <div class="form_grid_4 mx">
                                    <div class="btn_24_blue">
                                        <a href="<?php echo base_url('Assets/tambah_asset')?>"><span>Tambah Data</span></a>
                                    </div>
                                </div>

                                <div class="form_grid_2">
                                    <select data-placeholder="Cari Berdasarkan" name="jenispencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                        <option value=""></option>
                                        <option value="Group_Asset">Group Asset</option>
                                        <option value="Kode_Asset">Kode Asset</option>
                                        <option value="Nama_Asset">Nama Asset</option>
                                    </select>
                                </div>
                                <div class="form_grid_2">
                                    <select data-placeholder="Cari Berdasarkan" name="statuspencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                        <option value=""></option>
                                        <option value="aktif">Aktif</option>
                                        <option value="tidak aktif">Tidak Aktif</option>
                                    </select>
                                </div>
                                <div class="form_grid_3">
                                    <input name="keyword" type="text">
                                    <!-- <span class=" label_intro">Last Name</span> -->
                                </div>
                                <div class="form_grid_1 flot-right">
                                    <div class="btn_24_blue">
                                        <input type="submit" name="" value="search">
                                    </div>
                                </div>
                                <span class="clear"></span>

                            </div>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="widget_content">
                <table class="display data_tbl">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Asset</th>
                            <th>Nama Asset</th>
                            <th>Group Asset</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(empty($assets)){
                            ?>
                            <?php
                        }else{
                            $i=1;
                            foreach ($assets as $data) {
                                ?>
                                <tr class="odd gradeA">
                                 <td><?php echo $i; ?></td>
                                 <td><?php echo $data->Kode_Asset; ?></td>
                                 <td><?php echo $data->Nama_Asset; ?></td>
                                 <td><?php echo $data->Group_Asset; ?></td>
                                 <td><?php echo $data->Aktif; ?></td>
                                 <td class="text-center ukuran-logo">
                                   <span><a class="action-icons c-edit" href="<?php echo base_url('Assets/edit/'.$data->IDAsset)?>" title="Ubah Data">Edit</a></span>
                                   <span><a class="action-icons c-delete" href="#open-modal<?php echo $data->IDAsset ?>" title="Hapus Data">Delete</a></span>
                               </td>
                           </tr>
                           <?php
                           $i++;
                       }
                   }
                   ?>
                            <!-- <tr class="gradeA">
                                <td>Trident</td>
                                <td>Internet Explorer 4.0</td>
                                <td>Win 95+</td>
                                <td class="center">4</td>
                                <td class="center">X</td>
                            </tr> -->
                        </tbody>
                        <!-- <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                        </tfoot> -->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
if ($assets=="" || $assets==null) {
}else{
    foreach ($assets as $data) {
        ?>
        <div id="open-modal<?php echo $data->IDAsset ?>" class="modal-window">
         <div>
            <a href="#modal-close" title="Close" class="modal-close">X</a>
            <h1>Konfirmasi Hapus</h1>
            <div class="modal-body">Apakah anda yakin akan menghapus data ini ?</div>
            <div class="modal-foot">
                <div class="btn_30_light">
                    <a href="#modal-close" title="Close"><span>Batal</span></a>
                </div>
                <div class="btn_30_blue">
                    <a href="<?php echo base_url('Assets/delete/'.$data->IDAsset)?>"><span>Hapus</span></a>
                </div>
            </div>
        </div>
    </div>
    <?php 
}
} 
?> 
<?php $this->load->view('administrator/footer') ; ?>
