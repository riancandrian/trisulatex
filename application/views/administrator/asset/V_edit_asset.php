<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Data Asset</h3>

  <div class="top_search">
  </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Assets">Data Asset</a></li>
              <li class="active">Ubah Data Asset</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Ubah Data Asset</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="<?php echo base_url() ?>Assets/update" class="form_container left_label">
            <input type="hidden" name="id" value="<?php echo $Assets->IDAsset?>">
            <ul>
              <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title">Kode Asset</label>
                  <div class="form_input">

                    <input type="text" class="form-control" name="kode_asset" value="<?php echo $Assets->Kode_Asset?>" required oninvalid="this.setCustomValidity('Kode Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <span class="clear"></span>
                  </div>
                  <label class="field_title">Nama Asset</label>
                  <div class="form_input">
                    <input type="text" class="form-control" name="nama_asset" value="<?php echo $Assets->Nama_Asset?>" required oninvalid="this.setCustomValidity('Nama Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <span class="clear"></span>
                  </div>
                  <label class="field_title">Group Asset</label>
                  <div class="form_input">
                    <select data-placeholder="Cari Berdasarkan Group Asset" style=" width:100%;" class="chzn-select" tabindex="13"  name="group_asset" required oninvalid="this.setCustomValidity('Group Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      <?php foreach ($group_asset as $row) : ?>
                        <option value="<?php echo $row->IDGroupAsset ?>" <?php echo $row->IDGroupAsset == $Assets->IDGroupAsset ? 'selected' : '' ?>><?php echo $row->Group_Asset;?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <label class="field_title">COA Asset</label>
                  <div class="form_input">
                    <select data-placeholder="Pilih COA Asset" name="coa_asset" id="coa_asset" required oninvalid="this.setCustomValidity('COA Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                      <option value=""></option>
                      <?php
                      foreach ($coa_asset as $coa_assets) {
                      if ($Assets->Coa_Asset==$coa_assets->IDCoa) {
                      ?>
                      <option value="<?php echo $coa_assets->IDCoa ?>" selected><?php echo $coa_assets->Nama_COA; ?></option>
                      <?php
                    } else {
                      ?>
                      <option value="<?php echo $coa_assets->IDCoa ?>"><?php echo $coa_assets->Nama_COA; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>

              <label class="field_title">COA Akumulasi</label>
              <div class="form_input">
                <select data-placeholder="Pilih COA Akumulasi" name="coa_akumulasi" id="coa_akumulasi" required oninvalid="this.setCustomValidity('COA Akumulasi Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                  <option value=""></option>
                  <?php
                      foreach ($coa_akumulasi as $coa_akumulasis) {
                      if ($Assets->Coa_Akumulasi_Asset==$coa_akumulasis->IDCoa) {
                      ?>
                      <option value="<?php echo $coa_akumulasis->IDCoa ?>" selected><?php echo $coa_akumulasis->Nama_COA; ?></option>
                      <?php
                    } else {
                      ?>
                      <option value="<?php echo $coa_akumulasis->IDCoa ?>"><?php echo $coa_akumulasis->Nama_COA; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>

              <label class="field_title">COA Beban Depresiasi</label>
              <div class="form_input">
                <select data-placeholder="Pilih COA Beban Depresiasi" name="coa_beban" id="coa_beban" required oninvalid="this.setCustomValidity('COA Beban Depresiasi Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                  <?php
                      foreach ($coa_beban as $coa_bebans) {
                      if ($Assets->Coa_Beban_Asset==$coa_bebans->IDCoa) {
                      ?>
                      <option value="<?php echo $coa_bebans->IDCoa ?>" selected><?php echo $coa_bebans->Nama_COA; ?></option>
                      <?php
                    } else {
                      ?>
                      <option value="<?php echo $coa_bebans->IDCoa ?>"><?php echo $coa_bebans->Nama_COA; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
                               <!--  <label class="field_title">Status</label>
                                <div class="form_input">
                                    <select data-placeholder="Pilih Status" name="status" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                        <option value=""></option>
                                        <?php //if($Assets->Aktif=="aktif"){ ?>
                                        <option value="aktif" selected>Aktif</option>
                                        <option value="tidak aktif">Tidak Aktif</option>
                                        <?php //}//elseif($Assets->Aktif=="tidak aktif"){ ?>
                                        <option value="aktif">Aktif</option>
                                        <option value="tidak aktif" selected>Tidak Aktif</option>
                                        <?php //}//else{ ?>
                                        <option value="aktif">Aktif</option>
                                        <option value="tidak aktif">Tidak Aktif</option>
                                        <?php //} ?>
                                    </select>
                                  </div> -->
                                </div>
                              </li>
                              <li>
                                <div class="form_grid_12">
                                  <div class="form_input">
                                    <div class="btn_30_light">
                                     <span> <a href="<?php echo base_url() ?>Assets" name="simpan" title=".classname">Kembali</a></span>
                                   </div>
                                   <div class="btn_30_blue">
                                    <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                  </div>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php $this->load->view('administrator/footer') ; ?>