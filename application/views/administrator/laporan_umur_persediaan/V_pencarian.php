<?php $this->load->view('administrator/header') ; ?>
<style type="text/css">
table.display td input{
  border: transparent !important;
  background: transparent !important;
}
</style>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<!-- tittle data -->
<div class="page_title">
  <?php echo $this->session->flashdata('Pesan');?>
  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Laporan Umur Persediaan</h3>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>

<!-- body data -->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li> Laporan Umur Persediaan </li>
            </ul>
          </div>
        </div>
        <div class="widget_content">
          <form action="<?php echo base_url() ?>LapUmurPersediaan/pencarian" method="post" class="form_container left_label">
            <ul>
              <li class="px-1">

                <!-- search -->
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_9 ">

                    <!-- form 1 -->
                    <div class="form_grid_3 ">   
                      <label class="field_title mt-dot2">Periode</label>
                      <div class="form_input">
                        <input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_3 ">   
                      <label class="field_title mt-dot2"> S/D </label>
                      <div class="form_input">
                        <input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
                      </div>
                    </div>

                    <div class="form_grid_3 ">   
                      <select name="jenispencarian" data-placeholder="No Intruksi" style="width: 100%!important" class="chzn-select" tabindex="13">
                        <option value="Corak">Corak</option>
                        <option value="Merk">Merk</option>
                        <option value="Warna">Warna</option>
                      </select>
                    </div>
                    <div class="form_grid_2">   
                      <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
                    </div>
                    <div class="form_grid_1">
                      <div class="btn_24_blue">
                        <input type="submit" name="" value="search">
                      </div>
                    </div>
                  </div>

                  <span class="clear"></span>
                </div>
              </li>
            </ul>
          </form>
        </div>
        <div class="widget_content">
         <table class="display" id="action_tbl">
          <!-- <table class="display data_tbl"> -->
            <thead>
             <tr>
              <th rowspan="3">No</th>
              <th rowspan="3">Corak</th>
              <th rowspan="3">Warna </th>
              <th colspan="3"><center>0-60</center></th>
              <th colspan="3"><center>61-90</center></th>
              <th colspan="3"><center>>120</center></th>
              <th colspan="3"><center>Total Stok</center></th>
            </tr>
            <tr>
              <th colspan="2"><center>Qty</center></th>
              <th rowspan="2"><center>Nilai Rp</center></th>
              <th colspan="2"><center>Qty</center></th>
              <th rowspan="2"><center>Nilai Rp</center></th>
              <th colspan="2"><center>Qty</center></th>
              <th rowspan="2"><center>Nilai Rp</center></th>
              <th colspan="2"><center>Qty</center></th>
              <th rowspan="2"><center>Nilai Rp</center></th>
            </tr>
            <tr>
              <th><center>Yard</center></th>
              <th><center>Meter</center></th>
              <th><center>Yard</center></th>
              <th><center>Meter</center></th>
              <th><center>Yard</center></th>
              <th><center>Meter</center></th>
              <th><center>Yard</center></th>
              <th><center>Meter</center></th>
            </tr>
          </thead>
          <tbody>
            <?php if(empty($umurpersediaan)){ ?>
            <?php }else{
              $i=1;
              $corak="";
              $warna="";
              $jumlah_qty_yard = 0;
              $jumlah_qty_meter = 0;

              foreach ($umurpersediaan as $key => $data) {

                // echo "key2 : ".$data->Qty_yard."<br>";
                if(($key+1)<(count($umurpersediaan))){
                  if($data->IDCorak==$umurpersediaan[$key+1]->IDCorak && $data->IDWarna==$umurpersediaan[$key+1]->IDWarna){ 
                    $corak=$data->IDCorak;
                    $warna=$data->IDWarna;
                    $jumlah_qty_yard += $data->Qty_yard;
                    $jumlah_qty_meter += $data->Qty_meter;
                    $munculin = false;
                  }else{
                    $munculin = true;

                  }
                  }else{ //jika di array berikutnya udah ga ada data maka langsung munculkan datanya
                    $munculin = true;

                  }

                  if($munculin){
                    $jumlah_qty_yard += $data->Qty_yard;
                    $jumlah_qty_meter += $data->Qty_meter;

                    ?>
                    <tr class="odd gradeA">
                      <td class="text-center"><?php echo $i; ?></td>
                      <td><?php echo $data->Corak; ?></td>
                      <td><?php echo $data->Warna; ?></td>

                      <?php
                          foreach($lapumur as $lapumurs){ //untuk get tanggal aja
                            $adadata = false;
                            if($lapumurs->IDCorak==$data->IDCorak && $lapumurs->IDWarna==$data->IDWarna){
                              $adadata = true;
                              date_default_timezone_set('Asia/Jakarta');
                              $tgl1 = date('d-m-Y');
                              $tgl2 = $lapumurs->Tanggal;
                              $selisih = strtotime($tgl1) -  strtotime($tgl2);
                              $hari = $selisih/(60*60*24);
                              // echo $hari ."lllll <br>";

                              if($hari >= 0 && $hari <=60)
                              {
                                ?>
                                <td id="yard1<?php echo $i ?>"><input type="text" id="hyard1<?php echo $i ?>" value="0" disabled></td>
                                <td id="meter1<?php echo $i ?>"><input type="text" id="hmeter1<?php echo $i ?>" value="0" disabled></td>
                                <td id="rupiah1<?php echo $i ?>"><input type="text" id="hrupiah1<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }else{
                                ?>
                                <td id="yard1<?php echo $i ?>"><input type="text" id="hyard1<?php echo $i ?>" value="0" disabled></td>
                                <td id="meter1<?php echo $i ?>"><input type="text" id="hmeter1<?php echo $i ?>" value="0" disabled></td>
                                <td id="rupiah1<?php echo $i ?>"><input type="text" id="hrupiah1<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }
                              ?>
                              <?php
                              if($hari >= 61 && $hari <=90)
                              {
                                ?>
                                <td id="yard2<?php echo $i ?>"><input type="text" id="hyard2<?php echo $i ?>" value="0" disabled></td>
                                <td id="meter2<?php echo $i ?>"><input type="text" id="hmeter2<?php echo $i ?>" value="0" disabled></td>
                                <td id="rupiah2<?php echo $i ?>"><input type="text" id="hrupiah2<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }else{
                                ?>
                                <td id="yard2<?php echo $i ?>"><input type="text" id="hyard2<?php echo $i ?>" value="0" disabled></td>
                                <td id="meter2<?php echo $i ?>"><input type="text" id="hmeter2<?php echo $i ?>" value="0" disabled></td>
                                <td id="rupiah2<?php echo $i ?>"><input type="text" id="hrupiah2<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }
                              ?>
                              <?php
                              if($hari > 120)
                              {
                                ?>
                                <td id="yard3<?php echo $i ?>"><input type="text" id="hyard3<?php echo $i ?>" value="0" disabled></td>
                                <td id="meter3<?php echo $i ?>"><input type="text" id="hmeter3<?php echo $i ?>" value="0" disabled></td>
                                <td id="rupiah3<?php echo $i ?>"><input type="text" id="hrupiah3<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }else{
                                ?>
                                <td id="yard3<?php echo $i ?>"><input type="text" id="hyard3<?php echo $i ?>" value="0" disabled></td>
                                <td id="meter3<?php echo $i ?>"><input type="text" id="hmeter3<?php echo $i ?>" value="0" disabled></td>
                                <td id="rupiah3<?php echo $i ?>"><input type="text" id="hrupiah3<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }
                              ?>
                              <td id="yard4<?php echo $i ?>"><input type="text" id="hyard4<?php echo $i ?>" value="0" disabled></td>
                              <td id="yard5<?php echo $i ?>"><input type="text" id="hyard5<?php echo $i ?>" value="0" disabled></td>
                              <td id="yard6<?php echo $i ?>"><input type="text" id="hyard6<?php echo $i ?>" value="0" disabled></td>
                              <?php
                            }
                            if($adadata){
                              break;
                            }
                          }

                          foreach($lapumur as $key=>$lapumurs){
                            if($lapumurs->IDCorak==$data->IDCorak && $lapumurs->IDWarna==$data->IDWarna){
                              date_default_timezone_set('Asia/Jakarta');
                              $tgl1 = date('d-m-Y');
                              $tgl2 = $lapumurs->Tanggal;
                              $selisih = strtotime($tgl1) -  strtotime($tgl2);
                              $hari = $selisih/(60*60*24);
                              // echo $hari . "<br>";
                              ?>
                              <script type="text/javascript">

                                jsfunction2(<?php echo $key ?>, <?php echo $hari ?>, <?php echo $lapumurs->Qty_yard ?>, <?php echo $lapumurs->Qty_meter ?>, <?php echo $lapumurs->Harga ?>,<?php echo $i ?>);
                                function jsfunction2(key, hari, yard, meter,harga, inc)
                                {
                                  if(hari>=0 && hari<=60){

                                    $('#hyard1'+inc).val(parseInt($('#hyard1'+inc).val()) + parseInt(yard));
                                    $('#hmeter1'+inc).val(parseInt($('#hmeter1'+inc).val()) + parseInt(meter));
                                    $('#hrupiah1'+inc).val(parseInt($('#hrupiah1'+inc).val()) + parseInt(harga));

                                  }

                                  if(hari>=61 && hari<=90){

                                    $('#hyard2'+inc).val(parseInt($('#hyard2'+inc).val()) + parseInt(yard));
                                    $('#hmeter2'+inc).val(parseInt($('#hmeter2'+inc).val()) + parseInt(meter));
                                    $('#hrupiah2'+inc).val(parseInt($('#hrupiah2'+inc).val()) + parseInt(harga));

                                  }

                                  if(hari>120){

                                    $('#hyard3'+inc).val(parseInt($('#hyard3'+inc).val()) + parseInt(yard));
                                    $('#hmeter3'+inc).val(parseInt($('#hmeter3'+inc).val()) + parseInt(meter));
                                    $('#hrupiah3'+inc).val(parseInt($('#hrupiah3'+inc).val()) + parseInt(harga));

                                  }

                                  $('#hyard4'+inc).val(parseInt($('#hyard4'+inc).val()) + parseInt(yard));
                                  $('#hyard5'+inc).val(parseInt($('#hyard5'+inc).val()) + parseInt(meter));
                                  $('#hyard6'+inc).val(parseInt($('#hyard6'+inc).val()) + parseInt(harga));
                                }
                              </script>
                              <?php
                            }
                          }
                          ?>

                        </tr>
                        <?php
                        $i++;
                      }

                    }
                  }
                  ?>

                </tbody>
                <tfoot>

                </tfoot>
                <!-- </table> -->
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('administrator/footer') ; ?>