<?php $this->load->view('administrator/header'); ?>
    <style type="text/css">
        .chzn-container {
            width: 102.5% !important;
        }
    </style>
    <div class="page_title">
        <?php echo $this->session->flashdata('Pesan');?>
        <span class="title_icon"><span class="blocks_images"></span></span>
        <h3>Data Invoice Penjualan</h3>

        <div class="top_search">
        </div>
    </div>
    <!-- ======================= -->

    <div id="content">
        <div class="grid_container">
            <div class="grid_12">
                <div class="widget_wrap">
                    <div class="breadCrumbHolder module">
                        <div id="breadCrumb0" class="breadCrumb module white_lin">
                            <ul>
                                <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                                <li><a href="<?php echo base_url() ?>InvoicePenjualanNonKain">Data Invoice Penjualan Non Kain</a></li>
                                <li class="active">Ubah Invoice Penjualan Non Kain</li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget_top">
                        <div class="grid_12">
                            <span class="h_icon blocks_images"></span>
                            <h6>Ubah Data Invoice Penjualan Non Kain</h6>
                        </div>
                    </div>
                    <div class="widget_content">
                        <form method="post" action="" class="form_container left_label">
                            <ul>
                                <li class="body-search-data">
                                    <!-- form header -->
                                    <div class="form_grid_12 w-100 alpha">
                                        <input type="hidden" name="IDFJS" id="IDFJS"  value="<?php echo $invoice->IDFJS; ?>">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb mt-dot2">No Invoice</label>
                                            <div class="form_input">
                                                <input type="text" name="Nomor_invoice" id="Nomor_invoice" class="form-control" value="<?php echo $invoice->Nomor; ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb">Tgl Jatuh Tempo</label>
                                            <div class="form_input">
                                                <input id="Tanggal_jatuh_tempo" type="date" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" value="<?php echo $invoice->Tanggal_jatuh_tempo; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb mt-dot2">Tgl Invoice</label>
                                            <div class="form_input">
                                                <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?php echo $invoice->Tanggal ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb mt-dot2">Nama di Faktur</label>
                                            <div class="form_input">
                                                <input type="text" name="nama_faktur" id="nama_faktur" class="form-control" value="<?php echo $invoice->Nama_di_faktur ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_grid_12 w-100 alpha">
                                        <!-- form 7 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb">Nama Customer</label>
                                            <div class="form_input">
                                                <select data-placeholder="Cari Customer" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDCustomer"  id="IDCustomer" required oninvalid="this.setCustomValidity('Nama Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($customers as $data) {
                                                        ?>
                                                        <option value="<?php echo $data->IDCustomer ?>" <?php echo $data->IDCustomer == $invoice->IDCustomer ? 'selected' : '' ?>><?php echo $data->Nama; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- form 11 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb mt-dot2">Status PPN</label>
                                            <div class="form_input">
                                                <select style=" width:100%;" class="chzn-select" tabindex="13"  name="Status_ppn" id="Status_ppn">
                                                    <option value="Include" <?php echo $invoice->Status_ppn == 'Include' ? 'selected' : '' ?>>Include</option>
                                                    <option value="Exclude" <?php echo $invoice->Status_ppn == 'Exclude' ? 'selected' : '' ?>>Exclude</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb">No SJ</label>
                                            <div class="form_input">
                                                    <div class="list_filter" style="margin-top: -5px;">
                                                        <input type="text" class="" name="Nomor" id="Nomor" value="<?php echo $invoice->Nomor_sjcs; ?>" readonly>
                                                        <!-- <button type="button" id="add-to-cart" class="list_refresh"><span class="filter_btn" style="top: 1px; left: 3px;"></span></button> -->
                                                 
                                                </div>
                                            </div>
                                        </div>
                                        <span class="clear"></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="form_grid_12">
                                        <div class="form_input ml text-center w-100">
                                            <!-- <button class="btn_24_blue" type="button" id="add-to-cart">
                                              Tambah Data
                                            </button> -->
                                        </div>
                                    </div>
                                </li>
                                <input id="Total_qty" type="hidden" name="Total_qty">
                            </ul>

                            <!-- content body tab & pane -->
                            <div class="grid_12 mx w-100">
                                <div class="widget_wrap tabby mb-4">
                                    <div class="widget_top">
                                        <!-- button tab & pane -->
                                        <div id="widget_tab">
                                            <ul>
                                                <li class="px py"><a href="#tab1" class="active_tab">List Barang</a></li>
                                               <!--  <li class="px py"><a href="#tab2">Total Pembayaran</a></li> -->
                                            </ul>
                                        </div>
                                        <!-- end button tab & pane -->
                                    </div>

                                    <!-- tab & pane body -->
                                    <div class="widget_content">
                                        <!-- tab 1 -->
                                        <div id="tab1">
                                            <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                                <div class="grid_12 w-100">
                                                    <span class="h_icon blocks_images"></span>
                                                    <h6>List Barang</h6>
                                                </div>
                                            </div>
                                            <div>
                                                <table class="display data_tbl">
                                                    <thead>
                                                    <tr>
                                                        <th class="center" style="width: 40px">No</th>
                                                        <th>Nama Barang</th>
                                                        <th>Qty</th>
                                                        <th>Satuan</th>
                                                        <th>Harga Satuan</th>
                                                        <th>Sub Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tb_preview_penerimaan">

                                                    </tbody>
                                                </table>
                                                <ul>
                                                    <li>
                                                        <div class="form_grid_12 w-100 alpha">
                                                            <div class="form_grid_8">
                                                                <label class="field_title mb mt-dot2">Keterangan</label>
                                                                <div class="form_input">
                                                                    <textarea id="Keterangan" name="Keterangan" class="form-control" rows="9" style="resize: none;"><?php echo $invoice->Keterangan ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form_grid_4">
                                                                <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mb mt-dot2">DPP</label>
                                                                    <div class="form_input input-not-focus">
                                                                        <input type="text" id="DPP" name="DPP" class="form-control" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mb mt-dot2">PPN</label>
                                                                    <div class="form_input input-not-focus">
                                                                        <input type="text" id="PPN" name="PPN" class="form-control" readonly>
                                                                    </div>
                                                                </div>
                                                              
                                                                        <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" value="0" readonly>
                                                                <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mb">Total Invoice</label>
                                                                    <div class="form_input input-not-focus">
                                                                        <input type="text" id="total_invoice" name="total_invoice" class="form-control" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- clear content -->
                                                            <div class="clear"></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end tab 1 -->

                                        <!-- tab 2 -->
                                     
                                    </div>
                                    <!-- end tab 2 -->

                                </div>
                                <!-- end tab & pane body -->
                            </div>
                    </div>
                    <!-- end content body tab & pane -->
                    </form>
                    <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2">
                            <div class="btn_30_blue">
                                <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                            </div>
                            <div class="btn_30_blue">
                                <span><a class="btn_24_blue" id="simpan" onclick="saveEditInvoice()" style="cursor: pointer;">Simpan</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>libjs/invoice_penjualan_seragam.js"></script>
    <script type="text/javascript">
        var base_url = window.location.pathname.split('/');
        var jenis;

        jenis_bayar = "<?php echo $grand->Pembayaran ?>";
        if (jenis_bayar) {
            jenis = jenis_bayar;
        } else {    
            $("#namacoa").prop("disabled", true);
        }

        $("#Jatuh_tempo").prop("disabled", true);
        $("#Discount").prop("disabled", true);


        function discount_inv(){
            if (document.getElementById('disc').checked)
            {
                $("#Discount").prop("disabled", false);
            } else {
                $("#Discount").prop("disabled", true);
            }
        }
        function jenis_pembayaran(){
            if (document.getElementById('jenis2').checked)
            {
                $("#namacoa").prop("disabled", false);
                jenis = 'Transfer';
            } else if (document.getElementById('jenis3').checked){
                $("#namacoa").prop("disabled", false);
                jenis = 'Giro';
            } else if (document.getElementById('jenis').checked){
                $("#namacoa").prop("disabled", true);
                jenis = 'Cash';
            }
        }
        function hitung() {
            var a = new Date($("#Tanggal").val());
            var b = $("#Jatuh_tempo").val();

            var today = new Date(a.getTime()+(b*24*60*60*1000));
            var dd = today.getDate();
            var mm = today.getMonth()+1;

            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd;
            }
            if(mm<10){
                mm='0'+mm;
            }
            var today = yyyy+'-'+mm+'-'+dd;
            //$("#Tanggal_jatuh_tempo").val(today);
        }
    </script>
    <script type="text/javascript">
        function Penerimaan(){
            this.Corak;
            this.Warna;
            this.Merk;
            this.Saldo_qty;
            this.Total_qty;
            this.Grade;
            this.Satuan;
            this.Tanggal;
            this.ID_customer;
            this.NoSJ;
            this.Keterangan;
            this.Total_yard;
            this.Total_meter;
            this.Qty;
            this.Saldo_qty;
            this.Qty_roll;
            this.Saldo_roll;
            this.IDMata_uang;
            this.IDBarang;
            this.IDWarna;
            this.IDCorak;
            this.IDMerk;
            this.IDSJCS;
            this.Kurs;
            this.IDSatuan;
            this.Nama_barang;
            this.Harga_satuan;
            this.Subtotal;
        }

        var PM = new Array();
        index = 0;
        totaldpp=0;

        var PB = document.getElementById("Nomor");
        $('#add-to-cart').on('click', function(){
            //event.preventDefault();

            Nomor = $('#Nomor').val();
            //alert($('#no_sjc').val());

            $.post("<?php echo base_url("InvoicePenjualanNonKain/check_surat_jalan")?>", {'Nomor' : Nomor})
                .done(function(response) {

                    var json = JSON.parse(response);
                    //console.log(json);
                    if (!json.error) {
                        $.each(json.data, function (key, value) {

                            if(PM == null)
                                PM = new Array();
                            totalqtyyard = 0;
                            totalqtymeter = 0;
                            totalroll = 0;

                            var M = new Penerimaan();

                            M.Corak = value.Corak;
                            M.Warna = value.Warna;
                            M.Merk = value.Merk;
                            M.Saldo_qty = value.Saldo_qty;
                            M.Total_qty = value.Total_qty;
                            M.Qty = value.Qty;
                            M.Satuan = value.Satuan;
                            M.Qty_roll = value.Qty_roll;
                            M.Saldo_roll = value.Saldo_roll;
                            M.ID_customer = $('#IDCustomer').val();
                            M.IDMata_uang = value.IDMataUang;
                            M.IDBarang = value.IDBarang;
                            M.IDWarna = value.IDWarna;
                            M.IDCorak = value.IDCorak;
                            M.IDMerk = value.IDMerk;
                            M.IDSJCS = value.IDSJCS;
                            M.Kurs = value.Kurs;
                            M.IDSatuan = value.IDSatuan;
                            M.Nama_barang = value.Nama_Barang;

                            PM[index] = M;
                            index++;

                            $("#tb_preview_penerimaan").html("");
                            for(i=0; i < index; i++){
                                value = "<tr>"+
                                    "<td class='text-center'>"+(i+1)+"</td>"+
                                    "<td>"+PM[i].Nama_barang+"</td>"+
                                    "<td>"+PM[i].Qty+"</td>"+
                                    "<td>"+PM[i].Satuan+"</td>"+
                                    "<td>"+
                                        "<input type=text name=harga_satuan id=harga_satuan"+i+" onkeyup=hitung_total("+i+")>" +
                                    "</td>"+
                                    "<td>"+"<input type=text name=harga_total id=harga_total"+i+" value=0 readonly>"+"</td>"+
                                    "<td class='text-center'>" +
                                        "<div class='hidden-sm hidden-xs action-buttons'>"+
                                            "<span><a href='#' class='action-icons c-delete'  id='bootbox-confirm' onclick='deleteItem("+i+")'>Delete"+"</a></span>"+
                                        "</div>"+
                                    "</td>"+
                                    "</tr>";
                                $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);
                                totalqtyyard += parseInt(PM[i].Qty);
                                totalroll += parseInt(PM[i].Qty_roll);
                            }
                            $("#Total_qty").val(totalqtyyard);
                            $("#Total_roll").val(totalroll);
                        });
                    } else {
                        alert('Tidak ada data dengan nomor Penerimaan ' + Nomor);
                    }
                });

        });

        function hitung_total(i) {
            if (PM[i].Qty) {
                var a = PM[i].Qty;
            } else {
                var a = PM[i].Qty_yard;
            }

            var b = $("#harga_satuan"+i).val();
            var c=a*b;

            PM[i].Harga_satuan = parseInt(b);
            PM[i].Subtotal = c;

            $("#harga_total"+i).val(c);
            totaldpp=0;
            for(l=0; l < index; l++){
                totaldpp += parseInt($("#harga_total"+l).val());
            }
            //disc=  $("#Discount").val();
            disc = 0;
            if($("#Status_ppn").val() == "Include"){
                ppn= totaldpp*0.1;
                $("#DPP").val(totaldpp-ppn);
                $("#PPN").val(ppn);
                $("#Discount_total").val(disc);
                total= totaldpp-ppn;
                $("#total_invoice").val(total-disc+ppn);
                $("#total_invoice_pembayaran").val(total-disc+ppn);
            }else{
                ppn= totaldpp*0.1;
                $("#DPP").val(totaldpp);
                $("#PPN").val(ppn);
                $("#Discount_total").val(disc);
                $("#total_invoice").val(totaldpp-disc+ppn);
                $("#total_invoice_pembayaran").val(totaldpp-disc+ppn);
            }

        }
        function fieldInvoice(){
            var data1 = {
                "IDFJS" : $("#IDFJS").val(),
                "Tanggal" :$("#Tanggal").val(),
                "Nomor":$("#Nomor_invoice").val(),
                //"Nomor":$("#Nomor").val(),
                "IDCustomer":$("#IDCustomer").val(),
                "Nama_di_faktur":$("#nama_faktur").val(),
                "IDSJCS": PM[0].IDSJCS,
                "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
                "IDMataUang":PM[0].IDMata_uang,
                "Kurs":PM[0].Kurs,
                "Total_yard":$("#Total_qty").val(),
                "Total_pieces":$("#Total_roll").val(),
                "Discount":0,
                "Status_ppn":$("#Status_ppn").val(),
                "Keterangan":$("#Keterangan").val()
            }
            return data1;
        }
        function fieldgrandtotal(){
            var data3 = {
                //"id_invoice_grand_total" : $("#id_invoice_grand_total").val(),
                //"id_pembayaran" : $("#id_pembayaran").val(),
                "DPP" :$("#DPP").val(),
                "Discount" :0,
                "PPN" :$("#PPN").val(),
                "total_invoice_pembayaran" :$("#total_invoice").val(),
                "nominal" :$("#nominal").val(),
                "tgl_giro" :$("#tgl_giro").val(),
                "namacoa" : $("#namacoa").val()
            }
            return data3;
        }

        function harga_satuan_total(i)
        {
            // var data6 = new Array();
            for(i=0; i < index; i++){
                PM[i].harga_satuan = $("#harga_satuan"+i).val();
                PM[i].harga_total = $("#harga_total"+i).val();
            }
            // return data6;
        }

        function saveEditInvoice(){
            if ($('#Tanggal_jatuh_tempo').val() != '' && $("#IDCustomer").val() != '' && $("#nama_faktur").val() != '') {
                var data1 = fieldInvoice();
                var data2 = fieldgrandtotal();
                var idfjs = '_';
                console.log(data1);
                console.log(data2);

                $.ajax({
                    url: "../ubah_invoice_penjualan",
                    type: "POST",
                    data: {
                        "data1" : data1,
                        "data2" : data2
                    },
                    dataType: 'json',
                    success: function (msg, status) {
                        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');

                        document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
                        document.getElementById("simpan").setAttribute("onclick", "return false;");
                        document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
                        document.getElementById("printdata").setAttribute("onclick", "return true;");
                        document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/InvoicePenjualanNonKain/print_/'+data1['Nomor'] + '/' + idfjs);
                    },
                    error: function(msg, status){
                        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
                    }
                });

            } else {
                $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
            }
        }

        $(document).ready(function(){
            renderJSONE($.parseJSON('<?php echo json_encode($detail) ?>'));
        });
    </script>
<?php $this->load->view('administrator/footer') ; ?>