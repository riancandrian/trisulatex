<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Invoice Penjualan Non Kain</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('InvoicePenjualanNonKain')?>">Data Invoice Penjualan Non Kain</a></li>
                            <li style="text-transform: capitalize;"> Detail Invoice Penjualan Non Kain</li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">
                            </thead>
                            <tbody>
                            <tr>
                                <td style="background-color: #ffffff;">Tanggal</td>
                                <td style="background-color: #ffffff;"><?php echo $data->Tanggal ?></td>
                            </tr>
                            <tr>
                                <td width="35%" style="background-color: #e5eff0;">No Invoice</td>
                                <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
                            </tr>
                            <tr>
                                <td  style="background-color: #ffffff;">Nama Customer</td>
                                <td  style="background-color: #ffffff;"><?php echo $data->Nama ?></td>
                            </tr>
                            <tr>
                                <td  style="background-color: #e5eff0;">Nama di Faktur</td>
                                <td  style="background-color: #e5eff0;"><?php echo $data->Nama_di_faktur ?></td>
                            </tr>
                            <tr>
                                <td  style="background-color: #ffffff;">Top</td>
                                <td  style="background-color: #ffffff;"><?php echo $data->TOP ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: #e5eff0;">Tanggal Jatuh Tempo</td>
                                <td style="background-color: #e5eff0;"><?php echo $data->Tanggal_jatuh_tempo ?></td>
                            </tr>
                            <tr>
                                <td  style="background-color: #e5eff0;">Diskon</td>
                                <td  style="background-color: #e5eff0;"><?php echo $data->Discount ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: #ffffff;">Status PPN</td>
                                <td style="background-color: #ffffff;"><?php echo $data->Status_ppn ?></td>
                            </tr>
                             <tr>
                                <td  style="background-color: #e5eff0;">Keterangan</td>
                                <td  style="background-color: #e5eff0;"><?php echo $data->Keterangan ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: #ffffff;">Status</td>
                                <td style="background-color: #ffffff;"><?php echo $data->Batal ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: #ffffff;">Total Yard</td>
                                <td style="background-color: #ffffff;"><?php echo $data->Saldo_yard ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: #e5eff0;">Total Pieces</td>
                                <td style="background-color: #e5eff0;"><?php echo $data->Saldo_pieces ?></td>
                            </tr>

                            </tbody>
                            <tfoot></tfoot>
                        </table>

                        <br><br>
                        <div class="widget_content">
                            <table class="display data_tbl">
                                <thead>
                                <tr>
                                    <th class="center" style="width: 40px">No</th>
                                    <th>Nama Barang</th>
                                    <th class="hidden-xs">Corak</th>
                                    <th>Warna</th>
                                    <th>Qty Yard</th>
                                    <th>Satuan</th>
                                    <th>Harga Satuan</th>
                                    <th style="width: 50px">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(empty($invdetail)){
                                    ?>
                                    <?php
                                }else{
                                    $i=1;
                                    foreach ($invdetail as $data2) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td class="text-center"><?php echo $i; ?></td>
                                            <td><?php echo $data2->Nama_Barang; ?></td>
                                            <td><?php echo $data2->Corak; ?></td>
                                            <td><?php echo $data2->Warna; ?></td>
                                            <td><?php echo convert_currency($data2->Saldo_yard); ?></td>
                                            <td><?php echo $data2->Satuan; ?></td>
                                            <td><?php echo convert_currency($data2->Harga); ?></td>
                                            <td><?php echo convert_currency($data2->Sub_total); ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('InvoicePenjualanNonKain')?>">Kembali</a></span>
                                </div>
                                <div class="btn_30_blue">
                                    <span><a href="<?php echo base_url() ?>InvoicePenjualanNonKain/print_/_/<?php echo $data->IDFJS ?>">Print Data</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>