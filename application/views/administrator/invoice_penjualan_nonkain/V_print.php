<!DOCTYPE HTML>
<html>

<head>
    <title>PT Trisula Textile Industries Tbk</title>
    <link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen,print">
    <style type="text/css">
        @page {
            size: F4;
            margin: 0;
        }

    </style>
</head>
<body>
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="grid_6 invoice_num" style="margin-bottom: .5rem">
                    <span>ITC Baranangsiang Blok F12-15<br>Bandung 40112<br>022-4222065 / 4222067</span>
                </div>
                <div class="grid_6 invoice_num" style="margin-bottom: .5rem">
                    <h1 style="float: right">I N V O I C E</h1>
                </div>
                <div class="widget_content">
                    <div class=" page_content">
                        <div class="invoice_container" style="margin-top: 0">
                            <div class="invoice_action_bar" style="position: relative;">
                                <div class="grid_12">
                                    <div class="grid_6 invoice_num" style="margin-bottom: .5rem">
                                        <table border="1">
                                            <tr><td>Customer</td></tr>
                                            <tr><td><?php echo $print->Nama ?> <br> <?php echo $print->Alamat ?><br><br><?php echo $print->Kota ?><br><?php echo $print->No_Telpon ?> Attn : <?php echo $print->Nama ?></td></tr>
                                        </table>
                                    </div>
                                    <div class="grid_5 invoice_num" style="margin-bottom: .5rem">
                                        <div class="grid_3">
                                            <div class="relative" style="opacity: 0"> tes</div>
                                        </div>
                                        <div class="grid_6" style="margin-bottom: .5rem">
                                            <table border="1">
                                                <tr>
                                                    <td>Invoice Date</td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $print->Tanggal ?></td>
                                                </tr>
                                            </table>


                                        </div>
                                        <div class="grid_3" style="margin-bottom: .5rem">
                                            <table border="1">
                                                <tr>
                                                    <td>Invoice No</td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $print->Nomor ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="grid_3" style="margin-bottom: .5rem">
                                            <div class="relative" style="opacity: 0"> tes</div>
                                        </div>
                                        <div class="grid_6" style="margin-bottom: .5rem">
                                            <table border="1">
                                                <tr>
                                                    <td>Form No</td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo "-" ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="clear"></span>

                            <div class="grid_12 invoice_to" style="padding: 0; margin:0">

                                <div class="grid_2">
                                    <table border="1" style="width: 100%">
                                        <tr>
                                            <td>Terms</td>
                                        </tr>
                                        <tr>
                                            <td><?php echo "-" ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="grid_2">
                                    <table border="1" style="width: 100%">
                                        <tr>
                                            <td>Amount</td>
                                        </tr>
                                        <tr>
                                            <td><?php echo "-" ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="grid_2">
                                    <table border="1" style="width: 100%">
                                        <tr>
                                            <td>FOB</td>
                                        </tr>
                                        <tr>
                                            <td><?php echo "-" ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="grid_2">
                                    <table border="1" style="width: 100%">
                                        <tr>
                                            <td>Ship Via</td>
                                        </tr>
                                        <tr>
                                            <td><?php echo "-" ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="grid_2">
                                    <table border="1" style="width: 100%">
                                        <tr>
                                            <td>Ship Date</td>
                                        </tr>
                                        <tr>
                                            <td><?php echo "-" ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="grid_2">
                                    <table border="1" style="width: 100%">
                                        <tr>
                                            <td>Inclusive Tax</td>
                                        </tr>
                                        <tr>
                                            <td><?php echo "-" ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <span class="clear"></span>
                            <div class="grid_12 invoice_details">
                                <div class="invoice_tbl">
                                    <table>
                                        <thead>
                                        <tr class=" gray_sai">
                                            <th>
                                                Item
                                            </th>
                                            <th>
                                                Uraian
                                            </th>
                                            <th>
                                                Grade
                                            </th>
                                            <th>
                                                Qty
                                            </th>
                                            <th>
                                                Satuan
                                            </th>
                                            <th>
                                                Harga / Unit
                                            </th>
                                            <th>
                                               Jumlah
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(!empty($printdetail)){
                                            foreach ($printdetail as $data2) {
                                                ?>
                                                <tr class="odd gradeA">
                                                    <td><?php echo $print->Nomor; ?></td>
                                                    <td><?php echo $print->Nomor.' '. $data2->Corak.' '.$data2->Warna. ' '.$data2->Satuan; ?></td>
                                                    <td>-</td>
                                                    <td><?php echo $data2->Qty_yard; ?></td>
                                                    <td><?php echo $data2->Satuan; ?></td>
                                                    <td><?php echo $data2->Harga; ?></td>
                                                    <td><?php echo $data2->Sub_total; ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="grid_12" style="padding: 0; margin: .5rem 0">
                                    <div class="grid_2" style="padding: 0; margin: 0">
                                        <table border="1" >
                                            <tr >
                                                <td style="text-align: left;">Keterangan</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;"><?php echo $print->Keterangan ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="grid_2">
                                    <h5 class="notes" style="margin-top: .5rem">Menyetujui, </h5>
                                    <br><br>
                                    <p style="border-top: 1px solid #000">
                                        Date :
                                    </p>
                                </div>
                                <div class="grid_2">
                                    <h5 class="notes" style="margin-top: .5rem">Dibuat Oleh, </h5>
                                    <br><br>
                                    <p style="border-top: 1px solid #000">
                                        Date :
                                    </p>
                                </div>
                            </div>
                            <span class="clear"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</body>
</html>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        window.print();
        //console.log('masuk kesini');
        window.location.href = "<?php echo base_url() ?>InvoicePenjualanNonKain/index";
    });
</script>