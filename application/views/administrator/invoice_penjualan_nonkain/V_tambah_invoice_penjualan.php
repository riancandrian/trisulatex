<?php $this->load->view('administrator/header') ;

if ($kodenoinv->curr_number == null) {
    $number = 1;
} else {
    $number = $kodenoinv->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_no_inv = 'INVJN'.date('y').str_replace(' ', '',$agen).$kodemax;
?>
<style type="text/css">
.chzn-container {
    width: 102.5% !important;
}
</style>
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Invoice Penjualan</h3>

    <div class="top_search">
    </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>InvoicePenjualanNonKain">Data Invoice Penjualan Non Kain</a></li>
                            <li class="active">Tambah Invoice Penjualan Non Kain</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Invoice Penjualan Non Kain</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="" class="form_container left_label">
                        <ul>
                            <li class="body-search-data">
                                <!-- form header -->
                                <div class="form_grid_12 w-100 alpha">
                                    <!-- form 1 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb mt-dot2">No Invoice</label>
                                        <div class="form_input">
                                            <input type="text" name="Nomor_invoice" id="Nomor_invoice" class="form-control" placeholder="No Asset" value="<?php echo $code_no_inv; ?>">
                                        </div>
                                    </div>
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb">Tgl Jatuh Tempo</label>
                                        <div class="form_input">
                                            <input id="Tanggal_jatuh_tempo" type="date" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" placeholder="Tanggal Jatuh tempo">
                                        </div>
                                    </div>
                                </div>
                                <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb mt-dot2">Tgl Invoice</label>
                                        <div class="form_input">
                                            <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?php echo date('Y-m-d'); ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        </div>
                                    </div>
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb mt-dot2">Nama di Faktur</label>
                                        <div class="form_input">
                                            <input type="text" name="nama_faktur" id="nama_faktur" class="form-control" placeholder="Nama Faktur">
                                        </div>
                                    </div>
                                </div>
                                <div class="form_grid_12 w-100 alpha">
                                    <!-- form 11 -->
                                    <div class="form_grid_6 mb-1">
                    <label class="field_title mb mt-dot2">Status PPN</label>
                    <div class="form_input">
                      <select style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" class="form-control" name="Status_ppn" id="Status_ppn" onchange="ubah_status_ppn()">
                        <option value="Include">Include</option>
                        <option value="Exclude" selected>Exclude</option>
                        <option value="Nonppn">Non PPN</option>
                      </select>
                    </div>
                  </div>

                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb">Nama Customer</label>
                                        <div class="form_input">
                                            <!-- <select data-placeholder="Cari Customer" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDCustomer"  id="IDCustomer" required oninvalid="this.setCustomValidity('Nama Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                <option value=""></option>
                                                <?php
                                               // foreach ($customers as $data) {
                                                    ?>
                                                    <option value="<?php //echo $data->IDCustomer ?>"><?php// echo $data->Nama; ?></option>
                                                    <?php
                                              //  }
                                                ?>
                                            </select> -->
                                            <input type="hidden" name="IDCustomer" id="IDCustomer" readonly>
                                            <input type="text" name="NamaCustomer" id="NamaCustomer" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb">No SJ</label>
                                        <div class="form_input">
                                            <div class="list_left">
                                                <div class="list_filter" style="margin-top: -5px;">
                                                    <!-- <input style="width: 63.5% !important" type="text" class="" name="Nomor" id="Nomor" onkeyup="cek_customer()">
                                                    <button type="button" id="add-to-cart" class="list_refresh"><span class="filter_btn" style="top: 1px; left: 3px;"></span></button> -->
                                                    <select name="Nomor" id="Nomor" class="chzn-select" onchange ="cek_customer()" style="width: 89% !important" class="form-control">
                                                    <option value="">Pilih No SJ</option>
                                                    <?php
                                                    foreach($sj as $sjs)
                                                    { 
                                                      ?>
                                                      <option value="<?php echo $sjs->Nomor ?>"><?php echo $sjs->Nomor ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                  </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input ml text-center w-100">
                                            <!-- <button class="btn_24_blue" type="button" id="add-to-cart">
                                              Tambah Data
                                          </button> -->
                                      </div>
                                  </div>
                              </li>
                              <input id="Total_qty" type="hidden" name="Total_qty">
                              <input id="Total_roll" type="hidden" name="Total_roll">
                          </ul>

                          <!-- content body tab & pane -->
                          <div class="grid_12 mx w-100">
                            <div class="widget_wrap tabby mb-4">
                                <div class="widget_top">
                                    <!-- button tab & pane -->
                                    <div id="widget_tab">
                                        <ul>
                                            <li class="px py"><a href="#tab1" class="active_tab">List Barang</a></li>
                                            <!--  <li class="px py"><a href="#tab2">Total Pembayaran</a></li> -->
                                        </ul>
                                    </div>
                                    <!-- end button tab & pane -->
                                </div>

                                <!-- tab & pane body -->
                                <div class="widget_content">
                                    <!-- tab 1 -->
                                    <div id="tab1">
                                        <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                            <div class="grid_12 w-100">
                                                <span class="h_icon blocks_images"></span>
                                                <h6>List Barang</h6>
                                            </div>
                                        </div>
                                        <div>
                                            <table class="display data_tbl" id="tabelfsdfsf">
                                                <thead>
                                                    <tr>
                                                        <th class="center" style="width: 40px">No</th>
                                                        <th>Nama Barang</th>
                                                        <th>Qty</th>
                                                        <th>Satuan</th>
                                                        <th>Harga Satuan</th>
                                                        <th>Sub Total</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tb_preview_penerimaan">

                                                </tbody>
                                            </table>
                                            <ul>
                                                <li>
                                                    <div class="form_grid_12 w-100 alpha">
                                                        <div class="form_grid_8">
                                                            <label class="field_title mb mt-dot2">Keterangan</label>
                                                            <div class="form_input">
                                                                <textarea id="Keterangan" name="Keterangan" class="form-control" placeholder="Keterangan" rows="9" style="resize: none;"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form_grid_4">
                                                            <div class="form_grid_12 mb-1">
                                                                <label class="field_title mb mt-dot2">DPP</label>
                                                                <div class="form_input input-not-focus">
                                                                    <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form_grid_12 mb-1">
                                                                <label class="field_title mb mt-dot2">PPN</label>
                                                                <div class="form_input input-not-focus">
                                                                    <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" readonly>
                                                                </div>
                                                            </div>
                                                                <!-- <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mb mt-dot2">Discount</label>
                                                                    <div class="form_input input-not-focus"> -->
                                                                        <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" readonly>
                                                                   <!--  </div>
                                                                   </div> -->
                                                                   <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mb">Total Invoice</label>
                                                                    <div class="form_input input-not-focus">
                                                                        <input type="hidden" id="Total" name="Total" class="form-control" placeholder="Total" readonly>
                                                                        <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- clear content -->
                                                            <div class="clear"></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end tab 1 -->

                                        <!-- tab 2 -->
                                     <!--    <div id="tab2">
                                            <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                                <div class="grid_12 w-100">
                                                    <span class="h_icon blocks_images"></span>
                                                    <h6>Total Pembayaran</h6>
                                                </div>
                                            </div>
                                            <ul>
                                                <li>
                                                    <div class="form_grid_12 w-100 alpha">
                                                        <div class="form_grid_6 mb-1">
                                                            <label class="field_title mb mt-dot2">Total Invoice</label>
                                                            <div class="form_input input-not-focus">
                                                                <input type="text" id="total_invoice_pembayaran" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                                            </div>
                                                        </div>

                                                        <div class="form_grid_6 mb-1">
                                                            <label class="field_title mb mt-dot2">Nama COA</label>
                                                            <div class="form_input input-disabled">
                                                                <select data-placeholder="Cari COA" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem" tabindex="13"  name="namacoa"  id="namacoa" required oninvalid="this.setCustomValidity('COA Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                                    <?php
                                                                    foreach ($bank as $data) {
                                                                        ?>
                                                                        <option value="<?php echo $data->IDCoa ?>"><?php echo $data->Nama_COA; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Jenis Pembayaran</label>
                                                        <div class="form_input">
                                                            <div class="mb-1">
                                                              <span>
                                                                <input type="radio" class="radio" name="jenis" id="jenis" value="Cash" onclick="jenis_pembayaran()">
                                                                <label class="choice">Cash</label>
                                                              </span>
                                                            </div>
                                                            <div class="mb-1">
                                                              <span>
                                                                <input type="radio" class="radio" name="jenis" id="jenis2" value="Transfer" onclick="jenis_pembayaran()">
                                                                <label class="choice">Transfer</label>
                                                              </span>
                                                            </div>
                                                            <div class="mb-1">
                                                              <span>
                                                                <input type="radio" class="radio" name="jenis" id="jenis3" value="Giro" onclick="jenis_pembayaran()">
                                                                <label class="choice">Giro</label>
                                                              </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Tgl Giro</label>
                                                        <div class="form_input">
                                                            <input type="date" id="tgl_giro" name="tgl_giro" class="input-date-padding-3-2 form-control" value="<?php echo date('Y-m-d'); ?>" placeholder="Tanggal Giro">
                                                        </div>
                                                    </div>
                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Nominal</label>
                                                        <div class="form_input">
                                                            <input type="text" id="nominal" name="nominal" class="form-control" value="0">
                                                        </div>
                                                    </div>
                                                    <span class="clear"></span>
                                                </li>
                                            </ul>
                                        </div> -->
                                    </div>
                                    <!-- end tab 2 -->

                                </div>
                                <!-- end tab & pane body -->

                            </div>
                        </div>
                        <!-- end content body tab & pane -->
                    </form>
                    <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2">
                            <div class="btn_30_blue">
                                <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                            </div>
                            <div class="btn_30_blue">
                                <span><a class="btn_24_blue" id="simpan" onclick="saveInvoice()" style="cursor: pointer;">Simpan</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    var base_url = window.location.pathname.split('/');
    var jenis;
    $("#Jatuh_tempo").prop("disabled", true);
    $("#Discount").prop("disabled", true);
    $("#namacoa").prop("disabled", true);


    function discount_inv(){
        if (document.getElementById('disc').checked)
        {
            $("#Discount").prop("disabled", false);
        } else {
            $("#Discount").prop("disabled", true);
        }
    }
    function jenis_pembayaran(){
        if (document.getElementById('jenis2').checked)
        {
            $("#namacoa").prop("disabled", false);
            jenis = 'Transfer';
        } else if (document.getElementById('jenis3').checked){
            $("#namacoa").prop("disabled", false);
            jenis = 'Giro';
        } else if (document.getElementById('jenis').checked){
            $("#namacoa").prop("disabled", true);
            jenis = 'Cash';
        }
    }
    function hitung() {
        var a = new Date($("#Tanggal").val());
        var b = $("#Jatuh_tempo").val();

        var today = new Date(a.getTime()+(b*24*60*60*1000));
        var dd = today.getDate();
        var mm = today.getMonth()+1;

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var today = yyyy+'-'+mm+'-'+dd;
            //$("#Tanggal_jatuh_tempo").val(today);
        }
    </script>
    <script type="text/javascript">
        function Penerimaan(){
            this.Corak;
            this.Warna;
            this.Merk;
            this.Saldo_qty;
            this.Total_qty;
            this.Grade;
            this.Satuan;
            this.Tanggal;
            this.ID_customer;
            this.NoSJ;
            this.Keterangan;
            this.Total_yard;
            this.Total_meter;
            this.Qty;
            this.Saldo_qty;
            this.Qty_roll;
            this.Saldo_roll;
            this.IDMata_uang;
            this.IDBarang;
            this.IDWarna;
            this.IDCorak;
            this.IDMerk;
            this.IDSJCS;
            this.Kurs;
            this.IDSatuan;
            this.Nama_barang;
            this.Harga_satuan;
            this.Subtotal;
            this.Harga;
            this.Harga_total;
        }

        var PM = new Array();
        index = 0;
        totaldpp=0;

        $(document).ready(function(){
            var PB = document.getElementById("Nomor");
            $('#Nomor').change(function(){
            //event.preventDefault();
            $(".rowid").remove();
            Nomor = $('#Nomor').val();
            //alert($('#no_sjc').val());

            $.post("<?php echo base_url("InvoicePenjualanNonKain/check_surat_jalan")?>", {'Nomor' : Nomor})
            .done(function(response) {

                var json = JSON.parse(response);
                    //console.log(json);
                    if (!json.error) {
                       index=0;
                       $("#tb_preview_penerimaan").html("");
                       $.each(json.data, function (key, value) {


                        totalqtyyard = 0;
                        totalqtymeter = 0;
                        totalroll = 0;
                        totaldpp=0;

                        var M = new Penerimaan();

                        M.Corak = value.Corak;
                        M.Warna = value.Warna;
                        M.Merk = value.Merk;
                        M.Saldo_qty = value.Saldo_qty;
                        M.Total_qty = value.Total_qty;
                        M.Qty = value.Qty;
                        M.Satuan = value.Satuan;
                        M.Qty_roll = value.Qty_roll;
                        M.Saldo_roll = value.Saldo_roll;
                        M.ID_customer = $('#IDCustomer').val();
                        M.IDMata_uang = value.IDMataUang;
                        M.IDBarang = value.IDBarang;
                        M.IDWarna = value.IDWarna;
                        M.IDCorak = value.IDCorak;
                        M.IDMerk = value.IDMerk;
                        M.IDSJCS = value.IDSJCS;
                        M.Kurs = value.Kurs;
                        M.IDSatuan = value.IDSatuan;
                        M.Nama_barang = value.Nama_Barang;
                        M.Harga = value.Harga;
                        M.Harga_total = value.Harga_total;

                        PM[index] = M;


                                // value = "<tr class='rowid'>"+
                                // "<td class='text-center'>"+(index+1)+"</td>"+
                                // "<td>"+PM[index].Nama_barang+"</td>"+
                                // "<td>"+PM[index].Qty+"</td>"+
                                // "<td>"+PM[index].Satuan+"</td>"+
                                // "<td>"+
                                // "<input type=text name=harga_satuan value="+PM[index].Harga+" id=harga_satuan"+index+" onkeyup=hitung_total("+index+")>" +
                                // "</td>"+
                                // "<td>"+"<input type=hidden name=harga_total id=harga_total"+index+" value=0 readonly> <input type=text name=harga_total_b id=harga_total_b"+index+" readonly>"+"</td>"+
                                // "<td class='text-center'>" +
                                // "<div class='hidden-sm hidden-xs action-buttons'>"+
                                // "<span><a href='#' class='action-icons c-delete'  id='bootbox-confirm' onclick='deleteItem("+index+")'>Delete"+"</a></span>"+
                                // "</div>"+
                                // "</td>"+
                                // "</tr>";
                                // $("#tb_preview_penerimaan").append(value);

                                var table = $('#tabelfsdfsf').DataTable();
                                table.clear().draw();

                                table.row.add([
                                    index+1,
                                    PM[index].Nama_barang,
                                    PM[index].Qty,
                                    PM[index].Satuan,
                                    "<input type=text name=harga_satuan value="+PM[index].Harga+" id=harga_satuan"+index+" onkeyup=hitung_total("+index+")>",
                                    "<input type=hidden name=harga_total id=harga_total"+index+" value=0 readonly> <input type=text name=harga_total_b id=harga_total_b"+index+" readonly>",
                                    "<span><a href='#' class='action-icons c-delete'  id='bootbox-confirm' onclick='deleteItem("+index+")'>Delete"+"</a></span>"
                                    ]).draw();
                                totalqtyyard += parseInt(PM[index].Qty);
                                totalroll += parseInt(PM[index].Qty_roll);

                                var harga_satuan1= $("#harga_satuan"+index).val();
                                var a = PM[index].Qty;
                                var b = harga_satuan1.replace(".","");
                                var c=a*b;
                                $("#harga_total"+index).val(c);
                                $("#harga_total_b"+index).val(rupiah(c));
                                PM[index].Harga_total=c;


                                // for(l=0; l < index; l++){
                                    totaldpp += parseInt($("#harga_total"+index).val());
                                // }
                                disc = 0;
                                if($("#Status_ppn").val()=="Include"){
                                 if(disc == 0)
                                 {
                                   dpp=  100/110*totaldpp;
                                   ppn= dpp*0.1;
                                   $("#DPP").val(rupiah(Math.round(dpp)));
                                   $("#PPN").val(rupiah(Math.round(ppn)));
                                   $("#Discount_total").val(rupiah(disc));
                                   total= totaldpp-ppn;
                                   $("#total_invoice").val(rupiah(total-disc+ppn));
                                   $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                 }else{
                                   dpp=  100/110*(totaldpp-disc);
                                   ppn=  dpp*0.1;
                                   $("#DPP").val((Math.round(dpp)));
                                   $("#PPN").val(rupiah(Math.round(ppn)));
                                   $("#Discount_total").val(rupiah(disc));
                                   total= totaldpp-ppn;
                                   $("#total_invoice").val(rupiah(total-disc+ppn));
                                   $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                 }
                                 
                               }else if($("#Status_ppn").val()=="Nonppn"){
                                 ppn= 0;
                                 $("#DPP").val(rupiah(Math.round(totaldpp)));
                                 $("#PPN").val(rupiah(Math.round(ppn)));
                                 $("#Discount_total").val(rupiah(disc));
                                 total= totaldpp-ppn;
                                 $("#total_invoice").val(rupiah(total-disc+ppn));
                                 $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                               }else{
                                 if(disc == 0){
                                   ppn= totaldpp*0.1;
                                   $("#DPP").val(rupiah(Math.round(totaldpp)));
                                   $("#PPN").val(rupiah(Math.round(ppn)));
                                   $("#Discount_total").val(rupiah(disc));
                                   $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                                   $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                 }else{
                                   ppn= (totaldpp-disc)*0.1;
                                   $("#DPP").val(rupiah(Math.round(totaldpp)));
                                   $("#PPN").val(rupiah(Math.round(ppn)));
                                   $("#Discount_total").val(rupiah(disc));
                                   $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                                   $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                 }
                               }


                            // }
                            index++;
                            $("#Total_qty").val(totalqtyyard);
                            $("#Total_roll").val(totalroll);
                            $("#Total").val(totaldpp);
                        });
} else if($("#Nomor").val()==''){

}else {
  alert('Tidak ada data dengan nomor Penerimaan ' + Nomor);
}
});
});
});

function ubah_status_ppn()
{
  disc = 0;
  if($("#Status_ppn").val()=="Include"){
    // ppn= $("#Total").val()*0.1;
    if(disc==0){
      dpp=  100/110*$("#Total").val();
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(Math.round(dpp)));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      dpp=  100/110*($("#Total").val()-disc);
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(Math.round(dpp)));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }else if($("#Status_ppn").val()=="Nonppn"){
    ppn= 0;
    $("#DPP").val(rupiah(Math.round($("#Total").val())));
    $("#PPN").val(rupiah(Math.round(ppn)));
    $("#Discount_total").val(rupiah(disc));
    total= $("#Total").val()-ppn;
    $("#total_invoice").val(rupiah(total-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
  }else{
    if(disc == 0){
      ppn= $("#Total").val()*0.1;
      $("#DPP").val(rupiah(Math.round($("#Total").val())));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      ppn= ($("#Total").val()-disc)*0.1;
      $("#DPP").val(rupiah(Math.round($("#Total").val())));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }
}

function hitung_total(i) {
    var harga_satuan1= $("#harga_satuan"+i).val();
    var a = PM[i].Qty;
    var b = harga_satuan1.replace(".","");
    var c=a*b;

    PM[i].Harga_satuan = parseInt(b);
    PM[i].Subtotal = c;

    $("#harga_total"+i).val(c);
    $("#harga_total_b"+i).val(rupiah(c));
    PM[i].Harga_total=c;
    totaldpp=0;
    for(l=0; l < index; l++){
        totaldpp += parseInt($("#harga_total"+l).val());
    }
            //disc=  $("#Discount").val();
            disc = 0;
            if($("#Status_ppn").val()=="Include"){
                ppn= totaldpp*0.1;
                $("#DPP").val(rupiah(totaldpp-ppn));
                $("#PPN").val(rupiah(ppn));
                $("#Discount_total").val(rupiah(disc));
                total= totaldpp-ppn;
                $("#total_invoice").val(rupiah(total-disc+ppn));
                $("#total_invoice_pembayaran").val(rupiah(total-disc+ppn));
            }else{
                ppn= totaldpp*0.1;
                $("#DPP").val(rupiah(totaldpp));
                $("#PPN").val(rupiah(ppn));
                $("#Discount_total").val(rupiah(disc));
                $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
            }

        }
        function fieldInvoice(){
            var data1 = {
                "id_invoice" : $("#id_invoice").val(),
                "Tanggal" :$("#Tanggal").val(),
                "Nomor":$("#Nomor_invoice").val(),
                "Nomor_seragam":$("#Nomor").val(),
                "IDCustomer":$("#IDCustomer").val(),
                "Nama_di_faktur":$("#nama_faktur").val(),
                "IDSJCS": PM[0].IDSJCS,
                "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
                "IDMataUang":PM[0].IDMata_uang,
                "Kurs":PM[0].Kurs,
                "Total_yard":$("#Total_qty").val(),
                "Total_pieces":$("#Total_roll").val(),
                "Discount":0,
                "Status_ppn":$("#Status_ppn").val(),
                "Keterangan":$("#Keterangan").val()
            }
            return data1;
        }
        function fieldgrandtotal(){
            var data3 = {
                //"id_invoice_grand_total" : $("#id_invoice_grand_total").val(),
                //"id_pembayaran" : $("#id_pembayaran").val(),
                "jenis" :jenis,
                "DPP" :$("#DPP").val(),
                "Discount" :0,
                "PPN" :$("#PPN").val(),
                "total_invoice" :$("#total_invoice").val(),
                "nominal" :$("#nominal").val(),
                "tgl_giro" :$("#tgl_giro").val(),
                "namacoa" : $("#namacoa").val()
            }
            return data3;
        }

        function harga_satuan_total(i)
        {
            // var data6 = new Array();
            for(i=0; i < index; i++){
                PM[i].harga_satuan = $("#harga_satuan"+i).val();
                PM[i].harga_total = $("#harga_total"+i).val();
            }
            // return data6;
        }

        function saveInvoice(){
          $("#Nomor").css('border', '');
          $("#Tanggal_jatuh_tempo").css('border', '');
          $("#nama_faktur").css('border', '');

          if($("#Nomor").val()==""){
            $('#alert').html('<div class="pesan sukses">Data SJ Tidak Boleh Kosong</div>');
            $("#Nomor").css('border', '1px #C33 solid').focus();
        }else if($("#Tanggal_jatuh_tempo").val()==""){
            $('#alert').html('<div class="pesan sukses">Tanggal_jatuh_tempo Tidak Boleh Kosong</div>');
            $("#Tanggal_jatuh_tempo").css('border', '1px #C33 solid').focus();
        }else if($("#nama_faktur").val()==""){
            $('#alert').html('<div class="pesan sukses">Nama Faktur Tidak Boleh Kosong</div>');
            $("#nama_faktur").css('border', '1px #C33 solid').focus();
        }else if($("#NamaCustomer").val()==""){
            $('#alert').html('<div class="pesan sukses">Nama Customer Tidak Boleh Kosong</div>');
            $("#NamaCustomer").css('border', '1px #C33 solid').focus();
        }else{
            var data1 = fieldInvoice();
            var data3 = fieldgrandtotal();
            var idfjs = '_';

                //console.log(data1);
                console.log(PM);
                //console.log(data3);

                $.ajax({
                    url: "simpan_invoice_penjualan",
                    type: "POST",
                    data: {
                        "data1" : data1,
                        "data2" : PM,
                        "data3" : data3
                    },
                    dataType: 'json',
                    success: function (msg, status) {
                        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');

                        document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
                        document.getElementById("simpan").setAttribute("onclick", "return false;");
                        document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
                        document.getElementById("printdata").setAttribute("onclick", "return true;");
                        document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/InvoicePenjualanNonKain/print_/'+data1['Nomor'] + '/' + idfjs);
                    },
                    error: function(msg, status){
                        console.log(msg);
                        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
                    }
                });

            }
        }

        function cek_customer()
        {
          var Nomor = $("#Nomor").val();
          $.ajax({
            url : '/'+base_url[1]+'/InvoicePenjualanNonKain/cek_customer',
            type: "POST",
            data:{Nomor:Nomor},
            dataType:'json',
            success: function(data)
            { 
              var html = '';
              if (data <= 0) {
                console.log('-----------data kosong------------');
            } else {
               $("#NamaCustomer").val(data[0].Nama);
               $("#IDCustomer").val(data[0].IDCustomer);
               $("#Tanggal_jatuh_tempo").val(data[0].Tanggal_jatuh_tempo);
           }
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
    }
});
      }
  </script>
  <?php $this->load->view('administrator/footer') ; ?>