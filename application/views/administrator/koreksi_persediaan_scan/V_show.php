<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Koreksi Persediaan Scan</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('KoreksiPersediaanScan')?>">Data Koreksi Persediaan</a></li>
                            <li style="text-transform: capitalize;"> Detail Koreksi Persediaan Scan</li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Tanggal ?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">Nomor</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #ffffff;">Total QTY Yard</td>
                                    <td  style="background-color: #ffffff;"><?php echo $data->Total_yard ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">Total QTY Meter</td>
                                    <td style="background-color: #e5eff0;"><?php echo $data->Total_meter ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #ffffff;">Jenis</td>
                                    <td  style="background-color: #ffffff;"><?php echo $data->Jenis ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Keterangan</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $data->Keterangan ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Batal ?></td>
                                </tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                        

                         <br><br>
                                 <div class="widget_content">

                <table class="display data_tbl">
                  <thead>
                   <tr>
                    <th class="center" style="width: 40px">No</th>
                    <th>Barcode</th>
                    <th class="hidden-xs">Corak</th>
                    <th>Warna</th>
                    <th>Qty Yard</th>
                    <th>Qty Meter</th>
                    <th>Grade</th>
                    <th>Harga</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($koreksidetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($koreksidetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Barcode; ?></td>
                          <td><?php echo $data2->Corak; ?></td>
                          <td><?php echo $data2->Warna; ?></td>
                          <td><?php echo $data2->Qty_yard; ?></td>
                          <td><?php echo $data2->Qty_meter; ?></td>
                          <td><?php echo $data2->Grade; ?></td>
                          <td><?php echo $data2->Harga; ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>


                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('KoreksiPersediaanScan')?>">Kembali</a></span>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>