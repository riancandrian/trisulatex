<?php $this->load->view('administrator/header') ; 
if ($kodenoasset->curr_number == null) {
  $number = 1;
} else {
  $number = $kodenoasset->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_no_kp = 'KPS'.'/'.date('y').'/'.str_replace(' ', '',$agen).'/'.$kodemax;
?>
<div class="page_title">
    <div id="alert"></div>
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Koreksi Persediaan</h3>

    <div class="top_search">
    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>KoreksiPersediaanScan">Data Koreksi Persediaan Scan</a></li>
                            <li class="active">Tambah Koreksi Persediaan Scan</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Koreksi Persediaan Scan</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="" class="form_container left_label">
                        <ul>
                            <li class="body-search-data">
                                    <div class="form_grid_12 w-100 alpha">

                                        <div class="form_grid_8">
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Tanggal</label>
                                                <div class="form_input">
                                                    <input type="date" class="form-control input-date-padding date-picker hidden-sm-down" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d');?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>

                                    
                                          
                                            
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Total Yard</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="total_yard" id="total_yard" readonly>
                                                </div>
                                            </div>

                                              <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Nomor KP</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="no_kp" id="no_kp" value="<?php echo $code_no_kp; ?>" readonly>
                                                </div>
                                            </div>
                                             <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Jenis</label>
                                                <div class="form_input">
                                                    <select data-placeholder="Cari Jenis" style="width:100%;" class="chzn-select" tabindex="13" name="jenis" id="jenis" required oninvalid="this.setCustomValidity('Jenis Tidak Boleh Kosong')">
                                                           <option value='minus'>Minus (-)</option>
                                                           <option value='plus'>Plus (+)</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Total Meter</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="total_meter" id="total_meter" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form_grid_4 mb-1">
                                            <label class="field_title mt-dot2">Keterangan</label>
                                            <div class="form_input">
                                                <textarea class="form-control" name="keterangan" id="keterangan" style="resize: none;" rows="9"></textarea>
                                            </div>
                                        </div>
                                        <span class="clear"></span>
                                    </div>
                                </li>
                            <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title mt-dot2 mb">Barcode Scan</label>
                  <div class="form_input">
                    <input type="text" name="Barcode" id="Barcode" class="form-control" placeholder="Scan Barcode" autofocus>
                  </div>
                </div>
              </li>
              <!-- =============================-Table Bawah -->
              <div class="widget_content">
                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Barcode</th>
                      <th>Corak</th>
                      <th>Warna</th>
                      <th>Merk</th>
                      <th>Qty Yard</th>
                      <th>Qty Meter</th>
                      <th>Grade</th>
                      <th>Satuan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>

              </div>

                            
                        </ul>


                    </form>
                    <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2">
                           <!--  <div class="btn_30_blue">
                          <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                      </div> -->
                      
                      <div class="btn_30_blue">
                        <span><a class="btn_24_blue" id="simpan" style="cursor: pointer;" onclick="SavePersediaanScan()">Simpan</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/koreksi_persediaan_scan.js"></script>
<script>
   function hilang_huruf(value, degree)
{
  // this.value=this.value.replace(/[^0-9.]/,'');
  document.getElementById("qty_yard").value=value.replace(/[^0-9.]/,'');
    var x;
    if (degree == "C") {
        x = document.getElementById("qty_yard").value * 0.9144
        document.getElementById("qty_meter").value = x.toFixed(2);
    } else {
        x = (document.getElementById("qty_meter").value * 1.09361);
        document.getElementById("qty_yard").value = x.toFixed(2);
    }
}

function hilang_huruf2(value, degree)
{
  // this.value=this.value.replace(/[^0-9.]/,'');
  document.getElementById("qty_meter").value=value.replace(/[^0-9.]/,'');
    var x;
    if (degree == "C") {
        x = document.getElementById("qty_yard").value * 0.9144
        document.getElementById("qty_meter").value = x.toFixed(2);
    } else {
        x = (document.getElementById("qty_meter").value * 1.09361);
        document.getElementById("qty_yard").value = x.toFixed(2);
    }
}
</script>
<?php $this->load->view('administrator/footer') ; ?>