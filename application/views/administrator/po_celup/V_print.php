<!DOCTYPE HTML>
<html>

<head>
	<title>PT Trisula Textile Industries Tbk</title>
	<link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen,print">

</head>
<body>
	<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">

					<h1 style="text-align: center;" class="mb">DYEING-ORDER</h1>
					<div class="widget_content">
						<div class=" page_content">
							<div class="invoice_container">
								<div class="grid_6 invoice_num" style="margin-bottom: 0">
									<table style="font-size: 12px">
										<tr>
											<td>Tanggal</td>
											<td> : </td>
											<td><?php echo $print->Tanggal ?></td>
										</tr>
										<tr>
											<td>No Urut</td>
											<td> : </td>
											<td><?php echo $print->Nomor ?></td>
										</tr>
										<tr>
											<td>Corak</td>
											<td> : </td>
											<td><?php echo $print->Corak ?></td>
										</tr>

									</table>

								</div>

								<div class="grid_6 invoice_num mb" style="margin-bottom: 0">
									<table style="font-size: 12px">
										<tr>
											<td>Agent</td>
											<td> : </td>
											<td><?php echo $print->Nama_Perusahaan ?></td>
										</tr>
										<tr>
											<td>Untuk</td>
											<td> : </td>
											<td><div style="font-size: 12px !important">
										<?php if($print->Jenis_Pesanan=="seragam"){
											?>
											<input type="checkbox" value="Seragam" checked> Seragam
											<input type="checkbox" value="Stock"> Stock
											<input type="checkbox" value="Garment"> Garment
											<?php
										} elseif($print->Jenis_Pesanan=="stok"){
											?>
											<input type="checkbox" value="Seragam"> Seragam
											<input type="checkbox" value="Stock" checked> Stock
											<input type="checkbox" value="Garment"> Garment
											<?php
										} elseif($print->Jenis_Pesanan=="garment"){
											?>
											<input type="checkbox" value="Seragam"> Seragam
											<input type="checkbox" value="Stock"> Stock
											<input type="checkbox" value="Garment" checked> Garment
											<?php
										}
										?>
									</div></td>
										</tr>
										<tr>
											<td>LOT</td>
											<td> : </td>
											<td><?php echo $print->Lot ?></td>
										</tr>

									</table>
									
								</div>
								
								<div class="grid_12 invoice_details">
									<div class="invoice_tbl">
										<table>
											<thead>
												<tr class=" gray_sai">
													<th>
														No
													</th>
													<th>
														Warna
													</th>
													<th>
														Qty
													</th>
												</tr>
											</thead>
											<tbody>
												<?php if(empty($printdetail)){
													?>
													<?php
												}else{
													$i=1;
													foreach ($printdetail as $data2) {
														?>
														<tr class="odd gradeA">
															<td><?php echo $i; ?></td>
															<td><?php echo $data2->Warna; ?></td>
															<td><?php echo $data2->Qty; ?></td>
														</tr>
														<?php
														$i++;
													}
												}
												?>


											</tbody>
										</table>
									</div>
									<hr>
									<h4><u><strong>DELIVERY</strong></u></h4>
									<div class="btn_30_light text-left">
										<table style="border: none; ">
											<tr style="border: none;padding: 0; box-shadow: none;">
												<td style="border: none;padding: 0; text-align: left;"><strong>YANG DIHARAPKAN AGENT</strong></td>
												<td style="border: none;padding: 0; text-align: left;">:</td>
												<td style="border: none;padding: 0; text-align: left;"><?php echo strtoupper($print->Prioritas) ?></td>
											</tr>
											<tr style="border: none;padding: 0; box-shadow:none;">
												<td style="border: none;padding: 0; text-align: left;"><strong>KONFIRMASI PIC</strong></td>
												<td style="border: none;padding: 0; text-align: left;">:</td>
												<td style="border: none;padding: 0; text-align: left;"><?php echo strtoupper($print->PIC) ?></td>
											</tr>
										</table>
									</div>
									<hr>
									<u><h4><strong>PACKING & PENGIRIMAN</strong></h4></u>
									<div style="margin-top: .5rem">
										<table style="border: none; ">
											<tr style="border: none;padding: 0; box-shadow: none;">
												<td style="border: none;padding: 0; text-align: left;"><strong>A.</strong> Bentuk Gulungan</td>
												<td style="border: none;padding: 0; text-align: left;"><strong>B.</strong> Panjang </td>
												<td style="border: none;padding: 0; text-align: left;"><strong>C.</strong> Point </td>
											</tr>
											<tr style="border: none;padding: 0; box-shadow: none;">
												<td style="border: none;padding: 0; text-align: left;"><?php if($print->Bentuk=="Piece (Double Fold)")
						{
							?>
							<input type="checkbox" value="Piece (Double Fold)" checked> Piece (Double Fold)<br>
							<input type="checkbox" value="Roll (Single Fold)"> Roll (Single Fold)
							<?php
						}elseif($print->Bentuk=="Roll (Single Fold)"){
							?>
							<input type="checkbox" value="Piece (Double Fold)"> Piece (Double Fold)<br>
							<input type="checkbox" value="Roll (Single Fold)" checked> Roll (Single Fold)
							<?php
						}elseif($print->Bentuk=="-"){
							?>
							<input type="checkbox" value="Piece (Double Fold)"> Piece (Double Fold)<br>
							<input type="checkbox" value="Roll (Single Fold)"> Roll (Single Fold)
							<?php
						} ?></td>
												<td style="border: none;padding: 0; text-align: left;"><?php if($print->Panjang=="Standar")
						{
							?>
							<input type="checkbox" value="Standar" checked> Standar 30 Y<br>
							<input type="checkbox" value="Besar"> Besar 50 Y
							<?php
						}elseif($print->Panjang=="Besar"){
							?>
							<input type="checkbox" value="Standar"> Standar 30 Y<br>
							<input type="checkbox" value="Besar" checked> Besar 50 Y
							<?php
						}elseif($print->Panjang=="-"){
							?>
							<input type="checkbox" value="Standar"> Standar 30 Y<br>
							<input type="checkbox" value="Besar"> Besar 50 Y
							<?php
						} ?></td>
												<td style="border: none;padding: 0; text-align: left;"><?php if($print->Point=="Terpisah")
						{
							?>
							<input type="checkbox" value="Terpisah" checked> Terpisah <br>
							<input type="checkbox" value="Ikut piece lain"> Ikut piece lain
							<?php
						}elseif($print->Point=="Ikut piece lain"){
							?>
							<input type="checkbox" value="Terpisah"> Terpisah <br>
							<input type="checkbox" value="Ikut piece lain" checked> Ikut piece lain
							<?php
						}elseif($print->Point=="-"){
							?>
							<input type="checkbox" value="Terpisah"> Terpisah <br>
							<input type="checkbox" value="Ikut piece lain"> Ikut piece lain
							<?php
						} ?></td>
											</tr>

										</table>
									</div>
									<hr>
									<u><h4><strong>CAP / STAMPING</strong></h4></u>
									<div style="margin-top: .5rem">
										<table style="border: none; width:100%;">
											<tr style="border: none;padding: 0; box-shadow: none;">
												<?php if($print->Stamping=="Cap")
												{
													?>
													<td style="border: none;padding: 0; text-align: left;">
														<input type="checkbox" value="Cap" checked> Cap
													</td>
													<td style="border: none;padding: 0; text-align: left;">
														<input type="checkbox" value="Tanpa Cap"> Tanpa Cap
													</td>
													<?php
												}elseif($print->Stamping=="Tanpa Cap"){
													?>
													<td style="border: none;padding: 0; text-align: left;">
														<input type="checkbox" value="Cap"> Cap
													</td>
													<td style="border: none;padding: 0; text-align: left;">
														<input type="checkbox" value="Tanpa Cap" checked> Tanpa Cap
													</td>
													<?php
												} ?>
												<td style="border: none;padding: 0; text-align: left;">
													<?php if($print->Posisi!="-")
													{
														?>
														<input type="checkbox" value="Selvedge" checked> Selvage / Pinggir
														<?php
													}elseif($print->Posisi=="-"){
														?>
														<input type="checkbox" value="Selvedge"> Selvage / Pinggir
														<?php
													} ?>
													<?php if($print->Posisi1!="-")
													{
														?>
														<input type="checkbox" value="Face" checked> Face / Kepala
														<?php
													}elseif($print->Posisi=="-"){
														?>
														<input type="checkbox" value="Selvedge"> Face / Kepala
														<?php
													} ?></td>
													<td style="border: none;padding: 0; text-align: left; padding-top: .4rem;padding-left: .5rem">Merk : <?php echo $print->Merk ?></td>
												</tr>

											</table>
										</div>
										<hr>
										<u><h4><strong>SAMPLE</strong></h4></u>
										<div style="margin-top: .5rem">
											<table style="border: none; width:100%;">
												<tr style="border: none;padding: 0; box-shadow: none;">Album : <?php echo $print->Album ?></tr><br>
												<tr style="border: none;padding: 0; box-shadow: none;">M10/20 : <?php echo $print->M10 ?></tr><br>
												<tr style="border: none;padding: 0; box-shadow: none;">Kain : <?php echo $print->Kain ?></tr><br>
												<tr style="border: none;padding: 0; box-shadow: none;">Lembaran : <?php echo $print->Lembaran ?></tr>

											</table>
										</div>
										<hr>
										<u><h4><strong>CATATAN - CATATAN</strong></h4></u>
										<div style="margin-top: .5rem">
											<h6><b>Perhatikan Standard2 (Warna, Handling dll)</b></h6><br>
											<?php echo $print->Keterangan ?>
										</div>
										<hr>
										<p class="amount_word"><b><u><h4>Keperluan PIC dan SCh SO</h4></u></b></p>
										<div class="btn_30_light">
											<input type="checkbox" value="A"> A
											<input type="checkbox" value="B"> B
											<input type="checkbox" value="C"> C
										</div>
										<span class="clear"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="clear"></span>
			</div>
		</body>
		</html>
		<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				window.print();
				console.log('masuk kesini');
     			window.location.href = "<?php echo base_url() ?>PoCelup/index";
  });
</script>