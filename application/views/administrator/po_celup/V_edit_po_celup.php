<?php $this->load->view('administrator/header') ; 
?>
<!-- =============================================Style untuk tabs -->
<style type="text/css" scoped>
  /* Style the tab */
  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
  }
  .tab a {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
    color:black;
  }
  .tab a:hover {
    background-color: #ddd;
  }
  .tab a.active {
    background-color: #ccc;
  }
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }
  .chzn-container {
    width: 103.5% !important;
  }
    .table_bottom{
  margin-top: 3rem;
}
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Purchase Order Celup</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>PoCelup">Data Purchase Order Celup</a></li>
              <li class="active">Edit Purchase Order Celup</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Edit Data Purchase Order Celup</h6>
          </div>
        </div>
        <!-- =============================================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>
              <!-- =============================-Form Atas -->
              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_10">
                    <!-- form 1 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Tanggal</label>
                      <div class="form_input">
                        <input type="hidden" name="IDPO" id="IDPO" value="<?php echo $datapo->IDPO ?>">
                        <input id="Tanggal" type="date" value="<?= $datapo->Tanggal ?>" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Supplier</label>
                      <div class="form_input">
                        <input type="hidden" name="IDSupplier" id="IDSupplier" value="<?php echo $trisula->IDSupplier ?>" readonly>
                        <input type="text" name="namaSupplier" value="<?php echo $trisula->Nama ?>" readonly>
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Agent</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="IDAgen" id="IDAgen" class="form-control" data-id="<?php echo $datapo->IDAgen ?>" placeholder="No Asset" value="<?php echo $datapo->Initial ?>" readonly>
                      </div>
                    </div>

                    <!-- form 4 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">No PO</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $datapo->Nomor ?>" readonly>
                      </div>
                    </div>

                    <!-- form 5 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Corak</label>
                      <div class="form_input">
                        <input type="text" name="namacorak" id="namacorak" value="<?php echo $datapo->Corak ?>" readonly>
                        <input type="hidden" name="corak" id="corak" value="<?php echo $datapo->IDCorak ?>" readonly  >
                      </div>
                    </div>

                    <!-- form 6 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Lot</label>
                      <div class="form_input">
                        <input type="text" name="Lot" id="Lot" class="form-control" placeholder="Lot" value="<?= $datapo->Lot ?>">
                      </div>
                    </div>

                    <!-- form 7 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title">Tanggal Selesai</label>
                      <div class="form_input">
                        <input id="Tanggal_selesai" value="<?= $datapo->Tanggal_selesai ?>" type="date" name="Tanggal_selesai" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <span class="clear"></span>
                      </div>
                    </div>

                    <!-- form 8 -->
                    <!-- <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Merk</label>
                      <div class="form_input input-not-focus"> -->
                      <input type="hidden" name="merk" id="merk" data-idmerk class="form-control" placeholder="Merk" readonly>
                    <!-- </div>
                  </div> -->

                  <!-- form 9 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mt-dot2">Total</label>
                    <div class="form_input input-not-focus">
                      <input type="text" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" value="<?= $datapo->Total_qty ?>" readonly>
                      <span class="clear"></span>
                    </div>
                  </div>
                </div>
                <div class="form_grid_2">

                  <!-- radio 1 -->
                  <div class="form_grid_12 mb-1">
                    <div class="form_input radio-padding-form">

                      <?php
                      if ($datapo->Jenis_Pesanan == "seragam") { ?>
                      <span>
                        <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="seragam" checked>
                        <label class="choice">Seragam</label>
                      </span>
                      <?php } else { ?>
                      <span>
                        <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="seragam">
                        <label class="choice">Seragam</label>
                      </span>
                      <?php }
                      ?>

                    </div>
                  </div>

                  <!-- radio 2 -->
                  <div class="form_grid_12 mb-1">
                    <div class="form_input radio-padding-form">
                      <?php
                      if ($datapo->Jenis_Pesanan == "stok") { ?>
                      <span>
                        <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="stok" checked>
                        <label class="choice">Stok</label>
                      </span>
                      <?php } else { ?>
                      <span>
                        <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="seragam">
                        <label class="choice">Stok</label>
                      </span>
                      <?php }
                      ?>
                    </div>
                  </div>

                  <!-- radio 3 -->
                  <div class="form_grid_12 mb-1">
                    <div class="form_input radio-padding-form">
                      <?php
                      if ($datapo->Jenis_Pesanan == "garment") { ?>
                      <span>
                        <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="garment" checked>
                        <label class="choice">Garment</label>
                      </span>

                      <?php } else { ?>
                      <span>
                        <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="garment">
                        <label class="choice">Garment</label>
                      </span>
                      <?php }
                      ?>
                    </div>
                  </div>
                </div>

                <!-- clear content -->
                <span class="clear"></span>
              </div>
            </li>

          </ul>

          <div class="grid_12 mx w-100">
            <div class="widget_wrap tabby">
              <div class="widget_top">
                <!-- <span class="h_icon bended_arrow_right"></span>
                  <h6>Tabby Widget</h6> -->
                  <div id="widget_tab">
                    <ul>
                      <li class="px py"><a href="#tab1" class="active_tab">Input Warna</a></li>
                      <li class="px py"><a href="#tab2">Metode Packing</a></li>
                      <li class="px py"><a href="#tab3">Cap Pinggir</a></li>
                      <li class="px py"><a href="#tab4">Catatan</a></li>

                    </ul>
                  </div>
                </div>
                <div class="widget_content">

                  <!-- tab & pane 3 -->
                  <div id="tab1">
                    <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                      <div class="grid_12 w-100">
                        <span class="h_icon blocks_images"></span>
                        <h6>Warna</h6>
                      </div>
                    </div>
                    <div class="form_grid_12 w-100 alpha">
                      <div class="form_grid_4 mb-1">
                        <ul>
                          <li>
                            <div class="form_grid_12 mb-1">
                              <label class="field_title mt-dot2">Warna</label>
                              <div class="form_input">
                               <select data-placeholder="--Silahkan Pilih Corak Dahulu--" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" tabindex="13" name="warna" id="warna" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                <option value=""></option>
                              </select>
                            </div>
                          </div>
                          <div class="form_grid_12 mb-1">
                            <span class="input-group-addon" style="right: -5px; z-index: 1">Yard</span>
                            <label class="field_title mt-dot2">Qty</label>
                            <div class="form_input">
                              <input type="text" name="Qty" id="Qty" class="form-control" placeholder="Qty" onkeyup="this.value=this.value.replace(/[^0-9.]/,'')">
                            </div>
                          </div>
                          <div class="form_grid_12 mb-1">
                            <label class="field_title mt-dot2">Harga</label>
                            <div class="form_input">
                              <input type="text" name="Harga" id="harga-123" class="form-control" placeholder="Harga">
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="form_input">
                            <div class="btn_24_blue">
                            <button class="btn_24_blue" type="button" id="tambah_sementara">
                              Tambah Data
                            </button>
                          </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="form_grid_8 mb-1 mr" style="width: 65.6%">
                      <table class="display data_tbl" id="tabelfsdfsf">
                        <thead>
                          <tr>
                            <th class="center" style="width: 40px">No</th>
                            <th>Warna</th>
                            <th>Qty Yard</th>
                            <th>Harga</th>
                            <th>Total</th>
                            <th style="width: 50px">Action</th>
                          </tr>
                        </thead>
                        <tbody id="tabel-cart-asset">

                        </tbody>
                      </table>

                       <div class="form_grid_12 w-100 alpha">


                      <!-- form 1 -->
                      <div class="form_grid_2 mb-1" style="right: 15px; bottom: 5.5rem; float: right;">
                        <div class="form_input">
                          <input type="text" name="Totalwarna" id="Totalwarna" class="form-control" placeholder="Grand Total" required>
                        </div>
                      </div>
                    </div>
                    </div>
                    <span class="clear"></span>
                  </div>
                  <div class="widget_top" style="margin-top: -10px">
                    <div class="grid_12" style="margin-top: -10px">
                      <span class="h_icon blocks_images"></span>
                      <h6>Delivery</h6>
                    </div>
                  </div>

                  <ul>
                    <li>
                      <div class="form_grid_12 w-100 alpha">
                        <div class="form_grid_6 mb-1">
                          <label class="field_title mt-dot2">PIC</label>
                          <div class="form_input">
                            <input type="text" id="PIC" name="PIC" class="form-control" placeholder="PIC" value="<?= $datapo->PIC ?>">
                          </div>
                        </div>
                        <span class="clear"></span>
                      </div>

                      <div class="form_grid_12 w-100 alpha">
                        <div class="form_grid_6 mb-1">
                          <div class="form_input mb-4">
                            <?php if ($datapo->Prioritas == "normal") { ?>
                            <input type="radio" class="radio" name="Prioritas" id="Prioritas" value="normal" checked> 
                            <label class="choice">Normal</label>
                            <?php } else { ?>
                            <input type="radio" class="radio" name="Prioritas" id="Prioritas" value="normal">
                            <label class="choice">Normal</label>
                            <?php }
                            ?>

                            <?php if ($datapo->Prioritas == "urgent") { ?>
                            <input type="radio" class="radio" name="Prioritas" id="Prioritas" value="urgent" checked>
                            <label class="choice">Urgent</label>
                            <?php } else { ?>
                            <input type="radio" class="radio" name="Prioritas" id="Prioritas" value="urgent">
                            <label class="choice">Urgent</label>
                            <?php }
                            ?>
                          </div>
                          <span class="clear"></span>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <span class="clear"></span>
                </div>
              </div>

              <!-- tab & pane 2 -->
              <div id="tab2">
                <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                  <div class="grid_12 w-100">
                    <span class="h_icon blocks_images"></span>
                    <h6>Metode Packing & Pengiriman</h6>
                  </div>
                </div>

                <ul>
                  <li>

                    <div class="form_grid_12 multiline">
                      <div class="form_grid_6">
                        <label class="field_title dot-2">Bentuk</label>
                        <div class="form_input">
                          <select style="width:100%;" class="form-control" name="Bentuk" id="Bentuk" required oninvalid="this.setCustomValidity('Bentuk Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                           <?php 
                            if ($datapo->Bentuk == "Piece (Double Fold)") {
                              ?>
                              <option value="-">-</option>
                               <option value="Piece (Double Fold)" selected>Piece (Double Fold)</option>
                            <option value="Roll (Single Fold)">Roll (Single Fold)</option>
                            <?php
                            }elseif ($datapo->Bentuk == "Roll (Single Fold)") {
                              ?>
                              <option value="-">-</option>
                               <option value="Piece (Double Fold)">Piece (Double Fold)</option>
                            <option value="Roll (Single Fold)" selected>Roll (Single Fold)</option>
                            <?php
                          }elseif ($datapo->Bentuk == "-") {
                              ?>
                              <option value="-" selected>-</option>
                             <option value="Piece (Double Fold)">Piece (Double Fold)</option>
                            <option value="Roll (Single Fold)">Roll (Single Fold)</option>
                            <?php
                          }else{
                            ?>
                            <option value="-">-</option>
                            <option value="Piece (Double Fold)">Piece (Double Fold)</option>
                            <option value="Roll (Single Fold)">Roll (Single Fold)</option>
                           <?php 
                          }
                           ?>
                          </select>
                        </div>
                        <span class="clear"></span>

                        <label class="field_title dot-2">Panjang</label>
                        <div class="form_input">
                          <select style="width:100%;" class="form-control" name="Panjang" id="Panjang" required oninvalid="this.setCustomValidity('Panjang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                            <?php 
                            if ($datapo->Panjang == "Standar") {
                              // echo "<option value='".$datapo->Panjang."' selected>".$datapo->Panjang."</option>";
                              ?>
                              <option value="-">-</option>
                               <option value="Standar" selected>Standar</option>
                            <option value="Besar">Besar</option>
                            <?php
                            }elseif($datapo->Panjang == "Besar"){
                              ?>
                              <option value="-">-</option>
                            <option value="Standar">Standar</option>
                            <option value="Besar" selected>Besar</option>
                              <?php
                            }elseif($datapo->Panjang == "-"){
                              ?>
                              <option value="-" selected>-</option>
                            <option value="Standar">Standar</option>
                            <option value="Besar">Besar</option>
                              <?php
                            }else{
                              ?>
                              <option value="-">-</option>
                              <option value="Standar">Standar</option>
                            <option value="Besar">Besar</option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                        <span class="clear"></span>
                      </div>
                      <div class="form_grid_6">
                        <label class="field_title dot-2">Point</label>
                        <div class="form_input">
                          <select style="width:100%;" class="form-control" name="Point" id="Point" required oninvalid="this.setCustomValidity('Point Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                            <?php 
                            if ($datapo->Point == "Terpisah") {
                              // echo "<option value='".$datapo->Point."' selected>".$datapo->Point."</option>";
                              ?>
                              <option value="-">-</option>
                               <option value="Terpisah" selected>Terpisah</option>
                            <option value="Ikut piece lain">Ikut Piece Lain</option>
                            <?php
                            }elseif($datapo->Point == "Ikut piece lain"){
                            ?>
                            <option value="-">-</option>
                             <option value="Terpisah">Terpisah</option>
                            <option value="Ikut piece lain" selected>Ikut Piece Lain</option>
                            <?php
                            }elseif($datapo->Point == "-"){
                            ?>
                            <option value="-" selected>-</option>
                             <option value="Terpisah">Terpisah</option>
                            <option value="Ikut piece lain">Ikut Piece Lain</option>
                            <?php
                            }else{
                              ?>
                              <option value="-">-</option>
                            <option value="Terpisah">Terpisah</option>
                            <option value="Ikut piece lain">Ikut Piece Lain</option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                        <span class="clear"></span>

                        <label class="field_title dot-2">Kirim Ke</label>
                        <div class="form_input">
                          <select style="width:100%;" class="form-control" name="Kirim" id="Kirim" required oninvalid="this.setCustomValidity('Kirim Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                             <?php 
                            if ($datapo->Kirim == "agen") {
                              // echo "<option value='".$datapo->Kirim."' selected>".$datapo->Kirim."</option>";
                              ?>
                              <option value="-">-</option>
                              <option value="agen" selected>agen</option>
                            <option value="Langsung Ke Pembeli">Langsung Ke Pembeli</option>
                            <option value="Gudang Titipan">Gudang Titipan</option>
                            <?php
                            }elseif($datapo->Kirim == "Langsung Ke Pembeli"){
                              ?>
                              <option value="-">-</option>
                              <option value="agen">agen</option>
                            <option value="Langsung Ke Pembeli" selected>Langsung Ke Pembeli</option>
                            <option value="Gudang Titipan">Gudang Titipan</option>
                              <?php
                            }elseif ($datapo->Kirim == "Gudang Titipan"){
                              ?>
                              <option value="-">-</option>
                              <option value="agen">agen</option>
                            <option value="Langsung Ke Pembeli">Langsung Ke Pembeli</option>
                            <option value="Gudang Titipan" selected>Gudang Titipan</option>
                            <?php
                            }else{
                              ?>
                              <option value="-">-</option>
                             <option value="agen">agen</option>
                            <option value="Langsung Ke Pembeli">Langsung Ke Pembeli</option>
                            <option value="Gudang Titipan">Gudang Titipan</option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>

                      </div>
                      <span class="clear"></span>
                    </div>
                  </li>
                </ul>
              </div>
              <!-- end tab & pane -->

              <!-- tab & pane 3 -->
              <div id="tab3">
                <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                  <div class="grid_12 w-100">
                    <span class="h_icon blocks_images"></span>
                    <h6>Cap Pinggir</h6>
                  </div>
                </div>

                <ul>
                  <li>
                    <div class="form_grid_12 multiline">
                      <div class="form_grid_6">
                        <label class="field_title">Cap Pinggir</label>
                        <div class="form_input">

                          <?php if ($datapo->Stamping == "Cap") { ?>
                          <input type="radio" class="radio" name="cap" id="cap" value="Cap" checked>
                          <label class="choice">Cap</label>
                          <?php } else { ?>
                          <input type="radio" class="radio" name="cap" id="cap" value="Cap">
                          <label class="choice">Cap</label>
                          <?php }
                          ?>

                          <?php if ($datapo->Stamping == "Tanpa Cap") { ?>
                          <input type="radio" class="radio" name="cap" id="tanpa_cap" value="Tanpa Cap" checked>
                          <label class="choice">Tanpa Cap</label>
                          <?php } else { ?>
                          <input type="radio" class="radio" name="cap" id="tanpa_cap" value="Tanpa Cap">
                          <label class="choice">Tanpa Cap</label>
                          <?php }
                          ?>


                        </div>
                        <span class="clear"></span>
                      </div>
                      <div class="form_grid_6">
                        <?php if ($datapo->Stamping == "Cap") { ?>
                        <?php if ($datapo->Posisi == "Selvedge") { ?>
                        <input type="checkbox" name="_cap1" id="_cap1" value="Selvedge" checked> Selvedge (Pinggir) <br>
                        <?php } else { ?>
                        <input type="checkbox" name="_cap1" id="_cap1" value="Selvedge" > Selvedge (Pinggir) <br>
                        <?php } ?>

                        <?php if ($datapo->Posisi1 == "Face") { ?>
                        <input type="checkbox" name="_cap2" id="_cap2" value="Face" checked> Face (Kepala) 
                        <?php }  else { ?>
                        <input type="checkbox" name="_cap2" id="_cap2" value="Face" > Face (Kepala)
                        <?php } ?>

                        <?php } else { ?>
                        <input type="checkbox" name="_cap1" id="_cap1" value="Selvedge" disabled> Selvedge (Pinggir) <br>
                        <input type="checkbox" name="_cap2" id="_cap2" value="Face" disabled> Face (Kepala)
                        <?php }?>



                      </div>
                      <span class="clear"></span>
                    </div>
                  </li>
                </ul>

                <div class="widget_top">
                  <div class="grid_12">
                    <span class="h_icon blocks_images"></span>
                    <h6>Keperluan Sample Sesuai Standar yang Berlaku</h6>
                  </div>
                </div>
                <ul>
                <li>
                  <div class="form_grid_12 alpha">
                    <div class="form_grid_6">
                      <label class="field_title dot-2">Album</label>
                      <div class="form_input mb-4">
                       <!--  <select style="width:100%;" class="chzn-select" name="Sample" id="Sample" required oninvalid="this.setCustomValidity('keperluan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <?php
                          //echo '<option value="'.$datapo->Sample.'" selected>'.$datapo->Sample.'</option>'
                          ?>
                          <option value="Album" selected>Album</option>
                          <option value="M10/20">M10/20</option>
                          <option value="Kain">Kain</option>
                          <option value="Lembaran">Lembaran</option>
                          <option value="Hanger">Hanger</option>
                        </select> -->
                        <input type="text" name="Sample" id="Sample" value="<?= $datapo->Album ?>">
                      </div>
                      <span class="clear"></span>
                    </div>

                     <div class="form_grid_6">
                      <label class="field_title dot-2">M10/20</label>
                      <div class="form_input mb-4">
                        <input type="text" name="M10" id="M10" value="<?= $datapo->M10 ?>">
                      </div>
                      <span class="clear"></span>
                    </div>

                     <div class="form_grid_6">
                      <label class="field_title dot-2">Kain</label>
                      <div class="form_input mb-4">
                        <input type="text" name="Kain" id="Kain" value="<?= $datapo->Kain ?>">
                      </div>
                      <span class="clear"></span>
                    </div>

                    <div class="form_grid_6">
                      <label class="field_title dot-2">Lembaran</label>
                      <div class="form_input mb-4">
                        <input type="text" name="Lembaran" id="Lembaran" value="<?= $datapo->Lembaran ?>">
                      </div>
                      <span class="clear"></span>
                    </div>
                    <span class="clear"></span>
                  </div>
                </li>
              </ul>
              </div>

              <div id="tab4">
                <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                  <div class="grid_12 w-100">
                    <span class="h_icon blocks_images"></span>
                    <h6>Catatan</h6>
                  </div>
                </div>
                <ul>
                  <li>

                    <div class="form_grid_12 multiline">
                      <div class="form_grid_12">
                        <label class="field_title dot-2">Catatan</label>
                        <div class="form_input">
                          <textarea name="Keterangan" id="Keterangan" cols="142" rows="10" tabindex="5" style="resize: none;"><?php echo $datapo->Keterangan ?></textarea>
                        </div>
                        <span class="clear"></span>
                      </div>

                      <span class="clear"></span>
                    </div>
                  </li>
                </ul>
                <div class="widget_top">
                  <div class="grid_12">
                    <span class="h_icon blocks_images"></span>
                    <h6>Untuk Keperluan PIC & diisi oleh SCH_SO</h6>
                  </div>
                </div>
                <ul>
                  <li>
                    <label class="field_title">Untuk Keperluan PIC & diisi oleh SCH_SO</label>
                    <div class="form_input">
                      <input type="checkbox" name="" id="" value="a"> A 
                      <input type="checkbox" name="" id="" value="b"> B 
                      <input type="checkbox" name="" id="" value="c"> C
                    </div>
                  </li>
                  <li>

                    <div class="form_grid_12">
                      <div class="text-right">
                        
                        <!-- <div class="btn_30_blue">
                          <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                        </div> -->
                        <div class="btn_30_blue">
                          <span><input name="simpan" id="simpan" onclick="save_change()" type="button" value="Simpan"></span>
                        </div>
                      </div>

                      <span class="clear"></span>
                    </div>
                    
                  </li>

                </ul>

              </div>

            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/po_celup.js"></script>
<script>
  $(document).ready(function(){
    renderJSONE($.parseJSON('<?php echo json_encode($datapodetail) ?>'));
        //console.log(<?php echo json_encode($datapodetail) ?>);
      });
    </script>
    <?php $this->load->view('administrator/footer') ; ?>