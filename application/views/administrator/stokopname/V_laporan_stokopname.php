<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Laporan Stok Opname</h3>
    <div class="top_search">

    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li> Laporan Stok Opname </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form action="<?php echo base_url() ?>Stokopname/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                                <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_9 mb-1">
                                        <div class="form_grid_3 mb-1">
                                            <label class="field_title mt-dot2">Periode</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_3 mb-1">
                                            <label class="field_title mt-dot2">s/d</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_3 mb-1">
                                            <select name="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13" readonly>
                                                <option value="Barcode">Barcode</option>
                                                <option value="Corak">Corak</option>
                                                <option value="Warna">Warna</option>
                                            </select>
                                        </div>
                                        <div class="form_grid_2 mb-1">
                                            <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form-control">
                                        </div>
                                        <div class="form_grid_1 mb-1">
                                            <div class="btn_24_blue">
                                                <input type="submit" name="" value="search">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clear"></div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="widget_content">
                    <table class="display" id="action_tbl">
                       <thead>
                                 <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Barcode</th>
                                    <th>Barang</th>
                                    <th>Corak</th>
                                    <th>Warna</th>
                                    <th>Gudang</th>
                                    <th>Qty Yard</th>
                                    <th>Qty Meter</th>
                                    <th>Grade</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php if(empty($stokopname)){
                                    ?>
                                    <?php
                                }else{
                                    $i=1;
                                    foreach ($stokopname as $data) {
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $data->tanggal; ?></td>
                                            <td><?php echo $data->barcode; ?></td>
                                            <td><?php echo $data->nama_barang; ?></td>
                                            <td><?php echo $data->corak ?></td>
                                            <td><?php echo $data->warna ?></td>
                                            <td><?php echo $data->gudang ?></td>
                                            <td><?php echo $data->qty_yard ?></td>
                                            <td><?php echo $data->qty_meter ?></td>
                                            <td><?php echo $data->grade ?></td>
                                            <td><?php echo $data->satuan ?></td>
                                            <td><?php echo $data->harga ?></td>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tr>
                            </tbody>
                        <tfoot>

                        </tfoot>
                        <!-- </table> -->
                    </table>

                </div>
                <div class="widget_content text-right px-2">
                    <div class="py-4 mx-2">
                        <div class="btn_24_blue">
                            <a href="#" type="button"><span>Kembali</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>