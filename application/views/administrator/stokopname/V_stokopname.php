<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Stok Opname</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content" style="min-height: 510px">
    <div class="grid_container">
        <div class="widget_wrap">
            <div class="breadCrumbHolder module">
                <div id="breadCrumb0" class="breadCrumb module white_lin">
                    <ul>
                        <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                        <li> Stop Opname </li>
                    </ul>
                </div>
            </div>
            <div class="widget_content">
                <form action="#" method="post" class="form_container left_label">
                    <ul>
                        <li class="body-search-data">

                            <!-- form search header -->
                            <div class="form_grid_12">
                                <label class="field_title">Barcode Scan</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" name="barcode" id="barcode" placeholder="Scan Barcode" onkeyup="this.value=this.value.replace(/[^0-9, A-Z]/g,'', '*')" Autofocus>
                                </div>

                                <div class="clear"></div>
                            </div>
                            <!-- end form search header -->

                        </li>
                    </ul>
                </form>
                <form action="#" method="post" class="form_container left_label" id="data_in">

                    <ul>
                        <li>
                            <div class="form_grid_12 w-100 alpha">

                                <!-- form 1 -->
                                <div class="form_grid_6">
                                    <label class="field_title pt mb">Barcode Scan</label>
                                    <div class="form_input">
                                        <span>: </span>
                                    </div>
                                </div>

                                <!-- form 2 -->
                                <div class="form_grid_6">
                                    <label class="field_title pt mb">Panjang Yard</label>
                                    <div class="form_input">
                                        <span>: </span>
                                    </div>
                                </div>

                                <div class="clear"></div>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12 w-100 alpha">

                                <!-- form 1 -->
                                <div class="form_grid_6">
                                    <label class="field_title pt mb">Barang</label>
                                    <div class="form_input">
                                        <span>: </span>
                                    </div>
                                </div>

                                <!-- form 2 -->
                                <div class="form_grid_6">
                                    <label class="field_title pt mb">Panjang Meter</label>
                                    <div class="form_input">
                                        <span>: </span>
                                    </div>
                                </div>

                                <div class="clear"></div>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12 w-100 alpha">

                                <!-- form 1 -->
                                <div class="form_grid_6">
                                    <label class="field_title pt mb">Custdes/Corak</label>
                                    <div class="form_input">
                                        <span>: </span>
                                    </div>
                                </div>

                                <!-- form 2 -->
                                <div class="form_grid_6">
                                    <label class="field_title pt mb">Grade</label>
                                    <div class="form_input">
                                        <span>: </span>
                                    </div>
                                </div>

                                <div class="clear"></div>
                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12 w-100 alpha">

                                <!-- form 1 -->
                                <div class="form_grid_6">
                                    <label class="field_title pt mb">Warna Cus</label>
                                    <div class="form_input">
                                        <span>: </span>
                                    </div>
                                </div>

                                <!-- form 2 -->
                                <div class="form_grid_6">
                                    <label class="field_title pt mb">Satuan</label>
                                    <div class="form_input">
                                        <span>: </span>
                                    </div>
                                </div>

                                <div class="clear"></div>
                            </div>
                        </li>
                        
                </form>
            </div>
        </div>

        <span class="clear"></span>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        var kosong;
        $(document).on('change', '#barcode', function (event) {
            event.preventDefault();
            var barcod = $("#barcode");
            var html = '';
            //console.log(barcod.val());
            if (barcod.val() == "SIMPAN") {
                if (kosong == undefined) {
                    html += '<h2 style="text-align:center">Data Kosong</h2>';
                    console.log('data kosong');
                } else {

                }
                kosong = undefined;
            } else {
                console.log(barcod.val());
                $.ajax({
                    url: "<?php echo base_url('Stokopname/getTampungan'); ?>",
                    type:"POST",
                    data:{barcode:barcod.val()},
                    dataType : "json",
                    success: function (data) {
                        console.log(data);
                        kosong = data;
                        if (data <= 0 || data === null) {
                            var audioElement = document.createElement('audio');
                            audioElement.setAttribute('src', 'upload/no.mp3');
                            audioElement.setAttribute('autoplay', 'autoplay');

                            html += '<br><br>'
                            html += '<h2 style="text-align:center; margin-top:0;">Data Tidak Tersedia</h2>'
                            html += '<form action="#" method="post" class="form_container left_label" id="data_in">'
                            html += '<div class="form_grid_6">'
                            html += '<ul>'
                            html += '<li>' +
                            '<label class="field_title">Barcode Scan</label>' +
                            '<div class="form_input">' +
                            '<span>: -</span>' +
                            '</div>' +
                            '</li>'
                            /*html += '<li>' +
                                '<label class="field_title">No SO</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                            html += '<li>' +
                                '<label class="field_title">Party</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                            html += '<li>' +
                                '<label class="field_title">Indent</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'*/
                                html += '<li>' +
                                '<label class="field_title">Corak</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                                html += '<li>' +
                                '<label class="field_title">Warna</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                                html += '<li>' +
                                '<label class="field_title">Barang</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                                html += '</ul>'
                                html += '</div>'
                                html += '<div class="form_grid_6">'
                                html += '<ul>'
                                html += '<li>' +
                                '<label class="field_title">Qty Yard</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                                html += '<li>' +
                                '<label class="field_title">Qty Meter</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                                html += '<li>' +
                                '<label class="field_title">Grade</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                            /**html += '<li>' +
                                '<label class="field_title">Lebar</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                            html += '<li>' +
                                '<label class="field_title">Remark</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'*/
                                html += '<li>' +
                                '<label class="field_title">Satuan</label>' +
                                '<div class="form_input">' +
                                '<span>: -</span>' +
                                '</div>' +
                                '</li>'
                                html += '</ul>'
                                html += '</div>'
                                html += '</form>'
                                kosong = undefined;
                            } else {
                                html += '<br><br><br>'
                                html += '<form action="#" method="post" class="form_container left_label" id="data_in">'
                                html += '<div class="form_grid_6">'
                                html += '<ul>'
                                html += '<li>' +
                                '<label class="field_title">Barcode Scan</label>' +
                                '<div class="form_input">' +
                                '<span>: '+ data[0]['Barcode'] +'</span>' +
                                '</div>' +
                                '</li>'
                            /*html += '<li>' +
                                        '<label class="field_title">No SO</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["NoSO"] +'</span>' +
                                        '</div>' +
                                    '</li>'
                            html += '<li>' +
                                        '<label class="field_title">Party</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Party"] +'</span>' +
                                        '</div>' +
                                    '</li>'
                            html += '<li>' +
                                        '<label class="field_title">Indent</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Indent"] +'</span>' +
                                        '</div>' +
                                        '</li>'*/
                                        html += '<li>' +
                                        '<label class="field_title">Corak</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Corak"] +'</span>' +
                                        '</div>' +
                                        '</li>'
                                        html += '<li>' +
                                        '<label class="field_title">Warna</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Warna"] +'</span>' +
                                        '</div>' +
                                        '</li>'
                                        html += '<li>' +
                                        '<label class="field_title">Barang</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Nama_Barang"] +'</span>' +
                                        '</div>' +
                                        '</li>'
                                        html += '</ul>'
                                        html += '</div>'
                                        html += '<div class="form_grid_6">'
                                        html += '<ul>'
                                        html += '<li>' +
                                        '<label class="field_title">Qty Yard</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Saldo_yard"] +'</span>' +
                                        '</div>' +
                                        '</li>'
                                        html += '<li>' +
                                        '<label class="field_title">Qty Meter</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Saldo_meter"] +'</span>' +
                                        '</div>' +
                                        '</li>'
                                        html += '<li>' +
                                        '<label class="field_title">Grade</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Grade"] +'</span>' +
                                        '</div>' +
                                        '</li>'
                            /*html += '<li>' +
                                        '<label class="field_title">Lebar</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Lebar"] +'</span>' +
                                        '</div>' +
                                    '</li>'
                            html += '<li>' +
                                        '<label class="field_title">Remark</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Remark"] +'</span>' +
                                        '</div>' +
                                        '</li>'*/
                                        html += '<li>' +
                                        '<label class="field_title">Satuan</label>' +
                                        '<div class="form_input">' +
                                        '<span>: '+ data[0]["Satuan"] +'</span>' +
                                        '</div>' +
                                        '</li>'
                                        html += '</ul>'
                                        html += '</div>'
                                        html += '</form>'

                            var Tanggal = kosong[0]['Tanggal'];
                            var Barcode_in = kosong[0]['Barcode'];
                            var Corak = kosong[0]['IDCorak'];
                            var Warna = kosong[0]['IDWarna'];
                            var Barang = kosong[0]['IDBarang'];
                            var Gudang = kosong[0]['IDGudang'];
                            var Qty_Yard = kosong[0]['Qty_yard'];
                            var Qty_Meter = kosong[0]['Qty_meter'];
                            var Grade_in = kosong[0]['Grade'];
                            var Harga = kosong[0]['Harga'];
                            var Satuan_in = kosong[0]['IDSatuan'];

                            console.log(kosong);
                            $.ajax({
                                url: "<?php echo base_url('Stokopname/input_data'); ?>",
                                type: "POST",
                                data: {
                                    Tanggal: Tanggal,
                                    Barcode: Barcode_in,
                                    Gudang : Gudang,
                                    Corak: Corak,
                                    Warna: Warna,
                                    Barang: Barang,
                                    Qty_yard: Qty_Yard,
                                    Qty_meter: Qty_Meter,
                                    Grade: Grade_in,
                                    Satuan: Satuan_in,
                                    Harga : Harga
                                },
                                dataType: "json",
                                success: function (a) {
                                    // console.log(a);
                                    if (a == "Proses Behasil, kasus data ada yang sama") {
//                                html += '<center><h2 style="margin-top:-48px">Data Sudah Diinputkan</h2></center>';
html += '<center><h2 style="margin-top:-36px">Data Sudah Diinputkan</h2></center>';

var audioElement = document.createElement('audio');
audioElement.setAttribute('src', 'upload/double.mp3');
audioElement.setAttribute('autoplay', 'autoplay');

} else {
    html += '<center><h2 style="margin-top:-36px">Data Berhasil Disimpan</h2></center>';
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', 'upload/ok.mp3');
    audioElement.setAttribute('autoplay', 'autoplay');
}
$('#data_in').html(html);
},
error: function (jqXHR, textStatus, errorThrown) {
    console.log('Error insert data from ajax');
}

});

                        }
                        $('#data_in').html(html);
                        $('#data_in2').html(html);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(barcod.val() + textStatus + ' ' + errorThrown);
                    }
                });
}
$('#data_in').html(html);
barcod.val('');
});
});
</script>

<?php $this->load->view('administrator/footer') ; ?>