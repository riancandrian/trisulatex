<?php $this->load->view('administrator/header') ; 

?>
<style type="text/css">
table.display td input{
  border: transparent !important;
  background: transparent !important;
  height: 15px !important;
  width: 100px;
}
table.display thead th, table.display td{
  white-space: nowrap;
}
.table-responsive{
  min-height: .01%;
  overflow-x: auto;
}
table.display tr.gradeA td{
  padding: 4px 10px !important
}
#content {
  min-height: 476px;
}
</style>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <form action="<?php echo base_url() ?>SettingLR/cari_bulan_aging_schedule" method="post">
  <br>
  <select name="bulan" class="form-control">
    <option value="">Pilih Bulan</option>
    <option value="1">Januari</option>
    <option value="2">Februari</option>
    <option value="3">Maret</option>
    <option value="4">April</option>
    <option value="5">Mei</option>
    <option value="6">Juni</option>
    <option value="7">Juli</option>
    <option value="8">Agustus</option>
    <option value="9">September</option>
    <option value="10">Oktober</option>
    <option value="11">November</option>
    <option value="12">Desember</option>
  </select><input type="submit" value="Submit"><br><br>
</form>

<div class="top_search">
</div>
</div>

<div id="content">
 <center><h1><i>Aging Schedule</i> Piutang Usaha</h1></center><br>
 <div class="grid_container">
   <div class="table-responsive" style="height: 400px;">
    <div class="grid_12 w-100 alpha">
      <div class="grid_9 mx">
        <?php
        if($this->input->post('bulan') != ""){

          if($bulan_ini=="1"){
            $bulan_sekarang="Januari";
          }elseif($bulan_ini=="2"){
            $bulan_sekarang="Februari";
          }elseif($bulan_ini=="3"){
            $bulan_sekarang="Maret";
          }elseif($bulan_ini=="4"){
            $bulan_sekarang="April";
          }elseif($bulan_ini=="5"){
            $bulan_sekarang="Mei";
          }elseif($bulan_ini=="6"){
            $bulan_sekarang="Juni";
          }elseif($bulan_ini=="7"){
            $bulan_sekarang="Juli";
          }elseif($bulan_ini=="8"){
            $bulan_sekarang="Agustus";
          }elseif($bulan_ini=="9"){
            $bulan_sekarang="September";
          }elseif($bulan_ini=="10"){
            $bulan_sekarang="Oktober";
          }elseif($bulan_ini=="11"){
            $bulan_sekarang="November";
          }elseif($bulan_ini=="12"){
            $bulan_sekarang="Desember";
          }
          ?>
          <table class="display">

            <thead style="font-size: 13px;">

              <tr style="position: absolute;z-index: 4; background: #f4f4f4;">
                <th style="width: 386px;">Keterangan</th>
                <th>Saldo Awal</th>
                <th>Penjualan</th>
                <th>Ret.</th>
                <th>Terima</th>
                <th>Saldo Akhir</th>
              </tr>
              <tr style="position: fixed; z-index:1; background: #f4f4f4;">
                <th style="width: 386px;">Keterangan</th>
                <th>Saldo Awal</th>
                <th>Penjualan</th>
                <th>Ret.</th>
                <th>Terima</th>
                <th>Saldo Akhir</th>
              </tr>

            </thead>

            <tbody style="font-size: 13px;margin-top: 15px;">
              <tr>
                <td colspan="6" style="height: 15px;"></td>
              </tr>
              <?php
              $n=1;
              foreach($set_lr as $lr)
              {
                if($n==1){
                  $total_saldo_awal = $lr->saldo_awal;
                  $total_penjualan = $lr->penjualan;
                  $total_retur = $lr->retur;
                  $total_bayar = $lr->bayar;
                  $total_saldo = $lr->saldo_akhir;
                  $n=2;
                }else{
                 $total_saldo_awal= $total_saldo_awal+$lr->saldo_awal;
                 $total_penjualan= $total_penjualan+$lr->penjualan;
                 $total_retur = $total_retur+$lr->retur;
                 $total_bayar = $total_bayar+$lr->bayar;
                 $total_saldo = $total_saldo+$lr->saldo_akhir;
               }
               ?>
               <tr style="border-bottom: 1px solid #000;">
                <td style="width: 386px;"><?php echo $lr->nama ?></td>
                <td style="width: 70px;"><?php echo convert_currency($lr->saldo_awal) ?></td>
                <td style="width: 62px;"><?php echo convert_currency($lr->penjualan) ?></td>
                <td style="width: 25px;"><?php echo convert_currency($lr->retur) ?></td>
                <td style="width: 42px;"><?php echo convert_currency($lr->bayar) ?></td>
                <td><?php echo convert_currency($lr->saldo_akhir) ?></td>
              </tr>
              <?php
            }
            ?>

            <tr style="font-size: 13px;border: 1px solid #000;">
              <th >TOTAL</th>
              <th class="text-right" style="border: 1px solid #000;padding: 9px 10px"><?php echo convert_currency($total_saldo_awal); ?></th>
              <th class="text-right" style="border: 1px solid #000;padding: 9px 10px"><?php echo convert_currency($total_penjualan); ?></th>
              <th class="text-right" style="border: 1px solid #000;padding: 9px 10px"><?php echo convert_currency($total_retur); ?></th>
              <th class="text-right" style="border: 1px solid #000;padding: 9px 10px"><?php echo convert_currency($total_bayar); ?></th>
              <!-- <th>Koreksi</th> -->
              <th class="text-right" style="border: 1px solid #000;padding: 9px 10px"><?php echo convert_currency($total_saldo); ?></th>
            </tr>
          </tbody>

        </table>
      </div>
      <input type="hidden" name="total30" id="total30" value="0">
      <input type="hidden" name="total60" id="total60" value="0">
      <input type="hidden" name="total90" id="total90" value="0">
      <input type="hidden" name="total120" id="total120" value="0">
      <input type="hidden" name="total125" id="total125" value="0">
      <div class="grid_3 mx">
        <table class="display">

          <thead style="font-size: 13px;">

            <tr style="position: absolute;z-index: 3; background: #f4f4f4;">
              <th style="padding: 8px 50px">< 30</th>
              <th style="padding: 8px 46px">31-60</th>
              <th style="padding: 8px 46px">61-90</th>
              <th style="padding: 8px 42.5px">91-120</th>
              <th style="padding: 8px 46.6px">120 <</th>
            </tr>
            <tr style="position: fixed; background: #f4f4f4;">
              <th style="padding: 8px 50px">< 30</th>
              <th style="padding: 8px 46px">31-60</th>
              <th style="padding: 8px 46px">61-90</th>
              <th style="padding: 8px 42.5px">91-120</th>
              <th style="padding: 8px 46.6px">120 <</th>
            </tr>

          </thead>

          <tbody style="font-size: 13px;">
            <tr>
              <td colspan="5" style="height: 15px;"></td>
            </tr>
            <?php if(empty($set_lr)){ ?>
            <?php }else{
              $i=1;
              $nama="";
              $harga = 0;

              foreach ($set_lr as $key => $data) {

                // echo "key2 : ".$data->Qty_yard."<br>";
                if(($key+1)<(count($set_lr))){
                  if($data->nama==$set_lr[$key+1]->nama){ 
                    $nama=$data->nama;
                    $harga += $data->saldo_akhir;
                    $munculin = false;
                  }else{
                    $munculin = true;

                  }
                  }else{ //jika di array berikutnya udah ga ada data maka langsung munculkan datanya
                    $munculin = true;

                  }

                  if($munculin){
                    $harga += $data->saldo_akhir;

                    ?>
                    <tr class="odd gradeA" style="border-bottom: 1px solid #000;">
                     <!--  <td class="text-center"><?php echo $i; ?></td>
                      <td><?php echo $data->nama; ?></td> -->

                      <?php
                          foreach($set_lr_tanggal as $lr_tangal){ //untuk get tanggal aja
                            $adadata = false;
                            if($lr_tangal->nama==$data->nama){
                              $adadata = true;
                              date_default_timezone_set('Asia/Jakarta');
                              $tgl1 = date('d-m-Y');
                              $tgl2 = $lr_tangal->tanggal;
                              if($tgl2=="" || $tgl2==NULL){
                                $hari= 0;
                              }else{
                                $selisih = strtotime($tgl1) -  strtotime($tgl2);
                                $hari = $selisih/(60*60*24);
                              }
                              // echo $hari ."lllll <br>";

                              if($hari >= 0 && $hari <= 30)
                              {
                                ?>
                                <td id="rupiah1<?php echo $i ?>"><input type="text" id="hrupiah1<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }else{
                                ?>
                                <td id="rupiah1<?php echo $i ?>"><input type="text" id="hrupiah1<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }
                              ?>
                              <?php
                              if($hari >= 31 && $hari <= 60)
                              {
                                ?>
                                <td id="rupiah2<?php echo $i ?>"><input type="text" id="hrupiah2<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }else{
                                ?>
                                <td id="rupiah2<?php echo $i ?>"><input type="text" id="hrupiah2<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }
                              ?>
                              <?php
                              if($hari >= 61 && $hari <=90)
                              {
                                ?>
                                <td id="rupiah3<?php echo $i ?>"><input type="text" id="hrupiah3<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }else{
                                ?>
                                <td id="rupiah3<?php echo $i ?>"><input type="text" id="hrupiah3<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }
                              ?>
                              <?php
                              if($hari >= 91 && $hari <=120)
                              {
                                ?>
                                <td id="rupiah4<?php echo $i ?>"><input type="text" id="hrupiah4<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }else{
                                ?>
                                <td id="rupiah4<?php echo $i ?>"><input type="text" id="hrupiah4<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }
                              ?>
                              <?php
                              if($hari > 120)
                              {
                                ?>
                                <td id="rupiah5<?php echo $i ?>"><input type="text" id="hrupiah5<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }else{
                                ?>
                                <td id="rupiah5<?php echo $i ?>"><input type="text" id="hrupiah5<?php echo $i ?>" value="0" disabled></td>
                                <?php
                              }
                              ?>
                              <?php
                            }
                            if($adadata){
                              break;
                            }
                          }

                          foreach($set_lr_tanggal as $key=>$lr_tangals){
                            if($lr_tangals->nama==$data->nama){
                              date_default_timezone_set('Asia/Jakarta');
                              $tgl1 = date('d-m-Y');
                              $tgl2 = $lr_tangals->tanggal;
                              if($tgl2=="" || $tgl2==NULL){
                                $hari= 0;
                              }else{
                                $selisih = strtotime($tgl1) -  strtotime($tgl2);
                                $hari = $selisih/(60*60*24);
                              }
                              // echo $hari . "<br>";
                              ?>
                              <script type="text/javascript">

                                jsfunction2(<?php echo $key ?>, <?php echo $hari ?>, <?php echo $lr_tangals->saldo_akhir ?>,<?php echo $i ?>);
                                function jsfunction2(key, hari, harga, inc)
                                {
                                  if(hari >= 0 && hari <= 30){
                                    $('#hrupiah1'+inc).val(parseInt($('#hrupiah1'+inc).val()) + parseInt(harga));
                                    total1= parseInt(total1)+parseInt($('#hrupiah1'+inc).val()) + parseInt(harga);
                                    $("#total30").val(total1);
                                  }

                                  if(hari>=31 && hari<=60){
                                    $('#hrupiah2'+inc).val(parseInt($('#hrupiah2'+inc).val()) + parseInt(harga));
                                    total2= parseInt(total2)+parseInt($('#hrupiah2'+inc).val()) + parseInt(harga);
                                    $("#total60").val(total2);
                                  }

                                  if(hari>=61 && hari<=90){
                                    $('#hrupiah3'+inc).val(parseInt($('#hrupiah3'+inc).val()) + parseInt(harga));
                                    total3= parseInt(total3)+parseInt($('#hrupiah3'+inc).val()) + parseInt(harga);
                                    $("#total90").val(total3);
                                  }

                                  if(hari>=91 && hari<=120){
                                    $('#hrupiah4'+inc).val(parseInt($('#hrupiah4'+inc).val()) + parseInt(harga));
                                    total4= parseInt(total4)+parseInt($('#hrupiah4'+inc).val()) + parseInt(harga);
                                    $("#total120").val(total4);
                                  }

                                  if(hari>120){
                                    $('#hrupiah5'+inc).val(parseInt($('#hrupiah5'+inc).val()) + parseInt(harga));
                                    total5= parseInt(total5)+parseInt($('#hrupiah5'+inc).val()) + parseInt(harga);
                                    $("#total125").val(total5);
                                  }
                                  // $('#hyard6'+inc).val(parseInt($('#hyard6'+inc).val()) + parseInt(harga));
                                }
                              </script>
                              <?php
                            }
                          }
                          ?>

                        </tr>
                        <?php
                        $i++;
                      }

                    }
                  }
                  ?>


                  <?php
                }
                ?>
                <tr style="font-size: 13px;border: 1px solid #000;">
                  <td class="text-right" style="border: 1px solid #000;padding: 5px 10px"><input type="text" class="text-right" name="total30a" id="total30a" value="0" readonly></td>
                  <td class="text-right" style="border: 1px solid #000;padding: 5px 10px"><input type="text" class="text-right" name="total60a" id="total60a" value="0" readonly></td>
                  <td class="text-right" style="border: 1px solid #000;padding: 5px 10px"><input type="text" class="text-right" name="total90a" id="total90a" value="0" readonly></td>
                  <td class="text-right" style="border: 1px solid #000;padding: 5px 10px"><input type="text" class="text-right" name="total120a" id="total120a" value="0" readonly></td>
                  <!-- <th>Koreksi</th> -->
                  <td class="text-right" style="border: 1px solid #000;padding: 5px 10px"><input type="text" class="text-right" name="total125a" id="total125a" value="0" readonly></td>
                </tr>
              </tbody>

            </table>
            <script type="text/javascript">
              tt= $("#total30").val();
              $("#total30a").val(tt);

              tt2= $("#total60").val();
              $("#total60a").val(tt2);

              tt3= $("#total90").val();
              $("#total90a").val(tt3);

              tt4= $("#total120").val();
              $("#total120a").val(tt4);

              tt5= $("#total125").val();
              $("#total125a").val(tt5);
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('administrator/footer') ; ?>