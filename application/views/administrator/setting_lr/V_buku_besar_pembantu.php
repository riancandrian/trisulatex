<?php $this->load->view('administrator/header') ; 

?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
   <form action="<?php echo base_url() ?>SettingLR/cari_bulan_buku_besar_pembantu" method="post">
    <br>
    <select name="bulan" class="form-control">
      <option value="">Pilih Bulan</option>
      <option value="1">Januari</option>
      <option value="2">Februari</option>
      <option value="3">Maret</option>
      <option value="4">April</option>
      <option value="5">Mei</option>
      <option value="6">Juni</option>
      <option value="7">Juli</option>
      <option value="8">Agustus</option>
      <option value="9">September</option>
      <option value="10">Oktober</option>
      <option value="11">November</option>
      <option value="12">Desember</option>
    </select><input type="submit" value="Submit"><br><br>
  </form>

 <div class="top_search">
 </div>
</div>

<div id="content">
     <center><h1>Buku Besar</h1></center><br>
<div class="grid_container">
    <div class="table-responsive">
  <?php
  if($this->input->post('bulan') != ""){

    if($bulan_ini=="1"){
        $bulan_sekarang="Januari";
    }elseif($bulan_ini=="2"){
        $bulan_sekarang="Februari";
    }elseif($bulan_ini=="3"){
        $bulan_sekarang="Maret";
    }elseif($bulan_ini=="4"){
        $bulan_sekarang="April";
    }elseif($bulan_ini=="5"){
        $bulan_sekarang="Mei";
    }elseif($bulan_ini=="6"){
        $bulan_sekarang="Juni";
    }elseif($bulan_ini=="7"){
        $bulan_sekarang="Juli";
    }elseif($bulan_ini=="8"){
        $bulan_sekarang="Agustus";
    }elseif($bulan_ini=="9"){
        $bulan_sekarang="September";
    }elseif($bulan_ini=="10"){
        $bulan_sekarang="Oktober";
    }elseif($bulan_ini=="11"){
        $bulan_sekarang="November";
    }elseif($bulan_ini=="12"){
        $bulan_sekarang="Desember";
    }
  ?>
   <table class="display">

      <thead style="font-size: 13px;">

        <tr>

          <th>Kode COA</th>
          <th>Nama COA</th>
          <th>Saldo Awal</th>
          <th>Debit</th>
          <th>Kredit</th>
          <th>Saldo Akhir</th>
        </tr>

      </thead>

      <tbody style="font-size: 13px">
       <?php
       foreach($set_lr as $lr)
       {
        ?>
        <tr style="border-bottom: 1px solid #000;">
            <td><?php echo $lr->kode_coa ?></td>
          <td><?php echo $lr->nama_coa ?></td>
          <td><?php echo convert_currency($lr->saldo_awal) ?></td>
          <td><?php echo convert_currency($lr->debit) ?></td>
          <td><?php echo convert_currency($lr->kredit) ?></td>
          <td><?php echo convert_currency($lr->saldo_akhir) ?></td>
          </tr>
          <?php
        }
        ?>
      </tr>
    </tbody>

  </table>
  <?php
}
?>

</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>