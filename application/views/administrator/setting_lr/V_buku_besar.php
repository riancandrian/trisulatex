<?php $this->load->view('administrator/header') ; 

?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <form action="<?php echo base_url() ?>SettingLR/cari_bulan_buku_besar" method="post">
  <br>
  <select name="coa" class="form-control">
    <option value="">Pilih COA</option>
    <?php

    foreach ($getcoa as $getcoas) {
      if ($coa1==$getcoas->IDCOA) {
        ?>
        <option value="<?php echo $getcoas->IDCOA ?>" selected><?php echo $getcoas->Nama_COA; ?></option>
        <?php
      } else {
        ?>
        <option value="<?php echo $getcoas->IDCOA ?>"><?php echo $getcoas->Kode_COA ?> <?php echo $getcoas->Nama_COA; ?></option>
        <?php
      }
    }
    ?>
  </select>
  <select name="bulan" class="form-control">
    <option value="">Pilih Bulan</option>
    <option value="1">Januari</option>
    <option value="2">Februari</option>
    <option value="3">Maret</option>
    <option value="4">April</option>
    <option value="5">Mei</option>
    <option value="6">Juni</option>
    <option value="7">Juli</option>
    <option value="8">Agustus</option>
    <option value="9">September</option>
    <option value="10">Oktober</option>
    <option value="11">November</option>
    <option value="12">Desember</option>
  </select>
  <input type="submit" value="Submit"><br><br>
</form>

<div class="top_search">
</div>
</div>

<div id="content">
  <?php if($this->input->post('coa') == ""){ ?>
  <center><h1>General Ledger</h1></center><br>
  <?php }else{
    ?>
    <center><h1>
      <?php echo $getcoatampil->Nama_COA; ?><br><br>
    </h1></center>
    <?php
  } ?>
  <div class="grid_container">
    <div class="table-responsive">
      <?php
      if($this->input->post('coa') != ""){
        ?>
        <table class="display">

          <thead style="font-size: 13px;">

            <tr>
              <th>Tanggal</th>
              <th>Nomor</th>
              <th>Keterangan</th>
              <!-- <th>Saldo Awal</th> -->
              <th>Debit</th>
              <th>Kredit</th>
              <th>Saldo Akhir</th>
            </tr>

          </thead>

          <tbody style="font-size: 13px">
           <?php
           $i=1;
           $n=1;
           foreach($set_lr as $lr)
           {
            if($n==1){
              $Saldo_akhir = $lr->debet-$lr->kredit;
              $n=2;
            }else{
             $Saldo_akhir= $Saldo_akhir+$lr->debet-$lr->kredit;
           }
           ?>
           <tr style="border-bottom: 1px solid #000;">
            <td><?php echo $lr->tanggal ?></td>
            <td><?php echo $lr->nomor ?></td>
            <td><?php echo $lr->keterangan ?></td>
            <!-- <td><?php //echo convert_currency($lr->saldo_awal) ?></td> -->
            <td><?php
            if($lr->debet < 0){
              echo "(".convert_currency(ltrim($lr->debet,"-")).")";
            }else{
             echo convert_currency($lr->debet);
           }
           ?>
         </td>
         <td><?php
            if($lr->kredit < 0){
              echo "(".convert_currency(ltrim($lr->kredit,"-")).")";
            }else{
             echo convert_currency($lr->kredit);
           }
           ?></td>
         <td>
          <?php 
          if($Saldo_akhir < 0){
            echo "(".convert_currency(ltrim($Saldo_akhir,"-")).")";
          }else{
            echo convert_currency($Saldo_akhir);
          } ?>
        </td>
      </tr>
      <?php
    }
    ?>
  </tr>
</tbody>

</table>
<?php
}
?>

</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>