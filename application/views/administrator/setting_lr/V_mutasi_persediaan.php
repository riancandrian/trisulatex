<?php $this->load->view('administrator/header') ; 

?>
<style type="text/css">
table.display td input{
  border: transparent !important;
  background: transparent !important;
}
</style>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>

<div class="top_search">
</div>
</div>

<div id="content">
  <form action="<?php echo base_url() ?>SettingLR/cari_tanggal_mutasi_persediaan" method="post">
  <br>
  <input type="date" name="tanggal_mulai" class="form-control" required>
  <input type="date" name="tanggal_selesai" class="form-control" required>
  <input type="submit" value="Submit"><br><br>
</form>
 <center><h1>Mutasi Persediaan</h1></center><br>
 <div class="grid_container">
  <div class="table-responsive">
    <?php
     if($this->input->post('tanggal_mulai') != "" && $this->input->post('tanggal_selesai') != ""){
      ?>
      <table class="display">

        <thead style="font-size: 13px;">

          <tr>
            <th colspan="4">Art No</th>
            <th rowspan="2">Satuan</th>
            <th rowspan="2">Harga Satuan</th>
            <th colspan="2">Persediaan Awal</th>
            <th colspan="2">Beli (+)</th>
            <th colspan="2">Retur Beli (-)</th>
            <th colspan="2">Koreksi (+)</th>
            <th colspan="2">Koreksi (-)</th>
            <th colspan="2">HPP/Jual (-)</th>
            <th colspan="2">Sample (-)</th>
            <th colspan="2">Retur Penjualan (+)</th>
            <th colspan="2">Persediaan Akhir</th>
          </tr>
          <tr>
          <th>Corak</th>
          <th>Grade</th>
          <th>Warna</th>
          <th>Merk</th>
          <th>Qty</th>
          <th>Nilai</th>
          <th>Qty</th>
          <th>Nilai</th>
          <th>Qty</th>
          <th>Nilai</th>
          <th>Qty</th>
          <th>Nilai</th>
          <th>Qty</th>
          <th>Nilai</th>
          <th>Qty</th>
          <th>Nilai</th>
          <th>Qty</th>
          <th>Nilai</th>
          <th>Qty</th>
          <th>Nilai</th>
          <th>Qty</th>
          <th>Nilai</th>
        </tr>

        </thead>

        <tbody style="font-size: 13px">
       <?php
         foreach($set_lr as $lr)
         {
          ?>
          <tr style="border-bottom: 1px solid #000;">
            <td><?php echo $lr->corak ?></td>
            <td><?php echo $lr->grade ?></td>
            <td><?php echo $lr->warna ?></td>
            <td><?php echo $lr->merk ?></td>
            <td><?php echo $lr->satuan ?></td>
            <td><?php echo $lr->harga ?></td>
            <td><?php echo convert_currency($lr->persediaan_awal_qty) ?></td>
            <td><?php echo convert_currency($lr->persediaan_awal_nilai) ?></td>
            <td><?php echo convert_currency($lr->beli_qty) ?></td>
            <td><?php echo convert_currency($lr->beli_nilai) ?></td>
            <td><?php echo convert_currency($lr->retur_beli_qty) ?></td>
            <td><?php echo convert_currency($lr->retur_beli_nilai) ?></td>
            <td><?php echo convert_currency($lr->koreksi_qty) ?></td>
            <td><?php echo convert_currency($lr->koreksi_nilai) ?></td>
            <td><?php echo convert_currency($lr->koreksi_scan_qty) ?></td>
            <td><?php echo convert_currency($lr->koreksi_scan_nilai) ?></td>
            <td><?php echo convert_currency($lr->hpp_qty) ?></td>
            <td><?php echo convert_currency($lr->hpp_nilai) ?></td>
            <td><?php echo convert_currency($lr->sample_qty) ?></td>
            <td><?php echo convert_currency($lr->sample_nilai) ?></td>
            <td><?php echo convert_currency($lr->retur_jual_qty) ?></td>
            <td><?php echo convert_currency($lr->retur_jual_nilai) ?></td>
            <td><?php echo convert_currency($lr->persediaan_akhir_qty) ?></td>
            <td><?php echo convert_currency($lr->persediaan_akhir_nilai) ?></td>
          </tr>
          <?php
        }
        ?>
      </tr>
    </tbody>

  </table>

<?php
}
?>

</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>