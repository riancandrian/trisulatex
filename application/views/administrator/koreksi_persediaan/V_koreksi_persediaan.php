<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Koreksi Persediaan</h3>
 <div class="top_search">

 </div>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li> Koreksi Persediaan </li>
            </ul>
          </div>
        </div>
        <?php
        $hari_ini = date("Y-m-d");
        $tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
        ?>
        <div class="widget_content">
          <form action="<?php echo base_url() ?>KoreksiPersediaan/pencarian" method="post" class="form_container left_label">
            <ul>
              <li class="px-1">
                <div class="form_grid_12 w-100 alpha">
                  <div class="form_grid_3 mb-1">

                    <div class="btn_24_blue">
                      <a href="<?php echo base_url('KoreksiPersediaan/tambah_koreksi_persediaan')?>"><span>Tambah Data</span></a>
                    </div>
                  </div>
                  <div class="form_grid_9 mb-1">
                    <div class="form_grid_3 mb-1">                        
                      <label class="field_title mt-dot2">Periode</label>
                      <div class="form_input">
                        <input type="date" class="input-date-padding date-picker hidden-sm-down" name="date_from" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>
                    <div class="form_grid_3 mb-1">                        
                      <label class="field_title mt-dot2">s/d</label>
                      <div class="form_input">
                        <input type="date" class="input-date-padding date-picker hidden-sm-down" name="date_until" value="<?php echo $tgl_terakhir ?>">
                      </div>
                    </div>
                    <div class="form_grid_3 mb-1">
                      <select name="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13" readonly>
                        <option value="Nomor">No Koreksi Persediaan</option>
                      </select>
                    </div>
                    <div class="form_grid_2 mb-1">
                      <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form-control">
                    </div>
                    <div class="form_grid_1 mb-1">
                      <div class="btn_24_blue">
                        <input type="submit" name="" value="search">
                      </div>
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>
              </li>
            </ul>
          </form>
        </div>
        <form action="<?php echo base_url() ?>KoreksiPersediaan/delete_multiple" method="post" id="ubahstatus">
          <div class="widget_content">

            <table class="display" id="action_tbl">
              <!-- <table class="display data_tbl"> -->
                <thead>
                  <tr>
                    <th class="center">
                      <input name="checkbox" type="checkbox" value="" class="checkall"> Do
                    </th>
                    <th>No</th>
                    <th>No KP</th>
                    <th>Tanggal</th>
                    <th>Corak</th>
                    <th>Qty Yard </th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(empty($persediaan)){ ?>
                  <?php }else{
                    $i=1;
                    foreach ($persediaan as $data) {
                      ?>
                      <tr class="odd gradeA">
                        <td class="center tr_select">
                          <input name="msg[]" type="checkbox" value="<?php echo $data->IDKP ?>">
                        </td>
                        <td class="text-center"><?php echo $i; ?></td>
                        <td><?php echo $data->Nomor; ?></td>
                        <td><?php echo $data->Tanggal; ?></td>
                        <td><?php echo $data->Corak; ?></td>
                        <td><?php echo $data->Qty_yard; ?></td>
                        <td class="text-center ukuran-logo">
                          <span><a class="action-icons c-Detail" href="<?php echo base_url('KoreksiPersediaan/show/'.$data->IDKP); ?>" title="Detail Data"> Detail</a></span>
                          <?php if($data->Batal=="aktif"){ ?>
                          <span><a class="action-icons c-sa" href="<?php echo base_url() ?>KoreksiPersediaan/status_gagal/<?php echo $data->IDKP;?>" title="Status Tidak Aktif">Status</a></span>
                          <?php }elseif($data->Batal=="tidak aktif"){ ?>
                          <span><a class="action-icons c-approve" href="<?php echo base_url() ?>KoreksiPersediaan/status_berhasil/<?php echo $data->IDKP;?>" title="Status Aktif">Status</a></span>
                          <?php } ?>

                          <span><a class="action-icons c-edit" href="<?php echo base_url('KoreksiPersediaan/edit/'.$data->IDKP); ?>" title="Ubah Data"> Edit</a></span>

                        </td>
                      </tr>
                      <?php
                      $i++;
                    }
                  }
                  ?>

                </tbody>
                <tfoot>

                </tfoot>
                <!-- </table> -->
              </table>

            </div>

            <div class="widget_content text-right px-2">
              <div class="py-4 mx-2">

                <div class="btn_24_blue">
                  <a href="#" type="button" onclick="document.getElementById('ubahstatus').submit();"><span>Batal</span></a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('administrator/footer') ; ?>
