<?php $this->load->view('administrator/header') ; 
?>
<div class="page_title">
    <div id="alert"></div>
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Koreksi Persediaan</h3>

    <div class="top_search">
    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>KoreksiPersediaan">Data Koreksi Persediaan</a></li>
                            <li class="active">Ubah Koreksi Persediaan</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Ubah Data Koreksi Persediaan</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="" class="form_container left_label">
                        <ul>
                            <li class="body-search-data">
                                    <div class="form_grid_12 w-100 alpha">
                                    <input type="hidden" name="IDKP" id="IDKP" value="<?php echo $koreksi->IDKP ?>">
                                        <div class="form_grid_8">
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Tanggal</label>
                                                <div class="form_input">
                                                    <input type="date" class="form-control input-date-padding date-picker hidden-sm-down" name="tanggal" id="tanggal" value="<?php echo $koreksi->Tanggal ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>

                                    
                                          
                                            
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Total Yard</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="total_yard" id="total_yard" readonly>
                                                </div>
                                            </div>

                                              <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Nomor KP</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="no_kp" id="no_kp" value="<?php echo $koreksi->Nomor; ?>" readonly>
                                                </div>
                                            </div>
                                             <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Jenis</label>
                                                <div class="form_input">
                                                    <select data-placeholder="Cari Jenis" style="width:100%;" class="chzn-select" tabindex="13" name="jenis" id="jenis" required oninvalid="this.setCustomValidity('Jenis Tidak Boleh Kosong')">
                                                           <!-- <option value='minus'>Minus (-)</option> -->
                                                           <option value='plus'>Plus (+)</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Total Meter</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="total_meter" id="total_meter" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form_grid_4 mb-1">
                                            <label class="field_title mt-dot2">Keterangan</label>
                                            <div class="form_input">
                                                <textarea class="form-control" name="keterangan" id="keterangan" style="resize: none;" rows="9"><?php echo $koreksi->Keterangan ?></textarea>
                                            </div>
                                        </div>
                                        <span class="clear"></span>
                                    </div>
                                </li>

                                <li>
                                <div class="form_grid_12 w-100 alpha">

                                    <!-- form 1 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2">Barcode</label>
                                        <div class="form_input">
                                          <input type="text" class="form-control" name="barcode" placeholder="Barcode" id="barcode" required oninvalid="this.setCustomValidity('Barcode Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                    <!-- form 2 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title">Grade</label>
                                        <div class="form_input">
                                            <select data-placeholder="Cari Grade" style=" width:100%;" class="chzn-select" tabindex="13"  name="grade" id="grade" required oninvalid="this.setCustomValidity('Grade Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="S">S</option>
                                                <option value="E">E</option>
                                            </select>
                                        </div>
                                    </div>
                                    <span class="clear"></span>

                                    <!-- form 3 -->
                                    <div class="form_grid_6 mb-1">
                                <label class="field_title">Corak</label>
                                <div class="form_input">
                                    <select style="width:100%;" class="chzn-select" name="corak" id="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <option value="" selected>--Pilih Corak--</option>
                                      <?php foreach($corak as $row) {
                                        echo "<option value='$row->IDCorak'>$row->Corak</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                                    <!-- form 4 -->
                                    <div class="form_grid_6 mb-1">

                                        <label class="field_title pt">Qty Yard</label>
                                        <div class="form_input">
                                            <input type="text" id="qty_yard" name="qty_yard" class="form-control" placeholder="Qty yard" required oninvalid="this.setCustomValidity('Qty Yard Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="hilang_huruf(this.value, 'C')">
                                          
                                        </div>
                                    </div>
                                      <span class="clear"></span>
                                    
                                    <!-- form 5 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title">Warna</label>
                                        <div class="form_input">
                                            <select name="warna" id="warna" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                <option value="">--Silahkan Pilih Corak Dahulu--</option>
                                                 </select> 
                                        </div>
                                    </div>

                                    <!-- form 6 -->
                                   <div class="form_grid_6 mb-1">

                                        <label class="field_title pt">Qty Meter</label>
                                        <div class="form_input">
                                            <input type="text" id="qty_meter" name="qty_meter" class="form-control" placeholder="Qty yard" required oninvalid="this.setCustomValidity('Qty Yard Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="hilang_huruf2(this.value, 'F')">
                                          
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>

                                <!-- form 7 -->
                               <div class="form_grid_6 mb-1">
                                        <label class="field_title">Merk</label>
                                        <div class="form_input">
                                            <input type="text" id="merk" class="form-control" data-idmerk data-idbarang name="merk" placeholder="Merk" required oninvalid="this.setCustomValidity('Merk Perolehan Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                                        </div>
                                    </div>
                                <div class="form_grid_6 mb-1">
                                        <label class="field_title">Harga</label>
                                        <div class="form_input">
                                            <input type="text" id="harga" class="form-control" name="harga" placeholder="Harga" required oninvalid="this.setCustomValidity('Harga Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                                        </div>
                                    </div>
             
                                
                            </li>
                            <br><br>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input ml text-center w-100">
                                        <!--   <div class="btn_30_light">
                                            <span> <a href="<?php //echo base_url() ?>Piutang" name="simpan">Kembali</a></span>
                                        </div> -->
                                        <button class="btn_24_blue" type="button" id="add-to-cart-edit">
                                            Tambah Data
                                        </button>


                                    </div>
                                </div>
                            </li>
                            
                        </ul>
                        <div>
                            <table class="display">
                                <thead>
                                    <tr>
                                        <th class="center" style="width: 40px">No</th>
                                        <th>Barcode</th>
                                        <th class="hidden-xs">Corak</th>
                                        <th>Warna</th>
                                        <th>Grade</th>
                                        <th>Qty Yard</th>
                                        <th>Qty Meter</th>
                                        <th>Harga</th>
                                        <th style="width: 50px">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tabel-cart-asset">
                                    
                                </tbody>
                            </table>
                           
                        </div>


                    </form>
                    <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2">
                           <!--  <div class="btn_30_blue">
                          <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                      </div> -->
                      
                      <div class="btn_30_blue">
                        <span><a class="btn_24_blue" id="simpan" style="cursor: pointer;" onclick="savePersediaanChange()">Simpan</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/koreksi_persediaan.js"></script>
<script>
      
    $(document).ready(function(){
        renderJSONE($.parseJSON('<?php echo json_encode($detail) ?>'));
    });
</script>
<script>
   function hilang_huruf(value, degree)
{
  // this.value=this.value.replace(/[^0-9.]/,'');
  document.getElementById("qty_yard").value=value.replace(/[^0-9.]/,'');
    var x;
    if (degree == "C") {
        x = document.getElementById("qty_yard").value * 0.9144
        document.getElementById("qty_meter").value = x.toFixed(2);
    } else {
        x = (document.getElementById("qty_meter").value * 1.09361);
        document.getElementById("qty_yard").value = x.toFixed(2);
    }
}

function hilang_huruf2(value, degree)
{
  // this.value=this.value.replace(/[^0-9.]/,'');
  document.getElementById("qty_meter").value=value.replace(/[^0-9.]/,'');
    var x;
    if (degree == "C") {
        x = document.getElementById("qty_yard").value * 0.9144
        document.getElementById("qty_meter").value = x.toFixed(2);
    } else {
        x = (document.getElementById("qty_meter").value * 1.09361);
        document.getElementById("qty_yard").value = x.toFixed(2);
    }
}
</script>
<?php $this->load->view('administrator/footer') ; ?>