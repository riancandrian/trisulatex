<?php $this->load->view('administrator/header') ; 

?>
<style>
.bg-blue{
  background: -webkit-linear-gradient(top, #ffffff 0%,#ededed 40%,#d4d4d4 80%);
  /* color: #fff; */
  height: 15px;
  display: block;
  padding: 10px 0;
}
.btn-min, .btn-plus{
  padding: 5px;
  background: -webkit-linear-gradient(top, #ffffff 0%,#ededed 40%,#d4d4d4 80%);
  border: 1px solid #d4d4d4;
  font-weight: bold;
  font-size: 15px;
  display: inline-block;
  width: 9px;
}
.btn-min{
  color: red;
}
.btn-plus{
  color: blue;
}

</style>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Setting PL</h3>

 <div class="top_search">
 </div>
</div>

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons"  href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>

              <li> Setting PL </li>
            </ul>
          </div>
        </div>

        <div class="widget_content">
          <form action="<?php echo base_url() ?>SettingLR/simpan_pl" method="post" class="form_container left_label">
            <ul>
              <li class="px">
                <div class="form_grid_12 multiline mx">
                  <div class="form_grid_9 mx">
                    <div class="bg-blue">
                      <div class="form_grid_12">
                        <div class="form_grid_1">
                          aksi  
                          <?php $j = 98;?>
                          <a class="btn-plus" style="cursor: pointer" onclick="addRow('<?php echo $j ?>')">+</a>
                        </div>
                        <div class="form_grid_5">
                          COA
                        </div>
                        <div class="form_grid_2">
                          Keterangan
                        </div>
                        <div class="form_grid_1">
                          Kategori
                        </div>
                        <div class="form_grid_2">
                          Perhitungan
                        </div>
                        <div class="form_grid_1">
                          Group
                        </div>
                      </div>
                    </div>
                    <?php $i = 99;?>

                    <?php if ($edit_sub == null): ?>

                    <?php else: 
                    for ($k=98; $k >= 1; $k--) { 
                      ?>
                      <div id="tambahbaris<?php echo $k ?>"></div>
                      <?php
                    }
                    ?>

                    <?php foreach ($edit_sub as $data): ?>

                      <?php $i += 1;?>
                      <div class="form_grid_12">
                        <div id="hilangbaris<?php echo $i ?>">
                          <div class="form_grid_1 mt-1 text-center">
                            <span>
                              <a class="btn-min" style="cursor: pointer" onclick="removeRowUtama('<?php echo $i ?>')">-</a>
                              <a class="btn-plus" style="cursor: pointer" onclick="addRow('<?php echo $i ?>')">+</a>

                            </span>
                          </div>
                          <!--   -->
                          <div class="form_grid_5">
                            <select  id="coa" name="coa[]" class="form-control mt-1" style="width: 100%">
                              <?php foreach($coa as $coas) { ?>
                              <option value='<?php echo $coas->IDCoa ?>' <?php echo $coas->IDCoa == $data->IDCoa ? 'selected' : ''; ?>>
                                <?php echo $coas->Nama_COA?> 
                              </option>
                              <?php } ?>
                            </select>
                          </div>

                          <div class="form_grid_2">
                            <div class="form-group is-empty m-top-0">
                              <input type="hidden" name="id_sub[]" id="id_sub" value="<?php echo $data->Keterangan ?>">
                              <input type="text" placeholder="Nama Bank" id="namabank" name="keterangan[]" class="form-control mt-1" value="<?php echo $data->Keterangan ?>">

                            </div>
                          </div>
                          <div class="form_grid_1">
                            <select name="kategori[]" id="kategori" class="form-control mt-1">
                              <option value="judul" <?php if($data->Kategori == "judul") { ?> selected="selected"  <?php }else{ echo ""; } ?>>judul</option>
                              <option value="data" <?php if($data->Kategori == "data") { ?> selected="selected"  <?php }else{ echo ""; } ?>>data</option>
                              <option value="hasil" <?php if($data->Kategori == "hasil") { ?> selected="selected"  <?php }else{ echo ""; } ?>>hasil</option>
                              <option value="spasi" <?php if($data->Kategori == "spasi") { ?> selected="selected"  <?php }else{ echo ""; } ?>>spasi</option>
                              <option value="total" <?php if($data->Kategori == "total") { ?> selected="selected"  <?php }else{ echo ""; } ?>>total</option>
                            </select>
                          </div>
                          <div class="form_grid_2">
                           <div class="form-group is-empty m-top-0">
                            <select name="perhitungan[]" id="perhitungan" class="form-control mt-1">
                              <option value="tidak ada" <?php if($data->Perhitungan == "tidak ada") { ?> selected="selected"  <?php }else{ echo ""; } ?>>tidak ada</option>
                              <option value="tambah" <?php if($data->Perhitungan == "tambah") { ?> selected="selected"  <?php }else{ echo ""; } ?>>tambah</option>
                              <option value="kurang" <?php if($data->Perhitungan == "kurang") { ?> selected="selected"  <?php }else{ echo ""; } ?>>kurang</option>
                            </select>
                          </div>
                        </div>
                        <div class="form_grid_1">
                         <div class="form-group is-empty m-top-0">
                          <input type="text" placeholder="Group" id="group" name="group[]" class="form-control mt-1" required size="3" value="<?php echo $data->Group ?>">

                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="tambahbaris<?php echo $i ?>"></div>
                <?php endforeach ?>

              <?php endif ?>
              <br><br><br><br><br>
              <center><input type="submit" value="Submit"></center>
            </div>
            <div class="form_grid_3 mx">
             <table class="display" style="width: 90%;">

              <thead style="font-size: 11px;">

                <tr>

                  <th>Keterangan</th>

                </tr>

              </thead>

              <tbody style="font-size: 11px">
               <?php
               if($setting_pl == NULL)
               {
                echo "";
              }else{
               foreach($setting_pl as $lr1)
               {
                ?>
                <tr>
                  <td>
                    <?php 
                    if($lr1->Kategori=="judul"){
                      echo "<b>- ".$lr1->Keterangan."<b>"; 
                    }else if($lr1->Kategori=="data"){
                      echo "-- ".$lr1->Keterangan; 
                    }else if($lr1->Kategori=="hasil"){
                     echo "<b>--- ".$lr1->Keterangan."</b>"; 
                   }else if($lr1->Kategori=="spasi"){
                     echo "---- ".$lr1->Keterangan; 
                   }else if($lr1->Kategori=="total"){
                     echo "<b>----- ".$lr1->Keterangan."</b>"; 
                   }
                   ?></td>
                 </tr>
                 <?php
               }
             }
             ?>
           </tbody>

         </table>
       </div>
       <span class="clear"></span>
     </div>
   </li>
 </ul>
</form>
</div>
</div>
</div>
</div>
</div>

<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
  var i=99;
  function addRow(z){
    i = z;

      // $('#tambahbaris'+z).append(
      //   '<tr id="rowvar'+i+'">'+
      //   '<td><input type="hidden" value="'+i+'"><div class="form-group is-empty m-top-0"><select name="coa[]"  id="coa'+i+'"class="form-control"><?php //foreach ($coa as $coas){ echo "<option value=$coas->IDCoa>$coas->Nama_COA</option>"; } ?></select></div></td><td><div class="form-group is-empty m-top-0"><input type="text" id="keterangan'+i+'" name="keterangan[]" class="form-control" required></div></td><td><div class="form-group is-empty m-top-0"><select name="kategori[]" id="kategori'+i+'" class="form-control" required><option value="judul">judul</option><option value="data">data</option><option value="hasil">hasil</option><option value="spasi">spasi</option><option value="total">total</option></select></div></td><td><div class="form-group is-empty m-top-0"><select name="perhitungan[]" id="perhitungan'+i+'" class="form-control" required><option value="tidak ada">tidak ada</option><option value="tambah">tambah</option><option value="kurang">kurang</option></select></div></td><td><div class="form-group is-empty m-top-0"><input type="text" name="group[]" size="6" id="group'+i+'" class="form-control" required></div></td></td align="center"><td style="border-left: none !important;"><a class="btn btn-danger btn-xs" onclick="removeRow('+i+')" title="Hapus Baris" style="width:100%;">Hapus</i></a></td></tr>');

      $('#tambahbaris'+z).append(
        '<div class="form_grid_12" id="hapusbaris2'+i+'">'+
        '<div class="form_grid_1 mt-1 text-center">'+
        '<a class="btn-min" style="cursor: pointer" onclick="removeRow('+i+')" title="Hapus Baris" style="width:100%;">-</i></a>'+
        '</div>'+
        '<div class="form_grid_5">'+
        '<div class="form-group is-empty m-top-0"><select name="coa[]"  id="coa'+i+'"class="form-control mt-1" style="width:100%"><?php foreach ($coa as $coas){ echo "<option value=$coas->IDCoa>$coas->Nama_COA</option>"; } ?></select></div>'+
        '</div>'+
        '<div class="form_grid_2">'+
        '<div class="form-group is-empty m-top-0"><input type="text" id="keterangan'+i+'" name="keterangan[]" class="form-control mt-1" required></div>'+
        '</div>'+
        '<div class="form_grid_1">'+
        '<div class="form-group is-empty m-top-0"><select name="kategori[]" id="kategori'+i+'" class="form-control mt-1" width="100%" required><option value="judul">judul</option><option value="data">data</option><option value="hasil">hasil</option><option value="spasi">spasi</option><option value="total">total</option></select></div>'+
        '</div>'+
        '<div class="form_grid_2">'+
        '<div class="form-group is-empty m-top-0"><select name="perhitungan[]" id="perhitungan'+i+'" class="form-control mt-1" required><option value="tidak ada">tidak ada</option><option value="tambah">tambah</option><option value="kurang">kurang</option></select></div>'+
        '</div>'+
        '<div class="form_grid_1">'+
        '<div class="form-group is-empty m-top-0"><input type="text" name="group[]" size="6" id="group'+i+'" class="form-control mt-1" required></div>'+
        '</div>'+
        '</div>'


        );
      console.log(i);
    }

    function removeRow(i){
      $('#hapusbaris2'+i).remove();
    }

    function removeRowUtama(i){
      $('#hilangbaris'+i).remove();
    }
  </script>
</script>
<?php $this->load->view('administrator/footer') ; ?>