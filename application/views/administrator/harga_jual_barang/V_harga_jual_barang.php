<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Harga Jual</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <!-- <li><a href="#">Dashboard</a></li> -->
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Harga Jual Barang </li>
                        </ul>
                    </div>
                </div>
                <div class="widget_content">
                    <form action="<?php echo base_url() ?>HargaJualBarang/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                                <div class="form_grid_12 w-100 mx">
                                    <div class="form_grid_4">
                                        <div class="btn_24_blue">
                                            <a href="<?php echo base_url() ?>HargaJualBarang/tambah_harga_jual_barang"><span>Tambah Data</span></a>
                                        </div>
                                    </div>
                                    <div class="form_grid_3">
                                        <select name="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="Kode_Barang">Kode Barang</option>
                                            <option value="Nama_Barang">Nama Barang</option>
                                            <option value="Satuan">Satuan</option>
                                            <option value="Modal">Modal</option>
                                            <option value="Harga_Jual">Harga Jual</option>
                                            <option value="Kode_Group_Customer">Kode Group Customer</option>
                                            <option value="Nama_Group_Customer">Nama Group Customer</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_4">
                                        <input name="keyword" type="text" placeholder="Masukkan Keyword">

                                        <!-- <span class=" label_intro">Last Name</span> -->
                                    </div>
                                    <div class="form_grid_1">
                                        <div class="btn_24_blue flot-right">
                                            <input type="submit" name="" value="search">
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>
                            </li>
                        </form>
                    </div>
                    <div class="widget_content">
                        <table class="display data_tbl">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Barang</th>
                                    <th>Satuan</th>
                                    <th>Modal</th>
                                    <th>Harga Jual</th>
                                    <th>Group Customer</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($hargajual)){
                                    ?>
                                    <?php
                                }else{
                                    $i=1;
                                    foreach ($hargajual as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $data->Nama_Barang; ?></td>
                                            <td><?php echo $data->Satuan; ?></td>
                                            <td><?php echo 'Rp.'.convert_currency($data->Modal); ?></td>
                                            <td><?php echo 'Rp.'.convert_currency($data->Harga_Jual); ?></td>
                                            <td><?php echo $data->Nama_Group_Customer; ?></td>
                                            <td class="text-center ukuran-logo">
                                                <span><a class="action-icons c-edit" href="<?php echo base_url()?>HargaJualBarang/edit_harga_jual_barang/<?php echo $data->IDHargaJual ?>" title="Ubah Data">Edit</a></span>
                                                <span>
                                                    <a class="action-icons c-delete confirm" href="#open-modal<?php echo $data->IDHargaJual ?>" title="Hapus Data">Delete</a>
                                                </span>
                                            <!-- <a href="">  <i title="Ubah Data" data-toggle="tooltip" data-placement="top" class="hvr-buzz-out fa fa-edit"></i>
                                            </a>
                                            <a href="" data-toggle="modal" data-target="#delete-harga-jual-modal"><i title="Hapus Data" data-toggle="tooltip" data-placement="top" class="hvr-buzz-out fa fa-window-close-o"></i></a> -->
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ==================================== -->


<!-- <div class="content-wrapper">
    <div class="content">
        <div class="content-header">
            <div class="header-icon">
               <i class="hvr-buzz-out fa fa-money"></i>
           </div>
           <div class="header-title">
            <h1>Data Harga Jual</h1>
            <?php //echo $this->session->flashdata('Pesan');?>
            <ol class="breadcrumb">
                <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                <li class="active">Data Harga Jual</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>List Harga Jual</h4>
                        <span class="pull-right">

                           <a href="<?php echo base_url() ?>HargaJualBarang/tambah_harga_jual_barang" class="btn btn-primary btn--raised">Tambah Data</a>
                       </span>
                   </div>

               </div>
               <div class="panel-body">

                   <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>HargaJualBarang/pencarian">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2">

                            </div>
                            <div class="col-md-10 search-data px">
                                <?php if(empty($hargajual)){
                                    ?>
                                    <div class="col-md-8 col-xs-12 px" style="float: right;">
                                        <?php }else{ ?>
                                        <div class="col-md-8 col-xs-12 search-data px">
                                            <?php } ?>
                                            <div class="col-md-4">
                                                <select name="jenispencarian" class="form-control">
                                                    <option value="">Cari Berdasarkan</option>
                                                    <option value="Kode_Barang">Kode Barang</option>
                                                    <option value="Nama_Barang">Nama Barang</option>
                                                    <option value="Satuan">Satuan</option>
                                                    <option value="Modal">Modal</option>
                                                    <option value="Harga_Jual">Harga Jual</option>
                                                    <option value="Kode_Group_Customer">Kode Group Customer</option>
                                                    <option value="Nama_Group_Customer">Nama Group Customer</option>
                                                </select>   
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback has-feedback-left">
                                                    <input class="form-control" type="text" name="keyword">
                                                    <div class="form-control-feedback">
                                                        <i class="icon-search4 text-size-base"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 px">
                                                <center>

                                                    <button class="btn btn-primary pull-right" type="submit">Search</button>
                                                </center>
                                            </div>
                                            <?php if(empty($hargajual)){
                                                ?>
                                            </div>
                                            <?php }else{ ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Satuan</th>
                                        <th>Modal</th>
                                        <th>Harga Jual</th>
                                        <th>Group Customer</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php //if(empty($hargajual)){
                                        ?>
                                        <?php
                                    // }else{
                                    //     $i=1;
                                    //     foreach ($hargajual as $data) {
                                            ?>
                                            <tr>
                                                <td><?php //echo $i; ?></td>
                                                <td><?php //echo $data->Nama_Barang; ?></td>
                                                <td><?php //echo $data->Satuan; ?></td>
                                                <td><?php //echo 'Rp.'.convert_currency($data->Modal); ?></td>
                                                <td><?php //echo 'Rp.'.convert_currency($data->Harga_Jual); ?></td>
                                                <td><?php //echo $data->Nama_Group_Customer; ?></td>
                                                <td class="text-center ukuran-logo">
                                                    <a href="<?php echo base_url()?>HargaJualBarang/edit_harga_jual_barang/<?php echo $data->IDHargaJual ?>">  <i title="Ubah Data" data-toggle="tooltip" data-placement="top" class="hvr-buzz-out fa fa-edit"></i>
                                                    </a>
                                                    <a href="" data-toggle="modal" data-target="#delete-harga-jual-modal<?php echo $data->IDHargaJual ?>"><i title="Hapus Data" data-toggle="tooltip" data-placement="top" class="hvr-buzz-out fa fa-window-close-o"></i></a>
                                                </td>
                                                <?php
                                        //         $i++;
                                        //     }
                                        // }
                                        ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<?php 
if ($hargajual=="" || $hargajual==null) {

}else{
    foreach ($hargajual as $data) {
        ?>
           <div id="open-modal<?php echo $data->IDHargaJual; ?>" class="modal-window">
            <div>
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Konfirmasi Hapus</h1>
                <div class="modal-body">Apakah anda yakin akan menghapus data ini ?</div>
                <div class="modal-foot">
                    <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div>
                    <div class="btn_30_blue">
                        <a href="<?php echo base_url() ?>HargaJualBarang/hapus_harga_jual_barang/<?php echo $data->IDHargaJual; ?>"><span>Hapus</span></a>
                    </div>
                </div>
            </div>
        </div>

        <?php 
    }
} 
?> 
<?php $this->load->view('administrator/footer') ; ?>