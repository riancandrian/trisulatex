<?php $this->load->view('administrator/header') ; 

?>
<!-- tittle data -->
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Tambah Data Harga Jual</h3>
</div>
<!-- ======================= -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>HargaJualBarang">Data Harga Jual</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Tambah Harga Jual Barang </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Harga Jual Barang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>HargaJualBarang/proses_harga_jual_barang" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <label class="field_title">Barang</label>
                                    <div class="form_input">
                                        <select data-placeholder="Cari Berdasarkan Barang" id="barang" name="barang" style=" width:100%;" class="chzn-select" tabindex="13" onchange="getcorakmerk()">
                                            <option value=""></option>
                                            <?php foreach($barang as $bar)
                                            {
                                                echo "<option value='$bar->IDBarang'>$bar->Nama_Barang</option>";
                                            } 
                                            ?>
                                        </select>
                                    </div>

                                    <label class="field_title">Corak</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Corak" name="corak" id="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                    </div>

                                    <label class="field_title">Merk</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Merk" id="merk" name="merk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                    </div>
                                    <label class="field_title">Satuan</label>
                                    <div class="form_input">
                                        <select data-placeholder="Cari Berdasarkan Satuan" style=" width:100%;" class="chzn-select" tabindex="13" name="satuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            <option value=""></option>
                                            <?php foreach($satuan as $sat)
                                            {
                                                echo "<option value='$sat->IDSatuan'>$sat->Satuan</option>";
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                    <label class="field_title">Harga Beli</label>
                                    <div class="form_input">
                                        <span class="input-group-addon" style="">Rp. </span>
                                        <input name="modal" type="text" id="tanpa-rupiah-debit" class="form-control price-date" required oninvalid="this.setCustomValidity('Modal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <!-- <span class=" label_intro">Street Address</span> -->
                                        <span class="clear"></span>
                                    </div>
                                    <label class="field_title">Harga Jual</label>
                                    <div class="form_input">
                                        <span class="input-group-addon" style="">Rp. </span>
                                        <input name="harga_jual" id="tanpa-rupiah-kredit" class="form-control price-date" type="text" required oninvalid="this.setCustomValidity('Harga Jual Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <span class="clear"></span>
                                    </div>
                                    <label class="field_title">Group Customer</label>
                                    <div class="form_input">
                                        <select data-placeholder="Cari Berdasarkan Group Customer" style=" width:100%;" class="chzn-select" tabindex="13"  name="groupcustomer" required oninvalid="this.setCustomValidity('Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            <option value=""></option>
                                            <?php foreach($groupcustomer as $group)
                                            {
                                                echo "<option value='$group->IDGroupCustomer'>$group->Nama_Group_Customer</option>";
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                         <span> <a href="<?php echo base_url() ?>HargaJualBarang" name="simpan" title=".classname">Kembali</a></span>
                                     </div>
                                     <div class="btn_30_blue">
                                      <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                  </div>
                              </div>
                          </div>
                      </li>
                  </ul>
              </form>
          </div>
      </div>
  </div>
</div>
</div>
<!-- <div class="content-wrapper">
    <div class="content">
        <div class="content-header">
            <div class="header-icon">
               <i class="hvr-buzz-out fa fa-money"></i>
            </div>
            <div class="header-title">
                <h1>Data Harga Jual</h1>
                 <?php echo $this->session->flashdata('Pesan');?>
                <ol class="breadcrumb">
                    <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                    <li><a href="<?php echo base_url() ?>HargaJualBarang">Data Harga Jual</a></li>
                    <li class="active">Tambah Data</li>
                </ol>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-12">
                <div class="panel panel-bd">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Tambah Data Harga Jual</h4>
                        </div>
                    </div>
                    <form method="post" action="<?php echo base_url() ?>HargaJualBarang/proses_harga_jual_barang">
                        <div class="panel-body">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Barang</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="barang" required oninvalid="this.setCustomValidity('Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    <option value="">Pilih Barang</option>
                                    <?php foreach($barang as $bar)
                                    {
                                        echo "<option value='$bar->IDBarang'>$bar->Nama_Barang</option>";
                                    } 
                                    ?>
                                </select>
                            </div>
                        </div>
                       <div class="form-group row">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Satuan</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="satuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    <option value="">Pilih Satuan</option>
                                    <?php foreach($satuan as $sat)
                                    {
                                        echo "<option value='$sat->IDSatuan'>$sat->Satuan</option>";
                                    } 
                                    ?>
                                </select>
                            </div>
                        </div>
                     <div class="form-group row">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Modal</label>
                                <div class="col-sm-9">
                                    <div class="input-group"> 
                                    <span class="input-group-addon">Rp. </span>
                                    <input type="text" class="form-control" name="modal" id="tanpa-rupiah-debit" placeholder="Modal" required oninvalid="this.setCustomValidity('Modal Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                                </div>
                                </div>
                            </div>
                  <div class="form-group row">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Harga Jual</label>
                                <div class="col-sm-9">
                                    <div class="input-group"> 
                                    <span class="input-group-addon">Rp. </span>
                                    <input type="text" class="form-control" name="harga_jual" id="tanpa-rupiah-kredit" placeholder="Harga Jual" required oninvalid="this.setCustomValidity('Harga Jual Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                                </div>
                                </div>
                            </div>
                   
                <div class="form-group row">
                    <label for="example-search-input" class="col-sm-2 col-form-label">Group Customer</label>
                    <div class="col-sm-9">
                      <select class="form-control" name="groupcustomer" required oninvalid="this.setCustomValidity('Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <option value="">Pilih Group Customer</option>
                        <?php foreach($groupcustomer as $group)
                        {
                            echo "<option value='$group->IDGroupCustomer'>$group->Nama_Group_Customer</option>";
                        } 
                        ?>
                    </select>
                </div>
            </div>
    <center>
        <a href="<?php echo base_url() ?>HargaJualBarang" name="simpan" class="btn btn-primary">Kembali</a>
        <input type="submit" name="simtam" class="btn btn-primary" value="Simpan Data">

    </center>
</div>
</form>
</div>
</div>
</div>
</div>
</div> -->
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
 function getcorakmerk() {
    var barang = $("#barang").val();

    $.ajax({
      url : '/'+base_url[1]+'/HargaJualBarang/get_corakmerk',
      type: "POST",
      data:{id_barang:barang},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
            console.log(data);
            $('#namamerk').val('');
            $('#merk').val('');
        } else {
            console.log(data);
            $("#corak").val(data[0]["Corak"]);
            $("#merk").val(data[0]["Merk"]);
        }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
  }
});
}
</script>
<?php $this->load->view('administrator/footer') ; ?>