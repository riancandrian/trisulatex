<?php $this->load->view('administrator/header') ;

if ($kodenoasset->curr_number == null) {
    $number = 1;
} else {
    $number = $kodenoasset->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_booking = 'FJA'.date('y').str_replace(' ', '',$agen).$kodemax;
//echo $code_booking;
?>
<!--  style -->
<style type="text/css">
    .chzn-container{
        width: 101% !important;
    }
</style>
    <!-- tittle data -->
    <div class="page_title">
        <span class="title_icon"><span class="blocks_images"></span></span>
        <h3>Tambah Data Penjualan Asset</h3>
    </div>
    <!-- ======================= -->

    <!-- body content -->
    <div id="content">
        <div class="grid_container">
            <div class="grid_12">
                <div class="widget_wrap">

                    <!-- breadcrumb -->
                    <div class="breadCrumbHolder module">
                        <div id="breadCrumb0" class="breadCrumb module white_lin">
                            <ul>
                                <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                                <li><a href="<?php echo base_url() ?>BookingOrder">Data Penjualan Asset</a></li>
                                <li> Tambah Penjualan Asset </li>
                            </ul>
                        </div>
                    </div>

                    <div class="widget_top">
                        <div class="grid_12">
                            <span class="h_icon blocks_images"></span>
                            <h6>Tambah Penjualan Asset</h6>
                        </div>
                    </div>
                    <div class="widget_content">
                        <form class="form_container left_label">
                            <ul>
                                <li>
                                    <div class="form_grid_12 multiline">
                                        <label class="field_title">Tanggal</label>
                                        <div class="form_input">
                                            <input type="date" class="form-control date-picker hidden-sm-down" name="Tanggal" id="Tanggal" value="<?php echo date('Y-m-d');?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            <span class="clear"></span>
                                        </div>
                                        <label class="field_title">Nomor</label>
                                        <div class="form_input">
                                            <input type="text" class="form-control" name="nomor" id="nomor" value="<?php echo $code_booking; ?>" readonly>
                                        </div>
                                        <label class="field_title">Customer</label>
                                        <div class="form_input">
                                            <select data-placeholder="Cari Berdasarkan Customer" style="width:100%;" class="chzn-select" tabindex="13" name="customer" id="customer" required oninvalid="this.setCustomValidity('Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                <option value=""></option>
                                                <?php foreach($customer as $row) {
                                                    echo "<option value='$row->IDCustomer'>$row->Nama</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <label class="field_title">Nama Di Faktur</label>
                                        <div class="form_input">
                                            <input type="text" class="form-control" name="nama_faktur" id="nama_faktur">
                                        </div>
                                        <label class="field_title">Qty</label>
                                        <div class="form_input">
                                            <input type="text" class="form-control" name="qty" id="qty" onkeyup="this.value=this.value.replace(/[^0-9.]/,'')">
                                        </div>
                                        <label class="field_title">Asset</label>
                                        <div class="form_input">
                                            <select data-placeholder="Cari Berdasarkan Asset" style="width:100%;" class="chzn-select" tabindex="13" name="asset" id="asset" required oninvalid="this.setCustomValidity('Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')" onchange="getpembelianasset()">
                                                <option value=""></option>
                                                <?php foreach($asset as $row) {
                                                    echo "<option value='$row->IDFBA'>$row->Nomor</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <label class="field_title">Harga</label>
                                        <div class="form_input">
                                            <input type="text" class="form-control" name="harga" id="harga">
                                        </div>

                                        <label id="akumulasipenyusutan" class="field_title"></label>
                                        <div class="form_input">
                                            <div id="akumulasi_penyusutan"></div>
                                        </div>

                                        <label id="nilaibuku" class="field_title"></label>
                                        <div class="form_input">
                                            <div id="nilai_buku"></div>
                                        </div>

                                        <label id="metodepenyusutan" class="field_title"></label>
                                        <div class="form_input">
                                            <div id="metode_penyusutan"></div>
                                        </div>

                                        <label id="tanggalpenyusutan" class="field_title"></label>
                                        <div class="form_input">
                                            <div id="tanggal_penyusutan"></div>
                                        </div>

                                        <label id="umur" class="field_title"></label>
                                        <div class="form_input">
                                            <div id="umur_text"></div>
                                        </div>

                                        <label id="groupasset" class="field_title"></label>
                                        <div class="form_input">
                                            <div id="group_asset"></div>
                                        </div>

                                        <label id="namaasset" class="field_title"></label>
                                        <div class="form_input">
                                            <div id="namaasset_text"></div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                            <div class="btn_30_blue">
                                                <span><input name="simtam" id="simtam" type="button" class="btn_small btn_blue" value="Simpan Data"></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
 function getpembelianasset() {
    var asset = $("#asset").val();

    $.ajax({
      url : '/'+base_url[1]+'/Penjualan_asset/get_pembelian_asset',
      type: "POST",
      data:{id_asset:asset},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
            console.log(data);
            $('#harga').val('');
        } else {
            console.log(data);
            $("#harga").val(data[0]["Nilai_perolehan"]);
            $("#akumulasipenyusutan").html("Akumulasi Penyusutan ");
            $("#akumulasi_penyusutan").html(data[0]["Akumulasi_penyusutan"]);
            $("#nilaibuku").html("Nilai Buku ");
            $("#nilai_buku").html(data[0]["Nilai_buku"]);
            $("#metodepenyusutan").html("Metode Penyusutan ");
            $("#metode_penyusutan").html(data[0]["Metode_penyusutan"]);
            $("#tanggalpenyusutan").html("Tanggal Penyusutan ");
            $("#tanggal_penyusutan").html(data[0]["Tanggal_penyusutan"]);
            $("#umur").html("Umur ");
            $("#umur_text").html(data[0]["Umur"]);
            $("#groupasset").html("Group Asset ");
            $("#group_asset").html(data[0]["Group_Asset"]);
            $("#namaasset").html("Asset ");
            $("#namaasset_text").html(data[0]["Nama_Asset"]);
        }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
  }
});
}
</script>
<?php $this->load->view('administrator/footer') ; ?>