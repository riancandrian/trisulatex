<?php $this->load->view('administrator/header') ; 

?>
<!-- tittle Data -->
<div class="page_title">

  <!-- notify -->
  <?php echo $this->session->flashdata('Pesan');?>
  <!-- ====== -->

  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Data Customer</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">

        <!-- breadcrumb -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>customer">Data Customer</a></li>
              <!-- <li><a href="#">Product Discovery</a></li>
              <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
              <li><a href="#">Kits and Assays</a></li>
              <li><a href="#">Mutagenesis Kits</a></li> -->
              <li> Tambah Customer </li>
            </ul>
          </div>
        </div>

        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Customer</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="<?php echo base_url() ?>Customer/proses_customer" enctype="multipart/form-data" class="form_container left_label">
            <ul>
              <li>
                <div class="form_grid_12 multiline">

                  <label class="field_title">Group Customer</label>
                   <div class="form_input">
                                        <div class="grid_12 w-100 alpha">
                                          
                                            
                                            <div class="grid_9 ml">
                    <select data-placeholder="Pilih Group Customer"  name="group" required oninvalid="this.setCustomValidity('Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                      <option value=""></option> 
                      <?php foreach($groupcustomer as $group)
                      {
                        echo "<option value='$group->IDGroupCustomer'>$group->Nama_Group_Customer</option>";
                      } 
                      ?>
                    </select>
                  </div>
                  <div class="form_grid_3 mr">
                                                <div class="btn_24_blue flot-right">
                                                   <span><a href="#open-modal">Tambah Group Customer</a></span>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="clear"></div>
                                   </div>


                  <!-- Kode Customer -->
                  <label class="field_title">Kode Customer</label>
                  <div class="form_input">
                    <input type="text" class="form-control" placeholder="Kode Customer" name="kode" required oninvalid="this.setCustomValidity('Kode Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                  </div>

                  <!-- Nama Customer -->
                  <label class="field_title">Nama Customer</label>
                  <div class="form_input">
                    <input type="text" class="form-control" placeholder="Nama" name="nama" required oninvalid="this.setCustomValidity('Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                  </div>

                  <!-- Alamat -->
                  <label class="field_title">Alamat</label>
                  <div class="form_input">
                    <textarea class="form-control" placeholder="Alamat" name="alamat" required oninvalid="this.setCustomValidity('Alamat Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="resize: none;" rows="6"></textarea>
                  </div>
                  <label class="field_title">Kota</label>
                  <div class="form_input">
                                        <div class="grid_12 w-100 alpha">
                                          
                                            
                                            <div class="grid_9 ml">
                    <select data-placeholder="Pilih Kota"  name="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                      <option value=""></option> 
                      <?php foreach($kota as $kot)
                      {
                        echo "<option value='$kot->IDKota'>$kot->Kota</option>";
                      } 
                      ?>
                    </select>
                  </div>
                  <div class="form_grid_3 mr">
                                                <div class="btn_24_blue flot-right">
                                                   <span><a href="#open-modal-kota">Tambah Kota</a></span>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="clear"></div>
                                   </div>

                  <!-- No telepon -->
                  <label class="field_title">No Telepon</label>
                  <div class="form_input">
                    <input type="text" class="form-control" placeholder="No Telpon" value="+62" name="telp" required oninvalid="this.setCustomValidity('No Telpon Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                  </div>

                  <!-- FAx -->
                  <label class="field_title">Fax</label>
                  <div class="form_input">
                    <input type="text" class="form-control" placeholder="Fax" name="fax" required oninvalid="this.setCustomValidity('Fax Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
                  </div>

                  <!-- Email -->
                  <label class="field_title">Email</label>
                  <div class="form_input">
                    <input type="email" class="form-control" placeholder="Email" name="email" required oninvalid="this.setCustomValidity('Email Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                  </div>

                  <!-- NPWP -->
                  <label class="field_title">NPWP</label>
                  <div class="form_input">
                    <input type="text" class="form-control" placeholder="NPWP" name="npwp" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
                  </div>

                  <!-- NPWP -->
                  <label class="field_title">No KTP</label>
                  <div class="form_input">
                    <input type="text" class="form-control" placeholder="No KTP" name="ktp" required oninvalid="this.setCustomValidity('No KTP Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
                  </div>

                </div>
              </li>
              <li>
                <div class="form_grid_12">
                  <div class="form_input">
                    <div class="btn_30_light">
                      <span> <a href="<?php echo base_url() ?>Customer" name="simpan" title=".classname">Kembali</a></span>
                    </div>
                    <div class="btn_30_blue">
                      <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- =============== -->
       <div id="open-modal" class="modal-window">
 <div>
     <form method="post" action="<?php echo base_url() ?>GroupCustomer/simpan" enctype="multipart/form-data" class="form_container left_label">
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Tambah Group Customer</h1>
                <div class="modal-body">
                     
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">

                                    <!-- Kode Barang -->
                                    <label class="field_title">Kode Group Customer</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Kode Group Customer" name="kode_group" required oninvalid="this.setCustomValidity('Kode Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Nama Barang -->
                                    <label class="field_title">Group Customer</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Nama Group Customer" name="groupcustomer" required oninvalid="this.setCustomValidity('Nama Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    
                </div>
                <div class="modal-foot">
                  <!--   <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div> -->
                    <div class="btn_30_blue">
                       <input type="submit" name="simbar" value="Simpan">
                    </div>
                </div>
                </form>
            </div>
</div>


       <div id="open-modal-kota" class="modal-window">
 <div>
     <form method="post" action="<?php echo base_url() ?>Kota/proses_kota" enctype="multipart/form-data" class="form_container left_label">
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Tambah Kota</h1>
                <div class="modal-body">
                     
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">

                                    <label class="field_title">Kode Kota</label>
                                    <div class="form_input">

                                        <input type="text" class="form-control" placeholder="Kode Kota" name="kode" required oninvalid="this.setCustomValidity('Kode Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <span class="clear"></span>
                                    </div>
                                    <label class="field_title">Provinsi</label>
                                    <div class="form_input">
                                       <input type="text" class="form-control" placeholder="Provinsi" name="provinsi" required oninvalid="this.setCustomValidity('Provinsi Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                       <span class="clear"></span>
                                   </div>
                                   <label class="field_title">Kota</label>
                                   <div class="form_input">
                                      <input type="text" class="form-control" placeholder="Kota" name="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <span class="clear"></span>
                                  </div>
                                </div>
                            </li>
                        </ul>
                    
                </div>
                <div class="modal-foot">
                  <!--   <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div> -->
                    <div class="btn_30_blue">
                       <input type="submit" name="simbar" value="Simpan">
                    </div>
                </div>
                </form>
            </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>