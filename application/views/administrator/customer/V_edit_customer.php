<?php $this->load->view('administrator/header') ; ?>
<!-- tittle Data -->
<div class="page_title">

  <!-- notify -->
  <?php echo $this->session->flashdata('Pesan');?>
  <!-- ====== -->

  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Data Customer</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>customer">Data Customer</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Edit Customer </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Data Customer</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Customer/proses_edit_customer/<?php echo $edit->IDCustomer ?>" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">

                                    <!-- Group Customer -->
                                    <label class="field_title">Group Customer</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Group Customer"  name="group" required oninvalid="this.setCustomValidity('Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option> 
                                            <?php
                                            foreach ($groupcustomer as $group) {
                                                if ($edit->IDGroupCustomer==$group->IDGroupCustomer) {
                                                    ?>
                                                    <option value="<?php echo $group->IDGroupCustomer ?>" selected><?php echo $group->Nama_Group_Customer; ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $group->IDGroupCustomer ?>"><?php echo $group->Nama_Group_Customer; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <!-- Kode Customer -->
                                    <label class="field_title">Kode Customer</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Kode Customer" name="kode" value="<?php echo $edit->Kode_Customer ?>" required oninvalid="this.setCustomValidity('Kode Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Nama Customer -->
                                    <label class="field_title">Nama Customer</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Nama" name="nama" value="<?php echo $edit->Nama ?>" required oninvalid="this.setCustomValidity('Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Alamat -->
                                    <label class="field_title">Alamat</label>
                                    <div class="form_input">
                                        <textarea class="form-control" placeholder="Alamat" name="alamat" required oninvalid="this.setCustomValidity('Alamat Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="resize: none;" rows="6"><?php echo $edit->Alamat ?></textarea>
                                    </div>

                                    <!-- Kota -->
                                    <label class="field_title">Kota</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Kota"  name="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option> 
                                            <?php  foreach ($kota as $kot) {
                                                if ($edit->IDKota==$kot->IDKota) {
                                                    ?>
                                                    <option value="<?php echo $kot->IDKota ?>" selected><?php echo $kot->Kota; ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $kot->IDKota ?>"><?php echo $kot->Kota; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <!-- No telepon -->
                                    <label class="field_title">No Telepon</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="No Telpon" name="telp" value="<?php echo $edit->No_Telpon ?>" required oninvalid="this.setCustomValidity('No Telpon Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- FAx -->
                                    <label class="field_title">Fax</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Fax" name="fax" value="<?php echo $edit->Fax ?>" required oninvalid="this.setCustomValidity('Fax Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
                                    </div>

                                    <!-- Email -->
                                    <label class="field_title">Email</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo $edit->Email ?>" required oninvalid="this.setCustomValidity('Email Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- NPWP -->
                                    <label class="field_title">NPWP</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="NPWP" name="npwp" value="<?php echo $edit->NPWP ?>" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
                                    </div>

                                    <!-- NPWP -->
                                    <label class="field_title">No KTP</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="No KTP" name="ktp" value="<?php echo $edit->No_KTP ?>" required oninvalid="this.setCustomValidity('No KTP Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
                                    </div>

                                
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                            <span> <a href="<?php echo base_url() ?>Customer" name="simpan" title=".classname">Kembali</a></span>
                                        </div>
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simpan" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ====================== -->

<?php $this->load->view('administrator/footer') ; ?>