<?php $this->load->view('administrator/header'); ?>

<!-- style -->
<style type="text/css">
.chzn-container {
    width: 103% !important;
}
.simplemodal-container,
#simplemodal-container .simplemodal-data{
    padding: 0 !important;
}
.tittle-header-modal{
    background: #165a91;
    padding: 15px;
    color: #fff;
}
#simplemodal-container h3{
    color: #fff;
}
.simplemodal-container{
    height: auto !important;
    top: 30% !important;
}
</style>
<!-- tittle data -->
<div class="page_title" xmlns="http://www.w3.org/1999/html">
    <div id="alert"></div>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Edit Data Penerimaan Barang Kain Non Trisula</h3>
</div>
<!-- ======================= -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Penerimaan_barang_umum">Data Penerimaan Barang Kain Non Trisula</a></li>
                            <li> Edit Penerimaan Barang Kain Non Trisula</li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Penerimaan Barang Kain Non Trisula</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form  class="form_container left_label">
                        <ul>
                            <li class="body-search-data">

                                <!-- form header -->
                                <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_8">
                                        <input type="hidden" name="IDTBS" id="IDTBS" value="<?php echo $data->IDTBS?>">
                                        <!-- form 1 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Tanggal</label>
                                            <div class="form_input">
                                                <input type="date" class="input-date-padding-3-2 form-control date-picker hidden-sm-down" name="Tanggal" id="Tanggal" value="<?php echo $data->Tanggal?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                        <!-- form 2 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">No SJC</label>
                                            <div class="form_input">
                                                <input type="text" class="form_control" name="no_sjc" id="no_sjc" value="<?php echo $data->Nomor_sj?>" > <!-- value nya belum -->
                                            </div>
                                        </div>

                                        <!-- form 3 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">No PB</label>
                                            <div class="form_input input-not-focus">
                                                <input type="text" class="form-control" name="no_bo" id="no_bo" value="<?php echo $data->Nomor?>" readonly>
                                            </div>
                                        </div>

                                        <!-- form 4 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">No PO</label>
                                            <div class="form_input">
                                                <input type="text" class="form-control" name="no_po" id="no_po" value="<?php echo $data->IDPO?>" > <!-- value belum -->
                                            </div>
                                        </div>

                                        <!-- form 5 -->
                                        <div class="form_grid_6">
                                            <label class="field_title mt-dot2 mb">Supplier</label>
                                            <div class="form_input">
                                                <select data-placeholder="Cari Berdasarkan Supplier" style="width:100%;" class="chzn-select" tabindex="13" name="id_supplier" id="id_supplier" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                    <option value=""></option>
                                                    <?php foreach($supplier as $row) { ?>
                                                    <option value='<?php echo $row->IDSupplier ?>' <?php echo $data->IDSupplier == $row->IDSupplier ? 'selected' : ''; ?>><?php echo $row->Nama?></option>";
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- form 6 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">No SO</label>
                                            <div class="form_input">
                                                <input type="text" class="form-control" name="no_so" id="no_so" value="<?php echo $data->NoSO?>" > <!-- value belum -->
                                            </div>
                                        </div>

                                        <!-- form 7 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Total Yard</label>
                                            <div class="form_input input-not-focus">
                                                <input type="text" class="form-control" name="total_yard" id="total_yard" value="<?php echo $data->Total_qty_yard?>"  readonly> <!-- value belum -->
                                            </div>
                                        </div>

                                        <!-- form 8 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Total Meter</label>
                                            <div class="form_input input-not-focus">
                                                <input type="text" class="form-control" name="total_meter" id="total_meter" value="<?php echo $data->Total_qty_meter?>" readonly><!-- value belum -->
                                            </div>
                                        </div>

                                    </div>

                                    <!-- form 9 -->
                                    <div class="form_grid_4 mb-1">
                                        <label class="field_title mt-dot2 mb">Keterangan</label>
                                        <div class="form_input input-not-focus">
                                            <textarea class="form-control" name="Keterangan" id="Keterangan" rows="9" style="resize: none;"><?php echo $data->Keterangan?></textarea>
                                        </div>
                                    </div>

                                    <!-- clear body -->
                                    <span class="clear"></span>

                                </div>
                                <!-- end form header -->

                            </li>
                            <li>

                                <!-- form body -->
                                <div class="widget_content">
                                    <div class="form_grid_12 multiline">

                                        <!-- form 1 -->
                                        <div class="form_grid_3 mb-1">
                                            <input type="text" class="form-control" name="barcode" id="barcode" placeholder="Barcode">
                                        </div>

                                        <!-- form 2 -->
                                        <div class="form_grid_3 mb-1">
                                            <!--<input type="text" class="form-control" name="corak" id="corak" placeholder="Corak" required>-->
                                            <select data-placeholder="Cari corak" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" tabindex="13" name="corak" id="corak" onchange="findColor()">
                                                <option value="" selected="" disabled>Pilih Corak</option>
                                                <?php foreach($corak as $row) {
                                                    echo "<option value='$row->IDCorak'>$row->Corak</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <!-- form 3 -->
                                        <div class="form_grid_3 mb-1">
                                            <!--<input type="text" class="form-control" name="warna" id="warna" placeholder="Warna" required>-->
                                            <select name="warna" id="warna" data-placeholder="Cari warna" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="form_control" tabindex="13">
                                                <option value="" selected disabled>Pilih Warna</option>
                                            </select>
                                        </div>

                                        <!-- form 4 -->
                                        <div class="form_grid_3 mb-1">
                                            <input type="text" class="form-control" name="merk" id="merk" placeholder="Merk">
                                        </div>

                                        <!-- form 5 -->
                                        <div class="form_grid_3 mb-1">
                                            <input type="text" class="form-control" name="qty_yard" id="qty_yard" placeholder="Qty Yard" onkeyup="convert('C')">
                                        </div>

                                        <!-- form 6 -->
                                        <div class="form_grid_3 mb-1">
                                            <input type="text" class="form-control" name="qty_meter" id="qty_meter" placeholder="Qty Meter" onkeyup="convert('F')">
                                        </div>

                                        <!-- form 7 -->
                                        <div class="form_grid_3 mb-1">
                                            <select name="grade" id="grade" class="form-control" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;">
                                                <option value="">Pilih Grade</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="S">S</option>
                                                <option value="E">E</option>
                                            </select>
                                        </div>

                                        <div class="form_grid_3 mb-1">
                                            <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga">
                                        </div>

                                        <!-- form 8 -->
                                        <div class="form_grid_3 mb-1">
                                            <!--<input type="text" class="form-control" name="satuan" id="satuan" placeholder="Satuan" required>-->
                                            <select data-placeholder="Cari satuan" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="" tabindex="13" name="satuan" id="satuan">
                                                <option value="" selected="" disabled>Pilih Satuan</option>
                                                <?php foreach($satuan as $row) {
                                                    echo "<option value='$row->Satuan'>$row->Satuan</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </li>
                            <li><center>
                                <div class="btn_30_blue">
                                    <span><input name="add" id="add-to-detail" onclick="addToDetail()" type="button" class="btn_small btn_blue" value="Tambah Data"></span>
                                </div>
                            </center>
                        </li>
                      <div class="widget_content">
                                <div class="form_grid_12 w-100 alpha">
                                    <table class="display data_tbl" id="tabelfsdfsf">
                                        <thead>
                                            <tr class="odd gradeA">
                                                <th class="text-center">No</th>
                                                <th class="text-center">Barcode</th>
                                                <th class="text-center">Corak</th>
                                                <th class="text-center">Warna</th>
                                                 <th class="text-center">Grade</th>
                                                <th class="text-center">Qty Yard</th>
                                                <th class="text-center">Qty Meter</th>
                                                <th class="text-center">Satuan</th>
                                                <th class="text-center">Harga Satuan</th>
                                                <th class="text-center">Total Harga</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabel-cart-asset">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <li>
                            <div class="widget_content px-2 text-center">
                                <div class="py-4 mx-2">
                                 <div class="btn_30_blue">
                                            <span><input type="hidden" class="btn_24_blue" id="print_" name="print_" onclick="print_cek_edit()" value="Print Data"></span>
                                        </div>
                          
                          <div class="btn_30_blue">
                            <span><input type="button" id="simpan" onclick="updateChange()" class="btn_small btn_blue" value="Simpan" title="Simpan Data"></span>
                        </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </form>
</div>
</div>
</div>

<div id="basic-modal-content">
    <div class="tittle-header-modal">
        <h3>Ubah detail penerimaan barang umum</h3>
    </div>
    <form class="form_container left_label">
        <ul>
            <li>
                <div class="form_grid_12">
                    <div class="form_grid_6 mb-1">
                        <label class="field_title">Corak</label>
                        <div class="form_input">
                            <!--<input type="text" class="form-control" name="corak" id="corak" placeholder="Corak" required>-->
                            <select data-placeholder="Cari satuan" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="" tabindex="13" name="vcorak" id="vcorak" onchange="getColor()"></select>
                        </div>
                    </div>
                    <div class="form_grid_6 mb-1">
                        <label class="field_title">Warna</label>
                        <div class="form_input">
                            <!--<input type="text" class="form-control" name="warna" id="warna" placeholder="Warna" required>-->
                            <select data-placeholder="Cari warna" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="" tabindex="13" name="vwarna" id="vwarna"></select>
                        </div>
                    </div>
                    <div class="form_grid_6 mb-1">
                        <label class="field_title">Grade</label>
                        <div class="form_input">
                            <select class="form-control" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" name="vgrade" id="vgrade" placeholder="vGrade" required>
                             <option value="">Pilih Grade</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="S">S</option>
                                                <option value="E">E</option>
                                            </select>
                        </div>
                    </div>
                    <div class="form_grid_6 mb-1">
                        <label class="field_title">Qty Yard</label>
                        <div class="form_input">
                            <input type="text" class="form-control" name="vqty_yard" id="vqty_yard" placeholder="Qty Yard" required>
                        </div>
                    </div>
                    <div class="form_grid_6 mb-1">
                        <label class="field_title">Qty Meter</label>
                        <div class="form_input">
                            <input type="text" class="form-control" name="vqty_meter" id="vqty_meter" placeholder="Qty Meter" required>
                        </div>
                    </div>
                    <div class="form_grid_6 mb-1">
                        <label class="field_title">Satuan</label>
                        <div class="form_input">
                            <!--<input type="text" class="form-control" name="satuan" id="satuan" required>-->
                            <select data-placeholder="Cari satuan" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="" tabindex="13" name="vsatuan" id="vsatuan"></select>
                        </div>
                    </div>

                    <div class="clear"></div><br>
                </div>
            </li>
            <input type="hidden" name="IDTBSDetail" id="IDTBSDetail">
            <input type="hidden" name="vindex" id="vindex">
            <li class="text-center">
                <div class="btn_24_blue">
                    <a href="#" onclick="save_tmp()" class="simplemodal-close"><span>Simpan</span></a>
                </div>
            </li>
        </ul>
    </form>
</div>

</div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/penerimaan_barang_umum.js"></script>
<script>
    var baseUrl = "<?php echo base_url()?>";
    $(document).ready(function(){
        renderJSON($.parseJSON('<?php echo json_encode($detail) ?>'));
    });
</script>
<script>
    function convert(degree) {
        var x;
        if (degree == "C") {
            x = document.getElementById("qty_yard").value * 0.9144
            document.getElementById("qty_meter").value = x.toFixed(2);
        } else {
            x = (document.getElementById("qty_meter").value * 1.09361);
            document.getElementById("qty_yard").value = x.toFixed(2);
        }
    }
</script>
<?php $this->load->view('administrator/footer') ; ?>