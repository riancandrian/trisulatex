<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Penerimaan Barang Kain Non Trisula</h3>
</div>
<!-- ======================= -->
            <?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li> Penerimaan Barang Kain Non Trisula </li>
                        </ul>
                    </div>
                </div>
                <div class="widget_content">
                    <form action="<?php echo base_url() ?>Penerimaan_barang_umum/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">

                                <!-- search -->
                                <div class="form_grid_12 w-100 alpha">

                                    <!-- button -->
                                    <div class="form_grid_3 ">
                                        <div class="btn_24_blue">
                                            <a href="<?php echo base_url() ?>Penerimaan_barang_umum/create"><span>Tambah Data</span></a>
                                        </div>
                                    </div>

                                    <div class="form_grid_9 ">

                                        <!-- form 1 -->
                                        <div class="form_grid_3 ">   
                                            <label class="field_title mt-dot2">Periode</label>
                                            <div class="form_input">
                                                <input type="date" class="input-date-padding form-control date-picker hidden-sm-down" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>

                                        <!-- form 2 -->
                                        <div class="form_grid_3 ">   
                                            <label class="field_title mt-dot2"> S/D </label>
                                            <div class="form_input">
                                                <input type="date" class="input-date-padding form-control date-picker hidden-sm-down" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                            </div>
                                        </div>

                                        <div class="form_grid_3 ">   
                                            <select name="jenispencarian" data-placeholder="No PB" style="width: 100%!important" class="chzn-select" tabindex="13">
                                                <option value="Nomor">No PB</option>
                                            </select>
                                        </div>
                                        <div class="form_grid_2">   
                                            <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
                                        </div>
                                        <div class="form_grid_1">
                                            <div class="btn_24_blue">
                                                <input type="submit" name="" value="search">
                                            </div>
                                        </div>
                                    </div>

                                    <span class="clear"></span>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
                <form action="<?php echo base_url('Penerimaan_barang_umum/bulk'); ?>" method="post">
                    <div class="widget_content">
                        <table class="display" id="action_tbl">
                            <thead>
                                <tr>
                                    <th class="center">
                                        <input name="checkbox" type="checkbox" value="" class="checkall"> Do
                                    </th>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nomor PB</th>
                                    <th>Nomor SJC</th>
                                    <th>Total Yard</th>
                                    <th>Total Meter</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($PenerimaanBU)){
                                    $i=1;
                                    foreach ($PenerimaanBU as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td class="center tr_select"><input type="checkbox" name="bulk[]" value="<?php echo $data->Nomor; ?>" class="block_form"></td>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo DateFormat($data->Tanggal); ?></td>
                                            <td><?php echo $data->Nomor; ?></td>
                                            <td><?php echo $data->Nomor_sj; ?></td>
                                            <td><?php echo $data->Total_qty_yard; ?></td>
                                            <td><?php echo $data->Total_qty_meter; ?></td>
                                            <td><?php echo $data->Batal; ?></td>
                                            <td class="text-center ukuran-logo">
                                                <span><a class="action-icons c-Detail" href="<?= site_url('Penerimaan_barang_umum/show/'.$data->IDTBS); ?>" title="Detail Data"> Detail</a></span>
                                                <?php if($data->Batal=='Aktif'){ ?>
                                                <span><a class="action-icons c-edit" href="<?= site_url('Penerimaan_barang_umum/edit/'.$data->IDTBS); ?>" title="Edit Data">Edit</a></span>
                                                <?php }else{
                                                    ?>
                                                <span><a class="action-icons c-edit" style="cursor: no-drop;" title="Edit Data">Edit</a></span>
                                                <?php } ?>
                                                
                                                <?php if($data->Batal == "Aktif"){ ?>
                                                <span><a class="action-icons c-sa" href="<?php echo base_url('Penerimaan_barang_umum/soft_delete/'.$data->IDTBS) ?>" title="Status Tidak Aktif">Status</a></span>
                                                <?php } else { ?>
                                                <span><a class="action-icons c-approve" href="<?php echo base_url('Penerimaan_barang_umum/soft_success/'.$data->IDTBS) ?>" title="Status Aktif">Status</a></span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>
                    <div class="widget_content text-right px-2">
                        <div class="py-4 mx-2">
                            <div class="btn_24_blue">
                                <input type="submit" value="Batal">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ==================================== -->

<?php 
if ($PenerimaanBU !== "" || $PenerimaanBU !== null) {
    foreach ($PenerimaanBU as $data) {
        ?>
        <div id="open-modal<?php echo $data->IDTBS; ?>" class="modal-window">
            <div>
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Konfirmasi Hapus</h1>
                <div class="modal-body">Apakah anda yakin akan me-non-aktifkan data ini ?</div>
                <div class="modal-foot">
                    <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div>
                    <div class="btn_30_blue">
                        <a href="<?php echo base_url() ?>PenerimaanBarang/soft_delete/<?php echo $data->IDTBS; ?>"><span> Non-aktifkan</span></a>
                    </div>
                </div>
            </div>
        </div>

        <?php 
    }
} 
?> 
<?php $this->load->view('administrator/footer') ; ?>