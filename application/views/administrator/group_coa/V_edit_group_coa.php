
<?php $this->load->view('administrator/header') ; ?>

<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Group COA</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>GroupCOA">Data Group COA</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Edit Group COA </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Data Group COA</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>GroupCOA/proses_edit_group_coa/<?php echo $edit->IDGroupCOA ?>" enctype="multipart/form-data" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <!-- Kode Group Barang -->
                                    <label class="field_title">Kode Group COA</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Kode Group COA" name="kode" value="<?php echo $edit->Kode_Group_COA ?>" required oninvalid="this.setCustomValidity('Kode Group COA Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Group Barang -->
                                    <label class="field_title">Group COA</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Group Barang" name="group" value="<?php echo $edit->Nama_Group ?>" required oninvalid="this.setCustomValidity('Group COA Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Status -->
                               <label class="field_title">Normal Balance</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Normal Balance" name="normal" required oninvalid="this.setCustomValidity('Normal Balance Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php if ($edit->Normal_Balance == 'debet') {
                                                echo '<option selected value="debet" selected>Debet</option>
                                                <option value="kredit">Kredit</option>';
                                            }elseif ($edit->Normal_Balance == 'kredit') {
                                                echo '<option selected value="debet">Debet</option>
                                                <option value="kredit" selected>Kredit</option>';
                                            }else{
                                                echo '<option value="debet">Debet</option><option selected value="kredit">Kredit</option>';
                                            } ?>
                                        </select>
                                    </div> 
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                            <span> <a href="<?php echo base_url() ?>GroupBarang" name="simpan" title=".classname">Kembali</a></span>
                                        </div>
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simpan" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('administrator/footer') ; ?>