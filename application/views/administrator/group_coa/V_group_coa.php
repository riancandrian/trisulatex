<?php $this->load->view('administrator/header') ; ?>
<!-- tittle page -->
<div class="page_title">

    <!-- pesan -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ===== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Group COA</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <!-- <li><a href="#">Dashboard</a></li> -->
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Data Group COA </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>GroupCOA/pencarian" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                                <div class="form_grid_12 w-100 px">
                                    <div class="form_grid_4 mx">
                                        <div class="btn_24_blue">
                                            <a href="<?php echo base_url() ?>GroupCOA/tambah_group_coa"><span>Tambah Data</span></a>
                                        </div>
                                    </div>
                                    <div class="form_grid_2">
                                        <select data-placeholder="Cari Berdasarkan" name="jenispencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="Kode_Group_COA">Kode Group COA</option>
                                            <option value="Nama_Group">Group COA</option>
                                            <option value="Normal_Balance">Normal Balance</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_2">
                                        <select data-placeholder="Pilih Status" name="statuspencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_3">
                                        <input name="keyword" type="text" placeholder="Masukkan Keyword">
                                        <!-- <span class=" label_intro">Last Name</span> -->
                                    </div>
                                    <div class="form_grid_1 flot-right">
                                        <div class="btn_24_blue">
                                            <input type="submit" name="" value="search">
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>

                <div class="widget_content">
                    <table class="display data_tbl">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Group COA</th>
                                <th>Group COA</th>
                                <th>Normal Balance</th>
                                <th>Aktif</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(empty($groupcoa)){
                                ?>

                                <?php
                            }else{
                                $i=1;
                                foreach ($groupcoa as $data) {
                                    ?>
                                    <tr class="odd gradeA">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $data->Kode_Group_COA; ?></td>
                                        <td><?php echo $data->Nama_Group; ?></td>
                                        <td><?php echo $data->Normal_Balance; ?></td>
                                        <td><?php echo $data->Aktif; ?></td>
                                        <td class="text-center ukuran-logo">
                                            <span><a class="action-icons c-edit" href="<?php echo base_url()?>GroupCOA/edit_group_coa/<?php echo $data->IDGroupCOA ?>" title="Ubah Data">Edit</a></span>
                                            <span><a class="action-icons c-delete confirm" href="#open-modal<?php echo $data->IDGroupCOA ?>" title="Hapus Data">Delete</a></span>
                                        </td>
                                    </tr>
                                    <?php
                                    $i+=1;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ============ -->
<?php 
if ($groupcoa=="" || $groupcoa==null) {

}else{
    foreach ($groupcoa as $data) {
                                # code...
        ?>
        <div id="open-modal<?php echo $data->IDGroupCOA ?>" class="modal-window">
              <div>
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Konfirmasi Hapus</h1>
                <div class="modal-body">Apakah anda yakin akan menghapus data ini ?</div>
                <div class="modal-foot">
                    <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div>
                    <div class="btn_30_blue">
                        <a href="<?php echo base_url() ?>GroupCOA/hapus_group_coa/<?php echo $data->IDGroupCOA; ?>"><span>Hapus</span></a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
    }
} 
?> 

<?php $this->load->view('administrator/footer') ; ?>