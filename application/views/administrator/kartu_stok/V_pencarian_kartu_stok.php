<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
  /*  .table_top{
        display: none !important;
        }*/
        .chzn-container-single .chzn-single{
          height: 24px;
        }
      </style>
      <!-- tittle data -->
      <div class="page_title">
       <?php echo $this->session->flashdata('Pesan');?>
       <span class="title_icon"><span class="blocks_images"></span></span>
       <h3>Kartu Stok</h3>
       <div class="top_search">

       </div>
     </div>
     <!-- ======================= -->
     <?php
     $hari_ini = date("Y-m-d");
     $tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
     ?>
     <!-- body data -->
     <div id="content">
      <div class="grid_container">
        <div class="grid_12">
          <div class="widget_wrap">
            <div class="breadCrumbHolder module">
              <div id="breadCrumb0" class="breadCrumb module white_lin">
                <ul>
                  <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                  <li class="active">Kartu Stok</li>
                </ul>
              </div>
            </div>

            <div class="widget_content">
              <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>KartuStok/pencarian_kedua" class="form_container left_label">
                <ul>
                  <li class="px-1">

                    <div class="form_grid_12 w-100 alpha">

                      <!-- form 1 -->
                      <div class="form_grid_3 ">   
                        <label class="field_title mt-dot2">Periode</label>
                        <div class="form_input">
                          <input type="date" class="form-control input-date-padding-3-2" name="tanggal_mulai" value="<?php echo date('Y-m-d') ?>">
                        </div>
                      </div>

                      <!-- form 2 -->
                      <div class="form_grid_3 ">   
                        <label class="field_title mt-dot2"> S/D </label>
                        <div class="form_input">
                          <input type="date" class="form-control input-date-padding-3-2" name="tanggal_akhir" value="<?php echo $tgl_terakhir ?>">
                        </div>
                      </div>
                      <div class="form_grid_1"> 
                        <input type="text" class="form-control" name="custdes" value="<?php echo $cust ?>" required>
                      </div>

                      <div class="form_grid_2 ">   
                        <select name="jenispencarian" style="width: 100%!important" class="chzn-select input-date-padding-3-2" tabindex="13">
                          <option value="Grade">Grade</option>
                          <option value="Warna">Warna</option>
                          <option value="Barcode">Barcode</option>
                        </select>
                      </div>
                      <div class="form_grid_2">   
                        <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container ">
                      </div>
                      <div class="form_grid_1">
                        <div class="btn_24_blue">
                          <input type="submit" name="" value="search">
                        </div>
                      </div>

                      <span class="clear"></span>
                    </div>
                    <!-- end search -->

                  </li>

                </form>
              </div>
             <!--  <form action="<?php //echo base_url() ?>po/delete_multiple" method="post" id="ubahstatus"> -->
                <div class="widget_content">
                  <table class="display" id="action_tbl">
                   <thead>
                     <tr >
                      <th  rowspan="2">No</th>
                      <th rowspan="2">Tanggal</th>
                      <th rowspan="2">Jenis Faktur</th>
                      <th rowspan="2">Barcode</th>
                      <th rowspan="2">Corak</th>
                      <th rowspan="2">Grade</th>
                      <th rowspan="2">Warna</th>
                      <th colspan="2"><center>Masuk</center></th>
                      <th colspan="2"><center>Keluar</center></th>
                      <th colspan="2"><center>Saldo</center></th>
                      <th rowspan="2">Aksi</th>
                    </tr>
                    <tr>
                     <th>Yard</th>
                     <th>Meter</th>
                     <th>Yard</th>
                     <th>Meter</th>
                     <th>Yard</th>
                     <th>Meter</th>
                   </tr>
                 </thead>
                 <tbody>
                   <?php if(empty($kartustok)){
                    ?>
                    <?php
                  }else{
                    $i=1;
                    $k=1;
                    foreach ($kartustok as $data) {
                      if($k==1){
                       $Saldo_Yard_t = $data->Masuk_yard-$data->Keluar_yard;
                       $Saldo_Meter_t = $data->Masuk_meter-$data->Keluar_meter;
                       $k=2;
                     }else{
                       $Saldo_Yard_t= $Saldo_Yard_t+$data->Masuk_yard-$data->Keluar_yard;
                       $Saldo_Meter_t= $Saldo_Meter_t+$data->Masuk_meter-$data->Keluar_meter;
             // echo $data->Masuk_Yard ." ".$data->Keluar_Yard;
                     }
                     ?>
                     <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $data->Tanggal; ?></td>
                      <td><?php echo $data->Jenis_faktur; ?></td>
                      <td><?php echo $data->Barcode; ?></td>
                      <td><?php echo $data->Corak; ?></td>
                      <td><?php echo $data->Grade; ?></td>
                      <td><?php echo $data->Warna; ?></td>
                      <td><?php echo $data->Masuk_yard ?></td>
                      <td><?php echo $data->Masuk_meter ?></td>
                      <td><?php echo $data->Keluar_yard ?></td>
                      <td><?php echo $data->Keluar_meter ?></td>
                      <td><?php echo round($Saldo_Yard_t, 2) <= 0 ? intval(round($Saldo_Yard_t, 2)) : round($Saldo_Yard_t, 2) ?></td>
                      <td><?php echo round($Saldo_Meter_t, 2) <= 0 ? intval(round($Saldo_Meter_t, 2)) : round($Saldo_Meter_t, 2) ?></td>
                                  <td class="text-center" style="white-space: nowrap; display: flex;">
                                       <!--  <form method="POST" action="<?php echo base_url() ?>KartuStok/hapus_kartu_stok/<?php echo $data->IDKartuStok ?>" onsubmit="return confirm('Anda Yakin Menghapus Stok Ini ?');">
                                            <input type="hidden" name="idkartu" value="<?php echo $data->IDKartuStok ?>">
                                            <input type="hidden" name="barcode" value="<?php echo $data->Barcode ?>">
                                            <input type="hidden" name="custdes" value="<?php echo $data->Corak ?>">
                                            <input type="hidden" name="grade" value="<?php echo $data->Grade ?>">
                                            <input type="hidden" name="masukyard" value="<?php echo $data->Masuk_yard ?>">
                                            <input type="hidden" name="masukmeter" value="<?php echo $data->Masuk_meter ?>">
                                            <input type="hidden" name="keluaryard" value="<?php echo $data->Keluar_yard ?>">
                                            <input type="hidden" name="keluarmeter" value="<?php echo $data->Keluar_meter ?>">
                                            <button  data-toggle="tooltip" data-placement="bottom" title="Hapus Data">Hapus</button>
                                        </form> -->
                                        <?php echo "&nbsp"; ?>
                                        <form method="POST" action="<?php echo base_url() ?>KartuStok/print_kartu_stok/<?php echo $data->IDKartuStok ?>" target="__blank">
                                            <input type="hidden" name="idkartu" value="<?php echo $data->IDKartuStok ?>">
                                            <input type="hidden" name="barcode" value="<?php echo $data->Barcode ?>">
                                            <input type="hidden" name="custdes" value="<?php echo $data->Corak ?>">
                                            <input type="hidden" name="grade" value="<?php echo $data->Grade ?>">
                                            <input type="hidden" name="warnacust" value="<?php echo $data->Warna ?>">
                                            <input type="hidden" name="masukyard" value="<?php echo $data->Masuk_yard ?>">
                                            <input type="hidden" name="masukmeter" value="<?php echo $data->Masuk_meter ?>">
                                            <input type="hidden" name="keluaryard" value="<?php echo $data->Keluar_yard ?>">
                                            <input type="hidden" name="keluarmeter" value="<?php echo $data->Keluar_meter ?>">
                                            <input type="hidden" name="saldoyard" value="<?php echo round($Saldo_Yard_t, 2) <= 0 ? intval(round($Saldo_Yard_t, 2)) : round($Saldo_Yard_t, 2) ?>">
                                            <input type="hidden" name="saldometer" value="<?php echo round($Saldo_Meter_t, 2) <= 0 ? intval(round($Saldo_Meter_t, 2)) : round($Saldo_Meter_t, 2) ?>">
                                            <button data-toggle="tooltip" data-placement="bottom" title="Print Data">Print</button>
                                        </form>
                                      </td> 
                                      <?php
                                      $i++;
                                    }
                                  }
                                  ?>
                                </tbody>
                              </table>
                            </div>
                          <!-- </form> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php $this->load->view('administrator/footer') ; ?>
