
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style>
  body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    background-color: #FAFAFA;
    font: 10pt "Tahoma";
  }   
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
    html, body {
      width: 60mm;
      min-height: 60mm;
      transform : fit;        
    }
  }
</style>
</head>
<body>
  <!-- <body> -->
    <table style="font-size: 12px;width: 100%" border="0">
      <tbody>
        <tr colspan="2">
          <td style="font-size: 8px"><?php echo date('d/m/Y'); ?></td>
          <td colspan="5">
            <center style="margin-top: -5px;font-size: 10px;float: right"><?php echo $barcode ?></center>
          </td>
        </tr>
        <tr>
          <td  align="center" colspan="6"> <?php
          echo "<img style='width:200px;height:60px;margin-bottom: -5px;'  alt='barcode' src='" . base_url('asset/barcode.php?codetype=Code128&size=100&print=true&text='.$barcode) . "'/>";
          ?></b></td>
        </tr>
       <!--  <tr>
          <td>Cust</td> 
          <td>:</td>
          <td colspan="4"> <?php echo $kartu->Buyer ?></td>
        </tr> -->
        <!-- <tr>
          <td>SO</td> 
          <td>:</td> 
          <td><b><?php echo $noso ?></b> </td>
          <td>Batch</td>
          <td>:</td>
          <td><b><?php echo $kartu->Party ?></b></td>
        </tr> -->
        <!-- <tr>
          <td>Art.</td>
          <td>:</td> 
          <td colspan="4"><b><?php echo $kartu->Corak ?></b></td>
        </tr> -->
        <tr>
          <td>Color</td>
          <td>:</td>
          <td colspan="4"> <?php echo $warnacust ?></td>
        </tr>
         <tr>
          <td>Length</td>
          <td>:</td>
          <td><b><?php echo $saldoyard ?></b> yd</td>
          <td colspan="3" style="text-align: right; white-space: nowrap;"><b><?php echo $saldometer ?></b> m</td>
        </tr>
        <tr>
          <td>Grade</td>
          <td>:</td>
          <td><b><?php echo $grade ?></b></td>
        </tr>

      </tbody>
    </table>
    <!-- jQuery 3 -->
    <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
     window.print();
     console.log('masuk kesini');
      // window.close();
   });
 </script>
</script>
</body>
</html>