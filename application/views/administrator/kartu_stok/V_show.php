<?php $this->load->view('administrator/header') ; ?>
            <div class="content-wrapper">
                <div class="content">
                    <div class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-note2"></i>
                        </div>
                        <div class="header-title">
                            <h1>Detail Kartu Stok</h1>
                            <ol class="breadcrumb">
                                <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                                <li><a href="#">Data Kartu Stok</a></li>
                                <li class="active">Detail Kartu Stok</li>
                            </ol>
                        </div>
                    </div> <!-- /. Content Header (Page header) -->
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="panel panel-bd">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="user" class="table table-bordered table-striped">
                                            <tbody> 
                                                <tr>         
                                                    <td width="35%">Buyer</td>
                                                    <td width="65%"><?php echo $edit->Buyer ?></td>
                                                </tr>
                                                <tr>         
                                                    <td>Barcode</td>
                                                    <td><?php echo $edit->Barcode ?></td>
                                                </tr>  
                                                <tr>         
                                                    <td>No SO</td>
                                                    <td><?php echo $edit->NoSO ?></td>
                                                </tr>  
                                               <tr>         
                                                    <td>Party</td>
                                                    <td><?php echo $edit->Party ?></td>
                                                </tr> 
                                                <tr>         
                                                    <td>Indent</td>
                                                    <td><?php echo $edit->Indent;?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>CustDes</td>
                                                    <td><?php echo $edit->CustDes; ?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>WarnaCust</td>
                                                    <td><?php echo $edit->WarnaCust; ?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>Masuk Yard</td>
                                                    <td><?php echo $edit->Masuk_Yard; ?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>Masuk Meter</td>
                                                    <td><?php echo $edit->Masuk_Meter; ?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>Keluar Yard</td>
                                                    <td><?php echo $edit->Keluar_Yard; ?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>Keluar Meter</td>
                                                    <td><?php echo $edit->Keluar_Meter; ?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>Grade</td>
                                                    <td><?php echo $edit->Grade; ?></td>
                                                </tr>  
                                                <tr>         
                                                    <td>Remark</td>
                                                    <td><?php echo $edit->Remark; ?></td>
                                                </tr>  
                                                <tr>         
                                                    <td>Lebar</td>
                                                    <td><?php echo $edit->Lebar; ?></td>
                                                </tr>  
                                                <tr>         
                                                    <td>Satuan</td>
                                                    <td><?php echo $edit->Satuan; ?></td>
                                                </tr>  
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body__title">
                                <center>
            <a href="<?php echo base_url('KartuStok/index')?>"><button type="button" class="btn btn-demo">Kembali</button></a>
        </center>
        </div>
                        </div>
                    </div>
                </div> <!-- /.main content -->
            </div>
            <?php $this->load->view('administrator/footer') ; ?>