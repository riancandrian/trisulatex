<?php $this->load->view('administrator/header') ; 

if ($last_number->curr_number == null) {
    $number = 1;
} else {
    $number = $last_number->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen= $namaagent->Initial;

$code_booking = 'PBMJ'.date('y').$agen.$kodemax;
?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Penerimaan Barang Makloon Jahit</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>PenerimaanBarangJahit">Data Penerimaan Makloon Jahit</a></li>
              <li class="active">Tambah Penerimaan Barang Makloon Jahit</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Penerimaan Barang Makloon Jahit</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <!-- body content header -->
              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <!-- form half left body content header -->
                  <div class="form_grid_8 alpha">

                    <!-- form 1 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2">Tanggal</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                    <!-- form 2 -->
<!--                     <div class="form_grid_6 mb-1">
                      <label class="field_title">No Makloon Jahit</label>
                      <div class="form_input">
                        <input id="IDSJH" type="text" name="IDSJH" class="form-control" placeholder="No makloon jahit" required oninvalid="this.setCustomValidity('No makloon jahit Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <span class="clear"></span>
                      </div>
                    </div> -->

                     <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2">No Makloon Jahit</label>
                      <div class="form_input">
                        <select data-placeholder="Cari No Makloon Jahit" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDSJH" id="IDSJH" required oninvalid="this.setCustomValidity('No Makloon Jahit Tidak Boleh Kosong')" oninput="setCustomValidity('')" onchange="cek_supplier(); cek_surat()">
                          <option value=""></option>
                          <?php 
                          foreach ($nomakloonjahit as $jahit) {
                            ?>
                            <option value="<?php echo $jahit->Nomor ?>"><?php echo $jahit->Nomor; ?></option>
                            <?php
                          }
                          ?>
                        </select>
					
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2">No PBMJ</label>
                      <div class="form_input">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $code_booking; ?>">
                      </div>
                    </div>

                    <!-- form 4 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title">No Inv Supplier</label>
                      <div class="form_input">
                        <input id="Nomor_supplier" type="text" name="Nomor_supplier" class="form-control" placeholder="No Supplier" required oninvalid="this.setCustomValidity('Nomor Suppplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <span class="clear"></span>
                      </div>
                    </div>

                    <!-- form 5 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2">Supplier</label>
                      <div class="form_input">
                        <!-- <select data-placeholder="Cari Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDSupplier" id="IDSupplier" required oninvalid="this.setCustomValidity('Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <option value=""></option>
                          <?php 
                         // foreach ($supplier as $data) {
                            ?>
                            <option value="<?php// echo $data->IDSupplier ?>"><?php// echo $data->Nama; ?></option>
                            <?php
                        //  }
                          ?>
                        </select> -->
                        <input type="hidden" name="IDSupplier" id="IDSupplier" readonly>
                         <input type="text" name="NamaSupplier" id="NamaSupplier" class="form-control" readonly>
						 
                      </div>
                    </div>

                    <!--div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2">No Surat Jalan</label>
                      <div class="form_input">
                        <select data-placeholder="Cari No Makloon Jahit" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDSJ" id="IDSJ" required oninvalid="this.setCustomValidity('No Makloon Jahit Tidak Boleh Kosong')" oninput="setCustomValidity('')" onchange="cek_supplier()">
                          <option value=""></option>
                          <?php 
                          foreach ($nomakloonjahit as $jahit) {
                            ?>
                            <option value="<?php echo $jahit->Nomor ?>"><?php echo $jahit->Nomor; ?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div-->

                    <!-- form 6 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2">Total Qty</label>
                      <div class="form_input">
                        <input id="Total_qty" type="text" name="Total_qty" style="cursor: no-drop;" class="form-control" placeholder="Total Qty" required oninvalid="this.setCustomValidity('Total Qty Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                        <span class="clear"></span>
                      </div>
                    </div>

                  </div>

                  <!-- form half right body content header -->
                  <!-- form area -->
                  <div class="form_grid_4">
                    <label class="field_title">Keterangan</label>
                    <div class="form_input">
                      <textarea id="Keterangan" type="text" name="Keterangan" class="form-control" placeholder="Keterangan" required oninvalid="this.setCustomValidity('Keterangan Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="resize: none;" rows="8"></textarea>
                    </div>
                  </div>

                  <!-- clear content -->
                  <div class="clear"></div>
                </div>
              </li>
              <!-- body content body -->
              <li>

                <div class="form_grid_12 w-100 alpha">

                  <!-- form 1 -->
                  <div class="form_grid_6 mb-1">
                    <label class="field_title mt-dot2">Nama Barang</label>
                    <div class="form_input">
                      <select data-placeholder="Cari Barang" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDBarang"  id="IDBarang" required oninvalid="this.setCustomValidity('Nama Barang Tidak Boleh Kosong')" onchange="cek_barang()" oninput="setCustomValidity('')">
                        <option value=""></option>
                        <?php 
                        foreach ($barang as $data) {
                          ?>
                          <option value="<?php echo $data->IDBarang ?>" data-namabarang=<?php echo $data->Nama_Barang ?>><?php echo $data->Nama_Barang; ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <!-- form 2 -->
                  <div class="form_grid_6 mb-1">
                    <label class="field_title mt-dot2">Qty</label>
                    <div class="form_input">
                      <input type="text" id="Qty" class="form-control" name="Qty" placeholder="Qty" required oninvalid="this.setCustomValidity('Qty Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                    </div>
                  </div>

                  <!-- form 3 -->
                  <div class="form_grid_6 mb-1">
                    <label class="field_title mt-dot2">Merk</label>
                    <div class="form_input">
                      <!-- <select data-placeholder="Cari Merk" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDMerk" id="IDMerk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <option value=""></option>
                        <?php 
                        //foreach ($merk as $data) {
                          ?>
                          <option value="<?php //echo $data->Merk ?>"><?php// echo $data->Merk; ?></option>
                          <?php
                        //}
                        ?>
                      </select> -->
                        <input type="hidden" id="IDMerk" class="form-control" name="IDMerk" placeholder="Merk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                        <input type="text" id="NamaMerk" class="form-control" name="NamaMerk" placeholder="Nama Merk" required oninvalid="this.setCustomValidity('Nama Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                    </div>
                  </div>

                  <!-- form 4 -->
                  <div class="form_grid_6 mb-1">
                    <label class="field_title mt-dot2">Satuan</label>
                    <div class="form_input">
                      <!-- <select data-placeholder="Cari Satuan" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDSatuan" id="IDSatuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <option value=""></option>
                        <?php 
                       // foreach ($satuan as $data) {
                          ?>
                          <option value="<?php// echo $data->Satuan ?>"><?php //echo $data->Satuan; ?></option>
                          <?php
                        //}
                        ?>
                      </select> -->
                       <input type="hidden" id="IDSatuan" class="form-control" name="IDSatuan" placeholder="Satuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                       <input type="text" id="NamaSatuan" class="form-control" name="NamaSatuan" placeholder="Nama Satuan" required oninvalid="this.setCustomValidity('Nama Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                    </div>
                  </div>

                  <div class="form_grid_6 mb-1">
                    <label class="field_title mt-dot2">Harga</label>
                    <div class="form_input">
                      <input type="text" id="harga-123" class="form-control" name="Harga" placeholder="Harga" required oninvalid="this.setCustomValidity('Qty Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                    </div>
                  </div>

                  <!-- clear content -->
                  <div class="clear"></div>
                </div>

              </li>
              <li>
                <div class="form_grid_12 text-center">
                  <div class="btn_30_blue">
                    <!--   <div class="btn_30_light">
                      <span> <a href="<?php //echo base_url() ?>Piutang" name="simpan">Kembali</a></span>
                    </div> -->
                    <center>

                      <button class="btn_24_blue" type="button" id="add-to-cart">
                        Tambah Data
                      </button>

                    </center>

                  </div>
                </div>
              </li>
              <div>
               <table class="display data_tbl" id="tabelfsdfsf">
                <thead>
                  <tr>
                    <th class="center" style="width: 40px">No</th>
                    <th>Nama Barang</th>
                    <th>Merk</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Total Harga</th>
                    <th style="width: 50px">Action</th>
                  </tr>
                </thead>
                <tbody id="tabel-cart-pb-jahit">

                </tbody>
              </table>
            </div>
			<div><!--Detail dari surat jalan-->
				<table class="display data_tbl" id="tabelsuratjalan">
					<thead>
						<tr>
							<th class="center" style="width: 40px">No</th>
							<th>Barcode</th>
							<th>Corak</th>
							<th>Warna</th>
							<th>Merk</th>
							<th>Qty Yard</th>
							<th>Qty Meter</th>
							<th>Grade</th>
							<th>Satuan</th>
						</tr>
					</thead>
					<tbody id="tabel-cart-surat-jahit">

					</tbody>
				</table>
			</div>


          </ul>
        </form>
        <div class="widget_content px-2 text-center">
          <div class="py-4 mx-2">
            <div class="btn_30_blue">
              <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
            </div>
            <div class="btn_30_blue">
              <span><a class="btn_24_blue" id="simpan" style="cursor: pointer;" onclick="savepbjahit()">Simpan</a></span>
            </div>

          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div id="basic-modal-content">
  <h3>Ubah Detail Penerimaan Barang Jahit</h3>
  <form class="form_container left_label">
   
    <div class="clear"></div><br>
    <div class="form_grid_12">
      <div class="form_grid_3">
        <label class="field_title">Barang</label>
      </div>
      <div class="form_grid_9">
        <div class="form_input">
          <select data-placeholder="Cari Barang" style="width:100%;" class="" tabindex="13" name="vbarang" id="vbarang"></select>
        </div>
      </div>
    </div>
    <div class="clear"></div><br>

    <div class="form_grid_12">
      <div class="form_grid_3">
        <label class="field_title">Merk</label>
      </div>
      <div class="form_grid_9">
        <div class="form_input">
          <select data-placeholder="Cari Merk" style="width:100%;" class="" tabindex="13" name="vmerk" id="vmerk"></select>
        </div>
      </div>
    </div>
    <div class="clear"></div><br>
    
    <div class="form_grid_12">
      <div class="form_grid_3">
        <label class="field_title">Qty</label>
      </div>
      <div class="form_input form_grid_9">
        <input type="text" class="form-control" name="vqty" id="vqty" required>
      </div>
    </div>
    <div class="clear"></div><br>
    <div class="form_grid_12">
      <div class="form_grid_3">
        <label class="field_title">Satuan</label>
      </div>
      <div class="form_grid_9">
        <div class="form_input">
          <select data-placeholder="Cari Satuan" style="width:100%;" class="" tabindex="13" name="vsatuan" id="vsatuan"></select>
        </div>
      </div>
    </div>
    <div class="clear"></div><br>
    
    <input type="hidden" name="vindex" id="vindex">
    <div class="btn_24_blue">
      <a href="#" onclick="save_tmp_create()" class="simplemodal-close"><span>Simpan</span></a>
    </div>
  </form>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/penerimaan_barang_jahit.js"></script>



<?php $this->load->view('administrator/footer') ; ?>