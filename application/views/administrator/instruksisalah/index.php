<?php $this->load->view('administrator/header'); 
$array_kategori = array('','Tanggal','Nomor','IDSupplier', 'Nomor_sj', 'IDPO','NoSO','Tanggal_kirim');
?>
<div class="page_title">
	<?php echo $this->session->flashdata('Pesan');?>
	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Instruksi Pengiriman</h3>
	<div class="top_search">
	</div>
</div>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<!-- <li><a href="<?php //echo base_url() ?>Hutang">Data Instruksi</a></li> -->
							<li class="active">Data Instruksi</li>
						</ul>
					</div>
				</div>
				<div class="widget_content">
					<form action="<?php echo base_url('InstruksiPengiriman/filter'); ?>" method="post" class="form_container left_label">
						<ul>
							<li class="px-1">
								<div class="form_grid_12 w-100 alpha">
									<div class="form_grid_3 mb-1">

										<div class="btn_24_blue">
											<a href="<?php echo base_url('InstruksiPengiriman/input_instruksi')?>"><span>Tambah Data</span></a>
										</div>
									</div>
									<div class="form_grid_9 mb-1">
										<div class="form_grid_3 mb-1">                        
											<label class="field_title mt-dot2">Periode</label>
											<div class="form_input">
												<input type="text" class="form-control date-picker hidden-sm-down datepicker" name="date_from" placeholder="mm/dd/yyyy">
											</div>
										</div>
										<div class="form_grid_3 mb-1">                        
											<label class="field_title mt-dot2">s/d</label>
											<div class="form_input">
												<input type="text" class="form-control date-picker hidden-sm-down datepicker" name="date_until" placeholder="mm/dd/yyyy">
											</div>
										</div>
										<div class="form_grid_3 mb-1">
											<select name="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13">
												<?php foreach ($array_kategori as $key): ?>
													<?php if ($key==$kategori): ?>
														<option value="<?php echo $key ?>" selected><?php echo $key ?></option>
													<?php else: ?>
														<option value="<?php echo $key ?>"><?php echo $key ?></option>
													<?php endif ?>
												<?php endforeach ?>
											</select>
										</div>
										<div class="form_grid_2 mb-1">
											<input type="text" class="form-control" placeholder="Cari Berdasarkan" name="keyword" value="<?php echo $keyword; ?>">
										</div>
										<div class="form_grid_1 mb-1">
											<div class="btn_24_blue">
												<input type="submit" name="" value="search">
											</div>
										</div>
									</div>

									<span class="clear"></span>
								</div>
							</li>
						</ul>
					</form>
					<div style="position: absolute; z-index: 2; top: 32.5%;left: 15px;">
						<input type="checkbox" id="ceklis" onclick="cek()"><span id="ketcheck">Check All</span>
					</div>
					<form action="<?php echo base_url('InstruksiPengiriman/batal'); ?>" method="post" class="form_container left_label">	
						<table class="display data_tbl">
							<thead>
								<tr>
									<th>No</th>
									<th>Do</th>
									<th>Tanggal</th>
									<th>No Instruksi</th>
									<th>Supplier</th>
									<th>No SJCk</th>
									<th>Corak</th>
									<th>Tanggal Kirim</th>
									<th>Total Yard</th>
									<th>Total Meter</th>
									<th>Status</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody id="tabel_detail_tampungan">
								<?php if ($daftar_instruksi==null): ?>
								<?php else: ?>
									<?php $i=0; foreach ($daftar_instruksi as $instruksi): $i++; ?>
									<tr>
										<td><?php echo $i; ?></td>
										<td class="text-center ukuran-logo">
											<input type="checkbox" name="do[]" class="doall" value="<?php echo $instruksi->IDIP; ?>">
										</td>
										<td><?php echo $instruksi->Tanggal; ?></td>
										<td><?php echo $instruksi->Nomor; ?></td>
										<td><?php echo $instruksi->IDSupplier; ?></td>
										<td><?php echo $instruksi->Nomor_sj; ?></td>
										<td><?php echo 'corak' ?></td>
										<td><?php echo $instruksi->Tanggal_kirim; ?></td>
										<td><?php echo $instruksi->Total_yard; ?></td>
										<td><?php echo $instruksi->Total_meter; ?></td>
										<td><?php echo $instruksi->Batal; ?></td>
										<td class="text-center ukuran-logo">
											<span><a class="action-icons c-Detail" href="<?php echo base_url('InstruksiPengiriman/detail/'.$instruksi->IDIP); ?>" title="Detail">Detail</a></span>
											<?php if ($instruksi->Batal=="Tidak Aktif"): ?>
												<span><a class="action-icons c-Aktif" href="<?php echo base_url('InstruksiPengiriman/aktif/'.$instruksi->IDIP); ?>" title="Aktif">Aktif</a></span>
											<?php else: ?>
												<span><a class="action-icons c-Delete" href="<?php echo base_url('InstruksiPengiriman/hapus/'.$instruksi->IDIP); ?>" title="Batal">Batal</a></span>
											<?php endif ?>
											<span><a class="action-icons c-edit" href="<?php echo base_url('InstruksiPengiriman/edit/'.$instruksi->IDIP); ?>" title="Ubah Data">Edit</a></span>
										</td>
									</tr>
								<?php endforeach ?>
							<?php endif ?>
						</tbody>
					</table>
					<div class="widget_content text-right px-2">
						<div class="py-4 mx-2">

							<div class="btn_24_blue">
								<input type="submit" value="Batal">
							</div>
							<!-- <div class="btn_24_blue">
								<a href="#"><span>Kembali</span></a>
							</div> -->
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>
<script type="text/javascript">
	function cek(){
		var x = document.getElementById("ceklis").checked;
		if(x == true){
			console.log("x");
			document.getElementById("ketcheck").innerHTML= ' Uncheck All';
			$('.doall').prop('checked', true);
		}else{
			console.log("x");
			document.getElementById("ketcheck").innerHTML= ' Check All';
			$('.doall').prop('checked', false);
		}
	}
</script>