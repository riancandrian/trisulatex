<?php $this->load->view('administrator/header'); ?>
<div class="page_title">
	<?php echo $this->session->flashdata('Pesan');?>
	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Data Instruksi</h3>
	<div class="top_search">
	</div>
</div>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<li><a href="<?php echo base_url() ?>InstruksiPengiriman">Data Instruksi</a></li>
							<li class="active">Edit Instruksi</li>
						</ul>
					</div>
				</div>
				<div class="widget_top">
					<div class="grid_12">
						<span class="h_icon blocks_images"></span>
						<h6>Edit Data Instruksi</h6>
					</div>
				</div>
				<div class="widget_content">
					<form method="post" action="<?php echo base_url('InstruksiPengiriman/update/'.$instruksi->IDIP); ?>" class="form_container left_label">
						<ul>
							<li>
								<div class="multiline">
									<!-- Tanggal -->
									<label class="field_title">Tanggal</label>
									<div class="form_input">
										<input type="text" class="form-control datepicker" value="<?php echo $instruksi->Tanggal; ?>" name="tanggal" required readonly>
									</div>
									<!-- No Instruksi -->
									<label class="field_title">No Instruksi</label>
									<div class="form_input">
										<input type="text" class="form-control" placeholder="Nomor Instruksi" value="<?php echo $instruksi->Nomor	;?>" name="no_instruksi" required readonly>
									</div>
									<!-- Supplier -->
									<label class="field_title">Supplier</label>
									<div class="form_input">
										<select data-placeholder="Pilih Supplier" name="supplier" required style=" width:100%;" class="chzn-select" tabindex="13" readonly>
											<?php foreach ($data_supplier as $suplier): ?>
												<?php if ($suplier->Kode_Suplier==$instruksi->IDSupplier): ?>
													<option selected value="<?php echo $suplier->Kode_Suplier; ?>"><?php echo '('.$suplier->Kode_Suplier.') '.$suplier->Nama; ?></option>
													<?php else: ?>
														<option value="<?php echo $suplier->Kode_Suplier; ?>"><?php echo '('.$suplier->Kode_Suplier.') '.$suplier->Nama; ?></option>
													<?php endif ?>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</li>
								<li>
									<div class="multiline">
										<!-- No SJC -->
										<label class="field_title">No SJC</label>
										<div class="form_input">
											<select data-placeholder="Pilih No SJC" name="in_id_sjc" id="in_id_sjc" required style=" width:100%;" class="chzn-select" tabindex="13" readonly>
												<!-- <option value="">pilih</option> -->
												<?php foreach ($daftarsjc as $sjc): ?>
													<option value="<?php echo $sjc->Nomor_sj ?>"><?php echo $sjc->Nomor_sj ?></option>
												<?php endforeach ?>
											</select>
											<span><a class='action-icons c-Search' href='#' title='Cari' onclick='carisjc()'></a></span>
										</div>
										<!-- No PO -->
										<label class="field_title">No PO</label>
										<div class="form_input">
											<input type="text" readonly class="form-control" value="<?php echo $instruksi->IDPO; ?>" name="no_po" id="no_po" required>
										</div>
										<!-- No SO -->
										<label class="field_title">No SO</label>
										<div class="form_input">
											<input type="text" readonly class="form-control" value="<?php echo $instruksi->NoSO; ?>" name="no_so" id="no_so" required>
										</div>
									</div>
								</li>
								<li>
									<div class="multiline">
										<!-- Tanggal Kirim -->
										<label class="field_title">Tanggal Kirim</label>
										<div class="form_input">
											<input type="text" class="form-control datepicker" value="<?php echo $instruksi->Tanggal_kirim; ?>" name="tanggal_kirim" required readonly>
										</div>
										<!-- Total Yard -->
										<label class="field_title">Total Yard</label>
										<div class="form_input">
											<input type="text" readonly class="form-control" value="<?php echo $instruksi->Total_yard; ?>" name="total_yard" id="total_yard" required>
										</div>
										<!-- No SO -->
										<label class="field_title">Total Meter</label>
										<div class="form_input">
											<input type="text" readonly class="form-control" value="<?php echo $instruksi->Total_meter; ?>" name="total_meter" id="total_meter" required>
										</div>
									</div>
								</li>
							</ul>
							<table class="display">
								<thead>
									<tr>
										<th>No</th>
										<th>Barcode</th>
										<th>Corak</th>
										<th>Warna</th>
										<th>Merk</th>
										<th>Qty Yard</th>
										<th>Qty Meter</th>
										<th>Grade</th>
										<th>Satuan</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody id="tabel_detail_tampungan">
									<?php $i=0; foreach ($data_tabel as $data): $i++;?>
									<tr class='row_tampungan' id='row_tampungan<?php echo $i; ?>'>
										<td><input type='hidden' id='IDIPDetail<?php echo $i ?>' value='<?php echo $data->IDIPDetail; ?>'><?php echo $i ?></td>
										<td><?php echo $data->Barcode; ?></td>
										<td><?php echo $data->IDCorak; ?></td>
										<td><?php echo $data->IDWarna; ?></td>
										<td><?php echo $data->IDMerk; ?></td>
										<td><input type="hidden" id="Qty_yard<?php echo $i ?>" value="<?php echo $data->Qty_yard; ?>"><?php echo $data->Qty_yard; ?></td>
										<td><input type="hidden" id="Qty_meter<?php echo $i ?>" value="<?php echo $data->Qty_meter; ?>"><?php echo $data->Qty_meter; ?></td>
										<td><?php echo $data->Grade; ?></td>
										<td><?php echo $data->IDSatuan; ?></td>
										<td class='text-center ukuran-log'>
											<span><a class='action-icons c-Delete' href='#' title='Delete' onclick='deleteItemawal(<?php echo $i; ?>)'>Delete</a></span>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						<table id="tabel_hapus"></table>
						<div class="btn_24_blue">
							<input type="submit" value="simpan">
							<a href="<?php echo base_url('InstruksiPengiriman/print/'.$instruksi->IDIP)?>"><span>Print</span></a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script> -->
<?php $this->load->view('administrator/footer') ; ?>
<script type="text/javascript">
	var i=0;
	var array_yard = [];
	var array_meter = [];
	var total_yard = 0;
	var total_meter = 0;
	
	base_url = window.location.pathname.split( '/' );
	function carisjc(){
		id_sjc = $('#in_id_sjc').val();
		$.ajax({
			url: '/'+base_url[1]+'/InstruksiPengiriman/cari_sjc',
			type: "POST",
			data: {
				"id_sjc" :  id_sjc
			},
			dataType: "json",
			success: function (data,msg,response,status) {
        // console.log(data);
        document.getElementById("no_so").value = data.dataPOSO.NoSO;
        document.getElementById("no_po").value = data.dataPOSO.NoPO;
        total_yard = 0;
        total_meter = 0;
        array_yard = [];
        array_meter = [];
        i=0;
        $('.row_tampungan').remove();
        $.each(data.dataTabel, function(idx, obj) {
        	array_yard[i]= parseInt(obj.Qty_yard);
        	array_meter[i]= parseInt(obj.Qty_meter);
        	i+=1;
        	var value =
        	"<tr class='row_tampungan' id='row_tampungan"+i+"'>" +
        	"<td><input type='hidden' name='IDT[]' value='"+obj.IDT+"'>"+i+"</td>"+
        	"<td>"+obj.Barcode+"</td>"+
        	"<td>"+obj.Corak+"</td>"+
        	"<td>"+obj.Warna+"</td>"+
        	"<td>merek</td>"+
        	"<td>"+obj.Qty_yard+"</td>"+
        	"<td>"+obj.Qty_meter+"</td>"+
        	"<td>"+obj.Grade+"</td>"+
        	"<td>"+obj.Satuan+"</td>"+
        	"<td class='text-center ukuran-log'>" +
        	"<span><a class='action-icons c-Delete' href='#' title='Delete' onclick='deleteItem("+i+")'>Delete</a></span>"+
        	"</td>"+
        	"</tr>";
        	$("#tabel_detail_tampungan").append(value);
        });
        for(x=0; x<array_yard.length; x++){
        	total_yard += parseInt(array_yard[x]);
        	total_meter += parseInt(array_meter[x]);
        }
        // console.log(data);
        document.getElementById("total_yard").value = total_yard;
        document.getElementById("total_meter").value = total_meter;
    },
    error: function(data,msg,response,status){
    	console.log("Failure"+data+msg+response+status);
    }
});
	}

	function deleteItem(i){
		$('#row_tampungan'+i).remove();
		array_yard.splice(i-1, 1);
		array_meter.splice(i-1, 1);
		total_yard = 0;
		total_meter = 0;
		for(x=0; x<array_yard.length; x++){
			total_yard += parseInt(array_yard[x]);
			total_meter += parseInt(array_meter[x]);
		}
		document.getElementById("total_yard").value = total_yard;
		document.getElementById("total_meter").value = total_meter;
	}
	function deleteItemawal(i){
		var value =
		"<tr>" +
		"<td><input type='hidden' name='hapusIDIPDetail[]' value='"+parseInt($('#IDIPDetail'+i).val())+"'></td>"+
		"</tr>";
		$("#tabel_hapus").append(value);
		id_total_yard = parseInt($('#total_yard').val())-parseInt($('#Qty_yard'+i).val());
		id_total_meter = parseInt($('#total_meter').val())-parseInt($('#Qty_meter'+i).val());
		document.getElementById("total_yard").value = id_total_yard;
		document.getElementById("total_meter").value = id_total_meter;
		$('#row_tampungan'+i).remove();
	}
</script>