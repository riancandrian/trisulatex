<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Hutang</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Hutang">Data Hutang</a></li>
                            <li class="active">Edit Hutang</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Data Hutang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Hutang/update" class="form_container left_label">
                        <input type="hidden" name="id" value="<?php echo $hutang->IDHutang?>">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <label class="field_title">Supplier</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="supplier" required oninvalid="this.setCustomValidity('Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            <option value=""></option>
                                            <?php foreach ($suppliers as $row) : ?>
                                                <option value="<?php echo $row->IDSupplier ?>" <?php echo $row->IDSupplier == $hutang->IDSupplier ? 'selected' : '' ?>><?php echo $row->Nama ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <label class="field_title">Tanggal Hutang</label>
                                    <div class="form_input">

                                     <input type="text" name="tanggal_hutang" class="form-control date-picker hidden-sm-down" value="<?php echo $hutang->Tanggal_Hutang; ?>" required oninvalid="this.setCustomValidity('Tanggal Hutang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                     <span class="clear"></span>
                                 </div>
                                 <label class="field_title">Jatuh Tempo</label>
                                 <div class="form_input">
                                     <input type="text" name="jatuh_tempo" class="form-control date-picker hidden-sm-down" value="<?php echo $hutang->Jatuh_Tempo;?>" required oninvalid="this.setCustomValidity('Jatuh Tempo Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                     <span class="clear"></span>
                                 </div>
                                 <label class="field_title">No Faktur</label>
                                 <div class="form_input">
                                    <input type="text" class="form-control" name="no_faktur" value="<?php echo $hutang->No_Faktur?>" required oninvalid="this.setCustomValidity('No Faktur Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    <span class="clear"></span>
                                </div>

                                <label class="field_title">Nilai Hutang</label>
                                <div class="form_input">
                                    <span class="input-group-addon">Rp. </span>
                                    <input type="text" class="form-control price-date" name="nilai_hutang" id="tanpa-rupiah-debit" value="<?php echo convert_currency($hutang->Nilai_Hutang) ?>" required oninvalid="this.setCustomValidity('Nilai Hutang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    <span class="clear"></span>
                                </div>

                                <label class="field_title">Saldo Awal</label>
                                <div class="form_input">
                                    <span class="input-group-addon">Rp. </span>
                                    <input type="text" class="form-control price-date" name="saldo_awal" value="<?php echo convert_currency($hutang->Saldo_Awal);?>" required oninvalid="this.setCustomValidity('Saldo Awal Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                    <span class="clear"></span>
                                </div>
                               <!--  <label class="field_title">Pembayaran</label>
                                <div class="form_input">
                                   <span class="input-group-addon">Rp. </span> -->
                                   <input type="hidden" class="form-control price-date" name="pembayaran" id="tanpa-rupiah-kredit" value="0" required oninvalid="this.setCustomValidity('Pembayaran Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                  <!--  <span class="clear"></span>
                               </div> -->
                               <label class="field_title">Saldo Akhir</label>
                               <div class="form_input">
                                <span class="input-group-addon">Rp. </span>
                                <input type="text" class="form-control price-date" name="saldo_akhir" value="<?php echo convert_currency($hutang->Saldo_Akhir); ?>" required oninvalid="this.setCustomValidity('Saldo Akhir Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly> 
                                <span class="clear"></span>
                            </div>

                        </div>
                    </li>
                    <li>
                        <div class="form_grid_12">
                            <div class="form_input">
                                <div class="btn_30_light">
                                 <span> <a href="<?php echo base_url() ?>Hutang" name="simpan">Kembali</a></span>
                             </div>
                             <div class="btn_30_blue">
                              <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                          </div>
                      </div>
                  </div>
              </li>
          </ul>
      </form>
  </div>
</div>
</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>