<?php $this->load->view('administrator/header') ; 

?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Hutang</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Hutang">Data Hutang</a></li>
                            <li class="active">Tambah Hutang</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Hutang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Hutang/simpan" class="form_container left_label">
                       <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <label class="field_title">Supplier</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="supplier" required oninvalid="this.setCustomValidity('Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            <option value=""></option>
                                            <?php foreach ($suppliers as $row) : ?>
                                                <option value="<?php echo $row->IDSupplier ?>"><?php echo $row->Nama ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <label class="field_title">Tanggal Hutang</label>
                                    <div class="form_input">
                                     <input type="date" name="tanggal_hutang" class="form-control date-picker hidden-sm-down" placeholder="Tanggal Hutang" required oninvalid="this.setCustomValidity('Tanggal Hutang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                     <span class="clear"></span>
                                 </div>
                                 <label class="field_title">Jatuh Tempo</label>
                                 <div class="form_input">
                                   <input type="date" name="jatuh_tempo" class="form-control date-picker hidden-sm-down" placeholder="Jatuh Tempo" required oninvalid="this.setCustomValidity('Jatuh Tempo Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                   <span class="clear"></span>
                               </div>
                               <label class="field_title">No Faktur</label>
                               <div class="form_input">
                                <input type="text" class="form-control" name="no_faktur" placeholder="Nomor Faktur" required oninvalid="this.setCustomValidity('No Faktur Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                <span class="clear"></span>
                            </div>

                            <label class="field_title">Nilai Hutang</label>
                            <div class="form_input">
                              <span class="input-group-addon" style="">Rp. </span>
                                <input type="text" class="form-control price-date" name="nilai_hutang" id="tanpa-rupiah-debit" placeholder="Nilai Hutang" required oninvalid="this.setCustomValidity('Nilai Hutang Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                                <span class="clear"></span>
                            </div>

                           <!--  <label class="field_title">Pembayaran</label>
                            <div class="form_input">
                               <span class="input-group-addon">Rp. </span> -->
                               <input type="hidden" class="form-control price-date" name="pembayaran" id="tanpa-rupiah-kredit" placeholder="Pembayaran" required oninvalid="this.setCustomValidity('Pembayaran Tidak Boleh Kosong')" value="0" oninput="setCustomValidity('')"/>
                               <!-- <span class="clear"></span>
                           </div> -->

                          <!--  <label class="field_title">Jenis Faktur</label> -->
                           <!-- <div class="form_input"> -->
                             <input type="hidden" class="form-control" name="jenis_faktur" placeholder="Jenis Faktur" value="SA" readonly>
                             <!-- <span class="clear"></span>
                         </div> -->

                     </div>
                 </li>
                 <li>
                    <div class="form_grid_12">
                        <div class="form_input">
                            <div class="btn_30_light">
                             <span> <a href="<?php echo base_url() ?>Hutang" name="simpan">Kembali</a></span>
                         </div>
                         <div class="btn_30_blue">
                          <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                      </div>
                  </div>
              </div>
          </li>
      </ul>
  </form>
</div>
</div>
</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>