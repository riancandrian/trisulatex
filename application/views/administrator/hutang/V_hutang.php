<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
  /*  .table_top{
        display: none !important;
        }*/
    </style>
    <!-- tittle data -->
    <div class="page_title">
     <?php echo $this->session->flashdata('Pesan');?>
     <span class="title_icon"><span class="blocks_images"></span></span>
     <h3>Data Hutang</h3>
     <div class="top_search">

     </div>
 </div>
 <!-- ======================= -->

 <!-- body data -->
 <div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li class="active">Data Hutang</li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Hutang/pencarian" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                              <div class="form_grid_12 w-100 px">
                                <div class="form_grid_4 mx">
                                    <div class="btn_24_blue">
                                        <a href="<?php echo base_url('Hutang/tambah_hutang')?>"><span>Tambah Data</span></a>
                                    </div>
                                </div>

                                <div class="form_grid_2">
                                    <select data-placeholder="Cari Berdasarkan" name="jenispencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                        <option value=""></option>
                                        <option value="No_Faktur">No Faktur</option>
                                        <option value="Nama">Nama Supplier</option>
                                    </select>
                                </div>
                                   <!--  <div class="form_grid_2">
                                        <select data-placeholder="Cari Berdasarkan" name="statuspencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value="">Pilih Status</option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                        </select>
                                    </div> -->
                                    <div class="form_grid_5">
                                        <input name="keyword" type="text">
                                        <!-- <span class=" label_intro">Last Name</span> -->
                                    </div>
                                    <div class="form_grid_1 flot-right">
                                        <div class="btn_24_blue">
                                            <input type="submit" name="" value="search">
                                        </div>
                                    </div>
                                    <span class="clear"></span>

                                </div>
                            </li>
                        </form>
                    </div>
                    <div class="widget_content">
                        <table class="display data_tbl">
                            <thead>
                                <tr>
                                   <th>No</th>
                                   <th>No Faktur</th>
                                   <th>Suplier</th>
                                   <th>Tanggal Hutang</th>
                                   <th>Jatuh Tempo</th>
                                   <th>Nilai Hutang</th>
                                   <th>Saldo Akhir</th>
                                   
                                   <th>Aksi</th>
                               </tr>
                           </thead>
                           <tbody>
                            <?php if(empty($hutang)){
                                ?>
                                <?php
                            }else{
                                $i=1;
                                foreach ($hutang as $data) {
                                    ?>
                                    <tr class="odd gradeA">
                                      <td><?php echo $i; ?></td>
                                      <td><?php echo $data->No_Faktur; ?></td>
                                      <td><?php echo $data->Nama; ?></td>
                                      <td><?php echo $data->Tanggal_Hutang; ?></td>
                                      <td><?php echo $data->Jatuh_Tempo; ?></td>
                                      <td><?php echo 'Rp.'.convert_currency($data->Nilai_Hutang); ?></td>
                                      <td><?php echo 'Rp.'.convert_currency($data->Saldo_Akhir); ?></td>
                                      
                                      <td class="text-center ukuran-logo">
                                        <span><a class="action-icons c-Detail" href="<?php echo base_url('Hutang/show/'.$data->IDHutang)?>" title="Detail Data">Detail</a></span>
                                        <span><a class="action-icons c-edit" href="<?php echo base_url('Hutang/edit/'.$data->IDHutang)?>" title="Ubah Data">Edit</a></span>
                                        <span><a class="action-icons c-delete" href="#open-modal<?php echo $data->IDHutang ?>" title="Hapus Data">Delete</a></span>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                            <!-- <tr class="gradeA">
                                <td>Trident</td>
                                <td>Internet Explorer 4.0</td>
                                <td>Win 95+</td>
                                <td class="center">4</td>
                                <td class="center">X</td>
                            </tr> -->
                        </tbody>
                        <!-- <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                        </tfoot> -->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
if ($hutang=="" || $hutang==null) {
}else{
    foreach ($hutang as $data) {
        ?>
        <div id="open-modal<?php echo $data->IDHutang ?>" class="modal-window">
          <div>
            <a href="#modal-close" title="Close" class="modal-close">X</a>
            <h1>Konfirmasi Hapus</h1>
            <div class="modal-body">Apakah anda yakin akan menghapus data ini ?</div>
            <div class="modal-foot">
                <div class="btn_30_light">
                    <a href="#modal-close" title="Close"><span>Batal</span></a>
                </div>
                <div class="btn_30_blue">
                    <a href="<?php echo base_url('Hutang/delete/'.$data->IDHutang)?>"><span>Hapus</span></a>
                </div>
            </div>
        </div>
    </div>
    <?php 
}
} 
?> 
<?php $this->load->view('administrator/footer') ; ?>
