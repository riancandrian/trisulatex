<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Hutang</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('Hutang/index')?>">Data Hutang</a></li>
                            <li style="text-transform: capitalize;"> Detail Hutang</b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                           <tbody> 
                                                <tr>         
                                                    <td width="35%" style="background-color: #ffffff;">Tanggal Hutang</td>
                                                    <td width="65%" style="background-color: #ffffff;"><?php echo indonesian_date($hutang->Tanggal_Hutang) ?></td>
                                                </tr>
                                                <tr>         
                                                    <td style="background-color: #e5eff0;">Jatuh Tempo</td>
                                                    <td style="background-color: #e5eff0;"><?php echo indonesian_date($hutang->Jatuh_Tempo)?></td>
                                                </tr>  
                                                <tr>         
                                                    <td style="background-color: #ffffff;">No Faktur</td>
                                                    <td style="background-color: #ffffff;"><?php echo $hutang->No_Faktur?></td>
                                                </tr>  
                                               <tr>         
                                                    <td style="background-color: #e5eff0;">Nilai Hitung</td>
                                                    <td style="background-color: #e5eff0;">Rp.<?php echo convert_currency($hutang->Nilai_Hutang)?></td>
                                                </tr> 
                                                <tr>         
                                                    <td style="background-color: #ffffff;">Saldo Awal</td>
                                                    <td style="background-color: #ffffff;">Rp.<?php echo convert_currency($hutang->Saldo_Awal);?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td style="background-color: #e5eff0;">Pembayaran</td>
                                                    <td style="background-color: #e5eff0;">Rp.<?php echo convert_currency($hutang->Pembayaran)?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td style="background-color: #ffffff;">Saldo Akhir</td>
                                                    <td style="background-color: #ffffff;">Rp.<?php echo convert_currency($hutang->Saldo_Akhir); ?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td style="background-color: #e5eff0;">Supplier</td>
                                                    <td style="background-color: #e5eff0;"><?php echo $hutang->Nama; ?></td>
                                                </tr>  
                                                
                                            </tbody>
                            <tfoot></tfoot>
                        </table>

                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('Hutang/index')?>" name="simpan">Kembali</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>