<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Konversi Satuan</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>KonversiSatuan">Data Konversi Satuan</a></li>
                            <li class="active">Edit Konversi Satuan</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Data Konversi Satuan</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>KonversiSatuan/proses_edit_konversi/<?php echo $edit->IDKonversi ?>" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <label class="field_title">Satuan Berat</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Satuan Berat" style=" width:100%;" class="chzn-select" tabindex="13"  name="satuan_berat" required oninvalid="this.setCustomValidity('Satuan Berat Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            <option value=""></option>
                                            <?php
                                            foreach ($satuan as $sat) {
                                                if ($edit->IDSatuanBerat==$sat->IDSatuan) {
                                                    ?>
                                                    <option value="<?php echo $sat->IDSatuan ?>" selected><?php echo $sat->Satuan; ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $sat->IDSatuan ?>"><?php echo $sat->Satuan; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <label class="field_title">Qty</label>
                                    <div class="form_input">
                                      <input type="text" class="form-control" placeholder="Qty" name="qty" value="<?php echo str_replace(".", ",", $edit->Qty) ?>" required oninvalid="this.setCustomValidity('Qty Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <span class="clear"></span>
                                  </div>
                                  <label class="field_title">Satuan Ringan</label>
                                  <div class="form_input">
                                    <select data-placeholder="Pilih Satuan Ringan" style=" width:100%;" class="chzn-select" tabindex="13"  name="satuan_ringan" required oninvalid="this.setCustomValidity('Satuan Ringan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <option value=""></option>
                                        <?php
                                        foreach ($satuan as $sat) {
                                            if ($edit->IDSatuanKecil==$sat->IDSatuan) {
                                                ?>
                                                <option value="<?php echo $sat->IDSatuan ?>" selected><?php echo $sat->Satuan; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="<?php echo $sat->IDSatuan ?>"><?php echo $sat->Satuan; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>
                        </li>
                        <li>
                            <div class="form_grid_12">
                                <div class="form_input">
                                    <div class="btn_30_light">
                                     <span> <a href="<?php echo base_url() ?>KonversiSatuan" name="simpan">Kembali</a></span>
                                 </div>
                                 <div class="btn_30_blue">
                                  <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                              </div>
                          </div>
                      </div>
                  </li>
              </ul>
          </form>
      </div>
  </div>
</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>