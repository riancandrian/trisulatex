<?php $this->load->view('administrator/header') ; ?>
<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Barang</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="<?php echo base_url() ?>Barang">Data Barang</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Edit Data Barang </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Barang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Barang/proses_edit_barang/<?php echo $edit->IDBarang ?>" enctype="multipart/form-data" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">

                                    <!-- Group Barang -->
                                    <label class="field_title">Group Barang <?php echo $edit->IDBarang ?></label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Group Barang" name="group" required oninvalid="this.setCustomValidity('Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php
                                            foreach ($groupbarang as $group) {
                                                if ($edit->IDGroupBarang==$group->IDGroupBarang) {
                                                    ?>
                                                    <option value="<?php echo $group->IDGroupBarang ?>" selected><?php echo $group->Group_Barang; ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $group->IDGroupBarang ?>"><?php echo $group->Group_Barang; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <!-- Kode Barang -->
                                    <label class="field_title">Kode Barang</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Kode Barang" name="kode" value="<?php echo $edit->Kode_Barang ?>" required oninvalid="this.setCustomValidity('Kode Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Nama Barang -->
                                    <label class="field_title">Nama Barang</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Nama Barang" name="nama" value="<?php echo $edit->Nama_Barang ?>" required oninvalid="this.setCustomValidity('Nama Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>
                                    
                                    <!-- Corak -->
                                    <label class="field_title">Corak</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Corak" name="corak" id="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13" onchange="getmerk()">
                                            <option value=""></option>
                                            <?php
                                            foreach ($corak as $cor) {
                                                if ($edit->IDCorak==$cor->IDCorak) {
                                                    ?>
                                                    <option value="<?php echo $cor->IDCorak ?>" selected><?php echo $cor->Corak; ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $cor->IDCorak ?>"><?php echo $cor->Corak; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <!-- Merk -->
                                    <label class="field_title">Merk</label>
                                    <div class="form_input">
                                      <!--   <select data-placeholder="Pilih Merk" name="merk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php
                                           // foreach ($merk as $m) {
                                                //if ($edit->IDMerk==$m->IDMerk) {
                                                    ?>
                                                    <option value="<?php// echo $m->IDMerk ?>" selected><?php// echo $m->Merk; ?></option>
                                                    <?php
                                               // } else {
                                                    ?>
                                                    <option value="<?php //echo $m->IDMerk ?>"><?php// echo $m->Merk; ?></option>
                                                    <?php
                                               // }
                                           // }
                                            ?>
                                        </select> -->
                                        <input type="hidden" class="form-control" placeholder="Merk" name="merk" id="merk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                        <input type="text" class="form-control" placeholder="Merk" name="namamerk" id="namamerk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                    </div>

                                    <!-- Satuan -->
                                    <label class="field_title">Satuan</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Satuan" name="satuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php
                                            foreach ($satuan as $sat) {
                                                if ($edit->IDSatuan==$sat->IDSatuan) {
                                                    ?>
                                                    <option value="<?php echo $sat->IDSatuan ?>" selected><?php echo $sat->Satuan; ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $sat->IDSatuan ?>"><?php echo $sat->Satuan; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>



                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                            <span> <a href="<?php echo base_url() ?>Barang" name="simpan" title=".classname">Kembali</a></span>
                                        </div>
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simpan" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============ -->
<?php $this->load->view('administrator/footer') ; ?>
<script type="text/javascript">
    $(document).ready(function(){
        getmerk();
});

function getmerk() {
        var corak = $("#corak").val();

        $.ajax({
          url : '/'+base_url[1]+'/Barang/get_merk',
          type: "POST",
          data:{id_corak:corak},
          dataType:'json',
          success: function(data)
          { 
            var html = '';
            if (data <= 0) {
                console.log(data);
                $('#namamerk').val('');
                $('#merk').val('');
            } else {
                console.log(data);
                $("#namamerk").val(data[0]["Merk"]);
                $("#merk").val(data[0]["IDMerk"]);
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
      }
  });
    }
</script>