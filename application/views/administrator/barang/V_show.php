<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Barang</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('Barang/index')?>">Data Barang</a></li>
                            <li style="text-transform: capitalize;"> Detail Barang - <b><?php echo $barang->Nama_Barang ?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody> 
                                                <tr>         
                                                    <td width="35%" style="background-color: #e5eff0;">Group Barang</td>
                                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $barang->Group_Barang ?></td>
                                                </tr>
                                                <tr>         
                                                    <td style="background-color: #ffffff;">Kode Barang</td>
                                                    <td style="background-color: #ffffff;"><?php echo $barang->Kode_Barang?></td>
                                                </tr>  
                                                <tr>         
                                                    <td  style="background-color: #e5eff0;">Nama Barang</td>
                                                    <td  style="background-color: #e5eff0;"><?php echo $barang->Nama_Barang ?></td>
                                                </tr>  
                                               <tr>         
                                                    <td style="background-color: #ffffff;">Merk</td>
                                                    <td style="background-color: #ffffff;"><?php echo $barang->Merk ?></td>
                                                </tr> 
                                                <tr>         
                                                    <td  style="background-color: #e5eff0;">Satuan</td>
                                                    <td  style="background-color: #e5eff0;"><?php echo $barang->Satuan ?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td style="background-color: #ffffff;">Corak</td>
                                                    <td style="background-color: #ffffff;"><?php echo $barang->Corak?></td>
                                                </tr>  
                                            </tbody>
                            <tfoot></tfoot>
                        </table>

                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('Barang/index')?>" name="simpan">Kembali</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>