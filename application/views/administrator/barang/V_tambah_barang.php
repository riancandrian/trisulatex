<?php $this->load->view('administrator/header') ; 
?>

<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Barang</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Barang">Data Barang</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Tambah Barang </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Barang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Barang/proses_barang" enctype="multipart/form-data" class="form_container left_label">
                       <ul>
                            <li>
                                <div class="form_grid_12 multiline">

                                    <!-- Group Barang -->
                                    
                                    <label class="field_title">Group Barang</label>
                                    <div class="form_input">
                                        <div class="grid_12 w-100 alpha">
                                          
                                            
                                            <div class="grid_9 ml">
                                                <select data-placeholder="Pilih Group Barang" name="group" required oninvalid="this.setCustomValidity('Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                                    <option value=""></option>
                                                    <?php foreach($groupbarang as $group)
                                                    {
                                                        echo "<option value='$group->IDGroupBarang'>$group->Group_Barang</option>";
                                                    } 
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form_grid_3 mr">
                                                <div class="btn_24_blue flot-right">
                                                   <span><a href="#open-modal">Tambah Group Barang</a></span>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="clear"></div>
                                   </div>

                                   <!-- Kode Barang -->
                                   <label class="field_title">Kode Barang</label>
                                   <div class="form_input">
                                    <input type="text" class="form-control" placeholder="Kode Barang" name="kode" required oninvalid="this.setCustomValidity('Kode Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                </div>

                                <!-- Nama Barang -->
                                <label class="field_title">Nama Barang</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" placeholder="Nama Barang" name="nama" required oninvalid="this.setCustomValidity('Nama Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                </div>
                                
                                <!-- Corak -->
                                <label class="field_title">Corak</label>
                                <div class="form_input">
                                    <select data-placeholder="Pilih Corak" name="corak" id="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13" onchange="getmerk()">
                                        <option value=""></option>
                                        <?php foreach($corak as $cor)
                                        {
                                            echo "<option value='$cor->IDCorak'>$cor->Corak</option>";
                                        } 
                                        ?>
                                    </select>
                                </div>

                                <!-- Merk -->
                                <label class="field_title">Merk</label>
                                <div class="form_input">
                                    <!-- <select data-placeholder="Pilih Merk" name="merk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                        <option value=""></option>
                                        <?php //foreach($merk as $m)
                                       // {
                                           // echo "<option value='$m->IDMerk'>$m->Merk</option>";
                                       // } 
                                        ?>
                                    </select> -->
                                     <input type="hidden" class="form-control" placeholder="Merk" name="merk" id="merk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                    <input type="text" class="form-control" placeholder="Merk" name="namamerk" id="namamerk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                </div>

                                <!-- Satuan -->
                                <label class="field_title">Satuan</label>
                                <div class="form_input">
                                    <select data-placeholder="Pilih Satuan" name="satuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                        <option value=""></option>
                                        <?php foreach($satuan as $sat)
                                        {
                                            echo "<option value='$sat->IDSatuan'>$sat->Satuan</option>";
                                        } 
                                        ?>
                                    </select>
                                </div>

                                

                                <!-- Status -->
                                  <!--   <label class="field_title">Status</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Status" name="status" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                        </select>
                                    </div> -->
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                            <span> <a href="<?php echo base_url() ?>Barang" name="simpan" title=".classname">Kembali</a></span>
                                        </div>
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============ -->
<!-- modal edit data -->

<div id="open-modal" class="modal-window">
   <div>
       <form method="post" action="<?php echo base_url() ?>GroupBarang/proses_group_barang" enctype="multipart/form-data" class="form_container left_label">
        <a href="#modal-close" title="Close" class="modal-close">X</a>
        <h1>Tambah Group Barang</h1>
        <div class="modal-body">
           
            <ul>
                <li>
                    <div class="form_grid_12 multiline">

                        <!-- Kode Barang -->
                        <label class="field_title">Kode Group Barang</label>
                        <div class="form_input">
                            <input type="text" class="form-control" placeholder="Kode Group Barang" name="kode" required oninvalid="this.setCustomValidity('Kode Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        </div>

                        <!-- Nama Barang -->
                        <label class="field_title">Group Barang</label>
                        <div class="form_input">
                            <input type="text" class="form-control" placeholder="Nama Group Barang" name="nama" required oninvalid="this.setCustomValidity('Nama Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                </li>
            </ul>
            
        </div>
        <div class="modal-foot">
                  <!--   <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div> -->
                    <div class="btn_30_blue">
                     <input type="submit" name="simbar" value="Simpan">
                 </div>
             </div>
         </form>
     </div>
 </div>

 <script type="text/javascript">
   function getmerk() {
    var corak = $("#corak").val();

    $.ajax({
      url : '/'+base_url[1]+'/Barang/get_merk',
      type: "POST",
      data:{id_corak:corak},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
            console.log(data);
          $('#namamerk').val('');
          $('#merk').val('');
        } else {
            console.log(data);
            $("#namamerk").val(data[0]["Merk"]);
            $("#merk").val(data[0]["IDMerk"]);
          }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
        }
      });
  }
 </script>
 <?php $this->load->view('administrator/footer') ; ?>