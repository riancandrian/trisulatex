<?php $this->load->view('administrator/header') ; 




if ($kodejurnalumum->curr_number == null) {
  $number = 1;
} else {
  $number = $kodejurnalumum->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_jurnalumum = 'JU'.date('y').$agen.$kodemax;
//echo $code_po;
?>
<!-- =============================================Style untuk tabs -->
<style type="text/css" scoped>
  .chzn-container {
    width: 103% !important;
  }
</style>

<!--========================Head Content -->
<div class="page_title">
  <div id="alert"></div>
  <?php echo $this->session->flashdata('Pesan');?>
  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Data Jurnal Umum</h3>
  <div class="top_search">
  </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>JurnalUmum">Data Jurnal Umum</a></li>
              <li class="active">Tambah Jurnal Umum</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Jurnal Umum</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>
              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">
                  <div class="form_grid_6">
                    <!-- form 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tanggal JU</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Komutasigirong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">No JU</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $code_jurnalumum ?>" readonly>
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Nama Perkiraan</label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" name="NamaPerkiraan1" id="NamaPerkiraan1">
                          <option value="">--Pilih Nama COA--</option>
                          <?php foreach($coa as $row) {
                            echo "<option value='$row->IDCoa' data-kodeCOA='$row->Kode_COA' data-Namacoa='$row->Nama_COA'>$row->Nama_COA</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Kode Perkiraan</label>
                      <div class="form_input">
                        <input type="text" name="KodePerkiraan1" id="KodePerkiraan1" class="form-control" placeholder="Kode Perkiraan" readonly>
                      </div>
                    </div>

                  

                  
                  </div>
                  <div class="form_grid_6">
                    <!-- radio 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title dot-2">Posisi</label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" name="Posisi" id="Posisi">
                          <option value="Kredit">Kredit</option>
                          <option value="Debet">Debet</option>
                        </select>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="15" rows="5"></textarea>
                       
                      </div>
                    </div>
               <div class="form_grid_12 mb-1">
                    <label class="field_title mt-dot2">Uang Sejumlah</label>
                    <div class="form_input">
                      <input type="text" name="uangsejumlah" id="uangsejumlah" class="form-control" placeholder="xxx">
                    </div>
                    <span class="clear"></span>
                  </div>

                    <!-- radio 3 -->
                  
                  </div>
                  <span class="clear"></span>
                </div>
                <div class="form_grid_12 multiline">


                  <div class="form_grid_6">
                  

                    <div class="form_input">
                      <div class="btn_24_blue">
                        <button class="btn_24_blue" type="button" id="tambah_sementara">
                          Tambah Data
                        </button>
                      </div>
                    </div>
                  </div>
                  <span class="clear"></span>
                </div>
              </li>

              <!-- =============================-Table Bawah -->
              <div class="widget_content">
                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Perkiraan</th>
                      <th>Nama Perkiraan</th>
                      <th>Debet</th>
                      <th>Kredit</th>
                      <th>Keterangan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>
              </div>
      <!--         <input type="text" name="totaldebet" id="totaldebet" placeholder="total debet">
              <input type="text" name="totalkredit" id="totalkredit" placeholder="total kredit">
              <input type="text" name="selisih" id="selisih" placeholder="selisih"> -->
              <ul>
                       <li>
                        <div class="form_grid_12 w-100 alpha">
                          
                          <div class="form_grid_12">
                            <div class="form_grid_6 mb-1">
                              <label class="field_title mb mt-dot2">Total Debit</label>
                              <div class="form_input input-not-focus">
                                <input type="text" id="totaldebet" name="totaldebet" class="form-control" placeholder="Total Debit" readonly>
                              </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                              <label class="field_title mb mt-dot2">Total Kredit</label>
                              <div class="form_input input-not-focus">
                                <input type="text" id="totalkredit" name="totalkredit" class="form-control" placeholder="Total Kredit" readonly>
                              </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                              <label class="field_title mb mt-dot2">Selisih</label>
                              <div class="form_input input-not-focus">
                                <input type="text" id="selisih" name="selisih" class="form-control" placeholder="Selisih" readonly>
                              </div>
                            </div>

                          </div>

                          <!-- clear content -->
                          <div class="clear"></div>
                        </div>
                      </li>
                    </ul>

              <li>
                <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                   <!--  <div class="btn_30_blue">
                     <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                  </div> -->
                  <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save()" type="button" value="Simpan Data" title="Simpan Data"></span>
                  </div>
                </div>
              </li>
            </ul>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/jurnalumum.js"></script>
<?php $this->load->view('administrator/footer') ; ?>