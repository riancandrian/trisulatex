<?php $this->load->view('administrator/header') ; 
?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3><?= $title_master ?></h3>
 
 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Giro/index_giro_keluar"><?= $title ?></a></li>
              <li class="active">Tambah Data</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6><?= $title ?></h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="<?php echo $action; ?>" class="form_container left_label">
        
            <ul>
              <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title">Supplier</label>
                  <div class="form_input">

                    <select data-placeholder="Pilih Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="supplier" required oninvalid="this.setCustomValidity('Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                     <option value=""></option>
                    <?php
                                    foreach ($IDPerusahaan as $key) {
                                        if ($key->IDSupplier==$IDSupplier) {
                                            ?>
                                            <option value="<?php echo $key->IDSupplier ?>" selected><?php echo $key->Nama; ?></option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $key->IDSupplier ?>"><?php echo $key->Nama; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                    </select>
                    <span class="clear"></span>
                  </div>

                  <!-- <label class="field_title">Jenis Faktur</label>
                  <div class="form_input"> -->

                     <input type="hidden" class="form-control" name="Jenis_Faktur" id="Jenis_Faktur" placeholder="Jenis Faktur" value="SA" readonly>
                   <!--  <span class="clear"></span>
                  </div> -->

                  <label class="field_title">Nomor Faktur</label>
                  <div class="form_input">
 <input type="text" class="form-control" name="Nomor_Faktur" id="Nomor_Faktur" required oninvalid="this.setCustomValidity('Nomor Faktur Tidak Boleh Kosong')" oninput="setCustomValidity('')" placeholder="Nomor Faktur" value="<?php echo $Nomor_Faktur; ?>">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Bank</label>
                  <div class="form_input">

                         <select data-placeholder="Pilih Bank" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDBank" required oninvalid="this.setCustomValidity('Bank Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                     <option value=""></option>
                     <?php 
                      foreach ($DataBank as $key ) { 
                        if ($key->IDBank == $IDBank) { ?>
                          <option value="<?= $key->IDBank ?>" selected> <?= 'no rekening : '.$key->Nomor_Rekening.' an '.$key->Atas_Nama ?></option> 
                          <!-- Bila Data Sudah isi sebelumnya -->
                        <?php }
                        ?>
                        <option value="<?= $key->IDBank ?>"> <?= 'no rekening : '.$key->Nomor_Rekening.' an '.$key->Atas_Nama ?></option>
                        
                      <?php }
                    ?>
                    </select>
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Nomor Giro</label>
                  <div class="form_input">
<input type="text" class="form-control" name="Nomor_Giro" id="Nomor_Giro" placeholder=" Nomor_Giro" value="<?php echo $Nomor_Giro; ?>" required oninvalid="this.setCustomValidity('Nomor Giro Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Tanggal Faktur</label>
                  <div class="form_input">

                      <input type="date" class="form-control date-picker hidden-sm-down" name="Tanggal_Faktur" id="Tanggal_Faktur" value="<?php echo $Tanggal_Faktur; ?>" required oninvalid="this.setCustomValidity('Tanggal Faktur Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Tanggal Cair</label>
                  <div class="form_input">
<input type="date" class="form-control date-picker hidden-sm-down" name="Tanggal_Cair" id="Tanggal_Cair" value="<?php echo $Tanggal_Cair; ?>" required oninvalid="this.setCustomValidity('Tanggal Cair Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Nilai</label>
              <div class="form_input">
                 <span class="input-group-addon" style="">Rp. </span>
               <input type="text" class="form-control price-date" name="Nilai" id="tanpa-rupiah-debit" placeholder=" Nilai" value="<?php echo $Nilai; ?>" required oninvalid="this.setCustomValidity('Nilai Tidak Boleh Kosong')" oninput="setCustomValidity('')">
               <span class="clear"></span>
             </div>
                 <input type="hidden" class="form-control" name="Status_Giro" id="Status_Giro" placeholder=" Status Giro" value="belum cair" required oninvalid="this.setCustomValidity('Nilai Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <!--  <label class="field_title">Status Giro</label> -->
                 <!--  <div class="form_input">
                  
                     <select data-placeholder="Pilih Status Giro" style=" width:100%;" class="chzn-select" tabindex="13"  name="Status_Giro" required oninvalid="this.setCustomValidity('Status Giro Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                     <option value=""></option>
                   <?php 
                  //  if($Status_Giro == "")
                    {
                      ?>
                      <option value="belum cair" selected>belum cair</option>
                  <?php
                    }
                    //  elseif ($Status_Giro == "sudah cair") { 
                    //  echo "<option value='sudah cair' selected>sudah cair</option> ";
                  //    echo "<option value='belum cair'>belum cair</option> ";
                 //   } else if ($Status_Giro == "belum cair") {
                   //   echo "<option value='belum cair' selected>belum cair</option> ";
                   //   echo "<option value='sudah cair'>sudah cair</option> ";
                   // }else{
                    ?>
                    <option value="sudah cair">sudah cair</option>
                    <option value="belum cair">belum cair</option>
                    <?php
                 // }
                  ?>
                    </select>
                   <span class="clear"></span>
                 </div> -->

                 <!-- <label class="field_title">Jenis Giro</label>
                  <div class="form_input">

                    <select data-placeholder="Pilih Jenis Giro" style=" width:100%;" class="chzn-select" tabindex="13"  name="Jenis_Giro" required oninvalid="this.setCustomValidity('Jenis Giro Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>
                    <?php //if ($Jenis_Giro == "giro masuk") { 
                    //  echo "<option value='giro masuk' selected>giro masuk</option> ";
                     // echo "<option value='giro keluar'>giro keluar</option> ";
                 //   } else if ($Jenis_Giro == "giro keluar") {
                  //    echo "<option value='giro keluar' selected>giro keluar</option> ";
                  //    echo "<option value='giro masuk' >giro masuk</option> ";
                  //  }elseif($Jenis_Giro==""){
                    ?>
                    <option value="giro keluar" selected>giro keluar</option>
                    <?php
                 // }else{
                    ?>
                     <option value="giro masuk">giro masuk</option>
                    <option value="giro keluar">giro keluar</option>
                    <?php
                //  }
                  ?>

                  </select>
                   <span class="clear"></span>
                 </div> -->
                  <input type="hidden" class="form-control" name="Jenis_Giro" id="Jenis_Giro" placeholder=" Jenis Giro" value="giro keluar" required oninvalid="this.setCustomValidity('Nilai Tidak Boleh Kosong')" oninput="setCustomValidity('')">
               </div>
             </li>
             <li>
              <div class="form_grid_12">
                <div class="form_input">
                  <div class="btn_30_light">
                     <input type="hidden" name="IDGiro" value="<?php echo $IDGiro; ?>" /> 
                    <span> <a href="<?php echo base_url() ?>Giro/index_giro_keluar" name="simpan">Kembali</a></span>
                  </div>
                  <div class="btn_30_blue">
                    <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>