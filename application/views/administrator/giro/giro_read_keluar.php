<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Giro Keluar</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('Giro/index_giro_keluar')?>">Data Giro Keluar</a></li>
                            <li style="text-transform: capitalize;"> Detail Giro Keluar</b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody> 
                                <tr>         
                                    <td style="background-color: #e5eff0;">Jenis Faktur</td>
                                    <td style="background-color: #e5eff0;"><?php echo $Jenis_Faktur; ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff;">Nomor Faktur</td>
                                    <td style="background-color: #ffffff;"><?php echo $Nomor_Faktur; ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Bank</td>
                                    <td style="background-color: #e5eff0;"> <?= 'no rekening : '.$Nomor_Rekening.' an '.$Atas_Nama ?></td>
                                </tr> 
                                <tr>         
                                    <td style="background-color: #ffffff;">Nomor Giro</td>
                                    <td style="background-color: #ffffff;"><?php echo $Nomor_Giro; ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Tanggal</td>
                                    <td style="background-color: #e5eff0;"><?php echo $Tanggal_Faktur; ?></td>
                                </tr>  
                                <tr>         
                                    <td>Nilai</td>
                                    <td><?php echo $Nilai; ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Status Giro</td>
                                    <td style="background-color: #e5eff0;"><?= $Status_Giro ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff;">Jenis Giro</td>
                                    <td style="background-color: #ffffff;"><?= $Jenis_Giro ?></td>
                                </tr>   
                            </tbody>
                            <tfoot></tfoot>
                        </table>

                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('Giro/index_giro_keluar')?>" name="simpan">Kembali</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>