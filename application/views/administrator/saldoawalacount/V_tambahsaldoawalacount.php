<?php $this->load->view('administrator/header') ; 


?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Saldo Awal Acount</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>SaldoAwalAcount">Data Saldo Awal Acount</a></li>
                            <li class="active">Tambah Saldo Awal Acount</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Saldo Awal Acount</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>SaldoAwalAcount/simpan" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                   <label class="field_title">Kode COA</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih COA" name="kode" id="kode" required oninvalid="this.setCustomValidity('Kode Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php foreach ($coa as $row) : ?>
                                                <option value="<?php echo $row->IDCoa ?>"><?php echo $row->Kode_COA ?> <?php echo $row->Nama_COA ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                  <label class="field_title">Nama COA</label>
                                  <div class="form_input">
                                     <input type="text" name="Nama" id="Nama" class="form-control" placeholder="Nama COA" required oninvalid="this.setCustomValidity('Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                     <span class="clear"></span>
                                 </div>
                                 <input type="hidden" name="Normal_balance" id="Normal_balance">
                                  <label class="field_title">Group COA</label>
                                  <div class="form_input">
                                     <input type="text" name="Group" id="Group" class="form-control" placeholder="Group COA" required oninvalid="this.setCustomValidity('Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                     <span class="clear"></span>
                                 </div>

                                <label class="field_title">Nilai Saldo Awal</label>
                                    <div class="form_input">
                                        <span class="input-group-addon" style="">Rp. </span>
                                        <input name="nilai" type="text" id="tanpa-rupiah-debit" class="form-control price-date" required oninvalid="this.setCustomValidity('Nilai Saldo Awal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <!-- <span class=" label_intro">Street Address</span> -->
                                        <span class="clear"></span>
                                    </div>
                                
                            </div>
                        </li>
                        <li>
                            <div class="form_grid_12">
                                <div class="form_input">
                                    <div class="btn_30_light">
                                     <span> <a href="<?php echo base_url() ?>SaldoAwalAcount" name="simpan">Kembali</a></span>
                                 </div>
                                 <div class="btn_30_blue">
                                  <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                              </div>
                          </div>
                      </div>
                  </li>
              </ul>
          </form>
      </div>
  </div>
</div>
</div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
  var base_url = window.location.pathname.split('/');
  $(document).ready(function(){
    if(($("#kode").val() != "")){
      cek();
    }
    $("#kode").change(cek);
  });
    function cek() {
    var asset = $("#kode").val();
    console.log(asset);
    //console.log(base_url[1]+'/Po/getmerk');
    $.ajax({
        url : '/'+base_url[1]+'/SaldoAwalAcount/getasset',
        type: "POST",
        data:{IDAsset:asset},
        dataType:'json',
        success: function(data)
        { 
 
            $("#Nama").val(data["Nama_COA"]);
            $("#Group").val(data["Nama_Group"]);
            $("#IDGroup").val(data["IDGroupCOA"]);
            $("#Normal_balance").val(data["Normal_Balance"]);

          
           
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
  }
</script>
<?php $this->load->view('administrator/footer') ; ?>