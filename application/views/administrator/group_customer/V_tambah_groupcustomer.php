<?php $this->load->view('administrator/header') ; 

?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Group Customer</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>GroupCustomer">Data Group Customer</a></li>
                            <li class="active">Tambah Group Customer</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Group Customer</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>GroupCustomer/simpan" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <label class="field_title">Kode Group Customer</label>
                                    <div class="form_input">

                                      <input type="text" class="form-control" name="kode_group" placeholder="Kode Group Customer" required oninvalid="this.setCustomValidity('Kode Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <span class="clear"></span>
                                  </div>
                                  <label class="field_title">Group Customer</label>
                                  <div class="form_input">
                                      <input type="text" class="form-control" name="groupcustomer" placeholder="Group Customer" required oninvalid="this.setCustomValidity('Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <span class="clear"></span>
                                  </div>
                              
                            </div>
                        </li>
                        <li>
                            <div class="form_grid_12">
                                <div class="form_input">
                                    <div class="btn_30_light">
                                     <span> <a href="<?php echo base_url() ?>GroupCustomer" name="simpan">Kembali</a></span>
                                 </div>
                                 <div class="btn_30_blue">
                                  <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                              </div>
                          </div>
                      </div>
                  </li>
              </ul>
          </form>
      </div>
  </div>
</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>