<?php 
$this->load->view('administrator/header') ;
?>

<div class="page_title">
    <span class="title_icon"><span class="computer_imac"></span></span>
    <h3>Dashboard</h3>
</div>
<div class="switch_bar">
    <h1>
        Welcome, <?php echo $this->session->userdata('username'); ?>
    </h1>
</div>

<?php
$this->load->view('administrator/footer') ;
?>