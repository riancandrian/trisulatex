<?php $this->load->view('administrator/header') ; 

?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data User</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>User">Data User</a></li>
              <li class="active">Tambah User</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data User</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="<?php echo base_url() ?>User/simpan" class="form_container left_label">
            <ul>
              <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title">Nama Lengkap</label>
                  <div class="form_input">

                    <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" required oninvalid="this.setCustomValidity('Nama Lengkap Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <span class="clear"></span>
                  </div>
                  <label class="field_title">Username</label>
                  <div class="form_input">
                   <input type="text" class="form-control" name="username" placeholder="Username" required oninvalid="this.setCustomValidity('Username Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>
                 <label class="field_title">Password</label>
                 <div class="form_input">
                   <input type="password" class="form-control" name="password" placeholder="Password" required oninvalid="this.setCustomValidity('Password Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>
                 <label class="field_title">Group User</label>
                 <div class="form_input">
                  <select data-placeholder="Cari Berdasarkan Group User" style=" width:100%;" class="chzn-select" tabindex="13"  name="group_user" required oninvalid="this.setCustomValidity('Group User Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>
                    <?php foreach ($group_users as $row) : ?>
                      <option value="<?php echo $row->IDGroupUser ?>"><?php echo $row->Kode_Group_User . '-' . $row->Group_User ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <label class="field_title">Status</label>
                <div class="form_input">
                  <select data-placeholder="Pilih Status" style=" width:100%;" class="chzn-select" tabindex="13"  name="status" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>
                    <option value="Aktif">Aktif</option>
                    <option value="Tidak Aktif">Tidak Aktif</option>
                  </select>
                </div>

                <fieldset>
                  <legend>Daftar Privileges</legend>
                  <div class="form_grid_12">
                    <label class="field_title">Data data master</label>
                    <div class="form_input">
                      <?php
                      foreach ($menu as $row1) :
                        // echo "<label class='col-form-label'>DATA ".strtoupper($row1->Menu)."</label><br>";
                        foreach ($details_menu as $row2) :
                          if ($row1->IDMenu == $row2->IDMenu) {
                            //echo "<label style='margin-left: 4%'>".$row2->Menu_Detail."</label><br>";
                            ?>
                            <div class="form_grid_4">
                              <span>
                                <input class="checkbox" type="checkbox" tabindex="1" id="<?php echo 'Check'.$row2->IDMenuDetail?>" name="details[]" value="<?php echo $row2->IDMenuDetail?>">
                                <label class="choice"><?php echo ' '.$row2->Menu_Detail?></label>
                              </span>
                            </div>
                            <?php 

                          } else {
                          }
                        endforeach;
                      endforeach;
                      ?>
                    </div>
                  </div>
                  <!-- <div class="row">
                    <div class="col-md-6">
                      <?php
                      // foreach ($menu as $row1) :
                      //   echo "<label class='col-form-label'>DATA ".strtoupper($row1->Menu)."</label><br>";
                      //   foreach ($details_menu as $row2) :
                      //     if ($row1->IDMenu == $row2->IDMenu) {
                                            //echo "<label style='margin-left: 4%'>".$row2->Menu_Detail."</label><br>";
                            ?>
                            <div class="checkbox" style="margin-left: 6%">
                              <input type="checkbox" id="<?php //echo 'Check'.$row2->IDMenuDetail?>" name="details[]" value="<?php //echo $row2->IDMenuDetail?>">
                              <label class="checkbox__label" for="<?php //echo 'Check'.$row2->IDMenuDetail?>"><?php //echo ' '.$row2->Menu_Detail?></label>
                            </div>
                            <?php
                      //     } else {

                      //     }
                      //   endforeach;
                      // endforeach;
                      ?>
                    </div>
                  </div> -->
                </fieldset>
              </div>
            </li>
            <li>
              <div class="form_grid_12">
                <div class="form_input">
                  <div class="btn_30_light">
                   <span> <a href="<?php echo base_url() ?>User" name="simpan">Kembali</a></span>
                 </div>
                 <div class="btn_30_blue">
                  <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </form>
    </div>
  </div>
</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>