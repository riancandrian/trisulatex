 </div>

 <!-- ==================================== -->
 <!--                script                -->
 <!-- ==================================== -->
 <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery-ui-1.8.18.custom.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.ui.touch-punch.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chosen.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/uniform.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/bootstrap-dropdown.js"></script>
 <script src="<?php echo base_url() ?>asset/js/bootstrap-colorpicker.js"></script>
 <script src="<?php echo base_url() ?>asset/js/sticky.full.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.noty.js"></script>
 <script src="<?php echo base_url() ?>asset/js/selectToUISlider.jQuery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/fg.menu.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.tagsinput.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.cleditor.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.tipsy.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.peity.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.simplemodal.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.jBreadCrumb.1.1.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.colorbox-min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.idTabs.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.multiFieldExtender.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.confirm.js"></script>
 <script src="<?php echo base_url() ?>asset/js/elfinder.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/accordion.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/autogrow.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/check-all.jquery.js"></script>
 <!--  <script src="<?php// echo base_url() ?>asset/js/data-table.jquery.js"></script> -->
 <script src="<?php echo base_url() ?>asset/datatables_jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/ZeroClipboard.js"></script>
 <script src="<?php echo base_url() ?>asset/js/TableTools.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jeditable.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/duallist.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/easing.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/full-calendar.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/input-limiter.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/inputmask.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/iphone-style-checkbox.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/meta-data.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/quicksand.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/raty.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/smart-wizard.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/stepy.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/treeview.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/ui-accordion.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/vaidation.jquery.js"></script>
 <script src="<?php echo base_url() ?>asset/js/mosaic.1.0.1.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.collapse.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.cookie.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.autocomplete.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/localdata.js"></script>
 <script src="<?php echo base_url() ?>asset/js/excanvas.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/jquery.jqplot.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.dateAxisRenderer.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.cursor.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.logAxisRenderer.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.canvasTextRenderer.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.highlighter.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.pieRenderer.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.barRenderer.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.categoryAxisRenderer.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.pointLabels.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/chart-plugins/jqplot.meterGaugeRenderer.min.js"></script>
 <script src="<?php echo base_url() ?>asset/js/custom-scripts.js"></script>
 <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
 <script>
    $(function () {
        $.jqplot._noToImageButton = true;
        var prevYear = [["2011-08-01",398], ["2011-08-02",255.25], ["2011-08-03",263.9], ["2011-08-04",154.24],
        ["2011-08-05",210.18], ["2011-08-06",109.73], ["2011-08-07",166.91], ["2011-08-08",330.27], ["2011-08-09",546.6],
        ["2011-08-10",260.5], ["2011-08-11",330.34], ["2011-08-12",464.32], ["2011-08-13",432.13], ["2011-08-14",197.78],
        ["2011-08-15",311.93], ["2011-08-16",650.02], ["2011-08-17",486.13], ["2011-08-18",330.99], ["2011-08-19",504.33],
        ["2011-08-20",773.12], ["2011-08-21",296.5], ["2011-08-22",280.13], ["2011-08-23",428.9], ["2011-08-24",469.75],
        ["2011-08-25",628.07], ["2011-08-26",516.5], ["2011-08-27",405.81], ["2011-08-28",367.5], ["2011-08-29",492.68],
        ["2011-08-30",700.79], ["2011-08-31",588.5], ["2011-09-01",511.83], ["2011-09-02",721.15], ["2011-09-03",649.62],
        ["2011-09-04",653.14], ["2011-09-06",900.31], ["2011-09-07",803.59], ["2011-09-08",851.19], ["2011-09-09",2059.24],
        ["2011-09-10",994.05], ["2011-09-11",742.95], ["2011-09-12",1340.98], ["2011-09-13",839.78], ["2011-09-14",1769.21],
        ["2011-09-15",1559.01], ["2011-09-16",2099.49], ["2011-09-17",1510.22], ["2011-09-18",1691.72],
        ["2011-09-19",1074.45], ["2011-09-20",1529.41], ["2011-09-21",1876.44], ["2011-09-22",1986.02],
        ["2011-09-23",1461.91], ["2011-09-24",1460.3], ["2011-09-25",1392.96], ["2011-09-26",2164.85],
        ["2011-09-27",1746.86], ["2011-09-28",2220.28], ["2011-09-29",2617.91], ["2011-09-30",3236.63]];
        var currYear = [["2011-08-01",796.01], ["2011-08-02",510.5], ["2011-08-03",527.8], ["2011-08-04",308.48],
        ["2011-08-05",420.36], ["2011-08-06",219.47], ["2011-08-07",333.82], ["2011-08-08",660.55], ["2011-08-09",1093.19],
        ["2011-08-10",521], ["2011-08-11",660.68], ["2011-08-12",928.65], ["2011-08-13",864.26], ["2011-08-14",395.55],
        ["2011-08-15",623.86], ["2011-08-16",1300.05], ["2011-08-17",972.25], ["2011-08-18",661.98], ["2011-08-19",1008.67],
        ["2011-08-20",1546.23], ["2011-08-21",593], ["2011-08-22",560.25], ["2011-08-23",857.8], ["2011-08-24",939.5],
        ["2011-08-25",1256.14], ["2011-08-26",1033.01], ["2011-08-27",811.63], ["2011-08-28",735.01], ["2011-08-29",985.35],
        ["2011-08-30",1401.58], ["2011-08-31",1177], ["2011-09-01",1023.66], ["2011-09-02",1442.31], ["2011-09-03",1299.24],
        ["2011-09-04",1306.29], ["2011-09-06",1800.62], ["2011-09-07",1607.18], ["2011-09-08",1702.38],
        ["2011-09-09",4118.48], ["2011-09-10",1988.11], ["2011-09-11",1485.89], ["2011-09-12",2681.97],
        ["2011-09-13",1679.56], ["2011-09-14",3538.43], ["2011-09-15",3118.01], ["2011-09-16",4198.97],
        ["2011-09-17",3020.44], ["2011-09-18",3383.45], ["2011-09-19",2148.91], ["2011-09-20",3058.82],
        ["2011-09-21",3752.88], ["2011-09-22",3972.03], ["2011-09-23",2923.82], ["2011-09-24",2920.59],
        ["2011-09-25",2785.93], ["2011-09-26",4329.7], ["2011-09-27",3493.72], ["2011-09-28",4440.55],
        ["2011-09-29",5235.81], ["2011-09-30",6473.25]];
        var plot1 = $.jqplot("chart1", [prevYear, currYear], {
            seriesColors: ["rgba(78, 135, 194, 0.7)", "rgb(211, 235, 59)"],
            title: 'Monthly Revenue',
            highlighter: {
                show: true,
                sizeAdjust: 1,
                tooltipOffset: 9
            },
            grid: {
                background: 'rgba(57,57,57,0.0)',
                drawBorder: false,
                shadow: false,
                gridLineColor: '#666666',
                gridLineWidth: 2
            },
            legend: {
                show: true,
                placement: 'outside'
            },
            seriesDefaults: {
                rendererOptions: {
                    smooth: true,
                    animation: {
                        show: true
                    }
                },
                showMarker: false
            },
            series: [
            {
                fill: true,
                label: '2010'
            },
            {
                label: '2011'
            }
            ],
            axesDefaults: {
                rendererOptions: {
                    baselineWidth: 1.5,
                    baselineColor: '#444444',
                    drawBaseline: false
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.DateAxisRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        formatString: "%b %e",
                        angle: -30,
                        textColor: '#dddddd'
                    },
                    min: "2011-08-01",
                    max: "2011-09-30",
                    tickInterval: "7 days",
                    drawMajorGridlines: false
                },
                yaxis: {
                    renderer: $.jqplot.LogAxisRenderer,
                    pad: 0,
                    rendererOptions: {
                        minorTicks: 1
                    },
                    tickOptions: {
                        formatString: "$%'d",
                        showMark: false
                    }
                }
            }
        });
    });

</script>

<script>
    var base_url = window.location.pathname.split('/');
    $(document).ready(function(){
        setTimeout(function(){$(".pesan").fadeIn('slow');}, 1500);
    });
    setTimeout(function(){$(".pesan").fadeOut('slow');}, 3000);

</script>

<script type="text/javascript">
    
    var tanpa_rupiah_debit = document.getElementById('harga-123');
    if(tanpa_rupiah_debit){
    tanpa_rupiah_debit.addEventListener('keyup', function(e)
    {
        tanpa_rupiah_debit.value = formatRupiah(this.value);
    });
    
    tanpa_rupiah_debit.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}

    
    var tanpa_rupiah_kredit = document.getElementById('tanpa-rupiah-kredit');
    if(tanpa_rupiah_kredit){
    tanpa_rupiah_kredit.addEventListener('keyup', function(e)
    {
        tanpa_rupiah_kredit.value = formatRupiah(this.value);
    });
    
    tanpa_rupiah_kredit.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}

var tanpa_rupiah_debit1 = document.getElementById('tanpa-rupiah-debit');
    if(tanpa_rupiah_debit1){
    tanpa_rupiah_debit1.addEventListener('keyup', function(e)
    {
        tanpa_rupiah_debit1.value = formatRupiah(this.value);
    });
    
    tanpa_rupiah_debit1.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}

// var qty = document.getElementById('Qty');
//     if(qty){
//     qty.addEventListener('keyup', function(e)
//     {
//         qty.value = formatRupiah(this.value);
//     });
    
//     qty.addEventListener('keydown', function(event)
//     {
//         limitCharacter(event);
//     });
// }

var uangsejumlah = document.getElementById('uangsejumlah');
    if(uangsejumlah){
    uangsejumlah.addEventListener('keyup', function(e)
    {
        uangsejumlah.value = formatRupiah(this.value);
    });
    
    uangsejumlah.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}

var KreditatauDebet = document.getElementById('KreditatauDebet');
    if(KreditatauDebet){
    KreditatauDebet.addEventListener('keyup', function(e)
    {
        KreditatauDebet.value = formatRupiah(this.value);
    });
    
    KreditatauDebet.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}



    function formatRupiah(bilangan, prefix)
    {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
        split   = number_string.split(','),
        sisa    = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
        
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    
</script>

<script>
   $(document).on('click', '#simtam', function () {
       tanggal = $('#Tanggal').val();
       no_bo = $('#no_bo').val();
       tanggal_selesai = $('#Tanggal_selesai').val();
       id_corak = $('#id_corak').val();
       qty = $('#qty').val();
       Keterangan = $('#Keterangan').val();

       if (tanggal != '' && no_bo != '' && tanggal_selesai != '' && qty != '') {
             //console.log(tanggal + ' ' + tanggal_selesai + ' ' + no_bo + ' ' + id_corak + ' ' + qty + ' ' + Keterangan);
             $.post("<?php echo base_url("BookingOrder/store")?>", {'tanggal' : tanggal, 'no_bo' : no_bo, 'tanggal_selesai' : tanggal_selesai, 'id_corak' : id_corak, 'qty' : qty, 'Keterangan' : Keterangan})
             .done(function(res) {
                     // $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
                     window.location.href = "/"+base_url[1]+"/BookingOrder/create";
                     //document.getElementById("print").disabled = false;
                     document.getElementById("simtam").disabled = true;
                 });
         } else {
           $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
       }
   });

   function print_data() {
       bo = $('#no_bo').val();

       window.open("<?php echo base_url("BookingOrder/print_data/")?>" + bo);
   }
</script>
<script type="text/javascript">
    function rupiah(nStr) {
       nStr += '';
       x = nStr.split('.');
       x1 = x[0];
       x2 = x.length > 1 ? '.' + x[1] : '';
       var rgx = /(\d+)(\d{3})/;
       while (rgx.test(x1))
       {
          x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      return x1 + x2;
  }

  function rupiahkoma(nStr) {
       nStr += '';
       x = nStr.split(',');
       x1 = x[0];
       x2 = x.length > 1 ? ',' + x[1] : '';
       var rgx = /(\d+)(\d{3})/;
       while (rgx.test(x1))
       {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
  }
  
  $(document).ready(function() {
    $('#DataTables_Table_0').DataTable( {
        "scrollY": 200,
        "scrollX": true
    } );
} );
</script>


</body>
</html>