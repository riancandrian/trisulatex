<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width"/>
    <title>PT Trisula Textile Industries Tbk</title>
    <link href="<?php echo base_url() ?>asset/css/reset.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/shCore.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/jquery.jqplot.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/data-table.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/form.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/styles-admin.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/modal.css" rel="stylesheet" type="text/css" media="screen">
    <style type="text/css">
        .pesan{
            margin: auto !important;
            left: 0 !important;
            right: 0 !important;
            
        }
    </style>
    
</head>
<body id="theme-default" class="full_block">
    <div id="actionsBox" class="actionsBox">
        <div id="actionsBoxMenu" class="menu">
            <span id="cntBoxMenu">0</span>
            <a class="button box_action">Archive</a>
            <a class="button box_action">Delete</a>
            <a id="toggleBoxMenu" class="open"></a>
            <a id="closeBoxMenu" class="button t_close">X</a>
        </div>
        <div class="submenu">
            <a class="first box_action">Move...</a>
            <a class="box_action">Mark as read</a>
            <a class="box_action">Mark as unread</a>
            <a class="last box_action">Spam</a>
        </div>
    </div>
    <div id="left_bar">

        <div id="sidebar">
            <div id="secondary_nav">
                <ul id="sidenav" class="accordion_mnu collapsible">
                  <li>
                    <a href="<?php echo base_url() ?>Dashboard" class="height-navbar">
                        <div>
                            <div class="icon-data-master1">
                            </div>
                        </div>
                        <div class="text-navbar"> Dashboard</div>
                    </a>
                </li>
                <?php 
                $i=1;
                foreach($aksesmenu as $menu){ 
                    ?>
                    <li>
                        <a href="#" class="height-navbar">
                            <div>
                                <div class="icon-data-master<?php echo $i+1 ?>">
                                    <!-- <img src="<?php //echo base_url() ?>asset/images/icon-master/data-master.png"> -->
                                </div>
                            </div>
                            <div class="text-navbar"> <?php echo $menu->Menu ?></div>
                        </a>
                        <ul class="acitem">
                          <?php foreach($aksesmenudetail as $detail){ 
                            if($menu->IDMenu==$detail->IDMenu){ ?>

                            <li><a href="<?php echo base_url() ?><?php echo $detail->Url ?>"><span class="list-icon">&nbsp;</span><?php echo $detail->Menu_Detail ?></a></li>
                            <?php if($detail->Menu_Detail=='Laporan Pembelian'){ ?>
                               <li><a href="#"><span class="nav_icon computer_imac"></span> Dashboard<span class="alert_notify blue">4</span><span class="up_down_arrow">&nbsp;</span></a>
                        <ul class="acitem">
                          <li><a href="dashboard.html"><span class="list-icon">&nbsp;</span>Dashboard Main</a></li>
                      </ul>
                  </li>
                            <?php }}} ?>


                        </ul>
                    </li>
                    <?php 
                    $i++;
                } 
                ?>
      </ul>
  </div>
  <div class="secondary-foot">
    PT Trisula Textile Industries Tbk
</div>
</div>
</div>
<div id="container" style="margin-left: 250px;">
    <div id="header" class="blue_lin">
        <?php echo $this->session->flashdata('alert');?>
     <center><div style="margin:auto;left:0;right: 0;" id="alert"></div></center>
     <div class="header_left">

        <div class="logo">
           <!--  <img src="<?php //echo base_url() ?>asset/images/logo.png" width="160" height="60" alt="Ekra"> -->
        </div>
        <div id="responsive_mnu">
            <a href="#responsive_menu" class="fg-button" id="hierarchybreadcrumb"><span class="responsive_icon"></span>Menu</a>
            <div id="responsive_menu" class="hidden">
                <ul>
                    <li><a href="#"> Dashboard</a>
                        <ul>
                            <li><a href="dashboard.html">Dashboard Main</a></li>
                            <li><a href="dashboard-01.html">Dashboard 01</a></li>
                            <li><a href="dashboard-02.html">Dashboard 02</a></li>
                            <li><a href="dashboard-03.html">Dashboard 03</a></li>
                            <li><a href="dashboard-04.html">Dashboard 04</a></li>
                        </ul>
                    </li>
                    <li><a href="#"> Forms</a>
                        <ul>
                            <li><a href="form-elements.html">All Forms Elements</a></li>
                            <li><a href="left-label-form.html">Left Label Form</a></li>
                            <li><a href="top-label-form.html">Top Label Form</a></li>
                            <li><a href="form-xtras.html">Additional Forms (3)</a></li>
                            <li><a href="form-validation.html">Form Validation</a></li>
                            <li><a href="signup-form.html">Signup Form</a></li>
                            <li><a href="content-post.html">Content Post Form</a></li>
                            <li><a href="wizard.html">wizard</a></li>
                        </ul>
                    </li>
                    <li><a href="table.html"> Tables</a></li>
                    <li><a href="ui-elements.html">User Interface Elements</a></li>
                    <li><a href="buttons-icons.html">Butons And Icons</a></li>
                    <li><a href="widgets.html">All Widgets</a></li>
                    <li><a href="#">Pages</a>
                        <ul>
                            <li><a href="post-preview.html">Content</a></li>
                            <li><a href="login-01.html" target="_blank">Login 01</a></li>
                            <li><a href="login-02.html" target="_blank">Login 02</a></li>
                            <li><a href="login-03.html" target="_blank">Login 03</a></li>
                            <li><a href="forgot-pass.html" target="_blank">Forgot Password</a></li>
                        </ul>
                    </li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="#">Grid</a>
                        <ul>
                            <li><a href="content-grid.html">Content Grid</a></li>
                            <li><a href="form-grid.html">Form Grid</a></li>
                        </ul>
                    </li>
                    <li><a href="chart.html">Chart/Graph</a></li>
                    <li><a href="gallery.html">Gallery</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                    <li><a href="file-manager.html">File Manager</a></li>
                    <li><a href="#">Error Pages</a>
                        <ul>
                            <li><a href="403.html" target="_blank">403</a></li>
                            <li><a href="404.html" target="_blank">404</a></li>
                            <li><a href="505.html" target="_blank">405</a></li>
                            <li><a href="500.html" target="_blank">500</a></li>
                            <li><a href="503.html" target="_blank">503</a></li>
                        </ul>
                    </li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="#">Email Templates</a>
                        <ul>
                            <li><a href="email-templates/forgot-pass-email-template.html" target="_blank">Forgot Password</a></li>
                            <li><a href="email-templates/registration-confirmation-email-template.html" target="_blank">Registaion Confirmation</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header_right">

        <div id="user_nav">
            <ul>
                <div id="top_notification">
                    <ul>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle alert"><span class="icon"></span><span class="alert_notify orange">Setting</span></a>
                            <div class="notification_list dropdown-menu pull-left blue_d">
                                <div class="white_lin nlist_block">
                                    <ul>
                                       <?php foreach($aksessetting as $setting){ ?>
                                       <div class="list_inf">
                                         <span class="btn_24_blue"><a href="<?php echo base_url() ?><?php echo $setting->Url ?>"><?php echo $setting->Menu_Detail ?></a></span>
                                     </div>
                                     <?php } ?>
                                 </ul>
                             </div>
                         </div>
                     </li>
                 </ul>
             </div>
            <!--  <li class="user_thumb">
                        <a href="#"><span class="icon"><img src="<?php //echo base_url() ?>asset/images/user_thumb.png" width="30" height="30" alt="User"></span></a>
                    </li> -->
                    <li class="user_info">
                        <span class="user_name"><?php echo $this->session->userdata("nama"); ?></span><span><a href="#"><?php echo $this->session->userdata("username"); ?></a></span>
                    </li>
                    <li class="logout">
                        <a href="<?php echo base_url() ?>Login/logout"><span class="icon"></span>Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

