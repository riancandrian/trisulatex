<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Retur Pembelian</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Retur_pembelian_umum">Data Retur Pembelian Umum</a></li>
              <li class="active">Ubah Retur Pembelian Umum</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Ubah Data Retur Pembelian Umum</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>
              <li class="body-search-data">
                <div class="form_grid_12 w-100">
                  <input type="hidden" name="IDRB" id="IDRB" value="<?php echo $retur->IDRBUmum ?>">
                  <div class="form_grid_2 alpha">
                    <label class="field_title w-100 mt-dot2">Tanggal RB</label>
                  </div>
                  <div class="form_grid_4">
                    <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $retur->Tanggal ?>">
                    <span class="clear"></span>
                    <!-- <span class=" label_intro">First Name</span> -->
                  </div>
                  <div class="form_grid_2">
                    <label class="field_title w-100 mt-dot2">Supplier</label>
                  </div>
                  <div class="form_grid_4">
                    <input type="hidden" name="IDSupplier" id="IDSupplier" value="<?php echo $retur->IDSupplier ?>" readonly>
                       <input type="text" name="NamaSupplier" id="NamaSupplier" value="<?php echo $retur->Nama ?>" readonly>
                </div>
                <span class="clear"></span>
              </div>
              <div class="form_grid_12 w-100">
                <div class="form_grid_2 alpha">
                  <label class="field_title w-100 mt-dot2">No RB</label>
                </div>
                <div class="form_grid_4">
                  <input id="RB" type="text" name="RB" class="input-date-padding form-control" placeholder="No RB" required oninvalid="this.setCustomValidity('No RB Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $retur->Nomor ?>" readonly>
                  <span class="clear"></span>
                  <!-- <span class=" label_intro">First Name</span> -->
                </div>
                <div class="form_grid_2">
                  <label class="field_title w-100 mt-dot2"></label>
                </div>
                <span class="clear"></span>
              </div>
              <div class="form_grid_12 w-100">
                <div class="form_grid_2 alpha">
                  <label class="field_title w-100 mt-dot2">No Inv Pembelian</label>
                </div>
                <div class="form_grid_4">
                  <input id="Nomor" type="text" name="Nomor" value="<?php echo $retur->nomorFB ?>" onkeyup="cek_grandtotal()" class="input-date-padding form-control" placeholder="No Inv Pembelian" readonly>
                  <span class="clear"></span>
                  <!-- <span class=" label_intro">First Name</span> -->
                </div>
                <div class="form_grid_2">
                  <label class="field_title w-100 mt-dot2">Status PPN</label>
                </div>
                <div class="form_grid_4">
                 <input type="text" name="Status_ppn" id="Status_ppn" value="<?php echo $retur->Status_ppn ?>" readonly>
                 <span class="clear"></span>
               </div> 
               <span class="clear"></span>
             </div>
             <div class="form_grid_12 w-100">
              <div class="form_grid_2 alpha">
                <label class="field_title w-100 mt-dot2">Tanggal Inv Pembelian</label>
              </div>
              <div class="form_grid_4">
                <input id="TanggalInv" type="date" name="TanggalInv" class="input-date-padding form-control" placeholder="Tanggal Inv Pembelian" required oninvalid="this.setCustomValidity('Tanggal Inv Pembelian Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $retur->Tanggal_fb ?>">
                <span class="clear"></span>
                <!-- <span class=" label_intro">First Name</span> -->
              </div>
              <div class="form_grid_4">
                <input id="totalqty" type="hidden" name="totalqty" class="input-date-padding form-control" readonly>
               <input id="id_fb_umum" type="hidden" name="id_fb_umum" value="<?php echo $retur->IDFBUmum ?>" class="form-control">
                <span class="clear"></span>
                <!-- <span class=" label_intro">First Name</span> -->
              </div>
              <span class="clear"></span>
            </div>
          </li>

          <li>
        <!--     <div class="form_grid_12">
              <div class="form_input ml text-center w-100">
                <button class="btn_24_blue" type="button" id="add-to-cart">
                  Tambah Data
                </button>


              </div>
            </div>
          </li> -->

        </ul>
        <div>
          <table class="display data_tbl">
            <thead>
              <tr>
                <th class="center" style="width: 40px">No</th>
                    <th>do</th>
                    <th class="hidden-xs">Barang</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Total</th>
              </tr>
            </thead>
            <tbody id="tb_preview_penerimaan">

            </tbody>
          </table>
        </div>
        <center>
        <div class="btn_30_blue">
          <span><button class="btn_24_blue" id="hapus" type="button" onclick="hapus_array()">
            Hapus
          </button></span>
        </div>
      </center>

     
        <input type="hidden" id="Pembayaran" name="Pembayaran" class="form-control" placeholder="Pembayaran">
                <div class="form_grid_12">
                  <div class="form_grid_8">
                    <label class="field_title mt-dot2 mb">Keterangan</label>
                    <div class="form_input">
                      <textarea id="Keterangan" type="text" name="Keterangan" class="form-control" placeholder="Keterangan" required oninvalid="this.setCustomValidity('Keterangan Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="resize: none;" rows="8"><?php echo $retur->Keterangan ?></textarea>
                    </div>
                  </div>
                  <div class="form_grid_4">

                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2 mb">DPP</label>
                      <div class="form_input input-not-focus">
                        <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                      </div>
                    </div>

                   <!--  <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2 mb">Discount</label>
                      <div class="form_input input-not-focus"> -->
                        <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" value="0" readonly>
                    <!--   </div>
                    </div> -->

                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2 mb">PPN</label>
                      <div class="form_input input-not-focus">
                        <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title pt mb">Total Invoice</label>
                      <div class="form_input input-not-focus">
                        <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                      </div>
                    </div>

                  </div>

                  <div class="clear"></div>
                </div>
     <!--    <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP">
        <input type="text" id="Discount_total" name="Discount_total" class="form-control diskon" placeholder="Discount">
        <input type="text" id="PPN" name="PPN" class="form-control ppn_inv" placeholder="PPN">
        <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice"> -->


      </form>
      <div class="widget_content px-2">
        <center>
        <div class="py-4 mx-2">
           <div class="btn_30_blue">
                        <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                    </div>
          <div class="btn_30_blue">
            <span><button class="btn_24_blue" type="button" id="simpan" onclick="saveEditReturPembelian()">
              Simpan
            </button></span>
          </div>
        </div>
      </center>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script src="<?php echo base_url() ?>libjs/retur_pembelian_umum.js"></script>
<script type="text/javascript">
  var base_url = window.location.pathname.split('/');

  function Retur(){
   this.Barcode;
   this.Corak;
   this.Warna;
   this.Merk;
   this.Qty_yard;
   this.Qty_meter;
   this.Grade;
   this.Satuan;
   this.NoSO;
   this.Party;
   this.Indent;
   this.Lebar;
   this.Tanggal;
   this.NoPB;
   this.ID_supplier;
   this.IDBarang;
   this.Remark;
   this.Keterangan;
   this.Total_yard;
   this.Total_meter;
   this.Harga;
   this.Subtotal;
 }

 var PM = new Array();
 index = 0;
 totaldpp=0;



    function fieldRetur(){
  var data1 = {
    "IDRB" : $("#IDRB").val(),
    "Tanggal" :$("#Tanggal").val(),
     "RB":$("#RB").val(),
    "IDSupplier":$("#IDSupplier").val(),
    "TanggalInv":$("#TanggalInv").val(),
    "Keterangan":$("#Keterangan").val(),
    "Nomor":$("#Nomor").val(),
    "totalqty":$("#totalqty").val(),
    "totalmeter":$("#totalmeter").val(),
    "Discount_total":$("#Discount_total").val(),
    "Pembayaran":$("#Pembayaran").val(),
    "DPP":$("#DPP").val(),
    "PPN":$("#PPN").val(),
    "id_fb_umum":$("#id_fb_umum").val(),
    "Status_ppn":$("#Status_ppn").val(),
    "total_invoice":$("#total_invoice").val(),
  }
  return data1;
}

    function saveEditReturPembelian(){
    var data1 = fieldRetur();
    console.log(PM);

    $.ajax({

      url : '/'+base_url[1]+'/retur_pembelian_umum/ubah_retur_pembelian',

      type: "POST",

      data: {

       "data1" : data1,
       "data2": PM
      
       // "data3" : temp

     },

     // dataType: 'json',

     success: function (msg, status) {
      // window.location.href = "../index";
       $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/retur_pembelian_umum/print/'+data1['IDRB']);

    },
    error: function(msg, status, data){
      console.log(msg);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });


  }
// function cek_grandtotal()
// {
//   var totalinv=0;
//   var Nomor = $("#Nomor").val();
//   $.ajax({
//     url : '/'+base_url[1]+'/ReturPembelian/cek_grand_total',
//     type: "POST",
//     data:{Nomor:Nomor},
//     dataType:'json',
//     success: function(data)
//     { 
//       var html = '';
//       if (data <= 0) {
//         console.log('-----------data kosong------------');
//       } else {
//        console.log(data);
//        console.log(data[0].Discount);
//        if(data[0].Discount==0){
//         $("#jenis").prop( "checked", false );
//       }else{
//         $("#jenis").prop( "checked", true );
//       }
//     }

//     if(data[0].PPN==0){
//       $("#jenis2").prop( "checked", false );
//     }else{
//       $("#jenis2").prop( "checked", true );
//     }

//     if($('#jenis').is(":checked")){
//       $("#Discount_total").val(data[0].Discount);
//     }else{
//       a=0;
//       $("#Discount_total").val(a);
//     }

//     if($('#jenis2').is(":checked")){
//       $("#PPN").val(data[0].PPN);
//     }else{
//       a=0;
//       $("#PPN").val(a);
//     }

//     $("#Pembayaran").val(data[0].Pembayaran);

//   },
//   error: function (jqXHR, textStatus, errorThrown)
//   {
//     console.log(jqXHR);
//     console.log(textStatus);
//     console.log(errorThrown);
//   }
// });
// }

  $(document).ready(function(){
    var totalinv=0;
    var Nomor = $("#Nomor").val();
    $.ajax({
      url : '/'+base_url[1]+'/ReturPembelian/cek_grand_total',
      type: "POST",
      data:{Nomor:Nomor},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0) {
          console.log('-----------data kosong------------');
        } else {
         console.log(data);
         console.log(data[0].Discount);
         if(data[0].Discount==0){
          $("#jenis").prop( "checked", false );
        }else{
          $("#jenis").prop( "checked", true );
        }
      }

      if(data[0].PPN==0){
        $("#jenis2").prop( "checked", false );
      }else{
        $("#jenis2").prop( "checked", true );
      }

      if($('#jenis').is(":checked")){
        $("#Discount_total").val(data[0].Discount);
      }else{
        a=0;
        $("#Discount_total").val(a);
      }

      if($('#jenis2').is(":checked")){
        $("#PPN").val(data[0].PPN);
      }else{
        a=0;
        $("#PPN").val(a);
      }

      $("#Pembayaran").val(data[0].Pembayaran);
      $("#Pembayaran").val(data[0].Pembayaran);
      t=  $("#DPP").val();
      tot= t-parseInt($("#Discount_total").val())+ parseInt($("#PPN").val());
      $("#total_invoice").val(tot);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
    }
  });

    renderJSONE($.parseJSON('<?php echo json_encode($detail) ?>'));
  })
</script>

<?php $this->load->view('administrator/footer') ; ?>