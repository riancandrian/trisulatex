<!DOCTYPE HTML>
<html>

<head>
    <title>PT Trisula Textile Industries Tbk</title>
    <link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen,print">
    
</head>
<body>
	<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
				
						<h1 style="float: right">Purchase Return</h1>
					<div class="widget_content">
						<div class=" page_content">
							<div class="invoice_container">
								<div class="invoice_action_bar">
									<div class="btn_30_light">
										<table border="1">
											<tr>
												<td>Date</td>
											</tr>
											<tr>
												<td><?php echo $print->Tanggal ?></td>
											</tr>
										</table>
										

									</div>
									<div class="btn_30_light">
										<table border="1">
											<tr>
												<td>Return No</td>
											</tr>
											<tr>
												<td><?php echo $print->Nomor ?></td>
											</tr>
										</table>
									</div>
									<div class="btn_30_light">
										<table border="1">
											<tr>
												<td>Receipt / Inv No</td>
											</tr>
											<tr>
												<td><?php echo "-" ?></td>
											</tr>
										</table>
									</div>
								</div>

								<div class="grid_6 invoice_num">
									<span>ITC Baranangsiang Blok F12-15<br>Bandung 40112<br>022-4222065 / 4222067</span>
								</div>
								<span class="clear"></span>
								<div class="grid_8 invoice_num">
									<table border="1">
										<tr><td>PT. Trisula Textille Industries</td></tr>
										<tr><td>Jl. Leuwigajah no 170<br>Cimahi 40522 <br> Jawa Barat <br> Indonesia</td></tr>
									</table>
								</div>
							
								<div class="grid_12 invoice_details">
									<div class="invoice_tbl">
										<table>
										<thead>
										<tr class=" gray_sai">
											<th>
												Items
											</th>
											<th>
												Description
											</th>
											<th>
												Qty
											</th>
											<th>
												Unit Price
											</th>
											<th>
												Amount
											</th>
											
										</tr>
										</thead>
										<tbody>
										 <?php if(empty($printdetail)){
                      ?>
                      <?php
                    }else{
                      foreach ($printdetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td><?php echo $data2->Nomor; ?></td>
                          <td><?php echo $data2->Nomor.' '.$data2->Nama_Barang ?></td>
                          <td><?php echo $data2->Qty; ?></td>
                          <td><?php echo $data2->Harga_satuan; ?></td>
                          <td><?php echo $data2->Sub_total; ?></td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
										<tr>
											<td colspan="4" class="grand_total">
												Sub Total:
											</td>
											<td>
												<?php echo $printgrandtotal->DPP ?>
											</td>
										</tr>
										<tr>
											<td colspan="4" class="grand_total">
												PPN :
											</td>
											<td>
												<?php echo $printgrandtotal->PPN ?>
											</td>
										</tr>
										<tr>
											<td colspan="4" class="grand_total">
												Total Return:
											</td>
											<td>
												<?php echo $printgrandtotal->Grand_total ?>
											</td>
										</tr>
										</tbody>
										</table>
									</div>
									<p class="amount_word">
										<div class="btn_30_light">
										<table border="1">
											<tr>
												<td>Description</td>
											</tr>
											<tr>
												<td><?php echo $print->Keterangan ?></td>
											</tr>
										</table>
									</div>
									</p>
									<div class="btn_30_light">
									<h5 class="notes">Prepared By</h5>
									<br><br>
									<p>
										Date :
									</p>
									</div>
									<div class="btn_30_light">
									<h5 class="notes">Reviewed By</h5>
									<br><br>
									<p>
										Date :
									</p>
									</div>

									<div class="btn_30_light">
									<h5 class="notes">Approved By</h5>
									<br><br>
									<p>
										Date :
									</p>
									</div>
								</div>
								<span class="clear"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</body>
</html>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
				<script type="text/javascript">
    $(document).ready(function() {
     window.print();
     console.log('masuk kesini');
     window.location.href = "<?php echo base_url() ?>Retur_pembelian_umum/index";
   });
 </script>