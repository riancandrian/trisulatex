<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width"/>
    <title>PT Trisula Textile Industries Tbk</title>
    <link href="<?php echo base_url() ?>asset/css/reset.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/shCore.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/jquery.jqplot.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/data-table.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/form.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/styles-admin.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url() ?>asset/css/modal.css" rel="stylesheet" type="text/css" media="screen">
</head>
<body id="theme-default" class="full_block" onload="printer()">
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal</td>
                                    <td style="background-color: #ffffff;"><?php echo DateFormat($data->Tanggal)?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">Nomor Booking Order</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #ffffff;">Tanggal Selesai</td>
                                    <td  style="background-color: #ffffff;"><?php echo DateFormat($data->Tanggal) ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">Kode Corak</td>
                                    <td style="background-color: #e5eff0;"><?php echo $data->Kode_Corak ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Corak</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Corak ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Qrt Yard</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $data->Qty ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Batal?></td>
                                </tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>asset/assets/plugins/jQuery/jquery-1.12.4.min.js"></script>
<script>
    function printer() {
        window.print();
        window.close();
        window.location.href = "<?php echo base_url() ?>BookingOrder/index";
    }
</script>
</body>
</html>