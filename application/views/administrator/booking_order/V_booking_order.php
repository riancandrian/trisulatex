<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Booking Order</h3>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li> Booking Order </li>
                        </ul>
                    </div>
                </div>
                <div class="widget_content">
                    <form action="<?php echo base_url() ?>BookingOrder/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">

                                <!-- search -->
                                <div class="form_grid_12 w-100 alpha">

                                    <!-- button -->
                                    <div class="form_grid_3 ">
                                        <div class="btn_24_blue">
                                            <a href="<?php echo base_url() ?>BookingOrder/create"><span>Tambah Data</span></a>
                                        </div>
                                    </div>

                                    <div class="form_grid_9 ">

                                        <!-- form 1 -->
                                        <div class="form_grid_3 ">   
                                            <label class="field_title mt-dot2">Periode</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>

                                        <!-- form 2 -->
                                        <div class="form_grid_3 ">   
                                            <label class="field_title mt-dot2"> S/D </label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                            </div>
                                        </div>

                                        <div class="form_grid_3 ">   
                                            <select name="jenispencarian" data-placeholder="Nomor BO" style="width: 100%!important" class="chzn-select" tabindex="13">
                                                <option value="Nomor">No BO</option>
                                                <option value="Corak">Corak</option>
                                            </select>
                                        </div>
                                        <div class="form_grid_2">   
                                            <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
                                        </div>
                                        <div class="form_grid_1">
                                            <div class="btn_24_blue">
                                                <input type="submit" name="" value="search">
                                            </div>
                                        </div>
                                    </div>

                                    <span class="clear"></span>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
                <form action="<?php echo base_url('BookingOrder/bulk'); ?>" method="post">
                    <div class="widget_content">
                        <table class="display" id="action_tbl">
                          <thead>
                            <tr>
                              <th class="center">
                                <input name="checkbox" type="checkbox" value="" class="checkall"> Do
                            </th>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nomor BO</th>
                            <th>Tanggal Selesai</th>
                            <th>Corak</th>
                            <th>Qty Yard</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($booking_order)){
                            $i=1;
                            foreach ($booking_order as $data) {
                                ?>
                                <tr class="odd gradeA">
                                    <td class="center tr_select"><input type="checkbox" name="bulk[]" value="<?php echo $data->Nomor; ?>" class="block_form"></td>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo DateFormat($data->Tanggal); ?></td>
                                    <td><?php echo $data->Nomor; ?></td>
                                    <td><?php echo DateFormat($data->Tanggal_selesai); ?></td>
                                    <td><?php echo $data->Corak; ?></td>
                                    <td class="text-right"><?php echo convert_currency($data->Qty); ?></td>
                                    <td><?php echo $data->Batal; ?></td>
                                    <td class="text-center ukuran-logo">
                                        <span><a class="action-icons c-Detail" href="<?php echo base_url('BookingOrder/show/'.$data->IDBO); ?>" title="Detail Data"> Detail</a></span>
                                        <?php if($data->Batal=="Aktif"){ ?>
                                        <span><a class="action-icons c-edit" href="<?php echo base_url('BookingOrder/edit/'.$data->IDBO); ?>" title="Ubah Data"> Edit</a></span>
                                        <?php } else {
                                            ?>
                                            <span><a class="action-icons c-edit" style="cursor: no-drop;" title="Ubah Data"> Edit</a></span>
                                            <?php
                                        } ?>
                                        <!--<span><a class="action-icons c-delete" href="#open-modal<?php echo $data->IDBO ?>" title="Non-aktifkan Data"> Delete</a></span>-->
                                        <?php if($data->Batal == "Aktif"){ ?>
                                        <span><a class="action-icons c-sa" href="<?php echo base_url('BookingOrder/soft_delete/'.$data->IDBO) ?>" title="Status Tidak Aktif">Status</a></span>
                                        <?php } else { ?>
                                        <span><a class="action-icons c-approve" href="<?php echo base_url('BookingOrder/soft_success/'.$data->IDBO) ?>" title="Status Aktif">Status</a></span>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <div class="widget_content text-right px-2">
                <div class="py-4 mx-2">
                    <div class="btn_24_blue">
                        <input type="submit" name="" value="Batal">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
<!-- ==================================== -->

<?php 
if ($booking_order !== "" || $booking_order !== null) {
    foreach ($booking_order as $data) {
        ?>
        <div id="open-modal<?php echo $data->IDBO; ?>" class="modal-window">
            <div>
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Konfirmasi Hapus</h1>
                <div class="modal-body">Apakah anda yakin akan me-non-aktifkan data ini ?</div>
                <div class="modal-foot">
                    <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div>
                    <div class="btn_30_blue">
                        <a href="<?php echo base_url() ?>BookingOrder/soft_delete/<?php echo $data->IDBO; ?>"><span> Non-aktifkan</span></a>
                    </div>
                </div>
            </div>
        </div>

        <?php 
    }
} 
?> 
<?php $this->load->view('administrator/footer') ; ?>