<?php $this->load->view('administrator/header'); ?>
<!--  style -->
<style type="text/css">
    .chzn-container{
        width: 101% !important;
    }
</style>
<!-- tittle data -->
<div class="page_title">
    <div id="alert"></div>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Edit Data Booking Order</h3>
</div>
<!-- ======================= -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>BookingOrder">Data Booking Order</a></li>
                            <li> Edit Booking Order </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Booking Order</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>BookingOrder/update" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <label class="field_title">Tanggal</label>
                                    <div class="form_input">
                                        <input type="date" class="form-control date-picker hidden-sm-down" name="Tanggal" id="Tanggal" value="<?php echo $data->Tanggal?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <span class="clear"></span>
                                    </div>
                                    <label class="field_title">Nomor BO</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" name="no_bo" id="no_bo" value="<?php echo $data->Nomor?>" readonly>
                                    </div>
                                    <label class="field_title">Tanggal Selesai</label>
                                    <div class="form_input">
                                        <input type="date" class="form-control date-picker hidden-sm-down" name="Tanggal_selesai" id="Tanggal_selesai" value="<?php echo $data->Tanggal_selesai?>" required oninvalid="this.setCustomValidity('Tanggal selesai Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <span class="clear"></span>
                                    </div>
                                    <label class="field_title">Corak</label>
                                    <div class="form_input">
                                        <select data-placeholder="Cari Berdasarkan Corak" style="width:100%;" class="chzn-select" tabindex="13" name="id_corak" id="id_corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            <option value=""></option>
                                            <?php foreach($corak as $row) { ?>
                                            <option value='<?php echo $row->IDCorak ?>' <?php echo $data->IDCorak == $row->IDCorak ? 'selected' : ''; ?>><?php echo $row->Corak?></option>";
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <label class="field_title">Qty Yard</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" name="qty" id="qty" value="<?php echo $data->Qty?>" onkeyup="this.value=this.value.replace(/[^0-9.]/,'')">
                                    </div>
                                    <label class="field_title">Keterangan</label>
                                    <div class="form_input">
                                        <textarea class="form-control" name="Keterangan" id="Keterangan" rows="5"><?php echo $data->Keterangan?></textarea>
                                    </div>
                                    <input type="hidden" value="<?php echo $data->IDBO?>" name="id">
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                       <!--  <div class="btn_30_light">
                                            <span><a href="<?php //echo base_url() ?>BookingOrder" name="simpan" title=".classname">Kembali</a></span>
                                        </div> -->
                                        <!-- <div class="btn_30_blue">
                                            <span><input type="button" class="btn_24_blue" id="print" value="Print Data"></span>
                                        </div> -->
                                        <div class="btn_30_blue">
                                            <span><input name="update" id="update" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>