<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Booking Order</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('BookingOrder')?>">Data Booking Order</a></li>
                            <li style="text-transform: capitalize;"> Detail Booking Order </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal</td>
                                    <td style="background-color: #ffffff;"><?php echo DateFormat($data->Tanggal)?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">Nomor Booking Order</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #ffffff;">Tanggal Selesai</td>
                                    <td  style="background-color: #ffffff;"><?php echo DateFormat($data->Tanggal_selesai) ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">Kode Corak</td>
                                    <td style="background-color: #e5eff0;"><?php echo $data->Kode_Corak ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Corak</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Corak ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Qty Yard</td>
                                    <td  style="background-color: #e5eff0;"><?php echo convert_currency($data->Qty) ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Batal?></td>
                                </tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table>

                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('BookingOrder')?>">Kembali</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>