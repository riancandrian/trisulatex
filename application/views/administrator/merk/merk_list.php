<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
  /*  .table_top{
        display: none !important;
        }*/
    </style>
    <!-- tittle data -->
    <div class="page_title">
     <?php echo $this->session->flashdata('Pesan');?>
     <span class="title_icon"><span class="blocks_images"></span></span>
     <h3><?= $title_master ?></h3>
     <div class="top_search">

     </div>
 </div>
 <!-- ======================= -->

 <!-- body data -->
 <div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                          <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                          <li class="active"><?= $title ?></li>
                      </ul>
                  </div>
              </div>

              <div class="widget_content">
                <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Merk/pencarian" class="form_container left_label">
                    <ul>
                        <li class="px-1">
                          <div class="form_grid_12 w-100 px">
                            <div class="form_grid_4 mx">
                                <div class="btn_24_blue">
                                    <a href="<?= $action ?>"><span><?= $button ?></span></a>
                                </div>
                            </div>

                            <div class="form_grid_2">
                                <select data-placeholder="Cari Berdasarkan" name="jenispencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                    <option value=""></option>
                                    <option value="Kode_Merk">Kode Merk</option>
                                     <option value="Merk">Merk</option>
                                </select>
                            </div>
                            <div class="form_grid_2">
                                <select data-placeholder="Cari Berdasarkan" name="statuspencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                    <option value=""></option>
                                     <option value="aktif">Aktif</option>
                                                    <option value="tidak aktif">Tidak Aktif</option>
                                </select>
                            </div>
                            <div class="form_grid_3">
                                <input name="keyword" type="text" placeholder="Masukkan Keyword">
                                <!-- <span class=" label_intro">Last Name</span> -->
                            </div>
                            <div class="form_grid_1 flot-right">
                                <div class="btn_24_blue">
                                    <input type="submit" name="" value="search">
                                </div>
                            </div>
                            <span class="clear"></span>

                        </div>
                    </li>
                </form>
            </div>
            <div class="widget_content">
                <table class="display data_tbl">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Merk</th>
                            <th>Merk</th>
                            <th>Aktif</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(empty($Merk)){
                            ?>
                            <?php
                        }else{
                            $i=1;
                            foreach ($Merk as $data) {
                                ?>
                                <tr class="odd gradeA">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $data->Kode_Merk; ?></td>
                                    <td><?php echo $data->Merk; ?></td>
                                    <td><?php echo $data->Aktif; ?></td>
                                   <td class="text-center ukuran-logo">
                                     <span><a class="action-icons c-edit" href="<?= site_url('Merk/update/'.$data->IDMerk); ?>" title="Ubah Data">Edit</a></span>
                                      <span><a class="action-icons c-delete" href="#open-modal<?php echo $data->IDMerk ?>" title="Hapus Data">Delete</a></span>
                                 </td>
                             </tr>
                             <?php
                             $i++;
                         }
                     }
                     ?>
                 </tbody>
             </table>
         </div>
     </div>
 </div>
</div>
</div>
<?php 
if ($Merk=="" || $Merk==null) {
}else{
    foreach ($Merk as $data) {
        ?>
        <div id="open-modal<?php echo $data->IDMerk ?>" class="modal-window">
   <div>
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Konfirmasi Hapus</h1>
                <div class="modal-body">Apakah anda yakin akan menghapus data ini ?</div>
                <div class="modal-foot">
                    <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div>
                    <div class="btn_30_blue">
                        <a href="<?= site_url('Merk/delete/'.$data->IDMerk); ?>"><span>Hapus</span></a>
                    </div>
                </div>
</div>
</div>
     <?php 
 }
} 
?> 
<?php $this->load->view('administrator/footer') ; ?>
