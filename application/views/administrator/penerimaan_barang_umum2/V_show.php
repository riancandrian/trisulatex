<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Penerimaan Barang Umum</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('Penerimaan_barang_umum2')?>">Data Penerimaan Barang</a></li>
                            <li style="text-transform: capitalize;"> Detail Penerimaan Barang </li>
                        </ul>
                    </div>
                </div>
                

                <div class="form_container left_label">
                    <div class="widget_content">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal</td>
                                    <td style="background-color: #ffffff;"><?php echo DateFormat($data->Tanggal)?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">Nomor Penerimaan Barang</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Supplier</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Nama ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">Total Qty</td>
                                    <td style="background-color: #e5eff0;"><?php echo $data->Total_qty ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Batal?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">Keterangan</td>
                                    <td style="background-color: #e5eff0;"><?php echo $data->Keterangan?></td>
                                </tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    </div>
                    <br>
                    <br><br>
                                 <div class="widget_content">

                <table class="display data_tbl">
                  <thead>
                   <tr>
                    <th class="center" style="width: 40px">No</th>
                    <th>Barang</th>
                    <th class="hidden-xs">Qty</th>
                    <th>Harga Satuan</th>
                    <th>Sub Total</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($pbdetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($pbdetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Nama_Barang; ?></td>
                          <td><?php echo $data2->Qty; ?></td>
                          <td><?php echo $data2->Harga_satuan; ?></td>
                          <td><?php echo $data2->Sub_total; ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>
                    <div class="widget_content py-4 text-center">
                        <div class="form_grid_12">
                            <div class="btn_30_light">
                                <span> <a href="<?php echo base_url('Penerimaan_barang_umum2')?>">Kembali</a></span>
                            </div>
                             <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>Penerimaan_barang_umum2/print_data_edit/<?php echo $data->IDTBSUmum ?>">Print Data</a></span>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>