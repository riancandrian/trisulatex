<?php $this->load->view('administrator/header') ;

if ($last_number->curr_number == null) {
    $number = 1;
} else {
    $number = $last_number->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen= $namaagent->Initial;

$code_booking = 'PBU'.date('y').$agen.$kodemax;
//echo $code_booking;
?>

<!-- style -->
<style type="text/css">
.chzn-container{
    width: 102.5% !important;
}
.chzn-container .chzn-drop{
    width: 198px !important;
}
.chzn-container-single .chzn-search input{
    width: 163px !important;
}
</style>
<!-- tittle data -->
<div class="page_title">
    <div id="alert"></div>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Penerimaan Barang Umum</h3>
</div>
<!-- ======================= -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Penerimaan_barang_umum2">Data Penerimaan Barang Umum</a></li>
                            <li> Tambah Penerimaan Barang Umum</li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Penerimaan Barang Umum</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <!--<form method="post" action="<?php echo base_url() ?>BookingOrder/proses_harga_jual_barang" class="form_container left_label">-->
                        <form class="form_container left_label">
                            <ul>
                                <li class="body-search-data">
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_8">
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">Tanggal</label>
                                                <div class="form_input">
                                                    <input type="date" class="form-control input-date-padding date-picker hidden-sm-down" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d');?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>

                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2">No PO</label>
                                                <div class="form_input">
                                                  <div class="list_left" style="width: 200px !important;">
                                                    <div class="list_filter">
                                                      <select name="no_sjc" id="no_sjc" required style=" width:100%;" class="chzn-select" tabindex="13" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong dan lakukan search')" oninput="setCustomValidity('')">
                                                        <option value=""></option>
                                                        <?php foreach ($nosjc as $sjc): ?>
                                                            <option value="<?php echo $sjc->Nomor ?>"><?php echo $sjc->Nomor ?></option>
                                                      <?php endforeach ?>
                                                  </select>


                                                  <!-- <button type="button" title='Cari'  onclick="search_sjc()" id="box1Clear" class="list_refresh" style="height: 28px;top: 0;position: absolute;right: 0 !important;left: 90%;"><span class="filter_btn" style="top: 1px; left: 3px;"></span></button> -->
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="form_grid_6 mb-1">
                                    <label class="field_title mt-dot2">Nomor PB</label>
                                    <div class="form_input input-not-focus">
                                        <input type="text" class="form-control" name="no_bo" id="no_bo" value="<?php echo $code_booking; ?>" readonly>
                                    </div>
                                </div>
                       
                                        <input type="hidden" class="form-control" name="id_po_umum" id="id_po_umum" readonly>

                                <div class="form_grid_6 mb-1">
                                    <label class="field_title mt-dot2">Supplier</label>
                                    <div class="form_input">
                                        <select data-placeholder="Cari Supplier" style="width:100%;" class="chzn-select" tabindex="13" name="id_supplier" id="id_supplier" required oninvalid="this.setCustomValidity('Supplier Tidak Boleh Kosong')">
                                            <?php foreach($supplier as $row) {
                                                echo "<option value='$row->IDSupplier'>$row->Nama</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form_grid_6 mb-1">
                                    <label class="field_title mt-dot2">Total Qty</label>
                                    <div class="form_input input-not-focus">
                                        <input type="text" class="form-control" name="total_qty" id="total_qty" readonly>
                                    </div>
                                </div>

                                <div class="form_grid_6 mb-1">
                                    <label class="field_title mt-dot2">Total Harga</label>
                                    <div class="form_input input-not-focus">
                                        <input type="text" class="form-control" name="total_harga" id="total_harga" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="form_grid_4 mb-1">
                                <label class="field_title mt-dot2">Keterangan</label>
                                <div class="form_input">
                                    <textarea class="form-control" name="keterangan" id="keterangan" style="resize: none;" rows="9"></textarea>
                                </div>
                            </div>
                            <span class="clear"></span>
                        </div>
                    </li>
                    <div class="widget_content">
                        <table class="display data_tbl" id="tabelfsdfsf">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Barang</th>
                                    <th class="text-center">Qty</th>
                                    <th class="text-center">Satuan</th>
                                    <th class="text-center">Harga</th>
                                </tr>
                            </thead>
                            <tbody id="tb_preview_penerimaan">
                            </tbody>
                        </table>
                    </div>
                    <li>
                     <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2">
                            <div class="btn_30_blue">
                                <span><input type="hidden" class="btn_24_blue" style="cursor: no-drop;" id="print_" name="print_" onclick="print_cek()" value="Print Data"></span>
                            </div>
                            <div class="btn_30_blue">
                                <span><input name="simpan_" id="simpan_" onclick="save()" type="button" class="btn_small btn_blue" value="Simpan Data"></span>
                            </div>
                        </div>

                          <!--            <div class="widget_content px-2 text-center">
                            <div class="py-4 mx-2"><div class="btn_30_blue">
                                <span><a id="print_" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                          </div>

                          <div class="btn_30_blue">
                            <span><a class="btn_24_blue" id="simpan_" style="cursor: pointer;" onclick="save()">Simpan</a>
                            </span>
                        </div>
                    </div>
                </div> -->
            </li>
        </ul>
    </form>
</div>
</div>
</div>
</div>
</div>
<script>
    document.getElementById("print_").disabled = true;
    var baseUrl = "<?php echo base_url()?>";
</script>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/penerimaan_barang_umum2.js"></script>
<?php $this->load->view('administrator/footer') ; ?>

