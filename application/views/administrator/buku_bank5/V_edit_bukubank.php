<?php $this->load->view('administrator/header') ; 

?>
<!-- =============================================Style untuk tabs -->
<style type="text/css" scoped>
  .chzn-container {
    width: 103% !important;
  }
</style>

<!--========================Head Content -->
<div class="page_title">
  <div id="alert"></div>
  <?php echo $this->session->flashdata('Pesan');?>
  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Data Buku Bank</h3>
  <div class="top_search">
  </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>BukuBank">Data Buku Bank</a></li>
              <li class="active">Ubah Buku Bank</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Ubah Buku Bank</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>
              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">
                  <div class="form_grid_6">
                    <!-- form 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tanggal</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Komutasigirong')" oninput="setCustomValidity('')" value="<?php echo $bukubank->Tanggal ?>">
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">No BB</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $bukubank->Nomor ?>" readonly>
                        <input type="hidden" name="IDBukuBank" id="IDBukuBank" class="form-control" placeholder="No Asset" value="<?php echo $bukubank->IDBukuBank ?>" readonly>
                      </div>
                    </div>

                    <!-- form 2 -->
                    <!-- <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Kode Perkiraan</label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" name="KodePerkiraan1" id="KodePerkiraan1">
                          <option value="">--Pilih Kode COA--</option>
                          <?php //foreach($coa as $row) {
                           // if ($bukubank->IDCOA == $row->IDCoa) {
                             // echo "<option value='$row->IDCoa' data-namaCOA='$row->Nama_COA' selected>$row->Kode_COA</option>";
                           // }
                           // echo "<option value='$row->IDCoa' data-namaCOA='$row->Nama_COA'>$row->Kode_COA</option>";
                         // }
                          ?>
                        </select>
                      </div>
                    </div> -->
                     <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Kode Perkiraan</label>
                      <div class="form_input">
                        <input type="hidden" name="IDCOA" id="KodePerkiraan1" class="form-control" placeholder="Kode Perkiraan" value="<?php echo $bukubank->IDCoa ?>" readonly>
                        <input type="text" name="KodePerkiraan1" id="KodePerkiraan3" class="form-control" placeholder="Kode Perkiraan" value="<?php echo $bukubank->Kode_COA ?>" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Nama Perkiraan</label>
                      <div class="form_input">
                        <input type="text" name="NamaPerkiraan3" id="NamaPerkiraan3" class="form-control" placeholder="Nama Perkiraan" value="<?php echo $bukubank->Nama_COA ?>" readonly>
                      </div>
                    </div>

                  

                  
                  </div>
                  <div class="form_grid_6">
                    <!-- radio 2 -->
                    <!-- <div class="form_grid_12 mb-1">
                      <label class="field_title dot-2">Posisi</label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" name="Posisi" id="Posisi">
                          <option value="Kredit">Kredit</option>
                          <option value="Debet">Debet</option>
                        </select>
                      </div>
                    </div> -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Posisi</label>
                      <div class="form_input">
                        <input type="text" name="Posisi" id="Posisi" class="form-control" placeholder="Posisi" value="<?php echo $bukubank->Posisi ?>" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="30" rows="5"><?= $bukubank->Keterangan ?></textarea>
                       
                      </div>
                    </div>

                    <!-- radio 3 -->
                  
                  </div>
                  <span class="clear"></span>
                </div> <hr>
                <div class="form_grid_12 multiline">
                  <div class="form_grid_6">
                    <label class="field_title mt-dot2">Kode Perkiraan</label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" name="KodePerkiraan2" id="KodePerkiraan2">
                          <option value="">--Pilih Kode COA--</option>
                          <?php foreach($coa as $row) {
                            echo "<option value='$row->IDCoa' data-namaCOA='$row->Nama_COA' data-Kode_COA='$row->Kode_COA'>$row->Kode_COA</option>";
                          }
                          ?>
                        </select>
                      </div>
                    <span class="clear"></span>

                    <label class="field_title mt-dot2">Nama Perkiraan</label>
                    <div class="form_input">
                      <input type="text" name="NamaPerkiraan2" id="NamaPerkiraan2" class="form-control" placeholder="Nama Perkiraan" readonly>
                    </div>
                    <span class="clear"></span>
                  </div>

                  <div class="form_grid_6">
                  <label class="field_title mt-dot2" id="ubahPosisi">Kredit</label>
                    <div class="form_input">
                      <input type="text" name="KreditatauDebet" id="KreditatauDebet" class="form-control" placeholder="xxx">
                    </div>
                    <span class="clear"></span>

                    <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan2" id="Keterangan2" cols="30" rows="5"></textarea>
                      </div>
                    <span class="clear"></span>

                    <div class="form_input">
                      <div class="btn_24_blue">
                        <button class="btn_24_blue" type="button" id="tambah_sementara_edit">
                          Tambah Data
                        </button>
                      </div>
                    </div>
                  </div>
                  <span class="clear"></span>
                </div>
              </li>

              <!-- =============================-Table Bawah -->
              <div class="widget_content">
                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Perkiraan</th>
                      <th>Nama Perkiraan</th>
                      <th>Posisi</th>
                      <th>Debet</th>
                      <th>Kredit</th>
                      <th>Keterangan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>
              </div>

              <li>
                <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                    <div class="btn_30_blue">
                     <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                  </div>
                  <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save_change()" type="button" value="Simpan Data" title="Simpan Data"></span>
                  </div>
                </div>
              </li>
            </ul>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/buku_bank.js"></script>
<script>
  $(document).ready(function(){
    renderJSONE($.parseJSON('<?php echo json_encode($databukubankdetail) ?>'));
      });
    </script>
<?php $this->load->view('administrator/footer') ; ?>