<?php $this->load->view('administrator/header') ;


if ($koderetur->curr_number == null) {
    $number = 1;
} else {
    $number = $koderetur->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_rb = 'RJK'.date('y').$agen.$kodemax;
?>
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Retur Penjualan</h3>

    <div class="top_search">
    </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>ReturPenjualan">Data Retur Penjualan</a></li>
                            <li class="active">Tambah Retur Penjualan</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Retur Penjualan</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="" class="form_container left_label">
                        <ul>
                            <li class="body-search-data">
                                <!-- form search header -->
                                <div class="form_grid_12 w-100 alpha">
                                    <!-- form 1 -->
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2 mb">No RJ</label>
                                        <div class="form_input">
                                            <input id="RB" type="text" name="RB" class="form-control" placeholder="No RB" required oninvalid="this.setCustomValidity('No RB Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $code_rb ?>">
                                        </div>
                                    </div>
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb">Tanggal SJ</label>
                                        <div class="form_input">
                                            <input id="TanggalInv" type="date" name="TanggalInv" class="form-control" placeholder="Tanggal Inv Pembelian" required oninvalid="this.setCustomValidity('Tanggal Inv Pembelian Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        </div>
                                    </div>
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2 mb">Tanggal RJ</label>
                                        <div class="form_input">
                                            <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                                        </div>
                                    </div>
                                       <!--  <div class="form_grid_6 mb-1">
                                           
                                            <div class="form_input">
                                                <input id="jenis" type="checkbox" name="jenis" onclick="return false;" value="Discount" class="input-date-padding form-control"><span>Discount</span>
                                                <input id="jenis2" type="checkbox" name="jenis2" onclick="return false;" value="PPN" class="input-date-padding form-control">
                                                <span>PPN</span>
                                            </div>
                                        </div> -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Customer</label>
                                            <div class="form_input">
                                                <!-- <select data-placeholder="Cari Nama Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDSupplier"  id="IDSupplier" required oninvalid="this.setCustomValidity('Nama Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                    <option value=""></option>
                                                    <?php
                                                    //foreach ($supplier as $data) {
                                                        ?>
                                                        <option value="<?php //echo $data->IDSupplier ?>"><?php// echo $data->Nama; ?></option>
                                                        <?php
                                                    //}
                                                    ?>
                                                </select> -->
                                                <input type="hidden" name="IDSupplier" id="IDSupplier" readonly>
                                                <input type="text" name="NamaSupplier" id="NamaSupplier" readonly>
                                            </div>
                                        </div>

                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Status PPN</label>
                                            <div class="form_input">
                                               <!--  <select data-placeholder="Status PPN" style=" width:100%;" class="chzn-select" tabindex="13"  name="Status_ppn"  id="Status_ppn" required oninvalid="this.setCustomValidity('Nama Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                    <option value="Include">Include</option>
                                                    <option value="Exclude">Exclude</option>
                                                </select> -->
                                                <input type="text" name="Status_ppn" id="Status_ppn" readonly>
                                            </div>
                                        </div>

                                        <!-- form 5 -->
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">No Invoice</label>
                                            <div class="form_input">
                                                <select name="Nomor" id="Nomor" class="chzn-select" onchange ="cek_grandtotal()" style="width: 89% !important" class="form-control">
                                                    <option value="">Pilih No Invoice</option>
                                                    <?php
                                                    foreach($inv as $invs)
                                                    { 
                                                        ?>
                                                        <option value="<?php echo $invs->Nomor ?>"><?php echo $invs->Nomor ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <!-- <input id="Nomor" type="text" style="width: 89% !important" name="Nomor" onkeyup="cek_grandtotal()" class="form-control" placeholder="No Inv Penjualan"> -->
                                                <!-- <button type="button" id="add-to-cart" class="list_refresh"><span class="filter_btn" style="top: 1px; left: 3px;"></span></button> -->
                                                <!--  <select data-placeholder="Cari No Invoice" style=" width:100%;" class="chzn-select" tabindex="13"  name="Nomor"  id="Nomor" required oninvalid="this.setCustomValidity('No Invoice Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="cek_grandtotal()">
                                                <?php
                                                //foreach ($inv as $data) {
                                                ?>
                                                    <option value="<?php// echo $data->Nomor ?>"><?php// echo $data->Nomor; ?></option>
                                                <?php
                                                //}
                                                ?>
                                            </select> -->
                                        </div>
                                    </div>

                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb">No Surat Jalan</label>
                                        <div class="form_input">
                                            <input id="suratjalan" type="text" name="suratjalan" class="form-control" placeholder="Surat Jalan" required oninvalid="this.setCustomValidity('Surat Jalan Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                    <input id="totalqty" type="hidden" name="totalqty" class="form-control">
                                    <input id="totalmeter" type="hidden" name="totalmeter" class="form-control">

                                    <!-- clear content -->
                                    <div class="clear"></div>
                                </div>
                            </li>

                                <!-- <li>
                                  <div class="form_grid_12">
                                    <div class="form_input ml text-center w-100">
                                      <button class="btn_24_blue" type="button" id="add-to-cart">
                                        Tambah Data
                                      </button>


                                    </div>
                                  </div>
                              </li> -->

                          </ul>
                          <div>
                              <table class="display data_tbl">
                                  <thead>
                                      <tr>
                                          <th class="center" style="width: 40px">No</th>
                                          <th>do</th>
                                          <th class="hidden-xs">Barcode</th>
                                          <th>Corak</th>
                                          <th>Warna</th>
                                          <th>Merek</th>
                                          <th>Grade</th>
                                          <th>Qty Yard</th>
                                          <th>Qty Meter</th>
                                          <th>Satuan</th>
                                          <th>Keterangan</th>
                                          <th>Harga</th>
                                          <th>Sub Total</th>
                                      </tr>
                                  </thead>
                                  <tbody id="tb_preview_penerimaan">

                                  </tbody>
                              </table>
                          </div>
                          <ul>

                              <li>
                                  <div class="form_grid_12 text-center">
                                      <div class="btn_30_blue">
                                          <span><button class="btn_24_blue" id="hapus" type="button" onclick="hapus_array()">Hapus</button></span>
                                      </div>
                                  </div>
                              </li>
                              <input type="hidden" id="Pembayaran" name="Pembayaran" class="form-control" placeholder="Pembayaran">
                              <li>
                                  <div class="form_grid_12">
                                      <div class="form_grid_8">
                                          <label class="field_title mt-dot2 mb">Keterangan</label>
                                          <div class="form_input">
                                              <textarea id="Keterangan" type="text" name="Keterangan" class="form-control" placeholder="Keterangan" required oninvalid="this.setCustomValidity('Keterangan Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="resize: none;" rows="8"></textarea>
                                          </div>
                                      </div>
                                      <div class="form_grid_4">
                                          <div class="form_grid_12 mb-1">
                                              <label class="field_title mt-dot2 mb">DPP</label>
                                              <div class="form_input input-not-focus">
                                                  <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                                              </div>
                                          </div>

                                          <!--   <div class="form_grid_12 mb-1">
                                                <label class="field_title mt-dot2 mb">Discount</label>
                                                <div class="form_input input-not-focus"> -->
                                                    <input type="hidden" value="0" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" readonly>
                                                <!-- </div>
                                                </div> -->
                                                <div class="form_grid_12 mb-1">
                                                    <label class="field_title mt-dot2 mb">PPN</label>
                                                    <div class="form_input input-not-focus">
                                                        <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" readonly>
                                                    </div>
                                                </div>
                                                <div class="form_grid_12 mb-1">
                                                    <label class="field_title pt mb">Total Invoice</label>
                                                    <div class="form_input input-not-focus">
                                                        <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clear"></div>
                                        </div>
                                    </li>
                                </ul>
                            </form>
                            <div class="widget_content px-2 text-center form_container left_label">
                                <ul>
                                    <li>
                                        <div class="form_grid_12">
                                            <div class="py-4 mx-2">
                                                <div class="btn_30_blue">
                                                    <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                                                </div>
                                                <div class="btn_30_blue">
                                                    <span><a class="btn_24_blue" id="simpan" onclick="saveRetur()">Simpan</a></span>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript">
            var base_url = window.location.pathname.split('/');
            function Retur(){
                this.Barcode;
                this.Corak;
                this.Warna;
                this.Merk;
                this.Qty_yard;
                this.Qty_meter;
                this.Grade;
                this.Satuan;
                this.Lebar;
                this.Tanggal;
                this.NoPB;
                this.ID_supplier;
                this.IDBarang;
                this.Remark;
                this.Keterangan;
                this.Total_yard;
                this.Total_meter;
                this.Harga;
                this.Sub_total;
                this.IDFJK;
                this.Status_ppn;
            }

            var PM = new Array();
            index = 0;
            totaldpp=0;


            $(document).ready(function(){
                var PB = document.getElementById("Nomor");
                $('#Nomor').change(function(){
            //event.preventDefault();
            $(".rowarray").remove();
            Nomor = $('#Nomor').val();
            //alert($('#no_sjc').val());

            $.post("<?php echo base_url("ReturPenjualan/check_invoice_penjualan")?>", {'Nomor' : Nomor})
            .done(function(response) {

                var json = JSON.parse(response);
                // console.log(json);
                if (!json.error) {
                    index=0;
                    $.each(json.data, function (key, value) {


                        totaldpp=0;
                        totalqtyyard=0;
                        totalqtymeter=0;


                        var M = new Retur();

                        M.Barcode = value.Barcode;
                        M.Grade = value.Grade;
                        M.Corak = value.Corak;
                        M.Warna = value.Warna;
                        M.Merk = value.Merk;
                        M.IDBarang = value.IDBarang;
                        M.IDCorak = value.IDCorak;
                        M.IDWarna = value.IDWarna;
                        M.IDMerk = value.IDMerk;
                        M.Qty_yard = value.Qty_yard;
                        M.Qty_meter = value.Qty_roll;
                        M.Satuan = value.Satuan;
                        M.IDSatuan = value.IDSatuan;
                        M.Harga = value.Harga;
                        M.Sub_total = value.Sub_total;
                        M.Keterangan = value.Keterangan;
                        M.IDFJK = value.IDFJK;
                        M.Status_ppn = value.Status_ppn;
                        M.Sub_total = value.Sub_total;

                        PM[index] = M;
                        index++;

                        $("#tb_preview_penerimaan").html("");
                        for(i=0; i < index; i++){
                            value = "<tr id='rowarray"+i+"'>"+
                            "<td class='text-center'>"+(i+1)+"</td>"+
                            "<td>"+"<input type='checkbox' name='do' id='do"+i+"'>"+"</td>"+
                            "<td>"+PM[i].Barcode+"</td>"+
                            "<td>"+PM[i].Corak+"</td>"+
                            "<td>"+PM[i].Merk+"</td>"+
                            "<td>"+PM[i].Warna+"</td>"+
                            "<td>"+PM[i].Grade+"</td>"+
                            "<td>"+PM[i].Qty_yard+"</td>"+
                            "<td>"+PM[i].Qty_meter+"</td>"+
                            "<td>"+PM[i].Satuan+"</td>"+
                            "<td>"+PM[i].Keterangan+"</td>"+
                            "<td>"+rupiah(PM[i].Harga)+"</td>"+
                            "<td>"+rupiah(PM[i].Sub_total)+"</td>"+
                            "</tr>";
                            $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);
                                // DPP += parseInt(PM[i].Sub_total);
                                if($("#Status_ppn").val()=="Include"){
                                    totaldpp += parseInt(PM[i].Sub_total);
                                    ppn= totaldpp*0.1;
                                    $("#DPP").val(rupiah(totaldpp-ppn));
                                    $("#PPN").val(rupiah(ppn));
                                    total= totaldpp-ppn;
                                    $("#total_invoice").val(rupiah(total+ppn));
                                }else{
                                    totaldpp += parseInt(PM[i].Sub_total);
                                    ppn= totaldpp*0.1;
                                    $("#DPP").val(rupiah(totaldpp));
                                    $("#PPN").val(rupiah(ppn));
                                    $("#total_invoice").val(rupiah(totaldpp+ppn));
                                }
                                totalqtyyard += parseInt(PM[i].Qty_yard);
                                totalqtymeter += parseInt(PM[i].Qty_meter);
                            }
                            // disk= $("#Discount_total").val();
                            // ppn_inv= $("#PPN").val();
                            // $("#DPP").val(DPP);
                            // t= DPP-parseInt(disk)+parseInt(ppn_inv);
                            // $("#total_invoice").val(t);
                            $("#totalqty").val(totalqtyyard);
                            $("#totalmeter").val(totalqtymeter);
                        });
                } else if($("#Nomor").val()==''){

                }else {
                    alert('Tidak ada data dengan nomor Penerimaan ' + Nomor);
                }

            });
});
});

function hapus_array()
{

    var totalarr=0;
    var totalinv=0;
    for(i=0; i < index; i++){
        if($('#do'+i).is(":checked"))
        {
            totalarr = parseInt(PM[i].Sub_total);

            $('#rowarray'+i).remove();
            DPP-=totalarr;
            PM.splice(i,0);
            $("#DPP").val(DPP);
            totalinv += DPP-parseInt(disk)+parseInt(ppn_inv);
            $("#total_invoice").val(totalinv);
        }
    }
}

function cek_grandtotal()
{
    var totalinv=0;
    var Nomor = $("#Nomor").val();
    $.ajax({
        url : '/'+base_url[1]+'/ReturPenjualan/cek_grand_total',
        type: "POST",
        data:{Nomor:Nomor},
        dataType:'json',
        success: function(data)
        {
            var html = '';
            if (data <= 0) {
                console.log('-----------data kosong------------');
            } else {
                        //console.log(data);
                        //console.log(data[0].Discount);
                    //     if(data[0].Discount==0){
                    //         $("#jenis").prop( "checked", false );
                    //     }else{
                    //         $("#jenis").prop( "checked", true );
                    //     }
                    // }

                    // if(data[0].PPN==0){
                    //     $("#jenis2").prop( "checked", false );
                    // }else{
                    //     $("#jenis2").prop( "checked", true );
                    // }

                    // if($('#jenis').is(":checked")){
                    //     $("#Discount_total").val(data[0].Discount);
                    // }else{
                    //     a=0;
                    //     $("#Discount_total").val(a);
                    // }

                    // if($('#jenis2').is(":checked")){
                    //     $("#PPN").val(data[0].PPN);
                    // }else{
                    //     a=0;
                    //     $("#PPN").val(a);
                    // }

                    $("#Pembayaran").val(data[0].Pembayaran);
                    $("#suratjalan").val(data[0].Nomor);
                    $("#IDSupplier").val(data[0].IDCustomer);
                    $("#NamaSupplier").val(data[0].Nama);
                    $("#Status_ppn").val(data[0].Status_ppn);
                    $("#TanggalInv").val(data[0].Tanggal);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
}


function fieldRetur(){
    var data1 = {
        "Tanggal" :$("#Tanggal").val(),
        "RJ":$("#RB").val(),
        "IDSupplier":$("#IDSupplier").val(),
        "TanggalInv":$("#TanggalInv").val(),
        "Keterangan":$("#Keterangan").val(),
        "Nomor":$("#Nomor").val(),
        "totalqty":$("#totalqty").val(),
        "totalmeter":$("#totalmeter").val(),
        "Discount_total":$("#Discount_total").val(),
        "Pembayaran":$("#Pembayaran").val(),
        "DPP":$("#DPP").val(),
        "PPN":$("#PPN").val(),
        "Status_ppn":$("#Status_ppn").val(),
        "total_invoice":$("#total_invoice").val()
    }
    return data1;
}

function saveRetur(){
    var data1 = fieldRetur();
    $("#Nomor").css('border', '');
    $("#TanggalInv").css('border', '');
    $("#NamaSupplier").css('border', '');
    $("#Status_ppn").css('border', '');

    if($("#Nomor").val()==""){
        $('#alert').html('<div class="pesan sukses">Data SJ Tidak Boleh Kosong</div>');
        $("#Nomor").css('border', '1px #C33 solid').focus();
    }else if($("#TanggalInv").val()==""){
        $('#alert').html('<div class="pesan sukses">Tanggal SJ Tidak Boleh Kosong</div>');
        $("#TanggalInv").css('border', '1px #C33 solid').focus();
    }else if($("#Status_ppn").val()==""){
        $('#alert').html('<div class="pesan sukses">Status_ppn Tidak Boleh Kosong</div>');
        $("#Status_ppn").css('border', '1px #C33 solid').focus();
    }else if($("#NamaSupplier").val()==""){
        $('#alert').html('<div class="pesan sukses">Nama Customer Tidak Boleh Kosong</div>');
        $("#NamaSupplier").css('border', '1px #C33 solid').focus();
    }else{
        console.log(PM);
        console.log(data1);
        $.ajax({
            url: "simpan_retur_penjualan",
            type: "POST",
            data: {
                "data1" : data1,
                "data2" : PM
            },
                // dataType: 'json',
                success: function (msg, status) {
                    // window.location.href = "index";
                    $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');

                    document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
                    document.getElementById("simpan").setAttribute("onclick", "return false;");
                    document.getElementById("print").setAttribute("style", "cursor: pointer;");
                    document.getElementById("print").setAttribute("onclick", "return true;");
                    document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/ReturPenjualan/print_/'+data1['RJ']);

                },
                error: function(msg, status){
                    console.log(msg);
                    $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
                }
            });
    }
}
function cek_customer()
{
    var Nomor = $("#Nomor").val();
    $.ajax({
        url : '/'+base_url[1]+'/ReturPenjualan/cek_customer',
        type: "POST",
        data:{Nomor:Nomor},
        dataType:'json',
        success: function(data)
        { 
            var html = '';
            if (data <= 0) {
                console.log('-----------data kosong------------');
            } else {
                $("#NamaCustomer").val(data[0].Nama);
                $("#IDCustomer").val(data[0].IDCustomer);
                $("#Tanggal_jatuh_tempo").val(data[0].Tanggal_jatuh_tempo);
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}
</script>
<?php $this->load->view('administrator/footer') ; ?>