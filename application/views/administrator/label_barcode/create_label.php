
<?php $this->load->view('administrator/header') ; 

?>

<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Label Barcode</h3>
</div>

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li> Tambah Label Barcode </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Label Barcode</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>LabelBarcode/proses_tambah_barcode" enctype="multipart/form-data" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <!-- Kode Group Barang -->
                                    <label class="field_title">Barcode Scan</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Barcode Scan" name="barcode" required oninvalid="this.setCustomValidity('Barcode Scan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Group Barang -->
                                    <label class="field_title">NOSO</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="NOSO" name="noso" required oninvalid="this.setCustomValidity('NOSO Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Status -->
                                    <label class="field_title">Party</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Party" name="party" required oninvalid="this.setCustomValidity('Party Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <label class="field_title">Indent</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Indent" name="indent" required oninvalid="this.setCustomValidity('Indent Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <label class="field_title">CustDes</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Custdes" name="custdes" required oninvalid="this.setCustomValidity('Custdes Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <label class="field_title">Warna Cust</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Warnacust" name="warnacust" required oninvalid="this.setCustomValidity('Warna cust Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <label class="field_title">Panjang Yrd</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" id="p_yard" name="panjang" 
                                        onkeyup="hilang_huruf(this.value, 'C')" placeholder="Panjang Yard" required oninvalid="this.setCustomValidity('Panjang Yard Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <label class="field_title">Panjang Mtr</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" id="p_meter" name="panjangm" 
                                        onkeyup="hilang_huruf2(this.value, 'F')" placeholder="Panjang Meter" required oninvalid="this.setCustomValidity('Panjang Meter Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <label class="field_title">Grade</label>
                                    <div class="form_input">
                                       <select name="grade" class="form-control">
                                        <option value="A" selected>A</option>
                                        <option value="B">B</option>
                                        <option value="S">S</option>
                                        <option value="E">E</option>
                                    </select>    
                                </div>

                                <label class="field_title">Remark</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" placeholder="Remark" name="remark" required oninvalid="this.setCustomValidity('Remark Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                </div>

                                <label class="field_title">Lebar</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" placeholder="Lebar" name="lebar" required oninvalid="this.setCustomValidity('Lebar Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                </div>

                                <label class="field_title">Satuan</label>
                                <div class="form_input">
                                    <select name="satuan" class="form-control">
                                        <option value="Yard" selected>Yard</option>
                                        <option value="Meter">Meter</option>
                                    </select>  
                                </div>

                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <div class="form_input">
                                    <center>
                                      <input type="submit" disabled value="Print" class="btn btn-primary">
                                      <input type="submit" name="simtam" class="btn btn-primary" value="Simpan">
                                  </center>
                              </div>
                          </div>
                      </li>
                  </ul>
              </form>
          </div>
      </div>
  </div>
</div>
</div>
<script type="text/javascript">
    function hilang_huruf(value, degree)
    {
  // this.value=this.value.replace(/[^0-9.]/,'');
  document.getElementById("p_yard").value=value.replace(/[^0-9.]/,'');
  var x;
  if (degree == "C") {
    x = document.getElementById("p_yard").value * 0.9144
    document.getElementById("p_meter").value = x.toFixed(2);
} else {
    x = (document.getElementById("p_meter").value * 1.09361);
    document.getElementById("p_yard").value = x.toFixed(2);
}
}

function hilang_huruf2(value, degree)
{
  // this.value=this.value.replace(/[^0-9.]/,'');
  document.getElementById("p_meter").value=value.replace(/[^0-9.]/,'');
  var x;
  if (degree == "C") {
    x = document.getElementById("p_yard").value * 0.9144
    document.getElementById("p_meter").value = x.toFixed(2);
} else {
    x = (document.getElementById("p_meter").value * 1.09361);
    document.getElementById("p_yard").value = x.toFixed(2);
}
}
</script>
</script>
<?php $this->load->view('administrator/footer') ; ?>