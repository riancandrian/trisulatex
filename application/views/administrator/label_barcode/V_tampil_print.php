<?php $this->load->view('administrator/header') ; 

?>

<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Label Barcode</h3>
</div>

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li>Label Barcode </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Label Barcode</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url();?>LabelBarcode/printData/<?php echo $tampilprinttambah->IDBarcodePrint;?>" target="__blank" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <!-- Kode Group Barang -->
                                    <label class="field_title">Barcode Scan</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Barcode Scan" name="barcode" required oninvalid="this.setCustomValidity('Barcode Scan Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Barcode; ?>" readonly>
                                    </div>

                                    <!-- Group Barang -->
                                    <label class="field_title">NOSO</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="NOSO" name="noso" required oninvalid="this.setCustomValidity('NOSO Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->NoSO; ?>" readonly>
                                    </div>

                                    <!-- Status -->
                                    <label class="field_title">Party</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Party" name="party" required oninvalid="this.setCustomValidity('Party Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Party; ?>" readonly>
                                    </div>

                                    <label class="field_title">Indent</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Indent" name="indent" required oninvalid="this.setCustomValidity('Indent Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Indent; ?>" readonly>
                                    </div>

                                    <label class="field_title">CustDes</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Custdes" name="custdes" required oninvalid="this.setCustomValidity('Custdes Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->CustDes; ?>" readonly>
                                    </div>

                                    <label class="field_title">Warna Cust</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Warnacust" name="warnacust" required oninvalid="this.setCustomValidity('Warna cust Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->WarnaCust; ?>" readonly>
                                    </div>

                                    <label class="field_title">Panjang Yrd</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" id="p_yard" name="panjang" 
                                       placeholder="Panjang Yard" required oninvalid="this.setCustomValidity('Panjang Yard Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Panjang_Yard; ?>" readonly>
                                    </div>

                                    <label class="field_title">Panjang Mtr</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" id="p_meter" name="panjangm" 
                                        placeholder="Panjang Meter" required oninvalid="this.setCustomValidity('Panjang Meter Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Panjang_Meter; ?>" readonly>
                                    </div>

                                    <label class="field_title">Grade</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" name="grade" placeholder="Panjang Yard" required oninvalid="this.setCustomValidity('Panjang Yard Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Grade; ?>" readonly>
                                    </div>

                                <label class="field_title">Remark</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" placeholder="Remark" name="remark" required oninvalid="this.setCustomValidity('Remark Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Remark; ?>" readonly>
                                </div>

                                <label class="field_title">Lebar</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" placeholder="Lebar" name="lebar" required oninvalid="this.setCustomValidity('Lebar Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Lebar; ?>" readonly>
                                </div>

                                <label class="field_title">Satuan</label>
                                <div class="form_input">
                                        <input type="text" class="form-control" name="grade" placeholder="Panjang Yard" required oninvalid="this.setCustomValidity('Panjang Yard Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Satuan; ?>" readonly >
                                    </div>

                            </div>
                        </li>

                        <li>
                            <div class="form_grid_12">
                                <div class="form_input">
                                    <center>
                                      <input type="submit" disabled value="Print" class="btn btn-primary">
                                      <input type="submit" name="simtam" class="btn btn-primary" value="Simpan">
                                  </center>
                              </div>
                          </div>
                      </li>
                  </ul>
              </form>
          </div>
      </div>
  </div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>