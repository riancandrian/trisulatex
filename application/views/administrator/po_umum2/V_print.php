<!DOCTYPE HTML>
<html>

<head>
	<title>PT Trisula Textile Industries Tbk</title>
	<link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen,print">
	<link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen,print">

</head>
<body>
	<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">

					<h1 style="text-align: center;" class="mb">DYEING-ORDER</h1>
					<div class="widget_content">
						<div class=" page_content">
							<div class="invoice_container">
								<div class="grid_6 invoice_num" style="margin-bottom: 0">
									<table style="font-size: 12px">
										<tr>
											<td>Tanggal</td>
											<td> : </td>
											<td><?php echo $print->Tanggal ?></td>
										</tr>
										<tr>
											<td>No Urut</td>
											<td> : </td>
											<td><?php echo $print->Nomor ?></td>
										</tr>

									</table>

								</div>

								<div class="grid_6 invoice_num mb" style="margin-bottom: 0">
									<table style="font-size: 12px">
										<tr>
											<td>Agent</td>
											<td> : </td>
											<td><?php echo $print->Nama_Perusahaan ?></td>
										</tr>

									</table>
									
								</div>
								
								<div class="grid_12 invoice_details">
									<div class="invoice_tbl">
										<table>
											<thead>
												<tr class=" gray_sai">
													<th>
														No
													</th>
													<th>
														Barang
													</th>
													<th>
														Qty
													</th>
													<th>
														Satuan
													</th>
													<th>
														Harga
													</th>
												</tr>
											</thead>
											<tbody>
												<?php if(empty($printdetail)){
													?>
													<?php
												}else{
													$i=1;
													foreach ($printdetail as $data2) {
														?>
														<tr class="odd gradeA">
															<td><?php echo $i; ?></td>
															<td><?php echo $data2->Nama_Barang; ?></td>
															<td><?php echo $data2->Qty; ?></td>
															<td><?php echo $data2->Satuan; ?></td>
															<td><?php echo $data2->Harga_satuan; ?></td>
														</tr>
														<?php
														$i++;
													}
												}
												?>


											</tbody>
										</table>
									</div>
									<u><h4><strong>CATATAN - CATATAN</strong></h4></u>
										<div style="margin-top: .5rem">
											<?php echo $print->Catatan ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="clear"></span>
			</div>
		</body>
		</html>
		<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				window.print();
				console.log('masuk kesini');
     			window.location.href = "<?php echo base_url() ?>PoUmum2/index";
  });
</script>
