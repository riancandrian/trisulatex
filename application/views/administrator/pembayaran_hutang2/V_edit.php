<?php $this->load->view('administrator/header') ; ?>
    <style type="text/css">
        .chzn-container {
            width: 102.5% !important;
        }
    </style>
    <div class="page_title">
        <?php echo $this->session->flashdata('Pesan');?>
        <span class="title_icon"><span class="blocks_images"></span></span>
        <h3>Data Pembayaran Hutang</h3>

        <div class="top_search">
        </div>
    </div>
    <!-- ======================= -->

    <div id="content">
        <div class="grid_container">
            <div class="grid_12">
                <div class="widget_wrap">
                    <div class="breadCrumbHolder module">
                        <div id="breadCrumb0" class="breadCrumb module white_lin">
                            <ul>
                                <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                                <li><a href="<?php echo base_url() ?>PembayaranHutang">Pembayaran Hutang</a></li>
                                <li class="active">Pembayaran Hutang</li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget_top">
                        <div class="grid_12">
                            <span class="h_icon blocks_images"></span>
                            <h6>Pembayaran Hutang</h6>
                        </div>
                    </div>
                    <div class="widget_content">
                        <form method="post" action="" class="form_container left_label">
                            <ul>
                                <li class="body-search-data">
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">No PH</label>
                                            <div class="form_input">
                                                <input id="PH" type="text" name="PH" class="form-control" required  value="<?php echo $data->Nomor ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb">Supplier</label>
                                            <div class="form_input">
                                                <select data-placeholder="Cari Supplier" class="chzn-select" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem" tabindex="13" onchange="check_hutang()" name="IDSupplier" id="IDSupplier" required>
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($supplier as $row) { ?>
                                                        <option value="<?php echo $row->IDSupplier ?>" <?php echo $row->IDSupplier == $data->IDSupplier ? 'selected' : '' ?>><?php echo $row->Nama; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb">Tanggal PH</label>
                                            <div class="form_input">
                                                <input id="Tanggal_PH" type="date" name="Tanggal_PH" class="form-control" value="<?php echo $data->Tanggal?>" required >
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Keterangan</label>
                                            <div class="form_input">
                                                <textarea class="form_contol" rows="3" name="keterangan" id="keterangan"><?php echo $data->Keterangan?></textarea>
                                            </div>
                                        </div>

                                        <input id="totalqty" type="hidden" name="totalqty" class="form-control">
                                        <input id="totalmeter" type="hidden" name="totalmeter" class="form-control">

                                        <!-- clear content -->
                                        <div class="clear"></div>
                                    </div>
                                </li>
                            </ul>

                            <!-- content body tab & pane -->
                            <div class="grid_12 mx w-100">
                                <div class="widget_wrap tabby mb-4">
                                    <div class="widget_top">
                                        <!-- button tab & pane -->
                                        <div id="widget_tab">
                                            <ul>
                                                <li class="px py"><a href="#tab1" class="active_tab">List Hutang</a></li>
                                                <li class="px py"><a href="#tab2">Total Pembayaran</a></li>
                                            </ul>
                                        </div>
                                        <!-- end button tab & pane -->
                                    </div>

                                    <!-- tab & pane body -->
                                    <div class="widget_content">
                                        <!-- tab 1 -->
                                        <div id="tab1">
                                            <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                                <div class="grid_12 w-100">
                                                    <span class="h_icon blocks_images"></span>
                                                    <h6>List Hutang</h6>
                                                </div>
                                            </div>
                                            <div>
                                                <table class="display data_tbl">
                                                    <thead>
                                                    <tr>
                                                        <th class="center" style="width: 40px">No</th>
                                                        <!--<th>do</th>-->
                                                        <th class="hidden-xs">No Invoice</th>
                                                        <th>Tanggal Faktur</th>
                                                        <th>JT Faktur</th>
                                                        <th>Nilai Faktur</th>
                                                        <th>Selisih</th>
                                                        <th>Telah dibayar</th>
                                                        <!--<th>Dibayar</th>-->
                                                        <th>Saldo</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tb_preview_penerimaan">

                                                    </tbody>
                                                </table>
                                                <ul>
                                                    <input type="hidden" id="Pembayaran" name="Pembayaran" class="form-control" placeholder="Pembayaran">
                                                    <li>
                                                        <div class="form_grid_12">
                                                            <div class="form_grid_4">
                                                                <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mt-dot2 mb">Total</label>
                                                                    <div class="form_input">
                                                                        <input type="text" class="form-control" name="total" id="total" value="<?php echo $data->Total ?>" readonly>
                                                                        <input type="hidden" name="pembayaran_db" id="pembayaran_db">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form_grid_4">
                                                                <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mt-dot2 mb">Pembayaran</label>
                                                                    <div class="form_input input-not-focus">
                                                                        <input type="text" id="pembayaran" name="pembayaran" class="form-control" value="<?php echo $data->Pembayaran?>" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form_grid_4">
                                                                <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mt-dot2 mb">Kelebihan Bayar</label>
                                                                    <div class="form_input input-not-focus">
                                                                        <input type="text" id="kelebihan_bayar" name="kelebihan_bayar" class="form-control" value="<?php echo $data->Kelebihan_bayar ?>" readonly>
                                                                    </div>
                                                                </div>
                                                                <!--<div class="form_grid_12 mb-1">
                                                                    <div class="btn_30_blue">
                                                                        <span><a class="btn_24_blue" id="calculate" onclick="calculate()">Hitung</a></span>
                                                                    </div>
                                                                </div>-->
                                                            </div>

                                                            <div class="clear"></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end tab 1 -->

                                        <!-- tab 2 -->
                                        <div id="tab2">
                                            <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                                <div class="grid_12 w-100">
                                                    <span class="h_icon blocks_images"></span>
                                                    <h6>Total Pembayaran</h6>
                                                </div>
                                            </div>
                                            <ul>
                                                <li>
                                                    <div class="form_grid_12 w-100 alpha">
                                                        <div class="form_grid_6 mb-1">
                                                            <label class="field_title mt-dot2 mb">Uang Muka</label>
                                                            <div class="form_input">
                                                                <input type="text" class="form-control" name="uang_muka" id="uang_muka" value="<?php echo $data->Uang_muka ? $data->Uang_muka : 0 ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form_grid_6 mb-1">
                                                            <label class="field_title mt-dot2 mb">Kompensasi UM</label>
                                                            <div class="form_input">
                                                                <input type="text" class="form-control" name="kompensasi_um" id="kompensasi_um" value="<?php echo $data->Kompensasi_um ? $data->Kompensasi_um : 0 ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_grid_12 w-100 alpha">
                                                        <div class="form_grid_6 mb-1">
                                                            <label class="field_title mb mt-dot2">Jumlah Pembayaran</label>
                                                            <div class="form_input input-not-focus">
                                                                <input type="text" id="total_invoice_pembayaran" name="total_invoice" class="form-control" value="<?php echo $data->Total ?>" readonly>
                                                            </div>
                                                        </div>

                                                        <div class="form_grid_6 mb-1">
                                                            <label class="field_title mb mt-dot2">Nama COA</label>
                                                            <div class="form_input input-disabled">
                                                                <?php if($bayar->Jenis_pembayaran=="Cash"){
                                                                    ?>
                                                                    <select name="namacoa" id="namacoa" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem" readonly>
                                                                        <option value="-">-</option>
                                                                    </select>
                                                                    <?php
                                                                } else { ?>
                                                                    <select name="namacoa" id="namacoa" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem">
                                                                        <?php foreach ($bank as $row) : ?>
                                                                            <option value="<?php echo $row->IDCoa ?>" <?php echo $row->IDCoa == $bayar->IDCOA ? 'selected' : '' ?>><?php echo $row->Nama_COA ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                <?php }?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Jenis Pembayaran</label>
                                                        <div class="form_input">
                                                            <div class="form_input">
                                                                <div class="mb-1">
                                                                    <span>
                                                                        <input type="radio" class="radio" name="jenis" id="jenis" value="Cash" <?php if($bayar->Jenis_pembayaran=="Cash"){echo 'checked';} ?> onclick="jenis_pembayaran()">
                                                                        <label class="choice">Cash</label>
                                                                    </span>
                                                                </div>
                                                                <div class="mb-1">
                                                                    <span>
                                                                        <input type="radio" class="radio" name="jenis" id="jenis2" value="Transfer" <?php if($bayar->Jenis_pembayaran=="Transfer"){echo 'checked';} ?> onclick="jenis_pembayaran()">
                                                                        <label class="choice">Transfer</label>
                                                                    </span>
                                                                </div>
                                                                <div class="mb-1">
                                                                    <span>
                                                                        <input type="radio" class="radio" name="jenis" id="jenis3" value="Giro" <?php if($bayar->Jenis_pembayaran=="Giro"){echo 'checked';} ?> onclick="jenis_pembayaran()">
                                                                        <label class="choice">Giro</label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Tgl Giro</label>
                                                        <div class="form_input">
                                                            <input type="date" id="tgl_giro" name="tgl_giro" class="input-date-padding-3-2 form-control" value="<?php echo $bayar->Tanggal_giro ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Nominal</label>
                                                        <div class="form_input">
                                                            <input type="text" id="nominal" name="nominal" class="form-control" value="<?php echo $bayar->Nominal_pembayaran ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mt-dot2 mb">Selisih Pembulatan</label>
                                                        <div class="form_input">
                                                            <input type="text" class="form-control" name="selisih_pembulatan" id="selisih_pembulatan">
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="IDBS" id="IDBS">
                                                    <span class="clear"></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- end tab 2 -->
                                </div>
                                <!-- end tab & pane body -->
                            </div>
                    </div>
                    <!-- end content body tab & pane -->
                    <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2">
                            <div class="btn_30_blue">
                                <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                            </div>
                            <div class="btn_30_blue">
                                <span><a class="btn_24_blue" id="simpan" onclick="updateHutang()" style="cursor: pointer;">Simpan</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        var base_url = window.location.pathname.split('/');
        var PM = new Array();
        var PH = new Array();
        index = 0;

        function hutang() {
            this.Tanggal_Hutang;
            this.Jatuh_Tempo;
            this.No_Faktur;
            this.IDSupplier;
            this.Jenis_Faktur;
            this.Nilai_Hutang;
            this.Pembayaran;
            this.Saldo_Awal;
            this.Saldo_Akhir;
            this.IDHutang;
            this.IDFaktur;
            this.IDBS;
        }

        function check_hutang() {
            if ($('#IDSupplier').val() != '') {
                $.post("<?php echo base_url("PembayaranHutang/hutang_bayar")?>", {'IDSupplier' : $('#IDSupplier').val()})
                    .done(function(response) {
                        var json = JSON.parse(response);
                        //console.log(json);
                        if (!json.error) {
                            $.each(json.data, function (key, value) {
                                if(PM == null)
                                    PM = new Array();

                                var M = new hutang();

                                M.IDBS = value.IDBS;
                                M.Tanggal_Hutang = value.Tanggal_Hutang;
                                M.No_Faktur = value.No_Faktur;
                                M.Jatuh_Tempo = value.Jatuh_Tempo;
                                M.Nilai_Hutang = value.Nilai_Hutang;
                                M.Saldo_Awal = value.Saldo_Awal;
                                M.Saldo_Akhir = value.Saldo_Akhir;
                                M.Pembayaran = value.Pembayaran;
                                M.Jenis_Faktur = value.Jenis_Faktur;
                                M.IDSupplier = value.IDSupplier;
                                M.IDHutang = value.IDHutang;
                                M.IDFaktur = value.IDFaktur;

                                PM[index] = M;
                                index++;

                                $("#tb_preview_penerimaan").html("");
                                for(i=0; i < index; i++){
                                    val = "<tr id='rowarray"+i+"'>"+
                                        "<td class='text-center'>"+(i+1)+"</td>"+
                                        //"<td>"+"<input type='checkbox' name='do_' id='do"+i+"' value='"+i+"'>"+"</td>"+
                                        "<td>"+PM[i].No_Faktur+"</td>"+
                                        "<td>"+PM[i].Tanggal_Hutang+"</td>"+
                                        "<td>"+PM[i].Jatuh_Tempo+"</td>"+
                                        "<td>"+PM[i].Nilai_Hutang+"</td>"+
                                        "<td>"+(PM[i].Saldo_Awal-PM[i].Saldo_Akhir)+"</td>"+
                                        "<td>"+PM[i].Pembayaran+"</td>"+
                                        //"<td>" +
                                        //"<input type=text class='form-control' name=dibayar"+i+" id=dibayar"+i+" readonly>" +
                                        //"</td>"+
                                        "<td>"+PM[i].Saldo_Akhir+"</td>"+
                                        "</tr>";
                                    $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+val);
                                }
                                $('#IDBS').val(value.IDBS);
                            });
                        }
                    });
            }
        }

        var tmp = new Array();
        $(document).on('change', 'input[name="do_"]', function() {
            var val = document.getElementsByName('do_');
            var Total = 0;
            var Pembayaran = 0;
            for (var i = 0; i < val.length; i++) {
                if (val[i].checked) {
                    checked = val[i].value;
                    Total = Total + parseInt(PM[checked].Saldo_Akhir);
                    Pembayaran = Pembayaran + parseInt(PM[checked].Pembayaran);
                    tmp[i] = checked;
                    PH[i] = PM[checked];
                }
            }

            for( var j = 0; j < tmp.length-1; j++){
                if ( tmp[j] === undefined) {
                    tmp.splice(j, 1);
                }
            }
            tmp.filter(function(val){return val});

            for( var k = 0; k < PH.length-1; k++){
                if ( PH[k] === undefined) {
                    PH.splice(k, 1);
                }
            }
            PH.filter(function(val){return val});

            $('#total').val(Total);
            $('#pembayaran_db').val(Pembayaran);
            $('#total_invoice_pembayaran').val(Total);
        });

        function sama_dengan() {
            pembayaran = (parseInt($('#total').val()) - parseInt($('#pembayaran_db').val()));
            $('#kelebihan_bayar').val(pembayaran);
            $('#pembayaran').val($('#total').val());
        }

        function calculate() {
            //console.log(tmp);
            $.each(tmp, function (index, value) {
                $('#saldo_akhir'+value).val(0);
                $('#dibayar'+value).val(PM[value].Saldo_Akhir);
            });
        }

        var jenis;
        jenis_bayar = "<?php echo $bayar->Jenis_pembayaran ?>";
        if (jenis_bayar) {
            jenis = jenis_bayar;
        } else {
            $("#namacoa").prop("disabled", true);
        }

        $("#Jatuh_tempo").prop("disabled", true);
        $("#Discount").prop("disabled", true);

        function jenis_pembayaran(){
            if (document.getElementById('jenis2').checked)
            {
                $("#namacoa").prop("disabled", false);
                jenis = 'Transfer';
            } else if (document.getElementById('jenis3').checked){
                $("#namacoa").prop("disabled", false);
                jenis = 'Giro';
            } else if (document.getElementById('jenis').checked){
                $("#namacoa").prop("disabled", true);
                jenis = 'Cash';
            }
        }

        function hitung() {
            var a = new Date($("#Tanggal").val());
            var b = $("#Jatuh_tempo").val();

            var today = new Date(a.getTime()+(b*24*60*60*1000));
            var dd = today.getDate();
            var mm = today.getMonth()+1;

            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd;
            }
            if(mm<10){
                mm='0'+mm;
            }
            var today = yyyy+'-'+mm+'-'+dd;
            //$("#Tanggal_jatuh_tempo").val(today);
        }

        function fieldHutang(){
            var data1 = {
                "IDBS":$('#IDBS').val(),
                "nomor_PH":$("#PH").val(),
                "IDSupplier":$("#IDSupplier").val(),
                "Tanggal_PH":$("#Tanggal_PH").val(),
                "Keterangan":$("#keterangan").val(),
                //"IDMataUang":,
                //"Kurs":,
                "total":$("#total").val(),
                "Uang_muka":$("#uang_muka").val(),
                "Kompensasi_um":$("#kompensasi_um").val(),
                "Pembayaran":$("#pembayaran").val(),
                "kelebihan_bayar":$("#kelebihan_bayar").val(),
                "jenis" : jenis,
                "nominal" : $('#nominal').val(),
                "tgl_giro" : $('#tgl_giro').val(),
                "namacoa" : $('#namacoa').val()
            }
            return data1;
        }

        function updateHutang(){
            if ($('#nominal').val() != 0 && $('#Tanggal_PH').val() != '') {
                var data1 = fieldHutang();
                console.log(PH);
                console.log(data1);
                $.ajax({
                    url: "../update",
                    type: "POST",
                    data: {
                        "data1" : data1,
                        "data2" : PH
                    },
                    // dataType: 'json',
                    success: function (msg, status) {
                        // window.location.href = "index";
                        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');

                        document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
                        document.getElementById("simpan").setAttribute("onclick", "return false;");
                        document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
                        document.getElementById("printdata").setAttribute("onclick", "return true;");
                        document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/PembayaranHutang/print_/'+data1['nomor_PH']);
                    },
                    error: function(msg, status){
                        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
                    }
                });
            } else {
                $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
            }
        }
    </script>

<?php $this->load->view('administrator/footer') ; ?>