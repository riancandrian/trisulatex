<?php $this->load->view('administrator/header') ; 

?>
<!-- =============================================Style untuk tabs -->
<style type="text/css" scoped>
  .chzn-container {
    width: 103% !important;
  }
</style>

<!--========================Head Content -->
<div class="page_title">
  <div id="alert"></div>
  <?php echo $this->session->flashdata('Pesan');?>
  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Data Packing List</h3>
  <div class="top_search">
  </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>MakloonJahit">Data Packing List</a></li>
              <li class="active">Ubah Packing List</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Ubah Data Packing List</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>
              <li class="body-search-data">

                <!-- form header -->
                <div class="form_grid_12 w-100 alpha">

                  <!-- left form 8 -->
                  <div class="form_grid_4">
                   <!-- form 1 -->
                   <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">No PL</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $packinglist->Nomor ?>" readonly>
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tanggal SO</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $packinglist->Tanggal ?>">
                      </div>
                    </div>


                     <!-- form 4 -->
                     <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">SOK</label>
                      <div class="form_input">
                        
                        <select name="IDSOK" id="IDSOK" style="width:100%;" class="chzn-select" required onchange="cek_customer()">
                          <option value="" selected>--Pilih SOK--</option>
                          <?php foreach($sok as $row) {
                            if ($packinglist->IDSOK == $row->IDSOK) {
                              echo "<option value='$row->IDSOK' selected>$row->Nomor</option>";
                            }
                            echo "<option value='$row->IDSOK'>$row->Nomor</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                     <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Nama Customer</label>
                      <div class="form_input">
                        
                     <!--    <select name="IDCustomer" id="IDCustomer" style="width:100%;" class="chzn-select" required>
                          <option value="" selected>Pilih Customer</option>
                          <?php //foreach($trisula as $row) {
                          //  echo "<option value='$row->IDCustomer'>$row->Nama</option>";
                          //}
                          ?>
                        </select> -->
                        <input type="hidden" name="IDCustomer" id="IDCustomer" readonly>
                         <input type="text" name="NamaCustomer" id="NamaCustomer" readonly>
                      </div>
                    </div>

                    <!-- form 5 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="30" rows="10"><?= $packinglist->Keterangan ?></textarea>
                        <input type="hidden" name="IDPAC" id="IDPAC" value="<?php echo $packinglist->IDPAC ?>">
                      </div>
                    </div>


                  </div>
                  <div class="form_grid_4">
                   <!-- radio 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Qty Grade A</label>
                      <div class="form_input">
                      <input type="text" name="Total_qty_grade_a" id="Total_qty_grade_a" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Total Panjang A</label>
                      <div class="form_input">
                      <input type="text" name="Total_pcs_grade_a" id="Total_pcs_grade_a" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Qty Grade B</label>
                      <div class="form_input">
                      <input type="text" name="Total_qty_grade_b" id="Total_qty_grade_b" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Total Panjang B</label>
                      <div class="form_input">
                      <input type="text" name="Total_pcs_grade_b" id="Total_pcs_grade_b" readonly>
                      </div>
                    </div>

                    <!-- radio 2 -->
                  </div>
                  <div class="form_grid_4">
                    <!-- radio 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Qty Grade S</label>
                      <div class="form_input">
                      <input type="text" name="Total_qty_grade_s" id="Total_qty_grade_s" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Total Panjang S</label>
                      <div class="form_input">
                      <input type="text" name="Total_pcs_grade_s" id="Total_pcs_grade_s" readonly>
                      </div>
                    </div>

                      <div class="form_grid_12 mb-1">
                      <label class="field_title">Qty Grade E</label>
                      <div class="form_input">
                      <input type="text" name="Total_qty_grade_e" id="Total_qty_grade_e" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Total Panjang E</label>
                      <div class="form_input">
                      <input type="text" name="Total_pcs_grade_e" id="Total_pcs_grade_e" readonly>
                      </div>
                    </div>
                  </div>
                  <!-- clear content -->
                  <div class="clear"></div>

                </div>
                <!-- end form header -->

              </li>

              <!-- =============================-Scan Barcode -->
              <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title mt-dot2 mb">Barcode Scan</label>
                  <div class="form_input">
                    <input type="text" name="Barcode" id="Barcode" class="form-control" placeholder="Scan Barcode" autofocus>
                  </div>
                </div>
              </li>

              <!-- =============================-Table Bawah -->
              <div class="widget_content">
                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Barcode</th>
                      <th>Corak</th>
                      <th>Warna</th>
                      <th>Merk</th>
                      <!-- <th>Qty Yard</th> -->
                      <th>Panjang (Yard)</th>
                      <th>Grade</th>
                      <th>Satuan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>
              </div>

              <li>
                <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                    <div class="btn_30_blue">
                     <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                  </div>
                  <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save_change()" type="button" value="Simpan Data" title="Simpan Data"></span>
                  </div>
                </div>
              </li>
            </ul>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/packinglist.js"></script>
<script>
  $(document).ready(function(){
    renderJSONE($.parseJSON('<?php echo json_encode($datapackinglistdetail) ?>'));
        //console.log(<?php echo json_encode($datapackinglistdetail) ?>);
      });
    </script>
<?php $this->load->view('administrator/footer') ; ?>