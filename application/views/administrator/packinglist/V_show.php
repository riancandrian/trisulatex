<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Packing List</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('PackingList/index')?>">Data Packing List</a></li>
                            <li style="text-transform: capitalize;"> Detail Packing List - <b><?php echo $packinglist->Nomor ?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">
                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $packinglist->Tanggal ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Nomor</td>
                                    <td style="background-color: #ffffff;"><?php echo $packinglist->Nomor?></td>
                                </tr>
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Supplier</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $packinglist->Nama ?></td>
                                </tr>
                                <tr>         
                                    <td width="35%" style="background-color: #ffffff;">Keterangan</td>
                                    <td width="65%" style="background-color: #ffffff;"><?php echo $packinglist->Keterangan ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Status</td>
                                    <td style="background-color: #e5eff0;"><?php echo $packinglist->Batal?></td>
                                </tr>     
                                <tr>         
                                    <td width="35%" style="background-color: #ffffff;">Totay QTY Grade A</td>
                                    <td width="65%" style="background-color: #ffffff;"><?php echo $packinglist->Total_qty_grade_a ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Total Panjang A</td>
                                    <td style="background-color: #e5eff0;"><?php echo $packinglist->Total_pcs_grade_a?></td>
                                </tr>  
                                <tr>         
                                    <td width="35%" style="background-color: #ffffff;">Totay QTY Grade B</td>
                                    <td width="65%" style="background-color: #ffffff;"><?php echo $packinglist->Total_qty_grade_b ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Total Panjang b</td>
                                    <td style="background-color: #e5eff0;"><?php echo $packinglist->Total_pcs_grade_b?></td>
                                </tr>  
                                <tr>         
                                    <td width="35%" style="background-color: #ffffff;">Totay QTY Grade E</td>
                                    <td width="65%" style="background-color: #ffffff;"><?php echo $packinglist->Total_qty_grade_e ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Total Panjang E</td>
                                    <td style="background-color: #e5eff0;"><?php echo $packinglist->Total_pcs_grade_e?></td>
                                </tr>  
                                <tr>         
                                    <td width="35%" style="background-color: #ffffff;">Totay QTY Grade S</td>
                                    <td width="65%" style="background-color: #ffffff;"><?php echo $packinglist->Total_qty_grade_s ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Total Panjang S</td>
                                    <td style="background-color: #e5eff0;"><?php echo $packinglist->Total_pcs_grade_s?></td>
                                </tr>  
                                
                               
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                        <div class="widget_content">

                            <table class="display data_tbl">
                              <thead>
                                 <tr>
                                    <th>No</th>
                                    <th>Barcode</th>
                                    <th>Corak</th>
                                    <th>Warna</th>
                                    <th>Merk</th>
                                    <th>Panjang</th>
                                    <th>Grade</th>
                                    <th>Satuan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($packinglistdetail)){
                                  ?>
                                  <?php
                              }else{
                                  $i=1;
                                  foreach ($packinglistdetail as $data2) {
                                    ?>
                                    <tr class="odd gradeA">
                                      <td class="text-center"><?php echo $i; ?></td>
                                      <td><?php echo $data2->Barcode; ?></td>
                                      <td><?php echo $data2->Corak; ?></td>
                                      <td><?php echo $data2->Warna; ?></td>
                                      <td><?php echo $data2->Merk; ?></td>
                                      <td><?php echo $data2->Qty_yard; ?></td>
                                      <td><?php echo $data2->Grade; ?></td>
                                      <td><?php echo $data2->Satuan; ?></td>
                                  </tr>
                                  <?php
                                  $i++;
                              }
                          }
                          ?>

                      </tbody>
                  </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('PackingList/index')?>" name="simpan">Kembali</a></span>
                                </div>
                                 <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>PackingList/print/<?php echo $packinglist->IDPAC ?>">Print Data</a></span>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>