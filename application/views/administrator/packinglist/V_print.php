<!DOCTYPE HTML>
<html>

<head>
    <title>PT Trisula Textile Industries Tbk</title>
    <link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen,print">
    <style type="text/css">
    @page {
        size: F4;
        margin: 0;
    }

</style>
</head>
<body>
    <div id="content">
        <div class="grid_container">
            <div class="grid_12">
                <div class="widget_wrap">
                    
                       <center> <h1>Packing List</h1></center>
                    
                    <div class="widget_content">
                        <div class=" page_content">


                            <div class="grid_12 invoice_details">
                                <div class="invoice_tbl">
                                    <table>
                                      <thead>
                                          <tr class=" gray_sai">
                                            <th>
                                                No
                                            </th>
                                            <th>
                                                Desain
                                            </th>
                                            <th>
                                                Col No
                                            </th>
                                            <th>
                                                Grade
                                            </th>
                                            <th>
                                                Qty Yard
                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php if(empty($print)){
                                      ?>
                                      <?php
                                  }else{
                                   $i=1;
                                   $n=1;
                                   $k="";
                                   $m= "";
                                   foreach ($print as $data2) {

                                    if($n==1){
                                       $total = $data2->Qty_yard;
                                       $n=2;
                                   }else{
                                       $total= $total+$data2->Qty_yard;
             // echo $data->Masuk_Yard ." ".$data->Keluar_Yard;
                                   }
                                   ?>
                                   <tr class="odd gradeA">
                                      <td><?php echo $i; ?></td>
                                      <td><?php if($k == "" && $k != $data2->Corak) {echo $data2->Corak;} ?></td>
                                      <td><?php if($m == "" && $m != $data2->Warna) {echo $data2->Warna;} ?></td>
                                      <td><?php echo $data2->Grade ?></td>
                                      <td><?php echo $data2->Qty_yard ?></td>
                                  </tr>
                                  <
                                  <?php
                                    $k=$data2->Corak;
                                    $m=$data2->Warna;
                                  $i++;
                              }
                              ?>
                              <tr>
                                <td><b>Sub Total </b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td> <?php echo $total ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <span class="clear"></span>
    </div>
</div>
</div>
</div>
</div>
</div>
<span class="clear"></span>
</div>
</body>
</html>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
                <script type="text/javascript">
    $(document).ready(function() {
     window.print();
     console.log('masuk kesini');
     window.location.href = "<?php echo base_url() ?>PackingList/index";
   });
 </script>