<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
  /*  .table_top{
        display: none !important;
        }*/
    </style>
    <!-- tittle data -->
    <div class="page_title">
       <?php echo $this->session->flashdata('Pesan');?>
       <span class="title_icon"><span class="blocks_images"></span></span>
       <h3>Laporan Penerimaan Piutang</h3>
       <div class="top_search">

       </div>
   </div>
   <!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
   <!-- body data -->
   <div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                          <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                          <li class="active">Laporan Penerimaan Piutang</li>
                      </ul>
                  </div>
              </div>

              <div class="widget_content">
                <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>PenerimaanPiutang/pencarian_lap_penerimaan_piutang" class="form_container left_label">
                    <ul>
                        <li class="px-1">

                            <!-- search -->
                            <div class="form_grid_12 w-100 alpha">

                                <!-- button -->
                            

                                <div class="form_grid_9 ">

                                    <!-- form 1 -->
                                    <div class="form_grid_3 ">   
                                        <label class="field_title mt-dot2">Periode</label>
                                        <div class="form_input">
                                            <input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                        </div>
                                    </div>

                                    <!-- form 2 -->
                                    <div class="form_grid_3 ">   
                                        <label class="field_title mt-dot2"> S/D </label>
                                        <div class="form_input">
                                            <input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                        </div>
                                    </div>

                                    <div class="form_grid_3 ">   
                                        <select name="jenispencarian" data-placeholder="Nama Bank" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value="Nama">Nama Customer</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_2">   
                                        <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
                                    </div>
                                    <div class="form_grid_1">
                                        <div class="btn_24_blue">
                                            <input type="submit" name="" value="search">
                                        </div>
                                    </div>
                                </div>

                                <span class="clear"></span>
                            </div>
                            <!-- end search -->
                            
                        </li>

                    </form>
                </div>
                <?php
                    if($this->input->post('date_from') != ""){
                ?>
                <div style="position: relative;z-index: 1; left: 10px; top: 10px;font-size: 11px">
                    <!-- <div class="btn_30_light" style="position: absolute;">
                                    <a href="<?php //echo base_url() ?>Po/exportToExcel"><span class="icon doc_excel_table_co"></span><span class="btn_link">Exsport XLS</span></a>
                                </div> -->
                </div>
             
                    <div class="widget_content">
                        <table class="display" id="action_tbl">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nomor</th>
                                    <th>Jenis Pembayaran</th>
                                    <th>Nama COA</th>
                                    <th>Nilai Hutang</th>
                                    <th>Pembayaran</th>
                                    <th>Saldo Akhir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($laporanpenerimaanpiutang)){ ?>
                                <!-- <tr>
                                    <td colspan="7"> Data Kosong </td>
                                </tr> -->
                                <?php }else{

                                    $i=1;
                                    foreach ($laporanpenerimaanpiutang as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo DateFormat($data->Tanggal); ?></td>
                                            <td><?php echo $data->Nomor; ?></td>
                                            <td><?php echo $data->Nama_COA; ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                       
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>
