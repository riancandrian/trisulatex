<?php $this->load->view('administrator/header') ; 
?>
<style type="text/css" scoped>
  /* Style the tab */
  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
  }
  .tab a {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
    color:black;
  }
  .tab a:hover {
    background-color: #ddd;
  }
  .tab a.active {
    background-color: #ccc;
  }
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }
  .chzn-container {
    width: 103.5% !important;
  }
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Surat Jalan</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>SuratJalanSeragam">Data Surat Jalan Non Kain</a></li>
              <li class="active">Edit Surat Jalan Non Kain</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Edit Data Surat Jalan Non Kain</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_6">
                    <!-- form 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">No SJ</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $datasuratjalan->Nomor ?>" readonly>
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tanggal SJ</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $datasuratjalan->Tanggal ?>">
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Tanggal Kirim</label>
                      <div class="form_input">
                        <input id="Tanggal_kirim" type="date" name="Tanggal_kirim" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $datasuratjalan->Tanggal_kirim ?>">
                      </div>
                    </div>

                   
                  </div>
                  
                  <div class="form_grid_6">

                    <!-- radio 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">No PL</label>
                      <div class="form_input">
                      <select style="width:100%;" class="chzn-select" placeholder="--Pilih Packing List--" name="IDSOS2" id="IDSOS2" required oninvalid="this.setCustomValidity('Packing List Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                         
                          <?php foreach($sos as $row) {
                            if ($datasuratjalan->IDSOS == $row->IDSOS) {
                              echo "<option value='$row->IDSOS' data-idcustomer='$row->IDCustomer' data-namacustomer='$row->Nama' selected>$row->Nomor</option>";
                            }
                           
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Nama Customer</label>
                      <div class="form_input">
                        <input type="text" name="IDCustomer" id="IDCustomer"  data-idcustomer="<?php echo $datasuratjalan->IDCustomer ?>" class="form-control" placeholder="Silahkan Pilih Packing List Dahulu" value="<?php echo $datasuratjalan->Nama ?>"  readonly>
                      </div>
                    </div>

                    <!-- radio 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="30" rows="10"><?php echo $datasuratjalan->Keterangan ?></textarea>
                        <input type="hidden" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly value="<?php echo $datasuratjalan->Total_qty ?>">
                        <input type="hidden" name="Saldo_qty" id="Saldo_qty" class="form-control" placeholder="Grand Total" readonly value="<?php echo $datasuratjalan->Saldo_qty ?>">
                        <input type="hidden" name="IDSJCS" id="IDSJCS" value="<?php echo $datasuratjalan->IDSJCS ?>">
                      </div>
                    </div>

                    <!-- radio 3 -->
                   
                  </div>

                  <!-- clear content -->
                  <span class="clear"></span>
                </div> 
                <!-- <div class="form_input">
                  <div class="btn_24_blue">
                    <button class="btn_24_blue" type="button" id="Edit_sementara">
                      Edit Data
                    </button>
                  </div>
                </div> -->
                <hr>
              
              </li>
              <div class="widget_content">
                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Barang</th>
                      <th>Qty</th>
                      <th>Satuan</th>
                      <th>harga Satuan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>
              </div>

              <li>
                <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                    <div class="btn_30_blue">
                     <!-- <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span> -->
                     <span><a href="<?php echo site_url('SuratJalanSeragam'); ?>"> Batal</a></span>
                  </div>
                  <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save_change()" type="button" value="Simpan Data" title="Simpan Data"></span>
                  </div>
                </div>
              </li>
             
            </ul>

           
          </div>
          <!-- end tab & pane -->

        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/suratjalanseragam.js"></script>
<script>
  $(document).ready(function(){
    renderJSONE($.parseJSON('<?php echo json_encode($datasuratjalandetail) ?>'));
        //console.log(<?php echo json_encode($datasuratjalandetail) ?>);
      });
    </script>
<?php $this->load->view('administrator/footer') ; ?>