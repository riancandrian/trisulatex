<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
  /*  .table_top{
        display: none !important;
        }*/
    </style>
    <!-- tittle data -->
    <div class="page_title">
     <?php echo $this->session->flashdata('Pesan');?>
     <span class="title_icon"><span class="blocks_images"></span></span>
     <h3><?= $title_master ?></h3>
     <div class="top_search">

     </div>
 </div>
 <!-- ======================= -->

 <!-- body data -->
 <div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                          <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                          <li class="active"><?= $title ?></li>
                      </ul>
                  </div>
              </div>

              <div class="widget_content">
                <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Po/pencarian" class="form_container left_label">
                    <ul>
                        <li class="px-1">
                          <div class="form_grid_12">
                            <div class="form_grid_1">
                                <div class="btn_24_blue">
                                  <span>
                                    Periode
                                  </span>
                                </div>
                            </div>
                            <div class="form_grid_2"> 
                                <input type="date" name="tanggal_awal">
                            </div>
                             <div class="form_grid_1">
                                <div class="btn_24_blue">
                                  <span>
                                    S/d
                                  </span>
                                </div>
                            </div>
                            <div class="form_grid_2"> 
                                <input type="date" name="tanggal_akhir">
                            </div>
                            <div class="form_grid_2">
                                <select data-placeholder="No PO" name="nopo" style="width: 100%!important" class="chzn-select" tabindex="13">
                                    <option value=""></option>
                                     <option value="aktif">Aktif</option>
                                                    <option value="tidak aktif">Tidak Aktif</option>
                                </select>
                            </div>
                            <div class="form_grid_3">
                                <input name="keyword" type="text" placeholder="Masukkan Keyword">
                                <!-- <span class=" label_intro">Last Name</span> -->
                            </div>
                            <div class="form_grid_1 flot-right">
                                <div class="btn_24_blue">
                                    <input type="submit" name="" value="search">
                                </div>
                            </div>
                            <span class="clear"></span>

                        </div>
                    </li>
                </form>
            </div>
            <form action="<?php echo base_url() ?>po/delete_multiple" method="post" id="ubahstatus">
                <div class="widget_content">
                    <table class="display data_tbl">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Do</th>
                                <th>Tanggal</th>
                                <th>Nomor PO</th>
                                <th>Tanggal Selesai</th>
                                <th>Corak</th>
                                <th>Merk</th>
                                <th>Total Qty Yard</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(empty($po)){ ?>
                                <!-- <tr>
                                <td colspan="7"> Data Kosong </td>
                                </tr> -->
                            <?php
                            }else{
                            
                                $i=1;
                                foreach ($po as $data) {
                                    ?>
                                    <tr class="odd gradeA">
                                        <td><?php echo $i; ?></td>
                                        <td><input type="checkbox" name="msg[]" value="<?php echo $data->IDPO ?>"></td>
                                        <td><?php echo $data->Tanggal; ?></td>
                                        <td><?php echo $data->Nomor; ?></td>
                                        <td><?php echo $data->Tanggal_selesai; ?></td>
                                        <td><?php echo $data->Kode_Corak; ?></td>
                                        <td><?php echo $data->Kode_Merk; ?></td>
                                        <td><?php echo $data->Total_qty; ?></td>
                                        <td><?php echo $data->Batal; ?></td>
                                    <td class="text-center ukuran-logo">
                                        <span><a class="action-icons c-edit" href="<?= site_url('Po/show/'.$data->IDPO); ?>" title="Ubah Data">Detail</a></span>
                                        
                                        <?php if($data->Batal=="aktif"){ ?>
                                        <span><a class="action-icons c-sa" href="<?php echo base_url() ?>po/set_edit/<?php echo $data->IDPO;?>" title="Status Tidak Aktif">Status</a></span>
                                        <?php }elseif($data->Batal=="tidak aktif"){ ?>
                                        <span><a class="action-icons c-approve" href="<?php echo base_url() ?>po/set_edit/<?php echo $data->IDPO;?>" title="Status Aktif">Status</a></span>
                                        <?php } ?>

                                        <span><a class="action-icons c-edit" href="<?php echo site_url('Po/edit/').$data->IDPO ?>" title="Edit Data">Edit</a></span>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            }
                        ?>
                        </tbody>
                    </table>
                    <div class="btn_24_blue">
                        <a href="<?php echo base_url('po/create')?>"><span>Tambah Data</span></a>
                    </div>
                    <div class="btn_24_blue">
                        <a href="#" type="button" onclick="document.getElementById('ubahstatus').submit();"><span>Batal</span></a>
                    </div>
                    <div class="btn_24_blue">
                        <a href="<?php echo base_url('dashboard')?>"><span>Kembali</span></a>
                    </div>
                </div>
            </form>
     </div>
 </div>
</div>
</div>

<?php 
if ($po=="" || $po==null) {
}else{
    foreach ($po as $data) {
        ?>
        <div id="open-modal<?php echo $data->IDMerk ?>" class="modal-window">
   <div>
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Konfirmasi Hapus</h1>
                <div class="modal-body">Apakah anda yakin akan menghapus data ini ?</div>
                <div class="modal-foot">
                    <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div>
                    <div class="btn_30_blue">
                        <a href="<?= site_url('Merk/delete/'.$data->IDMerk); ?>"><span>Hapus</span></a>
                    </div>
                </div>
</div>
</div>
     <?php 
 }
} 
?> 
<?php $this->load->view('administrator/footer') ; ?>
