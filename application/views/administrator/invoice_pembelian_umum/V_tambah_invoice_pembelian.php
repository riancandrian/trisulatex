<?php $this->load->view('administrator/header') ; 

if ($kodenoinv->curr_number == null) {
  $number = 1;
} else {
  $number = $kodenoinv->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_no_inv = 'INVBU'.date('y').str_replace(' ', '',$agen).$kodemax;
?>
<style type="text/css">
.chzn-container {
  width: 102.5% !important;
}
.chzn-container-single .chzn-search input{
  width: 88% !important;
}
</style>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Invoice Pembelian Umum</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Invoice_pembelian_umum">Data Invoice Pembelian Umum</a></li>
              <li class="active">Tambah Invoice Pembelian Umum</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Invoice Pembelian Umum</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">

                <!-- form header -->
                <div class="form_grid_12 w-100 alpha">

                  <!-- form 1 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Tgl Invoice</label>
                    <div class="form_input">
                      <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?php echo date('Y-m-d'); ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    </div>
                  </div>

                  <!-- form 2 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Jatuh Tempo</label>
                    <div class="form_input">
                      <div class="form_grid_6"> 
                        <input id="Jatuh_tempo" type="text" onkeyup="hitung();" name="Jatuh_tempo" class="form-control tempo" placeholder="Jatuh tempo" value="75">
                      </div>
                      <div class="form_grid_6 text-right mt-dot2"> 
                        <input type="checkbox" class="checkbox" id="customize" onchange="customize_tempo()">
                        <label class="choice pr">Customize</label>
                      </div>
                    </div>
                  </div>

                  <!-- form 3 -->
                  <div class="form_grid_4 mb-1">
                    <!-- <label class="field_title mb mt-dot2">Discount</label> -->
                    <div class="form_input">
                      <div class="form_grid_6"> 
                        <input id="Discount" type="hidden" name="Discount" value="0" class="form-control" placeholder="Discount" onkeyup="ubah_discount()">
                      </div>
                      <!-- <div class="form_grid_6 text-right mt-dot2"> 
                        <input type="checkbox" class="checkbox" id="disc" onchange="discount_inv()">
                        <label class="choice pr">Discount</label>
                      </div> -->
                    </div>
                  </div>

                  <!-- form 4 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">No Invoice</label>
                    <div class="form_input">
                      <input type="text" name="Nomor_invoice" id="Nomor_invoice" class="form-control" placeholder="No Asset" value="<?php echo $code_no_inv; ?>">
                    </div>
                  </div>

                  <!-- form 5 -->
                  <?php
                  $tgl1 = date('Y-m-d');
                  $tgl2 = date('Y-m-d', strtotime('+75 days', strtotime($tgl1)));
                  ?>
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb pt">Tgl Jatuh Tempo</label>
                    <div class="form_input">
                      <input id="Tanggal_jatuh_tempo" type="date" value="<?php echo $tgl2; ?>" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" placeholder="Tanggal Jatuh tempo">
                    </div>
                  </div>

                  <!-- form 6 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Total QTY</label>
                    <div class="form_input input-not-focus">
                      <input id="total_qty" type="text" name="total_qty" class="form-control" placeholder="Total Qty" readonly>
                    </div>
                  </div>
                  
                  <input id="id_tbs_umum" type="hidden" name="id_tbs_umum" class="form-control" placeholder="Total Qty" readonly>


                  <!-- form 7 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb pt"><br>
                      No PB Umum
                    </label>
                    <div class="form_input">
                           <select name="Nomor" id="Nomor" class="chzn-select" onchange ="cek_supplier()" style="width: 59% !important" class="form-control">
                        <option value="">Pilih No PB</option>
                        <?php
                        foreach($pb as $pbs)
                        { 
                          ?>
                          <option value="<?php echo $pbs->Nomor ?>"><?php echo $pbs->Nomor ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>


                  <!-- form 10 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Supplier</label>
                    <div class="form_input">
                     <input id="NamaSupplier" type="text" name="NamaSupplier" class="form-control" placeholder="Supplier" readonly>
                     <input id="IDSupplier" type="hidden" name="IDSupplier" class="form-control" placeholder="Supplier" readonly>
                    </div>
                  </div>

                  <!-- form 11 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Status PPN</label>
                    <div class="form_input">
                      <select style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" class="form-control" name="Status_ppn" id="Status_ppn" onchange="ubah_status_ppn()">
                        <option value="Include">Include</option>
                        <option value="Exclude">Exclude</option>
                        <option value="Nonppn">Non PPN</option>
                      </select>
                    </div>
                  </div>

                  <span class="clear"></span>
                </div>
              </li>
              <li>
                <div class="form_grid_12">
                  <div class="form_input ml text-center w-100">

                    <!-- <button class="btn_24_blue" type="button" id="add-to-cart">
                      Tambah Data
                    </button> -->


                  </div>
                </div>
              </li>
            </ul>

            <!-- content body tab & pane -->
            <div class="grid_12 mx w-100">
              <div class="widget_wrap tabby mb-4">
                <div class="widget_top">

                  <!-- button tab & pane -->
                  <div id="widget_tab">
                    <ul>
                      <li class="px py"><a href="#tab1" class="active_tab">List Barang</a></li>
                     <!--  <li class="px py"><a href="#tab2">Pembayaran</a></li> -->
                    </ul>
                  </div>
                  <!-- end button tab & pane -->

                </div>

                <!-- tab & pane body -->
                <div class="widget_content">

                  <!-- tab 1 -->
                  <div id="tab1">
                    <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                      <div class="grid_12 w-100">
                        <span class="h_icon blocks_images"></span>
                        <h6>List Barang</h6>
                      </div>
                    </div>
                    <div>
                      <table class="display data_tbl" id="tabelfsdfsf">
                        <thead>
                          <tr>
                            <th class="center" style="width: 40px">No</th>
                            <th>Barang</th>
                            <th class="hidden-xs">Qty</th>
                            <th>Satuan</th>
                            <th>Harga</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody id="tb_preview_penerimaan">

                        </tbody>
                      </table>
                      <ul>
                       <li>
                        <div class="form_grid_12 w-100 alpha">
                          <div class="form_grid_8">
                            <label class="field_title mb mt-dot2">Keterangan</label>
                            <div class="form_input">
                              <textarea id="Keterangan" name="Keterangan" class="form-control" placeholder="Keterangan" rows="9" style="resize: none;"></textarea>
                            </div>
                          </div>
                          <div class="form_grid_4">
                            <div class="form_grid_12 mb-1">
                              
                                <input type="hidden" id="Total" name="Total" class="form-control" placeholder="Total" readonly>
                             

                              <label class="field_title mb mt-dot2">DPP</label>
                              <div class="form_input input-not-focus">
                                <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                              </div>
                            </div>

                            <div class="form_grid_12 mb-1">
                              <!-- <label class="field_title mb mt-dot2">Discount</label> -->
                              <div class="form_input input-not-focus">
                                <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" readonly>
                              </div>
                            </div>

                            <div class="form_grid_12 mb-1">
                              <label class="field_title mb mt-dot2">PPN</label>
                              <div class="form_input input-not-focus">
                                <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" readonly>
                              </div>
                            </div>

                            <div class="form_grid_12 mb-1">
                              <label class="field_title mb">Total Invoice</label>
                              <div class="form_input input-not-focus">
                                <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                              </div>
                            </div>

                          </div>

                          <!-- clear content -->
                          <div class="clear"></div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        
          </form>
          <div class="widget_content px-2 text-center">
            <div class="py-4 mx-2">
             <div class="btn_30_blue">
              <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
            </div>
            <div class="btn_30_blue">
              <span><a class="btn_24_blue" id="simpan" onclick="saveInvoice()" style="cursor: pointer;">Simpan</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
 var base_url = window.location.pathname.split('/');
 $("#Jatuh_tempo").prop("disabled", true);
 $("#Discount").prop("disabled", true);
 $("#namacoa").prop("disabled", true);

 function customize_tempo(){
  if (document.getElementById('customize').checked)
  {
    $("#Jatuh_tempo").prop("disabled", false);
  } else {
    $("#Jatuh_tempo").prop("disabled", true);
  }
}

function discount_inv(){
  if (document.getElementById('disc').checked)
  {
    $("#Discount").prop("disabled", false);
  } else {
    $("#Discount").prop("disabled", true);
  }
}
function jenis_pembayaran(){
  if (document.getElementById('jenis2').checked)
  {
    $("#namacoa").prop("disabled", false);
  } else if (document.getElementById('jenis3').checked){
   $("#namacoa").prop("disabled", false);
 } else if (document.getElementById('jenis').checked){
   $("#namacoa").prop("disabled", true);
 }
}
function hitung() {
  var a = new Date($("#Tanggal").val());
  var b = $("#Jatuh_tempo").val();

  var today = new Date(a.getTime()+(b*24*60*60*1000));
  var dd = today.getDate();
  var mm = today.getMonth()+1;

  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd;
  } 
  if(mm<10){
    mm='0'+mm;
  } 
  var today = yyyy+'-'+mm+'-'+dd;
  $("#Tanggal_jatuh_tempo").val(today);
}
</script>
<script type="text/javascript">
  function Penerimaan(){
   this.IDBarang;
   this.Nama_Barang;
   this.Qty;
   this.IDSatuan;
   this.Satuan;
   this.Harga_satuan;
   this.Sub_total;
   this.IDTBSUmum;
 }

 var PM = new Array();
 index = 0;
 totaldpp=0;
 $(document).ready(function(){
 var PB = document.getElementById("Nomor");
 // $('#add-to-cart').on('click', function(){
   $("#Nomor").change(function(){
   event.preventDefault();

   Nomor = $('#Nomor').val();
            //alert(Nomor);

            $.post("<?php echo base_url("Invoice_pembelian_umum/check_penerimaan_barang")?>", {'Nomor' : Nomor})
            .done(function(response) {

             var json = JSON.parse(response);
                     //console.log(json);
                     if (!json.error) {
                       $.each(json.data, function (key, value) {

                         $('#id_tbs_umum').val(value.IDTBSUmum);
                             // $('#no_po').val(value.NoPO);

                             // if(PM == null)
                             //   PM = new Array();

                             var M = new Penerimaan();
                             var totalqty=0;
                             M.IDBarang = value.IDBarang;
                             M.Nama_Barang = value.Nama_Barang;
                             M.Qty = value.Qty;
                             M.IDSatuan = value.IDSatuan;
                             M.Satuan = value.Satuan;
                             M.Harga_satuan = value.Harga_satuan;
                             M.Sub_total = value.Sub_total;
                             M.IDTBSUmum = value.IDTBSUmum;

                             PM[index] = M;
                             index++;

                             // $("#tb_preview_penerimaan").html("");
                             var table = $('#tabelfsdfsf').DataTable();
                             table.clear().draw();
                             for(i=0; i < index; i++){
                               table.row.add([
                                i+1,
                                PM[i].Nama_Barang,
                                PM[i].Qty,
                                PM[i].Satuan,
                                "<input type=text value="+rupiah(PM[i].Harga_satuan)+" name=harga_satuan id=harga_satuan"+i+" onkeyup=hitung_total("+i+")>",
                                "<input type=text name=harga_total_b id=harga_total_b"+i+" value="+rupiah(PM[i].Sub_total)+" readonly> <input value="+PM[i].Sub_total+" type=hidden name=harga_total id=harga_total"+i+" readonly>",
                                ]).draw();
                               totalqty += parseInt(PM[i].Qty);
                               totaldpp=0;
                               hargasatuan_ =$("#harga_satuan"+i).val();
                               var a = PM[i].Qty;
                               var b = hargasatuan_.replace(/\./g,'');
                               var c=a*b;

                               PM[i].Harga_satuan = parseInt(b);
                               PM[i].Subtotal = c;

                               console.log(c);
                               $("#sub_total"+i).val(c);

                               $("#harga_total"+i).val(c);


                               for(l=0; l < index; l++){

                                 totaldpp += parseInt($("#harga_total"+l).val());

                               }

                               disc = 0;
                               if($("#Status_ppn").val()=="Include"){
                                ppn= totaldpp*0.1;
                                $("#DPP").val(rupiah(totaldpp-ppn));
                                $("#PPN").val(rupiah(ppn));
                                $("#Discount_total").val(disc);
                                total= totaldpp-ppn;
                                $("#total_invoice").val(rupiah(total-disc+ppn));
                                $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                              }else{
                                ppn= totaldpp*0.1;
                                $("#DPP").val(rupiah(totaldpp));
                                $("#PPN").val(rupiah(ppn));
                                $("#Discount_total").val(disc);
                                $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                                $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                              }
                            }
                            $("#Total").val(totaldpp);
                            $("#total_qty").val(totalqty);

                          });
                     } else if($("#Nomor").val()==''){
  
                      }else {
                       alert('Tidak ada data dengan nomor Penerimaan ' + Nomor);
                     }

                   });
                });
          });
function ubah_status_ppn()
{
  disc = $("#Discount").val();
 if($("#Status_ppn").val()=="Include"){
  ppn= $("#Total").val()*0.1;
  $("#DPP").val(rupiah($("#Total").val()-ppn));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= $("#Total").val()-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}else if($("#Status_ppn").val()=="Nonppn"){
  ppn= 0;
  $("#DPP").val(rupiah($("#Total").val()));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= $("#Total").val()-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}else{
  ppn= $("#Total").val()*0.1;
  $("#DPP").val(rupiah($("#Total").val()));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}
}
function ubah_discount()
{
  disc = $("#Discount").val();
 if($("#Status_ppn").val()=="Include"){
  ppn= $("#Total").val()*0.1;
  $("#DPP").val(rupiah($("#Total").val()-ppn));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= $("#Total").val()-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}else if($("#Status_ppn").val()=="Nonppn"){
  ppn= 0;
  $("#DPP").val(rupiah($("#Total").val()));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= $("#Total").val()-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}else{
  ppn= $("#Total").val()*0.1;
  $("#DPP").val(rupiah($("#Total").val()));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}
}
function hitung_total(i) {
  var tanpa_rupiah_debit_5 = document.getElementById('harga_satuan'+i);
    tanpa_rupiah_debit_5.addEventListener('keyup', function(e)
    {
        tanpa_rupiah_debit_5.value = formatRupiah2(this.value);
    });
    
    tanpa_rupiah_debit_5.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });

  var harga_satuan1= $("#harga_satuan"+i).val();
  var a = PM[i].Qty;
  var b = harga_satuan1.replace(".","");
  var c=a*b;
   $("#harga_total_b"+i).val(rupiah(c));
  $("#harga_total"+i).val(c);
  totaldpp=0;
  for(l=0; l < index; l++){

   totaldpp += parseInt($("#harga_total"+l).val());

 }
 disc=  $("#Discount").val();
 if($("#Status_ppn").val()=="Include"){
  ppn= totaldpp*0.1;
  $("#DPP").val(rupiah(totaldpp-ppn));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= totaldpp-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah(total-disc+ppn));
}else{
  ppn= totaldpp*0.1;
  $("#DPP").val(rupiah(totaldpp));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
}

}
function fieldInvoice(){
  var data1 = {
    "id_invoice" : $("#id_invoice").val(),
    "IDTBSUmum" : $("#id_tbs_umum").val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor_invoice":$("#Nomor_invoice").val(),
    "Nomor":$("#Nomor").val(),
    "IDSupplier":$("#IDSupplier").val(),
    "Jatuh_tempo":$("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
    "No_supplier":$("#No_supplier").val(),
    "Status_ppn":$("#Status_ppn").val(),
    "Discount":$("#Discount").val(),
    "Keterangan":$("#Keterangan").val(),
    "total_qty":$("#total_qty").val(),
    "Jatuh_tempo":$("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
  }
  return data1;
}
function fieldgrandtotal(){
  var data3 = {
    "id_invoice_grand_total" : $("#id_invoice_grand_total").val(),
    "id_pembayaran" : $("#id_pembayaran").val(),
    "jenis" :$("#jenis").val(),
    "jenis" :$("#jenis2").val(),
    "jenis" :$("#jenis3").val(),
    "DPP" :$("#DPP").val(),
    "Discount" :$("#Discount").val(),
    "PPN" :$("#PPN").val(),
    "total_invoice_pembayaran" :$("#total_invoice").val(),
    "nominal" :$("#nominal").val(),
    "tgl_giro" :$("#tgl_giro").val(),
    "namacoa" : $("#namacoa").val()
  }
  return data3;
}

function harga_satuan_total(i)
{
  // var data6 = new Array();
  for(i=0; i < index; i++){
    PM[i].harga_satuan = $("#harga_satuan"+i).val();
    PM[i].harga_total = $("#harga_total"+i).val();
    // data6[i]=new Array(i);
//     data6 = {
//       "harga_satuan" : $("#harga_satuan"+i).val(),
//     "harga_total" : $("#harga_total"+i).val()
// }

// data6.push([i],$("#harga_satuan"+i).val());
// data6.push([i],$("#harga_total"+i).val());
// data6[i][0] = 
// data6[i][1] = $("#harga_total"+i).val();
}
  // return data6;
}

function saveInvoice(){
 var data1 = fieldInvoice();
 var data3 = fieldgrandtotal();
 var data6 = harga_satuan_total();
 // var arr=new Array();
 // for(i=0 ; i< PM.length ; i++)
 // {
 //  arr[i]=new Array(i);
 //  arr[i]["Barcode"] = PM[i].Barcode;
 //  arr[i]["harga_satuan"] = data6[i][0];
 // }
 console.log(PM);
 // console.log(PM);
 $("#Nomor").css('border', '');
 $("#NamaSupplier").css('border', '');

 if($("#Nomor").val()==""){
  $('#alert').html('<div class="pesan sukses">Data PB Tidak Boleh Kosong</div>');
  $("#Nomor").css('border', '1px #C33 solid').focus();
}else if($("#NamaSupplier").val()==""){
  $('#alert').html('<div class="pesan sukses">Data Supplier Tidak Boleh Kosong</div>');
  $("#NamaSupplier").css('border', '1px #C33 solid').focus();
}else{
 $.ajax({

  url: "simpan_invoice_pembelian",

  type: "POST",

  data: {

   "data1" : data1,
   "data2" : PM,
   "data3" : data3
   

 },

 dataType: 'json',

 success: function (data) {
   $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');

   document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
   document.getElementById("simpan").setAttribute("onclick", "return false;");
   document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
   document.getElementById("printdata").setAttribute("onclick", "return true;");
   document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/Invoice_pembelian_umum/print/'+data['IDFBUmum']);

 },
 error: function(msg, status){
  console.log(msg);
  $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

}

});
}
}

function cek_supplier()
{
  var totalinv=0;
  var Nomor = $("#Nomor").val();
  $.ajax({
    url : '/'+base_url[1]+'/Invoice_pembelian_umum/cek_supplier',
    type: "POST",
    data:{Nomor:Nomor},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data kosong------------');
      } else {
       $("#NamaSupplier").val(data[0].Nama);
       $("#IDSupplier").val(data[0].IDSupplier);
     }
   },
   error: function (jqXHR, textStatus, errorThrown)
   {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
  }
});
}

function formatRupiah2(bilangan, prefix)
    {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
        split   = number_string.split(','),
        sisa    = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
        
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>
<?php $this->load->view('administrator/footer') ; ?>