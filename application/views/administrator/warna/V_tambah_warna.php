<?php $this->load->view('administrator/header') ; 

?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Warna</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Warna">Data Warna</a></li>
                            <li class="active">Tambah Warna</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Warna</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Warna/simpan" class="form_container left_label">
                         <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <label class="field_title">Kode Warna</label>
                                    <div class="form_input">

                                       <input type="text" class="form-control" name="Kode_Warna" placeholder="Kode Warna" required oninvalid="this.setCustomValidity('Kode Warna Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                       <span class="clear"></span>
                                   </div>
                                   <label class="field_title">Warna</label>
                                   <div class="form_input">
                                       <input type="text" name="Warna" class="form-control" placeholder="Warna" required oninvalid="this.setCustomValidity('Warna Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                       <span class="clear"></span>
                                   </div>
                                   <label class="field_title">Corak</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Corak" name="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php foreach($corak as $cor)
                                            {
                                                echo "<option value='$cor->IDCorak'>$cor->Kode_Corak $cor->Corak</option>";
                                            } 
                                            ?>
                                        </select>
                                    </div>
                            </div>
                        </li>
                        <li>
                            <div class="form_grid_12">
                                <div class="form_input">
                                    <div class="btn_30_light">
                                     <span> <a href="<?php echo base_url() ?>Warna" name="simpan">Kembali</a></span>
                                 </div>
                                 <div class="btn_30_blue">
                                  <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                              </div>
                          </div>
                      </div>
                  </li>
              </ul>
          </form>
      </div>
  </div>
</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>