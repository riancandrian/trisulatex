<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
	<?php echo $this->session->flashdata('Pesan');?>
	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Data Scan</h3>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<li> Laporan Scan</li>
						</ul>
					</div>
				</div>
				<div class="widget_content">
					<form action="<?php echo base_url() ?>Scan_penerimaan_barang/pencarian" method="post" class="form_container left_label">
						<ul>
							<li class="px-1">

								<!-- search -->
								<div class="form_grid_12 w-100 alpha">

									<div class="form_grid_9 ">

										<div class="form_grid_3 ">   
											<select name="jenispencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
												<option value="Barcode">Barcode</option>
											</select>
										</div>
										<div class="form_grid_2">   
											<input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
										</div>
										<div class="form_grid_1">
											<div class="btn_24_blue">
												<input type="submit" name="" value="search">
											</div>
										</div>
									</div>

									<span class="clear"></span>
								</div>
							</li>
						</ul>
					</form>
				</div>
				<div class="widget_content">
					<table class="display data_tbl">
						<thead>
							<tr>
								<th>No</th>
								<th>Barcode</th>
								<th>Barang</th>
								<th>Corak</th>
								<th>Warna</th>
								<th>Merk</th>
								<th>Panjang Yard</th>
								<th>Panjang Meter</th>
								<th>Grade</th>
								<th>Satuan</th>
							</tr>
						</thead>
						<tbody>
							<?php if(!empty($Scan)){
								$i=1;
								foreach ($Scan as $data) {
									?>
									<tr class="odd gradeA">
										<td><?php echo $i; ?></td>
										<td><?php echo $data->barcode; ?></td>
										<td><?php echo $data->nama_barang; ?></td>
										<td><?php echo $data->corak; ?></td>
										<td><?php echo $data->warna; ?></td>
										<td><?php echo $data->merk; ?></td>
										<td><?php echo $data->panjang_yard; ?></td>
										<td><?php echo $data->panjang_meter; ?></td>
										<td><?php echo $data->grade; ?></td>
										<td><?php echo $data->satuan; ?></td>
									</tr>
									<?php
									$i++;
								}
							}
							?>
						</tbody>
						<tfoot>

						</tfoot>
					</table>
				</div>
				<div class="widget_content text-right px-2">
					<div class="py-4 mx-2">
						<div class="btn_24_blue">
							<input type="button" name="" value="Kembali">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>