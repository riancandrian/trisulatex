<?php $this->load->view('administrator/header') ; ?>

    <!-- tittle data -->
    <div class="page_title">
        <!-- pesan notif -->
        <?php echo $this->session->flashdata('Pesan');?>
        <!-- =========== -->

        <span class="title_icon"><span class="blocks_images"></span></span>
        <h3>Detail Penerimaan Piutang</h3>
    </div>
    <!-- ======================= -->

    <!-- body data -->
    <div id="content">
        <div class="grid_container">
            <div class="grid_12">
                <div class="widget_wrap">

                    <!-- breadcrumb -->
                    <div class="breadCrumbHolder module">
                        <div id="breadCrumb0" class="breadCrumb module white_lin">
                            <ul>
                                <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                                <li><a href="<?php echo base_url('PenerimaanPiutang/index')?>">Data Penerimaan Piutang</a></li>
                                <li style="text-transform: capitalize;"> Detail Penerimaan Piutang - <b><?php echo $data->Nomor ?></b> </li>
                            </ul>
                        </div>
                    </div>

                    <div class="widget_content">
                        <div class="form_container left_label">
                            <table class="display data_tbl">
                                <thead style="display: none;">
                                </thead>
                                <tbody>
                                <tr>
                                    <td width="35%" style="background-color: #ffffff;">Tanggal</td>
                                    <td width="65%" style="background-color: #ffffff;"><?php echo $data->Tanggal ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">Nomor</td>
                                    <td style="background-color: #e5eff0;"><?php echo $data->Nomor?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #ffffff;">Customer</td>
                                    <td width="65%" style="background-color: #ffffff;"><?php echo $data->Nama_customer ?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">Keterangan</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Keterangan ?></td>
                                </tr>
                                </tbody>
                            </table>
                            <br><br>
                            <div class="widget_content">

                                <!--<table class="display data_tbl">
                                    <thead>
                                    <tr>
                                        <th class="center" style="width: 40px">No</th>
                                        <th>Barcode</th>
                                        <th>Nama Barang</th>
                                        <th class="hidden-xs">Corak</th>
                                        <th>Warna</th>
                                        <th>Merek</th>
                                        <th>Qty Yard</th>
                                        <th>Qty Meter</th>
                                        <th>Satuan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($invdetail)){
                                        $i=1;
                                        foreach ($invdetail as $data2) {
                                            ?>
                                            <tr class="odd gradeA">
                                                <td class="text-center"><?php echo $i; ?></td>
                                                <td><?php echo $data2->Barcode; ?></td>
                                                <td><?php echo $data2->Nama_Barang; ?></td>
                                                <td><?php echo $data2->Corak; ?></td>
                                                <td><?php echo $data2->Warna; ?></td>
                                                <td><?php echo $data2->Merk; ?></td>
                                                <td><?php echo $data2->Qty_yard; ?></td>
                                                <td><?php echo $data2->Qty_meter; ?></td>
                                                <td><?php echo $data2->Satuan; ?></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>-->
                            </div>
                            <div class="widget_content py-4 text-center">
                                <div class="form_grid_12">
                                    <div class="btn_30_light">
                                        <span> <a href="<?php echo base_url('PenerimaanPiutang/index')?>" name="simpan">Kembali</a></span>
                                    </div>
                                    <div class="btn_30_blue">
                                        <span><a href="<?php echo base_url() ?>PenerimaanPiutang/print_/<?php echo $data->Nomor ?>">Print Data</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('administrator/footer') ; ?>