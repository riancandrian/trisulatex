<?php $this->load->view('administrator/header') ; ?>
<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Corak</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>corak">Data Corak</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Edit Corak </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Data Corak</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Corak/update" enctype="multipart/form-data" class="form_container left_label">
                        <input type="hidden" name="id" value="<?php echo $corak->IDCorak ?>">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">

                                    <!-- Kode Corak -->
                                    <label class="field_title">Kode Corak</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" name="kode_corak" value="<?php echo $corak->Kode_Corak ?>" required oninvalid="this.setCustomValidity('Kode Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Corak -->
                                    <label class="field_title">Corak</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" name="corak" value="<?php echo $corak->Corak ?>" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Merk -->
                                    <label class="field_title">Merk</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Warna" name="id_merk" required oninvalid="this.setCustomValidity('Merk Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php foreach ($merk as $row) : ?>
                                                <option value="<?php echo $row->IDMerk ?>" <?php echo $row->IDMerk == $corak->IDMerk ? 'selected' : '' ?>><?php echo $row->Kode_Merk . '-' . $row->Merk ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                            <span> <a href="<?php echo base_url() ?>corak" name="simpan">Kembali</a></span>
                                        </div>
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simpan" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ==================== -->

<?php $this->load->view('administrator/footer') ; ?>