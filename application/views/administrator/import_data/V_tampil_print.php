<?php $this->load->view('administrator/header') ; ?>
<div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
        <div class="content-header">
            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1>Data Barcode</h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Tambah Data</li>
                </ol>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Tambah Data Barcode</h4>
                        </div>
                    </div>
                    <form method="post" action="<?php echo base_url();?>ImportData/printData/<?php echo $tampilprinttambah->IDImport;?>">
                        <div class="panel-body">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Barcode Scan</label>
                            <div class="col-sm-9">
                               <input type="text" class="form-control" placeholder="Barcode Scan" name="barcode" required oninvalid="this.setCustomValidity('Barcode Tidak Boleh Kosong')" oninput="setCustomValidity('')" autofocus value="<?php echo $tampilprinttambah->Barcode; ?>"> 
                           </div>
                       </div>
                       <div class="form-group row">
                        <label for="example-search-input" class="col-sm-2 col-form-label">NOSO</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" placeholder="NoSO" name="noso" required oninvalid="this.setCustomValidity('NoSO Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->NoSO; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-search-input" class="col-sm-2 col-form-label">Party</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" placeholder="Indent" name="party" required oninvalid="this.setCustomValidity('Indent Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Party; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-search-input" class="col-sm-2 col-form-label">Indent</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" placeholder="Indent" name="indent" required oninvalid="this.setCustomValidity('Indent Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Indent; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-search-input" class="col-sm-2 col-form-label">CustDes</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" placeholder="CustDes" name="custdes" required oninvalid="this.setCustomValidity('CustDes Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->CustDes; ?>">
                      </div>
                  </div>
                  <div class="form-group row">
                    <label for="example-search-input" class="col-sm-2 col-form-label">Warna Cust</label>
                    <div class="col-sm-9">
                     <input type="text" class="form-control" placeholder="Warna Cust" name="warnacust" required oninvalid="this.setCustomValidity('Warna Cust Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->WarnaCust; ?>">
                 </div>
             </div>
             <div class="form-group row">
                <label for="example-search-input" class="col-sm-2 col-form-label">Panjang</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control" placeholder="Panjang" name="panjang" required oninvalid="this.setCustomValidity('Panjang Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Panjang_Yard; ?>">
               </div>
           </div>
           <div class="form-group row">
                <label for="example-search-input" class="col-sm-2 col-form-label">Panjang M</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control" placeholder="Panjang M" name="panjangm" required oninvalid="this.setCustomValidity('Panjang M Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Panjang_Meter; ?>">
               </div>
           </div>
           <div class="form-group row">
                <label for="example-search-input" class="col-sm-2 col-form-label">Grade</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control" placeholder="Grade" name="grade" required oninvalid="this.setCustomValidity('Grade Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Grade; ?>">
               </div>
           </div>
           <div class="form-group row">
                <label for="example-search-input" class="col-sm-2 col-form-label">Remark</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control" placeholder="Remark" name="remark" required oninvalid="this.setCustomValidity('Remark Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Remark; ?>">
               </div>
           </div>
           <div class="form-group row">
                <label for="example-search-input" class="col-sm-2 col-form-label">Lebar</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control" placeholder="Lebar" name="lebar" required oninvalid="this.setCustomValidity('Lebar Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Lebar; ?>">
               </div>
           </div>
           <div class="form-group row">
                <label for="example-search-input" class="col-sm-2 col-form-label">Satuan</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control" placeholder="Satuan" name="satuan" required oninvalid="this.setCustomValidity('Satuan Wajib Diisi')" oninput="setCustomValidity('')" value="<?php echo $tampilprinttambah->Satuan; ?>">
               </div>
           </div>
    <center>
        <input type="submit" value="Print" class="btn btn-primary">
        <input type="submit" disabled class="btn btn-primary" value="Simpan">
    </center>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

<?php $this->load->view('administrator/footer') ; ?>