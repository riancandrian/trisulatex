 <?php $this->load->view('administrator/header') ; ?>
 <div class="content-wrapper">
    <div class="content">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="header-icon">
                <i class="pe-7s-box1"></i>
            </div>
            <div class="header-title">
                <h1>Data Sementara</h1>
                <ol class="breadcrumb">
                    <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Data Sementara</li>
                </ol>
            </div>
        </div> <!-- /. Content Header (Page header) -->

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>List Data Sementara</h4>
                        </div>
    
                    </div>
                    <div class="panel-body">
                         <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>ImportData/pencarian">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                
                             </div>
                             <div class="col-md-10">
                                <?php if(empty($importdata)){
                                    ?>
                                    <div class="col-md-10 col-xs-12" style="float: right;">
                                        <?php }else{ ?>
                                        <div class="col-md-8 col-xs-12 search-data">
                                            <?php } ?>
                                            <div class="col-md-4">
                                                <select name="jenispencarian" class="form-control">
                                                    <option value="">Cari Berdasarkan</option>
                                                    <option value="Barcode">Barcode</option>
                                                    <option value="NoSO">NoSO</option>
                                                    <option value="Party">Party</option>
                                                    <option value="Indent">Indent</option>
                                                    <option value="CustDes">CustDes</option>
                                                    <option value="WarnaCust">WarnaCust</option>
                                                    <option value="Panjang_Yard">Panjang Yard</option>
                                                    <option value="Panjang_Meter">Panjang Meter</option>
                                                    <option value="Grade">Grade</option>
                                                    <option value="Remark">Remark</option>
                                                    <option value="Lebar">Lebar</option>
                                                    <option value="Satuan">Satuan</option>
                                                </select>   
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback has-feedback-left">
                                                    <input class="form-control" type="text" name="keyword">
                                                    <div class="form-control-feedback">
                                                        <i class="icon-search4 text-size-base"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <center>

                                                    <button class="btn btn-primary pull-right" type="submit">Search</button>
                                                </center>
                                            </div>
                                            <?php if(empty($importdata)){
                                                ?>
                                            </div>
                                            <?php }else{ ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </form>

                       <form action="<?php echo base_url(); ?>ImportData/proses_import_data" method="post" name="upload_excel" enctype="multipart/form-data">
<input type="file" name="file" id="file">
<button type="submit" id="submit" name="import">Import</button>
</form><br><br><br>
                        <div class="table-responsive">
                            <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
                                <thead>
                                 <tr>
                                    <th>No</th>
                                    <th>Buyer</th>
                                    <th>Barcode</th>
                                    <th>NoSO</th>
                                    <th>Party</th>
                                    <th>Indent</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php if(empty($importdata)){
                                    ?>
                                    <?php
                                }else{
                                    $i=1;
                                    foreach ($importdata as $data) {
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $data->Buyer; ?></td>
                                            <td><?php echo$data->Barcode; ?></td>
                                            <td><?php echo $data->NoSO; ?></td>
                                            <td><?php echo $data->Party; ?></td>
                                            <td><?php echo $data->Indent; ?></td>
                                            <td class="text-center">
                                                <a href="<?php echo base_url();?>ImportData/printData/<?php echo $data->IDImport;?>" title="Edit"><i class="hvr-buzz-out fa fa-print"></i></a>
                                            </td>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /.main content -->
</div>
<?php $this->load->view('administrator/footer') ; ?>