
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style>
  body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    background-color: #FAFAFA;
    font: 10pt "Tahoma";
  }   
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
    html, body {
      width: 60mm;
      min-height: 150mm;        
    }
  }
</style>
</head>
<body>
  <!-- <body> -->
    <table>
      <tbody>
        <tr>
          <td colspan="3" align="center"> <?php
   echo "<img alt='barcode' src='" . base_url('asset/plugins/barcode.php?codetype=Code39&size=80&print=true&text='.$tampilprint->Barcode) . "'/>";
   ?></b></td>
        </tr>
        <tr>
          <td colspan="3" align="center">Cust : <?php echo $tampilprint->Buyer ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center">No SO : <?php echo $tampilprint->NoSO ?></td>
        </tr>
         <tr>
          <td colspan="3" align="center">Batch : <?php echo $tampilprint->Party ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center">Art. : <?php echo $tampilprint->CustDes ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center">Color : <?php echo $tampilprint->WarnaCust ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center">Indent : <?php echo $tampilprint->Indent ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center">Length : <?php echo $tampilprint->Panjang_Yard ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center"><?php echo $tampilprint->Panjang_Meter ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center">Grade : <?php echo $tampilprint->Grade ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center">Width : <?php echo $tampilprint->Lebar ?></td>
        </tr>
        <tr>
          <td colspan="3" align="center">Remark : <?php echo $tampilprint->Remark ?></td>
        </tr>
       
    </tbody>
  </table>
   <!-- jQuery 3 -->
  <script src="<?php echo base_url() ?>asset/assets/plugins/jQuery/jquery-1.12.4.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
     window.print();
     console.log('masuk kesini');
   });
</script>
 </script>
</body>
</html>