<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Packing List</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('MutasiGiro/index')?>">Data Packing List</a></li>
                            <li style="text-transform: capitalize;"> Detail Packing List - <b><?php echo $mutasigiro->Nomor ?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">
                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $mutasigiro->Tanggal ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Nomor</td>
                                    <td style="background-color: #ffffff;"><?php echo $mutasigiro->Nomor?></td>
                                </tr>
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Total</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $mutasigiro->Total ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Batal</td>
                                    <td style="background-color: #ffffff;"><?php echo $mutasigiro->Batal?></td>
                                </tr>     
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Mata Uang</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $mutasigiro->IDMataUang ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Kurs</td>
                                    <td style="background-color: #ffffff;"><?php echo $mutasigiro->Kurs?></td>
                                </tr>  
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Keterangan</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $mutasigiro->Keterangan ?></td>
                                </tr>
                               
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                        <div class="widget_content">

                            <table class="display data_tbl">
                              <thead>
                                 <tr>
                                    <th>No</th>
                                    <th>Jenis Giro</th>
                                    <th>Giro</th>
                                    <th>Status</th>
                                    <th>Nomor Lama</th>
                                    <th>Nomor Baru</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($mutasigirodetail)){
                                  ?>
                                  <?php
                              }else{
                                  $i=1;
                                  foreach ($mutasigirodetail as $data2) {
                                    ?>
                                    <tr class="odd gradeA">
                                      <td class="text-center"><?php echo $i; ?></td>
                                      <td><?php echo $data2->Jenis_giro; ?></td>
                                      <td><?php echo $data2->IDGiro; ?></td>
                                      <td><?php echo $data2->Status; ?></td>
                                      <td><?php echo $data2->Nomor_lama; ?></td>
                                      <td><?php echo $data2->Nomor_baru; ?></td>
                                  </tr>
                                  <?php
                                  $i++;
                              }
                          }
                          ?>

                      </tbody>
                  </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('MutasiGiro/index')?>" name="simpan">Kembali</a></span>
                                </div>
                                 <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>PackingList/print/<?php echo $mutasigiro->Nomor ?>">Print Data</a></span>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>