<?php $this->load->view('administrator/header') ; 


if ($kodemutasigiro->curr_number == null) {
  $number = 1;
} else {
  $number = $kodemutasigiro->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_po = 'MG'.date('y').$kodemax;
?>
<style type="text/css" scoped>
  /* Style the tab */
  .tab {
    overflow: hidden;
    border: 1px mutasigirolid #ccc;
    background-color: #f1f1f1;
  }
  .tab a {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    curmutasigiror: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
    color:black;
  }
  .tab a:hover {
    background-color: #ddd;
  }
  .tab a.active {
    background-color: #ccc;
  }
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px mutasigirolid #ccc;
    border-top: none;
  }
  .chzn-container {
    width: 103.5% !important;
  }
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Mutasi Giro</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Po">Data Mutasi Giro</a></li>
              <li class="active">Tambah Mutasi Giro</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Mutasi Giro</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_6">
                    <!-- form 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">No Mutasi Giro</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $code_po ?>" readonly>
                        <input type="hidden" name="Total" id="Total" readonly class="form-control">
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tanggal</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Komutasigirong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                   

                   
                  </div>
                  
                  <div class="form_grid_6">
                    <!-- radio 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="30" rows="10"></textarea>
                     
                      </div>
                    </div>

                    <!-- radio 3 -->
                   
                  </div>

                  <!-- clear content -->
                  <span class="clear"></span>
                </div> <hr>
                <div class="form_grid_12 multiline">
                    <div class="form_grid_6">
                      <label class="field_title dot-2">Jenis Giro</label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" placeholder="--Giro Masuk--" name="giro" id="giro" required oninvalid="this.setCustomValidity('Mutasi Giro Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <option value="">--Giro Masuk--</option>
                          <?php foreach($giro as $row) {
                            echo "<option value='$row->IDGiro'>$row->Nomor_Giro.-.$row->Jenis_Giro</option>";
                          }
                          ?>
                        </select>
                      </div>
                      <span class="clear"></span>

                      <label class="field_title dot-2">Nama Perusahaan</label>
                      <div class="form_input">
                      <input type="text" name="IDPerusahaan" id="IDPerusahaan" class="form-control" placeholder="Nama Perusahaan" readonly>
                      <input type="hidden" name="JenisGiro" id="JenisGiro" class="form-control"  readonly>
                      <input type="hidden" name="IDBaruP" id="IDBaruP" class="form-control"  readonly>
                      <input type="hidden" name="IDGiro" id="IDGiro" class="form-control"  readonly>
                      </div>
                      <span class="clear"></span>

                       <label class="field_title dot-2">No Faktur</label>
                      <div class="form_input">
                      <input type="text" name="NoFaktur" id="NoFaktur" class="form-control" placeholder="No Faktur" readonly>
                      <!-- <input type="hidden" name="idmerk" id="idmerk" class="form-control"  readonly>
                      <input type="hidden" name="idbarang" id="idbarang" class="form-control"  readonly> -->
                      </div>
                      <span class="clear"></span>

                     
                      <label class="field_title mt-dot2">Tanggal Faktur</label>
                      <div class="form_input">
                        <input id="TanggalFaktur" type="date" name="TanggalFaktur" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Komutasigirong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                      <span class="clear"></span>
                  

                      <label class="field_title dot-2">Nilai Giro</label>
                      <div class="form_input">
                      <input type="text" name="NilaiGiro" id="NilaiGiro" class="form-control" placeholder="Nilai Giro" readonly>
                      </div>
                      <span class="clear"></span>
                    </div>

                    <div class="form_grid_6">
                      <label class="field_title dot-2">Nomor Giro</label>
                      <div class="form_input">
                        <input type="text" name="NomorGiro" id="NomorGiro" placeholder="Nomor Giro" readonly>
                      </div>
                      <span class="clear"></span>

                      <label class="field_title dot-2">Status</label>
                      <div class="form_input">
                        <select style="width:102.25%;" class="chzn-select" placeholder="Cari Pengirim" name="Status" id="Status">
                         <option value="">--Pilih Status--</option>
                         <option value="Cair">Cair</option>
                         <option value="Batal">Batal</option>
                         <!-- <option value="Ganti Nomor">Ganti Nomor</option> -->
                        </select>
                      </div>
                      <span class="clear"></span>
                      
                      <!-- <label class="field_title dot-2">Nomor Baru</label>
                      <div class="form_input">
                        <input type="text" name="NomorBaru" id="NomorBaru" placeholder="Nomor Baru" >
                      </div>
                      <span class="clear"></span> -->

                      <label class="field_title dot-2">Nama Bank</label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" name="bank" id="bank">
                          <option value="">--Pilih Bank--</option>
                          <?php foreach($bank as $row) {
                            echo "<option value='$row->IDBank'>$row->Nama_COA</option>";
                          }
                          ?>
                        </select>
                      </div>
                      <span class="clear"></span>

                      <label class="field_title dot-2">Nomor Rekening</label>
                      <div class="form_input">
                        <input type="text" name="NomorRekening" id="NomorRekening" placeholder="Nomor Rekening" >
                        <input type="hidden" name="NamaBank" id="NamaBank" placeholder="NamaBank" >
                      </div>
                      <span class="clear"></span>

                      <div class="form_input">
                        <div class="btn_24_blue">
                          <button class="btn_24_blue" type="button" id="tambah_sementara">
                            Tambah Data
                          </button>
                        </div>
                      </div>
                    </div>
                    <span class="clear"></span>
                </div>
              </li>
              <div class="widget_content">
                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Jenis Giro</th>
                      <th>Perusahaan</th>
                      <th>No Faktur</th>
                      <th>Tanggal Faktur</th>
                      <th>Nilai Giro</th>
                      <th>Nomor Giro</th>
                     <th>Status</th>
                     <!-- <th>Nomor Baru</th> -->
                     <th>Nama Bank</th>
                     <th>No Rekening</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>
              </div>

              <li>
                <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                    <div class="btn_30_blue">
                     <!-- <span><a id="printdata" onclick="return false;" style="curmutasigiror: no-drop;">Print Data</a></span> -->
                     <span><a href="<?php echo site_url('mutasigiro'); ?>"> Batal</a></span>
                  </div>
                  <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save()" type="button" value="Simpan Data" title="Simpan Data"></span>
                  </div>
                </div>
              </li>
             
            </ul>

           
          </div>
          <!-- end tab & pane -->

        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/mutasigiro.js"></script>
<?php $this->load->view('administrator/footer') ; ?>