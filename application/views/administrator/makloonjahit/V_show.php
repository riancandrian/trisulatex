<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Surat Jalan Makloon Jahit</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('MakloonJahit/index')?>">Data Surat Jalan Makloon Jahit</a></li>
                            <li style="text-transform: capitalize;"> Detail Surat Jalan Makloon Jahit - <b><?php echo $makloon->Nomor ?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">
                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $makloon->Tanggal ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Nomor</td>
                                    <td style="background-color: #ffffff;"><?php echo $makloon->Nomor?></td>
                                </tr>
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Supplier</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $makloon->Nama ?></td>
                                </tr>
                                 <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Keterangan</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $makloon->Keterangan ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $makloon->Batal?></td>
                                </tr>     
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Total Qty Yard</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $makloon->Total_qty_yard ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Total Qty Meter</td>
                                    <td style="background-color: #ffffff;"><?php echo $makloon->Total_qty_meter?></td>
                                </tr>  
                                <!-- <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Saldo_yard</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $makloon->Saldo_yard ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Saldo_meter</td>
                                    <td style="background-color: #ffffff;"><?php echo $makloon->Saldo_meter?></td>
                                </tr>   -->
                               
                               
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                        <div class="widget_content">

                            <table class="display data_tbl">
                              <thead>
                                 <tr>
                                    <th class="center" style="width: 40px">No</th>
                                    <th>Barcode</th>
                                    <th class="hidden-xs">Corak</th>
                                    <th>Warna</th>
                                    <th>Merk</th>
                                    <th>Qty Yard</th>
                                    <th>Qty Meter</th>
                                    <th>Grade</th>
                                    <th>Satuan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($makloondetail)){
                                  ?>
                                  <?php
                              }else{
                                  $i=1;
                                  foreach ($makloondetail as $data2) {
                                    ?>
                                    <tr class="odd gradeA">
                                      <td class="text-center"><?php echo $i; ?></td>
                                      <td><?php echo $data2->Barcode; ?></td>
                                      <td><?php echo $data2->Corak; ?></td>
                                      <td><?php echo $data2->Warna; ?></td>
                                      <td><?php echo $data2->Merk; ?></td>
                                      <td><?php echo $data2->Qty_yard; ?></td>
                                      <td><?php echo $data2->Qty_meter; ?></td>
                                      <td><?php echo $data2->Grade; ?></td>
                                      <td><?php echo $data2->Satuan; ?></td>
                                  </tr>
                                  <?php
                                  $i++;
                              }
                          }
                          ?>

                      </tbody>
                  </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('MakloonJahit/index')?>" name="simpan">Kembali</a></span>
                                </div>
                                 <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>MakloonJahit/print/<?php echo $makloon->IDSJH ?>">Print Data</a></span>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>