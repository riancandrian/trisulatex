<?php $this->load->view('administrator/header') ; 
// if ($kodemakloon->curr_number == null) {
//   $number = 1;
// } else {
//   $number = $kodemakloon->curr_number + 1;
// }

// $kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
// $agen = $this->session->userdata("nama");

// $code_po = 'SJMJ'.date('y').$kodemax;
//echo $code_po;
?>
<!-- =============================================Style untuk tabs -->
<style type="text/css" scoped>
  .chzn-container {
    width: 103% !important;
  }
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Surat Jalan Makloon Jahit</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>MakloonJahit">Data Surat Jalan Makloon Jahit</a></li>
              <li class="active">Tambah Surat Jalan Makloon Jahit</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Surat Jalan Makloon Jahit</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">

                <!-- form header -->
                <div class="form_grid_12 w-100 alpha">

                  <!-- left form 8 -->
                  <div class="form_grid_8">
                
                 <input type="hidden" name="IDSJH" id="IDSJH" data-saldoYard class="form-control" value="<?= $datamakloon->IDSJH ?>" readonly>
                    <!-- form 1 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2 mb">Tanggal</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?= $datamakloon->Tanggal ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2 mb">Total Yard</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Qty_yard" id="Qty_yard" data-saldoYard class="form-control" placeholder="Total Qty Yard" value="<?= $datamakloon->Total_qty_yard ?>" readonly>
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2 mb pt">No Makloon Jahit</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $datamakloon->Nomor ?>" readonly>
                      </div>
                    </div>

                    <!-- form 6 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2 mb">Total Meter</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Qty_meter" id="Qty_meter"  data-saldoMeter class="form-control" placeholder="Total Qty Meter" value="<?= $datamakloon->Total_qty_meter ?>" readonly>
                      </div>
                    </div>

                    <!-- form 5 -->
                    <div class="form_grid_6 mb-1">
                      <label class="field_title mt-dot2 mb">Supplier</label>
                      <div class="form_input">
                        <select name="IDSupplier" id="IDSupplier" style="width:100%;" class="chzn-select">

                          <?php foreach($supplier as $row) {
                            if ($datamakloon->IDSupplier == $row->IDSupplier) {
                              echo "<option value='$row->IDSupplier' selected>$row->Nama</option>";
                            }
                            echo "<option value='$row->IDSupplier'>$row->Nama</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>


                  </div>
                  <!-- end left form 8 -->

                  <!-- right form 4 -->
                  <!-- form 7 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mt-dot2 mb">Keterangan</label>
                    <div class="form_input">
                      <textarea name="Keterangan" id="Keterangan" cols="30" rows="6" style="resize: none;"> <?= $datamakloon->Keterangan ?></textarea>
                    </div>
                  </div>
                  <!-- end right form 4 -->

                  <!-- clear content -->
                  <div class="clear"></div>

                </div>
                <!-- end form header -->

              </li>

              <!-- =============================-Scan Barcode -->
              <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title mt-dot2 mb"> Barcode Scan</label>
                  <div class="form_input">
                    <input type="text" name="Barcode" id="Barcode" class="form-control" placeholder="Scan Barcode" autofocus>
                  </div>
                </div>
              </li>
              <!-- =============================-Table Bawah -->

              <div class="widget_content">
                <table class="display data_tbl" id="tabelfsdfsf">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Barcode</th>
                      <th>Corak</th>
                      <th>Warna</th>
                      <th>Merk</th>
                      <th>Qty Yard</th>
                      <th>Qty Meter</th>
                      <th>Grade</th>
                      <th>Satuan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>
              </div>
              <li>
              <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                <!--  <div class="btn_30_blue">
                     <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                  </div> -->
                  <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save_change()" type="button"  value="Simpan Data" title="Simpan"></span>
                  </div>
                </div>
                </div>
              </li>
            </ul>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/makloon_jahit.js"></script>
<script>
  $(document).ready(function(){
    renderJSONE($.parseJSON('<?php echo json_encode($datamakloondetail) ?>'));
        //console.log(<?php// echo json_encode($datamakloondetail) ?>);
      });
    </script>
    <?php $this->load->view('administrator/footer') ; ?>