<?php $this->load->view('administrator/header') ; 
if ($no_dp->curr_number == null) {
  $number = 1;
} else {
  $number = $no_dp->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);

$code_no_dp = 'DP'.'/'.date('y').'/'.$kodemax;
?>
<!-- tittle Data -->
<div class="page_title">

	<!-- notify -->
	<?php echo $this->session->flashdata('Pesan');?>
	<!-- ====== -->

	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Data Down Payment</h3>
</div>
<!-- =========== -->


<!-- body content -->
<!-- body content -->
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">

				<!-- breadcrumb -->
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<li><a href="<?php echo base_url() ?>DownPayment">Data Down Payment</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Tambah Down Payment </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                	<div class="grid_12">
                		<span class="h_icon blocks_images"></span>
                		<h6>Tambah Down Payment</h6>
                	</div>
                </div>
                <div class="widget_content">
                	<form method="post" action="<?php echo base_url() ?>DownPayment/simpan" class="form_container left_label">
                        <ul>
                			<li>
                				<div class="form_grid_12 multiline">

                					<!-- Kode Asset -->
                					<label class="field_title">No DP</label>
                					<div class="form_input">
                						<input type="text" class="form-control" name="no_dp" placeholder="No DP" required oninvalid="this.setCustomValidity('No DP Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $code_no_dp ?>" readonly>
                					</div>

                                    <label class="field_title">Nominal DP</label>
                                    <div class="form_input">
                                        <span class="input-group-addon" style="">Rp. </span>
                                        <input name="nominal_dp" type="text" id="tanpa-rupiah-debit" class="form-control price-date" required oninvalid="this.setCustomValidity('Nominal DP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <!-- <span class=" label_intro">Street Address</span> -->
                                        <span class="clear"></span>
                                    </div>

                                    <label class="field_title">Keterangan</label>
                                    <div class="form_input">
                                        <textarea class="form-control" name="keterangan" placeholder="Keterangan" required oninvalid="this.setCustomValidity('Keterangan Tidak Boleh Kosong')" oninput="setCustomValidity('')"></textarea>
                                    </div>

                				</div>
                			</li>

                			<li>
                				<div class="form_grid_12">
                					<div class="form_input">
                						<div class="btn_30_light">
                							<span> <a href="<?php echo base_url() ?>DownPayment" name="simpan" title=".classname">Kembali</a></span>
                						</div>
                						<div class="btn_30_blue">
                							<span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                						</div>
                					</div>
                				</div>
                			</li>
                		</ul>
                	</form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============ -->

<?php $this->load->view('administrator/footer') ; ?>