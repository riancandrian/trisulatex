<?php $this->load->view('administrator/header') ; 

?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Piutang</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Piutang">Data Piutang</a></li>
                            <li class="active">Tambah Piutang</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Piutang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>piutang/simpan" class="form_container left_label">
                       <ul>
                            <li>
                                <div class="form_grid_12 multiline">


                                    <label class="field_title">Customer</label>
                                    <div class="form_input">
                                        <select data-placeholder="Cari Customer" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDCustomer" required oninvalid="this.setCustomValidity('Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            <option value=""></option>
                                            <?php 
                                            foreach ($customer as $data) {
                                                ?>
                                                <option value="<?php echo $data->IDCustomer ?>"><?php echo $data->Nama; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <label class="field_title">Tanggal Piutang</label>
                                    <div class="form_input">
                                        <input id="tanggal_piutang" type="date" name="Tanggal_Piutang" class="form-control" placeholder="Tanggal Piutang" required oninvalid="this.setCustomValidity('Tanggal Piutang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <span class="clear"></span>
                                    </div>

                                    <label class="field_title">Tanggal Jatuh Tempo</label>
                                    <div class="form_input">
                                       <input id="jatuh_tempo" type="date" name="Jatuh_Tempo" class="form-control" placeholder="Jatuh Tempo" required oninvalid="this.setCustomValidity('Tanggal Jatuh Tempo Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                       <span class="clear"></span>
                                   </div>

                                   <label class="field_title">No Faktur</label>
                                   <div class="form_input">
                                     <input type="text" class="form-control" name="No_Faktur" placeholder="No Faktur" required oninvalid="this.setCustomValidity('No Faktur Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                     <span class="clear"></span>
                                 </div>

                                 <label class="field_title">Nilai Piutang</label>
                                 <div class="form_input">
                                     <span class="input-group-addon">Rp. </span>
                                     <input type="text" class="form-control price-date" name="Nilai_Piutang" id="tanpa-rupiah-debit" placeholder="Nilai Piutang" required oninvalid="this.setCustomValidity('Nilai Piutang Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                                     <span class="clear"></span>
                                 </div>

                                 <!-- <label class="field_title">Pembayaran</label>
                                 <div class="form_input">
                                   <span class="input-group-addon">Rp. </span> -->
                                   <input type="hidden" name="Pembayaran" class="form-control price-date" placeholder="Pembayaran" id="tanpa-rupiah-kredit" value="0" required oninvalid="this.setCustomValidity('Pembayaran Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                  <!--  <span class="clear"></span>
                               </div> -->

                              <!--  <label class="field_title">Jenis Faktur</label> -->
                               <!-- <div class="form_input"> -->
                                   <input type="hidden" name="Jenis_Faktur" class="form-control" placeholder="Jenis Faktur" value="SA" readonly>
                                <!--    <span class="clear"></span>
                               </div> -->
                           </div>
                       </li>
                       <li>
                        <div class="form_grid_12">
                            <div class="form_input">
                                <div class="btn_30_light">
                                 <span> <a href="<?php echo base_url() ?>Piutang" name="simpan">Kembali</a></span>
                             </div>
                             <div class="btn_30_blue">
                              <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                          </div>
                      </div>
                  </div>
              </li>
          </ul>
      </form>
  </div>
</div>
</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>