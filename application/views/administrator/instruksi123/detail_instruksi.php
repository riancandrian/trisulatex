<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Invoice Pembelian</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('InvoicePembelian')?>">Data Invoice Pembelian</a></li>
                            <li style="text-transform: capitalize;"> Detail Invoice Pembelian </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->Tanggal ?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">Nomor</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $instruksi->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #ffffff;">Nama Supplier</td>
                                    <td  style="background-color: #ffffff;"><?php echo $instruksi->IDSupplier ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">Nomor SJ</td>
                                    <td style="background-color: #e5eff0;"><?php echo $instruksi->Nomor_sj ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">IDPO</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->IDPO ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">NoSO</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $instruksi->NoSO ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal Kirim</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->Tanggal_kirim ?></td>
                                </tr>
                                 <tr>
                                    <td  style="background-color: #e5eff0;">Total Yard</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $instruksi->Total_yard ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Total Meter</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->Total_meter ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Keterangan</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $instruksi->Keterangan ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->Batal ?></td>
                                </tr>
                               
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                        

                         <br><br>
                                 <div class="widget_content">

                <table class="display data_tbl">
                  <thead>
                   <tr>
                    <th class="center" style="width: 40px">No</th>
                    <th>Barang</th>
                    <th class="hidden-xs">Merk</th>
                    <th>Qty</th>
                    <th>Saldo</th>
                    <th>Satuan</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($pbjahitdetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($pbjahitdetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->IDBarang; ?></td>
                          <td><?php echo $data2->IDMerk; ?></td>
                          <td><?php echo $data2->Qty; ?></td>
                          <td><?php echo $data2->Saldo; ?></td>
                          <td><?php echo $data2->IDSatuan; ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>


                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('InvoicePembelian')?>">Kembali</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>