
<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Gudang</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Gudang">Data Gudang</a></li>
                            <li class="active">Edit Gudang</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Data Gudang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Gudang/update" class="form_container left_label">
                     <input type="hidden" name="id" value="<?php echo $gudang->IDGudang ?>">
                     <ul>
                        <li>
                            <div class="form_grid_12 multiline">
                                <label class="field_title">Kode Gudang</label>
                                <div class="form_input">

                                    <input type="text" class="form-control" name="Kode_Gudang" placeholder="Kode Gudang" value="<?php echo $gudang->Kode_Gudang; ?>" required oninvalid="this.setCustomValidity('Kode Gudang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    <span class="clear"></span>
                                </div>
                                <label class="field_title">Nama Gudang</label>
                                <div class="form_input">
                                    <input type="text" name="Nama_Gudang" class="form-control" placeholder="Nama Gudang" value="<?php echo $gudang->Nama_Gudang ?>" required oninvalid="this.setCustomValidity('Nama Gudang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    <span class="clear"></span>
                                </div>

                            </div>
                        </li>
                        <li>
                            <div class="form_grid_12">
                                <div class="form_input">
                                    <div class="btn_30_light">
                                     <span> <a href="<?php echo base_url() ?>Gudang" name="simpan">Kembali</a></span>
                                 </div>
                                 <div class="btn_30_blue">
                                  <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                              </div>
                          </div>
                      </div>
                  </li>
              </ul>
          </form>
      </div>
  </div>
</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>