<?php $this->load->view('administrator/header') ; 


if ($kodeso->curr_number == null) {
  $number = 1;
} else {
  $number = $kodeso->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_po = 'SOK'.date('y').$agen.$kodemax;
?>
<style type="text/css" scoped>
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}
.tab a {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
  color:black;
}
.tab a:hover {
  background-color: #ddd;
}
.tab a.active {
  background-color: #ccc;
}
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
.chzn-container {
  width: 103.5% !important;
}
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Sales Order</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>So">Data Sales Order</a></li>
              <li class="active">Tambah Sales Order</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Sales Order</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_6">
                    <!-- form 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">No SO</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $code_po ?>" readonly>
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tanggal SO</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Nama Customer</label>
                      <div class="form_input">

                        <select name="IDCustomer" id="IDCustomer" style="width:100%;" class="chzn-select">
                          <option value="" selected>--Pilih Customer--</option>
                          <?php foreach($trisula as $row) {
                            echo "<option value='$row->IDCustomer'>$row->Nama</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <!-- form 4 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Jatuh Tempo</label>
                      <div class="form_input">
                        <div class="form_grid_6"> 
                          <input id="Jatuh_tempo" type="text" onkeyup="hitung();" name="Jatuh_tempo" class="form-control tempo" placeholder="Jatuh tempo" value="75">
                        </div>
                        <div class="form_grid_6 text-right mt-dot2"> 
                          <input type="checkbox" class="checkbox" id="customize" onchange="customize_tempo()">
                          <label class="choice pr">Customize</label>
                        </div>
                      </div>
                    </div>
                    <?php
                    $tgl1 = date('Y-m-d');
                    $tgl2 = date('Y-m-d', strtotime('+75 days', strtotime($tgl1)));
                    ?>
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tgl Jatuh Tempo</label>
                      <div class="form_input">
                        <input id="Tanggal_jatuh_tempo" type="date" value="<?php echo $tgl2; ?>" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" placeholder="Tanggal Jatuh tempo">
                      </div>
                    </div>


                  </div>
                  
                  <div class="form_grid_6">

                    <!-- radio 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">PO Customer</label>
                      <div class="form_input">
                        <input type="text" name="No_po_customer" id="No_po_customer">
                      </div>
                    </div>

                    <!-- radio 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="30" rows="6"></textarea>
                        <input type="hidden" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly>
                        <input type="hidden" name="Grand_total" id="Grand_total" class="form-control" placeholder="Grand Total" readonly>
                      </div>
                    </div>

                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Status PPN</label>
                      <div class="form_input">
                       <select name="Status_ppn" id="Status_ppn" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" class="form-control" onchange="ubah_status_ppn()">
                        <option value="Include">Include</option>
                        <option value="Exclude">Exclude</option>
                      </select>
                    </div>
                  </div>

                  <!-- radio 3 -->

                </div>

                <!-- clear content -->
                <span class="clear"></span>
              </div> <hr>
              <div class="form_grid_12 multiline">
                <div class="form_grid_6">
                  <label class="field_title dot-2">Corak</label>
                  <div class="form_input">
                    <select style="width:100%;" class="chzn-select" placeholder="--Pilih Corak--" name="corak" id="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      <option value="">--Pilih Corak--</option>
                      <?php foreach($corak as $row) {
                        echo "<option value='$row->IDCorak' data-corak='$row->Corak'>$row->Corak</option>";
                      }
                      ?>
                    </select>
                  </div>
                  <span class="clear"></span>

                  <label class="field_title dot-2">Merk</label>
                  <div class="form_input">
                    <input type="text" name="merk" id="merk" class="form-control" placeholder="Merk" readonly>
                    <input type="hidden" name="idmerk" id="idmerk" class="form-control" placeholder="Grand Total" readonly>
                    <input type="hidden" name="idbarang" id="idbarang" class="form-control" placeholder="Ini IDBarang Hidden" readonly>
                  </div>
                  <span class="clear"></span>

                  <label class="field_title dot-2">Warna</label>
                  <div class="form_input">
                    <select name="warna" id="warna" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                      <option value="">--Silahkan Pilih Corak Dahulu--</option>
                    </select>
                  </div>
                  <span class="clear"></span>
                </div>

                <div class="form_grid_6">
                  <label class="field_title dot-2">Qty</label>
                  <div class="form_input">
                    <input type="text" name="Qty" id="Qty" onkeyup="this.value=this.value.replace(/[^0-9.]/,'')" >
                  </div>
                  <span class="clear"></span>

                  <label class="field_title dot-2">Satuan</label>
                  <div class="form_input">
                    <select style="width:102.25%;" class="chzn-select" placeholder="Cari Pengirim" name="Satuan" id="Satuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                     <option value="">--Pilih Satuan--</option>
                     <?php foreach($satuan as $row) {
                      echo "<option value='$row->IDSatuan' data-satuan='$row->Satuan'>$row->Satuan</option>";
                    }
                    ?>
                  </select>
                </div>
                <label class="field_title dot-2">Harga Satuan</label>
                <div class="form_input">
                  <input type="text" name="Harga" id="harga-123">
                </div>
                <span class="clear"></span>

                <br><br>
                <div class="btn_24_blue">
                  <button class="btn_24_blue" type="button" id="tambah_sementara">
                    Tambah Data
                  </button>
                </div>
              </div>
              <span class="clear"></span>
            </div>
          </li>
          <div class="widget_content">
            <table class="display data_tbl" id="tabelfsdfsf">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Corak</th>
                  <th>Merk</th>
                  <th>Warna</th>
                  <th>Qty</th>
                  <th>Satuan</th>
                  <th>Harga Satuan</th>
                  <th>Total</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="tabel-cart-asset">

              </tbody>
            </table>
          </div>
          <li>
            <div class="form_grid_12 w-100 alpha">
              <div class="form_grid_8">
                <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                    <div class="btn_30_blue">
                     <!-- <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span> -->
                     <span><a href="<?php echo site_url('so'); ?>"> Batal</a></span>
                   </div>
                   <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save()" type="button" value="Simpan Data" title="Simpan Data"></span>
                  </div>
                </div>
              </div>
            </div>

            <input type="hidden" id="totaldpp" name="totaldpp" class="form-control" placeholder="total dpp" readonly>
            <div class="form_grid_4">
              <div class="form_grid_12 mb-1">
                <label class="field_title mb mt-dot2">DPP</label>
                <div class="form_input input-not-focus">
                  <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                </div>
              </div>
              <div class="form_grid_12 mb-1">
                <label class="field_title mb mt-dot2">PPN</label>
                <div class="form_input input-not-focus">
                  <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" readonly>
                </div>
              </div>
                                                            <!-- <div class="form_grid_12 mb-1">
                                                                <label class="field_title mb mt-dot2">Discount</label>
                                                                <div class="form_input input-not-focus"> -->
                                                                  <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" value="0" readonly>
                                                                <!-- </div>
                                                                </div> -->
                                                                <div class="form_grid_12 mb-1">
                                                                  <label class="field_title mb">Total Invoice</label>
                                                                  <div class="form_input input-not-focus">
                                                                    <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                              <!-- clear content -->
                                                              <div class="clear"></div>
                                                            </div>
                                                          </li>

                                                        </ul>



                                                      </div>
                                                      <!-- end tab & pane -->

                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                        <!--   <script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.41.js"></script> -->
                                          <script type="text/javascript" src="<?php echo base_url()?>libjs/so.js"></script>
                                          <script type="text/javascript">


                                            $("#Jatuh_tempo").prop("disabled", true);
                                            function hitung() {
                                              var a = new Date($("#Tanggal").val());
                                              var b = $("#Jatuh_tempo").val();

                                              var today = new Date(a.getTime()+(b*24*60*60*1000));
                                              var dd = today.getDate();
                                              var mm = today.getMonth()+1;

                                              var yyyy = today.getFullYear();
                                              if(dd<10){
                                                dd='0'+dd;
                                              } 
                                              if(mm<10){
                                                mm='0'+mm;
                                              } 
                                              var today = yyyy+'-'+mm+'-'+dd;
                                              $("#Tanggal_jatuh_tempo").val(today);
                                            }
                                            function customize_tempo(){
                                              if (document.getElementById('customize').checked)
                                              {
                                                $("#Jatuh_tempo").prop("disabled", false);
                                              } else {
                                                $("#Jatuh_tempo").prop("disabled", true);
                                              }
                                            }

                                            // $(".rupiahformat").autoNumeric('init', {
                                            //   aSep: '.', 
                                            //   aDec: ',',
                                            //   aForm: true,
                                            //   vMax: '999999999999999999999',
                                            //   vMin: '-999999999'
                                            // });

                                            
                                        </script>
                                        <?php $this->load->view('administrator/footer') ; ?>