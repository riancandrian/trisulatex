<?php $this->load->view('administrator/header') ; 

?>
<style type="text/css" scoped>
  /* Style the tab */
  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
  }
  .tab a {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
    color:black;
  }
  .tab a:hover {
    background-color: #ddd;
  }
  .tab a.active {
    background-color: #ccc;
  }
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }
  .chzn-container {
    width: 103.5% !important;
  }
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Sales Order</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>So">Data Sales Order</a></li>
              <li class="active">Edit Sales Order</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Edit Data Sales Order</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_6">
                    <!-- form 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">No SO</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $dataso->Nomor ?>" readonly>
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Tanggal SO</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $dataso->Tanggal ?>">
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title mt-dot2">Nama Customer</label>
                      <div class="form_input">
                        
                        <select name="IDCustomer" id="IDCustomer" style="width:100%;" class="chzn-select">
                          <option value="" selected>--Pilih Customer--</option>
                          <?php foreach($trisula as $row) {
                            if($dataso->IDCustomer == $row->IDCustomer){
                              echo "<option value='$row->IDCustomer' selected>$row->Nama</option>";
                            }
                            echo "<option value='$row->IDCustomer'>$row->Nama</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <!-- form 4 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Tanggal Jatuh Tempo</label>
                      <div class="form_input">
                        <input id="Tanggal_jatuh_tempo" type="date" name="Tanggal_jatuh_tempo" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $dataso->Tanggal_jatuh_tempo ?>">
                      </div>
                    </div>

                   
                  </div>
                  
                  <div class="form_grid_6">

                    <!-- radio 1 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">PO Customer</label>
                      <div class="form_input">
                      <input type="text" name="No_po_customer" id="No_po_customer" value="<?php echo $dataso->No_po_customer ?>">
                      </div>
                    </div>

                    <!-- radio 2 -->
                    <div class="form_grid_12 mb-1">
                      <label class="field_title">Keterangan</label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="30" rows="10"> <?= $dataso->Keterangan ?></textarea>
                        <input type="text" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly value="<?php echo $dataso->Total_qty ?>">
                        <input type="text" name="Grand_total" id="Grand_total" class="form-control" placeholder="Grand Total" readonly value="<?php echo $dataso->Grand_total ?>">
                        <input type="hidden" name="IDSOK" id="IDSOK" value="<?php echo $dataso->IDSOK ?>">
                      </div>
                    </div>

                    <!-- radio 3 -->
                   
                  </div>

                  <!-- clear content -->
                  <span class="clear"></span>
                </div> <hr>
                <div class="form_grid_12 multiline">
                    <div class="form_grid_6">
                      <label class="field_title dot-2">Corak</label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" placeholder="--Pilih Corak--" name="corak" id="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <option value="">--Pilih Corak--</option>
                          <?php foreach($corak as $row) {
                            echo "<option value='$row->IDCorak'>$row->Corak</option>";
                          }
                          ?>
                        </select>
                      </div>
                      <span class="clear"></span>

                       <label class="field_title dot-2">Merk</label>
                      <div class="form_input">
                      <input type="text" name="merk" id="merk" class="form-control" placeholder="Merk" readonly>
                      <input type="hidden" name="idmerk" id="idmerk" class="form-control" placeholder="Grand Total" readonly>
                      <input type="hidden" name="idbarang" id="idbarang" class="form-control" placeholder="Grand Total" readonly>
                      </div>
                      <span class="clear"></span>

                      <label class="field_title dot-2">Warna</label>
                      <div class="form_input">
                        <select name="warna" id="warna" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <option value="">--Silahkan Pilih Corak Dahulu--</option>
                        </select>
                      </div>
                      <span class="clear"></span>
                    </div>

                    <div class="form_grid_6">
                      <label class="field_title dot-2">Qty</label>
                      <div class="form_input">
                        <input type="text" name="Qty" id="Qty" onkeyup="this.value=this.value.replace(/[^0-9.]/,'')">
                      </div>
                      <span class="clear"></span>

                      <label class="field_title dot-2">Satuan</label>
                      <div class="form_input">
                        <select style="width:102.25%;" class="chzn-select" placeholder="Cari Pengirim" name="Satuan" id="Satuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                         <option value="">--Pilih Satuan--</option>
                          <?php foreach($satuan as $row) {
                            echo "<option value='$row->IDSatuan'>$row->Satuan</option>";
                          }
                          ?>
                        </select>
                      </div>
                      <label class="field_title dot-2">Harga Satuan</label>
                      <div class="form_input">
                        <input type="text" name="Harga" id="Harga" >
                      </div>
                      <span class="clear"></span>

                      <div class="form_input">
                        <div class="btn_24_blue">
                          <button class="btn_24_blue" type="button" id="tambah_sementara">
                            Edit Data
                          </button>
                        </div>
                      </div>
                    </div>
                    <span class="clear"></span>
                </div>
              </li>
              <div class="widget_content">
                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Corak</th>
                      <th>Merk</th>
                      <th>Warna</th>
                      <th>Qty</th>
                      <th>Satuan</th>
                      <th>Harga Satuan</th>
                     <th>Total</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="tabel-cart-asset">

                  </tbody>
                </table>
              </div>

              <li>
                <div class="widget_content px-2 text-center">
                  <div class="py-4 mx-2">
                    <div class="btn_30_blue">
                     <!-- <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span> -->
                     <span><a href="<?php echo site_url('so'); ?>"> Batal</a></span>
                  </div>
                  <div class="btn_30_blue">
                    <span><input name="simpan" id="simpan" onclick="save_change()" type="button" value="Simpan Data" title="Simpan Data"></span>
                  </div>
                </div>
              </li>
             
            </ul>

           
          </div>
          <!-- end tab & pane -->

        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/so.js"></script>
<script>
  $(document).ready(function(){
    renderJSONE($.parseJSON('<?php echo json_encode($datasodetail) ?>'));
        //console.log(<?php echo json_encode($datasodetail) ?>);
      });
    </script>
<?php $this->load->view('administrator/footer') ; ?>