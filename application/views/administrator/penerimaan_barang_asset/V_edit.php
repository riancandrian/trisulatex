<?php $this->load->view('administrator/header'); ?>

<!-- style -->
<style type="text/css">
.chzn-container{
    width: 102.5% !important;
}
</style>
<!-- tittle data -->
<div class="page_title">
    <div id="alert"></div>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Edit Data Penerimaan Asset</h3>
</div>
<!-- ======================= -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Penerimaan_barang_asset">Data Penerimaan Barang</a></li>
                            <li> Edit Penerimaan Barang </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Penerimaan Barang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>PenerimaanBarang/update" class="form_container left_label">
                        <ul>
                            <li class="body-search-data">
                                <div class="form_grid_12 w-100 alpha">
                                    <?php foreach ($data as $data): ?>
                                        
                                    
                                    <div class="form_grid_8">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2">Tanggal</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control date-picker hidden-sm-down" name="Tanggal" id="Tanggal" value="<?php echo $data->Tanggal?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2">Nomor PB</label>
                                            <div class="form_input input-not-focus">
                                                <input type="text" class="form-control" name="no_bo" id="no_bo" value="<?php echo $data->Nomor?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2">No PO</label>
                                            <div class="form_input input-not-focus">
                                                <input type="text" class="form-control" name="no_po" id="no_po" value="<?php echo $data->Nomor ?>" readonly>
                                            </div>
                                        </div>

                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2">Supplier</label>
                                            <div class="form_input">
                                                <select data-placeholder="Cari Berdasarkan Supplier" style="width:100%;" class="chzn-select" tabindex="13" name="id_supplier" id="id_supplier" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                    <option value=""></option>
                                                    <?php foreach($supplier as $row) { ?>
                                                    <option value='<?php echo $row->IDSupplier ?>' <?php echo $data->IDSupplier == $row->IDSupplier ? 'selected' : ''; ?>><?php echo $row->Nama?></option>";
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2">Total Qty</label>
                                            <div class="form_input input-not-focus">
                                                <input type="text" class="form-control" name="total_qty" id="total_qty" readonly>
                                            </div>
                                        </div>

                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2">Total Harga</label>
                                            <div class="form_input input-not-focus">
                                                <input type="text" class="form-control" name="total_harga" id="total_harga" readonly>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form_grid_4 mb-1">
                                        <label class="field_title mt-dot2">Keterangan</label>
                                        <div class="form_input">
                                            <textarea type="text" class="form-control" name="keterangan" id="Keterangan" rows="9" style="resize: none;"><?php echo $data->Keterangan?></textarea>
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo $data->IDTBAsset?>" name="id" id="IDTBS">
                                    <span class="clear"></span>
                                    <?php endforeach ?>
                                </div>
                            </li>
                            <div class="widget_content">
                                <table class="display data_tbl" id="tabelfsdfsf">
                                    <thead>
                                     <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Asset</th>
                                        <th class="text-center">Qty</th>
                                        <th class="text-center">Satuan</th>
                                        <th class="text-center">Harga</th>
                                    </tr>
                                </thead>
                                <tbody id="tabel-cart-asset">

                                </tbody>
                            </table>
                        </div>
                        <li>
                            <div class="widget_content px-2 text-center">
                                <div class="py-4 mx-2">
                                    <div class="btn_30_blue">
                                        <span><input type="button" class="btn_24_blue" id="print_" name="print_" onclick="print_cek_edit()" value="Print Data"></span>
                                    </div>
                                    <div class="btn_30_blue">
                                        <span><a id="simpan_" onclick="saveChange()" style="cursor: pointer;">Simpan</a></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/Penerimaan_barang_asset.js"></script>
<script>
    var baseUrl = "<?php echo base_url()?>";
    $(document).ready(function(){
        renderJSON($.parseJSON('<?php echo json_encode($detail) ?>'));
    });
</script>
<?php $this->load->view('administrator/footer') ; ?>