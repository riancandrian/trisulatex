<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
  /*  .table_top{
        display: none !important;
        }*/
    </style>
    <!-- tittle data -->
    <div class="page_title">
       <?php echo $this->session->flashdata('Pesan');?>
       <span class="title_icon"><span class="blocks_images"></span></span>
       <h3><?= $title_master ?></h3>
       <div class="top_search">

       </div>
   </div>
   <!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
   <!-- body data -->
   <div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                          <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                          <li class="active"><?= $title ?></li>
                      </ul>
                  </div>
              </div>

              <div class="widget_content">
                <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>PoUmum/pencarian_store" class="form_container left_label">
                    <ul>
                        <li class="px-1">

                            <!-- search -->
                            <div class="form_grid_12 w-100 alpha">

                                <!-- button -->
                            

                                <div class="form_grid_9 ">

                                    <!-- form 1 -->
                                    <div class="form_grid_3 ">   
                                        <label class="field_title mt-dot2">Periode</label>
                                        <div class="form_input">
                                            <input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                        </div>
                                    </div>

                                    <!-- form 2 -->
                                    <div class="form_grid_3 ">   
                                        <label class="field_title mt-dot2"> S/D </label>
                                        <div class="form_input">
                                            <input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                        </div>
                                    </div>

                                    <div class="form_grid_3 ">   
                                        <select name="jenispencarian" data-placeholder="No PO" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value="Nomor">No PO</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_2">   
                                        <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
                                    </div>
                                    <div class="form_grid_1">
                                        <div class="btn_24_blue">
                                            <input type="submit" name="" value="search">
                                        </div>
                                    </div>
                                </div>

                                <span class="clear"></span>
                            </div>
                            <!-- end search -->
                            
                        </li>

                    </form>
                </div>
                <div style="position: relative;z-index: 1; left: 10px; top: 10px;font-size: 11px">
                    <div class="btn_30_light" style="position: absolute;">
                                    <a href="<?php echo base_url() ?>PoUmum/exporttoexcel"><span class="icon doc_excel_table_co"></span><span class="btn_link">Exsport XLS</span></a>
                                </div>
                </div>
                <form action="<?php echo base_url() ?>po/delete_multiple" method="post" id="ubahstatus">
                    <div class="widget_content">
                        <table class="display" id="action_tbl">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nomor PO</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Supplier</th>
                                    <th>Corak</th>
                                    <th>Warna</th>
                                    <th>Merk</th>
                                    <th>Lot</th>
                                    <th>Jenis Pesanan</th>
                                    <th>PIC</th>
                                    <th>Prioritas</th>
                                    <th>Bentuk</th>
                                    <th>Panjang</th>
                                    <th>Point</th>
                                    <th>Kirim</th>
                                    <th>Stamping</th>
                                    <th>Posisi</th>
                                    <th>Posisi1</th>
                                    <th>M10</th>
                                    <th>Kain</th>
                                    <th>Lembaran</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($po)){ ?>
                                <!-- <tr>
                                    <td colspan="7"> Data Kosong </td>
                                </tr> -->
                                <?php }else{

                                    $i=1;
                                    foreach ($po as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($data->tanggal)); ?></td>
                                            <td><?php echo $data->nomor; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($data->tanggal_selesai)); ?></td>
                                            <td><?php echo $data->namasup; ?></td>
                                            <td><?php echo $data->kode_corak; ?></td>
                                            <td><?php echo $data->warna; ?></td>
                                            <td><?php echo $data->merk; ?></td>
                                            <td><?php echo $data->lot; ?></td>
                                            <td><?php echo $data->jenis_pesanan; ?></td>
                                            <td><?php echo $data->pic; ?></td>
                                            <td><?php echo $data->prioritas; ?></td>
                                            <td><?php echo $data->bentuk; ?></td>
                                            <td><?php echo $data->panjang; ?></td>
                                            <td><?php echo $data->point; ?></td>
                                            <td><?php echo $data->kirim; ?></td>
                                            <td><?php echo $data->stamping; ?></td>
                                            <td><?php echo $data->posisi; ?></td>
                                            <td><?php echo $data->posisi1; ?></td>
                                            <td><?php echo $data->m10; ?></td>
                                            <td><?php echo $data->kain; ?></td>
                                            <td><?php echo $data->lembaran; ?></td>
                                            <td><?php echo $data->qty; ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                       
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>
