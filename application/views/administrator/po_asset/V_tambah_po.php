<?php $this->load->view('administrator/header') ; 


if ($kodepo->curr_number == null) {
  $number = 1;
} else {
  $number = $kodepo->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
//$agen = $namaagent->Initial;

$code_po = 'POA'.date('y').$kodemax;
//echo $code_po;
?>
<!-- =============================================Style untuk tabs -->
<style type="text/css" scoped>
  /* Style the tab */
  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
  }
  .tab a {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
    color:black;
  }
  .tab a:hover {
    background-color: #ddd;
  }
  .tab a.active {
    background-color: #ccc;
  }
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }
  .chzn-container {
    width: 103.5% !important;
  }
  .table_bottom{
  margin-top: 3rem;
}
a.disabled {
   pointer-events: none;
   cursor: default;
}
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Purchase Order Asset</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>PO_Asset">Data Purchase Order Asset</a></li>
              <li class="active">Tambah Purchase Order Asset</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Purchase Order Asset</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>
              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_10">
                    <!-- form 1 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Tanggal</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Supplier</label>
                      <div class="form_input">
                        <select name="IDSupplier" id="IDSupplier" style="width:100%;" class="chzn-select">
                          <?php foreach ($supplier as $trisulas) {
                            ?>
                            <option value="<?php echo $trisulas->IDSupplier ?>"><?php echo $trisulas->Nama ?></option>
                            <?php
                          } 
                          ?>
                        </select>
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">No PO</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $code_po ?>" readonly>
                      </div>
                    </div>

                    <!-- form 9 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Total</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly>
                      </div>
                    </div>
                  </div>

                  <!-- clear content -->
                  <span class="clear"></span>
                </div>
              </li>
              <div class="grid_12 mx w-100">
                <div class="widget_wrap tabby">
                  <div class="widget_top">
                <!-- <span class="h_icon bended_arrow_right"></span>
                  <h6>Tabby Widget</h6> -->
                  <div id="widget_tab">
                    <ul>
                      <li class="px py"><a href="#tab1" class="active_tab">Input Data</a></li>
                      <li class="px py"><a href="#tab4">Catatan</a></li>

                    </ul>
                  </div>
                </div>
                <div class="widget_content">

                  <!-- tab & pane 3 -->
                  <div id="tab1">
                    <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                      <div class="grid_12 w-100">
                        <span class="h_icon blocks_images"></span>
                        <h6>Input Data</h6>
                      </div>
                    </div>
                    <div class="form_grid_12 w-100 alpha">
                      <div class="form_grid_4 mb-1">
                        <ul>
                          <li>
                            <div class="form_grid_12 mb-1">
                              <label class="field_title mt-dot2">Group Asset</label>
                              <div class="form_input">
                                <select name="GroupAsset" id="GroupAsset" style="width:100%;" class="chzn-select" onchange="get_asset()">
                                  <?php foreach ($group as $groupasset) {
                                    ?>
                                    <option value="<?php echo $groupasset->IDGroupAsset ?>" data-namagroupasset=<?php echo $groupasset->Group_Asset ?>><?php echo $groupasset->Group_Asset ?></option>
                                    <?php
                                  } 
                                  ?>
                                </select>
                              </div>
                            </div>
                            <div class="form_grid_12 mb-1">
                              <label class="field_title mt-dot2">Nama</label>
                              <div class="form_input">
                                <select name="Nama" id="Nama" style="width:100%;">
                                  <option value="">--Pilih Group Asset--</option>
                                </select>
                              </div>
                            </div>
                            
                            <div class="form_grid_12 mb-1">
                              <label class="field_title mt-dot2">Qty</label>
                              <div class="form_input">
                                <input type="text" name="Qty" id="Qty" class="form-control" placeholder="Qty" onkeyup="this.value=this.value.replace(/[^0-9.]/,'')">
                              </div>
                            </div>
                            <div class="form_grid_12 mb-1">
                              <label class="field_title mt-dot2">Harga</label>
                              <div class="form_input">
                                <input type="text" name="Harga" id="harga-123" class="form-control" placeholder="Harga" onkeyup="this.value=this.value.replace(/[^0-9.]/,'')">
                              </div>
                            </div>
                          </li>
                          <li>
                            <div class="form_input">
                              <div class="btn_24_blue">
                                <button class="btn_24_blue" type="button" id="tambah_sementara">
                                  Tambah Data
                                </button>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div class="form_grid_8 mb-1 mr" style="width: 65.6%">
                        <table class="display data_tbl" id="tabelfsdfsf">
                          <thead>
                            <tr>
                              <th class="center" style="width: 40px">No</th>
                              <th>Group Barang</th>
                              <th>Nama</th>
                              <th>Qty</th>
                              <th>Harga</th>
                              <th>Total</th>
                              <th style="width: 50px">Action</th>
                            </tr>
                          </thead>
                          <tbody id="tabel-cart-asset">

                          </tbody>
                        </table>

                          <div class="form_grid_12 w-100 alpha">


                      <!-- form 1 -->
                      <div class="form_grid_2 mb-1" style="right: 15px; bottom: 5.5rem; float: right;">
                        <div class="form_input">
                          <input type="text" name="Totalwarna" id="Totalwarna" class="form-control" placeholder="Grand Total" required>
                        </div>
                      </div>
                    </div>
                    <span class="clear"></span>
                      </div>
                      <span class="clear"></span>
                    </div>
                  </div>
                </div>


                <div id="tab4">
                  <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                    <div class="grid_12 w-100">
                      <span class="h_icon blocks_images"></span>
                      <h6>Catatan</h6>
                    </div>
                  </div>
                  <ul>
                    <li>

                      <div class="form_grid_12 multiline">
                        <div class="form_grid_12">
                          <label class="field_title dot-2">Catatan</label>
                          <div class="form_input">
                            <textarea name="Keterangan" id="Keterangan" cols="142" rows="10" tabindex="5" style="resize: none;"></textarea>
                          </div>
                          <span class="clear"></span>
                        </div>

                        <span class="clear"></span>
                      </div>
                    </li>
                  </ul>
                  <ul>
                    <li>

                      <div class="form_grid_12">
                        <div class="text-right">

                          <!-- <div class="btn_30_blue">
                            <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                          </div> -->
                          <div class="btn_30_blue">
                            <span><input name="simpan" onclick="save()" id="simpan" type="button" class="btn_small btn_blue" value="Simpan"></span>
                          </div>
                        </div>

                        <span class="clear"></span>
                      </div>

                    </li>
                  </ul>
                </div>

              </div>
            </div>
            <!-- end tab & pane -->
          </ul>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/poasset.js"></script>
<?php $this->load->view('administrator/footer') ; ?>