<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Purchase Order Asset</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('PO_Asset/index')?>">Data Purchase Order Asset</a></li>
                            <li style="text-transform: capitalize;"> Detail Purchase Order Asset - <b><?php echo $po->IDPOAsset ?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">
                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo DateFormat($po->CTime); ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Nomor</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Nomor?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Supplier</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Nama ?></td>
                                </tr>   
                                <tr>         
                                    <td style="background-color: #e5eff0;">Keterangan</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Keterangan?></td>
                                </tr> 
                                <tr>         
                                    <td style="background-color: #e5eff0;">Status</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Batal?></td>
                                </tr>     
                                <tr>         
                                    <td style="background-color: #e5eff0;">Total Harga</td>
                                    <td style="background-color: #e5eff0;"><?php echo convert_currency($po->Grand_Total) ?></td>
                                </tr>  

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                                 <div class="widget_content">

                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Group Asset</th>
                      <th>Nama Asset</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($podetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($podetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Group_Asset; ?></td>
                          <td><?php echo $data2->Nama_Asset; ?></td>
                          <td><?php echo convert_currency($data2->Qty); ?></td>
                          <td><?php echo convert_currency($data2->Harga_Satuan); ?></td>
                          <td><?php echo convert_currency($data2->Harga_Satuan*$data2->Qty); ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('PO_Asset/index')?>" name="simpan">Kembali</a></span>
                                </div>
                                <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>PO_Asset/print_data_po/<?php echo $po->IDPOAsset ?>">Print Data</a></span>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>