<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Booking Order</h3>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li> Booking Order </li>
                        </ul>
                    </div>
                </div>


                <div class="widget_content">
                    <form action="<?php echo base_url() ?>Laporan_bo/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">

                                <!-- search -->
                                <div class="form_grid_12 w-100 alpha">

                                    <div class="form_grid_10">

                                        <!-- form 1 -->
                                        <div class="form_grid_3 ">   
                                            <label class="field_title mt-dot2">Periode</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_from" id="date_from" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>

                                        <!-- form 2 -->
                                        <div class="form_grid_3 ">   
                                            <label class="field_title mt-dot2"> S/D </label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_until" id="date_until" value="<?php echo $tgl_terakhir ?>">
                                            </div>
                                        </div>

                                        <div class="form_grid_3 ">   
                                            <select name="jenispencarian" id="jenispencarian" data-placeholder="Nomor BO" style="width: 100%!important" class="chzn-select" tabindex="13">
                                                <option value="Nomor">No BO</option>
                                            </select>
                                        </div>
                                        <div class="form_grid_2">   
                                            <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container" id="keyword">
                                        </div>
                                        <div class="form_grid_1">
                                            <div class="btn_24_blue">
                                                <input type="submit" name="" value="search">
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="form_grid_10">
                                        <div class="form_grid_1">
                                            <div class="btn_24_blue">
                                                <input type="button" name="excel" id="excel" value="Export Excel" onclick="exportexcel()">
                                            </div>
                                        </div>
                                    </div>-->
                                    <span class="clear"></span>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
                <div style="position: relative;z-index: 1; left: 10px; top: 10px;font-size: 11px">
                    <div class="btn_30_light" style="position: absolute;">
                                    <a href="<?php echo base_url() ?>Laporan_bo/exporttoexcel"><span class="icon doc_excel_table_co"></span><span class="btn_link">Exsport XLS</span></a>
                                </div>
                </div>
                <div class="widget_content">
                    <table class="display data_tbl">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nomor BO</th>
                                <th>Tanggal Selesai</th>
                                <th>Corak</th>
                                <th>Qty Yard</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($booking_order)){
                                $i=1;
                                foreach ($booking_order as $data) {
                                    ?>
                                    <tr class="odd gradeA">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo DateFormat($data->tanggal); ?></td>
                                        <td><?php echo $data->nomor; ?></td>
                                        <td><?php echo DateFormat($data->tanggal_selesai); ?></td>
                                        <td><?php echo $data->corak; ?></td>
                                        <td><?php echo $data->qty; ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <div class="widget_content text-right px-2">
                    <div class="py-4 mx-2">
                        <div class="btn_24_blue">
                            <input type="submit" name="" value="Batal">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    base_url = "<?php echo base_url() ?>";
</script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/laporan_bo.js"></script>
<?php $this->load->view('administrator/footer') ; ?>