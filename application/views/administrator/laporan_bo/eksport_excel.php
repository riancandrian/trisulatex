<?php
header("Content-Type:application/vnd.ms-excel");
header('Content-Disposition:attachment; filename="Booking Order.xls"');
 ?>

  <table class="display data_tbl">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nomor BO</th>
                                <th>Tanggal Selesai</th>
                                <th>Corak</th>
                                <th>Qty Yard</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($booking_order)){
                                $i=1;
                                foreach ($booking_order as $data) {
                                    ?>
                                    <tr class="odd gradeA">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo DateFormat($data->tanggal); ?></td>
                                        <td><?php echo $data->nomor; ?></td>
                                        <td><?php echo DateFormat($data->tanggal_selesai); ?></td>
                                        <td><?php echo $data->corak; ?></td>
                                        <td><?php echo $data->qty; ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>