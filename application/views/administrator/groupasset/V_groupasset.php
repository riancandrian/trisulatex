 <?php $this->load->view('administrator/header') ; ?>
 <!-- tittle page -->
 <div class="page_title">

    <!-- pesan -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ===== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Group Asset</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <!-- <li><a href="#">Dashboard</a></li> -->
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Data Group Asset</li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>GroupAsset/pencarian" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                                <div class="form_grid_12 w-100 px">
                                    <div class="form_grid_4 mx">
                                        <div class="btn_24_blue">
                                            <a href="<?php echo base_url() ?>GroupAsset/tambah_groupasset"><span>Tambah Data</span></a>
                                        </div>
                                    </div>
                                    <div class="form_grid_2">
                                        <select data-placeholder="Cari Berdasarkan" name="jenispencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="Kode_Group_Asset">Kode Asset</option>
                                            <option value="Group_Asset">Group Asset</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_2">
                                        <select data-placeholder="Pilih Status" name="statuspencarian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_3">
                                        <input name="keyword" type="text" placeholder="Masukkan keyword" placeholder="Masukkan Keyword">
                                        <!-- <span class=" label_intro">Last Name</span> -->
                                    </div>
                                    <div class="form_grid_1 flot-right">
                                        <div class="btn_24_blue">
                                            <input type="submit" name="" value="search">
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>

                <div class="widget_content">
                    <table class="display data_tbl">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Asset</th>
                                <th>Group Asset</th>
                                <th>Tarif Penyusutan</th>
                                <th>Umur</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(empty($groupasset)){
                                ?>
                                <?php
                            }else{
                                $i=1;
                                foreach ($groupasset as $data) {
                                    ?>
                                    <tr class="odd gradeA">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $data->Kode_Group_Asset; ?></td>
                                        <td><?php echo $data->Group_Asset; ?></td>
                                        <td><?php echo $data->Tarif_Penyusutan*100; ?>%</td>
                                        <td><?php echo $data->Umur; ?></td>
                                        <td><?php echo $data->Aktif; ?></td>
                                        <td class="text-center ukuran-logo">
                                            <!-- <span><a class="action-icons c-Detail" href="<?php //echo base_url('Customer/show/'.$data->IDCustomer)?>" title="Detail Data">Detail</a></span> -->
                                            <span><a class="action-icons c-edit" href="<?php echo base_url() ?>GroupAsset/edit/<?php echo $data->IDGroupAsset;?>" title="Ubah Data">Edit</a></span>
                                            <span><a class="action-icons c-delete confirm" href="#open-modal<?php echo $data->IDGroupAsset;?>" title="Hapus Data">Delete</a></span>
                                        </td>
                                    </tr>
                                    <?php
                                    $i+=1;
                                }
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
if ($groupasset=="" || $groupasset==null) {

}else{
    foreach ($groupasset as $data) {
                                # code...
        ?>
        <div id="open-modal<?php echo $data->IDGroupAsset;?>" class="modal-window">
           <div>
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Konfirmasi Hapus</h1>
                <div class="modal-body">Apakah anda yakin akan menghapus data ini ?</div>
                <div class="modal-foot">
                    <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div>
                    <div class="btn_30_blue">
                        <a href="<?php echo base_url() ?>GroupAsset/hapus_groupasset/<?php echo $data->IDGroupAsset; ?>"><span>Hapus</span></a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
    }
} 
?> 


<?php $this->load->view('administrator/footer') ; ?>