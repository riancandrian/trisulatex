<?php $this->load->view('administrator/header') ; ?>
<!-- tittle Data -->
<div class="page_title">

	<!-- notify -->
	<?php echo $this->session->flashdata('Pesan');?>
	<!-- ====== -->

	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Data Group Asset</h3>
</div>
<!-- =========== -->


<!-- body content -->
<!-- body content -->
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">

				<!-- breadcrumb -->
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<li><a href="<?php echo base_url() ?>GroupAsset">Data Group Asset</a></li>
                            <li> Edit Group Asset </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                	<div class="grid_12">
                		<span class="h_icon blocks_images"></span>
                		<h6>Edit Data Customer</h6>
                	</div>
                </div>
                <div class="widget_content">
                	<form method="post" action="<?php echo base_url() ?>GroupAsset/update" class="form_container left_label">
                         <input type="hidden" name="id" value="<?php echo $groupasset->IDGroupAsset?>">
                		<ul>
                			<li>
                				<div class="form_grid_12 multiline">

                					<!-- Kode Asset -->
                					<label class="field_title">Kode Asset</label>
                					<div class="form_input">
                						<input type="text" class="form-control" name="Kode_Asset" value="<?php echo $groupasset->Kode_Group_Asset ?>" placeholder="Kode Asset" required oninvalid="this.setCustomValidity('Kode Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                					</div>

                					<!-- Group Asset -->
                					<label class="field_title">Group Asset</label>
                					<div class="form_input">
                						<input type="text" name="Group_Asset" class="form-control" value="<?php echo $groupasset->Group_Asset ?>" placeholder="Group Asset" required oninvalid="this.setCustomValidity('Group Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                					</div>

                                      <label class="field_title">Tarif Penyusutan</label>
                                    <div class="form_input">
                                         <span class="input-group-addon" style="">% </span>
                                        <input name="tarif" type="text" id="tanpa-rupiah-debit" class="form-control price-date" required value="<?php echo convert_currency($groupasset->Tarif_Penyusutan) ?>">
                                        <!-- <span class=" label_intro">Street Address</span> -->
                                        <span class="clear"></span>
                                    </div>

                                     <label class="field_title">Umur</label>
                                    <div class="form_input">
                                        <input name="umur" type="text" class="form-control" required oninvalid="this.setCustomValidity('Umur Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $groupasset->Umur ?>">
                                        <!-- <span class=" label_intro">Street Address</span> -->
                                        <span class="clear"></span>
                                    </div>
                				</div>
                			</li>

                			<li>
                				<div class="form_grid_12">
                					<div class="form_input">
                						<div class="btn_30_light">
                							<span> <a href="<?php echo base_url() ?>groupasset" name="simpan" title=".classname">Kembali</a></span>
                						</div>
                						<div class="btn_30_blue">
                							<span><input type="submit" name="simpan" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                						</div>
                					</div>
                				</div>
                			</li>
                		</ul>
                	</form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============ -->

<?php $this->load->view('administrator/footer') ; ?>