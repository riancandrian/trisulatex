<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Invoice Pembelian</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Invoice_pembelian_umum">Data Invoice Pembelian</a></li>
              <li class="active">Ubah Invoice Pembelian</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Ubah Data Invoice Pembelian</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">
              <input id="IDFB" type="hidden" name="IDFB" value="<?php echo $detailheader->IDFBAsset ?>" class="form-control" placeholder="Discount" onkeyup="ubah_discount()">
                <!-- form header -->
                <div class="form_grid_12 w-100 alpha">

                  <!-- form 1 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Tgl Invoice</label>
                    <div class="form_input">
                      <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?php echo date('Y-m-d'); ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $detailheader->Tanggal ?>">
                    </div>
                  </div>

                  <!-- form 2 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Jatuh Tempo</label>
                    <div class="form_input">
                      <div class="form_grid_6"> 
                        <input id="Jatuh_tempo" type="text" onkeyup="hitung();" value="<?php echo $detailheader->Jatuh_Tempo ?>" name="Jatuh_tempo" class="form-control tempo" placeholder="Jatuh tempo" value="<?php echo $detailheader->Jatuh_Tempo ?>">
                      </div>
                      <div class="form_grid_6 text-right mt-dot2"> 
                        <input type="checkbox" class="checkbox" id="customize" onchange="customize_tempo()">
                        <label class="choice pr">Customize</label>
                      </div>
                    </div>
                  </div>

                  <!-- form 3 -->
                  <div class="form_grid_4 mb-1">
                    <!-- <label class="field_title mb mt-dot2">Discount</label> -->
                    <div class="form_input">
                      <div class="form_grid_6"> 
                        <input id="Discount" type="hidden" name="Discount" value="0" class="form-control" placeholder="Discount" onkeyup="ubah_discount()">
                      </div>
                      <!-- <div class="form_grid_6 text-right mt-dot2"> 
                        <input type="checkbox" class="checkbox" id="disc" onchange="discount_inv()">
                        <label class="choice pr">Discount</label>
                      </div> -->
                    </div>
                  </div>

                  <!-- form 4 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">No Invoice</label>
                    <div class="form_input input-not-focus">
                      <input type="text" name="Nomor_invoice" id="Nomor_invoice" class="form-control" placeholder="No Asset" value="<?php echo $detailheader->Nomor ?>" readonly>
                    </div>
                  </div>

                  <!-- form 5 -->
                  <?php
                  $tgl1 = date('Y-m-d');
                  $tgl2 = date('Y-m-d', strtotime('+75 days', strtotime($tgl1)));
                  ?>
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb pt">Tgl Jatuh Tempo</label>
                    <div class="form_input">
                      <input id="Tanggal_jatuh_tempo" type="date" value="<?php echo $detailheader->Tanggal_Jatuh_Tempo ?>" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" placeholder="Tanggal Jatuh tempo">
                    </div>
                  </div>

                  <!-- form 6 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Total QTY</label>
                    <div class="form_input input-not-focus">
                      <input id="total_qty" type="text" name="total_qty" class="form-control" placeholder="Total Qty" readonly>
                    </div>
                  </div>


                  <!-- form 7 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">No PB Umum</label>
                    <div class="form_input input-not-focus">
                      <input id="Nomor" type="text" name="Nomor" value="<?php echo $detailheader->Nomorpb ?>" class="form-control" placeholder="Total Qty" readonly>
                    </div>
                  </div>


                  <!-- form 10 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Supplier</label>
                    <div class="form_input">
                     <input id="NamaSupplier" type="text" name="NamaSupplier" class="form-control" placeholder="Supplier" value="<?php echo $detailheader->Nama ?>" readonly>
                     <input id="IDSupplier" type="hidden" name="IDSupplier" class="form-control" value="<?php echo $detailheader->IDSupplier ?>" placeholder="Supplier" readonly>
                      <!-- <select data-placeholder="Cari Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDSupplier"  id="IDSupplier" required oninvalid="this.setCustomValidity('Nama Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <option value=""></option>
                        <?php 
                       // foreach ($supplier as $data) {
                          ?>
                          <option value="<?php// echo $data->IDSupplier ?>"><?php //echo $data->Nama; ?></option>
                          <?php
                        //}
                        ?>
                      </select> -->
                    </div>
                  </div>

                  <!-- form 11 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Status PPN</label>
                    <div class="form_input">
                      <select style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" class="form-control" name="Status_ppn" id="Status_ppn" onchange="ubah_status_ppn()">
                        <option value="Include" <?php if($detailheader->Status_PPN=='Include'){ echo "selected"; }else{ } ?>>Include</option>
                        <option value="Exclude" <?php if($detailheader->Status_PPN=='Exclude'){ echo "selected"; }else{ } ?>>Exclude</option>
                        <option value="Nonppn" <?php if($detailheader->Status_PPN=='Nonppn'){ echo "selected"; }else{ } ?>>Non PPN</option>
                      </select>
                    </div>
                  </div>

                  <span class="clear"></span>
                </div>
              </li>
              <li>
                <div class="form_grid_12">
                  <div class="form_input ml text-center w-100">

                    <!-- <button class="btn_24_blue" type="button" id="add-to-cart">
                      Tambah Data
                    </button> -->


                  </div>
                </div>
              </li>
            </ul>


            <!-- content body tab & pane -->
            <div class="grid_12 w-100 alpha">
              <div class="widget_wrap tabby mb-4">
                <div class="widget_top">

                  <!-- button tab & pane -->
                  <div id="widget_tab">
                    <ul>
                      <li class="px py"><a href="#tab1" class="active_tab">List Barang</a></li>
                    </ul>
                  </div>
                  <!-- end button tab & pane -->

                </div>

                <!-- tab & pane body -->
                <div class="widget_content">

                  <!-- tab 1 -->
                  <div id="tab1">
                    <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                      <div class="grid_12 w-100">
                        <span class="h_icon blocks_images"></span>
                        <h6>List Barang</h6>
                      </div>
                    </div>
                    <div>
                      <table class="display data_tbl">
                        <thead>
                          <tr>
                            <th class="center" style="width: 40px">No</th>
                            <th>Asset</th>
                            <th>Qty</th>
                            
                            <th>Harga</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody id="tb_preview_penerimaan">

                        </tbody>
                      </table>
                      <ul>
                        <li>
                          <div class="form_grid_12 w-100 alpha">
                            <div class="form_grid_8">
                              <label class="field_title mb mt-dot2">Keterangan</label>
                              <div class="form_input">
                                <textarea id="Keterangan" name="Keterangan" class="form-control" placeholder="Keterangan" rows="9" style="resize: none;"><?php echo $detailheader->Keterangan ?></textarea>
                              </div>
                            </div>
                             <input type="hidden" id="Total" name="Total" class="form-control" placeholder="Total" readonly>
                            <div class="form_grid_4">
                              <div class="form_grid_12 mb-1">
                                <label class="field_title mb mt-dot2">DPP</label>
                                <div class="form_input input-not-focus">
                                  <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                                </div>
                              </div>

                              <div class="form_grid_12 mb-1">
                                <div class="form_input input-not-focus">
                                  <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" readonly>
                                </div>
                              </div>

                              <div class="form_grid_12 mb-1">
                                <label class="field_title mb mt-dot2">PPN</label>
                                <div class="form_input input-not-focus">
                                  <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" value="<?php echo $invoice->PPN ?>" readonly>
                                </div>
                              </div>

                              <div class="form_grid_12 mb-1">
                                <label class="field_title mb">Total Invoice</label>
                                <div class="form_input input-not-focus">
                                  <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                </div>
                              </div>

                            </div>

                            <!-- clear content -->
                            <div class="clear"></div>
                          </div>
                        </li>
                      </ul>

                    </div>
                  </div>


                </div>
                <!-- end tab & pane body -->

              </div>
            </div>
            <!-- end content body tab & pane -->

          </ul>
        </form>
        <div class="widget_content px-2 text-center">
          <div class="py-4 mx-2">
            <div class="btn_30_blue">
             <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
           </div>
           <div class="btn_30_blue">
            <span><a class="btn_24_blue" id="simpan" style="cursor: pointer;" onclick="saveEditInvoice()">Simpan</a></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/pembelian_umum_asset.js"></script>
<script type="text/javascript">
 var base_url = window.location.pathname.split('/');
 $("#Jatuh_tempo").prop("disabled", true);
 $("#Discount").prop("disabled", true);
 $("#namacoa").prop("disabled", true);

 $(document).ready(function(){

  if ($("#jenis3").is(":checked")) {
   $("#namacoa").prop("disabled", false);
   $("#t_jenis").val('Giro');
 }else if($("#jenis2").is(":checked")){
   $("#namacoa").prop("disabled", false);
   $("#t_jenis").val('Transfer');
 }else if($("#jenis").is(":checked")){
   $("#namacoa").prop("disabled", true);
   $("#t_jenis").val('Cash');
 }
})

 function customize_tempo(){
  if (document.getElementById('customize').checked)
  {
    $("#Jatuh_tempo").prop("disabled", false);
  } else {
    $("#Jatuh_tempo").prop("disabled", true);
  }
}

function discount_inv(){
  if (document.getElementById('disc').checked)
  {
    $("#Discount").prop("disabled", false);
  } else {
    $("#Discount").prop("disabled", true);
  }
}
function jenis_pembayaran(){
  if (document.getElementById('jenis2').checked)
  {
    $("#namacoa").prop("disabled", false);
    $("#t_jenis").val('Transfer');
  } else if (document.getElementById('jenis3').checked){
   $("#namacoa").prop("disabled", false);
   $("#t_jenis").val('Giro');
 } else if (document.getElementById('jenis').checked){
   $("#namacoa").prop("disabled", true);
   $("#t_jenis").val('Cash');
 }
}
function diskon_edit()
{
  disc=  $("#Discount").val();
  ppn=  $("#DPP").val()*0.1;
  $("#DPP").val();
  $("#PPN").val();
  $("#Discount_total").val(disc);
  total= $("#DPP").val()-ppn;
  $("#total_invoice").val(total-disc+ppn);
  $("#total_invoice_pembayaran").val(total-disc+ppn);

}

function hitung() {
  var a = new Date($("#Tanggal").val());
  var b = $("#Jatuh_tempo").val();

  var today = new Date(a.getTime()+(b*24*60*60*1000));
  var dd = today.getDate();
  var mm = today.getMonth()+1;

  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd;
  } 
  if(mm<10){
    mm='0'+mm;
  } 
  var today = yyyy+'-'+mm+'-'+dd;
  $("#Tanggal_jatuh_tempo").val(today);
}
function Penerimaan(){
   this.IDBarang;
   this.Nama_Barang;
   this.Qty;
   this.Harga_satuan;
   this.Sub_total;
   this.IDTBAsset;
 }
function hitung_total(i) {
  hargasatuan_ =$("#harga_satuan"+i).val();
  var totaldpp=0;
  var a = PM[i].Qty;
  var b = hargasatuan_.replace(/\./g,'');
  var c=a*b;
                               // var c=b;

                               PM[i].Harga_satuan = parseFloat(b);
                               PM[i].Subtotal = c;

                               $("#harga_total_b"+i).val(rupiah(c));

                               $("#harga_total"+i).val(c);


                               totaldpp += parseFloat($("#harga_total"+i).val());



                               disc = $("#Discount").val();
                               if($("#Status_ppn").val()=="Include"){
                                 if(disc == 0)
                                 {
                                   dpp=  100/110*totaldpp;
                                   ppn= dpp*0.1;
                                   $("#DPP").val(rupiah(Math.round(dpp)));
                                   $("#PPN").val(rupiah(Math.round(ppn)));
                                   $("#Discount_total").val(rupiah(disc));
                                   total= totaldpp-ppn;
                                   $("#total_invoice").val(rupiah(total-disc+ppn));
                                   $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                 }else{
                                   dpp=  100/110*(totaldpp-disc);
                                   ppn=  dpp*0.1;
                                   $("#DPP").val((Math.round(dpp)));
                                   $("#PPN").val(rupiah(Math.round(ppn)));
                                   $("#Discount_total").val(rupiah(disc));
                                   total= totaldpp-ppn;
                                   $("#total_invoice").val(rupiah(total-disc+ppn));
                                   $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                 }
                                 
                               }else if($("#Status_ppn").val()=="Nonppn"){
                                 ppn= 0;
                                 $("#DPP").val(rupiah(Math.round(totaldpp)));
                                 $("#PPN").val(rupiah(Math.round(ppn)));
                                 $("#Discount_total").val(rupiah(disc));
                                 total= totaldpp-ppn;
                                 $("#total_invoice").val(rupiah(total-disc+ppn));
                                 $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                               }else{
                                 if(disc == 0){
                                   ppn= totaldpp*0.1;
                                   $("#DPP").val(rupiah(Math.round(totaldpp)));
                                   $("#PPN").val(rupiah(Math.round(ppn)));
                                   $("#Discount_total").val(rupiah(disc));
                                   $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                                   $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                 }else{
                                   ppn= (totaldpp-disc)*0.1;
                                   $("#DPP").val(rupiah(Math.round(totaldpp)));
                                   $("#PPN").val(rupiah(Math.round(ppn)));
                                   $("#Discount_total").val(rupiah(disc));
                                   $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                                   $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                 }
                               }

                             }
                           </script>
                           <script type="text/javascript">


                             var PM = new Array();
                             var temp = [];
                             index = 0;
                             totaldpp=0;

                             var PB = document.getElementById("Nomor");
                             $('#add-to-cart').on('click', function(){
                               event.preventDefault();

                               Nomor = $('#Nomor').val();
             //alert($('#no_sjc').val());

             $.post("<?php echo base_url("Pembelian_barang_asset/check_penerimaan_barang")?>", {'Nomor' : Nomor})
             .done(function(response) {

               var json = JSON.parse(response);
                    //console.log("KUMAHA..??");
                     if (!json.error) {
                       $.each(json.data, function (key, value) {

                         $('#id_tbs_umum').val(value.IDTBSUmum);
                             // $('#no_po').val(value.NoPO);

                             if(PM == null)
                               PM = new Array();

                             var M = new Penerimaan();
                             var totalqty=0;
                             M.IDBarang = value.IDBarang;
                             M.Nama_Barang = value.Nama_Asset;
                             M.Qty = value.Qty;
                             M.IDSatuan = value.IDSatuan;
                             M.Satuan = value.Satuan;
                             M.Harga_satuan = value.Harga_Satuan;
                             M.Subtotal = value.Subtotal;
                             M.IDTBSUmum = value.IDTBSUmum;

                             PM[index] = M;
                             index++;

                             $("#tb_preview_penerimaan").html("");
                             for(i=0; i < index; i++){
                               value = "<tr>"+
                               "<td class='text-center'>"+(i+1)+"</td>"+
                               "<td>"+PM[i].Nama_Barang+"</td>"+
                               "<td>"+PM[i].Qty+"</td>"+
                               
                               "<td>"+"<input type=text value="+PM[i].Harga_Satuan+" name=harga_satuan id=harga_satuan"+i+" onkeyup=hitung_total("+i+")>"+"</td>"+
                               "<td>"+"<input value="+PM[i].Subtotal+" type=text name=harga_total id=harga_total"+i+" value=0>"+"</td>"+
                               "</tr>";
                               $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);
                               totalqty += parseInt(PM[i].Qty);
                               var a = PM[i].Qty;
                               var b = $("#harga_satuan"+i).val();
                               var c=a*b;

                               PM[i].Harga_Satuan = parseInt(b);
                               PM[i].Subtotal = c;

                               console.log(c);
                               $("#sub_total"+i).val(c);

                               $("#harga_total"+i).val(c);


                               totaldpp += parseInt($("#harga_total"+i).val());

                               disc = 0;
                               if($("#Status_ppn").val()=="Include"){
                                ppn= totaldpp*0.1;
                                $("#DPP").val(rupiah(totaldpp-ppn));
                                $("#PPN").val(rupiah(ppn));
                                $("#Discount_total").val(disc);
                                total= totaldpp-ppn;
                                $("#total_invoice").val(rupiah(total-disc+ppn));
                                $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                              }else{
                                ppn= totaldpp*0.1;
                                $("#DPP").val(rupiah(totaldpp));
                                $("#PPN").val(rupiah(ppn));
                                $("#Discount_total").val(disc);
                                $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                                $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                              }
                            }
                            $("#total_qty").val(totalqty);

                          });
                     } else {
                       alert('Tidak ada data dengan nomor Penerimaan ' + Nomor);
                     }

                   });

           });


function fieldInvoice(){
  var data1 = {
    "IDFB" : $("#IDFB").val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor_invoice":$("#Nomor_invoice").val(),
    "Nomor":$("#Nomor").val(),
    "IDSupplier":$("#IDSupplier").val(),
    "Jatuh_tempo":$("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
    "No_supplier":$("#No_supplier").val(),
    "Status_ppn":$("#Status_ppn").val(),
    "Discount":$("#Discount").val(),
    "Keterangan":$("#Keterangan").val(),
    "total_qty":$("#total_qty").val(),
    "Jatuh_tempo":$("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
  }
  return data1;
}
function fieldgrandtotal(){
  var data3 = {
    "jenis" :$("#t_jenis").val(),
    "DPP" :$("#DPP").val(),
    "Discount" :$("#Discount").val(),
    "PPN" :$("#PPN").val(),
    "total_invoice_pembayaran" :$("#total_invoice").val(),
    "nominal" :$("#nominal").val(),
    "tgl_giro" :$("#tgl_giro").val(),
    "namacoa" :$("#namacoa").val()
  }
  return data3;
}

function saveEditInvoice(){
  var data1 = fieldInvoice();
  var data2 = fieldgrandtotal();
  console.log(data1);
  // console.log(data2);

  $.ajax({

    url: "../ubah_invoice_pembelian",

    type: "POST",

    data: {

     "data1" : data1,
     "data2": data2

       // "data3" : temp

     },

     // dataType: 'json',

     success: function (msg, status) {
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      console.log(msg);
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/Pembelian_barang_asset/print_invo/'+data1['IDFB']);


    },
    error: function(msg, status, data){
      console.log(msg);
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });


}


$(document).ready(function(){
  renderJSONE($.parseJSON('<?php echo json_encode($detail) ?>'));
});
</script>
<?php $this->load->view('administrator/footer') ; ?>