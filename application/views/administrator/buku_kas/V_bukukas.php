<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3><?= $title_master ?></h3>
    <div class="top_search">

    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li class="active"><?= $title ?></li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>BukuKas/pencarian" class="form_container left_label">
                        <ul>
                            <li class="px-1">

                                <!-- search -->
                                <div class="form_grid_12 w-100 alpha">

                                    <!-- button -->
                                    <div class="form_grid_3 ">
                                        <div class="btn_24_blue">
                                            <a href="<?php echo base_url('BukuKas/create')?>"><span>Tambah Data</span></a>
                                        </div>
                                    </div>

                                    <div class="form_grid_9 ">

                                        <!-- form 1 -->
                                        <div class="form_grid_3 ">   
                                            <label class="field_title mt-dot2">Periode</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>

                                        <!-- form 2 -->
                                        <div class="form_grid_3 ">   
                                            <label class="field_title mt-dot2"> S/D </label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                            </div>
                                        </div>

                                        <div class="form_grid_3 ">   
                                            <select name="jenispencarian" data-placeholder="No Makloon Jahit" style="width: 100%!important" class="chzn-select" tabindex="13">
                                                <option value="Nomor">Nomor</option>
                                                <option value="Posisi">Posisi</option>
                                                <option value="Kode_COA">Kode COA</option>
                                                <option value="Nama_COA">Nama COA</option>
                                            </select>
                                        </div>
                                        <div class="form_grid_2">   
                                            <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
                                        </div>
                                        <div class="form_grid_1">
                                            <div class="btn_24_blue">
                                                <input type="submit" name="" value="search">
                                            </div>
                                        </div>
                                    </div>

                                    <span class="clear"></span>
                                </div>
                                <!-- end search -->

                            </li>
                        </ul>
                    </form>
                </div>
                <form action="<?php echo base_url() ?>BukuKas/delete_multiple" method="post" id="ubahstatus">
                    <div class="widget_content">
                        <table class="display" id="action_tbl">
                            <thead>
                                <tr>
                                    <th class="center">
                                        <input name="checkbox" type="checkbox" value="" class="checkall"> Do
                                    </th>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nomor</th>
                                    <th>Posisi</th>
                                    <th>Kode Perkiraan</th>
                                    <th>Nama Perkiraan</th>
                                    <th>Uang Sejumlah</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($bukukas)){ ?>
                                <?php }else{

                                    $i=1;
                                    foreach ($bukukas as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td class="center tr_select"><input type="checkbox" name="msg[]" value="<?php echo $data->IDBukuKas ?>"></td>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo DateFormat($data->Tanggal); ?></td>
                                            <td><?php echo $data->Nomor; ?></td>
                                            <td><?php echo $data->Posisi; ?></td>
                                            <td><?php echo $data->Kode_COA; ?></td>
                                            <td><?php echo $data->Nama_COA; ?></td>
                                            <td><?php echo convert_currency($data->Debet); ?></td>
                                            <td><?php echo $data->Keterangan; ?></td>
                                            <td class="text-center ukuran-logo">
                                                <span><a class="action-icons c-Detail" href="<?= site_url('BukuKas/show/'.$data->IDBukuKas); ?>" title="Detail Data">Detail</a></span>
                                                
                                                <span><a class="action-icons c-edit" href="<?= site_url('BukuKas/edit/'.$data->IDBukuKas); ?>" title="Edit Data">Edit</a></span>

                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <div class="widget_content text-right px-2">
                            <div class="py-4 mx-2">
                                <div class="btn_24_blue">
                                    <a href="#" type="button" onclick="document.getElementById('ubahstatus').submit();"><span>Batal</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('administrator/footer') ; ?>
