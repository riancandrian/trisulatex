<?php $this->load->view('administrator/header') ; 

?>
<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Tambah Data Bank</h3>
</div>
<!-- =========== -->

<!-- body content -->
<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Bank">Data Bank</a></li>
                            <li> Tambah Bank </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Data Bank</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>Bank/simpan" enctype="multipart/form-data" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                   <!-- No Rekening -->
                                    <label class="field_title">No Rekening</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" name="no_rek" placeholder="Nomor Rekening" required oninvalid="this.setCustomValidity('Nomor Rekening Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
                                    </div>

                                    <!-- Atas Nama -->
                                    <label class="field_title">Atas Nama</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" name="an" placeholder="Atas Nama" required oninvalid="this.setCustomValidity('Atas Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- COA -->
                                    <label class="field_title">COA</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih COA" name="coa" required oninvalid="this.setCustomValidity('COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""> </option>
                                            <?php foreach ($coa as $row) : ?>
                                                <option value="<?php echo $row->IDCoa ?>"><?php echo $row->Kode_COA . '-' . $row->Nama_COA ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <!-- Status -->
                                    <!-- <label class="field_title">Status</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Status" name="status" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak aktif">Tidak Aktif</option>
                                        </select>
                                    </div> -->
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                            <span> <a href="<?php echo base_url() ?>Bank" name="simpan">Kembali</a></span>
                                        </div>
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============ -->

<?php $this->load->view('administrator/footer') ; ?>