<?php $this->load->view('administrator/header') ; ?>
            <div class="content-wrapper">
                <div class="content">
                    <div class="content-header">
                        <div class="header-icon">
                            <i class="hvr-buzz-out fa fa-bank"></i>
                        </div>
                        <div class="header-title">
                            <h1>Data Bank</h1>
                            <ol class="breadcrumb">
                                <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                                <li><a href="#">Bank</a></li>
                                <li class="active">Detail Bank</li>
                            </ol>
                        </div>
                    </div> <!-- /. Content Header (Page header) -->
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="panel panel-bd">
                                 <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Detail Bank</h4>
                        </div>

                    </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="user" class="table table-bordered table-striped">
                                            <tbody> 
                                                <tr>         
                                                    <td width="35%">Nomor Rekening</td>
                                                    <td width="65%"><?php echo $banks->Nomor_Rekening?></td>
                                                </tr>
                                                <tr>         
                                                    <td>Atas Nama</td>
                                                    <td><?php echo $banks->Atas_Nama?></td>
                                                </tr>  
                                                <tr>         
                                                    <td>Kode COA</td>
                                                    <td><?php echo $banks->Kode_COA?></td>
                                                </tr>  
                                               <tr>         
                                                    <td>Nama COA</td>
                                                    <td><?php echo $banks->Nama_COA?></td>
                                                </tr> 
                                                <tr>         
                                                    <td>Status</td>
                                                    <td><?php echo $banks->Status?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>Nama Group</td>
                                                    <td><?php echo $banks->Nama_Group?></td>
                                                </tr>  
                                                 <tr>         
                                                    <td>Debit</td>
                                                    <td>Rp.<?php echo number_format($banks->Debit, 0, ',', '.'); ?></td>
                                                </tr>  
                                                <tr>         
                                                    <td>Kredit</td>
                                                    <td>Rp.<?php echo number_format($banks->Kredit, 0, ',', '.'); ?></td>
                                                </tr>  
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body__title">
                                <center>
            <a href="<?php echo base_url('Bank/index')?>"><button type="button" class="btn btn-primary">Kembali</button></a>
        </center>
        </div>
                        </div>
                    </div>
                </div> <!-- /.main content -->
            </div>
            <?php $this->load->view('administrator/footer') ; ?>