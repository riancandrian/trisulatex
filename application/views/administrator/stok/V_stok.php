<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Laporan Stok</h3>
 <div class="top_search">

 </div>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li> Laporan Stok </li>
            </ul>
          </div>
        </div>
        <?php
        $hari_ini = date("Y-m-d");
        $tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
        ?>
        <div class="widget_content">
          <form action="<?php echo base_url() ?>Stok/pencarian" method="post" class="form_container left_label">
            <ul>
              <li class="px-1">
                <div class="form_grid_12 w-100 alpha">
                  <div class="form_grid_3 mb-1">

                  </div>
                  <div class="form_grid_10 mb-1">
                    <div class="form_grid_3 mb-1">
                      <select name="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13" readonly>
                        <option value="Corak">Corak</option>
                        <option value="Warna">Warna</option>
                        <option value="Grade">Grade</option>
                      </select>
                    </div>
                    <div class="form_grid_5 mb-1">
                      <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form-control">
                    </div>
                    <div class="form_grid_1 mb-1">
                      <div class="btn_24_blue">
                        <input type="submit" name="" value="search">
                      </div>
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>
              </li>
            </ul>
          </form>

          <form action="<?php echo base_url() ?>Stok/pencariangudang" method="post" name="frmgudang" class="form_container left_label">
            <ul>
              <li class="px-1">

                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_6 mb-1">
                    <label class="field_title mt-dot2 mr" style="width: 12%;">Gudang : </label>
                    <div class="form_input" style="margin-left: 12%;">
                      <div class="form_grid_10">
                        <select name="jenisgudang" style="width: 100%!important" class="input-date-padding">
                          <option value="">Semua</option>
                          <option value="0">Trisula</option>
                          <option value="1">Agen</option>
                        </select>
                      </div>
                      <div class="form_grid_2">
                        <div class="btn_24_blue">
                          <input type="submit" value="Search">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>
              </li>
            </ul>
          </form>
        </div>
        
        <div class="widget_content">

          <table class="display" id="action_tbl">
            <!-- <table class="display data_tbl"> -->
              <thead>
               <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">CustDes/Corak</th>
                <th rowspan="2">Warna Cust</th>
                <th rowspan="2">Grade</th>
                <th colspan="2"><center>Qty</center></th>
                <th rowspan="2">Gudang</th>
              </tr>
              <tr>
                <th><center>Yrd</center></th>
                <th><center>Mtr</center></th>
              </tr>
            </thead>
            <tbody>
              <?php if(empty($stok)){ ?>
              <?php }else{
                $i=1;
                foreach ($stok as $data) {
                  if($data->Saldo_yard <= 0 && $data->Saldo_meter <= 0)
                  {

                  }else{
                  ?>
                  <tr class="odd gradeA">
                    <td class="text-center"><?php echo $i; ?></td>
                    <td><?php echo $data->Corak; ?></td>
                    <td><?php echo $data->Warna; ?></td>
                    <td><?php echo $data->Grade; ?></td>
                    <td><?php echo $data->Saldo_yard; ?></td>
                    <td><?php echo $data->Saldo_meter; ?></td>
                    <td><?php if($data->IDGudang==1){ echo "Agen"; }else{ echo "Trisula"; } ?></td>
                  </tr>
                  <?php
                }
                  $i++;
                }
              }
              ?>

            </tbody>
            <tfoot>

            </tfoot>
            <!-- </table> -->
          </table>

        </div>

      </div>
    </div>
  </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>
