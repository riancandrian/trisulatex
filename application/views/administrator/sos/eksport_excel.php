<?php
header("Content-Type:application/vnd.ms-excel");
header('Content-Disposition:attachment; filename="PO.xls"');
 ?>

 <table class="display" id="action_tbl">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nomor PO</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Supplier</th>
                                    <th>Corak</th>
                                    <th>Warna</th>
                                    <th>Merk</th>
                                    <th>Lot</th>
                                    <th>Jenis Pesanan</th>
                                    <th>PIC</th>
                                    <th>Prioritas</th>
                                    <th>Bentuk</th>
                                    <th>Panjang</th>
                                    <th>Point</th>
                                    <th>Kirim</th>
                                    <th>Stamping</th>
                                    <th>Posisi</th>
                                    <th>Posisi1</th>
                                    <th>Sample</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($po)){ ?>
                                <!-- <tr>
                                    <td colspan="7"> Data Kosong </td>
                                </tr> -->
                                <?php }else{

                                    $i=1;
                                    foreach ($po as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo DateFormat($data->tanggal); ?></td>
                                            <td><?php echo $data->nomor; ?></td>
                                            <td><?php echo DateFormat($data->tanggal_selesai); ?></td>
                                            <td><?php echo $data->namasup; ?></td>
                                            <td><?php echo $data->kode_corak; ?></td>
                                            <td><?php echo $data->warna; ?></td>
                                            <td><?php echo $data->merk; ?></td>
                                            <td><?php echo $data->lot; ?></td>
                                            <td><?php echo $data->jenis_pesanan; ?></td>
                                            <td><?php echo $data->pic; ?></td>
                                            <td><?php echo $data->prioritas; ?></td>
                                            <td><?php echo $data->bentuk; ?></td>
                                            <td><?php echo $data->panjang; ?></td>
                                            <td><?php echo $data->point; ?></td>
                                            <td><?php echo $data->kirim; ?></td>
                                            <td><?php echo $data->stamping; ?></td>
                                            <td><?php echo $data->posisi; ?></td>
                                            <td><?php echo $data->posisi1; ?></td>
                                            <td><?php echo $data->sample; ?></td>
                                            <td><?php echo $data->qty; ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>