<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Invoice Pembelian</h3>

 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>InvoicePembelian">Data Invoice Pembelian</a></li>
              <li class="active">Ubah Invoice Pembelian</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Ubah Data Invoice Pembelian</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">

                <!-- form header -->
                <div class="form_grid_12 w-100 alpha">
                  <!-- hidden input -->
                  <input type="text" name="IDFB" id="IDFB"  value="<?php echo $invoice->IDFB; ?>" hidden>

                  <!-- form 1 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Tgl Invoice</label>
                    <div class="form_input">
                      <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?php echo $invoice->Tanggal ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    </div>
                  </div>

                  <!-- form 2 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Jatuh Tempo</label>
                    <div class="form_input">
                      <div class="form_grid_6"> 
                        <input id="Jatuh_tempo" type="text" onkeyup="hitung();" name="Jatuh_tempo" class="form-control tempo" placeholder="Jatuh tempo" value="75">
                      </div>
                      <div class="form_grid_6 text-right mt-dot2"> 
                        <input type="checkbox" class="checkbox" id="customize" onchange="customize_tempo()">
                        <label class="choice pr">Customize</label>
                      </div>
                    </div>
                  </div>

                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Supplier</label>
                    <div class="form_input">
                      <select data-placeholder="Cari Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDSupplier"  id="IDSupplier" required oninvalid="this.setCustomValidity('Nama Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <?php foreach ($supplier as $row) : ?>
                          <option value="<?php echo $row->IDSupplier ?>" <?php echo $row->IDSupplier == $invoice->IDSupplier ? 'selected' : '' ?>><?php echo $row->Nama ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <!-- form 3 -->
                  <div class="form_grid_4 mb-1">
                  <!--   <label class="field_title mb mt-dot2">Discount</label> -->
                    <div class="form_input">
                      <div class="form_grid_6"> 
                        <input id="Discount" type="hidden" name="Discount" value="0" class="form-control diskon" placeholder="Discount">
                      </div>
                     <!--  <div class="form_grid_6 text-right mt-dot2"> 
                        <input type="checkbox" class="checkbox" id="disc" onchange="discount_inv()">
                        <label class="choice pr">Discount</label>
                      </div> -->
                    </div>
                  </div>

                  <!-- form 4 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">No Invoice</label>
                    <div class="form_input">
                      <input type="text" name="Nomor_invoice" id="Nomor_invoice" class="form-control" placeholder="Nomor Invoice" value="<?php echo $invoice->Nomor; ?>">
                    </div>
                  </div>

                  <!-- form 5 -->
                  <?php
                  $tgl1 = date('Y-m-d');
                  $tgl2 = date('Y-m-d', strtotime('+75 days', strtotime($tgl1)));
                  ?>
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Tgl Jatuh Tempo</label>
                    <div class="form_input">
                      <input id="Tanggal_jatuh_tempo" type="date" value="<?php echo $invoice->Tanggal_jatuh_tempo; ?>" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" placeholder="Tanggal Jatuh tempo">
                    </div>
                  </div>

                  <!-- form 6 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Total QTY Yard</label>
                    <div class="form_input input-not-focus">
                      <input id="Total_qty_yard" type="text" name="Total_qty_yard" class="form-control" placeholder="Total Qty Yard" value="<?php echo $invoice->Total_qty_yard ?>" readonly>
                    </div>
                  </div>

                  <!-- form 7 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb pt">
                      <select name="no_penerimaan" id="no_penerimaan" style="width: 100%!important; padding: 3px 2px;border-color: #d8d8d8;color: #999999;">
                        <option value="Nomor">No PB</option>
                        <option value="Nomor">No PBNT</option>
                      </select>
                    </label>
                    <div class="form_input">
                      <input id="Nomor" type="text" name="Nomor" class="form-control" value="<?php echo $invoice->NomorTBS ?>">
                    </div>
                  </div>

                  <!-- form 8 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">No SJ Supplier</label>
                    <div class="form_input">
                      <input id="No_supplier" type="text" name="No_supplier" class="form-control" placeholder="No Supplier" value="<?php echo $invoice->No_sj_supplier ?>">
                    </div>
                  </div>

                  <!-- form 9 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Total QTY Meter</label>
                    <div class="form_input input-not-focus">
                      <input id="Total_qty_meter" type="text" name="Total_qty_meter" class="form-control" placeholder="Total Qty Meter" value="<?php echo $invoice->Total_qty_meter ?>" readonly>
                    </div>
                  </div>

                  <!-- form 10 -->
                  

                  <!-- form 11 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Status PPN</label>
                    <div class="form_input">
                      <select style=" width:100%;" class="form-control" tabindex="13"  name="Status_ppn" id="Status_ppn" onchange="ubah_status_ppn()">

                        <?php if($invoice->Status_ppn=="Include"){ ?>
                        <option value="Include" selected>Include</option>
                        <option value="Exclude">Exclude</option>
                         <option value="Nonppn">Non PPN</option>

                        <?php }elseif($invoice->Status_ppn=="Exclude"){ ?>
                        <option value="Include">Include</option>
                        <option value="Exclude" selected>Exclude</option>
                         <option value="Nonppn">Non PPN</option>

                        <?php }elseif($invoice->Status_ppn=="Nonppn"){ ?>
                        <option value="Include">Include</option>
                        <option value="Exclude">Exclude</option>
                        <option value="Nonppn" selected>Non PPN</option>

                        <?php } else {?>
                        <option value="Include">Include</option>
                        <option value="Exclude">Exclude</option>
                        <option value="Nonppn">Non PPN</option>

                        <?php }  ?>
                      </select>
                    </div>
                  </div>

                  <span class="clear"></span>
                </div>
              </li>
              <!-- <li>
                <div class="form_grid_12">
                  <div class="form_input"> -->
                    <!--   <div class="btn_30_light">
                      <span> <a href="<?php //echo base_url() ?>Piutang" name="simpan">Kembali</a></span>
                    </div> -->
                      <!-- <button class="btn_30_blue" type="button" id="add-to-cart">
                      Tambah Data
                    </button> -->


                  <!-- </div>
                </div>
              </li> -->

              <!-- content body tab & pane -->
              <div class="grid_12 w-100 alpha">
                <div class="widget_wrap tabby mb-4">
                  <div class="widget_top">

                    <!-- button tab & pane -->
                    <div id="widget_tab">
                      <ul>
                        <li class="px py"><a href="#tab1" class="active_tab">List Barang</a></li>
                      <!--   <li class="px py"><a href="#tab2">Pembayaran</a></li> -->
                      </ul>
                    </div>
                    <!-- end button tab & pane -->

                  </div>

                  <!-- tab & pane body -->
                  <div class="widget_content">

                    <!-- tab 1 -->
                    <div id="tab1">
                      <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                        <div class="grid_12 w-100">
                          <span class="h_icon blocks_images"></span>
                          <h6>List Barang</h6>
                        </div>
                      </div>
                      <div>
                        <table class="display data_tbl">
                          <thead>
                            <tr>
                              <th class="center" style="width: 40px">No</th>
                              <th>Barcode</th>
                              <th class="hidden-xs">Corak</th>
                              <th>Warna</th>
                              <th>Merek</th>
                              <th>Qty Yard</th>
                              <th>Qty Meter</th>
                              <th>Satuan</th>
                              <th>Harga Satuan</th>
                              <th style="width: 50px">Total</th>
                            </tr>
                          </thead>
                          <tbody id="tb_preview_penerimaan">

                          </tbody>
                        </table>
                        <ul>
                          <li>
                            <div class="form_grid_12 w-100 alpha">
                              <div class="form_grid_8">
                                <label class="field_title mb mt-dot2">Keterangan</label>
                                <div class="form_input">
                                  <textarea id="Keterangan" name="Keterangan" class="form-control" placeholder="Keterangan" rows="9" style="resize: none;"><?php echo $invoice->Keterangan ?></textarea>
                                </div>
                              </div>
                              <div class="form_grid_4">

                              
                                    <input type="hidden" id="Total" name="Total" class="form-control" placeholder="Total" readonly>
                                  

                                <div class="form_grid_12 mb-1">
                                  <label class="field_title mb mt-dot2">DPP</label>
                                  <div class="form_input input-not-focus">
                                    <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                                  </div>
                                </div>

                                <div class="form_grid_12 mb-1">
                                  <label class="field_title mb mt-dot2">Discount</label>
                                  <div class="form_input input-not-focus">
                                    <input type="text" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" value="<?php echo $invoice->Discount ?>" readonly>
                                  </div>
                                </div>

                                <div class="form_grid_12 mb-1">
                                  <label class="field_title mb mt-dot2">PPN</label>
                                  <div class="form_input input-not-focus">
                                    <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" readonly>
                                  </div>
                                </div>

                                <div class="form_grid_12 mb-1">
                                  <label class="field_title mb">Total Invoice</label>
                                  <div class="form_input input-not-focus">
                                    <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                  </div>
                                </div>

                              </div>

                              <!-- clear content -->
                              <div class="clear"></div>
                            </div>
                          </li>
                        </ul>

                      </div>
                    </div>
                    <!-- end tab 1 -->

                    <!-- tab 2 -->
                   <!--  <div id="tab2">
                      <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                        <div class="grid_12 w-100">
                          <span class="h_icon blocks_images"></span>
                          <h6>Pembayaran</h6>
                        </div>
                      </div>

                      <ul>
                        <li>
                          <div class="form_grid_12 w-100 alpha">

                            <div class="form_grid_6 mb-1">
                              <label class="field_title mb mt-dot2">Total Invoice</label>
                              <div class="form_input input-not-focus">
                                <input type="text" id="total_invoice_pembayaran" name="total_invoice" class="form-control" placeholder="Total Invoice" value="<?php echo $invoice->Grand_total ?>" readonly>
                              </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                              <label class="field_title mb mt-dot2">Nama COA</label>
                              <div class="form_input input-disabled">
                              <?php if($invoice->Jenis_pembayaran=="Cash"){
                                ?>
                                   <select name="namacoa" id="namacoa" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem" readonly>
                                 <option value="-">-</option>
                                  ?>
                                </select>
                                <?php
                              }else{ ?>
                                <select name="namacoa" id="namacoa" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem">
                                  <?php foreach ($bank as $row) : ?>
                          <option value="<?php echo $row->IDCoa ?>" <?php echo $row->IDCoa == $invoice->IDCOA ? 'selected' : '' ?>><?php echo $row->Nama_COA ?></option>
                        <?php endforeach; 

                      }?>
                                </select>
                              </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                              <label class="field_title mb mt-dot2">Jenis Pembayaran</label>
                              <div class="form_input">
                                <div class="mb-1">
                                  <span>
                                    <input type="radio" class="radio" name="jenis" id="jenis" value="Cash" <?php if($invoice->Pembayaran=="Cash"){echo 'checked';} ?> onclick="jenis_pembayaran()"> 
                                    <label class="choice">Cash</label>
                                  </span>
                                </div>
                                <div class="mb-1">
                                  <span>
                                    <input type="radio" class="radio" name="jenis" id="jenis2" value="Transfer" <?php if($invoice->Pembayaran=="Transfer"){echo 'checked';} ?> onclick="jenis_pembayaran()">
                                    <label class="choice">Transfer</label>
                                  </span>
                                </div>
                                <div class="mb-1">
                                  <span>
                                    <input type="radio" class="radio" name="jenis" id="jenis3" value="Giro" <?php if($invoice->Pembayaran=="Giro"){echo 'checked';} ?> onclick="jenis_pembayaran()">
                                    <label class="choice">Giro</label>
                                  </span>
                                </div>
                              </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                              <label class="field_title mb mt-dot2">Tgl Giro</label>
                              <div class="form_input">
                                <input type="date" id="tgl_giro" name="tgl_giro" class="input-date-padding-3-2 form-control" value="<?php echo $invoice->Tanggal_Giro ?>" placeholder="Tanggal Giro">
                              </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                              <label class="field_title mb mt-dot2">Nominal</label>
                              <div class="form_input">
                                <input type="text" id="nominal" name="nominal" class="form-control" value="<?php echo $invoice->NominalPembayaran ?>">
                              </div>
                            </div>

                            <span class="clear"></span>
                          </div>
                        </li>
                      </ul>

                    </div> -->
                    <!-- end tab 2 -->

                  </div>
                  <!-- end tab & pane body -->

                </div>
              </div>
              <!-- end content body tab & pane -->

            </ul>
          </form>
          <div class="widget_content px-2 text-center">
          <div class="py-4 mx-2">
              <div class="btn_30_blue">
                 <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
              </div>
              <div class="btn_30_blue">
                <span><a class="btn_24_blue" id="simpan" style="cursor: pointer;" onclick="saveEditInvoice()">Simpan</a></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/invoice_pembelian.js"></script>
<script type="text/javascript">
     var base_url = window.location.pathname.split('/');
  $("#Jatuh_tempo").prop("disabled", true);
  $("#Discount").prop("disabled", true);
  $("#namacoa").prop("disabled", true);

  $(document).ready(function(){

    if ($("#jenis3").is(":checked")) {
     $("#namacoa").prop("disabled", false);
   }else if($("#jenis2").is(":checked")){
     $("#namacoa").prop("disabled", false);
   }else if($("#jenis").is(":checked")){
     $("#namacoa").prop("disabled", true);
   }
 })

  function customize_tempo(){
    if (document.getElementById('customize').checked)
    {
      $("#Jatuh_tempo").prop("disabled", false);
    } else {
      $("#Jatuh_tempo").prop("disabled", true);
    }
  }

  function discount_inv(){
    if (document.getElementById('disc').checked)
    {
      $("#Discount").prop("disabled", false);
    } else {
      $("#Discount").prop("disabled", true);
    }
  }
  function jenis_pembayaran(){
    if (document.getElementById('jenis2').checked)
    {
      $("#namacoa").prop("disabled", false);
    } else if (document.getElementById('jenis3').checked){
     $("#namacoa").prop("disabled", false);
   } else if (document.getElementById('jenis').checked){
     $("#namacoa").prop("disabled", true);
   }
 }
function diskon_edit()
{
  disc = $("#Discount").val();
 if($("#Status_ppn").val()=="Include"){
  ppn= $("#Total").val()*0.1;
  $("#DPP").val(rupiah($("#Total").val()-ppn));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= $("#Total").val()-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}else if($("#Status_ppn").val()=="Nonppn"){
  ppn= 0;
  $("#DPP").val(rupiah($("#Total").val()));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  total= $("#Total").val()-ppn;
  $("#total_invoice").val(rupiah(total-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}else{
  ppn= $("#Total").val()*0.1;
  $("#DPP").val(rupiah($("#Total").val()));
  $("#PPN").val(rupiah(ppn));
  $("#Discount_total").val(rupiah(disc));
  $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
  $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
}
}

function hitung() {
  var a = new Date($("#Tanggal").val());
  var b = $("#Jatuh_tempo").val();

  var today = new Date(a.getTime()+(b*24*60*60*1000));
  var dd = today.getDate();
  var mm = today.getMonth()+1;

  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd;
  } 
  if(mm<10){
    mm='0'+mm;
  } 
  var today = yyyy+'-'+mm+'-'+dd;
  $("#Tanggal_jatuh_tempo").val(today);
}

function hitung_total(i) {
  var a = PM[i].Qty_yard;
  var b = $("#harga_satuan"+i).val();
  var c=a*b;
  $("#harga_total"+i).val(c);
  totaldpp=0;
  for(l=0; l < index; l++){

   totaldpp += parseFloat($("#harga_total"+l).val());

 }
 $("#Total").val(totaldpp);
 disc=  $("#Discount").val();
 if($("#Status_ppn").val()=="Include"){
  ppn= totaldpp*0.1;
  $("#DPP").val(totaldpp-ppn);
  $("#PPN").val(ppn);
  $("#Discount_total").val(disc);
  total= totaldpp-ppn;
  $("#total_invoice").val(total-disc+ppn);
  $("#total_invoice_pembayaran").val(total-disc+ppn);
}else{
  ppn= 0;
  $("#DPP").val(totaldpp);
  $("#PPN").val(ppn);
  $("#Discount_total").val(disc);
  $("#total_invoice").val(totaldpp-disc+ppn);
  $("#total_invoice_pembayaran").val(totaldpp-disc+ppn);
}

}
</script>
<script type="text/javascript">


 var PM = new Array();
 var temp = [];
 index = 0;
 totaldpp=0;

 var PB = document.getElementById("Nomor");
 $('#add-to-cart').on('click', function(){
   event.preventDefault();

   Nomor = $('#Nomor').val();
             //alert($('#no_sjc').val());

             $.post("<?php echo base_url("InvoicePembelian/check_penerimaan_barang")?>", {'Nomor' : Nomor})
             .done(function(response) {

               var json = JSON.parse(response);
                     //console.log(json);
                     if (!json.error) {
                       $.each(json.data, function (key, value) {

                             // $('#no_so').val(value.NoSO);
                             // $('#no_po').val(value.NoPO);

                             if(PM == null)
                               PM = new Array();
                             totalqtyyard=0;
                             totalqtymeter=0;

                             var M = new Penerimaan();

                             M.Barcode = value.Barcode;
                             M.Corak = value.Corak;
                             M.Warna = value.Warna;
                             M.Merk = value.Merk;
                             M.Qty_yard = value.Qty_yard;
                             M.Qty_meter = value.Qty_meter;
                             M.Grade = value.Grade;
                             M.Satuan = value.Satuan;
                             M.NoPO = value.NoPO;
                             M.NoSO = value.NoSO;
                             M.Party = value.Party;
                             M.Indent = value.Indent;
                             M.Lebar = value.Lebar;
                             M.Remark = value.Remark;
                             // M.NoSJ = no_sjc;
                             // M.Tanggal = $('#tanggal').val();
                             // M.ID_supplier = $('#id_supplier').val();
                             // M.NoPB = $('#no_bo').val();
                             // M.Keterangan = $('#keterangan').val();

                             PM[index] = M;
                             index++;

                             $("#tb_preview_penerimaan").html("");
                             for(i=0; i < index; i++){
                               value = "<tr>"+
                               "<td class='text-center'>"+(i+1)+"</td>"+
                               "<td>"+PM[i].Barcode+"</td>"+
                               "<td>"+PM[i].Corak+"</td>"+
                               "<td>"+PM[i].Warna+"</td>"+
                               "<td>"+PM[i].Merk+"</td>"+
                               "<td>"+PM[i].Qty_yard+"</td>"+
                               "<td class='text-center'>"+PM[i].Qty_meter+"</td>"+
                               "<td>"+PM[i].Satuan+"</td>"+
                               "<td>"+"<input type=text name=harga_satuan id=harga_satuan"+i+" onkeyup=hitung_total("+i+")>"+"</td>"+
                               "<td>"+"<input type=text name=harga_total id=harga_total"+i+" value=0>"+"</td>"+
                               "</tr>";
                               $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);
                               totalqtyyard+= parseFloat(PM[i].Qty_yard);
                               totalqtymeter+= parseFloat(PM[i].Qty_meter);
                             }
                             $("#Total_qty_yard").val(totalqtyyard);
                             $("#Total_qty_meter").val(totalqtymeter);

                           });
                     } else {
                       alert('Tidak ada data dengan nomor Penerimaan ' + Nomor);
                     }

                   });

           });


 function fieldInvoice(){
  var data1 = {
    "IDFB" : $("#IDFB").val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor_invoice":$("#Nomor_invoice").val(),
    "Nomor":$("#Nomor").val(),
    "IDSupplier":$("#IDSupplier").val(),
    "Jatuh_tempo":$("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
    "No_supplier":$("#No_supplier").val(),
    "Status_ppn":$("#Status_ppn").val(),
    "Discount":$("#Discount").val(),
    "Keterangan":$("#Keterangan").val(),
    "Total_qty_yard":$("#Total_qty_yard").val(),
    "Total_qty_meter":$("#Total_qty_meter").val(),
  }
  return data1;
}
function fieldgrandtotal(){
  var data3 = {
    "jenis" :$("#jenis").val(),
    "jenis" :$("#jenis2").val(),
    "jenis" :$("#jenis3").val(),
    "DPP" :$("#DPP").val(),
    "Discount" :$("#Discount").val(),
    "PPN" :$("#PPN").val(),
    "total_invoice_pembayaran" :$("#total_invoice_pembayaran").val(),
    "nominal" :$("#nominal").val(),
    "tgl_giro" :$("#tgl_giro").val(),
    "namacoa" :$("#namacoa").val()
  }
  return data3;
}

function saveEditInvoice(){
  var data1 = fieldInvoice();
  var data2 = fieldgrandtotal();
  console.log(data1);
  console.log(data2);

  $.ajax({

    url: "../ubah_invoice_pembelian",

    type: "POST",

    data: {

     "data1" : data1,
     "data2": data2

       // "data3" : temp

     },

     // dataType: 'json',

     success: function (msg, status) {
      $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
      
      document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
      document.getElementById("simpan").setAttribute("onclick", "return false;");
      document.getElementById("print").setAttribute("style", "cursor: pointer;");
      document.getElementById("print").setAttribute("onclick", "return true;");
      document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/InvoicePembelian/print/'+data1['IDFB']);

    },
    error: function(msg, status, data){
      $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

    }

  });


}


$(document).ready(function(){
  renderJSONE($.parseJSON('<?php echo json_encode($detail) ?>'));
});
</script>
<?php $this->load->view('administrator/footer') ; ?>