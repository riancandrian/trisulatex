 <?php $this->load->view('administrator/header') ; 

 if ($kodenoinv->curr_number == null) {
  $number = 1;
} else {
  $number = $kodenoinv->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_no_inv = 'INV'.date('y').str_replace(' ', '',$agen).$kodemax;
?>
<style type="text/css">
.chzn-container {
  width: 102.5% !important;
}
</style>
<div class="page_title">
  <?php echo $this->session->flashdata('Pesan');?>
  <span class="title_icon"><span class="blocks_images"></span></span>
  <h3>Data Invoice Pembelian</h3>

  <div class="top_search">
  </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>InvoicePembelian">Data Invoice Pembelian</a></li>
              <li class="active">Tambah Invoice Pembelian</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Invoice Pembelian</h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">

                <!-- form header -->
                <div class="form_grid_12 w-100 alpha">

                  <!-- form 1 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">Tgl Invoice</label>
                    <div class="form_input">
                      <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?php echo date('Y-m-d'); ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    </div>
                  </div>

                  <!-- form 2 -->
                  <div class="form_grid_4">
                    <label class="field_title mb mt-dot2">Jatuh Tempo</label>
                    <div class="form_input">
                      <!-- <div class="form_grid_12">  -->
                        <div class="form_grid_6 text-right mt-dot2">
                          <input id="Jatuh_tempo" type="text" onkeyup="hitung();" name="Jatuh_tempo" class="form-control tempo" placeholder="Jatuh tempo" value="75">
                        </div>
                        <div class="form_grid_6 text-right mt-dot2"> 
                          <input type="checkbox" class="checkbox" id="customize" onchange="customize_tempo()">
                          <label class="choice pr">Customize</label>
                        </div> 
                      </div>
                      <!-- </div> -->
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_4">
                     <!--  <label class="field_title mb mt-dot2">Discount</label> -->
                     <div class="form_input">
                      <div class="form_grid_6"> 
                        <input id="Discount" type="hidden" name="Discount" value="0" class="form-control" placeholder="Discount" onkeyup="ubah_discount()">
                      </div>
                       <!--  <div class="form_grid_6 text-right mt-dot2"> 
                          <input type="checkbox" class="checkbox" id="disc" onchange="discount_inv()">
                          <label class="choice pr">Discount</label>
                        </div> -->
                      </div>
                    </div>

                    
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mb mt-dot2">Supplier</label>
                      <div class="form_input">
                        <input id="NamaSupplier" type="text" name="NamaSupplier" class="form-control" placeholder="Supplier" readonly>
                        <input id="IDSupplier" type="hidden" name="IDSupplier" class="form-control" placeholder="Supplier" readonly>
                      <!-- <select data-placeholder="Cari Supplier" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDSupplier"  id="IDSupplier" required oninvalid="this.setCustomValidity('Nama Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        <option value=""></option>
                        <?php 
                       // foreach ($supplier as $data) {
                          ?>
                          <option value="<?php// echo $data->IDSupplier ?>"><?php //echo $data->Nama; ?></option>
                          <?php
                        //}
                        ?>
                      </select> -->
                    </div>
                  </div>
                  
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb mt-dot2">No Invoice</label>
                    <div class="form_input">
                      <input type="text" name="Nomor_invoice" id="Nomor_invoice" class="form-control" placeholder="No Asset" value="<?php echo $code_no_inv; ?>">
                    </div>
                  </div>

                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Status PPN</label>
                    <div class="form_input">
                      <select style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" class="form-control" name="Status_ppn" id="Status_ppn" onchange="ubah_status_ppn()">
                        <option value="Include">Include</option>
                        <option value="Exclude" selected>Exclude</option>
                        <option value="Nonppn">Non PPN</option>
                      </select>
                    </div>
                  </div>
                  <!-- form 4 -->

                  

                  <!-- form 5 -->
                  <?php
                  $tgl1 = date('Y-m-d');
                  $tgl2 = date('Y-m-d', strtotime('+75 days', strtotime($tgl1)));
                  ?>
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Tgl Jatuh Tempo</label>
                    <div class="form_input">
                      <input id="Tanggal_jatuh_tempo" type="date" value="<?php echo $tgl2; ?>" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" placeholder="Tanggal Jatuh tempo">
                    </div>
                  </div>
                  
                  <div class="form_grid_4 mb-1">
                    <div class="form_grid_12">
                      <label class="field_title mb pt">
                        <select name="no_penerimaan" id="no_penerimaan" style="width: 100%!important; padding: 3px 2px;border-color: #d8d8d8;color: #999999;" onchange="cek_nomor_pb()">
                          <option value="PB">No PB</option>
                          <option value="PBU">No PBNT</option>

                        </select>
                      </label>
                      <div class="form_input">
                        <select name="Nomor" id="Nomor" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <option value="">--Silahkan Pilih PB Dahulu--</option>
                        </select>
                      </div>
                    </div>
                  </div>



                  <!-- form 7 -->


                  <!-- form 8 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb" style="padding: 0!important">No SJ Supplier</label>
                    <div class="form_input">
                      <input id="No_supplier" type="text" name="No_supplier" class="form-control" placeholder="No Supplier">
                    </div>
                  </div>
                  
                  <!-- form 6 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Total QTY Yard</label>
                    <div class="form_input input-not-focus">
                      <input id="Total_qty_yard" type="text" name="Total_qty_yard" class="form-control" placeholder="Total Qty Yard" readonly>
                    </div>
                  </div>

                  <!-- form 9 -->
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mb">Total QTY Meter</label>
                    <div class="form_input input-not-focus">
                      <input id="Total_qty_meter" type="text" name="Total_qty_meter" class="form-control" placeholder="Total Qty Yard" readonly>
                    </div>
                  </div>

                  <!-- form 10 -->


                  <!-- form 11 -->
                  

                  <span class="clear"></span>
                </div>
              </li>
              <li>
                <div class="form_grid_12">
                  <div class="form_input ml text-center w-100">

                    <!-- <button class="btn_24_blue" type="button" id="add-to-cart">
                      Tambah Data
                    </button> -->


                  </div>
                </div>
              </li>
            </ul>

            <!-- content body tab & pane -->
            <div class="grid_12 mx w-100">
              <div class="widget_wrap tabby mb-4">
                <div class="widget_top">

                  <!-- button tab & pane -->
                  <div id="widget_tab">
                    <ul>
                      <li class="px py"><a href="#tab1" class="active_tab">List Barang</a></li>
                      <!-- <li class="px py"><a href="#tab2">Pembayaran</a></li> -->
                    </ul>
                  </div>
                  <!-- end button tab & pane -->

                </div>

                <!-- tab & pane body -->
                <div class="widget_content">

                  <!-- tab 1 -->
                  <div id="tab1">
                    <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                      <div class="grid_12 w-100">
                        <span class="h_icon blocks_images"></span>
                        <h6>List Barang</h6>
                      </div>
                    </div>
                    <div>
                      <table class="display data_tbl" id="tabelfsdfsf">
                        <thead>
                          <tr>
                            <th class="center" style="width: 40px">No</th>
                            <th>Barcode</th>
                            <th class="hidden-xs">Corak</th>
                            <th>Warna</th>
                            <th>Merek</th>
                            <th>Qty Yard</th>
                            <th>Qty Meter</th>
                            <th>Satuan</th>
                            <th style="width: 120px">Harga Satuan</th>
                            <th>Total Harga</th>
                          </tr>
                        </thead>
                        <tbody id="tb_preview_penerimaan">

                        </tbody>
                      </table>
                      <ul>
                        <li>
                          <div class="form_grid_12 w-100 alpha">
                            <div class="form_grid_8">
                              <label class="field_title mb mt-dot2">Keterangan</label>
                              <div class="form_input">
                                <textarea id="Keterangan" name="Keterangan" class="form-control" placeholder="Keterangan" rows="9" style="resize: none;"></textarea>
                              </div>
                            </div>
                            <div class="form_grid_4">
                              <input type="hidden" id="Total" name="Total" class="form-control" placeholder="Total" readonly>
                              <div class="form_grid_12 mb-1">
                                <label class="field_title mb mt-dot2">DPP</label>
                                <div class="form_input input-not-focus">
                                  <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                                </div>
                              </div>

                              <div class="form_grid_12 mb-1">
                                <!--   <label class="field_title mb mt-dot2">Discount</label> -->
                                <div class="form_input input-not-focus">
                                  <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" readonly>
                                </div>
                              </div>

                              <div class="form_grid_12 mb-1">
                                <label class="field_title mb mt-dot2">PPN</label>
                                <div class="form_input input-not-focus">
                                  <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" readonly>
                                </div>
                              </div>

                              <div class="form_grid_12 mb-1">
                                <label class="field_title mb">Total Invoice</label>
                                <div class="form_input input-not-focus">
                                  <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                </div>
                              </div>

                            </div>

                            <!-- clear content -->
                            <div class="clear"></div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- end tab 1 -->

                  <!-- tab 2 -->
                  <!-- <div id="tab2">
                    <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                      <div class="grid_12 w-100">
                        <span class="h_icon blocks_images"></span>
                        <h6>Warna</h6>
                      </div>
                    </div>

                    <ul>
                      <li>
                        <div class="form_grid_12 w-100 alpha">
                          <div class="form_grid_6 mb-1">
                            <label class="field_title mb mt-dot2">Total Invoice</label>
                            <div class="form_input input-not-focus">
                              <input type="text" id="total_invoice_pembayaran" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                            </div>
                          </div>

                          <div class="form_grid_6 mb-1">
                            <label class="field_title mb mt-dot2">Nama COA</label>
                            <div class="form_input input-disabled">
                              <select data-placeholder="Cari COA" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem" tabindex="13"  name="namacoa"  id="namacoa" required oninvalid="this.setCustomValidity('COA Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                <?php 
                                //foreach ($bank as $data) {
                                  ?>
                                  <option value="<?php //echo $data->IDCoa ?>"><?php //echo $data->Nama_COA; ?></option>
                                  <?php
                              //  }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="form_grid_6 mb-1">
                          <label class="field_title mb mt-dot2">Jenis Pembayaran</label>
                          <div class="form_input">
                            <div class="mb-1">
                              <span>
                                <input type="radio" class="radio" name="jenis" id="jenis" value="Cash" onclick="jenis_pembayaran()"> 
                                <label class="choice">Cash</label>
                              </span>
                            </div>
                            <div class="mb-1">
                              <span>
                                <input type="radio" class="radio" name="jenis" id="jenis2" value="Transfer" onclick="jenis_pembayaran()">
                                <label class="choice">Transfer</label>
                              </span>
                            </div>
                            <div class="mb-1">
                              <span>
                                <input type="radio" class="radio" name="jenis" id="jenis3" value="Giro" onclick="jenis_pembayaran()">
                                <label class="choice">Giro</label>
                              </span>
                            </div>
                          </div>
                        </div>

                        <div class="form_grid_6 mb-1">
                          <label class="field_title mb mt-dot2">Tgl Giro</label>
                          <div class="form_input">
                            <input type="date" id="tgl_giro" name="tgl_giro" class="input-date-padding-3-2 form-control" value="<?php echo date('Y-m-d'); ?>" placeholder="Tanggal Giro">
                          </div>
                        </div>

                        <div class="form_grid_6 mb-1">
                          <label class="field_title mb mt-dot2">Nominal</label>
                          <div class="form_input">
                            <input type="text" id="nominal" name="nominal" class="form-control" value="0">
                          </div>
                        </div>

                        <span class="clear"></span>
                      </div> -->
                    </li>
                  </ul>
                </div>
                <!-- end tab 2 -->

              </div>
              <!-- end tab & pane body -->

            </div>
          </div>
          <!-- end content body tab & pane --> 

            <!-- <div>
              <table class="display data_tbl">
                <thead>
                  <tr>
                    <th class="center" style="width: 40px">No</th>
                    <th>Barcode</th>
                    <th class="hidden-xs">Corak</th>
                    <th>Warna</th>
                    <th>Merek</th>
                    <th>Qty Yard</th>
                    <th>Qty Meter</th>
                    <th>Satuan</th>
                    <th>Harga Satuan</th>
                    <th style="width: 50px">Total</th>
                  </tr>
                </thead>
              <tbody id="tb_preview_penerimaan">
              </tbody>
            </table>
              <input type="text" id="Total_nilai_perolehan" name="Total_nilai_perolehan" class="form-control" placeholder="Keterangan">
              <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP">
              <input type="text" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount">
              <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN">
              <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice">
            </div> -->
          </form>
          <div class="widget_content px-2 text-center">
            <div class="py-4 mx-2">
              <!-- <div class="btn_30_blue">
                <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
              </div> -->
              <div class="btn_30_blue">
                <span><a class="btn_24_blue" id="simpan" onclick="saveInvoice()" style="cursor: pointer;">Simpan</a></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
  var base_url = window.location.pathname.split('/');
  $("#Jatuh_tempo").prop("disabled", true);
  $("#Discount").prop("disabled", true);
  $("#namacoa").prop("disabled", true);

  function customize_tempo(){
    if (document.getElementById('customize').checked)
    {
      $("#Jatuh_tempo").prop("disabled", false);
    } else {
      $("#Jatuh_tempo").prop("disabled", true);
    }
  }

  function discount_inv(){
    if (document.getElementById('disc').checked)
    {
      $("#Discount").prop("disabled", false);
    } else {
      $("#Discount").prop("disabled", true);
    }
  }
  function jenis_pembayaran(){
    if (document.getElementById('jenis2').checked)
    {
      $("#namacoa").prop("disabled", false);
    } else if (document.getElementById('jenis3').checked){
      $("#namacoa").prop("disabled", false);
    } else if (document.getElementById('jenis').checked){
      $("#namacoa").prop("disabled", true);
    }
  }
  function hitung() {
    var a = new Date($("#Tanggal").val());
    var b = $("#Jatuh_tempo").val();

    var today = new Date(a.getTime()+(b*24*60*60*1000));
    var dd = today.getDate();
    var mm = today.getMonth()+1;

    var yyyy = today.getFullYear();
    if(dd<10){
      dd='0'+dd;
    } 
    if(mm<10){
      mm='0'+mm;
    } 
    var today = yyyy+'-'+mm+'-'+dd;
    $("#Tanggal_jatuh_tempo").val(today);
  }
</script>
<script type="text/javascript">
  function Penerimaan(){
    this.Barcode;
    this.Corak;
    this.Warna;
    this.Merk;
    this.Qty_yard;
    this.Qty_meter;
    this.Grade;
    this.Satuan;
    this.NoPO;
    this.NoSO;
    this.Party;
    this.Indent;
    this.Lebar;
    this.Tanggal;
    this.NoPB;
    this.ID_supplier;
    this.NoSJ;
    this.Remark;
    this.Keterangan;
    this.Total_yard;
    this.Total_meter;
    this.Harga;
    this.Sub_total;
    this.IDSupplier;
    this.Nama;
  }

  var PM = new Array();
  index = 0;
  totaldpp=0;

  function cek_nomor_pb() {
    var penerimaan= $("#no_penerimaan").val();
    console.log(penerimaan);
    $.ajax({
      url : '/'+base_url[1]+'/InvoicePembelian/getnomorpb',
      type: "POST",
      data: {jenis_tbs:penerimaan},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0 || data == null || data =="") {
          html = '<option value="">--Data Kosong--</option>';
        } else {
          html += '<option value="">--Silahkan Pilih PB--</option>';
          for (var i = 0 ; i < data.length; i++) {
            html +='<option value="'+data[i]["Nomor"]+'"> '+data[i]["Nomor"]+' </option>';
          }

        }
        $("#Nomor").html(html);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
      }
    });
  }
  $(document).ready(function(){
    var PB = document.getElementById("Nomor");
    $('#Nomor').change(function(){
      Nomor = $('#Nomor').val();
             //alert($('#no_sjc').val());

             $.post("<?php echo base_url("InvoicePembelian/check_penerimaan_barang")?>", {'Nomor' : Nomor})
             .done(function(response) {

               var json = JSON.parse(response);
                     //console.log(json);
                     if (!json.error) {
                       index=0;
                       $.each(json.data, function (key, value) {

                             // $('#no_so').val(value.NoSO);
                             // $('#no_po').val(value.NoPO);
                             totalqtyyard=0;
                             totalqtymeter=0;

                             var M = new Penerimaan();

                             M.Barcode = value.Barcode;
                             M.Corak = value.Corak;
                             M.Warna = value.Warna;
                             M.Merk = value.Merk;
                             M.Qty_yard = value.Qty_yard;
                             M.Qty_meter = value.Qty_meter;
                             M.Grade = value.Grade;
                             M.Satuan = value.Satuan;
                             M.NoPO = value.NoPO;
                             M.NoSO = value.NoSO;
                             M.Party = value.Party;
                             M.Indent = value.Indent;
                             M.Lebar = value.Lebar;
                             M.Remark = value.Remark;
                             M.Harga = value.Harga;
                             M.IDSupplier = value.IDSupplier;
                             M.Nama = value.Nama;
                             PM[index] = M;
                             index++;

                             
                             

                           });

                       var table = $('#tabelfsdfsf').DataTable();
                       table.clear().draw();
                       var totaldpp=0;
                       for(i=0; i < index; i++){
                         table.row.add([
                           i+1,
                           PM[i].Barcode,
                           PM[i].Corak,
                           PM[i].Warna,
                           PM[i].Merk,
                           PM[i].Qty_yard,
                           PM[i].Qty_meter,
                           PM[i].Satuan,
                           "<td>"+"<input type=text value="+rupiah(PM[i].Harga)+" name=harga_satuan id=harga_satuan"+i+" onkeyup=hitung_total("+i+")></td>",
                           "<td><input type=text name=harga_total_b id=harga_total_b"+i+" value="+rupiah(PM[i].Harga*PM[i].Qty_yard)+"> <input type=hidden name=harga_total id=harga_total"+i+" value="+PM[i].Harga*PM[i].Qty_yard+">"+"</td>",
                           ]).draw();
                         namasupplier= PM[i].Nama;
                         idsupplier= PM[i].IDSupplier;
                         totalqtyyard+= parseFloat(PM[i].Qty_yard);
                         totalqtymeter+= parseFloat(PM[i].Qty_meter);

                         if(!isNaN(parseFloat(PM[i].Harga*PM[i].Qty_yard))){
                           totaldpp += parseFloat(PM[i].Harga*PM[i].Qty_yard);
                           console.log(i+'=-=-=-= '+ parseFloat(PM[i].Harga*PM[i].Qty_yard));
                         }


                         disc = $("#Discount").val();
                         if($("#Status_ppn").val()=="Include"){
                           if(disc == 0)
                           {
                             dpp=  100/110*totaldpp;
                             ppn= dpp*0.1;
                             $("#DPP").val(rupiah(Math.round(dpp)));
                             $("#PPN").val(rupiah(Math.round(ppn)));
                             $("#Discount_total").val(rupiah(disc));
                             total= totaldpp-ppn;
                             $("#total_invoice").val(rupiah(total-disc+ppn));
                             $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                           }else{
                             dpp=  100/110*(totaldpp-disc);
                             ppn=  dpp*0.1;
                             $("#DPP").val((Math.round(dpp)));
                             $("#PPN").val(rupiah(Math.round(ppn)));
                             $("#Discount_total").val(rupiah(disc));
                             total= totaldpp-ppn;
                             $("#total_invoice").val(rupiah(total-disc+ppn));
                             $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                           }

                         }else if($("#Status_ppn").val()=="Nonppn"){
                           ppn= 0;
                           $("#DPP").val(rupiah(Math.round(totaldpp)));
                           $("#PPN").val(rupiah(Math.round(ppn)));
                           $("#Discount_total").val(rupiah(disc));
                           total= totaldpp-ppn;
                           $("#total_invoice").val(rupiah(total-disc+ppn));
                           $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                         }else{
                          if(disc == 0){
                                  ppn= totaldpp*0.1;
                                  $("#DPP").val(rupiah(Math.round(totaldpp)));
                                  $("#PPN").val(rupiah(Math.round(ppn)));
                                  $("#Discount_total").val(rupiah(disc));
                                  $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                                  $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                                }else{
                                 ppn= (totaldpp-disc)*0.1;
                                 $("#DPP").val(rupiah(Math.round(totaldpp)));
                                 $("#PPN").val(rupiah(Math.round(ppn)));
                                 $("#Discount_total").val(rupiah(disc));
                                 $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
                                 $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
                               }
                             }
                           }

                           $("#IDSupplier").val(idsupplier);
                           $("#NamaSupplier").val(namasupplier);
                           $("#Total").val(totaldpp);
                           $("#Total_qty_yard").val(totalqtyyard);
                           $("#Total_qty_meter").val(totalqtymeter.toFixed(2));
                         }
                       });

});
});

 function ubah_status_ppn()
 {
  disc = $("#Discount").val();
  if($("#Status_ppn").val()=="Include"){
    // ppn= $("#Total").val()*0.1;
    if(disc==0){
      dpp=  100/110*$("#Total").val();
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(Math.round(dpp)));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      dpp=  100/110*($("#Total").val()-disc);
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(Math.round(dpp)));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }else if($("#Status_ppn").val()=="Nonppn"){
    ppn= 0;
    $("#DPP").val(rupiah(Math.round($("#Total").val())));
    $("#PPN").val(rupiah(Math.round(ppn)));
    $("#Discount_total").val(rupiah(disc));
    total= $("#Total").val()-ppn;
    $("#total_invoice").val(rupiah(total-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
  }else{
    if(disc == 0){
      ppn= $("#Total").val()*0.1;
      $("#DPP").val(rupiah(Math.round($("#Total").val())));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      ppn= ($("#Total").val()-disc)*0.1;
      $("#DPP").val(rupiah(Math.round($("#Total").val())));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }
}

function ubah_discount()
{
  disc = $("#Discount").val();
  if($("#Status_ppn").val()=="Include"){
    if(disc==0){
      dpp=  100/110*$("#Total").val();
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(dpp));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      dpp=  100/110*($("#Total").val()-disc);
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(dpp));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }else if($("#Status_ppn").val()=="Nonppn"){
    ppn= 0;
    $("#DPP").val(rupiah($("#Total").val()));
    $("#PPN").val(rupiah(ppn));
    $("#Discount_total").val(rupiah(disc));
    total= $("#Total").val()-ppn;
    $("#total_invoice").val(rupiah(total-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
  }else{
    if(disc == 0){
      ppn= $("#Total").val()*0.1;
      $("#DPP").val(rupiah($("#Total").val()));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      ppn= ($("#Total").val()-disc)*0.1;
      $("#DPP").val(rupiah($("#Total").val()));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }
}

function hitung_total(i) {
  var tanpa_rupiah_debit_5 = document.getElementById('harga_satuan'+i);
  tanpa_rupiah_debit_5.addEventListener('keyup', function(e)
  {
    tanpa_rupiah_debit_5.value = formatRupiah2(this.value);
  });

  tanpa_rupiah_debit_5.addEventListener('keydown', function(event)
  {
    limitCharacter(event);
  });

  var harga_satuan1= $("#harga_satuan"+i).val();
  var a = PM[i].Qty_yard;
  var b = harga_satuan1.replace(".","");
  var c=a*b;
  $("#harga_total_b"+i).val(rupiah(c));
  $("#harga_total"+i).val(c);

  totaldpp=0;
  for(l=0; l < index; l++){

    totaldpp += parseFloat($("#harga_total"+l).val());
  }
  disc=  $("#Discount").val();
  if($("#Status_ppn").val()=="Include"){

    ppn= totaldpp*0.1;
    $("#DPP").val(rupiah(totaldpp));
    $("#PPN").val(rupiah(ppn));
    $("#Discount_total").val(rupiah(disc));
    total= totaldpp-ppn;
    $("#total_invoice").val(rupiah(total-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah(total-disc+ppn));
  }else if($("#Status_ppn").val()=="Nonppn"){
    ppn= 0;
    $("#DPP").val(rupiah(totaldpp));
    $("#PPN").val(rupiah(ppn));
    $("#Discount_total").val(rupiah(disc));
    total= totaldpp-ppn;
    $("#total_invoice").val(rupiah(total-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
  }else{
    ppn= totaldpp*0.1;
    $("#DPP").val(totaldpp);
    $("#PPN").val(ppn);
    $("#Discount_total").val(rupiah(disc));
    $("#total_invoice").val(rupiah(totaldpp-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
  }

}
function fieldInvoice(){
  var data1 = {
    "id_invoice" : $("#id_invoice").val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor_invoice":$("#Nomor_invoice").val(),
    "Nomor":$("#Nomor").val(),
    "IDSupplier":$("#IDSupplier").val(),
    "Jatuh_tempo":$("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
    "No_supplier":$("#No_supplier").val(),
    "Status_ppn":$("#Status_ppn").val(),
    "Discount":$("#Discount").val(),
    "Keterangan":$("#Keterangan").val(),
    "Total_qty_yard":$("#Total_qty_yard").val(),
    "Total_qty_meter":$("#Total_qty_meter").val(),
  }
  return data1;
}
function fieldgrandtotal(){
  var data3 = {
    "id_invoice_grand_total" : $("#id_invoice_grand_total").val(),
    "id_pembayaran" : $("#id_pembayaran").val(),
    "jenis" :$("#jenis").val(),
    "jenis" :$("#jenis2").val(),
    "jenis" :$("#jenis3").val(),
    "DPP" :$("#DPP").val(),
    "Discount" :$("#Discount").val(),
    "PPN" :$("#PPN").val(),
    "total_invoice_pembayaran" :$("#total_invoice").val(),
    "nominal" :$("#nominal").val(),
    "tgl_giro" :$("#tgl_giro").val(),
    "namacoa" : $("#namacoa").val()
  }
  return data3;
}

function harga_satuan_total(i)
{
  for(i=0; i < index; i++){
    PM[i].harga_satuan = PM[i].Harga;
    PM[i].harga_total = parseFloat(PM[i].Harga)*parseFloat(PM[i].Qty_yard);
}
}

function saveInvoice(){
  var data1 = fieldInvoice();
  var data3 = fieldgrandtotal();
  var data6 = harga_satuan_total();

 $("#Nomor").css('border', '');
 $("#No_Supplier").css('border', '');

 if($("#Nomor").val()==""){
   $('#alert').html('<div class="pesan sukses">Data PB Tidak Boleh Kosong</div>');
   $("#Nomor").css('border', '1px #C33 solid').focus();
 }else if($("#No_supplier").val()==""){
   $('#alert').html('<div class="pesan sukses">No Supplier Tidak Boleh Kosong</div>');
   $("#No_supplier").css('border', '1px #C33 solid').focus();
 }else{

   $.ajax({

     url: "simpan_invoice_pembelian",

     type: "POST",

     data: {

       "data1" : data1,
       "data2" : PM,
       "data3" : data3


     },

     dataType: 'json',

     success: function (data) {
       $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
       window.location.href = "/"+base_url[1]+"/InvoicePembelian/index";
       // document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
       // document.getElementById("simpan").setAttribute("onclick", "return false;");
       // document.getElementById("printdata").setAttribute("style", "cursor: pointer;");
       // document.getElementById("printdata").setAttribute("onclick", "return true;");
       // document.getElementById("printdata").setAttribute("href", '/'+base_url[1]+'/InvoicePembelian/print/'+data['IDFB']);

     },
     error: function(msg, status){
       console.log(msg);
       $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

     }

   });
 }
}

function cek_supplier()
{
  var totalinv=0;
  var Nomor = $("#NomorB").val();
  $.ajax({
    url : '/'+base_url[1]+'/InvoicePembelian/cek_supplier',
    type: "POST",
    data:{Nomor:Nomor},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data kosong------------');
      } else {
        $("#NamaSupplier").val(data[0].Nama);
        $("#IDSupplier").val(data[0].IDSupplier);
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
    }
  });
}



function formatRupiah2(bilangan, prefix)
{
  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
  split   = number_string.split(','),
  sisa    = split[0].length % 3,
  rupiah  = split[0].substr(0, sisa),
  ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

</script>
<?php $this->load->view('administrator/footer') ; ?>