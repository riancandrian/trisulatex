<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Invoice Pembelian</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('InvoicePembelian')?>">Data Invoice Pembelian</a></li>
                            <li style="text-transform: capitalize;"> Detail Invoice Pembelian </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Tanggal ?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">No Invoice</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #ffffff;">Nama Supplier</td>
                                    <td  style="background-color: #ffffff;"><?php echo $data->IDSupplier ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">No PB</td>
                                    <td style="background-color: #e5eff0;"><?php echo $data->IDTBS ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">No SJ Supplier</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->No_sj_supplier ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Top</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $data->TOP ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal Jatuh Tempo</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Tanggal_jatuh_tempo ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Discount</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $data->Discount ?></td>
                                </tr>
                                 <tr>
                                    <td style="background-color: #ffffff;">Status PPN</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Status_ppn ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Keterangan</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $data->Keterangan ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Status</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Batal ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Total Qty Yard</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $data->Total_qty_yard ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Total Qty Meter</td>
                                    <td style="background-color: #ffffff;"><?php echo $data->Total_qty_meter ?></td>
                                </tr>   
                                <tr>
                                    <td  style="background-color: #e5eff0;">DPP</td>
                                    <td  style="background-color: #e5eff0;"><?php echo convert_currency($total_harga_inv->total_harga) ?></td>
                                </tr> 
                                <tr>
                                    <td style="background-color: #ffffff;">Grand Total</td>
                                    <td style="background-color: #ffffff;"><?php echo convert_currency($total_harga_inv->total_harga+$data->PPN); ?></td>
                                </tr> 
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                        

                         <br><br>
                                 <div class="widget_content">

                <table class="display data_tbl">
                  <thead>
                   <tr>
                    <th class="center" style="width: 40px">No</th>
                    <th>Barcode</th>
                    <th class="hidden-xs">Corak</th>
                    <th>Warna</th>
                    <th>Merk</th>
                    <th>Qty Yard</th>
                    <th>Qty Meter</th>
                    <th>Satuan</th>
                    <th>Harga Satuan</th>
                    <th style="width: 50px">Total</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($invdetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($invdetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Barcode; ?></td>
                          <td><?php echo $data2->Corak; ?></td>
                          <td><?php echo $data2->Warna; ?></td>
                          <td><?php echo $data2->Merk; ?></td>
                          <td><?php echo $data2->Qty_yard; ?></td>
                          <td><?php echo $data2->Qty_meter; ?></td>
                          <td><?php echo $data2->Satuan; ?></td>
                          <td><?php echo convert_currency($data2->Sub_total/$data2->Qty_yard); ?></td>
                          <td><?php echo convert_currency($data2->Sub_total); ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>


                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('InvoicePembelian')?>">Kembali</a></span>
                                </div>
                                <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>InvoicePembelian/print/<?php echo $data->IDFB ?>">Print Data</a></span>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>