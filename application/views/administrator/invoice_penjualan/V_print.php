<!DOCTYPE HTML>
<html>

<head>
    <title>PT Trisula Textile Industries Tbk</title>
    <link href="<?php echo base_url() ?>asset/css/layout.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/themes.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/typography.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/styles.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/ui-elements.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/wizard.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/sprite.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="<?php echo base_url() ?>asset/css/gradient.css" rel="stylesheet" type="text/css" media="screen,print">
    <style type="text/css">
        @page {
            size: F4;
            margin: 0;
        }

    </style>
</head>
<body>
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
             <!--    <div class="grid_6 invoice_num" style="margin-bottom: .5rem">
                    <span>ITC Baranangsiang Blok F12-15<br>Bandung 40112<br>022-4222065 / 4222067</span>
                </div> -->
                <div class="grid_6 invoice_num" style="margin-bottom: .5rem">
                   <center> <h1 style="float: right">I N V O I C E</h1> </center>
                </div>
                <div class="widget_content">
                    <div class=" page_content">
                        <div class="invoice_container" style="margin-top: 0">
                            <div class="invoice_action_bar" style="position: relative;">
                                <div class="grid_12">
                                    <div class="grid_6 invoice_num" style="margin-bottom: .5rem">
                                        <table border="0">
                                            <tr><td><b>To : </b></td></tr>
                                            <tr><td><?php echo $print->Nama ?> <br> <?php echo $print->Alamat ?><br><?php echo $print->Kota ?><br><?php echo $print->No_Telpon ?></td></tr>
                                        </table>
                                        <br>

                                           <table border="0">
                                            <tr><td>Invoice Number : <?php echo $print->Nomor ?> </td></tr>
                                            <tr><td>Invoice Date : <?php echo $print->Tanggal ?> </td></tr>
                                        </table>
                                        <br>

                                        <table border="0">
                                            <tr><td>Delivery Order : ---- </td></tr>
                                            <tr><td>Term Of Payment : ----- </td></tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <span class="clear"></span>

                           
                            <span class="clear"></span>
                            <div class="grid_12 invoice_details">
                                <div class="invoice_tbl">
                                    <table>
                                        <thead>
                                        <tr class=" gray_sai">
                                            <th>
                                                Material
                                            </th>
                                            <th>
                                                Grade
                                            </th>
                                            <th>
                                                Quantity
                                            </th>
                                            <th>
                                                Price
                                            </th>
                                            <th>
                                                Price Unit
                                            </th>
                                            <th>
                                                Amount
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(!empty($printdetail)){
                                            foreach ($printdetail as $data2) {
                                                ?>
                                                <tr class="odd gradeA">
                                                    <td><?php echo $print->Nomor; ?></td>
                                                    <td>-</td>
                                                    <td><?php echo $data2->Qty_yard; ?> <?php echo $data2->Satuan; ?></td>
                                                    <td><?php echo $data2->Harga; ?></td>
                                                    <td>--</td>
                                                    <td><?php echo $data2->Sub_total; ?></td>
                                                </tr>

                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <br>
                                     Total Amount : <br>
                                     V.A.T : <br>
                                     Total Amount Include Output Tax : <br>
                                </div>
                        
                                <br><br>
                                <div class="grid_2">
                                    <h5 class="notes" style="margin-top: .5rem">Menyetujui, </h5>
                                    <br><br>
                                    <p style="border-top: 1px solid #000">
                                        Date :
                                    </p>
                                </div>
                                <div class="grid_2">
                                    <h5 class="notes" style="margin-top: .5rem">Dibuat Oleh, </h5>
                                    <br><br>
                                    <p style="border-top: 1px solid #000">
                                        Date :
                                    </p>
                                </div>
                            </div>
                            <span class="clear"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</body>
</html>
 <<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        window.print();
        //console.log('masuk kesini');
        window.location.href = "<?php echo base_url() ?>InvoicePenjualan/index";
    });
</script>