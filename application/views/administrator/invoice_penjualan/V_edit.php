<?php $this->load->view('administrator/header') ; ?>
    <div class="page_title">
        <?php echo $this->session->flashdata('Pesan');?>
        <span class="title_icon"><span class="blocks_images"></span></span>
        <h3>Data Invoice Penjualan</h3>

        <div class="top_search">
        </div>
    </div>
    <!-- ======================= -->

    <div id="content">
        <div class="grid_container">
            <div class="grid_12">
                <div class="widget_wrap">
                    <div class="breadCrumbHolder module">
                        <div id="breadCrumb0" class="breadCrumb module white_lin">
                            <ul>
                                <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                                <li><a href="<?php echo base_url() ?>InvoicePenjualan">Data Invoice Penjualan</a></li>
                                <li class="active">Ubah Invoice Penjualan</li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget_top">
                        <div class="grid_12">
                            <span class="h_icon blocks_images"></span>
                            <h6>Ubah Data Invoice Penjualan</h6>
                        </div>
                    </div>
                    <div class="widget_content">
                        <form method="post" action="" class="form_container left_label">
                            <ul>

                                <li class="body-search-data">
                                    <!-- form header -->
                                    <div class="form_grid_12 w-100 alpha">
                                        <!-- hidden input -->
                                        <input type="text" name="IDFJK" id="IDFJK"  value="<?php echo $invoice->IDFJK; ?>" hidden>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb mt-dot2">No Invoice</label>
                                            <div class="form_input">
                                                <input type="text" name="Nomor_invoice" id="Nomor_invoice" class="form-control" placeholder="Nomor Invoice" value="<?php echo $invoice->Nomor; ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb">Tgl Jatuh Tempo</label>
                                            <div class="form_input">
                                                <input id="Tanggal_jatuh_tempo" type="date" value="<?php echo $invoice->Tanggal_jatuh_tempo; ?>" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" placeholder="Tanggal Jatuh tempo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb mt-dot2">Tgl Invoice</label>
                                            <div class="form_input">
                                                <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?php echo $invoice->Tanggal ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb mt-dot2">Nama di Faktur</label>
                                            <div class="form_input">
                                                <input type="text" name="nama_faktur" id="nama_faktur" value="<?php echo $invoice->Nama_di_faktur ?>" class="form-control" placeholder="Nama Faktur">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb">Nama Customer</label>
                                            <div class="form_input">
                                                <select data-placeholder="Cari Customer" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDCustomer"  id="IDCustomer" required oninvalid="this.setCustomValidity('Nama Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($customers as $data) {
                                                        ?>
                                                        <option value="<?php echo $data->IDCustomer ?>" <?php echo $data->IDCustomer == $invoice->IDCustomer ? 'selected' : '' ?>><?php echo $data->Nama; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb mt-dot2">Status PPN</label>
                                            <div class="form_input">
                                                <select style=" width:100%;" class="chzn-select" tabindex="13"  name="Status_ppn" id="Status_ppn">
                                                    <option value="Include" <?php echo $invoice->Status_ppn == 'Include' ? 'selected' : '' ?>>Include</option>
                                                    <option value="Exclude" <?php echo $invoice->Status_ppn == 'Exclude' ? 'selected' : '' ?>>Exclude</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mb">No SJ</label>
                                            <div class="form_input">
                                                <div class="list_left">
                                                    <div class="list_filter" style="margin-top: -5px;">
                                                        <input style="width: 63.5% !important" type="text" class="" name="Nomor" id="Nomor" value="<?php echo $invoice->nomor_sjck; ?>">
                                                        <button type="button" id="add-to-cart" class="list_refresh"><span class="filter_btn" style="top: 1px; left: 3px;"></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="clear"></span>
                                    </div>
                                </li>

                                <!-- content body tab & pane -->
                                <div class="grid_12 w-100 alpha">
                                    <div class="widget_wrap tabby mb-4">
                                        <div class="widget_top">
                                            <!-- button tab & pane -->
                                            <div id="widget_tab">
                                                <ul>
                                                    <li class="px py"><a href="#tab1" class="active_tab">List Barang</a></li>
                                                   <!--  <li class="px py"><a href="#tab2">Pembayaran</a></li> -->
                                                </ul>
                                            </div>
                                            <!-- end button tab & pane -->
                                        </div>

                                        <!-- tab & pane body -->
                                        <div class="widget_content">
                                            <!-- tab 1 -->
                                            <div id="tab1">
                                                <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                                    <div class="grid_12 w-100">
                                                        <span class="h_icon blocks_images"></span>
                                                        <h6>List Barang</h6>
                                                    </div>
                                                </div>
                                                <div>
                                                    <table class="display data_tbl">
                                                        <thead>
                                                        <tr>
                                                            <th class="center" style="width: 40px">No</th>
                                                            <th class="hidden-xs">Corak</th>
                                                            <th>Deskripsi Barang</th>
                                                            <th>Merek</th>
                                                            <th>Warna</th>
                                                            <th>Grade</th>
                                                            <th>Qty Pcs</th>
                                                            <th>Qty Yard</th>
                                                            <th>Harga Satuan</th>
                                                            <th>Sub Total</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tb_preview_penerimaan">

                                                        </tbody>
                                                    </table>
                                                    <ul>
                                                        <li>
                                                            <div class="form_grid_12 w-100 alpha">
                                                                <div class="form_grid_8">
                                                                    <label class="field_title mb mt-dot2">Keterangan</label>
                                                                    <div class="form_input">
                                                                        <textarea id="Keterangan" name="Keterangan" class="form-control" placeholder="Keterangan" rows="9" style="resize: none;"><?php echo $invoice->Keterangan ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form_grid_4">
                                                                    <div class="form_grid_12 mb-1">
                                                                        <label class="field_title mb mt-dot2">DPP</label>
                                                                        <div class="form_input input-not-focus">
                                                                            <input type="text" id="DPP" name="DPP" class="form-control" placeholder="DPP" readonly>
                                                                        </div>
                                                                    </div>
                                                               
                                                                            <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" value="0" readonly>
                                                                    <div class="form_grid_12 mb-1">
                                                                        <label class="field_title mb mt-dot2">PPN</label>
                                                                        <div class="form_input input-not-focus">
                                                                            <input type="text" id="PPN" name="PPN" class="form-control" placeholder="PPN" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form_grid_12 mb-1">
                                                                        <label class="field_title mb">Total Invoice</label>
                                                                        <div class="form_input input-not-focus">
                                                                            <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- clear content -->
                                                                <div class="clear"></div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- end tab 1 -->

                                            <!-- tab 2 -->
                                        <!--     <div id="tab2">
                                                <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                                    <div class="grid_12 w-100">
                                                        <span class="h_icon blocks_images"></span>
                                                        <h6>Pembayaran</h6>
                                                    </div>
                                                </div>

                                                <ul>
                                                    <li>
                                                        <div class="form_grid_12 w-100 alpha">
                                                            <div class="form_grid_6 mb-1">
                                                                <label class="field_title mb mt-dot2">Total Invoice</label>
                                                                <div class="form_input input-not-focus">
                                                                    <input type="text" id="total_invoice_pembayaran" name="total_invoice" class="form-control" placeholder="Total Invoice" value="<?php echo $grand->Grand_total ?>" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form_grid_6 mb-1">
                                                                <label class="field_title mb mt-dot2">Nama COA</label>
                                                                <div class="form_input input-disabled">
                                                                    <?php if($grand->Jenis_pembayaran=="Cash"){
                                                                        ?>
                                                                        <select name="namacoa" id="namacoa" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem" readonly>
                                                                            <option value="-">-</option>
                                                                        </select>
                                                                        <?php
                                                                    } else { ?>
                                                                        <select name="namacoa" id="namacoa" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem">
                                                                            <?php foreach ($bank as $row) : ?>
                                                                                <option value="<?php echo $row->IDCoa ?>" <?php echo $row->IDCoa == $grand->IDCOA ? 'selected' : '' ?>><?php echo $row->Nama_COA ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    <?php }?>
                                                                </div>
                                                            </div>
                                                            <div class="form_grid_6 mb-1">
                                                                <label class="field_title mb mt-dot2">Jenis Pembayaran</label>
                                                                <div class="form_input">
                                                                    <div class="mb-1">
                                                                        <span>
                                                                            <input type="radio" class="radio" name="jenis" id="jenis" value="Cash" <?php if($grand->Pembayaran=="Cash"){echo 'checked';} ?> onclick="jenis_pembayaran()">
                                                                            <label class="choice">Cash</label>
                                                                        </span>
                                                                    </div>
                                                                    <div class="mb-1">
                                                                        <span>
                                                                            <input type="radio" class="radio" name="jenis" id="jenis2" value="Transfer" <?php if($grand->Pembayaran=="Transfer"){echo 'checked';} ?> onclick="jenis_pembayaran()">
                                                                            <label class="choice">Transfer</label>
                                                                        </span>
                                                                    </div>
                                                                    <div class="mb-1">
                                                                        <span>
                                                                            <input type="radio" class="radio" name="jenis" id="jenis3" value="Giro" <?php if($grand->Pembayaran=="Giro"){echo 'checked';} ?> onclick="jenis_pembayaran()">
                                                                            <label class="choice">Giro</label>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form_grid_6 mb-1">
                                                                <label class="field_title mb mt-dot2">Tgl Giro</label>
                                                                <div class="form_input">
                                                                    <input type="date" id="tgl_giro" name="tgl_giro" class="input-date-padding-3-2 form-control" value="<?php echo $grand->Tanggal_giro ?>" placeholder="Tanggal Giro">
                                                                </div>
                                                            </div>
                                                            <div class="form_grid_6 mb-1">
                                                                <label class="field_title mb mt-dot2">Nominal</label>
                                                                <div class="form_input">
                                                                    <input type="text" id="nominal" name="nominal" class="form-control" value="<?php echo $grand->NominalPembayaran ?>">
                                                                </div>
                                                            </div>
                                                            <span class="clear"></span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div> -->
                                            <!-- end tab 2 -->
                                        </div>
                                        <!-- end tab & pane body -->
                                    </div>
                                </div>
                                <!-- end content body tab & pane -->
                            </ul>
                        </form>
                        <div class="widget_content px-2 text-center">
                            <div class="py-4 mx-2">
                                <div class="btn_30_blue">
                                    <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                                </div>
                                <div class="btn_30_blue">
                                    <span><a class="btn_24_blue" id="simpan" style="cursor: pointer;" onclick="saveEditInvoice()">Simpan</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>libjs/invoice_penjualan.js"></script>
    <script type="text/javascript">
        var base_url = window.location.pathname.split('/');
        var jenis;

        jenis_bayar = "<?php echo $grand->Pembayaran ?>";
        if (jenis_bayar) {
            jenis = jenis_bayar;
        } else {
            $("#namacoa").prop("disabled", true);
        }

        $("#Jatuh_tempo").prop("disabled", true);
        $("#Discount").prop("disabled", true);

        $(document).ready(function(){

            if ($("#jenis3").is(":checked")) {
                $("#namacoa").prop("disabled", false);
            }else if($("#jenis2").is(":checked")){
                $("#namacoa").prop("disabled", false);
            }else if($("#jenis").is(":checked")){
                $("#namacoa").prop("disabled", true);
            }
        })

        function customize_tempo(){
            if (document.getElementById('customize').checked)
            {
                $("#Jatuh_tempo").prop("disabled", false);
            } else {
                $("#Jatuh_tempo").prop("disabled", true);
            }
        }

        function discount_inv(){
            if (document.getElementById('disc').checked)
            {
                $("#Discount").prop("disabled", false);
            } else {
                $("#Discount").prop("disabled", true);
            }
        }
        function jenis_pembayaran(){
            if (document.getElementById('jenis2').checked)
            {
                $("#namacoa").prop("disabled", false);
                jenis = 'Transfer';
            } else if (document.getElementById('jenis3').checked){
                $("#namacoa").prop("disabled", false);
                jenis = 'Giro';
            } else if (document.getElementById('jenis').checked){
                $("#namacoa").prop("disabled", true);
                jenis = 'Cash';
            }
        }
        function diskon_edit()
        {
            disc=  $("#Discount").val();
            ppn=  $("#DPP").val()*0.1;
            $("#DPP").val();
            $("#PPN").val();
            $("#Discount_total").val(disc);
            total= $("#DPP").val()-ppn;
            $("#total_invoice").val(total-disc+ppn);
            $("#total_invoice_pembayaran").val(total-disc+ppn);

        }

        function hitung() {
            var a = new Date($("#Tanggal").val());
            var b = $("#Jatuh_tempo").val();

            var today = new Date(a.getTime()+(b*24*60*60*1000));
            var dd = today.getDate();
            var mm = today.getMonth()+1;

            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd;
            }
            if(mm<10){
                mm='0'+mm;
            }
            var today = yyyy+'-'+mm+'-'+dd;
            $("#Tanggal_jatuh_tempo").val(today);
        }

        function hitung_total(i) {
            var a = PM[i].Qty_yard;
            var b = $("#harga_satuan"+i).val();
            var c=a*b;
            $("#harga_total"+i).val(c);
            totaldpp=0;
            for(l=0; l < index; l++){

                totaldpp += parseInt($("#harga_total"+l).val());

            }
            disc=  $("#Discount").val();
            if($("#Status_ppn").val()=="Include"){
                ppn= totaldpp*0.1;
                $("#DPP").val(totaldpp-ppn);
                $("#PPN").val(ppn);
                $("#Discount_total").val(disc);
                total= totaldpp-ppn;
                $("#total_invoice").val(total-disc+ppn);
                $("#total_invoice_pembayaran").val(total-disc+ppn);
            }else{
                ppn= 0;
                $("#DPP").val(totaldpp);
                $("#PPN").val(ppn);
                $("#Discount_total").val(disc);
                $("#total_invoice").val(totaldpp-disc+ppn);
                $("#total_invoice_pembayaran").val(totaldpp-disc+ppn);
            }

        }
    </script>
    <script type="text/javascript">
        var PM = new Array();
        var temp = [];
        index = 0;
        totaldpp=0;

        var PB = document.getElementById("Nomor");
        $('#add-to-cart').on('click', function(){
            event.preventDefault();

            Nomor = $('#Nomor').val();
            //alert($('#no_sjc').val());

            $.post("<?php echo base_url("InvoicePenjualan/check_surat_jalan")?>", {'Nomor' : Nomor})
                .done(function(response) {

                    var json = JSON.parse(response);
                    //console.log(json);
                    if (!json.error) {
                        $.each(json.data, function (key, value) {

                            // $('#no_so').val(value.NoSO);
                            // $('#no_po').val(value.NoPO);

                            if(PM == null)
                                PM = new Array();
                            totalqtyyard = 0;
                            totalqtymeter = 0;
                            totalroll = 0;

                            var M = new Penerimaan();

                            M.Corak = value.Corak;
                            M.Warna = value.Warna;
                            M.Merk = value.Merk;
                            M.Saldo_qty = value.Saldo_qty;
                            M.Total_qty = value.Total_qty;
                            M.Satuan = value.Satuan;
                            M.Qty_yard = value.Qty_yard;
                            M.Qty_roll = value.Qty_roll;
                            M.ID_customer = $('#IDCustomer').val();
                            M.IDMata_uang = value.IDMataUang;
                            M.IDBarang = value.IDBarang;
                            M.IDWarna = value.IDWarna;
                            M.IDCorak = value.IDCorak;
                            M.IDSJCK = value.IDSJCK;
                            M.Kurs = value.Kurs;
                            M.IDSatuan = value.IDSatuan;
                            M.Nama_barang = value.Nama_Barang;
                            M.Grade = value.Grade;


                            PM[index] = M;
                            index++;

                            $("#tb_preview_penerimaan").html("");
                            for(i=0; i < index; i++){
                                value = "<tr>"+
                                    "<td class='text-center'>"+(i+1)+"</td>"+
                                    "<td>"+PM[i].Corak+"</td>"+
                                    "<td>"+PM[i].Nama_barang+"</td>"+
                                    "<td>"+PM[i].Merk+"</td>"+
                                    "<td>"+PM[i].Warna+"</td>"+
                                     "<td>"+PM[i].Grade+"</td>"+
                                    "<td>"+PM[i].Qty_roll+"</td>"+
                                    "<td>"+PM[i].Qty_yard+"</td>"+
                                    "<td>-</td>"+
                                    "<td>"+"<input type=text name=harga_satuan id=harga_satuan"+i+" onkeyup=hitung_total("+i+")>"+"</td>"+
                                    "<td>"+"<input type=text name=harga_total id=harga_total"+i+" value=0>"+"</td>"+
                                    "</tr>";
                                $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);
                                totalqtyyard+= parseInt(PM[i].Qty_yard);
                                totalroll += parseInt(PM[i].Qty_roll);
                            }
                            $("#Total_qty_yard").val(totalqtyyard);
                            $("#Total_roll").val(totalroll);

                        });
                    } else {
                        alert('Tidak ada data dengan nomor Penerimaan ' + Nomor);
                    }
                });
        });

        function fieldInvoice(){
            var data1 = {
                "IDFJK" : $("#IDFJK").val(),
                "Tanggal" :$("#Tanggal").val(),
                "Nomor_invoice":$("#Nomor_invoice").val(),
                "Nomor":$("#Nomor").val(),
                "IDCustomer":$("#IDCustomer").val(),
                "Nama_di_faktur":$("#nama_faktur").val(),
                "Jatuh_tempo":$("#Jatuh_tempo").val(),
                "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
                "Total_yard":$("#Total_qty_yard").val(),
                "Total_pieces":$("#Total_roll").val(),
                "Discount":0,
                "Status_ppn":$("#Status_ppn").val(),
                "Keterangan":$("#Keterangan").val()
            }
            return data1;
        }

        function fieldgrandtotal(){
            var data3 = {
                "jenis" :$("#jenis").val(),
                "jenis" :$("#jenis2").val(),
                "jenis" :$("#jenis3").val(),
                "DPP" :$("#DPP").val(),
                //"Discount" :$("#Discount").val(),
                "PPN" :$("#PPN").val(),
                "total_invoice_pembayaran" :$("#total_invoice").val(),
                "nominal" :$("#nominal").val(),
                "tgl_giro" :$("#tgl_giro").val(),
                "namacoa" :$("#namacoa").val()
            }
            return data3;
        }

        function saveEditInvoice(){
            var data1 = fieldInvoice();
            var data2 = fieldgrandtotal();
            var idfjk = '_';
            console.log(data1);
            console.log(data2);

            $.ajax({
                url: "../ubah_invoice_penjualan",
                type: "POST",
                data: {
                    "data1" : data1,
                    "data2" : data2
                },
                // dataType: 'json',

                success: function (msg, status) {
                    $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');

                    document.getElementById("simpan").setAttribute("style", "cursor: no-drop;");
                    document.getElementById("simpan").setAttribute("onclick", "return false;");
                    document.getElementById("print").setAttribute("style", "cursor: pointer;");
                    document.getElementById("print").setAttribute("onclick", "return true;");
                    document.getElementById("print").setAttribute("href", '/'+base_url[1]+'/InvoicePenjualan/print_/'+data1['Nomor_invoice']+ '/' + idfjk);
                },
                error: function(msg, status, data){
                    console.log(msg);
                    $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');

                }
            });
        }

        $(document).ready(function(){
            renderJSONE($.parseJSON('<?php echo json_encode($detail) ?>'));
        });
    </script>
<?php $this->load->view('administrator/footer') ; ?>