<?php $this->load->view('administrator/header'); ?>
<div class="page_title">
	<?php echo $this->session->flashdata('Pesan');?>
	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Data Instruksi</h3>
	<div class="top_search">
	</div>
</div>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<li><a href="<?php echo base_url() ?>InstruksiPengiriman">Data Instruksi</a></li>
							<li class="active">Edit Instruksi</li>
						</ul>
					</div>
				</div>
				<div class="widget_top">
					<div class="grid_12">
						<span class="h_icon blocks_images"></span>
						<h6>Edit Data Instruksi</h6>
					</div>
				</div>
				<div class="widget_content">
					
					<table class="display data_tbl">
                            <thead style="display: none;">

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->Tanggal ?></td>
                                </tr>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0;">No Instruksi</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $instruksi->Nomor ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #ffffff;">Supplier</td>
                                    <td  style="background-color: #ffffff;"><?php echo $instruksi->IDSupplier ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0;">No SJC</td>
                                    <td style="background-color: #e5eff0;"><?php echo $instruksi->Nomor_sj ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">No PO</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->IDPO ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">No SO</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $instruksi->NoSO ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff;">Tanggal Kirim</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->Tanggal_kirim ?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0;">Total Yard</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $instruksi->Total_yard ?></td>
                                </tr>
                                 <tr>
                                    <td style="background-color: #ffffff;">Total Meter</td>
                                    <td style="background-color: #ffffff;"><?php echo $instruksi->Total_meter ?></td>
                                </tr>
                                
                                
                                
                               
                            </tbody>
                            <tfoot></tfoot>
                        </table><br><br>
					<table class="display">
						<thead>
							<tr>
								<th>No</th>
								<th>Barcode</th>
								<th>Corak</th>
								<th>Warna</th>
								<th>Merk</th>
								<th>Qty Yard</th>
								<th>Qty Meter</th>
								<th>Grade</th>
								<th>Satuan</th>
							</tr>
						</thead>
						<tbody id="tabel_detail_tampungan">
							<?php $i=0; foreach ($data_tabel_detail as $data): $i++;?>
							<tr class='row_tampungan' id='row_tampungan<?php echo $i; ?>'>
								<td><input type='hidden' id='IDIPDetail<?php echo $i ?>' value='<?php echo $data->IDIPDetail; ?>'><?php echo $i ?></td>
								<td><?php echo $data->Barcode; ?></td>
								<td><?php echo $data->Corak; ?></td>
								<td><?php echo $data->Warna; ?></td>
								<td><?php echo $data->Merk; ?></td>
								<td><input type="hidden" id="Qty_yard<?php echo $i ?>" value="<?php echo $data->Qty_yard; ?>"><?php echo $data->Qty_yard; ?></td>
								<td><input type="hidden" id="Qty_meter<?php echo $i ?>" value="<?php echo $data->Qty_meter; ?>"><?php echo $data->Qty_meter; ?></td>
								<td><?php echo $data->Grade; ?></td>
								<td><?php echo $data->Satuan; ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<!-- <div class="btn_24_blue">
					<a href="<?php //echo base_url('InstruksiPengiriman/edit/'.$this->uri->segment(3))?>"><span>Ubah Data</span></a>
					<a href="<?php //echo base_url('InstruksiPengiriman/print/'.$instruksi->IDIP)?>"><span>Print</span></a>
				</div> -->
			</div>
		</div>
		
	</div>

</div>
</div>
<!-- <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script> -->
<?php $this->load->view('administrator/footer') ; ?>
<script type="text/javascript">
	var i=0;
	var array_yard = [];
	var array_meter = [];
	var total_yard = 0;
	var total_meter = 0;
	
	base_url = window.location.pathname.split( '/' );
	function carisjc(){
		id_sjc = $('#in_id_sjc').val();
		$.ajax({
			url: '/'+base_url[1]+'/InstruksiPengiriman/cari_sjc',
			type: "POST",
			data: {
				"id_sjc" :  id_sjc
			},
			dataType: "json",
			success: function (data,msg,response,status) {
        // console.log(data);
        document.getElementById("no_so").value = data.dataPOSO.NoSO;
        document.getElementById("no_po").value = data.dataPOSO.NoPO;
        total_yard = 0;
        total_meter = 0;
        array_yard = [];
        array_meter = [];
        i=0;
        $('.row_tampungan').remove();
        $.each(data.dataTabel, function(idx, obj) {
        	array_yard[i]= parseInt(obj.Qty_yard);
        	array_meter[i]= parseInt(obj.Qty_meter);
        	i+=1;
        	var value =
        	"<tr class='row_tampungan' id='row_tampungan"+i+"'>" +
        	"<td><input type='hidden' name='IDT[]' value='"+obj.IDT+"'>"+i+"</td>"+
        	"<td>"+obj.Barcode+"</td>"+
        	"<td>"+obj.Corak+"</td>"+
        	"<td>"+obj.Warna+"</td>"+
        	"<td>merek</td>"+
        	"<td>"+obj.Qty_yard+"</td>"+
        	"<td>"+obj.Qty_meter+"</td>"+
        	"<td>"+obj.Grade+"</td>"+
        	"<td>"+obj.Satuan+"</td>"+
        	"<td class='text-center ukuran-log'>" +
        	"<span><a class='action-icons c-Delete' href='#' title='Delete' onclick='deleteItem("+i+")'>Delete</a></span>"+
        	"</td>"+
        	"</tr>";
        	$("#tabel_detail_tampungan").append(value);
        });
        for(x=0; x<array_yard.length; x++){
        	total_yard += parseInt(array_yard[x]);
        	total_meter += parseInt(array_meter[x]);
        }
        // console.log(data);
        document.getElementById("total_yard").value = total_yard;
        document.getElementById("total_meter").value = total_meter;
    },
    error: function(data,msg,response,status){
    	console.log("Failure"+data+msg+response+status);
    }
});
	}

	function deleteItem(i){
		$('#row_tampungan'+i).remove();
		array_yard.splice(i-1, 1);
		array_meter.splice(i-1, 1);
		total_yard = 0;
		total_meter = 0;
		for(x=0; x<array_yard.length; x++){
			total_yard += parseInt(array_yard[x]);
			total_meter += parseInt(array_meter[x]);
		}
		document.getElementById("total_yard").value = total_yard;
		document.getElementById("total_meter").value = total_meter;
	}
	function deleteItemawal(i){
		var value =
		"<tr>" +
		"<td><input type='hidden' name='hapusIDIPDetail[]' value='"+parseInt($('#IDIPDetail'+i).val())+"'></td>"+
		"</tr>";
		$("#tabel_hapus").append(value);
		id_total_yard = parseInt($('#total_yard').val())-parseInt($('#Qty_yard'+i).val());
		id_total_meter = parseInt($('#total_meter').val())-parseInt($('#Qty_meter'+i).val());
		document.getElementById("total_yard").value = id_total_yard;
		document.getElementById("total_meter").value = id_total_meter;
		$('#row_tampungan'+i).remove();
	}
</script>