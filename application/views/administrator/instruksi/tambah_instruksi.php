<?php $this->load->view('administrator/header'); 

if ($kodenoins->curr_number == null) {
  $number = 1;
} else {
  $number = $kodenoins->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_no_ins = 'IPB'.date('y').str_replace(' ', '',$agen).$kodemax;
?>

<style type="text/css">
.list_filter input[type="text"]{
  width: auto !important;
}
.chzn-container {
  width: 102.5% !important;
}
</style>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Instruksi</h3>
 <div class="top_search">
 </div>
</div>
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>InstruksiPengiriman">Data Instruksi</a></li>
              <li class="active">Tambah Instruksi</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Instruksi</h6>
          </div>
        </div>
        <div class="widget_content">

          <form method="post" action="<?php echo base_url('InstruksiPengiriman/store'); ?>" class="form_container left_label">
            <ul>
              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mt-dot2">Tanggal</label>
                    <div class="form_input">
                      <input type="text" class="form-control datepicker" value="<?php echo date('m/d/Y'); ?>" id="tanggal" name="tanggal" required>
                    </div>
                  </div>
                  <div class="form_grid_4 mb-1">
                    <label class="field_title mt-dot2">No SJC</label>
                    <div class="form_input">
                      <!-- <div class="list_left" style="width: 200px !important;"> -->
                        <div class="list_filter">
                          <select name="in_id_sjc" id="in_id_sjc" onchange='carisjc(this.value)' required style=" width:100%;" class="chzn-select" tabindex="13" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong dan lakukan search')" oninput="setCustomValidity('')">
                            <option value=""></option>
                            <?php foreach ($daftarsjc as $sjc): ?>
                              <option value="<?php echo $sjc->Nomor_sj ?>"><?php echo $sjc->Nomor_sj ?></option>
                            <?php endforeach ?>
                          </select>
                          <!-- <button type="button" title='Cari' onclick='' class="list_refresh" style="height: 28px;top: 0;position: absolute;right: 0 !important;left: 90%;"><span class="filter_btn" style="top: 1px; left: 3px;"></span></button> -->
                          <!-- </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="form_grid_4 mb-1">
                      <label class="field_title pt">Tanggal Kirim</label>
                      <div class="form_input">
                        <input type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" name="tanggal_kirim" id="tanggal_kirim" required>
                      </div>
                    </div>
                    <div class="form_grid_4">
                      <label class="field_title pt">Nomor Intruksi</label>
                      <div class="form_input">
                        <input type="text" class="form-control" placeholder="Nomor Instruksi" value="<?php echo $code_no_ins; ?>" id="no_instruksi" name="no_instruksi" required>
                      </div>
                    </div>
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">No PO</label>
                      <div class="form_input input-not-focus">
                        <input type="text" readonly class="form-control" value="" name="no_po" id="no_po" required>
                      </div>
                    </div>
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Total Yard</label>
                      <div class="form_input input-not-focus">
                        <input type="text" readonly class="form-control" value="" name="total_yard" id="total_yard" required>
                      </div>
                    </div>
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Supplier</label>
                      <div class="form_input">
                        <select name="supplier" id="supplier" required style=" width:100%;" class="chzn-select" tabindex="13" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong dan lakukan search')" oninput="setCustomValidity('')">
                          <?php foreach ($data_supplier as $suplier): ?>
                            <option value="<?php echo $suplier->IDSupplier; ?>"><?php echo '('.$suplier->Kode_Suplier.') '.$suplier->Nama; ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">No SO</label>
                      <div class="form_input input-not-focus">
                        <input type="text" readonly class="form-control" value="" name="no_so" id="no_so" required>
                      </div>
                    </div>
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Total Meter</label>
                      <div class="form_input input-not-focus">
                        <input type="text" readonly class="form-control" value="" name="total_meter" id="total_meter" required>
                      </div>
                    </div>
                    <div class="form_grid_4 mb-1">
                    </div>
                    <div class="form_grid_4 mb-1">
                    </div>
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Keterangan</label>
                      <div class="form_input input-not-focus">
                        <textarea name="keterangan" class="form-control"></textarea>
                        <!-- <input type="textarea" readonly class="form-control" value="" name="total_meter" id="total_meter" required> -->
                      </div>
                    </div>
                    <span class="clear"></span>
                  </div>
                </li>
                <br><br>
                <div class="widget_content">
                  <table class="display">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Barcode</th>
                        <th>Corak</th>
                        <th>Warna</th>
                        <th>Merk</th>
                        <th>Qty Yard</th>
                        <th>Qty Meter</th>
                        <th>Grade</th>
                        <th>Satuan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody id="tabel_detail_tampungan">

                    </tbody>
                  </table>
                </div>
                <ul>
                  <li class="body-search-data total-accumulation">

                  </li>
                </ul>
              </form>
            </ul>
            <ul>
              <li>
               <div class="widget_content px-2 text-center">
                <div class="py-4 mx-2">

                  <div class="btn_30_blue">
                    <span><input type="submit" value="simpan" id="simpan" onclick="simpan()"></span>
                  </div>

                </div>
                <div class="clear"></div>
              </li>
            </ul>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script> -->
  <?php $this->load->view('administrator/footer') ; ?>
  <script type="text/javascript">
    var i=0;
    var array_IDT = [];
    var array_yard = [];
    var array_meter = [];
    var total_yard = 0;
    var total_meter = 0;
    base_url = window.location.pathname.split( '/' );
      // url: '/'+base_url[1]+'/InstruksiPengiriman/cari_sjc',
      function carisjc(value){
        id_sjc = value;
        id_sjc = $('#in_id_sjc').val();
        $.ajax({
          url: 'cari_sjc',
          type: "POST",
          data: {
            "id_sjc" :  id_sjc
          },
          dataType: "json",
          success: function (data,msg,response,status) {
        // console.log(data);
        document.getElementById("no_so").value = data.dataPOSO.NoSO;
        document.getElementById("no_po").value = data.dataPOSO.NoPO;
        total_yard = 0;
        total_meter = 0;
        array_yard = [];
        array_meter = [];
        i=0;
        //console.log(data);  
        $('.row_tampungan').remove();
        if(data.dataTabel!=null){
          $.each(data.dataTabel, function(idx, obj) {

            array_yard[i]= parseFloat(obj.Qty_yard);
            array_meter[i]= parseFloat(obj.Qty_meter);
            array_IDT[i]= parseInt(obj.IDT);
            i+=1;
            var value =
            "<tr class='row_tampungan' id='row_tampungan"+i+"'>" +
            "<td><input type='hidden' name='IDT[]' id='IDT[]' value='"+obj.IDT+"'>"+i+"</td>"+
            "<td>"+obj.Barcode+"</td>"+
            "<td>"+obj.Corak+"</td>"+
            "<td>"+obj.Warna+"</td>"+
            "<td>merek</td>"+
            "<td>"+obj.Qty_yard+"</td>"+
            "<td>"+obj.Qty_meter+"</td>"+
            "<td>"+obj.Grade+"</td>"+
            "<td>"+obj.Satuan+"</td>"+
            "<td class='text-center ukuran-log'>" +
            "<span><a class='action-icons c-Delete' href='#' title='Delete' onclick='deleteItem("+i+")'>Delete</a></span>"+
            "</td>"+
            "</tr>";
            $("#tabel_detail_tampungan").append(value);

          });
        }else{
          $('#alert').html('<div class="pesan sukses">Instruksi Pengiriman Sudah Tidak Ada</div>');
        }
        for(x=0; x<array_yard.length; x++){
          total_yard += parseFloat(array_yard[x]);
          total_meter += parseFloat(array_meter[x]);
        }
        
        document.getElementById("total_yard").value = total_yard;
        document.getElementById("total_meter").value = total_meter;
      },
      error: function(data,msg,response,status){
      }
    });
      }

      function deleteItem(i){
        $('#row_tampungan'+i).remove();
        array_yard.splice(i-1, 1);
        array_meter.splice(i-1, 1);
        array_IDT.splice(i-1, 1);
        total_yard = 0;
        total_meter = 0;
        for(x=0; x<array_yard.length; x++){
          total_yard += parseFloat(array_yard[x]);
          total_meter += parseFloat(array_meter[x]);
        }
        document.getElementById("total_yard").value = total_yard;
        document.getElementById("total_meter").value = total_meter;
      }
      function simpan(){
        // console.log('simmpan');
        $.ajax({
          url: 'store',
          type: "POST",
          data: {
            'IDIP' : $('#IDIP').val(),
            'tanggal' : $('#tanggal').val(),
            'no_instruksi' : $('#no_instruksi').val(),
            'supplier' : $('#supplier').val(),
            'in_id_sjc' : $('#in_id_sjc').val(),
            'no_po' : $('#no_po').val(),
            'no_so' : $('#no_so').val(),
            'tanggal_kirim' : $('#tanggal_kirim').val(),
            'total_yard' : $('#total_yard').val(),
            'total_meter' : $('#total_meter').val(),
            'IDT' : array_IDT
          },
          dataType: "json",
          success: function (data,msg,response,status) {
           window.location.href = "/"+base_url[1]+"/InstruksiPengiriman/input_instruksi";
         },
         error: function(data,msg,response,status){
        // console.log("Failure"+data+msg+response+status);
        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
      }
    });
      }
    </script>