<?php $this->load->view('administrator/header') ; ?>
<!-- tittle Data -->
<div class="page_title">

	<!-- notify -->
	<?php echo $this->session->flashdata('Pesan');?>
	<!-- ====== -->

	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Data COA (Charts Of Accounts)</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">

				<!-- breadcrumb -->
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<li><a href="<?php echo base_url() ?>coa">Data COA (Charts Of Accounts)</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Edit COA (Charts Of Accounts) </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                	<div class="grid_12">
                		<span class="h_icon blocks_images"></span>
                		<h6>Edit Data COA (Charts Of Accounts)</h6>
                	</div>
                </div>
                <div class="widget_content">
                	<form method="post" action="<?php echo base_url() ?>coa/update" enctype="multipart/form-data" class="form_container left_label">
                		<ul>
                			<li>
                				<div class="form_grid_12 multiline">
                					<input type="hidden" name="id" value="<?php echo $coa->IDCoa ?>">

                					<!-- Kode COA -->
                					<label class="field_title">Kode COA</label>
                					<div class="form_input">
                						<input type="text" class="form-control" value="<?php echo $coa->Kode_COA ?>" name="Kode_COA" placeholder="Kode COA" required oninvalid="this.setCustomValidity('Kode COA Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                					</div>

                					<!-- Nama COA -->
                					<label class="field_title">Nama COA</label>
                					<div class="form_input">
                						<input type="text" name="Nama_COA" value="<?php echo $coa->Nama_COA ?>" class="form-control" placeholder="Nama COA" required oninvalid="this.setCustomValidity('Nama COA Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                					</div>

                					<!-- Status COA -->
                					<label class="field_title">Status COA</label>
                					<div class="form_input">
                						<select data-placeholder="Pilih Group Barang" name="Status" required oninvalid="this.setCustomValidity('Status COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                							<option value=""></option>
                							<?php if ($coa->Status == 'anak') {
                								echo '<option selected value="anak">Anak</option><option value="kepala">Kepala</option>';
                							}else{
                								echo '<option value="anak">Anak</option><option selected value="kepala">Kepala</option>';
                							} ?>
                						</select>
                					</div>

                					<!-- Nama Group -->
                					<label class="field_title">Nama Group</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Corak" name="Nama_Group" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php
                                            foreach ($groupcoa as $co) {
                                                if ($coa->IDGroupCOA==$co->IDGroupCOA) {
                                                    ?>
                                                    <option value="<?php echo $co->IDGroupCOA ?>" selected><?php echo $co->Nama_Group; ?></option>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $co->IDGroupCOA ?>"><?php echo $co->Nama_Group; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                					<!-- Debit -->
                					<div class="form_input">
                						<input type="hidden" class="form-control price-date" name="Modal" value="<?php echo convert_currency($coa->Modal) ?>" id="tanpa-rupiah-debit" placeholder="Debit" required oninvalid="this.setCustomValidity('Debit Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                					</div>

                					<!-- Kredit -->
                					<!-- <label class="field_title">Kredit</label>
                					<div class="form_input">
                						<span class="input-group-addon" style="">Rp. </span>
                						<input type="text" class="form-control price-date" name="Kredit" value="<?php// echo convert_currency($coa->Kredit) ?>" placeholder="Kredit" id="tanpa-rupiah-kredit" required oninvalid="this.setCustomValidity('Kredit Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                					</div> -->

                				</div>
                			</li>

                			<li>
                				<div class="form_grid_12">
                					<div class="form_input">
                						<div class="btn_30_light">
                							<span> <a href="<?php echo base_url() ?>coa" name="simpan" title=".classname">Kembali</a></span>
                						</div>
                						<div class="btn_30_blue">
                							<span><input type="submit" name="simpan" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                						</div>
                					</div>
                				</div>
                			</li>
                		</ul>
                	</form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ================== -->

<?php $this->load->view('administrator/footer') ; ?>