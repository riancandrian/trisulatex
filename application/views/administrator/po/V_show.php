<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Purchase Order</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('Po/index')?>">Data Purchase Order</a></li>
                            <li style="text-transform: capitalize;"> Detail Purchase Order - <b><?php echo $po->Nomor ?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">
                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $po->Tanggal ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Nomor</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Nomor?></td>
                                </tr>  
                                <tr>         
                                    <td  style="background-color: #e5eff0;">Tanggal Selesai</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $po->Tanggal_selesai ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff;">Supplier</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Nama ?></td>
                                </tr> 
                                <tr>         
                                     <td style="background-color: #e5eff0;">Corak</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Corak?></td>
                                </tr>  
                                <tr>         
                                   <td style="background-color: #ffffff;">Merk</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Merk?></td>
                                </tr>  
                                <tr>    
                                <td  style="background-color: #e5eff0;">Nama Barang</td>
                                    <td  style="background-color: #e5eff0;"><?php echo $po->Nama_Barang ?></td>     
                                    
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Agen</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Nama_Perusahaan ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Lot</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Lot?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff;">Jenis Pesanan</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Jenis_Pesanan?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">PIC</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->PIC?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff;">Prioritas</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Prioritas?></td>
                                </tr> 
                                <tr>         
                                    <td style="background-color: #e5eff0;">Bentuk</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Bentuk?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff;">Panjang</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Panjang?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Point</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Point?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff;">Kirim</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Kirim?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Stamping</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Stamping?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Posisi 1</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Posisi?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Posisi 2</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Posisi1?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Album</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Album?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">M10/20</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->M10?></td>
                                </tr>   
                                <tr>         
                                    <td style="background-color: #ffffff;">Kain</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Kain?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #e5eff0;">Lembaran</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Lembaran?></td>
                                </tr>   
                                <tr>         
                                    <td style="background-color: #ffffff;">Keterangan</td>
                                    <td style="background-color: #ffffff;"><?php echo $po->Keterangan?></td>
                                </tr> 
                                <tr>         
                                    <td style="background-color: #e5eff0;">Status</td>
                                    <td style="background-color: #e5eff0;"><?php echo $po->Batal?></td>
                                </tr>   
                                <tr>         
                                    <td style="background-color: #ffffff;">Total Qty Yard</td>
                                    <td style="background-color: #ffffff;"><?php echo convert_currency($po->Total_qty) ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #e5eff0;">Total Harga</td>
                                    <td style="background-color: #e5eff0;"><?php echo convert_currency($po->Grand_total) ?></td>
                                </tr>  

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                                 <div class="widget_content">

                <table class="display data_tbl">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Warna</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($podetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($podetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Warna; ?></td>
                          <td><?php echo convert_currency($data2->Qty); ?></td>
                          <td><?php echo convert_currency($data2->Harga); ?></td>
                          <td><?php echo convert_currency($data2->Harga*$data2->Qty); ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('Po/index')?>" name="simpan">Kembali</a></span>
                                </div>
                                <div class="btn_30_blue">
                          <span><a href="<?php echo base_url() ?>Po/print_data_po/<?php echo $po->IDPO ?>">Print Data</a></span>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>