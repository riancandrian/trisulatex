<?php $this->load->view('administrator/header') ; 
if ($kodepo->curr_number == null) {
  $number = 1;
} else {
  $number = $kodepo->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $this->session->userdata("nama");

$code_po = 'PO'.date('y').str_replace(' ', '',$agen).$kodemax;
// echo $code_po;
?>
      <!-- ---------------------------------------------Style untuk tabs -->
      <style type="text/css" scoped>
        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }
        .tab a {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
            color:black;
        }
        .tab a:hover {
            background-color: #ddd;
        }
        .tab a.active {
            background-color: #ccc;
        }
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Purchase Order</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
    <div class="grid_container">
      <div class="grid_12">
          <div class="widget_wrap">
              <!-- -----------------------------Title -->
              <div class="breadCrumbHolder module">
                  <div id="breadCrumb0" class="breadCrumb module white_lin">
                      <ul>
                          <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                          <li><a href="<?php echo base_url() ?>Po">Data Purchase Order</a></li>
                          <li class="active">Edit Purchase Order</li>
                      </ul>
                  </div>
              </div>
              <div class="widget_top">
                  <div class="grid_12">
                      <span class="h_icon blocks_images"></span>
                      <h6>Edit Data Purchase Order</h6>
                  </div>
              </div>
              <!-- -----------------------------content -->
              <div class="widget_content">
                <form method="post" action="" class="form_container left_label">
                    <ul>
                        <!-- ------------------------------Form Atas -->
                        <li>
                          <div class="form_grid_12 multiline">
                            <div class="form_grid_4">
                              <label class="field_title">Tanggal</label>
                              <div class="form_input">
                                <input type="hidden" name="IDPO" id="IDPO" value="<?php echo $datapo->IDPO ?>">
                                <input id="Tanggal" type="date" value="<?= $datapo->Tanggal ?>" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_4">
                              <label class="field_title">Supplier</label>
                              <div class="form_input">
                                <select name="IDSupplier" id="IDSupplier" style="width:100%;" class="chzn-select">
                                      <option value="<?php echo $trisula->IDSupplier ?>"><?php echo $trisula->Nama ?></option>
                                  </select>
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_3">
                              <label class="field_title">Agent</label>
                              <div class="form_input">
                                <input type="text" name="IDAgen" id="IDAgen" class="form-control" data-id="<?php echo $this->session->userdata("id")  ?>" placeholder="No Asset" value="<?php echo $agen ?>" readonly>
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_1">
                              <div class="form_input">
                                <?php
                                  if ($datapo->Jenis_Pesanan == "seragam") { ?>
                                    <input type="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="seragam" checked>Seragam
                                  <?php } else { ?>
                                    <input type="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="seragam">Seragam
                                  <?php }
                                ?>
                              
                                <span class="clear"></span>
                              </div>
                            </div>
                          </div>
                          <div class="form_grid_12 multiline">
                            <div class="form_grid_4">
                              <label class="field_title">No PO</label>
                              <div class="form_input">
                                <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $datapo->Nomor ?>" readonly>
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_4">
                              <label class="field_title">Corak</label>
                              <div class="form_input">
                              <select style="width:100%;" class="chzn-select" name="corak" id="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                  <option value="" selected>--PilihCorak--</option>
                                  <?php foreach($corak as $row) {
                                      if ($row->IDCorak == $datapo->IDCorak) {
                                        echo "<option value='$row->IDCorak' selected>$row->Corak</option>";
                                      }
                                      echo "<option value='$row->IDCorak'>$row->Corak</option>";
                                  }
                                  ?>
                              </select>
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_3">
                              <label class="field_title">Lot</label>
                              <div class="form_input">
                                <input type="text" name="Lot" id="Lot" class="form-control" placeholder="Lot" value="<?= $datapo->Lot ?>">
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_1">
                              <div class="form_input">
                                <?php
                                  if ($datapo->Jenis_Pesanan == "stok") { ?>
                                    <input type="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="stok" checked>Stok
                                  <?php } else { ?>
                                    <input type="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="stok">Stok
                                  <?php }
                                ?>
                                <span class="clear"></span>
                              </div>
                            </div>
                          </div>
                          <div class="form_grid_12 multiline">
                            <div class="form_grid_4">
                              <label class="field_title">Tanggal Selesai</label>
                              <div class="form_input">
                                <input id="Tanggal_selesai" value="<?= $datapo->Tanggal_selesai ?>" type="date" name="Tanggal_selesai" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_4">
                              <label class="field_title">Merk</label>
                              <div class="form_input">
                                <!-- <select name="merk" id="merk" style="width:100%;" class="chzn-select">
                                        <option value="<?php echo $trisula->IDSupplier ?>"><?php echo $trisula->Nama ?></option>
                                </select> -->
                                <!-- <select name="merk" id="merk">
                                  <option value="">--Pilih--</option>
                                </select> -->
                                <input type="text" name="merk" id="merk" data-idmerk class="form-control" placeholder="Total Qty" readonly>
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_3">
                              <label class="field_title">Total</label>
                              <div class="form_input">
                                <input type="text" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" value="<?= $datapo->Total_qty ?>" readonly>
                                <span class="clear"></span>
                              </div>
                            </div>
                            <div class="form_grid_1">
                              <div class="form_input">
                                <?php
                                  if ($datapo->Jenis_Pesanan == "garment") { ?>
                                    <input type="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="garment" checked>Garment
                                  <?php } else { ?>
                                      <input type="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="garment">Garment 
                                 <?php }
                                ?>
                               
                                <span class="clear"></span>
                              </div>
                            </div>
                          </div>
                          <span class="clear"></span>
                        </li>
                        <!-- ------------------------------Form Bawah -->
                        <li>
                          <div class="tab">
                            <a type="none" class="tablinks" onclick="openCity(event, 'inputwarna')">Input Warna</a>
                            <a class="tablinks" onclick="openCity(event, 'metodepacking')">Metode Packing</a>
                            <a class="tablinks" onclick="openCity(event, 'cappinggir')">Cap Pinggir</a>
                            <a class="tablinks" onclick="openCity(event, 'catatan')">Catatan</a>
                          </div>
                        </li>
                        <span class="clear"></span>
                        <br>
                        
                        <!-- //------------------------input Warna -->
                        <div id="inputwarna" class="tabcontent">
                          <h3>Warna</h3>
                          <div class="form_grid_12 multiline">
                            <div class="form_grid_4">
                                <label class="field_title">Warna</label>
                                <div class="form_input">
                                  <select name="warna" id="warna" style="width:100%;" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <option value="">--Silahkan Pilih Corak Dahulu--</option>
                                  </select>
                                  <span class="clear"></span>
                                </div>

                                <label class="field_title">Qty</label>
                                <input type="text" name="Qty" id="Qty" class="form-control" placeholder="Qty">
                                <span class="clear"></span>
                                <br>
                                <div class="form_input">
                                    <button class="btn_30_blue" type="button" id="tambah_sementara">
                                        Edit Data
                                    </button>
                                </div>
                            </div>
                            <div class="form_grid_8">
                              <table class="display data_tbl">
                                  <thead>
                                  <tr>
                                      <th class="center" style="width: 40px">No</th>
                                      <th>Warna</th>
                                      <th>Qty Yard</th>
                                      <th style="width: 50px">Action</th>
                                  </tr>
                                  </thead>
                                  <tbody id="tabel-cart-asset">

                                  </tbody>
                              </table>
                            </div>
                            <div class="form_grid_12">
                              <b>Delivery</b>
                            </div>
                            
                            <span class="clear"></span>
                            <br>
                            <div class="form_grid_1">
                                PIC
                            </div>
                            
                            <div class="form_grid_5">
                              <input type="text" id="PIC" name="PIC" class="form-control" placeholder="PIC" value="<?= $datapo->PIC ?>">
                                <?php if ($datapo->Prioritas == "normal") { ?>
                                  <input type="radio" name="Prioritas" id="Prioritas" value="normal" checked> Normal
                                  <?php } else { ?>
                                    <input type="radio" name="Prioritas" id="Prioritas" value="normal"> Normal
                                 <?php }
                                ?>

                                <?php if ($datapo->Prioritas == "urgent") { ?>
                                  <input type="radio" name="Prioritas" id="Prioritas" value="urgent" checked>  Urgent
                                  <?php } else { ?>
                                    <input type="radio" name="Prioritas" id="Prioritas" value="urgent">  Urgent
                                 <?php }
                                ?>
                              
                              
                            </div>
                            <span class="clear"></span>
                          </div>
                        </div>
                        <!-- //------------------------input packing -->
                        <div id="metodepacking" class="tabcontent">
                          <h3>Metode Packing & Pengiriman</h3>
                          <div class="form_grid_12 multiline">
                            <div class="form_grid_6">
                                <label class="field_title">Bentuk</label>
                                <div class="form_input">
                                  <select style="width:450%;" class="chzn-select" name="Bentuk" id="Bentuk" required oninvalid="this.setCustomValidity('Bentuk Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <?php 
                                        if ($datapo->Bentuk != "") {
                                          echo "<option value='".$datapo->Bentuk."' selected>".$datapo->Bentuk."</option>";
                                        }
                                      ?>
                                      <option value="Piece (Double Fold)">Piece (Double Fold)</option>
                                      <option value="Roll (Single Fold)">Roll (Single Fold)</option>
                                  </select>
                                </div>
                                <span class="clear"></span>

                                <label class="field_title">Panjang</label>
                                <div class="form_input">
                                  <select style="width:450%;" class="chzn-select" name="Panjang" id="Panjang" required oninvalid="this.setCustomValidity('Panjang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <?php 
                                        if ($datapo->Panjang != "") {
                                          echo "<option value='".$datapo->Panjang."' selected>".$datapo->Panjang."</option>";
                                        }
                                      ?>
                                      <option value="Standar">Standar</option>
                                      <option value="Besar">Besar</option>
                                  </select>
                                </div>
                                <span class="clear"></span>
                            </div>
                            <div class="form_grid_6">
                                <label class="field_title">Point</label>
                                <div class="form_input">
                                  <select style="width:450%;" class="chzn-select" name="Point" id="Point" required oninvalid="this.setCustomValidity('Point Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <?php 
                                        if ($datapo->Point != "") {
                                          echo "<option value='".$datapo->Point."' selected>".$datapo->Point."</option>";
                                        }
                                      ?>
                                      <option value="Terpisah">Terpisah</option>
                                      <option value="Ikut piece lain">Ikut Piece Lain</option>
                                  </select>
                                </div>
                                <span class="clear"></span>

                                <label class="field_title">Kirim Ke</label>
                                <div class="form_input">
                                  <select style="width:450%;" class="chzn-select" name="Kirim" id="Kirim" required oninvalid="this.setCustomValidity('Kirim Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                      <?php 
                                        if ($datapo->Kirim != "") {
                                          echo "<option value='".$datapo->Kirim."' selected>".$datapo->Kirim."</option>";
                                        }
                                      ?>
                                      <option value="agen">agen</option>
                                      <option value="Langsung Ke Pembeli">Langsung Ke Pembeli</option>
                                  </select>
                                </div>
                                
                            </div>
                            <span class="clear"></span>
                          </div>
                        </div>
                        <!-- //------------------------input Cap Pinggir -->
                        <div id="cappinggir" class="tabcontent">
                          <h3>Cap Pinggir</h3>
                          <div class="form_grid_12 multiline">
                            <div class="form_grid_6">
                                <label class="field_title">Cap Pinggir</label>
                                <div class="form_input">
                                  
                                  <?php if ($datapo->Stamping == "Cap") { ?>
                                    <input type="radio" name="cap" id="cap" value="Cap" checked> Cap
                                    <?php } else { ?>
                                      <input type="radio" name="cap" id="cap" value="Cap"> Cap
                                  <?php }
                                  ?>

                                  <?php if ($datapo->Stamping == "Tanpa Cap") { ?>
                                    <input type="radio" name="cap" id="tanpa_cap" value="Tanpa Cap" checked> Tanpa Cap
                                    <?php } else { ?>
                                      <input type="radio" name="cap" id="tanpa_cap" value="Tanpa cap"> Tanpa Cap
                                  <?php }
                                  ?>
                                  
                                 
                                </div>
                                <span class="clear"></span>
                            </div>
                            <div class="form_grid_6">
                                  <?php if ($datapo->Stamping == "Cap") { ?>
                                    <?php if ($datapo->Posisi == "Selvedge") { ?>
                                      <input type="checkbox" name="_cap1" id="_cap1" value="Selvedge" checked> Selvedge (Pinggir) <br>
                                    <?php } else { ?>
                                      <input type="checkbox" name="_cap1" id="_cap1" value="Selvedge" > Selvedge (Pinggir) <br>
                                    <?php } ?>
                                      
                                    <?php if ($datapo->Posisi1 == "Face") { ?>
                                      <input type="checkbox" name="_cap2" id="_cap2" value="Face" checked> Face (Kepala) 
                                    <?php }  else { ?>
                                      <input type="checkbox" name="_cap2" id="_cap2" value="Face" > Face (Kepala)
                                    <?php } ?>
                                    
                                  <?php } else { ?>
                                    <input type="checkbox" name="_cap1" id="_cap1" value="Selvedge" disabled> Selvedge (Pinggir) <br>
                                    <input type="checkbox" name="_cap2" id="_cap2" value="Face" disabled> Face (Kepala)
                                  <?php }?>

                               
                               
                            </div>
                            <span class="clear"></span>
                          </div>
                          <div class="form_grid_12 multiline">
                            <div class="form_grid_12">
                                <label class="field_title"><b>Keperluan Sample Sesuai Standar yang Berlaku</b></label>
                                <div class="form_input">
                                  <select style="width:450%;" class="chzn-select" name="Sample" id="Sample" required oninvalid="this.setCustomValidity('keperluan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                        <?php
                                          echo '<option value="'.$datapo->Sample.'" selected>'.$datapo->Sample.'</option>'
                                        ?>
                                        <option value="Album">Album</option>
                                        <option value="M10/20">M10/20</option>
                                        <option value="Kain">Kain</option>
                                        <option value="Lembaran">Lembaran</option>
                                        <option value="Hanger">Hanger</option>
                                    </select>
                                </div>
                                <span class="clear"></span>
                            </div>
                            <span class="clear"></span>
                          </div>
                        </div>
                        <!-- //------------------------input Catatan -->
                        <div id="catatan" class="tabcontent">
                          <h3>Catatan</h3>
                          <div class="form_grid_12 multiline">
                            <div class="form_grid_12">
                                <div class="form_input">
                                <textarea name="Keterangan" id="Keterangan" cols="142" rows="10" tabindex="5"><?php echo $datapo->Keterangan ?></textarea>
                                </div>
                                <span class="clear"></span>
                            </div>
                            <div class="form_grid_12">
                                <div class="form_grid_6">
                                  <b><label class="field_title">Untuk Keperluan PIC & diisi oleh SCH_SO</label> </b> <br>
                                  <input type="checkbox" name="" id="" value="a"> A 
                                  <input type="checkbox" name="" id="" value="b"> B 
                                  <input type="checkbox" name="" id="" value="c"> C
                                </div>
                                <div class="form_grid_6">
                                  <div class="form_input">
                                    <div class="btn_30_blue">
                                        <span><input name="simpan" onclick="save_change()" type="button" class="btn_small btn_blue" value="Simpan Data"></span>
                                    </div>
                                    <div class="btn_30_light">
                                        <span><input type="button" class="btn_small" id="print" value="Print"></span>
                                    </div>
                                    <div class="btn_30_orange">
                                        <span><a href="<?php echo base_url() ?>Po" name="simpan" title=".classname">Kembali</a></span>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <span class="clear"></span>
                          </div>
                        </div>
                    </ul>
                </form>
              </div>
          </div>
      </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/po.js"></script>
<script>
    $(document).ready(function(){
        renderJSONE($.parseJSON('<?php echo json_encode($datapodetail) ?>'));
        //console.log(<?php echo json_encode($datapodetail) ?>);
    });
</script>
<?php $this->load->view('administrator/footer') ; ?>