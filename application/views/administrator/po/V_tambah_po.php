<?php $this->load->view('administrator/header') ; 


if ($kodepo->curr_number == null) {
  $number = 1;
} else {
  $number = $kodepo->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_po = 'PO'.date('y').str_replace(' ', '',$agen).$kodemax;
?>
<style type="text/css" scoped>
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}
.tab a {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
  color:black;
}
.tab a:hover {
  background-color: #ddd;
}
.tab a.active {
  background-color: #ccc;
}
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
.chzn-container {
  width: 103.5% !important;
}
#loading-status{
 position:fixed;
 top:40%;
 right:40%;
 background-color:#FFF;
 border:3px solid #000;
 padding:5px 7px;
 border-radius:5px;
 -moz-border-radius: 5px; 
 -webkit-border-radius: 5px;
 z-index:3000;
 display:none;
}
.table_bottom{
  margin-top: 3rem;
}
a.disabled {
   pointer-events: none;
   cursor: default;
}
</style>

<!--========================Head Content -->
<div class="page_title">
 <div id="alert"></div>
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3>Data Purchase Order</h3>
 <div class="top_search">
 </div>
</div>
<!--=======================Content-->
<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <!-- =============================Title -->
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>Po">Data Purchase Order</a></li>
              <li class="active">Tambah Purchase Order</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6>Tambah Data Purchase Order</h6>
          </div>
        </div>
        <!-- =============================content -->
        <div class="widget_content">
          <form method="post" action="" class="form_container left_label">
            <ul>

              <li class="body-search-data">
                <div class="form_grid_12 w-100 alpha">

                  <div class="form_grid_10">
                    <!-- form 1 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Tanggal</label>
                      <div class="form_input">
                        <input id="Tanggal" type="date" name="Tanggal" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                    <!-- form 2 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Supplier</label>
                      <div class="form_input input-not-focus">
                        <input type="hidden" name="IDSupplier" id="IDSupplier" value="<?php echo $trisula->IDSupplier ?>" readonly>
                        <input type="text" name="namaSupplier" value="<?php echo $trisula->Nama ?>" readonly>
                      </div>
                    </div>

                    <!-- form 3 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Agent</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="IDAgen" id="IDAgen" class="form-control" data-id="<?php echo $namaagent->IDAgen  ?>" value="<?php echo $agen ?>" readonly>
                      </div>
                    </div>

                    <!-- form 4 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">No PO</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Asset" value="<?php echo $code_po ?>" readonly>
                      </div>
                    </div>

                    <!-- form 5 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Corak<font style="color: red"> *</font></label>
                      <div class="form_input">
                        <select style="width:100%;" class="chzn-select" name="corak" id="corak" required oninvalid="this.setCustomValidity('Corak Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <option value="" selected>--Pilih Corak--</option>
                          <?php foreach($corak as $row) {
                            echo "<option value='$row->IDCorak'>$row->Corak</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <!-- form 6 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Lot</label>
                      <div class="form_input">
                        <input type="text" name="Lot" id="Lot" class="form-control" placeholder="Lot" value="-">
                      </div>
                    </div>

                    <!-- form 7 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title">Tanggal Selesai</label>
                      <div class="form_input">
                        <input id="Tanggal_selesai" type="date" name="Tanggal_selesai" class="form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                    <!-- form 8 -->
                    <!-- <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Merk</label>
                      <div class="form_input input-not-focus"> -->
                        <input type="hidden" name="merk" id="merk" data-idmerk data-idbarang class="form-control" placeholder="Merk" readonly>
                      <!-- </div>
                    </div> -->

                    <!-- form 9 -->
                    <div class="form_grid_4 mb-1">
                      <label class="field_title mt-dot2">Total</label>
                      <div class="form_input input-not-focus">
                        <input type="text" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="form_grid_2">

                    <!-- radio 1 -->
                     <div class="form_grid_12 mb-1">
                      <div class="form_input radio-padding-form">
                        <span>
                          <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="stok" checked>
                          <label class="choice">Stok Kain</label>
                        </span>
                      </div>
                    </div>
                    

                    <!-- radio 2 -->
                   <div class="form_grid_12 mb-1">
                      <div class="form_input radio-padding-form">

                        <span>
                          <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="seragam">
                          <label class="choice">Seragam</label>
                        </span>

                      </div>
                    </div>

                    <!-- radio 3 -->
                    <div class="form_grid_12 mb-1">
                      <div class="form_input radio-padding-form">
                        <span>
                          <input type="radio" class="radio" name="Jenis_Pesanan" id="Jenis_Pesanan" value="garment">
                          <label class="choice">Garment</label>
                        </span>
                      </div>
                    </div>
                  </div>

                  <!-- clear content -->
                  <span class="clear"></span>
                </div>
              </li>

            </ul>

            <div class="grid_12 mx w-100">
              <div class="widget_wrap tabby">
                <div class="widget_top">
                <!-- <span class="h_icon bended_arrow_right"></span>
                  <h6>Tabby Widget</h6> -->
                  <div id="widget_tab">
                    <ul>
                      <li class="px py"><a href="#tab1" id="tab_1" class="active_tab tab_1">Input Warna</a></li>
                      <li class="px py"><a href="#tab2" id="tab_2" class="disabled">Metode Packing</a></li>
                      <li class="px py"><a href="#tab3" id="tab_3" class="disabled">Cap Pinggir</a></li>
                      <li class="px py"><a href="#tab4" id="tab_4" class="disabled">Catatan</a></li>
                    </ul>
                  </div>
                </div>
                <div class="widget_content">

                  <!-- tab & pane 3 -->
                  <div id="tab1">
                    <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                      <div class="grid_12 w-100">
                        <span class="h_icon blocks_images"></span>
                        <h6>Warna</h6>
                      </div>
                    </div>
                    <div class="form_grid_12 w-100 alpha">
                      <div class="form_grid_4 mb-1">
                        <ul>
                          <li>
                            <div class="form_grid_12 mb-1">
                              <label class="field_title mt-dot2">Warna <font style="color: red"> *</font></label>
                              <div class="form_input">
                               <select name="warna" id="warna" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                <option value="">--Silahkan Pilih Corak Dahulu--</option>
                              </select>
                            </div>
                          </div>
                          <div class="form_grid_12 mb-1">
                            <span class="input-group-addon" style="right: -5px; z-index: 1">Yard</span>
                            <label class="field_title mt-dot2">Qty Yard<font style="color: red"> *</font></label>
                            <div class="form_input">
                              <input type="text" name="Qty" id="Qty" class="form-control" placeholder="Qty" onkeyup="this.value=this.value.replace(/[^0-9.]/,'')">
                            </div>
                          </div>
                          <div class="form_grid_12 mb-1">
                            <label class="field_title mt-dot2">Harga <font style="color: red"> *</font></label>
                            <div class="form_input">
                              <input type="text" name="Harga" id="harga-123" class="form-control" placeholder="Harga">
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="form_input">
                            <div class="btn_24_blue">
                              <button class="btn_24_blue" type="button" id="tambah_sementara">
                                Tambah Data
                              </button>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="form_grid_8 mb-1 mr" style="width: 65.6%">
                      <table class="display data_tbl" id="tabelfsdfsf">
                        <thead>
                          <tr>
                            <th class="center" style="width: 40px">No</th>
                            <th>Warna</th>
                            <th>Qty Yard</th>
                            <th>Harga</th>
                            <th>Total</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="tabel-cart-asset">

                        </tbody>
                      </table>

                      <div class="form_grid_12 w-100 alpha">


                      <!-- form 1 -->
                      <div class="form_grid_4 mb-1" style="right: 15px; bottom: 5.5rem; float: right;">
                        <div class="form_input">
                          <input type="text" name="Totalwarna" id="Totalwarna" class="form-control" placeholder="Grand Total" required readonly>
                        </div>
                      </div>
                    </div>
                    <span class="clear"></span>
                  </div>
                  <span class="clear"></span>
                </div>
                <div class="widget_top" style="margin-top: -10px">
                  <div class="grid_12" style="margin-top: -10px">
                    <span class="h_icon blocks_images"></span>
                    <h6>Delivery</h6>
                  </div>
                </div>

                <ul>
                  <li>
                    <div class="form_grid_12 w-100 alpha">
                      <div class="form_grid_6 mb-1">
                        <label class="field_title mt-dot2">PIC <font style="color: red"> *</font></label>
                        <div class="form_input">
                          <input type="text" id="PIC" name="PIC" class="form-control" placeholder="PIC" onkeyup="cek_tab()">
                        </div>
                      </div>
                      <span class="clear"></span>
                    </div>

                    <div class="form_grid_12 w-100 alpha">
                      <div class="form_grid_6 mb-1">
                        <div class="form_input mb-4">
                          <input type="radio" class="radio" name="Prioritas" id="Prioritas" value="normal" checked> 
                          <label class="choice">Normal</label>
                          <input type="radio" class="radio" name="Prioritas" id="Prioritas" value="urgent">
                          <label class="choice">Urgent</label>
                        </div>
                        <span class="clear"></span>
                      </div>
                    </div>
                  </li>
                </ul>
                <span class="clear"></span>
              </div>
            </div>

            <!-- tab & pane 2 -->
            <div id="tab2">
              <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                <div class="grid_12 w-100">
                  <span class="h_icon blocks_images"></span>
                  <h6>Metode Packing & Pengiriman</h6>
                </div>
              </div>

              <ul>
                <li>

                  <div class="form_grid_12 multiline">
                    <div class="form_grid_6">
                      <label class="field_title dot-2">Bentuk <font style="color: red"> *</font></label>
                      <div class="form_input">
                        <select style="width:100%;" class="form-control" placeholder="Cari Bentuk" name="Bentuk" id="Bentuk" required oninvalid="this.setCustomValidity('Bentuk Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <option value="-">-</option>
                          <option value="Piece (Double Fold)" selected>Piece (Double Fold)</option>
                          <option value="Roll (Single Fold)">Roll (Single Fold)</option>
                        </select>
                      </div>
                      <span class="clear"></span>

                      <label class="field_title dot-2">Panjang <font style="color: red"> *</font></label>
                      <div class="form_input">
                        <select style="width:100%;" class="form-control" placeholder="Cari Panjang" name="Panjang" id="Panjang" required oninvalid="this.setCustomValidity('Panjang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                           <option value="-">-</option>
                          <option value="Standar" selected>Standar</option>
                          <option value="Besar">Besar</option>
                        </select>
                      </div>
                      <span class="clear"></span>
                    </div>
                    <div class="form_grid_6">
                      <label class="field_title dot-2">Point <font style="color: red"> *</font></label>
                      <div class="form_input">
                        <select style="width:100%;" class="form-control" placeholder="Cari Point" name="Point" id="Point" required oninvalid="this.setCustomValidity('Point Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                           <option value="-">-</option>
                          <option value="Terpisah">Terpisah</option>
                          <option value="Ikut piece lain" selected>Ikut Piece Lain</option>
                        </select>
                      </div>
                      <span class="clear"></span>

                      <label class="field_title dot-2">Kirim Ke <font style="color: red"> *</font></label>
                      <div class="form_input">
                        <select style="width:450%;" class="form-control" placeholder="Cari Pengirim" name="Kirim" id="Kirim" required oninvalid="this.setCustomValidity('Kirim Tidak Boleh Kosong')" oninput="setCustomValidity('')"  onchange="cek_tab()">
                          <option value="-">-</option>
                          <option value="Agen" selected>Agen</option>
                          <option value="Langsung Ke Pembeli">Langsung Ke Pembeli</option>
                          <option value="Gudang Titipan">Gudang Titipan</option>
                        </select>
                      </div>

                    </div>
                    <span class="clear"></span>
                  </div>
                </li>
              </ul>
            </div>
            <!-- end tab & pane -->

            <!-- tab & pane 3 -->
            <div id="tab3">
              <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                <div class="grid_12 w-100">
                  <span class="h_icon blocks_images"></span>
                  <h6>Cap Pinggir</h6>
                </div>
              </div>

              <ul>
                <li>
                  <div class="form_grid_12 multiline">
                    <div class="form_grid_6">
                      <label class="field_title">Cap Pinggir <font style="color: red"> *</font></label>
                      <div class="form_input">

                        <input type="radio" class="radio" name="cap" id="cap" value="Cap" onchange ="cek_tab()" checked>
                        <label class="choice">Cap</label>
                        <input type="radio" class="radio" name="cap" id="tanpa_cap" value="Tanpa Cap">
                        <label class="choice">Tanpa Cap</label>

                      </div>
                      <span class="clear"></span>
                    </div>
                    <div class="form_grid_6">
                      <input type="checkbox" name="_cap1" id="_cap1" value="Selvedge" checked> Selvedge (Pinggir) <br>
                      <input type="checkbox" name="_cap2" id="_cap2" value="Face" checked> Selvedge (Kepala)
                    </div>
                    <span class="clear"></span>
                  </div>
                </li>
              </ul>

              <!-- tab & pane -->
              <div class="widget_top">
                <div class="grid_12">
                  <span class="h_icon blocks_images"></span>
                  <h6>Keperluan Sample Sesuai Standar yang Berlaku</h6>
                </div>
              </div>
              <ul>
                <li>
                  <div class="form_grid_12 alpha">
                    <div class="form_grid_6">
                      <label class="field_title dot-2">Album <font style="color: red"> *</font></label>
                      <div class="form_input mb-4">
                       <!--  <select style="width:100%;" class="chzn-select" name="Sample" id="Sample" required oninvalid="this.setCustomValidity('keperluan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                          <?php
                          //echo '<option value="'.$datapo->Sample.'" selected>'.$datapo->Sample.'</option>'
                          ?>
                          <option value="Album" selected>Album</option>
                          <option value="M10/20">M10/20</option>
                          <option value="Kain">Kain</option>
                          <option value="Lembaran">Lembaran</option>
                          <option value="Hanger">Hanger</option>
                        </select> -->
                        <input type="text" name="Sample" id="Sample" onkeyup="cek_tab()">
                      </div>
                      <span class="clear"></span>
                    </div>

                     <div class="form_grid_6">
                      <label class="field_title dot-2">M10/20 <font style="color: red"> *</font></label>
                      <div class="form_input mb-4">
                        <input type="text" name="M10" id="M10">
                      </div>
                      <span class="clear"></span>
                    </div>

                     <div class="form_grid_6">
                      <label class="field_title dot-2">Kain <font style="color: red"> *</font></label>
                      <div class="form_input mb-4">
                        <input type="text" name="Kain" id="Kain">
                      </div>
                      <span class="clear"></span>
                    </div>

                    <div class="form_grid_6">
                      <label class="field_title dot-2">Lembaran <font style="color: red"> *</font></label>
                      <div class="form_input mb-4">
                        <input type="text" name="Lembaran" id="Lembaran" onkeyup ="cek_tab()">
                      </div>
                      <span class="clear"></span>
                    </div>
                    <span class="clear"></span>
                  </div>
                </li>
              </ul>
            </div>

            <div id="tab4">
              <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                <div class="grid_12 w-100">
                  <span class="h_icon blocks_images"></span>
                  <h6>Catatan</h6>
                </div>
              </div>
              <ul>
                <li>

                  <div class="form_grid_12 multiline">
                    <div class="form_grid_12">
                      <label class="field_title dot-2">Catatan <font style="color: red"> *</font></label>
                      <div class="form_input">
                        <textarea name="Keterangan" id="Keterangan" cols="142" rows="10" tabindex="5" style="resize: none;" onkeyup="cek_tab()"></textarea>
                      </div>
                      <span class="clear"></span>
                    </div>

                    <span class="clear"></span>
                  </div>
                </li>
              </ul>
              <!-- <div class="widget_top">
                <div class="grid_12">
                  <span class="h_icon blocks_images"></span>
                  <h6>Untuk Keperluan PIC & diisi oleh SCH_SO</h6>
                </div>
              </div> -->
              <ul>
                <!-- <li>
                  <label class="field_title">Untuk Keperluan PIC & diisi oleh SCH_SO</label>
                  <div class="form_input">
                    <span>
                      <input class="checkbox" type="checkbox" value="a">
                      <label class="choice">A</label>
                    </span>
                    <span>
                      <input class="checkbox" type="checkbox" value="b">
                      <label class="choice">B</label>
                    </span>
                    <span>
                      <input class="checkbox" type="checkbox" value="c">
                      <label class="choice">C</label>
                    </span>
                  </div>
                </li> -->
                <li>

                  <div class="form_grid_12">
                    <div class="text-right">
                     <!-- <div class="btn_30_blue">
                      <span><a id="print" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                    </div> -->
                    <div class="btn_30_blue">
                      <span><a name="simpan" onclick="save()" class="btn_24_blue" style="cursor: pointer;" id="simpan">Simpan</a></span>
                    </div>

                  </div>

                  <span class="clear"></span>
                </div>

              </li>
            </ul>
          </div>

        </div>
      </div>
      <!-- end tab & pane -->

    </form>
  </div>
</div>
</div>
</div>
</div>

 <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/po.js"></script>
<div id="loading-status">
 <table>
  <tr>
   <td>Mohon tunggu...</td>
 </tr>
</table>
</div>
<?php $this->load->view('administrator/footer') ; ?>