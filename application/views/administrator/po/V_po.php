<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
  /*  .table_top{
        display: none !important;
        }*/
    </style>
    <!-- tittle data -->
    <div class="page_title">
       <?php echo $this->session->flashdata('Pesan');?>
       <span class="title_icon"><span class="blocks_images"></span></span>
       <h3><?= $title_master ?></h3>
       <div class="top_search">

       </div>
   </div>
   <!-- ======================= -->
        <?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
   <!-- body data -->
   <div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                          <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                          <li class="active"><?= $title ?></li>
                      </ul>
                  </div>
              </div>

              <div class="widget_content">
                <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Po/pencarian" class="form_container left_label">
                    <ul>
                        <li class="px-1">

                            <!-- search -->
                            <div class="form_grid_12 w-100 alpha">

                                <!-- button -->
                                <div class="form_grid_3 ">
                                    <div class="btn_24_blue">
                                        <a href="<?php echo base_url('po/create')?>"><span>Tambah Data</span></a>
                                    </div>
                                </div>

                                <div class="form_grid_9 ">

                                    <!-- form 1 -->
                                    <div class="form_grid_3 ">   
                                        <label class="field_title mt-dot2">Periode</label>
                                        <div class="form_input">
                                            <input type="date" class="input-date-padding form-control date-picker hidden-sm-down" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                        </div>
                                    </div>

                                    <!-- form 2 -->
                                    <div class="form_grid_3 ">   
                                        <label class="field_title mt-dot2"> S/D </label>
                                        <div class="form_input">
                                            <input type="date" class="input-date-padding form-control date-picker hidden-sm-down" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                        </div>
                                    </div>

                                    <div class="form_grid_3 ">   
                                        <select name="jenispencarian" data-placeholder="No PO" style="width: 100%!important" class="chzn-select" tabindex="13">
                                            <option value="Nomor">No PO</option>
                                            <option value="Corak">Corak</option>
                                            <option value="Merk">Merk</option>
                                        </select>
                                    </div>
                                    <div class="form_grid_2">   
                                        <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
                                    </div>
                                    <div class="form_grid_1">
                                        <div class="btn_24_blue">
                                            <input type="submit" name="" value="search">
                                        </div>
                                    </div>
                                </div>

                                <span class="clear"></span>
                            </div>
                            <!-- end search -->
                            
                        </li>

                    </form>
                </div>
                <form action="<?php echo base_url() ?>po/delete_multiple" method="post" id="ubahstatus">
                    <div class="widget_content">
                        <table class="display" id="action_tbl">
                            <thead>
                                <tr>
                                    <th class="center">
                                        <input name="checkbox" type="checkbox" value="" class="checkall"> Do
                                    </th>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nomor PO</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Corak</th>
                                    <th>Merk</th>
                                    <th>Total Qty Yard</th>
                                    <th>Total Harga</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($po)){ ?>
                                <!-- <tr>
                                    <td colspan="7"> Data Kosong </td>
                                </tr> -->
                                <?php }else{

                                    $i=1;
                                    foreach ($po as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td class="center tr_select"><input type="checkbox" name="msg[]" value="<?php echo $data->IDPO ?>"></td>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo DateFormat($data->Tanggal); ?></td>
                                            <td><?php echo $data->Nomor; ?></td>
                                            <td><?php echo DateFormat($data->Tanggal_selesai); ?></td>
                                            <td><?php echo $data->Kode_Corak; ?></td>
                                            <td><?php echo $data->Kode_Merk; ?></td>
                                            <td class="text-right"><?php echo convert_currency($data->Total_qty); ?></td>
                                            <td class="text-right"><?php echo convert_currency($data->Grand_total); ?></td>
                                            <td><?php echo $data->Batal; ?></td>
                                            <td class="text-center ukuran-logo">
                                                <span><a class="action-icons c-Detail" href="<?= site_url('Po/show/'.$data->IDPO); ?>" title="Detail">Detail</a></span>
                                                <?php if($data->Batal=='aktif'){ ?>
                                                <span><a class="action-icons c-edit" href="<?= site_url('po/edit/'.$data->IDPO); ?>" title="Edit Data">Edit</a></span>
                                                <?php }else{
                                                    ?>
                                                <span><a class="action-icons c-edit" style="cursor: no-drop;" title="Edit Data">Edit</a></span>
                                                <?php } ?>
                                                <?php if($data->Batal=="aktif"){ ?>
                                                <span><a class="action-icons c-sa" href="<?php echo base_url() ?>po/set_edit/<?php echo $data->IDPO;?>" title="Status Tidak Aktif">Status</a></span>
                                                <?php }elseif($data->Batal=="tidak aktif"){ ?>
                                                <span><a class="action-icons c-approve" href="<?php echo base_url() ?>po/set_edit/<?php echo $data->IDPO;?>" title="Status Aktif">Status</a></span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <div class="widget_content text-right px-2">
                            <div class="py-4 mx-2">
                                <div class="btn_24_blue">
                                    <a href="#" type="button" onclick="document.getElementById('ubahstatus').submit();"><span>Batal</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php 
if ($po=="" || $po==null) {
}else{
    foreach ($po as $data) {
        ?>
        <div id="open-modal<?php echo $data->IDMerk ?>" class="modal-window">
            <div>
                <a href="#modal-close" title="Close" class="modal-close">X</a>
                <h1>Konfirmasi Hapus</h1>
                <div class="modal-body">Apakah anda yakin akan menghapus data ini ?</div>
                <div class="modal-foot">
                    <div class="btn_30_light">
                        <a href="#modal-close" title="Close"><span>Batal</span></a>
                    </div>
                    <div class="btn_30_blue">
                        <a href="<?= site_url('Merk/delete/'.$data->IDMerk); ?>"><span>Hapus</span></a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
    }
} 
?> 
<?php $this->load->view('administrator/footer') ; ?>
