<?php $this->load->view('administrator/header') ;  ?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3><?= $title_master ?></h3>
 
 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>SaldoAwalAsset">Data <?= $title ?></a></li>
              <li class="active">Saldo Awal Asset Tetap</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6><?= $title ?></h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="<?php echo $action; ?>" class="form_container left_label">
            <ul>
              <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title">Asset</label>
                  <div class="form_input">

                    <select data-placeholder="Pilih Asset" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDAsset" required oninvalid="this.setCustomValidity('Asset Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                     <option value=""></option>
                     <?php 
                     foreach ($DataAsset as $key ) { 
                      if ($key->IDAsset == $IDAsset) { ?>
                      <option value="<?= $key->IDAsset ?>" selected> <?= 'Nama Asset : '.$key->Nama_Asset ?></option> 
                      <!-- Bila Data Sudah isi sebelumnya -->
                      <?php }
                      ?>
                      <option value="<?= $key->IDAsset ?>"><?= 'Nama Asset : '.$key->Nama_Asset ?></option>
                      
                      <?php }
                      ?>
                    </select>
                    <span class="clear"></span>
                  </div>

                  <label class="field_title">Nilai Saldo Awal</label>
                  <div class="form_input">

                     <span class="input-group-addon">Rp. </span>
                                    <input type="text" class="form-control price-date" name="Nilai_Saldo_awal" id="tanpa-rupiah-debit" placeholder=" Nilai Saldo Awal" value="<?php echo $Nilai_Saldo_awal; ?>" required oninvalid="this.setCustomValidity('Nilai Hutang Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                    <span class="clear"></span>
                  </div>

                  <label class="field_title">Tanggal Perolehan</label>
                  <div class="form_input">

                       <input type="date" class="form-control date-picker hidden-sm-down" name="Tanggal_Perolehan" id="Tanggal_Perolehan" placeholder="tanggal" value="<?php echo $Tanggal_Perolehan; ?>" required oninvalid="this.setCustomValidity('Tanggal Perolehan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>
                 <label class="field_title">Tanggal Penyusutan</label>
                  <div class="form_input">

                       <input type="date" class="form-control date-picker hidden-sm-down" name="Tanggal_Penyusutan" id="Tanggal_Penyusutan" placeholder="tanggal" value="<?php echo $Tanggal_Penyusutan; ?>" required oninvalid="this.setCustomValidity('Tanggal Penyusutan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Qty</label>
                  <div class="form_input">

                       <input type="number" class="form-control" name="Qty" id="Qty" placeholder=" Qty" value="<?php echo $Qty; ?>" required oninvalid="this.setCustomValidity('Qty Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Umur</label>
                  <div class="form_input">

                       <input type="number" class="form-control" name="Umur" id="Umur" placeholder=" Umur" value="<?php echo $Umur; ?>" required oninvalid="this.setCustomValidity('Umur Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Nilai Penyusutan Asset</label>
                  <div class="form_input">

                      <span class="input-group-addon">Rp. </span>
                                    <input type="text" class="form-control price-date" name="Nilai_Penyusutan_Asset" id="tanpa-rupiah-kredit" placeholder=" Nilai Penyusutan Asset" value="<?php echo $Nilai_Penyusutan_Asset; ?>" required oninvalid="this.setCustomValidity('Pembayaran Tidak Boleh Kosong')" oninput="setCustomValidity('')"/>
                   <span class="clear"></span>
                 </div>

                    <label class="field_title">Metode</label>
                                <div class="form_input">
                                    <select data-placeholder="Pilih Metode" name="Metode" required oninvalid="this.setCustomValidity('Metoode Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                        <option value=""></option>
                                        <?php if($Metode=="straight line"){ ?>
                                        <option value="straight line" selected>Straight Line</option>
                                        <option value="double decline">Dounle Decline</option>
                                        <?php }elseif($Metode=="double decline"){ ?>
                                        <option value="straight line">Straight Line</option>
                                        <option value="double decline" selected>Dounle Decline</option>
                                        <?php }else{ ?>
                                        <option value="straight line">Straight Line</option>
                                        <option value="double decline">Dounle Decline</option>
                                        <?php } ?>
                                    </select>
                                </div>
               </div>
             </li>
             <li>
              <div class="form_grid_12">
                <div class="form_input">
                  <div class="btn_30_light">
                    <input type="hidden" name="IDSaldoAwalAsset" value="<?php echo $IDSaldoAwalAsset; ?>" /> 
                    <span> <a href="<?php echo base_url() ?>SaldoAwalAsset" name="simpan">Kembali</a></span>
                  </div>
                  <div class="btn_30_blue">
                    <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>