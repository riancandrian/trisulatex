<?php $this->load->view('administrator/header') ; 

?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
   <form action="<?php echo base_url() ?>SettingLR/cari_bulan_cf" method="post">
    <br>
   <select name="bulan" class="form-control">
      <option value="">Pilih Bulan</option>
      <option value="1" <?php if($bulan=="1"){echo 'selected';}else{ } ?>>Januari</option>
      <option value="2" <?php if($bulan=="2"){echo 'selected';}else{ } ?>>Februari</option>
      <option value="3" <?php if($bulan=="3"){echo 'selected';}else{ } ?>>Maret</option>
      <option value="4" <?php if($bulan=="4"){echo 'selected';}else{ } ?>>April</option>
      <option value="5" <?php if($bulan=="5"){echo 'selected';}else{ } ?>>Mei</option>
      <option value="6" <?php if($bulan=="6"){echo 'selected';}else{ } ?>>Juni</option>
      <option value="7" <?php if($bulan=="7"){echo 'selected';}else{ } ?>>Juli</option>
      <option value="8" <?php if($bulan=="8"){echo 'selected';}else{ } ?>>Agustus</option>
      <option value="9" <?php if($bulan=="9"){echo 'selected';}else{ } ?>>September</option>
      <option value="10" <?php if($bulan=="10"){echo 'selected';}else{ } ?>>Oktober</option>
      <option value="11" <?php if($bulan=="11"){echo 'selected';}else{ } ?>>November</option>
      <option value="12" <?php if($bulan=="12"){echo 'selected';}else{ } ?>>Desember</option>
    </select>
    <?php
  $now= date('Y');
  ?>
    <select name="tahun" class="form-control">
    <?php
    for ($a=2019;$a<=$now;$a++)
    {
      ?>
       <option value="<?php echo $a ?>" <?php if($tahun==$a){echo 'selected';}else{ } ?>><?php echo $a ?></option>
    <?php
   }
   ?>
 </select>
    <input type="submit" value="Submit"><br><br>
  </form>

 <div class="top_search">
 </div>
</div>

<div id="content">
     <center><h1>Laporan Arus Kas</h1></center><br>
<div class="grid_container">
    <div class="table-responsive">
  <?php
  if($this->input->post('bulan') != ""){

    if($bulan_ini=="1"){
        $bulan_sekarang="Januari";
    }elseif($bulan_ini=="2"){
        $bulan_sekarang="Februari";
    }elseif($bulan_ini=="3"){
        $bulan_sekarang="Maret";
    }elseif($bulan_ini=="4"){
        $bulan_sekarang="April";
    }elseif($bulan_ini=="5"){
        $bulan_sekarang="Mei";
    }elseif($bulan_ini=="6"){
        $bulan_sekarang="Juni";
    }elseif($bulan_ini=="7"){
        $bulan_sekarang="Juli";
    }elseif($bulan_ini=="8"){
        $bulan_sekarang="Agustus";
    }elseif($bulan_ini=="9"){
        $bulan_sekarang="September";
    }elseif($bulan_ini=="10"){
        $bulan_sekarang="Oktober";
    }elseif($bulan_ini=="11"){
        $bulan_sekarang="November";
    }elseif($bulan_ini=="12"){
        $bulan_sekarang="Desember";
    }
  ?>
   <table class="display">

      <thead style="font-size: 13px;">

        <tr>

          <th>Keterangan</th>
          <th><?php echo $bulan_sekarang." ".$tahun ?></th>
          <th>s/d <?php echo $bulan_sekarang." ".$tahun ?></th>

        </tr>

      </thead>

      <tbody style="font-size: 13px">
       <?php
       $hasil_bulan_ini=0;
       $hasil_sampai_bulan_ini=0;
       $total_bulan_ini12=0;
       $total_sampai_bulan_ini12=0;
       foreach($set_lr as $lr)
       {

            if($lr->kategori=="judul"){
              $hasil_bulan_ini=0;
            }elseif($lr->kategori=="data"){
              $hasil_bulan_ini += $lr->total_bulan_ini;
            }

            if($lr->kategori=="judul"){
              $hasil_sampai_bulan_ini=0;
            }elseif($lr->kategori=="data"){
              $hasil_sampai_bulan_ini += $lr->total_tahun_lalu;
            }

             if($lr->kategori=="hasil"){
              $total_bulan_ini12 += $hasil_bulan_ini;
            }

            if($lr->kategori=="hasil"){
              $total_sampai_bulan_ini12 += $hasil_sampai_bulan_ini;
            }
        ?>
        <tr style="border-bottom: 1px solid #000;">
          <td>
            <?php 
           if($lr->kategori=="judul"){
              echo "<b>- ".$lr->keterangan."<b>"; 
         }else if($lr->kategori=="data"){
              echo "-- ".$lr->keterangan; 
            }else if($lr->kategori=="hasil"){
             echo "<b>--- ".$lr->keterangan."</b>"; 
           }else if($lr->kategori=="spasi"){
             echo "---- ".$lr->keterangan; 
           }else if($lr->kategori=="total"){
             echo "<b>----- ".$lr->keterangan."</b>"; 
            }
            ?></td>
            <td class="text-right"><?php 
            $hasil= 0;
            if($lr->kategori=="data"){
             if($lr->total_bulan_ini != ""){
              if($lr->total_bulan_ini < 0){
              echo "(".convert_currency(ltrim($lr->total_bulan_ini,"-")).")"; 
            }else{
              echo convert_currency($lr->total_bulan_ini); 
            }
            }else{
              echo 0;
            } 
          }
          elseif($lr->kategori=="hasil"){
            if($hasil_bulan_ini < 0){
           echo "(".convert_currency(ltrim($hasil_bulan_ini,"-")).")"; 
           }else{
            echo  convert_currency($hasil_bulan_ini);
           } 
         }elseif($lr->kategori=="total"){
          if($total_bulan_ini12 < 0){
          echo "(".convert_currency(ltrim($total_bulan_ini12,"-")).")"; 
        }else{
          echo convert_currency($total_bulan_ini12);
        }
        }
        else{
          echo "";
        } ?></td>
            <td class="text-right"><?php 
            $hasil= 0;
            if($lr->kategori=="data"){
             if($lr->total_tahun_lalu != ""){
              if($lr->total_tahun_lalu < 0){
               echo "(".convert_currency(ltrim($lr->total_tahun_lalu,"-")).")"; 
            }else{
               echo convert_currency($lr->total_tahun_lalu); 
            }
            }else{
              echo 0;
            } 
          }elseif($lr->kategori=="hasil"){
            if($hasil_sampai_bulan_ini < 0){
           echo "(".convert_currency(ltrim($hasil_sampai_bulan_ini,"-")).")"; 
         }else{
           echo  convert_currency($hasil_sampai_bulan_ini); 
         }
         }elseif($lr->kategori=="total"){
          if($total_sampai_bulan_ini12 < 0){
          echo "(".convert_currency(ltrim($total_sampai_bulan_ini12,"-")).")"; 
        }else{
          echo convert_currency($total_sampai_bulan_ini12);
        }
        }else{
          echo "";
        } ?></td>
        
          </tr>
          <?php
        }
        ?>
      </tr>
    </tbody>

  </table>
  <?php
}
?>

</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; 

?>