
<?php $this->load->view('administrator/header') ; ?>

<!-- tittle Data -->
<div class="page_title">

    <!-- notify -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- ====== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Group Barang</h3>
</div>
<!-- =========== -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>GroupBarang">Data Group Barang</a></li>
                            <!-- <li><a href="#">Product Discovery</a></li>
                            <li><a href="#">Life Science Products / Laboratory Supplies</a></li>
                            <li><a href="#">Kits and Assays</a></li>
                            <li><a href="#">Mutagenesis Kits</a></li> -->
                            <li> Edit Group Barang </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Edit Data Group Barang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="<?php echo base_url() ?>GroupBarang/proses_edit_group_barang/<?php echo $edit->IDGroupBarang ?>" enctype="multipart/form-data" class="form_container left_label">
                        <ul>
                            <li>
                                <div class="form_grid_12 multiline">
                                    <!-- Kode Group Barang -->
                                    <label class="field_title">Kode Group Barang</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Kode Group Barang" name="kode" value="<?php echo $edit->Kode_Group_Barang ?>" required oninvalid="this.setCustomValidity('Kode Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Group Barang -->
                                    <label class="field_title">Group Barang</label>
                                    <div class="form_input">
                                        <input type="text" class="form-control" placeholder="Group Barang" name="nama" value="<?php echo $edit->Group_Barang ?>" required oninvalid="this.setCustomValidity('Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                    </div>

                                    <!-- Status -->
                                 <!--    <label class="field_title">Status</label>
                                    <div class="form_input">
                                        <select data-placeholder="Pilih Group Customer" name="status" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="width:100%;" class="chzn-select" tabindex="13">
                                            <option value=""></option>
                                            <?php// if ($edit->Aktif == 'aktif') {
                                               // echo '<option selected value="aktif" selected>Aktif</option>
                                                //<option value="tidak aktif">Tidak aktif</option>';
                                            }//else{
                                               // echo '<option value="aktif">Aktif</option><option selected value="tidak aktif">Tidak Aktif</option>';
                                            } ?>
                                        </select>
                                    </div> -->
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <div class="btn_30_light">
                                            <span> <a href="<?php echo base_url() ?>GroupBarang" name="simpan" title=".classname">Kembali</a></span>
                                        </div>
                                        <div class="btn_30_blue">
                                            <span><input type="submit" name="simpan" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('administrator/footer') ; ?>