<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
 <?php echo $this->session->flashdata('Pesan');?>
 <span class="title_icon"><span class="blocks_images"></span></span>
 <h3><?= $title_master ?></h3>
 
 <div class="top_search">
 </div>
</div>
<!-- ======================= -->

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="breadCrumbHolder module">
          <div id="breadCrumb0" class="breadCrumb module white_lin">
            <ul>
              <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
              <li><a href="<?php echo base_url() ?>MenuDetail"><?= $title ?></a></li>
              <li class="active">Tambah Menu Detail</li>
            </ul>
          </div>
        </div>
        <div class="widget_top">
          <div class="grid_12">
            <span class="h_icon blocks_images"></span>
            <h6><?= $title ?></h6>
          </div>
        </div>
        <div class="widget_content">
          <form method="post" action="<?php echo $action; ?>" class="form_container left_label">
            <ul>
              <li>
                <div class="form_grid_12 multiline">
                  <label class="field_title">Nama Menu</label>
                  <div class="form_input">

                    <select data-placeholder="Pilih Nama Menu" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDMenu" required oninvalid="this.setCustomValidity('Status Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                     <option value=""></option>
                     <?php 
                     foreach ($data_menu as $key ) { 
                      if ($key->IDMenu == $IDMenu) { ?>
                      <option value="<?= $key->IDMenu ?>" selected> <?= $key->Menu ?></option> 
                      <!-- Bila Data Sudah isi sebelumnya -->
                      <?php }
                      ?>
                      <option value="<?= $key->IDMenu ?>"> <?= $key->Menu ?></option>
                      
                      <?php }
                      ?>
                    </select>
                    <span class="clear"></span>
                  </div>

                  <label class="field_title">Detail Menu</label>
                  <div class="form_input">

                   <input type="text" class="form-control" name="MenuDetail" id="MenuDetail" placeholder="Nama Menu" value="<?php echo $MenuDetail; ?>" required oninvalid="this.setCustomValidity('Detail Menu Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                  <label class="field_title">URL</label>
                  <div class="form_input">

                   <input type="text" class="form-control" name="Url" id="Url" placeholder="URL" value="<?php echo $Url; ?>" required oninvalid="this.setCustomValidity('Detail Menu Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>

                 <label class="field_title">Urutan Menu</label>
                  <div class="form_input">

                   <input type="text" class="form-control" name="Urutan" id="Urutan" placeholder="Urutan" value="<?php echo $Urutan; ?>" required oninvalid="this.setCustomValidity('Detail Menu Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                   <span class="clear"></span>
                 </div>
               </div>
             </li>
             <li>
              <div class="form_grid_12">
                <div class="form_input">
                  <div class="btn_30_light">
                    <input type="hidden" name="IDMenuDetail" value="<?php echo $IDMenuDetail; ?>" /> 
                    <span> <a href="<?php echo base_url() ?>MenuDetail" name="simpan">Kembali</a></span>
                  </div>
                  <div class="btn_30_blue">
                    <span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>