<?php $this->load->view('administrator/header');

if ($code->curr_number == null) {
	$number = 1;
} else {
	$number = $code->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

$code_PH = 'PH' . date('y') . $agen . $kodemax;
?>
<style type="text/css">
.chzn-container {
    width: 102.5% !important;
}
</style>
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan'); ?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Pembayaran Hutang</h3>

    <div class="top_search">
    </div>
</div>
<!-- ======================= -->

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>PembayaranHutang">Pembayaran Hutang</a></li>
                            <li class="active">Pembayaran Hutang</li>
                        </ul>
                    </div>
                </div>
                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Pembayaran Hutang</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <form method="post" action="" class="form_container left_label">
                        <ul>
                            <li class="body-search-data">
                                <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2 mb">No PH</label>
                                        <div class="form_input">
                                            <input id="PH" type="text" name="PH" class="form-control" required  value="<?php echo $code_PH ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb">Supplier</label>
                                        <div class="form_input">
                                            <select data-placeholder="Cari Supplier" class="chzn-select" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem" tabindex="13" onchange="check_hutang()" name="IDSupplier" id="IDSupplier" required>
                                                <option value=""></option>
                                                <?php
                                                foreach ($supplier as $data) {?>
                                                <option value="<?php echo $data->IDSupplier ?>"><?php echo $data->Nama; ?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mb">Tanggal PH</label>
                                        <div class="form_input">
                                            <input id="Tanggal_PH" type="date" name="Tanggal_PH" class="form-control" placeholder="Tanggal Inv Pembelian" required >
                                        </div>
                                    </div>
                                    <div class="form_grid_6 mb-1">
                                        <label class="field_title mt-dot2 mb">Keterangan</label>
                                        <div class="form_input">
                                            <textarea class="form_contol" rows="3" name="keterangan" id="keterangan"></textarea>
                                        </div>
                                    </div>

                                    <input id="totalqty" type="hidden" name="totalqty" class="form-control">
                                    <input id="totalmeter" type="hidden" name="totalmeter" class="form-control">

                                    <!-- clear content -->
                                    <div class="clear"></div>
                                </div>
                            </li>
                        </ul>

                        <!-- content body tab & pane -->
                        <div class="grid_12 mx w-100">
                            <div class="widget_wrap tabby mb-4">
                                <div class="widget_top">
                                    <!-- button tab & pane -->
                                    <div id="widget_tab">
                                        <ul>

                                            <li class="px py"><a href="#tab2" class="active_tab">Total Pembayaran</a></li>
                                            <li class="px py"><a href="#tab1">List Hutang</a></li>

                                        </ul>
                                    </div>
                                    <!-- end button tab & pane -->
                                </div>

                                <!-- tab & pane body -->
                                <div class="widget_content">
                                    <!-- tab 1 -->
                                    <div id="tab1">
                                        <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                            <div class="grid_12 w-100">
                                                <span class="h_icon blocks_images"></span>
                                                <h6>List Hutang</h6>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="form_grid_12 mb-1">
                                            <label class="field_title mb mt-dot2"><b>Total Nilai Pembayaran :</b></label><br><br>
                                            <div class="form_input input-not-focus">
                                                <input type="text" id="total_invoice" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                            </div>
                                        </div>

                                        <div>
                                            <table class="display data_tbl">
                                                <thead>
                                                    <tr>
                                                        <th class="center" style="width: 40px">No</th>
                                                        <th>do</th>
                                                        <th class="hidden-xs">No Invoice</th>
                                                        <th>Tanggal Faktur</th>
                                                        <th>JT Faktur</th>
                                                        <th>Nilai Faktur</th>
                                                        <th>Retur</th>
                                                        <th>Telah dibayar</th>
                                                        <th>Dibayar</th>
                                                        <th>Saldo</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tb_preview_penerimaan">

                                                </tbody>
                                            </table>
                                            <ul>
                                                <input type="hidden" id="Pembayaran" name="Pembayaran" class="form-control" placeholder="Pembayaran">
                                                <li>
                                                    <div class="form_grid_12">
                                                        <div class="form_grid_6">
                                                            <div class="form_grid_12 mb-1">
                                                                <label class="field_title mt-dot2 mb">Total</label>
                                                                <div class="form_input">
                                                                    <input type="text" class="form-control" name="total" id="total" readonly>
                                                                    <input type="hidden" name="pembayaran_db" id="pembayaran_db">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form_grid_6">
                                                            <div class="form_grid_12 mb-1">
                                                             <!--    <div class="form_grid_1 mb-1">
                                                                    <input type="button" class="btn btn_24_blue btn-small" value="=" onclick="sama_dengan()">
                                                                </div> -->
                                                                <div class="form_grid_12 mb-1">
                                                                    <label class="field_title mt-dot2 mb">Kelebihan Bayar</label>
                                                                    <div class="form_input">
                                                                        <input type="text" id="pembayaran" name="pembayaran" class="form-control" placeholder="Pembayaran" onkeyup="coa_lebih_bayar()">
                                                                    </div>
                                                                </div>

                                                                <select name="coa_lebih_bayar" id="coa_lebih_bayar" style="width: 100%!important; padding: 3px 2px;border-color: #d8d8d8;color: #999999;">
                                                                    <?php
                                                                    foreach($coasemua as $coasemuas)
                                                                    {
                                                                        ?>
                                                                        <option value="<?php echo $coasemuas->IDCoa ?>"><?php echo $coasemuas->Kode_COA ?> <?php echo $coasemuas->Nama_COA ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="form_grid_4">
                                                            <div class="form_grid_12 mb-1">
                                                                <label class="field_title mt-dot2 mb">Kelebihan Bayar</label>
                                                                <div class="form_input input-not-focus"> -->
                                                                    <input type="hidden" id="kelebihan_bayar" name="kelebihan_bayar" class="form-control" value="0" placeholder="kelebihan bayar" readonly>
                                                                <!-- </div>
                                                                </div> -->
                                                            <!-- <div class="form_grid_12 mb-1">
                                                                <div class="btn_30_blue">
                                                                    <span><a class="btn_24_blue" id="calculate" onclick="calculate()">Hitung</a></span>
                                                                </div>
                                                            </div> -->
                                                        </div>

                                                        <div class="clear"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- end tab 1 -->

                                    <!-- tab 2 -->
                                    <div id="tab2">
                                        <div class="widget_top" style="background: transparent;top: 22px; position: absolute;">
                                            <div class="grid_12 w-100">
                                                <span class="h_icon blocks_images"></span>
                                                <h6>Total Pembayaran</h6>
                                            </div>
                                        </div>
                                        <ul>
                                            <li>
                                               <!--  <div class="form_grid_12 w-100 alpha">
                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mt-dot2 mb">Uang Muka</label>
                                                        <div class="form_input">
                                                            <input type="text" class="form-control" name="uang_muka" id="uang_muka">
                                                        </div>
                                                    </div>
                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mt-dot2 mb">Kompensasi UM</label>
                                                        <div class="form_input">
                                                            <input type="text" class="form-control" name="kompensasi_um" id="kompensasi_um">
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="form_grid_12 w-100 alpha">
                                                    <div class="form_grid_12 mb-1">
                                                        <label class="field_title mb mt-dot2">Total Nilai Pembayaran</label>
                                                        <div class="form_input input-not-focus">
                                                            <input type="text" id="total_invoice_pembayaran" name="total_invoice" class="form-control" placeholder="Total Invoice" readonly>
                                                        </div>
                                                    </div>

                                                       <!--  <div class="form_grid_6 mb-1">
                                                            <label class="field_title mb mt-dot2">Nama COA</label>
                                                            <div class="form_input input-disabled">
                                                                <select data-placeholder="Cari COA" style="width: 101.7%!important; padding: 3px 2px;border-color: #d8d8d8;margin-bottom: .1rem" tabindex="13"  name="namacoa"  id="namacoa" required oninvalid="this.setCustomValidity('COA Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                                    <?php
//   foreach ($bank as $data) {
?>
                                                                        <option value="<?php //echo $data->IDCoa ?>"><?php// echo $data->Nama_COA; ?></option>
                                                                        <?php
//}
?>
                                                                </select>
                                                            </div>
                                                        </div> -->
                                                    </div>

                                                  <!--   <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Jenis Pembayaran</label>
                                                        <div class="form_input">
                                                            <div class="mb-1">
                                                              <span>
                                                                <input type="radio" class="radio" name="jenis" id="jenis" value="Cash" onclick="jenis_pembayaran()">
                                                                <label class="choice">Cash</label>
                                                              </span>
                                                            </div>
                                                            <div class="mb-1">
                                                              <span>
                                                                <input type="radio" class="radio" name="jenis" id="jenis2" value="Transfer" onclick="jenis_pembayaran()">
                                                                <label class="choice">Transfer</label>
                                                              </span>
                                                            </div>
                                                            <div class="mb-1">
                                                              <span>
                                                                <input type="radio" class="radio" name="jenis" id="jenis3" value="Giro" onclick="jenis_pembayaran()">
                                                                <label class="choice">Giro</label>
                                                              </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Tgl Giro</label>
                                                        <div class="form_input">
                                                            <input type="date" id="tgl_giro" name="tgl_giro" class="input-date-padding-3-2 form-control" value="<?php// echo date('Y-m-d'); ?>" placeholder="Tanggal Giro">
                                                        </div>
                                                    </div> -->
                                                   <!--  <div class="form_grid_6 mb-1">
                                                        <label class="field_title mt-dot2 mb">Selisih Pembulatan</label>
                                                        <div class="form_input">
                                                            <input type="text" class="form-control" name="selisih_pembulatan" id="selisih_pembulatan">
                                                        </div>
                                                    </div> --><!--
                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Total Pembayaran</label>
                                                        <div class="form_input">
                                                            <input type="text" id="nominal" name="nominal" class="form-control" value="0">
                                                        </div>
                                                    </div> -->

                                                    <span class="clear"></span>
                                                </li>
                                            </ul>
                                            <hr>
                                            <ul>
                                                <li>
                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Jenis Pembayaran</label>
                                                        <div class="form_input">
                                                            <!-- <div class="form_grid_6 mb-1">
                                                                <div class="mb-1">
                                                                    <span>
                                                                        <input type="radio" class="radio" name="jenis" id="jenis" value="Cash" onclick="jenis_pembayaran()">
                                                                        <label class="choice">Cash</label>
                                                                    </span>
                                                                </div>
                                                                <div class="mb-1">
                                                                    <span>
                                                                        <input type="radio" class="radio" name="jenis" id="jenis2" value="Transfer" onclick="jenis_pembayaran()">
                                                                        <label class="choice">Transfer</label>
                                                                    </span>
                                                                </div>

                                                                <span class="clear"></span>
                                                            </div> -->
                                                            <!-- <div class="form_grid_6 mb-1">
                                                                <div class="mb-1">
                                                                    <span>
                                                                        <input type="radio" class="radio" name="jenis" id="jenis3" value="Giro" onclick="jenis_pembayaran()">
                                                                        <label class="choice">Giro</label>
                                                                    </span>
                                                                </div>
                                                                <div class="mb-1">
                                                                    <span>
                                                                        <input type="radio" class="radio" name="jenis" id="jenis4" value="Nota Kredit" onclick="jenis_pembayaran()">
                                                                        <label class="choice">Nota Kredit</label>
                                                                    </span>
                                                                </div>
                                                                <div class="mb-1">
                                                                    <span>
                                                                        <input type="radio" class="radio" name="jenis" id="jenis5" value="Pembulatan" onclick="jenis_pembayaran()">
                                                                        <label class="choice">Pembulatan</label>
                                                                    </span>
                                                                </div>
                                                                <span class="clear"></span>
                                                            </div> -->
                                                            <select name="jenis" id="jenis" style="width: 100%!important; padding: 3px 2px;border-color: #d8d8d8;color: #999999;" onchange="jenis_pembayaran()">
                                                                <option value="transfer">Transfer</option>
                                                                <option value="giro">Giro</option>
                                                                <option value="cash">Cash</option>
                                                                <option value="nota_kredit">Nota Kredit</option>
                                                                <option value="pembulatan">Pembulatan</option>
                                                            </select>
                                                        </div>
                                                        <span class="clear"></span>
                                                    </div>

                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Nama COA</label>
                                                        <div class="form_input input-disabled">
                                                            <select name="namacoa" id="namacoa" style="width: 102.25%;padding: 3px 4px;border-color: #ccc;color: #757575;" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                                <option value="">--Silahkan Pilih Jenis Pembayaran Dahulu--</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Tgl Giro</label>
                                                        <div class="form_input">
                                                            <input type="date" id="tgl_giro" name="tgl_giro" id="tgl_giro" class="input-date-padding-3-2 form-control" placeholder="Tanggal Giro">
                                                        </div>
                                                    </div>

                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Nomor Giro</label>
                                                        <div class="form_input">
                                                            <input type="text" id="no_giro" name="no_giro" class="input-date-padding-3-2 form-control" placeholder="Nomor Giro">
                                                        </div>
                                                    </div>

                                                    <div class="form_grid_6 mb-1">
                                                        <label class="field_title mb mt-dot2">Nilai Pembayaran</label>
                                                        <div class="form_input">
                                                            <input type="text" id="nilai_bayar" name="nilai_bayar" class="input-date-padding-3-2 form-control" placeholder="Nilai Pembayaran">
                                                        </div>
                                                    </div>

                                                    <span class="clear"></span>
                                                    <div class="mt-4">
                                                        <center> <button class="btn_24_blue" type="button" id="add-to-cart">
                                                            Tambah Data
                                                        </button></center>
                                                    </div>

                                                </li>
                                                <table class="display data_tbl">
                                                    <thead>
                                                      <tr>
                                                        <th class="center" style="width: 40px">No</th>
                                                        <th>Jenis Pembayaran</th>
                                                        <th>Nama COA</th>
                                                        <th>Tanggal Giro</th>
                                                        <th>Nomor Giro</th>
                                                        <th>Nilai</th>
                                                        <th style="width: 50px">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabel-cart-hutang">

                                                </tbody>
                                            </table>
                                        </ul>

                                    </div>
                                </div>
                                <!-- end tab 2 -->
                            </div>
                            <!-- end tab & pane body -->
                        </div>
                    </div>
                    <!-- end content body tab & pane -->
                    <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2">
                          <!--   <div class="btn_30_blue">
                                <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                            </div> -->
                            <div class="btn_30_blue">
                                <span><a class="btn_24_blue" id="simpan" onclick="saveHutang()" style="cursor: pointer;">Simpan</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>libjs/pembayaran_hutang.js"></script>
    <script type="text/javascript">
        var base_url = window.location.pathname.split('/');
        var PM = new Array();
        var PH = new Array();
        if($("#pembayaran") != 0)
        {
            $("#namacoa").prop("disabled", false);
        }else{
            $("#namacoa").prop("disabled", true);
        }
        index = 0;

        function hutang() {
            this.Tanggal_Hutang;
            this.Jatuh_Tempo;
            this.No_Faktur;
            this.IDSupplier;
            this.Jenis_Faktur;
            this.Nilai_Hutang;
            this.Pembayaran;
            this.Saldo_Awal;
            this.Saldo_Akhir;
            this.IDHutang;
            this.IDFaktur;
            this.IDFaktur;
            this.Telah_diterima;
        }

        function check_hutang() {
            if ($('#IDSupplier').val() != '') {
                $.post("<?php echo base_url("PembayaranHutang/check_hutang") ?>", {'IDSupplier' : $('#IDSupplier').val()})
                .done(function(response) {
                    var json = JSON.parse(response);
                        //console.log(json);
                        if (!json.error) {
                            $.each(json.data, function (key, value) {
                                if(PM == null)
                                    PM = new Array();

                                var M = new hutang();

                                M.Tanggal_Hutang = value.Tanggal_Hutang;
                                M.No_Faktur = value.No_Faktur;
                                M.Jatuh_Tempo = value.Jatuh_Tempo;
                                M.Nilai_Hutang = value.Nilai_Hutang;
                                M.Saldo_Awal = value.Saldo_Awal;
                                M.Saldo_Akhir = value.Saldo_Akhir;
                                M.Pembayaran = value.Pembayaran;
                                M.Jenis_Faktur = value.Jenis_Faktur;
                                M.IDSupplier = value.IDSupplier;
                                M.IDHutang = value.IDHutang;
                                M.IDFaktur = value.IDFaktur;
                                M.Telah_diterima = value.Telah_diterima;

                                PM[index] = M;
                                index++;

                                $("#tb_preview_penerimaan").html("");
                                for(i=0; i < index; i++){
                                    value = "<tr id='rowarray"+i+"'>"+
                                    "<td class='text-center'>"+(i+1)+"</td>"+
                                    "<td>"+"<input type='checkbox' name='do_' id='do"+i+"' value='"+i+"' onclick=ceklis_list_hutang("+i+")>"+"</td>"+
                                    "<td>"+PM[i].No_Faktur+"</td>"+
                                    "<td>"+PM[i].Tanggal_Hutang+"</td>"+
                                    "<td>"+PM[i].Jatuh_Tempo+"</td>"+
                                    "<td>"+PM[i].Nilai_Hutang+"</td>"+
                                    // "<td>"+(PM[i].Saldo_Awal-PM[i].Saldo_Akhir)+"</td>"+
                                    "<td>"+0+"</td>"+
                                    "<td>"+PM[i].Pembayaran+"</td>"+
                                    "<td>" +
                                    "<input type=text class='form-control' name=dibayar"+i+" id=dibayar"+i+" readonly>" +
                                    "</td>"+
                                    "<td>"+
                                    "<input type=text class='form-control' name=saldo_akhir"+i+" id=saldo_akhir"+i+" value='"+PM[i].Saldo_Akhir+"' readonly>" +
                                    "</td>"+
                                    "</tr>";
                                    $("#tb_preview_penerimaan").html($("#tb_preview_penerimaan").html()+value);
                                }
                                $('#total').val(0);
                            });
                        }
                    });
            }
        }

        var tmp = new Array();
        function ceklis_list_hutang(i){
            var val = document.getElementsByName('do_');
            var Total = 0;
            var Pembayaran = 0;
            var Pembayaran_bayar = 0;

            checked = i;
            if ($('#do'+i).is(":checked")) {
                console.log(PM[checked].Saldo_Akhir);

                Pembayaran =  parseInt($('#pembayaran').val()) - parseInt(PM[checked].Saldo_Akhir);
                console.log("d");
                if(Pembayaran >0){
                    console.log("a");
                    $('#saldo_akhir'+checked).val(0);
                    $('#dibayar'+checked).val(PM[checked].Saldo_Akhir);
                    PM[checked].Pembayaran = PM[checked].Saldo_Akhir;
                    Pembayaran_bayar =  Pembayaran;
                }else{
                    console.log("b");
                    bayar = parseInt(PM[checked].Saldo_Akhir) - Math.abs($('#pembayaran').val());
                    $('#dibayar'+checked).val(Math.abs($('#pembayaran').val()));
                    $('#saldo_akhir'+checked).val(bayar);
                    Pembayaran_bayar =  0;
                }
                // Total = Total + parseInt(PM[checked].Saldo_Akhir);
                tmp[i] = checked;
                PM[i].Telah_diterima=$('#dibayar'+checked).val();
                PH[i] = PM[checked];
            }else{
                console.log("c");
                Pembayaran_bayar =  parseInt($('#pembayaran').val()) + parseInt($('#dibayar'+checked).val());
                bayar = parseInt(PM[checked].Saldo_Akhir);
                $('#saldo_akhir'+checked).val(bayar);
                $('#dibayar'+checked).val(0);
            }

            for( var j = 0; j < tmp.length-1; j++){
                if ( tmp[j] === undefined) {
                    tmp.splice(j, 1);
                }
            }
            tmp.filter(function(val){return val});

            //console.log(PH);
            for( var k = 0; k < PH.length-1; k++){
                if ( PH[k] === undefined) {
                    PH.splice(k, 1);
                }
            }
            PH.filter(function(val){return val});

            for (var i = 0; i < val.length; i++) {
                Total = parseInt(Total) + parseInt($('#saldo_akhir'+i).val());
            }

            $('#total').val(Total);
            $('#pembayaran_db').val(Pembayaran);
            $('#pembayaran').val(Pembayaran_bayar);
            $('#total_invoice_pembayaran').val(Total);
        //     if($("#pembayaran").val() > 0){
        //       $("#coa_lebih_bayar").show();
        //   }else{
        //     $("#coa_lebih_bayar").hide();
        // }
    }


       //  $(document).on('change', 'input[name="do_"]', function() {
       //      var val = document.getElementsByName('do_');
       //      var Total = 0;
       //      var Pembayaran = 0;
       //      var Pembayaran_bayar = 0;
       //      for (var i = 0; i < val.length; i++) {
       //              console.log("==========================="+i);
       //              checked = val[i].value;
       //          if (val[i].checked) {
       //              console.log(PM[checked].Saldo_Akhir);

       //              Pembayaran =  parseInt($('#pembayaran').val()) - parseInt(PM[checked].Saldo_Akhir);
       //              console.log("d");
       //              if(Pembayaran >0){
       //                  console.log("a");
       //                  $('#saldo_akhir'+checked).val(0);
       //                  $('#dibayar'+checked).val(PM[checked].Saldo_Akhir);
       //                  PM[checked].Pembayaran = PM[checked].Saldo_Akhir;
       //                  Pembayaran_bayar =  Pembayaran;
       //              }else{
       //                  console.log("b");
       //                  bayar = parseInt(PM[checked].Saldo_Akhir) - Math.abs($('#pembayaran').val());
       //                  $('#dibayar'+checked).val(Math.abs($('#pembayaran').val()));
       //                  $('#saldo_akhir'+checked).val(bayar);
       //                  Pembayaran_bayar =  0;
       //              }
       //              Total = Total + parseInt(PM[checked].Saldo_Akhir);
       //              tmp[i] = checked;
       //              PM[i].Telah_diterima=$('#dibayar'+checked).val();
       //              PH[i] = PM[checked];
       //          }else{
       //              console.log("c");
       //              Pembayaran_bayar =  $('#dibayar'+checked).val();
       //              $('#dibayar'+checked).val(0);
       //          }
       //      }
       // //      for (var i = 0; i < val.length; i++) {
       // //          if (val[i].checked) {
       // //              checked = val[i].value;

       // //              Pembayaran =  parseInt($('#pembayaran').val()) - parseInt(PM[checked].Saldo_Akhir);
       // //              Pembayaran_bb =  parseInt($('#pembayaran').val()) - parseInt(PM[checked].Saldo_Akhir);
       // //              if(Pembayaran >0){
       // //                  if($('#dibayar'+checked).val()!="" || $('#dibayar'+checked).val() != 0){
       // //                      $('#saldo_akhir'+checked).val(0);
       // //                      $('#dibayar'+checked).val(PM[checked].Saldo_Akhir);
       // //                      PM[checked].Pembayaran = PM[checked].Saldo_Akhir;
       // //                      Pembayaran_bayar =  Pembayaran;
       // //                      console.log('1');
       // //                  }else{
       // //                     $('#saldo_akhir'+checked).val(0);
       // //                     $('#dibayar'+checked).val(PM[checked].Saldo_Akhir);
       // //                     PM[checked].Pembayaran = PM[checked].Saldo_Akhir;
       // //                     Pembayaran_bayar =  Pembayaran_bb;
       // //                     console.log('1111');
       // //                 }
       // //             }else{
       // //              bayar = parseInt(PM[checked].Saldo_Akhir) - Math.abs($('#pembayaran').val());
       // //              $('#dibayar'+checked).val(Math.abs($('#pembayaran').val()));
       // //              $('#saldo_akhir'+checked).val(bayar);
       // //              Pembayaran_bayar =  0;
       // //              console.log('2');
       // //          }
       // //          Total = Total + parseInt(PM[checked].Saldo_Akhir);
       // //          tmp[i] = checked;
       // //          PM[i].Telah_diterima=$('#dibayar'+checked).val();
       // //          PH[i] = PM[checked];
       // //      }
       // //      else{
       // //          checked = val[i].value;
       // //          if($('#dibayar'+checked).val() == ""){
       // //              xl=0;
       // //              console.log('3');
       // //          }else{
       // //              if($('#dibayar'+checked).val()!="" || $('#dibayar'+checked).val() != 0){
       // //                  xl=$('#dibayar'+checked).val();
       // //                  console.log('4');
       // //                  Pembayaran =  parseInt(xl) + parseInt($('#saldo_akhir'+checked).val());
       // //                  $('#saldo_akhir'+checked).val(Pembayaran);
       // //                  $('#dibayar'+checked).val(0);
       // //                  Pembayaran_bayar =   parseInt($('#pembayaran').val())+parseInt(xl);
       // //                  console.log('4');
       // //              }else{
       // //                 xl=$('#dibayar'+checked).val();
       // //                 Pembayaran =  parseInt(xl) + parseInt($('#saldo_akhir'+checked).val());
       // //                 $('#saldo_akhir'+checked).val(Pembayaran);
       // //                 $('#dibayar'+checked).val(0);
       // //                 Pembayaran_bayar =   parseInt($('#pembayaran').val())-parseInt(xl);
       // //                 console.log('5');
       // //             }
       // //         }


       // //     }
       // // }



       //      for( var j = 0; j < tmp.length-1; j++){
       //          if ( tmp[j] === undefined) {
       //              tmp.splice(j, 1);
       //          }
       //      }
       //      tmp.filter(function(val){return val});

       //      //console.log(PH);
       //      for( var k = 0; k < PH.length-1; k++){
       //          if ( PH[k] === undefined) {
       //              PH.splice(k, 1);
       //          }
       //      }
       //      PH.filter(function(val){return val});

       //      $('#total').val(Total);
       //      $('#pembayaran_db').val(Pembayaran);
       //      $('#pembayaran').val(Pembayaran_bayar);
       //      $('#total_invoice_pembayaran').val(Total);
       //  });

       function sama_dengan() {
        $('#pembayaran').val($('#total').val());
    }

    function calculate() {
            //console.log(tmp);
            //pembayaran = (parseInt($('#pembayaran').val()) - parseInt($('#total').val()));
            //$('#kelebihan_bayar').val(pembayaran);
            pembayaran = parseInt($('#pembayaran').val());
            console.log(pembayaran);

            $.each(tmp, function (index, value) {
                pembayaran = pembayaran - parseInt(PM[value].Saldo_Akhir);
                Sisa = pembayaran;
                console.log(value + ' ' + pembayaran  + ' ' +parseInt(PM[value].Saldo_Akhir));
                console.log("========"+Sisa);
                if (Sisa > 0) {
                    $('#saldo_akhir'+value).val(0);
                    $('#dibayar'+value).val(PM[value].Saldo_Akhir);
                    PM[value].Pembayaran = PM[value].Saldo_Akhir;
                    PM[value].Saldo_Akhir = 0;
                } else {
                    $('#saldo_akhir'+value).val(Math.abs(Sisa));
                    bayar = parseInt(PM[value].Saldo_Akhir) - Math.abs(Sisa);
                    $('#dibayar'+value).val(bayar);
                    PM[value].Saldo_Akhir = Math.abs(Sisa);
                    PM[value].Pembayaran = bayar;
                }

                //$('#dibayar'+value).val(PM[value].Saldo_Akhir);
            });

            // if (Sisa > 0) {
            //     $('#kelebihan_bayar').val(Sisa);
            // } else {
            //     $('#kelebihan_bayar').val(0);
            // }

        }

        var jenis;
        $("#Jatuh_tempo").prop("disabled", true);
        $("#Discount").prop("disabled", true);

        // $("#namacoa").prop("disabled", true);


        function jenis_pembayaran() {
            var jenis= $("#jenis").val();
            var supplier= $("#IDSupplier").val();
            $.ajax({
              url : '/'+base_url[1]+'/PembayaranHutang/getcoa',
              type: "POST",
              data: {jenis_pembayaran:jenis, supplier:supplier},
              dataType:'json',
              success: function(data)
              { 
                if(jenis=='transfer' || jenis=='cash' || jenis=='nota_kredit' || jenis=='pembulatan')
                {
                    $("#no_giro").prop("disabled", true);
                }else if(jenis=='giro')
                {
                    $("#no_giro").prop("disabled", false);
                }

                var html = '';
                if (data <= 0 || data == null || data =="") {
                  html = '<option value="">--Data Kosong--</option>';
              } else {
                if(jenis=='transfer' || jenis=='cash' || jenis=='giro' || jenis=='pembulatan'){
                  html += '<option value="">--Silahkan Pilih COA--</option>';

                  for (var i = 0 ; i < data.length; i++) {
                    html +='<option value="'+data[i]["IDCoa"]+'" data-namacoa="'+data[i]["Nama_COA"]+'"> '+data[i]["Kode_COA"]+' '+data[i]["Nama_COA"]+' </option>';
                }
            }else{
                html = '<option value="P000023" data-namacoa="Uang Muka Pembelian Pihak Berelasi">Uang Muka Pembelian Pihak Berelasi</option>'+'<option value="P000024" data-namacoa="Uang Muka Pembelian Pihak Ketiga">Uang Muka Pembelian Pihak Ketiga</option>';
            }

        }
        $("#namacoa").html(html);
        if(jenis=='nota_kredit'){
            $("#nilai_bayar").val(data[0].Nominal);
        }else{
            $("#nilai_bayar").val('');
        }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
    }
});
        }

        function hitung() {
            var a = new Date($("#Tanggal").val());
            var b = $("#Jatuh_tempo").val();

            var today = new Date(a.getTime()+(b*24*60*60*1000));
            var dd = today.getDate();
            var mm = today.getMonth()+1;

            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd;
            }
            if(mm<10){
                mm='0'+mm;
            }
            var today = yyyy+'-'+mm+'-'+dd;
            //$("#Tanggal_jatuh_tempo").val(today);
        }

        function fieldHutang(){
            var data1 = {
                "nomor_PH":$("#PH").val(),
                "IDSupplier":$("#IDSupplier").val(),
                "Tanggal_PH":$("#Tanggal_PH").val(),
                "Keterangan":$("#keterangan").val(),
                //"IDMataUang":,
                //"Kurs":,
                "total":$("#total").val(),
                "Uang_muka":$("#uang_muka").val(),
                "Kompensasi_um":$("#kompensasi_um").val(),
                "Pembayaran":$("#pembayaran").val(),
                "kelebihan_bayar":$("#kelebihan_bayar").val(),
                "jenis" : jenis,
                "nominal" : $('#nominal').val(),
                "tgl_giro" : $('#tgl_giro').val(),
                "namacoa" : $('#namacoa').val(),
                "pembayaran2" : $('#total_invoice_pembayaran').val(),
                "sisa_pembayaran" : $('#pembayaran').val(),
            }
            return data1;
        }

        function saveHutang(){
           console.log(PK);
           if ($('#nominal').val() != 0 && $('#Tanggal_PH').val() != '') {
            var data1 = fieldHutang();

            $.ajax({
                url: "store",
                type: "POST",
                data: {
                    "data1" : data1,
                    "data2" : PH,
                    "data3" : PK
                },
                    // dataType: 'json',
                    success: function (msg, status) {
                        $('#alert').html('<div class="pesan sukses">Data berhasil disimpan</div>');
                        window.location.href = "/"+base_url[1]+"/PembayaranHutang/create";
                    },
                    error: function(msg, status){
                        console.log(msg);
                        $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
                    }
                });
        } else {
            $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
        }
    }
</script>

<?php $this->load->view('administrator/footer');?>