<?php $this->load->view('administrator/header') ; ?>

<style type="text/css">
    /*  .table_top{
          display: none !important;
          }*/
</style>
<!-- tittle data -->
<div class="page_title">
    <?php echo $this->session->flashdata('Pesan');?>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Pembayaran Hutang</h3>
    <div class="top_search">

    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li> Pembayaran Hutang </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form action="<?php echo base_url() ?>PembayaranHutang/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                                <!-- search -->
                                <div class="form_grid_12 w-100 alpha">
                                    <!-- button -->
                                    <div class="form_grid_3 ">
                                        <div class="btn_24_blue">
                                            <a href="<?php echo base_url('PembayaranHutang/create')?>"><span>Tambah Data</span></a>
                                        </div>
                                    </div>
                                    <div class="form_grid_9 ">
                                        <div class="form_grid_3 ">
                                            <label class="field_title mt-dot2">Periode</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_3 ">
                                            <label class="field_title mt-dot2"> S/D </label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_3 ">
                                            <select name="jenispencarian" data-placeholder="No Retur Pembelian" style="width: 100%!important" class="chzn-select" tabindex="13">
                                                <option value="Nomor">No PH</option>
                                            </select>
                                        </div>
                                        <div class="form_grid_2">
                                            <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
                                        </div>
                                        <div class="form_grid_1">
                                            <div class="btn_24_blue">
                                                <input type="submit" name="" value="search">
                                            </div>
                                        </div>
                                    </div>
                                    <span class="clear"></span>
                                </div>
                                <!-- end search -->
                            </li>
                        </ul>
                    </form>
                </div>
                <form action="<?php echo base_url() ?>PembayaranHutang/delete_multiple" method="post" id="ubahstatus">
                    <div class="widget_content">
                        <table class="display" id="action_tbl">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th class="center">
                                    <input name="checkbox" type="checkbox" value="" class="checkall"> Ceklis
                                </th>
                                <th>Tanggal</th>
                                <th>Nomor PH</th>
                                <th>Nama Supplier</th>
                                <th>Pembayaran</th>
                                <th>Cara Pembayaran</th>
                                <th>Keterangan</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($datas)){
                                $i=1;
                                foreach ($datas as $data) {
                                    ?>
                                    <tr class="odd gradeA">
                                        <td class="center"><?php echo $i; ?></td>
                                        <td class="center tr_select"><input type="checkbox" name="msg[]" value="<?php echo $data->IDBS ?>"></td>
                                        <td><?php echo $data->Tanggal; ?></td>
                                        <td><?php echo $data->Nomor_PH; ?></td>
                                        <td><?php echo $data->Nama; ?></td>
                                        <td><?php echo $data->Telah_diterima; ?></td>
                                        <td><?php echo $data->Jenis_pembayaran; ?></td>
                                        <td><?php echo $data->Keterangan; ?></td>
                                        <td class="text-center ukuran-logo">
                                            <span><a class="action-icons c-Detail" href="<?php echo base_url('PembayaranHutang/show/'.$data->IDBS); ?>" title="Detail Data"> Detail</a></span>
                                            <?php if($data->Batal=="aktif"){ ?>
                                                <span><a class="action-icons c-edit" href="<?php echo base_url('PembayaranHutang/edit/'.$data->IDBS); ?>" title="Ubah Data"> Edit</a></span>
                                            <?php } else {
                                                ?>
                                                <span><a class="action-icons c-edit" style="cursor: no-drop;" title="Ubah Data"> Edit</a></span>
                                                <?php
                                            } ?>
                                            <?php if($data->Batal=="aktif"){ ?>
                                                <span><a class="action-icons c-sa" href="<?php echo base_url() ?>PembayaranHutang/status_gagal/<?php echo $data->IDBS;?>" title="Status Tidak Aktif">Status</a></span>
                                            <?php }elseif($data->Batal=="tidak aktif"){ ?>
                                                <span><a class="action-icons c-approve" href="<?php echo base_url() ?>PembayaranHutang/status_berhasil/<?php echo $data->IDBS;?>" title="Status Aktif">Status</a></span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>

                            </tbody>
                        </table>
                        <div class="widget_content text-right px-2">
                            <div class="py-4 mx-2">
                                <div class="btn_24_blue">
                                    <a href="#" type="button" onclick="document.getElementById('ubahstatus').submit();"><span>Batal</span></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>