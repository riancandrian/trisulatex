<?php $this->load->view('administrator/header') ; ?>
<div class="page_title">
	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Data UM Customer</h3>

	<div class="top_search">
	</div>
</div>
<!-- ======================= -->

<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<li><a href="<?php echo base_url() ?>Umcustomer">Data UM Customer</a></li>
							<li class="active">Edit UM Customer</li>
						</ul>
					</div>
				</div>
				<div class="widget_top">
					<div class="grid_12">
						<span class="h_icon blocks_images"></span>
						<h6>Edit Data UM Customer</h6>
					</div>
				</div>
				<div class="widget_content">
					<form method="post" action="<?php echo base_url() ?>umcustomer/update/<?php echo $umcustomer->IDUMCustomer ?>" class="form_container left_label">
						  <input type="hidden" name="id" value="<?php echo $umcustomer->IDUMCustomer ?>">
						<ul>
							<li>
								<div class="form_grid_12 multiline">
									<label class="field_title">Customer</label>
									<div class="form_input">

										<select data-placeholder="Pilih Customer" style=" width:100%;" class="chzn-select" tabindex="13"  name="IDCustomer" required oninvalid="this.setCustomValidity('Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
											<option value=""></option>
											<?php 
                                    foreach ($customer as $data) {
                                    if ($data->IDCustomer == $umcustomer->IDCustomer) {
                                        ?>
                                        <option selected value="<?php echo $data->IDCustomer ?>"><?php echo $data->Nama; ?></option>
                                        <?php
                                    }else{
                                        ?>
                                        <option value="<?php echo $data->IDCustomer ?>"><?php echo $data->Nama; ?></option>
                                        <?php
                                        }
                                    }
                                 ?>
										</select>

										<span class="clear"></span>
									</div>
									<label class="field_title">Tanggal UM</label>
									<div class="form_input">
										 <input type="date" name="Tanggal_UM" value="<?php echo $umcustomer->Tanggal_UM ?>" class="form-control" placeholder="Tanggal UM" required oninvalid="this.setCustomValidity('Tanggal UM Tidak Boleh Kosong')" oninput="setCustomValidity('')">
										<span class="clear"></span>
									</div>
									<label class="field_title">No Faktur</label>
									<div class="form_input">
									 <input type="text" name="Nomor_Faktur" value="<?php echo $umcustomer->Nomor_Faktur ?>" class="form-control" placeholder="Nomor Faktur" required oninvalid="this.setCustomValidity('No Faktur Tidak Boleh Kosong')" oninput="setCustomValidity('')">
										<span class="clear"></span>
									</div>
									<label class="field_title">Nilai UM</label>
									<div class="form_input">
										 <span class="input-group-addon">Rp. </span>
                                     <input type="text" name="Nilai_UM" value="<?php echo convert_currency($umcustomer->Nilai_UM) ?>" id="tanpa-rupiah-debit" class="form-control price-date" placeholder="Nilai UM" required oninvalid="this.setCustomValidity('Nilai UM Tidak Boleh Kosong')" oninput="setCustomValidity('')">
										<span class="clear"></span>
									</div>
									<!-- <label class="field_title">Saldo UM</label>
									<div class="form_input">
										   <span class="input-group-addon">Rp. </span>
                                   <input type="text" name="Saldo_UM" id="tanpa-rupiah-kredit" class="form-control price-date" placeholder="Saldo UM" value="<?php //echo convert_currency($umcustomer->Saldo_UM) ?>" required oninvalid="this.setCustomValidity('Saldo UM Tidak Boleh Kosong')" oninput="setCustomValidity('')">
										<span class="clear"></span>
									</div> -->
								</div>
							</li>
							<li>
								<div class="form_grid_12">
									<div class="form_input">
										<div class="btn_30_light">
											<span> <a href="<?php echo base_url() ?>Umcustomer" name="simpan">Kembali</a></span>
										</div>
										<div class="btn_30_blue">
											<span><input type="submit" name="simtam" type="submit" class="btn_small btn_blue" value="Simpan Data"></span>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>