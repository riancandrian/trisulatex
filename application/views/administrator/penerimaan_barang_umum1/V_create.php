<?php $this->load->view('administrator/header') ;

if ($last_number->curr_number == null) {
    $number = 1;
} else {
    $number = intval($last_number->curr_number) + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen= $namaagent->Initial;

$code_booking = 'PBU'.date('y').$agen.$kodemax;
//echo $code_booking;
?>

<!-- style -->
<style type="text/css">
    .chzn-container {
        width: 103% !important;
    }
    .simplemodal-container,
    #simplemodal-container .simplemodal-data{
        padding: 0 !important;
    }
    .tittle-header-modal{
        background: #165a91;
        padding: 15px;
        color: #fff;
    }
    #simplemodal-container h3{
        color: #fff;
    }
    .simplemodal-container{
        height: auto !important;
        top: 30% !important;
    }
</style>

<!-- tittle data -->
<div class="page_title">
    <div id="alert"></div>
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Tambah Data Penerimaan Barang Umum</h3>
</div>
<!-- ======================= -->

<!-- body content -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url() ?>Penerimaan_barang_umum">Data Penerimaan Barang Umum</a></li>
                            <li> Tambah Penerimaan Barang Umum</li>
                        </ul>
                    </div>
                </div>

                <div class="widget_top">
                    <div class="grid_12">
                        <span class="h_icon blocks_images"></span>
                        <h6>Tambah Penerimaan Barang Umum</h6>
                    </div>
                </div>
                <div class="widget_content">
                    <!--<form method="post" action="<?php echo base_url() ?>BookingOrder/proses_harga_jual_barang" class="form_container left_label">-->
                        <form class="form_container left_label">
                            <ul>

                                <!-- form header -->
                                <li class="body-search-data">
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_8">

                                            <!-- form 1 -->
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2 mb">Tanggal</label>
                                                <div class="form_input">
                                                    <input type="date" class="input-date-padding-3-2 form-control date-picker hidden-sm-down" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d');?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>

                                            <!-- form 2 -->
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2 mb">No SJC</label>
                                                <div class="form_input">
                                                    <input type="text" class="form_control" name="no_sjc" id="no_sjc">
                                                </div>
                                            </div>

                                            <!-- form 3 -->
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2 mb">Nomor PB</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="no_bo" id="no_bo" value="<?php echo $code_booking; ?>" readonly>
                                                </div>
                                            </div>

                                            <!-- form 4 -->
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2 mb">No PO</label>
                                                <div class="form_input">
                                                    <input type="text" class="form-control" name="no_po" id="no_po">
                                                </div>
                                            </div>

                                            <!-- form 5 -->
                                            <div class="form_grid_6">
                                                <label class="field_title mt-dot2 mb">Supplier</label>
                                                <div class="form_input">
                                                    <select data-placeholder="Cari Supplier" style="width:100%;" class="chzn-select" tabindex="13" name="id_supplier" id="id_supplier" required oninvalid="this.setCustomValidity('Supplier Tidak Boleh Kosong')">
                                                        <option value=""></option>
                                                        <?php foreach($supplier as $row) {
                                                            echo "<option value='$row->IDSupplier'>$row->Nama</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- form 6 -->
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2 mb">No SO</label>
                                                <div class="form_input">
                                                    <input type="text" class="form-control" name="no_so" id="no_so">
                                                </div>
                                            </div>

                                            <!-- form 7 -->
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2 mb">Total Yard</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="total_yard" id="total_yard" readonly>
                                                </div>
                                            </div>

                                            <!-- form 8 -->
                                            <div class="form_grid_6 mb-1">
                                                <label class="field_title mt-dot2 mb">Total Meter</label>
                                                <div class="form_input input-not-focus">
                                                    <input type="text" class="form-control" name="total_meter" id="total_meter" readonly>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- form 9 -->
                                        <div class="form_grid_4 mb-1">
                                            <label class="field_title mt-dot2 mb">Keterangan</label>
                                            <div class="form_input">
                                                <textarea name="keterangan" id="keterangan" rows="9" style="resize: none;"></textarea>
                                            </div>
                                        </div>

                                        <!-- clear content -->
                                        <div class="clear"></div>

                                    </div>
                                    <!-- end form header -->
                                </li>

                                <li>

                                    <!-- form body -->
                                    <div class="widget_content">
                                        <div class="form_grid_12 multiline">

                                            <!-- form 1 -->
                                            <div class="form_grid_3 mb-1">
                                                <input type="text" class="form-control" name="barcode" id="barcode" placeholder="Barcode">
                                            </div>

                                            <!-- form 2 -->
                                            <div class="form_grid_3 mb-1">
                                                <!--<input type="text" class="form-control" name="corak" id="corak" placeholder="Corak" required>-->
                                                <select data-placeholder="Cari corak" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" tabindex="13" name="corak" id="corak" onchange="findColor()">
                                                    <option value="" selected="" disabled>Pilih Corak</option>
                                                    <?php foreach($corak as $row) {
                                                        echo "<option value='$row->IDCorak'>$row->Corak</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <!-- form 3 -->
                                            <div class="form_grid_3 mb-1">
                                                <!--<input type="text" class="form-control" name="warna" id="warna" placeholder="Warna" required>-->
                                                <select name="warna" id="warna" data-placeholder="Cari warna" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="form_control" tabindex="13">
                                                    <option value="" selected disabled>Pilih Warna</option>
                                                </select>
                                            </div>

                                            <!-- form 4 -->
                                            <div class="form_grid_3 mb-1">
                                                <input type="text" class="form-control" name="merk" id="merk" placeholder="Merk">
                                            </div>

                                            <!-- form 5 -->
                                            <div class="form_grid_3 mb-1">
                                                <input type="text" class="form-control" name="qty_yard" id="qty_yard" placeholder="Qty Yard" onkeyup="convert('C')">
                                            </div>

                                            <!-- form 6 -->
                                            <div class="form_grid_3 mb-1">
                                                <input type="text" class="form-control" name="qty_meter" id="qty_meter" placeholder="Qty Meter" onkeyup="convert('F')">
                                            </div>

                                            <!-- form 7 -->
                                            <div class="form_grid_3 mb-1">
                                                <select name="grade" id="grade" class="form-control" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;">
                                                <option value="">Pilih Grade</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="S">S</option>
                                                <option value="E">E</option>
                                            </select>
                                            </div>

                                            <!-- form 8 -->
                                            <div class="form_grid_3 mb-1">
                                                <!--<input type="text" class="form-control" name="satuan" id="satuan" placeholder="Satuan" required>-->
                                                <select data-placeholder="Cari satuan" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="" tabindex="13" name="satuan" id="satuan">
                                                    <option value="" selected="" disabled>Pilih Satuan</option>
                                                    <?php foreach($satuan as $row) {
                                                        echo "<option value='$row->Satuan'>$row->Satuan</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </li>
                                <li><center>
                                    <div class="btn_30_blue">
                                        <span><input name="add" id="add" onclick="TambahPembelianPreview()" type="button" class="btn_small btn_blue" value="Tambah Data"></span>
                                    </div>
                                </center>
                            </li>
                            <div class="widget_content">
                                <div class="form_grid_12 w-100 alpha">
                                    <table class="display data_tbl">
                                        <thead>
                                            <tr class="odd gradeA">
                                                <th class="text-center">No</th>
                                                <th class="text-center">Barcode</th>
                                                <th class="text-center">Corak</th>
                                                <th class="text-center">Warna</th>
                                                <th class="text-center">Merk</th>
                                                <th class="text-center">Qty Yard</th>
                                                <th class="text-center">Qty Meter</th>
                                                <th class="text-center">Grade</th>
                                                <th class="text-center">Satuan</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_preview_penerimaan">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <li>
                               <div class="widget_content px-2 text-center">
                        <div class="py-4 mx-2"><div class="btn_30_blue">
                                        <span><input type="button" id="print_" name="print_" onclick="print_cek()" value="Print Data" title="Print Data"></span>
                                    </div>
                                    <div class="btn_30_blue">
                                        <span><input name="simpan" id="simpan" onclick="TambahPenerimaan()" type="button" value="Simpan" title="Print Data"></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>

        <!-- modal edit data -->
        <div id="basic-modal-content">
            <div class="tittle-header-modal">
                <h3>Ubah detail penerimaan barang umum</h3>
            </div>
            <form class="form_container left_label">
                <ul>
                    <li>
                        <div class="form_grid_12">

                            <div class="form_grid_6 mb-1">
                                <label class="field_title">Barcode</label>
                                <div class="form_input">
                                    <!--<input type="text" class="form-control" name="corak" id="corak" placeholder="Corak" required>-->
                                    <input type="text" class="form-control" name="vbarcode" id="vbarcode" required>
                                </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                                <label class="field_title">Corak</label>
                                <div class="form_input">
                                    <!--<input type="text" class="form-control" name="corak" id="corak" placeholder="Corak" required>-->
                                    <select data-placeholder="Cari satuan" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="" tabindex="13" name="vcorak" id="vcorak" onchange="getColor()"></select>
                                </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                                <label class="field_title">Warna</label>
                                <div class="form_input">
                                    <!--<input type="text" class="form-control" name="warna" id="warna" placeholder="Warna" required>-->
                                    <select data-placeholder="Cari warna" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" class="" tabindex="13" name="vwarna" id="vwarna"></select>
                                </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                                <label class="field_title">Merk</label>
                                <div class="form_input">
                                    <!--<input type="text" class="form-control" name="warna" id="warna" placeholder="Warna" required>-->
                                    <input type="text" class="form-control" name="vmerk" id="vmerk" required>
                                </div>
                            </div>

                            <div class="form_grid_6 mb-1">
                                <label class="field_title">Grade</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" name="vgrade" id="vgrade" placeholder="Grade" required>
                                </div>
                            </div>
                            <div class="form_grid_6 mb-1">
                                <label class="field_title">Qty Yard</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" name="vqty_yard" id="vqty_yard" placeholder="Qty Yard" required>
                                </div>
                            </div>
                            <div class="form_grid_6 mb-1">
                                <label class="field_title">Qty Meter</label>
                                <div class="form_input">
                                    <input type="text" class="form-control" name="vqty_meter" id="vqty_meter" placeholder="Qty Meter" required>
                                </div>
                            </div>
                            <div class="form_grid_6 mb-1">
                                <label class="field_title">Satuan</label>
                                <div class="form_input">
                                    <!--<input type="text" class="form-control" name="satuan" id="satuan" required>-->
                                    <select data-placeholder="Cari satuan" style="width: 102.5%; padding: 3px 2px;border-color: #d8d8d8;color: #757575;" tabindex="13" name="vsatuan" id="vsatuan"></select>
                                </div>
                            </div>

                            <div class="clear"></div><br>
                        </div>
                    </li>
                    <input type="hidden" name="vindex" id="vindex">
                    <input type="hidden" name="tmp_qty_yard" id="tmp_qty_yard">
                    <input type="hidden" name="tmp_qty_meter" id="tmp_qty_meter">
                    <li class="text-center">
                        <div class="btn_24_blue">
                            <a href="#" onclick="save_tmp_create()" class="simplemodal-close"><span>Simpan</span></a>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
        <!-- end modal edit data -->

    </div>
</div>
<script>
    var baseUrl = "<?php echo base_url()?>";
</script>
<script type="text/javascript" src="<?php echo base_url()?>libjs/penerimaan_barang_umum.js"></script>
<script>
    function convert(degree) {
        var x;
        if (degree == "C") {
            x = document.getElementById("qty_yard").value * 0.9144
            document.getElementById("qty_meter").value = x.toFixed(2);
        } else {
            x = (document.getElementById("qty_meter").value * 1.09361);
            document.getElementById("qty_yard").value = x.toFixed(2);
        }
    }
</script>
<?php $this->load->view('administrator/footer') ; ?>