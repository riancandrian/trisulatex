<?php $this->load->view('administrator/header') ; ?>


<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Laporan Invoice Pembelian</h3>
    <div class="top_search">
    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li>Laporan Invoice Pembelian </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form action="<?php echo base_url() ?>Laporan_invoice_pembelian/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                                <div class="form_grid_12 w-100 alpha">

                                    <div class="form_grid_9 mb-1">
                                        <div class="form_grid_3 mb-1">
                                            <label class="field_title mt-dot2">Periode</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_from" placeholder="mm/dd/yyyy" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_3 mb-1">
                                            <label class="field_title mt-dot2">s/d</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_until" placeholder="mm/dd/yyyy" value="<?php echo $tgl_terakhir ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_3 mb-1">
                                            <select name="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13">
                                                <option value="Nomor">No Inv Pembelian</option>
                                            </select>
                                        </div>
                                        <div class="form_grid_2 mb-1">
                                            <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form-control">
                                        </div>
                                        <div class="form_grid_1 mb-1">
                                            <div class="btn_24_blue">
                                                <input type="submit" name="" value="search">
                                            </div>
                                        </div>
                                    </div>

                                    <span class="clear"></span>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
                 <div style="position: relative;z-index: 1; left: 10px; top: 10px;font-size: 11px">
                    <div class="btn_30_light" style="position: absolute;">
                                    <a href="<?php echo base_url() ?>Laporan_invoice_pembelian/exporttoexcel"><span class="icon doc_excel_table_co"></span><span class="btn_link">Exsport XLS</span></a>
                                </div>
                </div>
                <form action="<?php echo base_url() ?>Pembelian_asset/delete_multiple" method="post" id="ubahstatus">
                    <div class="widget_content">
                        <table class="display data_tbl">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>No Invoice</th>
                                    <th>Nama Supplier</th>
                                    <th>No SJ Supplier</th>
                                    <th>Status PPN</th>
                                    <th>Barcode</th>
                                    <th>NoSO</th>
                                    <th>Party</th>
                                    <th>Nama Barang</th>
                                    <th>Corak</th>
                                    <th>Merk</th>
                                    <th>Warna</th>
                                    <th>Qty Yard</th>
                                    <th>Qty Meter</th>
                                    <th>Grade</th>
                                    <th>Remark</th>
                                    <th>Lebar</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                    <th>Jatuh Tempo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($invoice)){
                                    ?>
                                    <?php
                                }else{
                                    $i=1;
                                    foreach ($invoice as $data) {
                                        ?>
                                        <tr class="odd gradeA">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo formatDMY($data->tanggal); ?></td>
                                            <td><?php echo $data->nomor; ?></td>
                                            <td><?php echo $data->nama; ?></td>
                                            <td><?php echo $data->no_sj_supplier; ?></td>
                                            <td><?php echo $data->status_ppn; ?></td>
                                            <td><?php echo $data->barcode; ?></td>
                                            <td><?php echo $data->noso; ?></td>
                                            <td><?php echo $data->party; ?></td>
                                            <td><?php echo $data->nama_barang; ?></td>
                                            <td><?php echo $data->corak; ?></td>
                                            <td><?php echo $data->merk; ?></td>
                                            <td><?php echo $data->warna; ?></td>
                                            <td><?php echo convert_currency($data->saldo_yard); ?></td>
                                            <td><?php echo convert_currency($data->saldo_meter); ?></td>
                                            <td><?php echo $data->grade; ?></td>
                                            <td><?php echo $data->remark; ?></td>
                                            <td><?php echo $data->lebar; ?></td>
                                            <td><?php echo $data->satuan; ?></td>
                                            <td><?php echo convert_currency($data->harga); ?></td>
                                            <td><?php echo convert_currency($data->sub_total); ?></td>
                                            <td><?php echo formatDMY($data->tanggal_jatuh_tempo); ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                        <div class="widget_content text-right px-2">
                            <div class="py-4 mx-2">

                                <div class="btn_24_blue">
                                  <a href="#"><span>Kembali</span></a>
                              </div>
                          </div>
                      </div>

                  </div>
              </form>
          </div>
      </div>
  </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>