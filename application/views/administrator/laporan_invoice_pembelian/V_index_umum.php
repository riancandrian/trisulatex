<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
	<?php echo $this->session->flashdata('Pesan');?>
	<span class="title_icon"><span class="blocks_images"></span></span>
	<h3>Laporan Invoice Pembelian Umum</h3>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="breadCrumbHolder module">
					<div id="breadCrumb0" class="breadCrumb module white_lin">
						<ul>
							<li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
							<li> Laporan Invoice Pembelian Umum</li>
						</ul>
					</div>
				</div>
				<div class="widget_content">
					<form action="<?php echo base_url() ?>Laporan_invoice_pembelian/pencarian_umum" method="post" class="form_container left_label">
						<ul>
							<li class="px-1">

								<!-- search -->
								<div class="form_grid_12 w-100 alpha">

									<div class="form_grid_9 ">

										<!-- form 1 -->
										<div class="form_grid_3 ">   
											<label class="field_title mt-dot2">Periode</label>
											<div class="form_input">
												<input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
											</div>
										</div>

										<!-- form 2 -->
										<div class="form_grid_3 ">   
											<label class="field_title mt-dot2"> S/D </label>
											<div class="form_input">
												<input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
											</div>
										</div>

										<div class="form_grid_3 ">   
											<select name="jenispencarian" data-placeholder="No Intruksi" style="width: 100%!important" class="chzn-select" tabindex="13">
												<option value="Nomor">No Invoice</option>
											</select>
										</div>
										<div class="form_grid_2">   
											<input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
										</div>
										<div class="form_grid_1">
											<div class="btn_24_blue">
												<input type="submit" name="" value="search">
											</div>
										</div>
									</div>

									<span class="clear"></span>
								</div>
							</li>
						</ul>
					</form>
				</div>
				<div class="widget_content">
					<table class="display data_tbl">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Nomor</th>
								<th>Supplier</th>
								<th>Nama Barang</th>
								<th>Qty</th>
								<th>Satuan</th>
								<th>Harga Satuan</th>
								<th>Sub Total</th>
							</tr>
						</thead>
						<tbody>
							<?php if(!empty($invoice)){
								$i=1;
								foreach ($invoice as $data) {
									?>
									<tr class="odd gradeA">
										<td><?php echo $i; ?></td>
										<td><?php echo formatDMY($data->tanggal); ?></td>
										<td><?php echo $data->nomor; ?></td>
										<td><?php echo $data->namasup; ?></td>
										<td><?php echo $data->barang; ?></td>
										<td><?php echo convert_currency($data->saldo); ?></td>
										<td><?php echo $data->satuan; ?></td>
										<td><?php echo convert_currency($data->harga_satuan); ?></td>
										<td><?php echo convert_currency($data->sub_total); ?></td>
									</tr>
									<?php
									$i++;
								}
							}
							?>
						</tbody>
						<tfoot>

						</tfoot>
					</table>
				</div>
				<div class="widget_content text-right px-2">
					<div class="py-4 mx-2">
						<div class="btn_24_blue">
							<input type="button" name="" value="Kembali">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>