<?php $this->load->view('administrator/header') ; 

?>
<style type="text/css">
table.display td input{
  border: transparent !important;
  background: transparent !important;
}
</style>
<script src="<?php echo base_url() ?>asset/js/jquery-1.7.1.min.js"></script>
<div class="page_title">
       <?php echo $this->session->flashdata('Pesan');?>
       <span class="title_icon"><span class="blocks_images"></span></span>
       <h3>Laporan Giro</h3>
       <div class="top_search">

       </div>
   </div>
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<div id="content">
  <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
<div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                          <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                          <li class="active">Laporan Giro</li>
                      </ul>
                  </div>
              </div>
 <div class="widget_content">
  <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Laporan_giro/pencarian" class="form_container left_label">
    <ul>
      <li class="px-1">

        <!-- search -->
        <div class="form_grid_12 w-100 alpha">

          <!-- button -->


          <div class="form_grid_12 ">

            <!-- form 1 -->
            <div class="form_grid_3 ">   
              <label class="field_title mt-dot2">Periode</label>
              <div class="form_input">
                <input type="date" class="form-control" name="tanggal_mulai" value="<?php echo date('Y-m-d') ?>">
              </div>
            </div>

            <!-- form 2 -->
            <div class="form_grid_3 ">   
              <label class="field_title mt-dot2"> S/D </label>
              <div class="form_input">
                <input type="date" class="form-control" name="tanggal_selesai" value="<?php echo $tgl_terakhir ?>">
              </div>
            </div>
            <div class="form_grid_3 ">   
             <!--  <label class="field_title mt-dot2"> Jenis Giro </label> -->
              <select name="jenis_giro" style="width: 100%!important" class="chzn-select" tabindex="13">
                <option value="giro masuk">Giro Masuk</option>
                <option value="giro keluar">Giro Keluar</option>
              </select>
            </div>
            <div class="form_grid_3 ">   
              <!-- <label class="field_title mt-dot3"> Status Giro </label> -->
              <select name="status_giro" style="width: 100%!important" class="chzn-select" tabindex="13">
                <option value="semua">Semua</option>
                <option value="belum cair">Belum Cair</option>
                <option value="sudah cair">Sudah Cair</option>
                <option value="batal">Batal</option>
              </select>
            </div>
            <div class="form_grid_3 ">   
              <select name="keyword" style="width: 100%!important" class="chzn-select" tabindex="13">
                <option value="Nama_Perusahaan">Cari Nama Perusahaan</option>
                <option value="IDGiro">Cari Nomor Giro</option>
                <option value="IDBank">Cari Nama Bank</option>
              </select>
            </div>
            <div class="form_grid_2">   
              <input name="detail_keyword" type="text" placeholder="Cari Berdasarkan" class="form_container">
            </div>
            <div class="form_grid_1">
              <div class="btn_24_blue">
                <input type="submit" name="" value="search">
              </div>
            </div>
          </div>

          <span class="clear"></span>
        </div>
        <!-- end search -->

      </li>

    </form>
  </div>
  <?php
      if($this->input->post('tanggal_mulai') != ""){
        ?>
<center><h1>Laporan Giro</h1></center><br>
    <div class="table-responsive">
      
        <table class="display">

          <thead style="font-size: 13px;">

            <tr>
              <th>Tgl PH</th>
              <th>Nomor PH</th>
              <th>Tgl JT Giro</th>
              <th>Tgl Cair</th>
              <th>Jumlah</th>
              <th>Nama Perusahaan</th>
              <th>Nomor Giro</th>
            </tr>

          </thead>

          <tbody style="font-size: 13px">
           <?php
           foreach($laporangiro as $lr)
           {
            ?>
            <tr style="border-bottom: 1px solid #000;">
              <td><?php echo $lr->Tanggal ?></td>
              <td><?php echo $lr->Nomor ?></td>
              <td>-</td>
              <td><?php echo $lr->Tanggal_Cair ?></td>
              <td><?php echo $lr->Total ?></td>
              <td><?php echo $lr->Nama_Perusahaan ?></td>
              <td><?php echo $lr->Nomor_Giro ?></td>
            </tr>
            <?php
          }
          ?> 
        </tr>
      </tbody>

    </table>



    <?php
  }
  ?>

</div>
</div>
</div>
<?php $this->load->view('administrator/footer') ; ?>