<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">

    <!-- pesan notif -->
    <?php echo $this->session->flashdata('Pesan');?>
    <!-- =========== -->

    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Detail Buku Bank</h3>
</div>
<!-- ======================= -->

<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">

                <!-- breadcrumb -->
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li><a href="<?php echo base_url('BukuBank/index')?>">Data Buku Bank</a></li>
                            <li style="text-transform: capitalize;"> Detail Buku Bank - <b><?php echo $bukubank->Nomor ?></b> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="display data_tbl">
                            <thead style="display: none;">
                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $bukubank->Tanggal ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Nomor</td>
                                    <td style="background-color: #ffffff;"><?php echo $bukubank->Nomor?></td>
                                </tr>
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Posisi</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $bukubank->Posisi ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">Batal</td>
                                    <td style="background-color: #ffffff;"><?php echo $bukubank->Batal?></td>
                                </tr>     
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0;">Keterangan</td>
                                    <td width="65%" style="background-color: #e5eff0;"><?php echo $bukubank->Keterangan ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff;">COA</td>
                                    <td style="background-color: #ffffff;"><?php echo $bukubank->Nama_COA?></td>
                                </tr>     
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                        <div class="widget_content">

                            <table class="display data_tbl">
                              <thead>
                                 <tr>
                                    <th>No</th>
                                    <th>COA</th>
                                    <th>Posisi</th>
                                    <th>Debet</th>
                                    <th>Kredit</th>
                                    <th>Keterangan</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($bukubankdetail)){
                                  ?>
                                  <?php
                              }else{
                                  $i=1;
                                  foreach ($bukubankdetail as $data2) {
                                    ?>
                                    <tr class="odd gradeA">
                                      <td class="text-center"><?php echo $i; ?></td>
                                      <td><?php echo $data2->Nama_COA; ?></td>
                                      <td><?php echo $data2->Posisi; ?></td>
                                      <td><?php echo convert_currency($data2->Debet); ?></td>
                                      <td><?php echo convert_currency($data2->Kredit); ?></td>
                                      <td><?php echo $data2->Keterangan; ?></td>
                                  </tr>
                                  <?php
                                  $i++;
                              }
                          }
                          ?>

                      </tbody>
                  </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn_30_light">
                                    <span> <a href="<?php echo base_url('BukuBank/index')?>" name="simpan">Kembali</a></span>
                                </div>
                                 <div class="btn_30_blue">
                       <!--    <span><a href="<?php echo base_url() ?>JurnalUmum/print/<?php echo $bukubank->Nomor ?>">Print Data</a></span> -->
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>