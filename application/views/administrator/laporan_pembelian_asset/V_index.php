<?php $this->load->view('administrator/header') ; ?>

<!-- tittle data -->
<div class="page_title">
    <span class="title_icon"><span class="blocks_images"></span></span>
    <h3>Data Pembelian Asset</h3>
    <div class="top_search">

    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="breadCrumbHolder module">
                    <div id="breadCrumb0" class="breadCrumb module white_lin">
                        <ul>
                            <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                            <li> Pembelian Asset </li>
                        </ul>
                    </div>
                </div>

                <div class="widget_content">
                    <form action="<?php echo base_url() ?>Laporan_pembelian_asset/pencarian" method="post" class="form_container left_label">
                        <ul>
                            <li class="px-1">
                                <div class="form_grid_12 w-100 alpha">
                                    <div class="form_grid_9 mb-1">
                                        <div class="form_grid_3 mb-1">
                                            <label class="field_title mt-dot2">Periode</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_from" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_3 mb-1">
                                            <label class="field_title mt-dot2">s/d</label>
                                            <div class="form_input">
                                                <input type="date" class="form-control" name="date_until" value="<?php echo $tgl_terakhir ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_3 mb-1">
                                            <select name="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="chzn-select" tabindex="13" readonly>
                                                <option value="Nomor">No Pembelian Asset</option>
                                            </select>
                                        </div>
                                        <div class="form_grid_2 mb-1">
                                            <input name="keyword" type="text" placeholder="Cari Berdasarkan" class="form-control">
                                        </div>
                                        <div class="form_grid_1 mb-1">
                                            <div class="btn_24_blue">
                                                <input type="submit" name="" value="search">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clear"></div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="widget_content">
                    <table class="display" id="action_tbl">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>No Transaksi Asset</th>
                            <th>No Asset</th>
                            <th>Nama Asset</th>
                            <th>Nilai Perolehan</th>
                            <th>Akumulasi Penyusutan</th>
                            <th>Nilai Buku</th>
                            <th>Metode Penyusutan</th>
                            <th>Tanggal Penyusutan</th>
                            <th>Umur</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(empty($p_asset)){ ?>
                        <?php }else{
                            $i=1;
                            foreach ($p_asset as $data) {
                                ?>
                                <tr class="odd gradeA">
                                    <td class="text-center"><?php echo $i; ?></td>
                                    <td><?php echo formatDMY($data->tanggal); ?></td>
                                    <td><?php echo $data->nomor; ?></td>
                                    <td><?php echo $data->kode_asset; ?></td>
                                    <td><?php echo $data->nama_asset; ?></td>
                                    <th><?php echo $data->nilai_perolehan; ?></th>
                                    <th><?php echo $data->akumulasi_penyusutan; ?></th>
                                    <th><?php echo $data->nilai_buku; ?></th>
                                    <th><?php echo $data->metode_penyusutan; ?></th>
                                    <th><?php echo formatDMY($data->tanggal_penyusutan); ?></th>
                                    <th><?php echo $data->umur; ?></th>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                        <!-- </table> -->
                    </table>

                </div>
                <div class="widget_content text-right px-2">
                    <div class="py-4 mx-2">
                        <div class="btn_24_blue">
                            <a href="#" type="button"><span>Kembali</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('administrator/footer') ; ?>