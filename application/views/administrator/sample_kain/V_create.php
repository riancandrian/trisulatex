<?php $this->load->view('administrator/header') ;

if ($kodesample->curr_number == null) {
    $number = 1;
} else {
    $number = $kodesample->curr_number + 1;
}

$kodemax = str_pad($number, 5, "0", STR_PAD_LEFT);
$agen = $namaagent->Initial;

//$code_sk = 'SK'.date('y').$agen.$kodemax;
$code_sk = 'SK'.$kodemax;
?>
    <!-- =============================================Style untuk tabs -->
    <style type="text/css" scoped>
        .chzn-container {
            width: 103% !important;
        }
    </style>

    <!--========================Head Content -->
    <div class="page_title">
        <div id="alert"></div>
        <?php echo $this->session->flashdata('Pesan');?>
        <span class="title_icon"><span class="blocks_images"></span></span>
        <h3>Data Sample Kain</h3>
        <div class="top_search">
        </div>
    </div>
    <!--=======================Content-->
    <div id="content">
        <div class="grid_container">
            <div class="grid_12">
                <div class="widget_wrap">
                    <!-- =============================Title -->
                    <div class="breadCrumbHolder module">
                        <div id="breadCrumb0" class="breadCrumb module white_lin">
                            <ul>
                                <li><span><a class="action-icons" href="<?php echo base_url() ?>Dashboard" title="Dashboard" style="border: none;">Home</a></span></li>
                                <li><a href="<?php echo base_url() ?>SampleKain">Data Sample Kain</a></li>
                                <li class="active">Tambah Sample Kain</li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget_top">
                        <div class="grid_12">
                            <span class="h_icon blocks_images"></span>
                            <h6>Tambah Data Sample Kain</h6>
                        </div>
                    </div>
                    <!-- =============================content -->
                    <div class="widget_content">
                        <form method="post" action="" class="form_container left_label">
                            <ul>
                                <li class="body-search-data">
                                    <!-- form header -->
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">No Sample Kain</label>
                                            <div class="form_input">
                                                <input type="text" name="Nomor" id="Nomor" class="form-control" value="<?php echo $code_sk; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Nama Customer</label>
                                            <div class="form_input">
                                                <select name="IDSupplier" id="IDSupplier" style="width:100%;" class="chzn-select">
                                                    <option value="" selected disabled>Pilih Supplier</option>
                                                    <?php foreach($supplier as $row) {
                                                        echo "<option value='$row->IDSupplier'>$row->Nama</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_grid_12 w-100 alpha">
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Tanggal</label>
                                            <div class="form_input">
                                                <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                        <div class="form_grid_6 mb-1">
                                            <label class="field_title mt-dot2 mb">Keterangan</label>
                                            <div class="form_input">
                                                <textarea class="form-control" rows="2" name="keterangan" id="keterangan"></textarea>
                                            </div>
                                        </div>
                                        <input type="hidden" name="Qty_yard" id="Qty_yard" data-saldoYard class="form-control" >
                                        <input type="hidden" name="Qty_meter" id="Qty_meter"  data-saldoMeter class="form-control">
                                    </div>
                                    <div class="clear"></div>
                                    <!-- end form header -->
                                </li>

                                <!-- =============================-Scan Barcode -->
                                <li>
                                    <div class="form_grid_12 multiline">
                                        <label class="field_title mt-dot2 mb">Barcode Scan</label>
                                        <div class="form_input">
                                            <input type="text" name="Barcode" id="Barcode" class="form-control" placeholder="Scan Barcode" autofocus>
                                        </div>
                                    </div>
                                </li>

                                <div class="widget_content">
                                    <table class="display data_tbl" id="tabelfsdfsf">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Barcode</th>
                                            <th>Corak</th>
                                            <th>Warna</th>
                                            <th>Grade</th>
                                            <th>Qty Jual</th>
                                            <th>Satuan</th>
                                            <th>Harga</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tabel-cart-asset">

                                        </tbody>
                                    </table>
                                </div>
                                <li>
                                    <div class="widget_content px-2 text-center">
                                        <div class="py-4 mx-2">
                                            <!-- <div class="btn_30_blue">
                                                <span><a id="printdata" onclick="return false;" style="cursor: no-drop;">Print Data</a></span>
                                            </div> -->
                                            <div class="btn_30_blue">
                                                <span><input name="simpan" id="simpan" onclick="save()" type="button" value="Simpan Data" title="Simpan Data"></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>libjs/sample_kain.js"></script>
<?php $this->load->view('administrator/footer') ; ?>