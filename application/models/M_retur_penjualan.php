<?php

class M_retur_penjualan extends CI_Model
{
    private $table1 = 'tbl_retur_penjualan_kain';
    private $table2 = 'tbl_retur_penjualan_kain_detail';
    private $table3 = 'tbl_retur_penjualan_kain_grand_total';
    private $table4 = 'tbl_customer';
    private $table5 = 'tbl_corak';
    private $table6 = 'tbl_penjualan_kain';
    private $table7 = 'tbl_penjualan_kain_detail';
    private $table8 = 'tbl_penjualan_kain_grand_total';
    private $table9 = 'tbl_surat_jalan_customer_kain';
    private $table10 = 'tbl_customer';

    public function all()
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.*,' .
            // $this->table3 . '.Grand_total,' .
            $this->table4 . '.Nama,' .
            $this->table5 . '.Corak'
        )
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDRPK = ' . $this->table2 . '.IDRPK')
            // ->join($this->table3, $this->table1 . '.IDRPK = ' . $this->table3 . '.IDRPK')
            ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
            ->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak');

        $query = $this->db->get()->result();
        return $query;
    }

    function tampil_inv()
    {
        $this->db->select("IDFJK, Nomor")
            ->from($this->table6);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

        function cek_group_customer($id)
    {
     $this->db->select('tbl_group_customer.Nama_Group_Customer')->from("tbl_customer")
     ->join("tbl_group_customer", "tbl_group_customer.IDGroupCustomer = tbl_customer.IDGroupCustomer")
     ->where("tbl_customer.IDCustomer", $id);
     return $this->db->get()->row();
 }

    function tampil_supplier()
    {
        $query = $this->db->select("*")->from($this->table4)->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function no_retur($tahun)
    {
        return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
            ->where('extract(year from "Tanggal") =', $tahun)
            ->from($this->table1)
            ->get()->row();
    }

    function tampil_invoice_grand_total()
    {
        $this->db->select("*")
            ->from("tbl_pembelian_grand_total");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function check_invoice_penjualan($nomor)
    {
        // $this->db->distinct();
        $this->db->select('DISTINCT(tbl_packing_list_detail."Barcode"), tbl_corak."Corak", tbl_warna."Warna", tbl_merk."Merk", tbl_penjualan_kain_detail."Qty_yard", tbl_penjualan_kain_detail."Qty_roll, tbl_satuan."Satuan", tbl_penjualan_kain."Keterangan", tbl_penjualan_kain_detail."Harga", tbl_penjualan_kain_detail."Sub_total", tbl_surat_jalan_customer_kain_detail."Grade", tbl_barang."IDBarang", tbl_corak."IDCorak", tbl_merk."IDMerk", tbl_warna."IDWarna", tbl_satuan."IDSatuan"
            ')
            ->from("tbl_penjualan_kain_detail")
            ->join("tbl_penjualan_kain", "tbl_penjualan_kain_detail.IDFJK = tbl_penjualan_kain.IDFJK")
            //->join("tbl_penjualan_kain_grand_total", "tbl_penjualan_kain_grand_total.IDFJK = tbl_penjualan_kain.IDFJK")
            ->join("tbl_surat_jalan_customer_kain", "tbl_penjualan_kain.IDSJCK = tbl_surat_jalan_customer_kain.IDSJCK")
            ->join("tbl_surat_jalan_customer_kain_detail", "tbl_surat_jalan_customer_kain.IDSJCK = tbl_surat_jalan_customer_kain_detail.IDSJCK")
            ->join("tbl_packing_list", "tbl_surat_jalan_customer_kain.IDPAC = tbl_packing_list.IDPAC")
            ->join("tbl_packing_list_detail", "tbl_packing_list.IDPAC = tbl_packing_list_detail.IDPAC")
            ->join("tbl_corak", "tbl_penjualan_kain_detail.IDCorak=tbl_corak.IDCorak")
            ->join("tbl_warna", "tbl_penjualan_kain_detail.IDWarna=tbl_warna.IDWarna")
            ->join("tbl_satuan", "tbl_penjualan_kain_detail.IDSatuan=tbl_satuan.IDSatuan")
            ->join("tbl_barang", "tbl_penjualan_kain_detail.IDBarang = tbl_barang.IDBarang")
            ->join("tbl_merk", "tbl_barang.IDMerk = tbl_merk.IDMerk")
            ->where("tbl_penjualan_kain.Nomor", $nomor);
        $query = $this->db->get();

        return $query->result();
    }

    function cek_grand_total($Nomor)
    {
        $this->db->select("*")
            ->from($this->table6)
            ->join($this->table8, $this->table6 . '.IDFJK = ' . $this->table8 . '.IDFJK')
            ->join($this->table9, $this->table6 . '.IDSJCK = ' . $this->table9 . '.IDSJCK')
            ->join($this->table10, $this->table10 . '.IDCustomer = ' . $this->table6 . '.IDCustomer')
            ->where($this->table6 . '.Nomor', $Nomor);
        $query = $this->db->get();

        return $query->result();
    }

    function tampilkan_id_retur()
    {
        $this->db->select_max("IDRPK")->from($this->table1);
        $query = $this->db->get();

   return $query->row();
    }

    function tampilkan_id_retur_detail()
    {
        $this->db->select_max("IDRPKDetail")->from($this->table2);
        $query = $this->db->get();

   return $query->row();
    }

    function tampilkan_id_retur_grand_total()
    {
        $this->db->select_max("IDRPKGrandTotal")->from($this->table3);
        $query = $this->db->get();

   return $query->row();
    }

    function get_TPKbyNomor($Nomor)
    {
        return $this->db->get_where($this->table6, array('Nomor' => $Nomor))->row();
    }

    public function add($data){
        $this->db->insert($this->table1, $data);
        return TRUE;
    }

    public function add_detail($data){
        $this->db->insert($this->table2, $data);
        return TRUE;
    }

    public function add_grand_total($data){
        $this->db->insert($this->table3, $data);
        return TRUE;
    }

    function print_retur_pembelian($nomor)
    {
        $this->db->select('*')->from($this->table1)->where("Nomor", $nomor);
        return $this->db->get()->row();
    }
    function print_retur_pembelian_detail($id)
    {
        $this->db->select("
            tbl_retur_penjualan_kain_detail.*, 
            tbl_corak.Corak, 
            tbl_warna.Warna, 
            tbl_merk.Merk, 
            tbl_satuan.Satuan, 
            tbl_barang.Nama_Barang, 
            tbl_retur_penjualan_kain.Nomor")
            ->from("tbl_retur_penjualan_kain_detail")
            ->join("tbl_retur_penjualan_kain", "tbl_retur_penjualan_kain_detail.IDRPK = tbl_retur_penjualan_kain.IDRPK")
            ->join("tbl_corak", "tbl_retur_penjualan_kain_detail.IDCorak=tbl_corak.IDCorak")
            ->join("tbl_barang", "tbl_barang.IDBarang=tbl_retur_penjualan_kain_detail.IDBarang")
            ->join("tbl_warna", "tbl_retur_penjualan_kain_detail.IDWarna=tbl_warna.IDWarna")
            ->join("tbl_merk", "tbl_retur_penjualan_kain_detail.IDMerk=tbl_merk.IDMerk")
            ->join("tbl_satuan", "tbl_retur_penjualan_kain_detail.IDSatuan=tbl_satuan.IDSatuan")
            ->where("tbl_retur_penjualan_kain_detail.IDRPK", $id);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function print_grand_total($id)
    {
        $this->db->select('*')->from($this->table3)
            ->where("IDRPK", $id);
        return $this->db->get()->row();
    }

    function find($id)
    {
        return $this->db->select(
            $this->table1 . '.*,' .
            $this->table10 . '.Nama,' .
            $this->table10 . '.IDCustomer,' .
            $this->table6 . '.Nomor AS Nomor_inv')
            ->from($this->table1)
            ->join($this->table10, $this->table1 . ".IDCustomer = " . $this->table4 .".IDCustomer")
            ->join($this->table6, $this->table6 . ".IDFJK = " . $this->table1 . ".IDFJK")
            ->where($this->table1 . '.IDRPK', $id)
            ->get()->row();
    }

    function find_detail($id)
    {
        $this->db->select("
        tbl_retur_penjualan_kain_detail.*, 
        tbl_merk.Merk, 
        tbl_barang.Nama_Barang, 
        tbl_corak.Corak, 
        tbl_satuan.Satuan,
        tbl_warna.Warna")
            ->from("tbl_retur_penjualan_kain_detail")
            ->join("tbl_barang", "tbl_barang.IDBarang=tbl_retur_penjualan_kain_detail.IDBarang")
            ->join("tbl_merk", "tbl_retur_penjualan_kain_detail.IDMerk=tbl_merk.IDMerk")
            ->join("tbl_corak", "tbl_retur_penjualan_kain_detail.IDCorak=tbl_corak.IDCorak")
            ->join("tbl_warna", "tbl_retur_penjualan_kain_detail.IDWarna=tbl_warna.IDWarna")
            ->join("tbl_satuan", "tbl_retur_penjualan_kain_detail.IDSatuan=tbl_satuan.IDSatuan")
            ->where("tbl_retur_penjualan_kain_detail". '.IDRPK', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.*,' .
            $this->table3 . '.Grand_total,' .
            $this->table4 . '.Nama,' .
            $this->table5 . '.Corak'
        )
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDRPK = ' . $this->table2 . '.IDRPK')
            ->join($this->table3, $this->table1 . '.IDRPK = ' . $this->table3 . '.IDRPK')
            ->join($this->table4, $this->table1 . '.IDSupplier = ' . $this->table4 . '.IDSupplier')
            ->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak');


        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by('IDRPK', 'DESC')->get();
        return $query->result();
    }

    function update_status($data, $where)
    {
        return $this->db->update($this->table1, $data, $where);
    }

// <<<<<<< Updated upstream
    public function update_master($id,$data){
        return $this->db->update($this->table1, $data, array('IDRPK' => $id));
    }

    public function drop($id){
        $this->db->where('IDRPK', $id);
        $this->db->delete($this->table2);
        return TRUE;
    }

    public function update_grand_total($id, $datas){
        return $this->db->update($this->table3, $datas, array('IDRPK' => $id));
    }
// =======
       public function get_all_procedure()
  {
    
        $query = ('
      SELECT * FROM laporan_retur_penjualan_kain()
    ');

  /*$query = ('select * from pengukuran');  */

  return $this->db->query($query)->result();
  }

  public function searching_store($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_retur_penjualan_kain(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_retur_penjualan_kain_like(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }
// >>>>>>> Stashed changes
}