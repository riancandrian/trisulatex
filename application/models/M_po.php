<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

/**
* 
*/
class M_po extends CI_Model
{
	
  private $table = 'tbl_purchase_order';
  private $table_umum = 'tbl_purchase_order_umum';
  private $table_po_asset = 'tbl_po_asset';
  private $order = 'DESC';
  private $id = 'IDPO';
  private $id2 = 'IDPOUmum';

  // Fungsi Select semua data Menu
  public function get_all($id)
  {

    $this->db->order_by($this->id, $this->order);
    $this->db->where('Jenis_PO', $id);
    
    $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_purchase_order.IDCorak', 'left');
    $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_purchase_order.IDMerk', 'left');
    
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
   } 
 }

function tampilkan_get_pb($id)
{
 return $this->db->select("IDPO")
  ->from("tbl_terima_barang_supplier")
  ->where("IDPO", $id)
 ->get()->row();
}

function group_barang()
{
  $this->db->select("*")
  ->from("tbl_group_barang")
  ->where("Group_Barang !=", 'kain');
  $query = $this->db->get();

  if ($query->num_rows() > 0)
  {
    return $query->result();
  }
}

 public function get_all_po_umum2()
 {

  $this->db->order_by($this->id2, $this->order);

  $data = $this->db->get($this->table_umum);

  $hitung = $this->db->count_all_results($this->table_umum);
  if ( $hitung > 0) {
   return $data->result();
 } 
}

  function cek_group_barang($group)
    {
        $this->db->select('*')
        ->from("tbl_barang")
        ->join("tbl_group_barang", "tbl_barang.IDGroupBarang=tbl_group_barang.IDGroupBarang")
        ->where("tbl_group_barang.Group_Barang", $group);
        $query = $this->db->get();

        return $query->result();
    }

    function cek_group_barang_asset()
    {
        $this->db->select('*')
        ->from("tbl_asset");
        $query = $this->db->get();

        return $query->result();
    }

    function cek_group_barang_asset2($group)
    {
        $this->db->select('*')
        ->from("tbl_asset")
        ->join("tbl_group_asset", "tbl_group_asset.IDGroupAsset=tbl_asset.IDGroupAsset")
        ->like("tbl_group_asset.Group_Asset", $group);
        $query = $this->db->get();

        return $query->result();
    }

function tampilkan_id_po()
{
  $this->db->select_max("IDPO")
  ->from("tbl_purchase_order");
  $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_po_umum2()
{
  $this->db->select_max("IDPOUmum")
  ->from("tbl_purchase_order_umum");
  $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_po_asset()
{
  $this->db->select_max("IDPOAsset")
  ->from("tbl_po_asset");
  $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_po_asset_detail()
{
  $this->db->select_max("IDPOAssetDetail")
  ->from("tbl_po_asset_detail");
  $query = $this->db->get();

  return $query->row();
}

function tampilkan_satuan()
{
  $this->db->select("*")
  ->from("tbl_satuan");
  $query = $this->db->get();

  if ($query->num_rows() > 0)
  {
    return $query->result();
  }
}

function tampilkan_id_po_detail()
{
  $this->db->select_max("IDPODetail")
  ->from("tbl_purchase_order_detail");
   $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_po_detail_umum2()
{
  $this->db->select_max("IDPOUmumDetail")
  ->from("tbl_purchase_order_umum_detail");
  $query = $this->db->get();

  return $query->row();
}

  // ---------------------------Fungsi get all procedure
public function get_all_procedure($id)
{

  $query = ('
    SELECT * FROM getpo2(?)
    ');

  /*$query = ('select * from pengukuran');	*/

  return $this->db->query($query, array($id))->result();
}

public function get_all_procedure_umum()
{

  $query = ('
    SELECT * FROM getpoumum()
    ');

  /*$query = ('select * from pengukuran');  */

  return $this->db->query($query)->result();
}

  //Get Data by ID
public function get_by_id($id)
{
    // $this->db->where($this->id, $id);
    // $data = $this->db->get($this->table);

    // $hitung = $this->db->count_all_results($this->table);
    // if ( $hitung > 0) {
    //  return $data->row();
    // } 

  return $this->db->select('tbl_purchase_order.*, tbl_agen.Nama_Perusahaan,  tbl_agen.Initial, tbl_agen.IDAgen, tbl_suplier.Nama, tbl_barang.Nama_Barang, tbl_corak.Corak, tbl_merk.Merk')
  ->join('tbl_agen', 'tbl_agen.IDAgen=tbl_purchase_order.IDAgen')
  ->join('tbl_suplier', 'tbl_suplier.IDSupplier=tbl_purchase_order.IDSupplier')
  ->join('tbl_barang', 'tbl_barang.IDBarang=tbl_purchase_order.IDBarang')
  ->join('tbl_corak', 'tbl_corak.IDCorak=tbl_purchase_order.IDCorak')
  ->join('tbl_merk', 'tbl_merk.IDMerk=tbl_purchase_order.IDMerk')
  ->where('tbl_purchase_order.IDPO', $id)
  ->from("tbl_purchase_order")
  ->get()->row();


}

public function get_by_id_umum($id)
{
  return $this->db->select('tbl_purchase_order_umum.*, tbl_agen.Nama_Perusahaan, tbl_agen.Initial, tbl_agen.IDAgen, tbl_agen.Nama_Perusahaan,tbl_suplier.Nama')
  ->join('tbl_agen', 'tbl_agen.IDAgen=tbl_purchase_order_umum.IDAgen')
  ->join('tbl_suplier', 'tbl_suplier.IDSupplier=tbl_purchase_order_umum.IDSupplier')
  ->where('tbl_purchase_order_umum.IDPOUmum', $id)
  ->from("tbl_purchase_order_umum")
  ->get()->row();


}

public function get_byid_asset($id)
{
  return $this->db->select('tbl_po_asset.*, tbl_suplier.Nama')
  ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_po_asset.IDSupplier")
  ->from("tbl_po_asset")
  ->where("tbl_po_asset.IDPOAsset", $id)
  ->get()->row();
}

public function get_byid_assetd($id)
{
  return $this->db->select('tbl_po_asset_detail.*,tbl_asset."Nama_Asset",tbl_group_asset."Group_Asset"')
  ->from("tbl_po_asset_detail")
  ->join("tbl_asset", "tbl_asset.'IDAsset'=tbl_po_asset_detail.'IDAsset'")
  ->join("tbl_group_asset", "tbl_asset.'IDGroupAsset'=tbl_group_asset.'IDGroupAsset'")
  ->join("tbl_po_asset", "tbl_po_asset.'IDPOAsset'=tbl_po_asset_detail.'IDPOAsset'")
  ->where("tbl_po_asset.IDPOAsset", $id)
  ->get()->row();
}

public function getPO()
{
  return $this->db->select('*')
  ->from("tbl_purchase_order")
  ->where("Nomor", $no)
  ->get()->row();


}

public function get_po_detail($id)
{
  $this->db->join('tbl_warna', 'tbl_warna.IDWarna = tbl_purchase_order_detail.IDWarna');

  $this->db->where('IDPO', $id);
  $data = $this->db->get('tbl_purchase_order_detail');

  $hitung = $this->db->count_all_results('tbl_purchase_order_detail');
  if ( $hitung > 0) {
   return $data->result();
 } 


}

public function get_po_detail_umum($id)
{
  $this->db->join('tbl_barang', 'tbl_barang.IDBarang = tbl_purchase_order_umum_detail.IDBarang');
  $this->db->join('tbl_satuan', 'tbl_satuan.IDSatuan = tbl_purchase_order_umum_detail.IDSatuan');
  $this->db->join('tbl_group_barang', 'tbl_group_barang.IDGroupBarang = tbl_barang.IDGroupBarang');

  $this->db->where('IDPOUmum', $id);
  $data = $this->db->get('tbl_purchase_order_umum_detail');

  $hitung = $this->db->count_all_results('tbl_purchase_order_umum_detail');
  if ( $hitung > 0) {
   return $data->result();
 } 




}

  //Fungsi Tambah Data
public function insert($data)
{
  $this->db->insert($this->table, $data);
}

  //Fungsi Ubah Data
public function update($id, $data)
{
  $this->db->where($this->id, $id);
  $this->db->update($this->table, $data);
}

public function update_umum($id, $data)
{
  $this->db->where($this->id2, $id);
  $this->db->update($this->table_umum, $data);
}

  //Fungsi Hapus Data
public function delete($id)
{
  $this->db->where($this->id, $id);
  $this->db->delete($this->table);
}

function cari($kolom,$status,$keyword){
  $this->db->select("*")
  ->from("tbl_merk")
  ->like("Aktif", $status)
  ->like("$kolom", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function cari_by_kolom($kolom,$keyword){
 $this->db->select("*")
 ->from("tbl_merk")
 ->like("$kolom", $keyword);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_status($status){
 $this->db->select("*")
 ->from("tbl_merk")
 ->where("Aktif", $status);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_keyword($keyword){
  $this->db->select("*")
  ->from("tbl_merk")
  ->like("Kode_Merk", $keyword)
  ->or_like("Merk", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function get_supplier(){
  $this->db->select("IDSupplier, Nama")
  ->from("tbl_suplier");
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function tampilkan_barang_non_kain(){
  $this->db->select("IDBarang, Nama_Barang")
  ->from("tbl_barang")
  ->join("tbl_group_barang", "tbl_barang.IDGroupBarang=tbl_group_barang.IDGroupBarang")
  ->where("tbl_group_barang.Group_Barang !=", 'kain');
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

  //-------------------NO Purchase Order
function no_po($id,$tahun)
{
  return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
  ->where('Jenis_PO', $id)
  ->where('extract(year from "Tanggal") =', $tahun)
  ->from($this->table)
  ->get()->row();
}

function no_poasset($id,$tahun)
{
  return $this->db->select('MAX(RIGHT("IDPOAsset",5)) as curr_number')
  ->where('Jenis_PO', $id)
  ->where('extract(year from "CTime") =', $tahun)
  ->from("tbl_po_asset")
  ->get()->row();
}


function no_po_umum2($tahun)
{
  return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
  ->where('extract(year from "Tanggal") =', $tahun)
  ->from("tbl_purchase_order_umum")
  ->get()->row();
}

  //-------------------Get Trisula
function get_trisula()
{
  $this->db->where('Kode_Suplier', 'SRLT-01');
  $data = $this->db->get('tbl_suplier');

  $hitung = $this->db->count_all_results('tbl_suplier');
  if ( $hitung > 0) {
   return $data->row();
 } 
}

  //---------------------GetMerk
function getmerk($id)
{
  $this->db->from('tbl_corak');

  $this->db->join('tbl_barang', 'tbl_barang.IDMerk = tbl_corak.IDMerk');
  $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk');
  $this->db->where('tbl_corak.IDCorak', $id);
    //$this->db->join('tbl_barang', 'tbl_barang.IDMerk = tbl_corak.IDMerk');

  $data = $this->db->get();

  $hitung = $this->db->count_all_results('tbl_corak');
  if ( $hitung > 0) {
   return $data->row();
 } 
}

    //--------------------GetGroupAsset
function getgroup($id)
{
  $this->db->where('IDGroupAsset', $id);
   //$this->db->join('tbl_group_asset', 'tbl_asset."IDGroupAsset" = tbl_group_asset."IDGroupAsset"', 'left');
   $data = $this->db->get('tbl_group_asset');
   $hitung = $this->db->count_all_results('tbl_group_asset');
   if ( $hitung > 0) {
    return $data->result();
 } 
}

function getasset($id)
{
    //$this->db->where('IDGroupAsset', "'".$id."'");
     //$this->db->join('tbl_group_asset', 'tbl_asset."IDGroupAsset" = tbl_group_asset."IDGroupAsset"', 'left');
     $this->db->order_by('Nama_Asset', 'asc');
     $data = $this->db->get('tbl_asset');
     $hitung = $this->db->count_all_results('tbl_asset');
     if ( $hitung > 0) {
      return $data->result();
   } 
}
   //---------------------Getwarna
function getwarna($id)
{
 $this->db->where('Kode_Corak', $id);
 $this->db->join('tbl_corak', 'tbl_warna.IDCorak = tbl_corak.IDCorak', 'left');
 $data = $this->db->get('tbl_warna');
 $hitung = $this->db->count_all_results('tbl_warna');
 if ( $hitung > 0) {
  return $data->result();
} 
}


   //Insert data Atas
public function add($data){
  $this->db->insert($this->table, $data);
  return TRUE;
}

public function add_asset($data){
  $this->db->insert("tbl_po_asset", $data);
  return TRUE;
}

public function add_po_umum2($data){
  $this->db->insert("tbl_purchase_order_umum", $data);
  return TRUE;
}

  //Insert Detail nya
public function add_detail($data){
  $this->db->insert('tbl_purchase_order_detail', $data);
  return TRUE;
}

public function add_detail_po_umum2($data){
  $this->db->insert('tbl_purchase_order_umum_detail', $data);
  return TRUE;
} 

public function add_detail_asset($data){
  $this->db->insert('tbl_po_asset_detail', $data);
  return TRUE;
}

  //Ubah Data Master
public function update_master($id,$data){
  return $this->db->update('tbl_purchase_order', $data, array('IDPO' => $id));
}

public function update_master_umum($id,$data){
  return $this->db->update('tbl_purchase_order_umum', $data, array('IDPOUmum' => $id));
}

public function drop($id){
  $this->db->where('IDPODetail', $id);
  $this->db->delete('tbl_purchase_order_detail');
  return TRUE;
}

public function drop_umum($id){
  $this->db->where('IDPOUmumDetail', $id);
  $this->db->delete('tbl_purchase_order_umum_detail');
  return TRUE;
}

function searching($date_from, $date_until, $search_type, $keyword, $id)
{


  $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_purchase_order.IDCorak');
  $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_purchase_order.IDMerk');

  $this->db->where('Jenis_PO', $id);

  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
  }

  if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
  }

  if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
  }

  $query = $this->db->order_by('IDPO', 'DESC')->get($this->table);
  return $query->result();
}

function searching_umum($date_from, $date_until, $search_type, $keyword, $id)
{

  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
  }

  if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
  }

  if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
  }

  $query = $this->db->order_by('IDPOUmum', 'DESC')->get("tbl_purchase_order_umum");
  return $query->result();
}

  //--------------------------Searching Store Procedure
public function searching_store($id, $date_from, $date_until, $keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_po(?, ?, ?, ?)
    ');

  /*$query = ('select * from pengukuran');	*/

  return $this->db->query($query, array($id, $date_from, $date_until, $new_keywords))->result();
}

  //--------------------------Searching Store Procedure
public function searching_store_like($id, $keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_po_like(?, ?)
    ');

  /*$query = ('select * from pengukuran');	*/

  return $this->db->query($query, array($id, $new_keywords))->result();
}

function print_po($nomor)
{
  $this->db->select('tbl_purchase_order.*, tbl_corak.Corak, tbl_merk.Merk, tbl_agen.Nama_Perusahaan')->from("tbl_purchase_order")
  ->join("tbl_corak", "tbl_purchase_order.IDCorak = tbl_corak.IDCorak")
  ->join("tbl_merk", "tbl_purchase_order.IDMerk = tbl_merk.IDMerk")
  ->join("tbl_agen", "tbl_purchase_order.IDAgen = tbl_agen.IDAgen")
  ->where("tbl_purchase_order.IDPO", $nomor);
  return $this->db->get()->row();
}

function print_po_umum($nomor)
{
  $this->db->select('tbl_purchase_order_umum.*, tbl_agen.Nama_Perusahaan')->from("tbl_purchase_order_umum")
  ->join("tbl_agen", "tbl_purchase_order_umum.IDAgen = tbl_agen.IDAgen")
  ->where("tbl_purchase_order_umum.IDPOUmum", $nomor);
  return $this->db->get()->row();
}
function print_po_detail($id)
{
 $this->db->select("tbl_purchase_order_detail.*, tbl_warna.Warna")
 ->from("tbl_purchase_order_detail")
 ->join("tbl_warna", "tbl_purchase_order_detail.IDWarna=tbl_warna.IDWarna")
 ->where("tbl_purchase_order_detail.IDPO", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}
function print_po_umum_detail($id)
{
 $this->db->select("tbl_purchase_order_umum_detail.*, tbl_barang.Nama_Barang, tbl_satuan.Satuan")
 ->from("tbl_purchase_order_umum_detail")
 ->join("tbl_barang", "tbl_purchase_order_umum_detail.IDBarang=tbl_barang.IDBarang")
 ->join("tbl_satuan", "tbl_satuan.IDSatuan=tbl_purchase_order_umum_detail.IDSatuan")
 ->where("tbl_purchase_order_umum_detail.IDPOUmum", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}
function last_Id()
{
  $this->db->select('IDFBA');
  $this->db->from('tbl_pembelian_asset');
  $this->db->order_by('IDFBA','DESC');
  $this->db->limit(1);
  $query = $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->row();
  }else{
    return "null";
  }
}

  public function exsport_excel($jenispo) {
        $this->db->select(array("tbl_purchase_order.Tanggal", "tbl_purchase_order.Nomor", "tbl_suplier.Nama", "tbl_corak.Corak", "tbl_warna.Warna", "tbl_merk.Merk", "tbl_purchase_order.Lot", "tbl_purchase_order.Jenis_Pesanan", "tbl_purchase_order.PIC", "tbl_purchase_order.Prioritas", "tbl_purchase_order.Stamping","tbl_purchase_order.Bentuk", "tbl_purchase_order.Panjang", "tbl_purchase_order.Point", "tbl_purchase_order.Kirim", "tbl_purchase_order.Posisi", "tbl_purchase_order.Posisi1", "tbl_purchase_order.Album", "tbl_purchase_order.M10", "tbl_purchase_order.Kain", "tbl_purchase_order.Lembaran"));
        $this->db->from('tbl_purchase_order');
        $this->db->join('tbl_purchase_order_detail', "tbl_purchase_order.IDPO=tbl_purchase_order_detail.IDPO");
        $this->db->join('tbl_corak', "tbl_purchase_order.IDCorak=tbl_corak.IDCorak");
        $this->db->join('tbl_warna', "tbl_purchase_order_detail.IDWarna=tbl_warna.IDWarna");
        $this->db->join('tbl_merk', "tbl_purchase_order.IDMerk=tbl_merk.IDMerk");
        $this->db->join('tbl_suplier', "tbl_suplier.IDSupplier=tbl_purchase_order.IDSupplier");
        $this->db->where("tbl_purchase_order.Batal", 'aktif');
        $this->db->where("tbl_purchase_order.Jenis_PO", $jenispo);
        $this->db->order_by("tbl_purchase_order.Nomor", 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function exsport_excel_umum() {
        $this->db->select(array("tbl_purchase_order_umum.Tanggal", "tbl_purchase_order_umum.Nomor", "tbl_suplier.Nama", "tbl_barang.Nama_Barang", "tbl_purchase_order_umum_detail.Qty", "tbl_satuan.Satuan", "tbl_purchase_order_umum_detail.Harga_satuan", "tbl_purchase_order_umum_detail.Sub_total"));
        $this->db->from('tbl_purchase_order_umum');
        $this->db->join('tbl_purchase_order_umum_detail', "tbl_purchase_order_umum.IDPOUmum=tbl_purchase_order_umum_detail.IDPOUmum");
        $this->db->join('tbl_barang', "tbl_barang.IDBarang=tbl_purchase_order_umum_detail.IDBarang");
        $this->db->join('tbl_suplier', "tbl_suplier.IDSupplier=tbl_purchase_order_umum.IDSupplier");
        $this->db->join('tbl_satuan', "tbl_satuan.IDSatuan=tbl_purchase_order_umum_detail.IDSatuan");
        $this->db->where("tbl_purchase_order_umum.Batal", 'aktif');
        $this->db->order_by("tbl_purchase_order_umum.Nomor", 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
}
?>