<?php

class M_penerimaan_barang_jahit extends CI_Model
{


    function tampilkan_pb_jahit()
    {
        $this->db->select("tbl_terima_barang_supplier_jahit.*, tbl_suplier.Nama")
            ->from("tbl_terima_barang_supplier_jahit")
            ->join("tbl_suplier", "tbl_terima_barang_supplier_jahit.IDSupplier=tbl_suplier.IDSupplier");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }


    function tampilkan_no_makloon()
    {
        $this->db->select("IDSJH, Nomor")
            ->from("tbl_surat_jalan_makloon_jahit");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_terima_barang_jahit_detail()
    {
        $this->db->select_max("IDTBSHDetail")
            ->from("tbl_terima_barang_supplier_jahit_detail");
        $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_terima_barang_jahit()
    {
        $this->db->select_max("IDTBSH")
            ->from("tbl_terima_barang_supplier_jahit");
        $query = $this->db->get();

  return $query->row();
    }

    function cek_group_supplier($id)
{
   $this->db->select('tbl_group_supplier.Group_Supplier')->from("tbl_suplier")
   ->join("tbl_group_supplier", "tbl_group_supplier.IDGroupSupplier = tbl_suplier.IDGroupSupplier")
   ->where("tbl_suplier.IDSupplier", $id);
   return $this->db->get()->row();
}

    function tampil_barang()
    {

        $this->db->select("*")
        ->from("tbl_barang")
        ->join("tbl_group_barang", "tbl_barang.IDGroupBarang=tbl_group_barang.IDGroupBarang")
        ->where("tbl_group_barang.Group_Barang !=", 'kain');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }

        // return $this->db->get('tbl_barang')
        // ->join("tbl_group_barang", "tbl_barang.IDGroupBarang=tbl_group_barang.IDGroupBarang")
        // ->where("tbl_group_barang.Group_Barang !=", 'kain')->result();
    }

     function tampil_supplier()
    {
        return $this->db->get('tbl_suplier')->result();
    }

     function tampil_merk()
    {
        return $this->db->get('tbl_merk')->result();
    }

     function tampil_satuan()
    {
        return $this->db->get('tbl_satuan')->result();
    }

    function no_pb_jahit($tahun)
    {
         return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
                ->where('extract(year from "Tanggal") =', $tahun)
                ->from("tbl_terima_barang_supplier_jahit")
                ->get()->row();
        }

       
    function simpan($data)
    {
        $this->db->insert('tbl_asset', $data);
        return TRUE;
    }

    function detail_pembelian_asset($id)
    {
         $this->db->select("*")
            ->from("tbl_pembelian_asset")
            ->join("tbl_pembelian_asset_detail", "tbl_pembelian_asset.IDFBA=tbl_pembelian_asset_detail.IDFBA")
            ->where("tbl_pembelian_asset_detail.IDFBA", $id);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }


    function update_status($data, $where)
    {
        return $this->db->update('tbl_terima_barang_supplier_jahit', $data, $where);
    }

    function delete($id)
    {
        $this->db->where('IDAsset', $id);
        $this->db->delete('tbl_asset');
    }

     function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->select("tbl_terima_barang_supplier_jahit.*, tbl_suplier.Nama")
            ->from("tbl_terima_barang_supplier_jahit")
            ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_terima_barang_supplier_jahit.IDSupplier");


        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by('IDTBSH', 'DESC')->get();
        return $query->result();
    }

public function add($data){
       $this->db->insert('tbl_terima_barang_supplier_jahit', $data);
        return TRUE;
    }

    public function add_detail($data){
        $this->db->insert('tbl_terima_barang_supplier_jahit_detail', $data);
        return TRUE;
    }
       function find($id)
    {
        return $this->db->select('*')
            ->from("tbl_terima_barang_supplier_jahit")
            ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_terima_barang_supplier_jahit.IDSupplier")
            ->where("tbl_terima_barang_supplier_jahit". '.IDTBSH', $id)
            ->get()->row();
    }
    public function drop($id){
        $this->db->where('IDTBSHDetail', $id);
        $this->db->delete('tbl_terima_barang_supplier_jahit_detail');
        return TRUE;
    }

    public function update_master($id,$data){
        return $this->db->update('tbl_terima_barang_supplier_jahit', $data, array('IDTBSH' => $id));
    }
    function getById($id)
    {
        // return $this->db->get_where('tbl_terima_barang_supplier_jahit', array('IDTBSH' => $id))->row();

        $this->db->select('tbl_terima_barang_supplier_jahit.*, tbl_surat_jalan_makloon_jahit.Nomor AS NomorSJH')
        ->from("tbl_terima_barang_supplier_jahit")
            ->join("tbl_surat_jalan_makloon_jahit", "tbl_surat_jalan_makloon_jahit.IDSJH = tbl_terima_barang_supplier_jahit.IDSJH")
            ->where("tbl_terima_barang_supplier_jahit.IDTBSH", $id);
        return $this->db->get()->row();
    }
    function detail_pb_jahit($id)
    {
         $this->db->select("tbl_terima_barang_supplier_jahit.*, tbl_terima_barang_supplier_jahit_detail.*, tbl_barang.Nama_Barang, tbl_merk.Merk, tbl_satuan.Satuan")
            ->from("tbl_terima_barang_supplier_jahit")
            ->join("tbl_terima_barang_supplier_jahit_detail", "tbl_terima_barang_supplier_jahit.IDTBSH=tbl_terima_barang_supplier_jahit_detail.IDTBSH")
            ->join("tbl_barang", "tbl_terima_barang_supplier_jahit_detail.IDBarang=tbl_barang.IDBarang")
            ->join("tbl_merk", "tbl_terima_barang_supplier_jahit_detail.IDMerk=tbl_merk.IDMerk")
            ->join("tbl_satuan", "tbl_terima_barang_supplier_jahit_detail.IDSatuan=tbl_satuan.IDSatuan")
            ->where("tbl_terima_barang_supplier_jahit_detail.IDTBSH", $id);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

 public function get_by_id_detail($id)
  {
    $this->db->select("*")
    ->from("tbl_terima_barang_supplier_jahit_detail")
    ->join("tbl_barang", "tbl_terima_barang_supplier_jahit_detail.IDBarang=tbl_barang.IDBarang")
    // ->join("tbl_warna", "tbl_terima_barang_supplier_jahit_detail.IDWarna=tbl_warna.IDWarna")
    ->join("tbl_merk", "tbl_barang.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_satuan", "tbl_barang.IDSatuan=tbl_satuan.IDSatuan")
    ->where("IDTBSH", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
    
    
  }

  public function get_barang()
    {
        return $this->db->get("tbl_barang")->result();
    }

    public function get_merk()
    {
        return $this->db->get("tbl_merk")->result();
    }

    public function get_satuan()
    {
        return $this->db->get("tbl_satuan")->result();
    }

    public function get_IDSJH($IDSJH)
    {
        return $this->db->get_where("tbl_surat_jalan_makloon_jahit", array('Nomor' => $IDSJH))->row();
    }
    public function get_IDBarang($IDBarang)
    {
        return $this->db->get_where("tbl_barang", array('Nama_Barang' => $IDBarang))->row();
    }
    public function get_IDMerk($IDMerk)
    {
        return $this->db->get_where("tbl_merk", array('Merk' => $IDMerk))->row();
    }
    public function get_IDSatuan($IDSatuan)
    {
        return $this->db->get_where("tbl_satuan", array('Satuan' => $IDSatuan))->row();
    }
    public function update_detail($id,$data){
        return $this->db->update('tbl_terima_barang_supplier_jahit_detail', $data, array('IDTBSHDetail' => $id));
    }
    function print_penerimaan_barang($nomor)
  {
    $this->db->select('*')->from("tbl_terima_barang_supplier_jahit")
            ->where("IDTBSH", $nomor);
     return $this->db->get()->row();
  }
  function print_penerimaan_barang_detail($id)
  {
     $this->db->select("tbl_terima_barang_supplier_jahit_detail.*, tbl_barang.Nama_Barang, tbl_merk.Merk, tbl_satuan.Satuan, tbl_terima_barang_supplier_jahit.Nomor")
    ->from("tbl_terima_barang_supplier_jahit_detail")
     ->join("tbl_terima_barang_supplier_jahit", "tbl_terima_barang_supplier_jahit_detail.IDTBSH=tbl_terima_barang_supplier_jahit.IDTBSH")
    ->join("tbl_barang", "tbl_terima_barang_supplier_jahit_detail.IDBarang=tbl_barang.IDBarang")
    ->join("tbl_merk", "tbl_terima_barang_supplier_jahit_detail.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_satuan", "tbl_terima_barang_supplier_jahit_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("tbl_terima_barang_supplier_jahit_detail.IDTBSH", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

    function ceksupplier($SJ)
    {
        $this->db->select('tbl_suplier."IDSupplier", tbl_suplier."Nama"')
        ->from("tbl_surat_jalan_makloon_jahit")
        ->join("tbl_suplier", "tbl_surat_jalan_makloon_jahit.IDSupplier=tbl_suplier.IDSupplier")
        ->where("tbl_surat_jalan_makloon_jahit.Nomor", $SJ);
        $query = $this->db->get();

        return $query->result();
    }
	
	function ceksurat($SuJa)
	{
		$this->db->select('tbl_surat_jalan_makloon_jahit_detail."Barcode", 
							tbl_corak."Corak",
							tbl_warna."Warna",
							tbl_merk."Merk",
							tbl_surat_jalan_makloon_jahit_detail."Qty_yard",
							tbl_surat_jalan_makloon_jahit_detail."Qty_meter",
							tbl_surat_jalan_makloon_jahit_detail."Grade",
							tbl_satuan."Satuan"
							')
        ->from("tbl_surat_jalan_makloon_jahit")
        ->join("tbl_surat_jalan_makloon_jahit_detail", "tbl_surat_jalan_makloon_jahit.IDSJH=tbl_surat_jalan_makloon_jahit_detail.IDSJH")
        ->join("tbl_corak","tbl_surat_jalan_makloon_jahit_detail.IDCorak=tbl_corak.IDCorak")
		->join("tbl_warna","tbl_surat_jalan_makloon_jahit_detail.IDWarna=tbl_warna.IDWarna")
		->join("tbl_merk","tbl_surat_jalan_makloon_jahit_detail.IDMerk=tbl_merk.IDMerk")
		->join("tbl_satuan","tbl_surat_jalan_makloon_jahit_detail.IDSatuan=tbl_satuan.IDSatuan")
		->where("tbl_surat_jalan_makloon_jahit.Nomor", $SuJa);
        $query = $this->db->get();
		return $query->result();
	}

    function cekbarang($nomor)
    {
        $this->db->select('tbl_merk."Merk", tbl_merk."IDMerk", tbl_satuan."IDSatuan", tbl_satuan."Satuan"')
        ->from("tbl_barang")
        ->join("tbl_merk", "tbl_merk.IDMerk=tbl_barang.IDMerk")
        ->join("tbl_satuan", "tbl_satuan.IDSatuan=tbl_barang.IDSatuan")
        ->where("tbl_barang.IDBarang", $nomor);
        $query = $this->db->get();

        return $query->result();
    }

    function tampilkan_id_jurnal()
{
    $this->db->select_max("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

  return $query->row();
}
function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}

 function all_jahit()
    {
        $query = ('SELECT * FROM laporan_makloon_jahit()');

        return $this->db->query($query)->result();
    }

   public function searching_store_umum($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_makloon_jahit(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like_umum($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_makloon_jahit_like(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }
}