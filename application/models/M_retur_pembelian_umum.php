<?php

class M_retur_pembelian_umum extends CI_Model
{

    function tampilkan_retur_pembelian()
    {
        $this->db->select("tbl_retur_pembelian_umum.*, tbl_suplier.Nama")
        ->from("tbl_retur_pembelian_umum")
        ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_retur_pembelian_umum.IDSupplier");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampil_supplier()
    {
     $this->db->select("*")
     ->from("tbl_suplier");
     $query = $this->db->get();

     if ($query->num_rows() > 0)
     {
        return $query->result();
    }
  }

    function tampil_inv()
    {
     $this->db->select("IDFBUmum, Nomor")
     ->from("tbl_pembelian_umum");
     $query = $this->db->get();

     if ($query->num_rows() > 0)
     {
        return $query->result();
    }
}

function cek_group_supplier($id)
{
   $this->db->select('tbl_group_supplier.Group_Supplier')->from("tbl_suplier")
   ->join("tbl_group_supplier", "tbl_group_supplier.IDGroupSupplier = tbl_suplier.IDGroupSupplier")
   ->where("tbl_suplier.IDSupplier", $id);
   return $this->db->get()->row();
}

function cek_group_barang($id)
{
   $this->db->select('tbl_group_barang.Group_Barang')->from("tbl_barang")
   ->join("tbl_group_barang", "tbl_group_barang.IDGroupBarang = tbl_barang.IDGroupBarang")
   ->where("tbl_barang.IDBarang", $id);
   return $this->db->get()->row();
}

function no_retur($tahun)
  {
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
                ->where('extract(year from "Tanggal") =', $tahun)
                ->from("tbl_retur_pembelian_umum")
                ->get()->row();
  }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->select("tbl_retur_pembelian_umum.*, tbl_suplier.Nama")
            ->from("tbl_retur_pembelian_umum")
            ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_retur_pembelian_umum.IDSupplier");


        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by('IDRBUmum', 'DESC')->get();
        return $query->result();
    }



function check_invoice($nomor)
{
        // $this->db->distinct();
    $this->db->select("*")
    ->from("tbl_pembelian_umum_detail")
    ->join("tbl_pembelian_umum", "tbl_pembelian_umum_detail.IDFBUmum=tbl_pembelian_umum.IDFBUmum")
    ->join("tbl_barang", "tbl_pembelian_umum_detail.IDBarang=tbl_barang.IDBarang")
    ->join("tbl_satuan", "tbl_pembelian_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("tbl_pembelian_umum.Nomor", $nomor);
    $query = $this->db->get();

    return $query->result();
}

function data_hutang($nomor)
{
    $this->db->select('tbl_pembelian_umum_grand_total.*')->from("tbl_pembelian_umum")
    ->join("tbl_pembelian_umum_grand_total", "tbl_pembelian_umum_grand_total.IDFBUmum=tbl_pembelian_umum.IDFBUmum")
    ->where("tbl_pembelian_umum.Nomor", $nomor);
    return $this->db->get()->row();
}

function check_grandtotal($IDFB)
{
    $this->db->select("*")
    ->from("tbl_pembelian_umum")
    ->join("tbl_pembelian_umum_grand_total", "tbl_pembelian_umum.IDFBUmum=tbl_pembelian_umum_grand_total.IDFBUmum")
    ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pembelian_umum.IDSupplier")
    ->where("tbl_pembelian_umum.Nomor", $IDFB);
    $query = $this->db->get();

    return $query->result();
}

function tampil_invoice_grand_total()
{
 $this->db->select("*")
 ->from("tbl_pembelian_umum_grand_total");
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
    return $query->result();
}
}

public function add($data){
   $this->db->insert('tbl_retur_pembelian_umum', $data);
   return TRUE;
}
function get_IDFB($Nomor)
{
    return $this->db->get_where("tbl_pembelian", array('Nomor' => $Nomor))->row();
}
public function add_detail($data){
    $this->db->insert('tbl_retur_pembelian_umum_detail', $data);
    return TRUE;
}
public function add_grand_total($data4){
    $this->db->insert('tbl_retur_pembelian_umum_grand_total', $data4);
    return TRUE;
}

function getById($id)
{
    $this->db->select('tbl_retur_pembelian_umum.*, tbl_pembelian_umum.Nomor AS nomorFB, tbl_suplier.IDSupplier, tbl_suplier.Nama')->from("tbl_retur_pembelian_umum")
    ->join("tbl_retur_pembelian_umum_grand_total", "tbl_retur_pembelian_umum.IDRBUmum = tbl_retur_pembelian_umum_grand_total.IDRBUmum")
    ->join("tbl_pembelian_umum", "tbl_pembelian_umum.IDFBUmum = tbl_retur_pembelian_umum.IDFBUmum")
     ->join("tbl_suplier", "tbl_retur_pembelian_umum.IDSupplier=tbl_suplier.IDSupplier")
    ->where("tbl_retur_pembelian_umum.IDRBUmum", $id);
    return $this->db->get()->row();
}

function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}

function tampilkan_id_jurnal()
{
    $this->db->select_max("IDJurnal")
    ->from("tbl_jurnal");
     $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_retur()
    {
        $this->db->select_max("IDRBUmum")
            ->from("tbl_retur_pembelian_umum");
        $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_retur_detail()
    {
        $this->db->select_max("IDRBUmumDetail")
            ->from("tbl_retur_pembelian_umum_detail");
        $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_retur_grand_total()
    {
        $this->db->select_max("IDRBUmumGrandTotal")
            ->from("tbl_retur_pembelian_umum_grand_total");
       $query = $this->db->get();

  return $query->row();
    }

function detail_retur_pembelian($id)
{
 $this->db->select("*")
 ->from("tbl_retur_pembelian_umum")
 ->join("tbl_retur_pembelian_umum_detail", "tbl_retur_pembelian_umum.IDRBUmum=tbl_retur_pembelian_umum_detail.IDRBUmum")
 ->join("tbl_barang", "tbl_retur_pembelian_umum_detail.IDBarang=tbl_barang.IDBarang")
 ->join("tbl_satuan", "tbl_retur_pembelian_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_retur_pembelian_umum_detail.IDRBUmum", $id);
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
    return $query->result();
}
}
public function update_master($id,$data){
    return $this->db->update('tbl_retur_pembelian_umum', $data, array('IDRBUmum' => $id));
}

public function update_detail($id,$data){
    return $this->db->update('tbl_retur_pembelian_umum_detail', $data, array('IDRBUmumDetail' => $id));
}

public function update_data_hutang($id,$data){
    return $this->db->update('tbl_hutang', $data, array('No_Faktur' => $id));
}

function update_status($data, $where)
    {
        return $this->db->update('tbl_retur_pembelian_umum', $data, $where);
    }

public function drop($id){
    $this->db->where('IDRBUmumDetail', $id);
    $this->db->delete('tbl_retur_pembelian_umum_detail');
    return TRUE;
}
function get_IDcorak($corak)
{
    return $this->db->get_where("tbl_corak", array('Corak' => $corak))->row();
}

function get_IDwarna($warna)
{
    return $this->db->get_where("tbl_warna", array('Warna' => $warna))->row();
}

function get_IDsatuan($satuan)
{
    return $this->db->get_where("tbl_satuan", array('Satuan' => $satuan))->row();
}

function get_IDbarang($corak)
{
    $this->db->select('IDBarang')->from("tbl_barang")
    ->join("tbl_corak", "tbl_barang.IDCorak = tbl_corak.IDCorak")
    ->where("tbl_corak.Corak", $corak);
    return $this->db->get()->row();
}
public function update_grand_total($id,$datas){
    return $this->db->update('tbl_retur_pembelian_grand_total', $datas, array('IDRB' => $id));
}

function find($id)
{
    return $this->db->select('tbl_retur_pembelian_umum.*, tbl_suplier.Nama')
    ->from("tbl_retur_pembelian_umum")
    ->from("tbl_suplier", "tbl_retur_pembelian_umum.IDSuplier=tbl_suplier.IDSupplier")
    ->where("tbl_retur_pembelian_umum". '.IDRBUmum', $id)
    ->get()->row();
}
function find_detail($id)
{
    $this->db->select("tbl_retur_pembelian_umum_detail.*, tbl_barang.Nama_Barang, tbl_satuan.Satuan")
   ->from("tbl_retur_pembelian_umum_detail")
   ->join("tbl_barang", "tbl_barang.IDBarang=tbl_retur_pembelian_umum_detail.IDBarang")
   ->join("tbl_satuan", "tbl_retur_pembelian_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
   ->where("tbl_retur_pembelian_umum_detail". '.IDRBUmum', $id);

   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}

  function print_retur_pembelian($nomor)
  {
    $this->db->select('*')->from("tbl_retur_pembelian_umum")
            ->where("IDRBUmum", $nomor);
     return $this->db->get()->row();
  }
  function print_retur_pembelian_detail($id)
  {
     $this->db->select("tbl_retur_pembelian_umum_detail.*, tbl_satuan.Satuan, tbl_barang.Nama_Barang, tbl_retur_pembelian_umum.Nomor")
    ->from("tbl_retur_pembelian_umum_detail")
     ->join("tbl_retur_pembelian_umum", "tbl_retur_pembelian_umum_detail.IDRBUmum=tbl_retur_pembelian_umum.IDRBUmum")
    ->join("tbl_barang", "tbl_barang.IDBarang=tbl_retur_pembelian_umum_detail.IDBarang")
    ->join("tbl_satuan", "tbl_retur_pembelian_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("tbl_retur_pembelian_umum_detail.IDRBUmum", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
}

 function print_grand_total($id)
  {
    $this->db->select('*')->from("tbl_retur_pembelian_umum_grand_total")
            ->where("IDRBUmum", $id);
     return $this->db->get()->row();
  }

  public function get_all_procedure_retur()
  {
    
        $query = ('
      SELECT * FROM laporan_retur_pembelian_umum()
    ');

  /*$query = ('select * from pengukuran');  */

  return $this->db->query($query)->result();
  }

    public function searching_store($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_retur_pembelian_umum(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_retur_pembelian_like_umum(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }

}
?>