<?php

class M_laporan_pembelian_asset extends CI_Model
{
    private $table = 'tbl_pembelian_asset';
    private $table2 = 'tbl_pembelian_asset_detail';

    function all()
    {
        /*$this->db->select("
        tbl_pembelian_asset.Nomor, 
        tbl_pembelian_asset.Tanggal, 
        tbl_asset.Kode_Asset,       
        tbl_asset.Nama_Asset,
        tbl_pembelian_asset_detail.Nilai_perolehan,
        tbl_pembelian_asset_detail.Akumulasi_penyusutan,
        tbl_pembelian_asset_detail.Nilai_buku,
        tbl_pembelian_asset_detail.Metode_penyusutan,
        tbl_pembelian_asset_detail.Tanggal_penyusutan,
        tbl_pembelian_asset_detail.Umur
        ")
            ->from("tbl_pembelian_asset")
            ->join("tbl_pembelian_asset_detail", "tbl_pembelian_asset.IDFBA=tbl_pembelian_asset_detail.IDFBA")
            ->join("tbl_asset", "tbl_asset.IDAsset=tbl_pembelian_asset_detail.IDAsset");
            //->group_by("tbl_pembelian_asset.Nomor, tbl_pembelian_asset.Tanggal, tbl_asset.Nama_Asset");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }*/
        $query = ('SELECT * FROM laporan_pembelian_asset()');

        return $this->db->query($query)->result();
    }

   public function searching_store($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_pembelian_asset(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_pembelian_asset_like(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }
}