<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* 
*/
class M_group_barang extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_group_barang()
	{
		$query=$this->db->query("SELECT * FROM tbl_group_barang");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampilkan_id_group_barang()
	{
		$this->db->select_max("IDGroupBarang")
		->from("tbl_group_barang");
		$query = $this->db->get();

		return $query->row();
	}
	/*hapus data*/
	function hapus_data_group_barang($id)
	{
		$this->db->where('IDGroupBarang', $id);
		$this->db->delete('tbl_group_barang');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function tambah_group_barang($data)
	{
		$this->db->insert('tbl_group_barang', $data);
		return TRUE;
	}

		function ambil_group_barang_byid($id)
	{
		return $this->db->get_where('tbl_group_barang', array('IDGroupBarang'=>$id))->row();
	}

	function aksi_edit_group_barang($id, $data)
	{
		$this->db->where('IDGroupBarang', $id);
		$this->db->update('tbl_group_barang', $data);
		return TRUE;
	}

function cari($kolom,$status,$keyword){
		$this->db->select("*")
    	->from("tbl_group_barang")
    	->like("Aktif", $status)
    	->like("$kolom", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
    	->from("tbl_group_barang")
    	->like("$kolom", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_status($status){
		$this->db->select("*")
    	->from("tbl_group_barang")
    	->where("Aktif", $status);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
			$this->db->select("*")
    	->from("tbl_group_barang")
    	->like("Kode_Group_Barang", $keyword)
    	->or_like("Group_Barang", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>