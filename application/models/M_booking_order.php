<?php

class M_booking_order extends CI_Model
{
    public $table1 = 'tbl_booking_order';
    public $table2 = 'tbl_corak';

    function all()
    {
        $this->db->select('*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDCorak = ' . $this->table2 . '.IDCorak');

        $query = $this->db->order_by('IDBO', 'DESC')->get();
        return $query->result();
    }

    function getCodeBooking($tahun)
    {
       return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
                ->where('extract(year from "Tanggal") =', $tahun)
                ->from("tbl_booking_order")
                ->get()->row();
    }

    function tampilkan_id_booking_order()
    {
        $this->db->select("IDBO")
            ->from("tbl_booking_order");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function save($data)
    {
        $this->db->insert($this->table1, $data);
        return TRUE;
    }

    function find($id)
    {
        return $this->db->select('*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDCorak = ' . $this->table2 . '.IDCorak')
            ->where($this->table1 . '.IDBO', $id)
            ->get()->row();
    }

    function soft_delete($id)
    {
        return $this->db->update($this->table1, array('Batal' => 'Tidak Aktif'), array('IDBO' => $id));
    }

    function soft_success($id)
    {
        return $this->db->update($this->table1, array('Batal' => 'Aktif'), array('IDBO' => $id));
    }

    function update($data, $id)
    {
        return $this->db->update($this->table1, $data, array('IDBO' => $id));
    }

    function bulk($where)
    {
        $this->db->where_in('Nomor', $where);
        return $this->db->update($this->table1, array('Batal' => 'Tidak Aktif'));
    }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->select('*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDCorak = ' . $this->table2 . '.IDCorak');

        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal_selesai <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by('IDBO', 'DESC')->get();
        return $query->result();
    }

    function getBO($bo)
    {
        return $this->db->select('*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDCorak = ' . $this->table2 . '.IDCorak')
            ->where($this->table1 . '.Nomor', $bo)
            ->get()->row();
    }
}