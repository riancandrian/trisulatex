<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 */
class M_user extends CI_Model
{
    function tampilkan_user()
    {
        $this->db->select("US.*, GU.Group_User")
            ->from("tbl_user US")
            ->join("tbl_group_user GU", "US.IDGroupUser = GU.IDGroupUser");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function simpan($data)
    {
        $this->db->insert('tbl_user', $data);
        return TRUE;
    }

    function group_user()
    {
        return $this->db->get('tbl_group_user')->result();
    }

    function getById($id)
    {
        return $this->db->get_where('tbl_user', array('IDUser' => $id))->row();
    }

    function update($data, $where)
    {
        return $this->db->update('tbl_user', $data, $where);
    }

    function details_menu()
    {
        return $this->db->get('tbl_menu_detail')->result();
    }

    function menu()
    {
        return $this->db->get('tbl_menu')->result();
    }

    function save_batch($detail)
    {
        return $this->db->insert_batch('tbl_menu_role', $detail);
    }

    function getAllRelations($id)
    {
        return $this->db->select('TU.*,TM.*')
                        ->from('tbl_user TU')
                        ->join('tbl_group_user TG', 'TU.IDGroupUser = TG.IDGroupUser')
                        ->join('tbl_menu_role TM', 'TG.IDGroupUser = TM.IDGroupUser')
                        ->where('TU.IDUser', $id)->get()->row();
    }

    function delete($id)
    {
        $this->db->where('IDUser', $id);
        $this->db->delete('tbl_user');
    }

    function getRoleById($id)
    {
        $data = $this->db->select('IDMenuDetail')->from('tbl_menu_role')->where('IDGroupUser', $id)->get()->result();

        foreach ($data as $row)
        {
            $roles[] = $row->IDMenuDetail;
        }

        return $roles;
    }

    function delete_batch($id)
    {
        $this->db->where('IDGroupUser', $id);
        $this->db->delete('tbl_menu_role');
    }

    function cari($kolom,$status,$keyword){
        $this->db->select("u.*, gu.Group_User")
    ->from("tbl_user u")
    ->join("tbl_group_user gu", "u.IDGroupUser=gu.IDGroupUser")
    ->like("u.Aktif", $status)
    ->like("$kolom", $keyword);
    $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_kolom($kolom,$keyword){
         $this->db->select("u.*, gu.Group_User")
    ->from("tbl_user u")
    ->join("tbl_group_user gu", "u.IDGroupUser=gu.IDGroupUser")
    ->like("$kolom", $keyword);
    $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_status($status){
         $this->db->select("u.*, gu.Group_User")
    ->from("tbl_user u")
    ->join("tbl_group_user gu", "u.IDGroupUser=gu.IDGroupUser")
    ->where("u.Aktif", $status);
    $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_keyword($keyword){
        $this->db->select("u.*, gu.Group_User")
    ->from("tbl_user u")
    ->join("tbl_group_user gu", "u.IDGroupUser=gu.IDGroupUser")
    ->like("u.Nama", $keyword)
     ->or_like("u.Username", $keyword)
     ->or_like("u.Email", $keyword);
    $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
}