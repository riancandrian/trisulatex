<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* 
*/
class M_kota extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_kota()
	{
		$query=$this->db->query("SELECT * FROM tbl_kota");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/*hapus data*/
	function hapus_data_kota($id)
	{
		$this->db->where('IDKota', $id);
		$this->db->delete('tbl_kota');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function tambah_kota($data)
	{
		$this->db->insert("tbl_kota", $data);
		return TRUE;
	}

		function ambil_kota_byid($id)
	{
		return $this->db->get_where('tbl_kota', array('IDKota'=>$id))->row();
	}

	function aksi_edit_kota($id, $data)
	{
		$this->db->where('IDKota', $id);
		$this->db->update('tbl_kota', $data);
		return TRUE;
	}

	function tampilkan_id_kota()
{
  $this->db->select_max("IDKota")
  ->from("tbl_kota");
  $query = $this->db->get();

  return $query->row();
}

function cari($kolom,$status,$keyword){
		$this->db->select("*")
		->from("tbl_kota")
		->like("Aktif", $status)
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_kota")
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_status($status){
		$this->db->select("*")
		->from("tbl_kota")
		->where("Aktif", $status);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_kota")
		->like("Kode_Kota", $keyword)
		->or_like("Provinsi", $keyword)
		->or_like("Kota", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}


}
?>