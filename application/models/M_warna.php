<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_warna extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_warna()
	{
		$query=$this->db->query('SELECT * FROM tbl_warna join tbl_corak on tbl_corak."IDCorak"=tbl_warna."IDCorak"');
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function tampilkan_id_warna()
	{
		$this->db->select_max("IDWarna")
		->from("tbl_warna");
		$query = $this->db->get();

		return $query->row();
	}

	/*hapus data*/
	function hapus_data_warna($id)
	{
		$this->db->where('IDWarna', $id);
		$this->db->delete('tbl_warna');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_warna', $data);
		return TRUE;
	}

	function get_warna_byid($id)
	{
		return $this->db->get_where('tbl_warna', array('IDWarna'=>$id))->row();
	}

	function update_warna($id, $data)
	{
		$this->db->where('IDWarna', $id);
		$this->db->update('tbl_warna', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_warna")
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_warna")
		->like("Kode_Warna", $keyword)
		->or_like("Warna", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>