<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_login extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	public function cek_user($user, $pass) {
		$this->db->where("(Username='$user')");
		$this->db->where('Password', md5($pass));
		$query = $this->db->get('tbl_user');
		if($query->num_rows() == 1){
			return $query->result();
		}else{
			return false;
		}
	}

	// public function cek_akses(){
	// 	$query=$this->db->query("SELECT * FROM hak_akses where id_user='$_SESSION[id]'");
	// 	if($query->num_rows()>0)
	// 	{
	// 		return $query->result();
	// 	}
	// }

	public function aksesmenu(){

		 $this->db->select("tbl_menu.Menu, tbl_menu.IDMenu, tbl_menu.Urutan_menu");
            $this->db->from("tbl_user");
            $this->db->join("tbl_group_user", "tbl_user.IDGroupUser = tbl_group_user.IDGroupUser");
            $this->db->join("tbl_menu_role", "tbl_group_user.IDGroupUser = tbl_menu_role.IDGroupUser");
            $this->db->join("tbl_menu_detail", "tbl_menu_role.IDMenuDetail = tbl_menu_detail.IDMenuDetail");
             $this->db->join("tbl_menu", "tbl_menu_detail.IDMenu = tbl_menu.IDMenu");
             $this->db->where("tbl_user.IDUser", $_SESSION['id']);
             $this->db->where_not_in("tbl_menu.Menu", 'Setting');
             $this->db->group_by("tbl_menu.IDMenu, tbl_menu.Menu, tbl_menu.Urutan_menu");
              $this->db->order_by("tbl_menu.Urutan_menu", "ASC");
        $query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	public function aksesmenudetail(){
	 $this->db->select("tbl_menu_detail.Menu_Detail, tbl_menu_detail.IDMenuDetail, tbl_menu.IDMenu, tbl_menu_detail.Url, tbl_menu_detail.Urutan");
            $this->db->from("tbl_user");
            $this->db->join("tbl_group_user", "tbl_user.IDGroupUser = tbl_group_user.IDGroupUser");
            $this->db->join("tbl_menu_role", "tbl_group_user.IDGroupUser = tbl_menu_role.IDGroupUser");
            $this->db->join("tbl_menu_detail", "tbl_menu_role.IDMenuDetail = tbl_menu_detail.IDMenuDetail");
             $this->db->join("tbl_menu", "tbl_menu_detail.IDMenu = tbl_menu.IDMenu");
             $this->db->where("tbl_user.IDUser", $_SESSION['id']);
             $menudetail = array('Agen', 'User', 'Group User', 'Menu', 'Menu Detail', 'Laporan Pembelian');
             $this->db->where_not_in("tbl_menu_detail.Menu_Detail", $menudetail);
              $this->db->order_by("tbl_menu_detail.Urutan", "ASC");
            $this->db->group_by("tbl_menu_detail.Menu_Detail, tbl_menu_detail.IDMenuDetail, tbl_menu.IDMenu, tbl_menu_detail.Url, tbl_menu_detail.Urutan"); //warning
              $query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	public function aksessetting(){

		 $this->db->select("tbl_menu_detail.Menu_Detail, tbl_menu_detail.IDMenuDetail, tbl_menu.IDMenu, tbl_menu_detail.Url");
            $this->db->from("tbl_user");
            $this->db->join("tbl_group_user", "tbl_user.IDGroupUser = tbl_group_user.IDGroupUser");
            $this->db->join("tbl_menu_role", "tbl_group_user.IDGroupUser = tbl_menu_role.IDGroupUser");
            $this->db->join("tbl_menu_detail", "tbl_menu_role.IDMenuDetail = tbl_menu_detail.IDMenuDetail");
             $this->db->join("tbl_menu", "tbl_menu_detail.IDMenu = tbl_menu.IDMenu");
             $this->db->where("tbl_user.IDUser", $_SESSION['id']);
             $this->db->where("tbl_menu.Menu", 'Setting');
              $this->db->group_by("tbl_menu_detail.Menu_Detail, tbl_menu_detail.IDMenuDetail, tbl_menu.IDMenu, tbl_menu_detail.Url");
        $query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}



}
?>