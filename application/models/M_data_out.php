<?php

class M_data_out extends CI_Model
{
    private $table = 'tbl_in';
    private $table2 = 'tbl_out';
    private $table3 = 'tbl_kartu_stok';
    private $table4 = 'tbl_stok';

    function getItem_in($barcode)
    {
        return $this->db->get_where($this->table, array('Barcode' => $barcode))->row();
    }

    function save($data)
    {
        $this->db->insert($this->table2, $data);
        return $this->db->insert_id();
    }

    function updateStock($where, $yard, $meter)
    {
        $q = $this->db->get_where($this->table4, $where)->row();

        $set = array(
            'Panjang_Yard' => ($q->Panjang_Yard - $q->Panjang_Yard),
            'Panjang_Meter' => ($q->Panjang_Meter - $q->Panjang_Meter),
            'Saldo_Yard' => ($q->Saldo_Yard - $q->Saldo_Yard),
            'Saldo_Meter' => ($q->Saldo_Meter - $q->Saldo_Meter)
        );
        return $this->db->update($this->table4, $set, $where);
    }

    function saveStockCard($dataCard)
    {
        $this->db->insert($this->table3, $dataCard);
        return $this->db->insert_id();
    }
}