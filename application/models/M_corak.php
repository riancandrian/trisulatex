<?php

/**
 *
 */
class M_corak extends CI_Model
{
    function tampilkan_corak()
    {
        $this->db->select("TC.*")
            ->from("tbl_corak TC");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_corak()
{
  $this->db->select_max("IDCorak")
  ->from("tbl_corak");
  $query = $this->db->get();

  return $query->row();
}

    function get_bo($id)
    {
        return $this->db->select("IDCorak")
      ->from("tbl_booking_order")
      ->where("IDCorak", $id)
      ->get()->row();
    }

    function get_barang($id)
    {
        return $this->db->select("IDCorak")
      ->from("tbl_barang")
      ->where("IDCorak", $id)
      ->get()->row();
    }

    function all()
    {
        return $this->db->get('tbl_corak')->result();
    }

    function getById($id)
    {
        return $this->db->get_where('tbl_corak', array('IDCorak' => $id))->row();
    }

     function tampil_po_corak()
    {
        $this->db->select("*")
            ->from("tbl_barang")
            ->join("tbl_corak", "tbl_barang.IDCorak=tbl_corak.IDCorak");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function getWarna()
    {
        return $this->db->get('tbl_warna')->result();
    }

    function getMerk()
    {
        return $this->db->get('tbl_merk')->result();
    }

    function simpan($data)
    {
        $this->db->insert('tbl_corak', $data);
        return TRUE;
    }

    function update($data, $where)
    {
        return $this->db->update('tbl_corak', $data, $where);
    }

    function delete($id)
    {
        $this->db->where('IDCorak', $id);
        $this->db->delete('tbl_corak');
    }

     function cari($kolom,$status,$keyword){
         $this->db->select("*")
        ->from("tbl_corak c")
        ->like("c.Aktif", $status)
        ->like("$kolom", $keyword);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_kolom($kolom,$keyword){
        $this->db->select("*")
        ->from("tbl_corak c")
        ->like("$kolom", $keyword);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_status($status){
        $this->db->select("*")
        ->from("tbl_corak c")
        ->where("c.Aktif", $status);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_keyword($keyword){
        $this->db->select("*")
        ->from("tbl_corak c")
        ->like("c.Kode_Corak", $keyword)
        ->or_like("c.Corak", $keyword);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
}