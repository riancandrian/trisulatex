<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class M_input extends CI_Model {

  public function getimport($barcode)
  {
    $this->db->where('Barcode', $barcode);
    return $this->db->get('tbl_import')->result_array();
  }
  
  public function insert_data($data, $data2, $data3)
  {
    $this->db->insert('tbl_in', $data);
    $this->db->insert('tbl_kartu_stok', $data2);
    $this->db->insert('tbl_stok', $data3);
    return TRUE;
  }
}

/* End of file M_input.php */
