<!-- Programmer : Rais Naufal Hawari
     Date       : 16-07-2018 -->

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_saldoawalasset extends CI_Model {

  // Set nama table sebagai const
  private $table = 'tbl_saldo_awal_asset';
  private $order = 'DESC';
  private $id = 'IDSaldoAwalAsset';
  // Fungsi Select semua data giro
  public function get_all()
  {
    $this->db->join('tbl_asset', 'tbl_asset.IDAsset = tbl_saldo_awal_asset.IDAsset', 'left');
    $this->db->order_by($this->id, $this->order);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

   function tampilkan_id_saldo_awal_asset()
    {
        $this->db->select_max("IDSaldoAwalAsset")
            ->from("tbl_saldo_awal_asset");
       $query = $this->db->get();

  return $query->row();
    }

  public function get_all_asset()
  {
    $this->db->order_by('IDAsset', $this->order);
    $data = $this->db->get('tbl_asset');

    $hitung = $this->db->count_all_results('tbl_asset');
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  //Get Data by ID
  public function get_by_id($id)
  {
    
    $this->db->where($this->id, $id);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->row();
    } 
    
    
  }

  function get_IDAsset($assetid)
    {
        return $this->db->get_where("tbl_asset", array('IDAsset' => $assetid))->row();
    }

  //Fungsi Tambah Data
  public function insert($data)
  {
    $this->db->insert($this->table, $data);
  }

  //Fungsi Ubah Data
  public function update($id, $data)
  {
    $this->db->where($this->id, $id);
    $this->db->update($this->table, $data);
  }

  //Fungsi Hapus Data
  public function delete($id)
  {
    $this->db->where($this->id, $id);
    $this->db->delete($this->table);
  }

  function cari_by_kolom($kolom,$keyword){
    $this->db->select("sas.*, a.Nama_Asset")
    ->from("tbl_saldo_awal_asset sas")
    ->join("tbl_asset a", "sas.IDAsset = a.IDAsset")
    ->like("$kolom", $keyword);
    $query=$this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function cari_by_keyword($keyword){
   $this->db->select("sas.*, a.Nama_Asset")
    ->from("tbl_saldo_awal_asset sas")
    ->join("tbl_asset a", "sas.IDAsset = a.IDAsset")
    ->like("a.Nama_Asset", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }



}

/* End of file M_saldoawalaset.php */
