<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

/**
* 
*/
class M_so extends CI_Model
{
	
  private $table = 'tbl_sales_order_kain';
  private $table_detail = 'tbl_sales_order_kain_detail';
  private $table_customer = 'tbl_customer';
  private $table_barang = 'tbl_barang';
  private $table_merk = 'tbl_merk';
  private $table_satuan = 'tbl_satuan';
  private $table_corak = 'tbl_corak';
  private $table_warna = 'tbl_warna';
  private $table_mata_uang = 'tbl_mata_uang';
  private $order = 'DESC';
  private $id = 'IDSOK';
  private $id_detail = 'IDSOKDetail';
  private $table_harga_jual = 'tbl_harga_jual_barang';  

  // Fungsi Select semua data Menu
  public function get_all()
  {
    
    $this->db->order_by($this->id, $this->order);

    $this->db->join($this->table_customer, $this->table_customer.'.IDCustomer ='. $this->table.'.IDCustomer');
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
   } 
 }

 function tampilkan_get_sj($id)
 {
   return $this->db->select("IDSOK")
   ->from("tbl_packing_list")
   ->where("IDSOK", $id)
   ->get()->row();
 }

 function tampilkan_id_so()
 {
   $this->db->select_max("IDSOK")
   ->from("tbl_sales_order_kain");
   $query = $this->db->get();

   return $query->row();
 }

 function tampilkan_id_so_detail()
 {
  $this->db->select_max("IDSOKDetail")
   ->from("tbl_sales_order_kain_detail");
   $query = $this->db->get();

   return $query->row();
}

  // ---------------------------Fungsi get all procedure
public function get_all_procedure()
{
  
  $query = ('
    SELECT * FROM laporan_sales_order_kain()
    ');

  /*$query = ('select * from pengukuran');	*/

  return $this->db->query($query)->result();
}

  //--------------Get Data by ID
public function get_by_id($id)
{
  $this->db->join($this->table_customer, $this->table_customer.'.IDCustomer ='. $this->table.'.IDCustomer');
  $this->db->join($this->table_mata_uang, $this->table_mata_uang.'.IDMataUang ='. $this->table.'.IDMataUang');
  $this->db->where('IDSOK', $id);
  $data = $this->db->get($this->table);

  $hitung = $this->db->count_all_results($this->table);
  if ( $hitung > 0) {
   return $data->row();
 } 
 
 
}

public function get_so_detail($id)
{
  $this->db->join($this->table_barang, $this->table_barang.'.IDBarang ='. $this->table_detail.'.IDBarang');
  $this->db->join($this->table_corak, $this->table_corak.'.IDCorak ='. $this->table_detail.'.IDCorak');
  $this->db->join($this->table_merk, $this->table_merk.'.IDMerk ='. $this->table_detail.'.IDMerk');
  $this->db->join($this->table_warna, $this->table_warna.'.IDWarna ='. $this->table_detail.'.IDWarna');
  $this->db->join($this->table_satuan, $this->table_satuan.'.IDSatuan ='. $this->table_detail.'.IDSatuan');
  $this->db->where('IDSOK', $id);
  $data = $this->db->get($this->table_detail);
  $hitung = $this->db->count_all_results($this->table);
  if ( $hitung > 0) {
   return $data->result();
 } 
 
 
}

  //Fungsi Tambah Data
public function insert($data)
{
  $this->db->insert($this->table, $data);
}

  //Fungsi Ubah Data
public function update($id, $data)
{
  $this->db->where($this->id, $id);
  $this->db->update($this->table, $data);
}

  //Fungsi Hapus Data
public function delete($id)
{
  $this->db->where($this->id, $id);
  $this->db->delete($this->table);
}

function cari($kolom,$status,$keyword){
  $this->db->select("*")
  ->from("tbl_merk")
  ->like("Aktif", $status)
  ->like("$kolom", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function cari_by_kolom($kolom,$keyword){
 $this->db->select("*")
 ->from("tbl_merk")
 ->like("$kolom", $keyword);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_status($status){
 $this->db->select("*")
 ->from("tbl_merk")
 ->where("Aktif", $status);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_keyword($keyword){
  $this->db->select("*")
  ->from("tbl_merk")
  ->like("Kode_Merk", $keyword)
  ->or_like("Merk", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function get_supplier(){
  $this->db->select("IDSupplier, Nama")
  ->from("tbl_suplier");
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

  //-------------------NO Purchase Order
function no_so($tahun)
{
  return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
  ->where('extract(year from "Tanggal") =', $tahun)
  ->from($this->table)
  ->get()->row();
}

  //-------------------Get Trisula
function get_customer()
{
  $data = $this->db->get($this->table_customer);

  $hitung = $this->db->count_all_results($this->table_customer);
  if ( $hitung > 0) {
   return $data->result();
 } 
}

  //---------------------GetMerk
function getmerk($id)
{
  $this->db->from('tbl_corak');
  
  $this->db->join('tbl_barang', 'tbl_barang.IDCorak = tbl_corak.IDCorak');
  $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk');
  $this->db->where('tbl_corak.IDCorak', $id);
    //$this->db->join('tbl_barang', 'tbl_barang.IDMerk = tbl_corak.IDMerk');
  
  $data = $this->db->get();

  $hitung = $this->db->count_all_results('tbl_corak');
  if ( $hitung > 0) {
   return $data->row();
 } 
}

   //---------------------Getwarna
function getwarna($id)
{
 $this->db->where('Kode_Corak', $id);
 $this->db->join('tbl_corak', 'tbl_warna.IDCorak = tbl_corak.IDCorak', 'left');
 $data = $this->db->get('tbl_warna');
 $hitung = $this->db->count_all_results('tbl_warna');
 if ( $hitung > 0) {
  return $data->result();
} 
}


   //Insert data Atas
public function add($data){
    //$this->db->insert($this->table, $data);
  $this->db->insert($this->table, $data);
  return TRUE;
}

  //Insert Detail nya
public function add_detail($data){
  $this->db->insert($this->table_detail, $data);
  return TRUE;
} 

  //Ubah Data Master
public function update_master($id,$data){
  return $this->db->update($this->table, $data, array($this->id => $id));
}

public function drop($id){
  $this->db->where($this->id_detail, $id);
  $this->db->delete($this->table_detail);
  return TRUE;
}

function searching($date_from, $date_until, $search_type, $keyword)
{
  
  
  $this->db->join($this->table_customer, $this->table_customer.'.IDCustomer ='. $this->table.'.IDCustomer');

  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
  }

  if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
  }

  if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
  }

  $query = $this->db->order_by($this->id, 'DESC')->get($this->table);
  return $query->result();
}

  //--------------------------Searching Store Procedure
public function searching_store($date_from, $date_until, $keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_laporan_sales_order_kain(?, ?, ?)
    ');

  /*$query = ('select * from pengukuran');	*/

  return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
}

  //--------------------------Searching Store Procedure
public function searching_store_like($keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_laporan_sales_order_kain_like(?)
    ');

  /*$query = ('select * from pengukuran');	*/

  return $this->db->query($query, array($new_keywords))->result();
}

function print_so($nomor)
{
  $this->db->select('tbl_purchase_order.*, tbl_corak.Corak, tbl_merk.Merk, tbl_agen.Nama_Perusahaan')->from("tbl_purchase_order")
  ->join("tbl_corak", "tbl_purchase_order.IDCorak = tbl_corak.IDCorak")
  ->join("tbl_merk", "tbl_purchase_order.IDMerk = tbl_merk.IDMerk")
  ->join("tbl_agen", "tbl_purchase_order.IDAgen = tbl_agen.IDAgen")
  ->where("tbl_purchase_order.IDPO", $nomor);
  return $this->db->get()->row();
}
function print_so_detail($id)
{
 $this->db->select("*")
 ->from("tbl_purchase_order_detail")
 ->where("IDPO", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}
function last_Id()
{
  $this->db->select('IDFBA');
  $this->db->from('tbl_pembelian_asset');
  $this->db->order_by('IDFBA','DESC');
  $this->db->limit(1);
  $query = $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->row();
  }else{
    return "null";
  }
}


  //---------------------get Satuan
function get_satuan()
{
    //  $where = array(
    //    'Satuan' => 'yard' ,
    //    'Satuan' => 'meter' 
    // );
  
    // $this->db->where('Satuan', 'yard' );
    //$this->db->or_where('Satuan', 'meter');
  $data = $this->db->get($this->table_satuan);
  $hitung = $this->db->count_all_results($this->table_satuan);
  if ( $hitung > 0) {
    return $data->result();
  } 
}

public function get_harga($id, $ids)
{
 $where = array(
   $this->table_harga_jual.'.IDSatuan' => $ids ,
   $this->table_harga_jual.'.IDBarang' => $id 
 );
 $this->db->from($this->table_harga_jual);
 $this->db->join($this->table_barang, $this->table_barang.'.IDBarang ='. $this->table_harga_jual.'.IDBarang');
     // $this->db->join($this->table_corak, $this->table_corak.'.IDCorak ='. $this->table_harga_jual.'.IDCorak');
     // $this->db->join($this->table_merk, $this->table_merk.'.IDMerk ='. $this->table_harga_jual.'.IDMerk');
 $this->db->where($where);
 
 $data = $this->db->get();
 $hitung = $this->db->count_all_results($this->table_harga_jual);
 if ( $hitung > 0) {
  return $data->row();
} 
}
}
?>