<!-- Programmer : Rais Naufal Hawari
     Date       : 14-07-2018 -->

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_satuan extends CI_Model {

  // Set nama table sebagai const
  private $table = 'tbl_satuan';
  private $order = 'DESC';
  private $id = 'IDSatuan';

  // Fungsi Select semua data Menu
  public function get_all()
  {
    
    // $this->db->select('IDSatuan, Menu_Detail, Menu');
    // $this->db->join('tbl_menu', 'tbl_menu.IDMenu = tbl_satuan.IDMenu', 'left');
    $this->db->order_by($this->id, $this->order);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  public function get_yard_meter()
  {
    $this->db->select("*");
            $this->db->from("tbl_satuan");
             $satuandetail = array('Yard', 'Meter');
             $this->db->where_in("Satuan", $satuandetail);
              $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function tampilkan_id_satuan()
    {
        $this->db->select_max("IDSatuan")
            ->from("tbl_satuan");
       $query = $this->db->get();

  return $query->row();
    }

  //Get Data by ID
  public function get_by_id($id)
  {
    $this->db->where($this->id, $id);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->row();
    } 
    
    
  }

  function get_barang($id)
    {
        return $this->db->select("IDSatuan")
      ->from("tbl_barang")
      ->where("IDSatuan", $id)
      ->get()->row();
    }

  //Fungsi Tambah Data
  public function insert($data)
  {
    $this->db->insert($this->table, $data);
  }

  //Fungsi Ubah Data
  public function update($id, $data)
  {
    $this->db->where($this->id, $id);
    $this->db->update($this->table, $data);
  }

  //Fungsi Hapus Data
  public function delete($id)
  {
    $this->db->where($this->id, $id);
    $this->db->delete($this->table);
  }

  function cari($kolom,$status,$keyword){
    $this->db->select("*")
    ->from("tbl_satuan")
    ->like("Aktif", $status)
    ->like("$kolom", $keyword);
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function cari_by_kolom($kolom,$keyword){
    $this->db->select("*")
    ->from("tbl_satuan")
    ->like("$kolom", $keyword);
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function cari_by_status($status){
     $this->db->select("*")
    ->from("tbl_satuan")
    ->where("Aktif", $status);
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function cari_by_keyword($keyword){
    $this->db->select("*")
    ->from("tbl_satuan")
    ->like("Kode_Satuan", $keyword)
    ->or_like("Satuan", $keyword);
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }



}

/* End of file M_satuan.php */
