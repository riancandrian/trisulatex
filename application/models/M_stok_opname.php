<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_stok_opname extends CI_Model
{
	private $table1 = 'tbl_stok';
    private $table3 = 'tbl_corak';
    private $table8 = 'tbl_merk';
    private $table9 = 'tbl_warna';
    private $table10 = 'tbl_satuan';
    private $table12 = 'tbl_barang';
    private $table13 = 'tbl_gudang';

    function __construct()
    {
      parent::__construct();
  }
  /*menmapilkan table*/
  function tampilkan_stokopname()
  {
     $query = ('SELECT * FROM laporan_stokopname()');

     return $this->db->query($query)->result();
 }


 public function getimport($barcode)
 {
    $this->db->select(
        $this->table1 . '.*,' .
        $this->table3 . '.IDCorak,' .
        $this->table3 . '.Corak,' .
        $this->table9 . '.IDWarna,' .
        $this->table9 . '.Warna,' .
        $this->table10 . '.IDSatuan,' .
        $this->table10 . '.Satuan,' .
        $this->table12 . '.IDBarang,' .
        $this->table12 . '.Nama_Barang'
    );
    $this->db->from($this->table1);
    $this->db->join($this->table3, $this->table1 . '.IDCorak = ' . $this->table3 . '.IDCorak');
    $this->db->join($this->table9, $this->table1 . '.IDWarna = ' . $this->table9 . '.IDWarna');
    $this->db->join($this->table10, $this->table1 . '.IDSatuan = ' . $this->table10 . '.IDSatuan');
    $this->db->join($this->table12, $this->table1 . '.IDBarang = ' . $this->table12 . '.IDBarang');
        //$this->db->join($this->table13, $this->table1 . '.IDGudang = ' . $this->table13 . '.IDGudang');
    $this->db->where($this->table1 . '.Barcode', $barcode);

    return $this->db->get()->result_array();
}

function tampilkan_id_stokopname()
{
    $this->db->select("IDStokOpname")
   ->from("tbl_stok_opname");
   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}


public function cek_ketersediaan($data, $nama_table)
{
    $this->db->where($data);
    $data = $this->db->get($nama_table);
    $hitung = $this->db->count_all_results($nama_table);
    if ($hitung > 0) {
        return $data->row();
    }

}


function getDataBarcode($id)
{
    return $this->db->get_where('tbl_in', array('IDIn'=>$id))->row();
}

function get_IDmerk($merk)
{
    return $this->db->select('IDMerk')->from($this->table8)->where('LOWER("Merk") = '."LOWER('$merk')")->get()->row();
}

function get_IDcorak($corak)
{
    return $this->db->get_where($this->table3, array('IDCorak' => $corak))->row();
        //return $this->db->select('*')->from($this->table3)->where('LOWER("Corak") = '."LOWER('$corak')")->get()->row();
}

function get_IDwarna($warna)
{
        //return $this->db->get_where($this->table9, array('Warna' => $warna))->row();
    return $this->db->select('*')->from($this->table9)->where('LOWER("Warna") = '."LOWER('$warna')")->get()->row();
}

function get_IDsatuan($satuan)
{
        //return $this->db->get_where($this->table10, array('Satuan' => $satuan))->row();
    return $this->db->select('*')->from($this->table10)->where('LOWER("Satuan") = '."LOWER('$satuan')")->get()->row();
}

function get_IDbarang($corak)
{
    $this->db->select('IDBarang')->from($this->table12)
    ->join($this->table3, $this->table12 . '.IDCorak = ' . $this->table3 . '.IDCorak')
    ->where('LOWER("Corak") = '."LOWER('$corak')");
            //->where($this->table3 . ".Corak", $corak);
    return $this->db->get()->row();
}

function get_IDgudang()
{
    $this->db->select('IDGudang')->from($this->table13)
    ->where("(Nama_Gudang = 'PT. TRISULA' OR Nama_Gudang = 'PT. Trisula')");
    return $this->db->get()->row();
}

function save($data)
{
    return $this->db->insert('tbl_stok_opname', $data);
}

public function searching_store($date_from, $date_until, $keyword)
{

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_stokopname(?, ?, ?)
      ');

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
}

  //--------------------------Searching Store Procedure
public function searching_store_like($keyword)
{

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_stokopname_like(?)
      ');

    return $this->db->query($query, array($new_keywords))->result();
}


}
?>