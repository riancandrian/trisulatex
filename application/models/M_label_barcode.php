<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_label_barcode extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

   public function cek_ketersediaan_data($data, $nama_table)
  {
    $this->db->where($data);
    $data = $this->db->get($nama_table);
    $hitung = $this->db->count_all_results($nama_table);
    if ($hitung > 0) {
     return $data->row();
 }
    }

    function simpan($data)
    {
        $this->db->insert('tbl_barcode_print', $data);
        return $this->db->insert_id();
    }

    function getById($ambil)
    {
        return $this->db->get_where('tbl_barcode_print', array('IDBarcodePrint'=>$ambil))->row();
    }

    function getDataBarcode($id)
    {
        return $this->db->get_where('tbl_barcode_print', array('IDBarcodePrint'=>$id))->row();
    }

    function tampil_label_barcode()
    {
    	$this->db->select("*")
            ->from("tbl_barcode_print");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function cari_by_kolom($kolom,$keyword){
        $this->db->select("*")
        ->from("tbl_barcode_print")
        ->like("$kolom", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
function cari_by_keyword($keyword){
         $this->db->select("*")
        ->from("tbl_barcode_print")
        ->like("Barcode", $keyword)
        ->or_like("NoSO", $keyword)
        ->or_like("Party", $keyword)
        ->or_like("Indent", $keyword)
        ->or_like("CustDes", $keyword)
        ->or_like("WarnaCust", $keyword)
        ->or_like("Grade", $keyword)
        ->or_like("Remark", $keyword)
        ->or_like("Lebar", $keyword)
        ->or_like("Satuan", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
}
?>