<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_gudang extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_gudang()
	{
		$query=$this->db->query("SELECT * FROM tbl_gudang");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function tampilkan_id_gudang()
	{
		$this->db->select_max("IDGudang")
		->from("tbl_gudang");
		$query = $this->db->get();

		return $query->row();
	}
	/*hapus data*/
	function hapus_data_gudang($id)
	{
		$this->db->where('IDGudang', $id);
		$this->db->delete('tbl_gudang');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_gudang', $data);
		return TRUE;
	}

	function get_gudang_byid($id)
	{
		return $this->db->get_where('tbl_gudang', array('IDGudang'=>$id))->row();
	}

	function update_gudang($id, $data)
	{
		$this->db->where('IDGudang', $id);
		$this->db->update('tbl_gudang', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_gudang")
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_gudang")
		->like("Kode_Gudang", $keyword)
		->or_like("Nama_Gudang", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari($kolom,$status,$keyword){
		$this->db->select("*")
		->from("tbl_gudang")
		->like("Aktif", $status)
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function cari_by_status($status){
		$this->db->select("*")
		->from("tbl_gudang")
		->where("Aktif", $status);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>