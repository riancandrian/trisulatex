<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* 
*/
class M_mutasi_giro extends CI_Model
{
	
  private $table = 'tbl_mutasi_giro';
  private $table_detail = 'tbl_mutasi_giro_detail';
  private $table_giro = 'tbl_giro';
  private $table_coa = 'tbl_coa';
  private $table_bank = 'tbl_bank';
  private $table_mata_uang = 'tbl_mata_uang';
  private $order = 'DESC';
  private $id = 'IDMG';
  private $id_detail = 'IDMGDetail';

  // Fungsi Select semua data Menu
  public function get_all()
  {
    
    $this->db->order_by($this->id, $this->order);
    // $this->db->where('Jenis_mutasigiro', $id);
    //$this->db->join($this->table_customer, $this->table_customer.'.IDCustomer ='. $this->table.'.IDCustomer');
    
    // $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_purchase_order.IDCorak', 'left');
    // $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_purchase_order.IDMerk', 'left');
    
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  function tampilkan_id_mutasi_giro()
    {
        $this->db->select($this->id)
            ->from($this->table);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_mutasi_giro_detail()
    {
        $this->db->select($this->id_detail)
            ->from($this->table_detail);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

  //--------------Get Data by ID
  public function get_by_id($id)
  {
    // $this->db->join($this->table_customer, $this->table_customer.'.IDCustomer ='. $this->table.'.IDCustomer');
    //$this->db->join($this->table_mata_uang, $this->table_mata_uang.'.IDMataUang ='. $this->table.'.IDMataUang');
    $this->db->where($this->id, $id);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->row();
    } 
    
    
  }

  public function get_mutasigiro_detail($id)
  {
    $this->db->join($this->table_giro, $this->table_giro.'.IDGiro ='. $this->table_detail.'.IDGiro');
    $this->db->where($this->id, $id);
    $data = $this->db->get($this->table_detail);
    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
    } 
    
    
  }

  //Fungsi Tambah Data
  public function insert($data)
  {
    $this->db->insert($this->table, $data);
  }

  //Fungsi Ubah Data
  public function update($id, $data)
  {
    $this->db->where($this->id, $id);
    $this->db->update($this->table, $data);
  }

  //Fungsi Hapus Data
  public function delete($id)
  {
    $this->db->where($this->id, $id);
    $this->db->delete($this->table);
  }

  function cari($kolom,$status,$keyword){
      $this->db->select("*")
    ->from("tbl_merk")
    ->like("Aktif", $status)
    ->like("$kolom", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function cari_by_kolom($kolom,$keyword){
     $this->db->select("*")
    ->from("tbl_merk")
    ->like("$kolom", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function cari_by_status($status){
   $this->db->select("*")
    ->from("tbl_merk")
    ->where("Aktif", $status);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function cari_by_keyword($keyword){
      $this->db->select("*")
    ->from("tbl_merk")
    ->like("Kode_Merk", $keyword)
    ->or_like("Merk", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  //-------------------NO Purchase Order
  function no_mutasigiro($tahun)
  {
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
                ->where('extract(year from "Tanggal") =', $tahun)
                ->from($this->table)
                ->get()->row();
  }

   //-------------------Get Giro
   function get_giro()
   {
     $data = $this->db->get($this->table_giro);
     $hitung = $this->db->count_all_results($this->table_giro);
     if ( $hitung > 0) {
      return $data->result();
     } 
   }

   //-------------------Get Giro Where
   function getgiro($id)
   {
    //  $this->db->join('Table', 'table.column = table.column', 'left');
     $this->db->select('Jenis_Giro');
     
     $this->db->where('IDGiro', $id);
     $data = $this->db->get($this->table_giro)->row();
     $hitung = $this->db->count_all_results($this->table_giro);
     if ( $hitung > 0) {
      if ($data->Jenis_Giro == "giro keluar") {
        $query = ('
        SELECT "IDGiro", tbl_suplier."Nama", "Nomor_Faktur", "Nilai", "Nomor_Giro", "IDGiro", "Jenis_Giro", tbl_suplier."IDSupplier" as "IDBaruP" FROM tbl_giro
        JOIN tbl_suplier on tbl_giro."IDPerusahaan" = tbl_suplier."IDSupplier"
        where tbl_giro."IDGiro" = ?
        ');
      } else if ($data->Jenis_Giro == "giro masuk"){
        $query = ('
        SELECT "IDGiro", tbl_customer."Nama", "Nomor_Faktur", "Nilai", "Nomor_Giro", "IDGiro", "Jenis_Giro", tbl_customer."IDCustomer" as "IDBaruP" FROM tbl_giro
        JOIN tbl_customer on tbl_giro."IDPerusahaan" = tbl_customer."IDCustomer"
        where tbl_giro."IDGiro" = ?
        ');
      }
     } 

    return $this->db->query($query, array($id))->row();
   }

   //-------------------Get bank
   function get_bank()
   {
     $this->db->select('IDBank, Nama_COA');
     
     $this->db->join($this->table_coa, $this->table_coa.'.IDCoa ='. $this->table_bank.'.IDCoa');
     $data = $this->db->get($this->table_bank);
     $hitung = $this->db->count_all_results($this->table_bank);
     if ( $hitung > 0) {
      return $data->result();
     } 
   }
   //-------------------Get bank where
   function getbank($id)
   {
     $this->db->select('Nomor_Rekening,IDBank, Nama_COA');
     $this->db->join($this->table_coa, $this->table_coa.'.IDCoa ='. $this->table_bank.'.IDCoa');
     $this->db->where('IDBank', $id);
     $data = $this->db->get($this->table_bank);
     $hitung = $this->db->count_all_results($this->table_bank);
     if ( $hitung > 0) {
      return $data->row();
     } 
   }


   //Insert data Atas
  public function add($data){
    //$this->db->insert($this->table, $data);
    $this->db->insert($this->table, $data);
    return TRUE;
  }

  //Insert Detail nya
  public function add_detail($data){
    $this->db->insert($this->table_detail, $data);
    return TRUE;
  } 

  //Ubah Data Master
  public function update_master($id,$data){
    return $this->db->update($this->table, $data, array($this->id => $id));
  }

  public function drop($id){
    $this->db->where($this->id_detail, $id);
    $this->db->delete($this->table_detail);
    return TRUE;
  }

  function searching($date_from, $date_until, $search_type, $keyword)
  {
    
  
    $this->db->join($this->table_mata_uang, $this->table_mata_uang.'.IDMataUang ='. $this->table.'.IDMataUang');

      if ($date_from) {
          $this->db->where("Tanggal >= ", $date_from);
      }

      if ($date_until) {
          $this->db->where("Tanggal <= ", $date_until);
      }

      if ($search_type) {
          $array = array($search_type => $keyword);
          $this->db->like($array);
      }

      $query = $this->db->order_by($this->id, 'DESC')->get($this->table);
      return $query->result();
  }


  function print_mutasigiro($nomor)
  {
    $this->db->select('tbl_purchase_order.*, tbl_corak.Corak, tbl_merk.Merk, tbl_agen.Nama_Perusahaan')->from("tbl_purchase_order")
            ->join("tbl_corak", "tbl_purchase_order.IDCorak = tbl_corak.IDCorak")
            ->join("tbl_merk", "tbl_purchase_order.IDMerk = tbl_merk.IDMerk")
            ->join("tbl_agen", "tbl_purchase_order.IDAgen = tbl_agen.IDAgen")
            ->where("tbl_purchase_order.IDMG", $nomor);
     return $this->db->get()->row();
  }
  function print_mutasigiro_detail($id)
  {
     $this->db->select("*")
    ->from($this->table_detail)
    ->where("IDMG", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }
  function last_Id()
    {
        $this->db->select('IDFBA');
        $this->db->from('tbl_pembelian_asset');
        $this->db->order_by('IDFBA','DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row();
        }else{
            return "null";
        }
    }

    function tampilkan_id_jurnal()
{
    $this->db->select("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}
function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}


}
?>