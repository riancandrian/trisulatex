<?php

class M_asset extends CI_Model
{
  function tampilkan_asset()
  {
    $this->db->select("TA.*, TG.Group_Asset")
    ->from("tbl_asset TA")
    ->join("tbl_group_asset TG", "TA.IDGroupAsset = TG.IDGroupAsset");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
      return $query->result();
    }
  }

  function tampilkan_id_asset()
  {
    $this->db->select_max("IDAsset")
    ->from("tbl_asset");
    $query = $this->db->get();

    return $query->row();
  }

  function coa_asset()
  {
   $this->db->select("*")
   ->from("tbl_coa")
   ->where('substring("Nama_COA" from 1 for 4)=', 'Aset');
   $query= $this->db->get();
   if($query->num_rows()>0)
   {
    return $query->result();
  }
}

function coa_akumulasi()
{
 $this->db->select("*")
 ->from("tbl_coa")
 ->where('substring("Nama_COA" from 1 for 2)=', 'Ak');
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function coa_beban()
{
 $this->db->select("*")
 ->from("tbl_coa")
 ->where('substring("Nama_COA" from 1 for 33)=', 'B. Administrasi Umum - Depresiasi');
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function group_asset()
{
  return $this->db->get('tbl_group_asset')->result();
}

function simpan($data)
{
  $this->db->insert('tbl_asset', $data);
  return TRUE;
}

function getById($id)
{
  return $this->db->get_where('tbl_asset', array('IDAsset' => $id))->row();
}

function update($data, $where)
{
  return $this->db->update('tbl_asset', $data, $where);
}

function delete($id)
{
  $this->db->where('IDAsset', $id);
  $this->db->delete('tbl_asset');
}

function cari($kolom,$status,$keyword){
  $this->db->select("a.*, ga.Group_Asset")
  ->from("tbl_asset a")
  ->join("tbl_group_asset ga", "a.IDGroupAsset=ga.IDGroupAsset")
  ->like("a.Aktif", $status)
  ->like("$kolom", $keyword);
  $query = $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function cari_by_kolom($kolom,$keyword){
 $this->db->select("a.*, ga.Group_Asset")
 ->from("tbl_asset a")
 ->join("tbl_group_asset ga", "a.IDGroupAsset=ga.IDGroupAsset")
 ->like("$kolom", $keyword);
 $query = $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_status($status){
 $this->db->select("a.*, ga.Group_Asset")
 ->from("tbl_asset a")
 ->join("tbl_group_asset ga", "a.IDGroupAsset=ga.IDGroupAsset")
 ->where("a.Aktif", $status);
 $query = $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_keyword($keyword){
 $this->db->select("a.*, ga.Group_Asset")
 ->from("tbl_asset a")
 ->join("tbl_group_asset ga", "a.IDGroupAsset=ga.IDGroupAsset")
 ->like("ga.Group_Asset", $keyword)
 ->or_like("a.Kode_Asset", $keyword)
 ->or_like("a.Nama_Asset", $keyword)
 ->or_like("a.Aktif", $keyword);
 $query = $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}
}