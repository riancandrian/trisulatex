<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

/**
* 
*/
class M_bukubank extends CI_Model
{

  private $table = 'tbl_buku_bank';
  private $table_detail = 'tbl_buku_bank_detail';

  private $table_coa = 'tbl_coa';
  private $table_mata_uang = 'tbl_mata_uang';
  private $order = 'DESC';
  private $id = 'IDBukuBank';
  private $id_detail = 'IDBukuBankDetail';

  public function get_all()
  {

   $this->db->select("*")
            ->from("tbl_buku_bank")
            ->join("tbl_buku_bank_detail", "tbl_buku_bank.IDBukuBank = tbl_buku_bank_detail.IDBukuBank")
            ->join("tbl_coa", "tbl_coa.IDCoa = tbl_buku_bank_detail.IDCoa")
            ->order_by("tbl_buku_bank.IDCoa", "DESC");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        } 
 }

 function tampilkan_id_buku_bank()
 {
  $this->db->select($this->id)
  ->from($this->table);
  $query = $this->db->get();

  if ($query->num_rows() > 0)
  {
    return $query->result();
  }
}

function searching_laporan_buku_bank($date_from, $date_until, $search_type, $keyword)
{


  // $this->db->join("tbl_buku_bank_detail", "tbl_buku_bank_detail.IDBukuBank=tbl_buku_bank.IDBukuBank");
  $this->db->join("tbl_coa", "tbl_coa.IDCoa=tbl_jurnal.IDCOA");

  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
  }

  if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
  }

  if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
  }

  $query = $this->db->order_by("tbl_jurnal.IDJurnal", 'DESC')->get("tbl_jurnal");
  return $query->result();
}

function tampilkan_id_buku_bank_detail()
{
  $this->db->select($this->id_detail)
  ->from($this->table_detail);
  $query = $this->db->get();

  if ($query->num_rows() > 0)
  {
    return $query->result();
  }
}

  //--------------Get Data by ID
public function get_by_id($id)
{
  $this->db->join($this->table_coa, $this->table_coa.'.IDCoa ='. $this->table.'.IDCoa');
  $this->db->where($this->id, $id);
  $data = $this->db->get($this->table);

  $hitung = $this->db->count_all_results($this->table);
  if ( $hitung > 0) {
   return $data->row();
 } 


}

public function get_bukubank_detail($id)
{
  $this->db->where($this->id, $id);
  $this->db->join($this->table_coa, $this->table_coa.'.IDCoa ='. $this->table_detail.'.IDCoa');
  $data = $this->db->get($this->table_detail);
  $hitung = $this->db->count_all_results($this->table_detail);
  if ( $hitung > 0) {
   return $data->result();
 } 


}


public function getstok($barcode)
{
  $this->db->where('Barcode', $barcode);
    //Get Merk, Corak
  $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_stok.IDCorak', 'left');
  $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk', 'left');

    //Get Warna
  $this->db->join('tbl_warna', 'tbl_warna.IDWarna = tbl_stok.IDWarna', 'left');

    //get Saruan
  $this->db->join('tbl_satuan', 'tbl_satuan.IDSatuan = tbl_stok.IDSatuan', 'left');


  return $this->db->get('tbl_stok')->result_array();
}

  //Fungsi Tambah Data
public function insert($data)
{
  $this->db->insert($this->table, $data);
}

  //Fungsi Ubah Data
public function update($id, $data)
{
  $this->db->where($this->id, $id);
  $this->db->update($this->table, $data);
}

  //Fungsi Hapus Data
public function delete($id)
{
  $this->db->where($this->id, $id);
  $this->db->delete($this->table);
}

function cari($kolom,$status,$keyword){
  $this->db->select("*")
  ->from("tbl_merk")
  ->like("Aktif", $status)
  ->like("$kolom", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function cari_by_kolom($kolom,$keyword){
 $this->db->select("*")
 ->from("tbl_merk")
 ->like("$kolom", $keyword);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_status($status){
 $this->db->select("*")
 ->from("tbl_merk")
 ->where("Aktif", $status);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_keyword($keyword){
  $this->db->select("*")
  ->from("tbl_merk")
  ->like("Kode_Merk", $keyword)
  ->or_like("Merk", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function get_supplier(){
  $this->db->select("IDSupplier, Nama")
  ->from("tbl_suplier");
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

  //-------------------NO Purchase Order
function no_bukubank($tahun)
{
  return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
  ->where('extract(year from "Tanggal") =', $tahun)
  ->from($this->table)
  ->get()->row();
}

  //-------------------Get COA
function get_coa_group()
{
 $this->db->select("IDCoa, Kode_COA, Nama_COA")
 ->from("tbl_coa");
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function get_coa_group_bank()
{
 $this->db->select("*")
 ->from("tbl_coa")
 ->where('substring("Nama_COA" from 1 for 4)=', 'Bank');
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function tampilkan_id_jurnal()
{
    $this->db->select("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

    function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}

function get_coa()
{
  $this->db->select('IDCoa, Kode_COA, Nama_COA');

  $data = $this->db->get($this->table_coa);

  $hitung = $this->db->count_all_results($this->table_coa);
  if ( $hitung > 0) {
   return $data->result();
 } 
}

  //---------------------GetMerk
function getmerk($id)
{
  $this->db->from('tbl_corak');

  $this->db->join('tbl_barang', 'tbl_barang.IDMerk = tbl_corak.IDMerk');
  $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk');
  $this->db->where('tbl_corak.IDCorak', $id);
    //$this->db->join('tbl_barang', 'tbl_barang.IDMerk = tbl_corak.IDMerk');

  $data = $this->db->get();

  $hitung = $this->db->count_all_results('tbl_corak');
  if ( $hitung > 0) {
   return $data->row();
 } 
}

   //---------------------Getwarna
function getwarna($id)
{
 $this->db->where('Kode_Corak', $id);
 $this->db->join('tbl_corak', 'tbl_warna.IDCorak = tbl_corak.IDCorak', 'left');
 $data = $this->db->get('tbl_warna');
 $hitung = $this->db->count_all_results('tbl_warna');
 if ( $hitung > 0) {
  return $data->result();
} 
}


   //Insert data Atas
public function add($data){
    //$this->db->insert($this->table, $data);
  $this->db->insert($this->table, $data);
  return TRUE;
}

  //Insert Detail nya
public function add_detail($data){
  $this->db->insert($this->table_detail, $data);
  return TRUE;
} 

  //Ubah Data Master
public function update_master($id,$data){
  return $this->db->update($this->table, $data, array($this->id => $id));
}

public function drop($id){
  $this->db->where($this->id_detail, $id);
  $this->db->delete($this->table_detail);
  return TRUE;
}

function searching($date_from, $date_until, $search_type, $keyword)
{

  $this->db->join($this->table_detail, $this->table_detail.'.IDBukuBank ='. $this->table.'.IDBukuBank');
  $this->db->join($this->table_coa, $this->table_coa.'.IDCoa ='. $this->table_detail.'.IDCoa');

  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
  }

  if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
  }

  if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
  }

  $query = $this->db->order_by($this->table.'.'.$this->id, 'DESC')->get($this->table);
  return $query->result();
}

  //--------------------------Searching Store Procedure
public function searching_store($id, $date_from, $date_until, $keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_jurnalumum(?, ?, ?, ?)
    ');

  /*$query = ('select * from pengukuran');  */

  return $this->db->query($query, array($id, $date_from, $date_until, $new_keywords))->result();
}

  //--------------------------Searching Store Procedure
public function searching_store_like($id, $keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_jurnalumum_like(?, ?)
    ');

  /*$query = ('select * from pengukuran');  */

  return $this->db->query($query, array($id, $new_keywords))->result();
}

function print_jurnalumum($nomor)
{
  $this->db->select('tbl_purchase_order.*, tbl_corak.Corak, tbl_merk.Merk, tbl_agen.Nama_Perusahaan')->from("tbl_purchase_order")
  ->join("tbl_corak", "tbl_purchase_order.IDCorak = tbl_corak.IDCorak")
  ->join("tbl_merk", "tbl_purchase_order.IDMerk = tbl_merk.IDMerk")
  ->join("tbl_agen", "tbl_purchase_order.IDAgen = tbl_agen.IDAgen")
  ->where("tbl_purchase_order.IDPO", $nomor);
  return $this->db->get()->row();
}
function print_jurnalumum_detail($id)
{
 $this->db->select("*")
 ->from($this->table_detail)
 ->where("IDPO", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}
function last_Id()
{
  $this->db->select('IDFBA');
  $this->db->from('tbl_pembelian_asset');
  $this->db->order_by('IDFBA','DESC');
  $this->db->limit(1);
  $query = $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->row();
  }else{
    return "null";
  }
}


  //---------------------get Satuan
function get_satuan()
{
  $query = ('
    select DISTINCT(s."IDSatuan"),s."Satuan" from tbl_satuan s
    JOIN tbl_barang b ON 
    s."IDSatuan" = b."IDSatuan" 
    ');
  $hitung = $this->db->query($query)->result();
  if ( $hitung > 0) {
    return $hitung;
  } 
}




  //----------------Get barang seragam
public function get_barang_seragam()
{
  $this->db->join($this->table_group_barang, $this->table_group_barang.'.IDGroupBarang ='. $this->table_barang.'.IDGroupBarang');
  $this->db->where('Group_Barang !=', 'kain');
  return $this->db->get($this->table_barang)->result();

}




  //-------------------Get Harga Satuan
public function get_harga($id)
{
  $this->db->from($this->table_harga_jual);
  $this->db->join($this->table_barang, $this->table_barang.'.IDBarang ='. $this->table_harga_jual.'.IDBarang');
    // $this->db->join($this->table_corak, $this->table_corak.'.IDCorak ='. $this->table_harga_jual.'.IDCorak');
    // $this->db->join($this->table_merk, $this->table_merk.'.IDMerk ='. $this->table_harga_jual.'.IDMerk');
  $this->db->where($this->table_barang.'.IDBarang', $id);
  $data = $this->db->get();
  $hitung = $this->db->count_all_results('tbl_corak');
  if ( $hitung > 0) {
   return $data->row();
 } 
}
}
?>