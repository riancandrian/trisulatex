<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_group_coa extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampil_group_coa()
	{
		$query=$this->db->query("SELECT * FROM tbl_group_coa");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function tampil_id_group_coa()
	{
		$this->db->select_max("IDGroupCOA")
		->from("tbl_group_coa");
		$query = $this->db->get();

		return $query->row();
	}
	/*hapus data*/
	function hapus_data_group_coa($id)
	{
		$this->db->where('IDGroupCOA', $id);
		$this->db->delete('tbl_group_coa');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function tambah_group_coa($data)
	{
		$this->db->insert('tbl_group_coa', $data);
		return TRUE;
	}

	function ambil_group_coa_byid($id)
	{
		return $this->db->get_where('tbl_group_coa', array('IDGroupCOA'=>$id))->row();
	}

	function aksi_edit_group_coa($id, $data)
	{
		$this->db->where('IDGroupCOA', $id);
		$this->db->update('tbl_group_coa', $data);
		return TRUE;
	}

	function cari($kolom,$status,$keyword){
		$this->db->select("*")
		->from("tbl_group_coa")
		->like("Aktif", $status)
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_group_coa")
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_status($status){
		$this->db->select("*")
		->from("tbl_group_coa")
		->where("Aktif", $status);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_group_coa")
		->like("Kode_Group_COA", $keyword)
		->or_like("Nama_Group", $keyword)
		->or_like("Normal_Balance", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>