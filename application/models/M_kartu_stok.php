<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_kartu_stok extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_kartustok()
	{
		$this->db->select("*")
		->from("tbl_kartu_stok");
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	function insert_csv($data) {
        $this->db->insert('tbl_import', $data);
    }
    function getDetailId($id)
    {
        return $this->db->select("*")
        ->from("tbl_kartu_stok")
        ->where("IDKartuStok", $id)->get()->row();
    }

     function cari_by_kolom($kolom,$keyword){
        $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_kartu_stok.Jenis_faktur, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        // ->join("tbl_merk", "tbl_kartu_stok.IDMerk=tbl_merk.IDMerk")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->like("$kolom", $keyword)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_full($kolom,$keyword, $mulai, $akhir){
           $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_kartu_stok.Jenis_faktur, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        // ->join("tbl_merk", "tbl_kartu_stok.IDMerk=tbl_merk.IDMerk")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->like("$kolom", $keyword)
        ->where("tbl_kartu_stok.Tanggal >=", $mulai)
        ->where("tbl_kartu_stok.Tanggal <=", $akhir)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    } 

    function cari_by_periode($mulai, $akhir){
           $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_corak.Corak, tbl_kartu_stok.Jenis_faktur, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        // ->join("tbl_merk", "tbl_kartu_stok.IDMerk=tbl_merk.IDMerk")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->where("tbl_kartu_stok.Tanggal >=", $mulai)
        ->where("tbl_kartu_stok.Tanggal <=", $akhir)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }


    function cari_by_keyword($keyword){
          $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_corak.Corak, tbl_kartu_stok.Jenis_faktur, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        // ->join("tbl_merk", "tbl_kartu_stok.IDMerk=tbl_merk.IDMerk")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->like("tbl_kartu_stok.Barcode", $keyword)
        ->or_like("tbl_corak.Corak", $keyword)
        ->or_like("tbl_merk.Merk", $keyword)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_perkey($mulai, $akhir, $keyword){
          $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_kartu_stok.Jenis_faktur, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        // ->join("tbl_merk", "tbl_kartu_stok.IDMerk=tbl_merk.IDMerk")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->like("Barcode", $keyword)
        ->or_like("tbl_corak.Corak", $keyword)
        ->or_like("tbl_warna.Warna", $keyword)
        ->where("tbl_kartu_stok.Tanggal >=", $mulai)
        ->where("tbl_kartu_stok.Tanggal <=", $akhir)
         ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_kolom_kedua($kolom,$keyword, $custdes){
         $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_kartu_stok.Jenis_faktur, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->like("$kolom", $keyword)
        ->like("tbl_corak.Corak", $custdes)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
function cari_by_full_kedua($kolom,$keyword, $mulai, $akhir, $custdes){
         $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_kartu_stok.Jenis_faktur, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->like("$kolom", $keyword)
        ->like('tbl_corak.Corak', $custdes)
        ->where("tbl_kartu_stok.Tanggal >=", $mulai)
        ->where("tbl_kartu_stok.Tanggal <=", $akhir)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    } 
 function cari_by_periode_kedua($mulai, $akhir, $custdes){
         $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_kartu_stok.Jenis_faktur, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->like('tbl_corak.Corak', $custdes)
        ->where("Tanggal >=", $mulai)
        ->where("Tanggal <=", $akhir)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
 function cari_by_keyword_kedua($keyword, $custdes){
          $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_kartu_stok.Jenis_faktur, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->join("tbl_merk", "tbl_kartu_stok.IDMerk=tbl_merk.IDMerk")
        ->like("tbl_corak.Corak", $custdes)
        ->like("tbl_merk.Merk", $keyword)
        ->or_like("tbl_warna.Warna", $keyword)
        ->or_like("tbl_kartu_stok.Grade", $keyword)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
function cari_by_perkey_kedua($mulai, $akhir, $keyword, $custdes){
          $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_kartu_stok.Jenis_faktur, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->join("tbl_merk", "tbl_kartu_stok.IDMerk=tbl_merk.IDMerk")
        ->like("tbl_corak.Corak", $custdes)
        ->like("tbl_merk.Merk", $keyword)
        ->or_like("tbl_warna.Warna", $keyword)
        ->where("tbl_kartu_stok.Tanggal >=", $mulai)
        ->where("tbl_kartu_stok.Tanggal <=", $akhir)
        ->order_by("tbl_kartu_stok.IDKartuStok", "ASC");
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function get_kartu($idfaktur)
    {
        return  $this->db->select("tbl_kartu_stok.IDKartuStok, tbl_kartu_stok.Tanggal, tbl_kartu_stok.Barcode, tbl_corak.Corak, tbl_warna.Warna, tbl_kartu_stok.Masuk_yard, tbl_kartu_stok.Masuk_meter, tbl_kartu_stok.Keluar_yard, tbl_kartu_stok.Keluar_meter, tbl_kartu_stok.Grade")
        ->from("tbl_kartu_stok")
        ->join("tbl_corak", "tbl_kartu_stok.IDCorak=tbl_corak.IDCorak")
        ->join("tbl_warna", "tbl_kartu_stok.IDWarna=tbl_warna.IDWarna")
        ->join("tbl_merk", "tbl_corak.IDMerk=tbl_merk.IDMerk")
        ->where("IDKartuStok", $idfaktur)->get()->row();
    }
  


}
?>