<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_konversi_satuan extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_konversi_satuan()
	{
		$this->db->select("*")
		->from("tbl_konversi_satuan ks")
		->join("tbl_satuan s", "ks.IDSatuanKecil=s.IDSatuan");
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampilkan_satuan()
	{
		$query=$this->db->query("SELECT * FROM tbl_satuan");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/*hapus data*/
	function hapus_data_konversi($id)
	{
		$this->db->where('IDKonversi', $id);
		$this->db->delete('tbl_konversi_satuan');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function tambah_konversi($database)
	{
		$this->db->insert('tbl_konversi_satuan', $database);
		return TRUE;
	}

	function ambil_konversi_byid($id)
	{
		return $this->db->get_where('tbl_konversi_satuan', array('IDKonversi'=>$id))->row();
	}

	function aksi_edit_konversi($id, $data)
	{
		$this->db->where('IDKonversi', $id);
		$this->db->update('tbl_konversi_satuan', $data);
		return TRUE;
	}

	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_konversi_satuan ks")
		->join("tbl_satuan s", "ks.IDSatuanKecil=s.IDSatuan")
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}


	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_konversi_satuan ks")
		->join("tbl_satuan s", "ks.IDSatuanKecil=s.IDSatuan")
		->like("s.Satuan", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>