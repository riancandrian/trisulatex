<?php

class M_penerimaan_barang_umum2 extends CI_Model
{
    private $table1 = 'tbl_terima_barang_supplier_umum';
    private $table2 = 'tbl_terima_barang_supplier_umum_detail';
    private $table3 = 'tbl_barang';
    private $table4 = 'tbl_tampungan';
    private $table5 = 'tbl_in';
    private $table6 = 'tbl_stok';
    private $table7 = 'tbl_kartu_stok';
    private $table8 = 'tbl_merk';
    private $table9 = 'tbl_warna';
    private $table10 = 'tbl_satuan';
    private $table11 = 'tbl_suplier';
    private $table12 = 'tbl_barang';
    private $table13 = 'tbl_gudang';
    private $table14 = 'tbl_purchase_order_umum';
     private $table15 = 'tbl_purchase_order_detail';
     private $table16 = 'tbl_jurnal';

    function all()
    {
        $this->db->distinct();
        $this->db->select($this->table1 . '.*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBSUmum = ' . $this->table2 . '.IDTBSUmum');

        $query = $this->db->order_by($this->table1 . '.IDTBSUmum', 'DESC')->get();
        return $query->result();
    }

    function getCodeReception($tahun)
  {
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
                ->where('extract(year from "Tanggal") =', $tahun)
                ->from("tbl_terima_barang_supplier_umum")
                ->get()->row();
  }

    public function getpobyid($id)
{
  $this->db->select('IDPOUmumDetail, Qty, Saldo');

  $this->db->where('IDPOUmumDetail', $id);
  return $this->db->get("tbl_purchase_order_umum_detail")->row();
}

public function updatepoqty($data,$id)
{
  $this->db->where('IDPOUmum', $id);
  $this->db->update("tbl_purchase_order_umum_detail", $data);
}

function tampilkan_id_jurnal()
{
    $this->db->select_max("IDJurnal")
    ->from("tbl_jurnal");
   $query = $this->db->get();

  return $query->row();
}

function save_jurnal($data)
{
    $this->db->insert($this->table16, $data);
    return TRUE;
}

function cek_group_barang($id)
{
        $this->db->select('tbl_group_barang.Group_Barang');
        $this->db->join("tbl_group_barang", "tbl_group_barang.IDGroupBarang = tbl_barang.IDGroupBarang");
        $this->db->where('tbl_barang.IDBarang', $id);
        return $this->db->get("tbl_barang")->row();
}

    function check_PO($no_po)
    {
        $this->db->select('*')
            ->from("tbl_purchase_order_umum")
            ->join("tbl_purchase_order_umum_detail", "tbl_purchase_order_umum_detail.IDPOUmum=tbl_purchase_order_umum.IDPOUmum")
            ->join("tbl_barang", "tbl_barang.IDBarang=tbl_purchase_order_umum_detail.IDBarang")
            ->join("tbl_satuan", "tbl_satuan.IDSatuan=tbl_purchase_order_umum_detail.IDSatuan")
            ->where("tbl_purchase_order_umum.Nomor", $no_po);
        $query = $this->db->get();

        return $query->result();
    }

    function tampilkan_id_terima_barang_umum()
    {
        $this->db->select_max("IDTBSUmum")
            ->from("tbl_terima_barang_supplier_umum");
        $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_terima_barang_umum_detail()
    {
        $this->db->select_max("IDTBSUmumDetail")
            ->from("tbl_terima_barang_supplier_umum_detail");
       $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_stok()
    {
        $this->db->select_max("IDStok")
            ->from("tbl_stok");
        $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_kartu_stok()
    {
        $this->db->select_max("IDKartuStok")
            ->from("tbl_kartu_stok");
        $query = $this->db->get();

  return $query->row();
    }

    function get_nosjc()
    {
         $this->db->select("IDPOUmum, Nomor")
            ->from("tbl_purchase_order_umum");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function save($data)
    {
        $this->db->insert($this->table1, $data);
        return TRUE;
    }

    function save_detail($data)
    {
        $this->db->insert($this->table2, $data);
        return TRUE;
    }

    function save_stock($data)
    {
        $this->db->insert($this->table6, $data);
        return TRUE;
    }

    function save_stock_card($data)
    {
        $this->db->insert($this->table7, $data);
        return TRUE;
    }

    function find($id)
    {
        return $this->db->select($this->table1 . '.*,' . $this->table11 . '.Nama,'. $this->table14 . '.Nomor AS nomorPO')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBSUmum = ' . $this->table2 . '.IDTBSUmum')
            ->join($this->table11, $this->table1 . '.IDSupplier = ' . $this->table11 . '.IDSupplier')
            ->join($this->table14, $this->table1 . '.IDPOUmum = ' . $this->table14 . '.IDPOUmum')
            ->where($this->table1 . '.IDTBSUmum', $id)
            ->get()->row();
    }

    function findDetail($id)
    {
        //return $this->db->get_where($this->table2, array('IDTBS' => $id))->result();
        $query = $this->db->select(
            $this->table2 . '.*,' .
            $this->table3 . '.Nama_Barang,' .
            $this->table10 . '.Satuan,' .
            $this->table10 . '.IDSatuan')
            ->from($this->table2)
            ->join($this->table3, $this->table2 . '.IDBarang = ' . $this->table3 . '.IDBarang')
            ->join($this->table10, $this->table2 . '.IDSatuan = ' . $this->table10 . '.IDSatuan')
            ->where($this->table2 . '.IDTBSUmum', $id)->get();
        return $query->result();
    }

    function soft_delete($id)
    {
        return $this->db->update($this->table1, array('Batal' => 'tidak aktif'), array('IDTBSUmum' => $id));
    }

    function soft_success($id)
    {
        return $this->db->update($this->table1, array('Batal' => 'aktif'), array('IDTBSUmum' => $id));
    }

    function update($data, $id)
    {
        return $this->db->update($this->table1, $data, array('IDTBSUmum' => $id));
    }

    function update_detail($data, $id, $barcode)
    {
        return $this->db->update($this->table2, $data, array('IDTBS' => $id, 'Barcode' => $barcode));
    }

    public function drop($idDetail, $id){

        $detail = $this->db->select('Qty_yard, Qty_meter')->from($this->table2)->where('IDTBSUmumDetail', $idDetail)->get()->row();

        $main = $this->db->select('Total_qty_yard, Total_qty_meter, Saldo_yard, Saldo_meter')->from($this->table1)->where('IDTBS', $id)->get()->row();

        $Update_totalYard = ($main->Total_qty_yard - $detail->Qty_yard);
        $Update_totalMeter = ($main->Total_qty_meter - $detail->Qty_meter);

        $data = array(
            'Total_qty_yard' => $Update_totalYard,
            'Total_qty_meter' => $Update_totalMeter,
            'Saldo_yard' => $Update_totalYard,
            'Saldo_meter' => $Update_totalMeter
        );
        $this->db->update($this->table1, $data, array('IDTBSUmum' => $id));

        $this->db->where('IDTBSUmumDetail', $idDetail);
        $this->db->delete($this->table2);
        return TRUE;
    }

    function bulk($where)
    {
        $this->db->where_in('Nomor', $where);
        return $this->db->update($this->table1, array('Batal' => 'tidak aktif'));
    }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->distinct()
        ->select($this->table1 . '.*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBSUmum = ' . $this->table2 . '.IDTBSUmum');

        if ($date_from) {
            $this->db->where("'".$date_from."'"  . " <= tbl_terima_barang_supplier_umum.Tanggal ");
        }

        if ($date_until) {
            $this->db->where("'".$date_until."'" ." >= tbl_terima_barang_supplier_umum.Tanggal ");
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by('IDTBSUmum', 'DESC')->get();
        return $query->result();
    }

    function get_IDcorak($corak)
    {
        //return $this->db->get_where($this->table3, array('Corak' => $corak))->row();
        return $this->db->select('*')->from($this->table3)->where('LOWER("Corak") = '."LOWER('$corak')")->get()->row();
    }

    function get_IDwarna($warna)
    {
        //return $this->db->get_where($this->table9, array('Warna' => $warna))->row();
        return $this->db->select('*')->from($this->table9)->where('LOWER("Warna") = '."LOWER('$warna')")->get()->row();
    }

    function get_IDsatuan($satuan)
    {
        //return $this->db->get_where($this->table10, array('Satuan' => $satuan))->row();
        return $this->db->select('*')->from($this->table10)->where('LOWER("Satuan") = '."LOWER('$satuan')")->get()->row();
    }

    function get_IDPO($noPO)
    {
        // return $this->db->select('*')->from($this->table14)->where("Nomor"=>$nopo)->get()->row();
        return $this->db->get_where($this->table14, array('Nomor' => $noPO))->row();
    }

    function get_IDbarang($corak)
    {
        $this->db->select('IDBarang')->from($this->table12)
            ->join($this->table3, $this->table12 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            //->where($this->table3 . '.Corak', $corak);
            ->where('LOWER("Corak") = '."LOWER('$corak')");
        return $this->db->get()->row();
    }

    function get_IDgudang()
    {
        $this->db->select('IDGudang')->from($this->table13)
            ->where("(Nama_Gudang = 'PT. TRISULA' OR Nama_Gudang = 'PT. Trisula')");
        return $this->db->get()->row();
    }

    function check_noPB($no_pb)
    {
        return $this->db->get_where($this->table1, array('Nomor' => $no_pb));
    }

    function getPrint_edit($sjc)
    {
        return $this->db->select($this->table1 . '.*,' . $this->table3 . '.Nama_Barang,'. $this->table10 . '.Satuan')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBSUmum = ' . $this->table2 . '.IDTBSUmum')
            ->join($this->table3, $this->table2 . '.IDBarang = ' . $this->table3 . '.IDBarang')
            ->join($this->table10, $this->table10 . '.IDSatuan = ' . $this->table2 . '.IDSatuan')
            ->where($this->table1 . '.IDTBSUmum', $sjc)
            ->get()->row();
    }

    function getPrint()
    {
        return $this->db->select($this->table1 . '.*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBSUmum = ' . $this->table2 . '.IDTBSUmum')
            ->order_by($this->table1.'.IDTBSUmum', 'DESC')
            ->limit(1)
            ->get()->row();
    }

    function getDetail($sjc)
    {
        $query = $this->db->select(
            $this->table2 . '.*,' .
            $this->table3 . '.Nama_Barang,' .
            $this->table10 . '.Satuan,' .
            $this->table10 . '.IDSatuan')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBSUmum = ' . $this->table2 . '.IDTBSUmum')
            ->join($this->table3, $this->table2 . '.IDBarang = ' . $this->table3 . '.IDBarang')
            ->join($this->table10, $this->table10 . '.IDSatuan = ' . $this->table2 . '.IDSatuan')
            ->where($this->table1 . '.IDTBSUmum', $sjc)->get();
        return $query->result();
     }

       public function get_by_id_detail($id)
  {
    $this->db->select("tbl_terima_barang_supplier_umum_detail.*, tbl_barang.Nama_Barang")
    ->from("tbl_terima_barang_supplier_umum_detail")
    ->join("tbl_barang", "tbl_terima_barang_supplier_umum_detail.IDBarang=tbl_barang.IDBarang")
    ->join("tbl_satuan", "tbl_terima_barang_supplier_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("IDTBSUmum", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
    
    
  }

  public function getDetail_tbs($id){
    $this->db->select("tbl_terima_barang_supplier_umum_detail.*, tbl_satuan.Satuan, tbl_barang.Nama_Barang")
    ->from("tbl_terima_barang_supplier_umum_detail")
    ->join("tbl_barang", "tbl_barang.IDBarang=tbl_terima_barang_supplier_umum_detail.IDBarang")
    ->join("tbl_satuan", "tbl_terima_barang_supplier_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("IDTBSUmum", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
    
  }
}