<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_piutang extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_piutang()
	{
		$this->db->select("*")
		->from("tbl_piutang p")
		->join("tbl_customer c", "p.IDCustomer=c.IDCustomer");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampilkan_id_piutang()
	{
		$this->db->select_max("IDPiutang")
		->from("tbl_piutang");
		$query = $this->db->get();

		return $query->row();
	}
	function get_customer()
	{
		$query=$this->db->query("SELECT * FROM tbl_customer;");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/*hapus data*/
	function hapus_data_piutang($id)
	{
		$this->db->where('IDPiutang', $id);
		$this->db->delete('tbl_piutang');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_piutang', $data);
		return TRUE;
	}

	function get_piutang_byid($id)
	{
		return $this->db->get_where('tbl_piutang', array('IDPiutang'=>$id))->row();
	}

	function update_piutang($id, $data)
	{
		$this->db->where('IDPiutang', $id);
		$this->db->update('tbl_piutang', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_piutang p")
		->join("tbl_customer c", "p.IDCustomer=c.IDCustomer")
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_piutang p")
		->join("tbl_customer c", "p.IDCustomer=c.IDCustomer")
		->like("c.Nama", $keyword)
		->or_like("p.No_Faktur", $keyword)
		->or_like("p.Jatuh_Tempo", $keyword)
		->or_like("p.Tanggal_Piutang", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>