<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_scan extends CI_Model
{
    private $table1 = 'tbl_stok';
    private $table3 = 'tbl_corak';
    private $table8 = 'tbl_merk';
    private $table9 = 'tbl_warna';
    private $table10 = 'tbl_satuan';
    private $table12 = 'tbl_barang';
    private $table13 = 'tbl_gudang';
    private $table14 = 'tbl_tampungan';

    public function getimport($barcode)
    {
        $this->db->select(
            $this->table1 . '.*,' .
            $this->table3 . '.IDCorak,' .
            $this->table3 . '.Corak,' .
            $this->table9 . '.IDWarna,' .
            $this->table9 . '.Warna,' .
            $this->table10 . '.IDSatuan,' .
            $this->table10 . '.Satuan,' .
            $this->table12 . '.IDBarang,' .
            $this->table12 . '.Nama_Barang'
        );
        $this->db->from($this->table1);
        $this->db->join($this->table3, $this->table1 . '.IDCorak = ' . $this->table3 . '.IDCorak');
        $this->db->join($this->table9, $this->table1 . '.IDWarna = ' . $this->table9 . '.IDWarna');
        $this->db->join($this->table10, $this->table1 . '.IDSatuan = ' . $this->table10 . '.IDSatuan');
        $this->db->join($this->table12, $this->table1 . '.IDBarang = ' . $this->table12 . '.IDBarang');
        //$this->db->join($this->table13, $this->table1 . '.IDGudang = ' . $this->table13 . '.IDGudang');
        $this->db->where($this->table1 . '.Barcode', $barcode);

        return $this->db->get()->result_array();
    }

    public function getin($barcode)
    {
        $this->db->where('Barcode', $barcode);
        return $this->db->get('tbl_in')->result_array();
    }

    public function insert_data_semua($data, $data2)
    {
        $this->db->insert('tbl_in', $data);
        $this->db->insert('tbl_kartu_stok', $data2);
        //$this->db->insert('tbl_stok', $data3);
        return TRUE;
    }

    public function save_stock($data)
    {
        $this->db->insert('tbl_stok', $data);
        return $this->db->insert_id();
    }

    public function cek_ketersediaan($data, $nama_table)
    {
        $this->db->where($data);
        $data = $this->db->get($nama_table);
        $hitung = $this->db->count_all_results($nama_table);
        if ($hitung > 0) {
            return $data->row();
        }

    }

    public function update_data_yang_sama($cek, $data)
    {
        $this->db->where($cek);
        $this->db->update('tbl_in', $data);

        $this->db->where($cek);
        $this->db->update('tbl_stok', $data);

        return TRUE;

    }

    public function insert_kartu_stok($data)
    {
        $this->db->insert('tbl_kartu_stok', $data);
        return TRUE;
    }

    function update_stok($data_in, $where)
    {
        return $this->db->update('tbl_in', $data_in, $where);
    }

    function update_stok_kartu($data_kartu, $where)
    {
        return $this->db->update('tbl_kartu_stok', $data_kartu, $where);
    }


    function getDataBarcode($id)
    {
        return $this->db->get_where('tbl_in', array('IDIn'=>$id))->row();
    }

    function get_IDmerk($merk)
    {
        return $this->db->select('IDMerk')->from($this->table8)->where('LOWER("Merk") = '."LOWER('$merk')")->get()->row();
    }

    function get_IDcorak($corak)
    {
        return $this->db->get_where($this->table3, array('IDCorak' => $corak))->row();
        //return $this->db->select('*')->from($this->table3)->where('LOWER("Corak") = '."LOWER('$corak')")->get()->row();
    }

    function get_IDwarna($warna)
    {
        //return $this->db->get_where($this->table9, array('Warna' => $warna))->row();
        return $this->db->select('*')->from($this->table9)->where('LOWER("Warna") = '."LOWER('$warna')")->get()->row();
    }

    function get_IDsatuan($satuan)
    {
        //return $this->db->get_where($this->table10, array('Satuan' => $satuan))->row();
        return $this->db->select('*')->from($this->table10)->where('LOWER("Satuan") = '."LOWER('$satuan')")->get()->row();
    }

    function get_IDbarang($corak)
    {
        $this->db->select('IDBarang')->from($this->table12)
            ->join($this->table3, $this->table12 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            ->where('LOWER("Corak") = '."LOWER('$corak')");
            //->where($this->table3 . ".Corak", $corak);
        return $this->db->get()->row();
    }

    function get_IDgudang()
    {
        $this->db->select('IDGudang')->from($this->table13)
            ->where("(Nama_Gudang = 'PT. TRISULA' OR Nama_Gudang = 'PT. Trisula')");
        return $this->db->get()->row();
    }

    function save($data)
    {
        return $this->db->insert('tbl_in', $data);
    }

    function update_tbl_stok($data_stok, $where)
    {
        return $this->db->update($this->table1, $data_stok, $where);
    }

    function update_tbl_tampungan($data_tampungan, $where)
    {
        return $this->db->update($this->table14, $data_tampungan, $where);
    }
    function all_scan()
    {
        $query = ('SELECT * FROM laporan_scan()');

        return $this->db->query($query)->result();
    }


  //--------------------------Searching Store Procedure
  public function searching_store_like_scan($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_scan(?)
    ');

    return $this->db->query($query, array($new_keywords))->result();
  }
}