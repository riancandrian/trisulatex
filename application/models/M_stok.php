<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_stok extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_stok()
	{
		$this->db->select("Corak, Warna, Grade, IDGudang")
    ->from("tbl_stok")
    ->join("tbl_corak", "tbl_stok.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_stok.IDWarna=tbl_warna.IDWarna")
    ->select_sum('Saldo_yard')
    ->select_sum('Saldo_meter')
    ->group_by("Corak, Warna, Grade, IDGudang");
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

    function pencariangudang($jenisgudang)
    {
        $this->db->select("Corak, Warna, Grade, IDGudang")
    ->from("tbl_stok")
    ->join("tbl_corak", "tbl_stok.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_stok.IDWarna=tbl_warna.IDWarna")
    ->select_sum('Saldo_yard')
    ->select_sum('Saldo_meter')
    ->where("IDGudang", $jenisgudang)
    ->group_by("Corak, Warna, Grade, IDGudang");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

	function cari_by_kolom($kolom,$keyword){
       $this->db->select("Corak, Warna, Grade")
    ->from("tbl_stok")
    ->join("tbl_corak", "tbl_stok.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_stok.IDWarna=tbl_warna.IDWarna")
    ->select_sum('Saldo_yard')
    ->select_sum('Saldo_meter')
    ->group_by("Corak, Warna, Grade")
        ->like("$kolom", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }


    function cari_by_keyword($keyword){
         $this->db->select("Corak, Warna, Grade")
    ->from("tbl_stok")
    ->join("tbl_corak", "tbl_stok.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_stok.IDWarna=tbl_warna.IDWarna")
    ->select_sum('Saldo_yard')
    ->select_sum('Saldo_meter')
    ->group_by("Corak, Warna, Grade")
        ->or_like("Corak", $keyword)
        ->or_like("Warna", $keyword)
        ->or_like("Grade", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

  
	


}
?>