<?php

class M_hutang extends CI_Model
{
    function tampilkan_hutang()
    {
        $this->db->select("TH.*, TS.Nama")
        ->from("tbl_hutang TH")
        ->join("tbl_suplier TS", "TH.IDSupplier = TS.IDSupplier");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_hutang()
    {
        $this->db->select_max("IDHutang")
      ->from("tbl_hutang");
      $query = $this->db->get();

      return $query->row();
  }

  function simpan($data)
  {
    $this->db->insert('tbl_hutang', $data);
    return TRUE;
}

function supplier()
{
    return $this->db->get('tbl_suplier')->result();
}

function getById($id)
{
    return $this->db->get_where('tbl_hutang', array('IDHutang' => $id))->row();
}

function update($data, $where)
{
    return $this->db->update('tbl_hutang', $data, $where);
}

function getDetailId($id)
{
    return $this->db->select("TH.*, TS.Nama")
    ->from("tbl_hutang TH")
    ->join("tbl_suplier TS", "TH.IDSupplier = TS.IDSupplier")
    ->where("TH.IDHutang", $id)->get()->row();
}

function delete($id)
{
    $this->db->where('IDHutang', $id);
    $this->db->delete('tbl_hutang');
}

function cari_by_kolom($kolom,$keyword){
    $this->db->select("*")
    ->from("tbl_hutang h")
    ->join("tbl_suplier s", "h.IDSupplier=s.IDSupplier")
    ->like("$kolom", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
        return $query->result();
    }
}


function cari_by_keyword($keyword){
   $this->db->select("*")
   ->from("tbl_hutang h")
   ->join("tbl_suplier s", "h.IDSupplier=s.IDSupplier")
   ->like("No_Faktur", $keyword)
   ->or_like("Nama", $keyword);
   $query= $this->db->get();
   if($query->num_rows()>0)
   {
    return $query->result();
}
}
}