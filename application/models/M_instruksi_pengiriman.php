<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_instruksi_pengiriman extends CI_Model {

	// Set nama table sebagai const
	private $table = 'tbl_tampungan';
	private $id_sjc = 'Nomor_sj';
	function __construct()
	{
		parent::__construct();
	}

	function semua_instruksi(){
		$this->db->select('*');
		$this->db->from('tbl_instruksi_pengiriman');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampilkan_id_instruksi()
	{
		$this->db->select_max("IDIP")
		->from("tbl_instruksi_pengiriman");
		$query = $this->db->get();

  return $query->row();
	}

	function tampilkan_id_instruksi_detail()
	{
		$this->db->select_max("IDIPDetail")
		->from("tbl_instruksi_pengiriman_detail");
		$query = $this->db->get();

  return $query->row();
	}
	function filter_instruksi($date_from, $date_until, $kategori, $keyword){
		$this->db->select('*');
		$this->db->from('tbl_instruksi_pengiriman');

		if ($date_from) {
			$this->db->where("Tanggal >= ", $date_from);
		}

		if ($date_until) {
			$this->db->where("Tanggal <= ", $date_until);
		}

		if ($kategori) {
			$array = array($kategori => $keyword);
			$this->db->like($array);
		}

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function getAll_sjc()
	{
		// $this->db->select('Nomor_sj,NoPO,NoSO');
		// $this->db->from($this->table);
		// $this->db->group_by('Nomor_sj');
		// $this->db->group_by('NoPO');
		// $this->db->group_by('NoSO');
		// $query = $this->db->get();
		// if($query->num_rows()>0)
		// {
		// 	return $query->result();
		// }
		$query= $this->db->query('SELECT "Nomor_sj" from tbl_tampungan where "Nomor_sj" NOT IN (SELECT "Nomor_sj" FROM tbl_instruksi_pengiriman) GROUP BY "Nomor_sj"');
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		
	}
	public function getID_sjc($id)
	{
		return $this->db->get_where('tbl_tampungan', array('Nomor_sj' => $id))->row();
	}
	public function getID_sjc_tabel($id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('Nomor_sj',$id);
		$this->db->where('Saldo_yard >',0);
		$this->db->where('Saldo_meter >',0);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function last_Id()
	{
		$this->db->select('*');
		$this->db->from('tbl_instruksi_pengiriman');
		$this->db->order_by('IDIP','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row();
		}else{
			return "null";
		}
	}
	function tambah_instruksi($data)
	{
		$this->db->insert('tbl_instruksi_pengiriman', $data);
		return TRUE;
	}
	function tambah_instruksi_detail($data)
	{
		$this->db->insert('tbl_instruksi_pengiriman_detail', $data);
		return TRUE;
	}
	public function delete($id)
	{
		$this->db->where('IDIP', $id);
		$this->db->delete('tbl_instruksi_pengiriman');
		$this->M_instruksi_pengiriman->delete_detail_IDIP($id);
	}
	public function pindah_tampungan_detail_instruksi($key,$last_Id, $urutan_id_detail){
		$this->db->select('*');
		$this->db->from('tbl_tampungan');
		$this->db->join('tbl_corak', 'tbl_corak.Corak = tbl_tampungan.Corak');
		$this->db->join('tbl_warna', 'tbl_warna.Warna = tbl_tampungan.Warna');
		$this->db->join('tbl_barang', 'tbl_barang.IDCorak = tbl_corak.IDCorak');
		$this->db->where("IDT", $key);
		$query = $this->db->get()->row();
		$data = array(
			'IDIPDetail' => $urutan_id_detail,
			'IDIP' => $last_Id,
			'Barcode' => $query->Barcode,
			'NoSO' => $query->NoSO,
			'Party' => $query->Party,
			'Indent' => $query->Indent,
			'IDBarang' => $query->IDBarang,
			'IDCorak' => $query->IDCorak,
			'IDWarna' => $query->IDWarna,
			'IDMerk' => $query->IDMerk,
			'Qty_yard' => $query->Qty_yard,
			'Qty_meter' => $query->Qty_meter,
			'Saldo_yard' => $query->Qty_yard,
			'Saldo_meter' => $query->Qty_meter,
			'Grade' => $query->Grade,
			'Remark' => $query->Remark,
			'Lebar' => $query->Lebar,
			'IDSatuan' => $query->IDSatuan
		);
		$this->M_instruksi_pengiriman->tambah_instruksi_detail($data);

		$nomorsj= $query->Nomor_sj;
		$data3= array(
			'Saldo_yard' => 0,
			'Saldo_meter' => 0
		);
		$this->M_instruksi_pengiriman->ubah_tampungan($nomorsj, $data3);

		// $data4= array(
		// 	'Saldo_yard' => $query->Saldo_yard,
		// 	'Saldo_meter' => $query->Saldo_meter
		// );
		// $this->M_instruksi_pengiriman->ubah_instruksi_saldo($last_Id, $data4);

	}

	function edit_instruksi($id)
	{
		return $this->db->get_where('tbl_instruksi_pengiriman', array('IDIP' => $id))->row();
	}
	
	function edit_instruksi_tabel($id)
	{
		return $this->db->get_where('tbl_instruksi_pengiriman_detail', array('IDIP' => $id))->result();
	}
	function edit_instruksi_tabel_detail($id)
	{
		$this->db->select("tbl_instruksi_pengiriman_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan")
		->from("tbl_instruksi_pengiriman_detail")
		->join("tbl_corak", "tbl_corak.IDCorak=tbl_instruksi_pengiriman_detail.IDCorak")
		->join("tbl_warna", "tbl_warna.IDWarna=tbl_instruksi_pengiriman_detail.IDWarna")
		->join("tbl_merk", "tbl_merk.IDMerk=tbl_instruksi_pengiriman_detail.IDMerk")
		->join("tbl_satuan", "tbl_satuan.IDSatuan=tbl_instruksi_pengiriman_detail.IDSatuan")
		->where("tbl_instruksi_pengiriman_detail.IDIP", $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	function update($data, $where)
	{
		return $this->db->update('tbl_instruksi_pengiriman', $data, $where);
	}
	function ubah_tampungan($nomorsj, $data3)
	{
		$this->db->where('Nomor_sj', $nomorsj);
		$this->db->update('tbl_tampungan', $data3);
		return TRUE;
	}
	function ubah_instruksi_saldo($last_Id, $data4)
	{
		$this->db->where('IDIP', $last_Id);
		$this->db->update('tbl_instruksi_pengiriman_detail', $data4);
		return TRUE;
	}
	function delete_detail($id)
	{
		$this->db->where('IDIPDetail', $id);
		$this->db->delete('tbl_instruksi_pengiriman_detail');
	}
	function delete_detail_IDIP($id)
	{
		$this->db->where('IDIP', $id);
		$this->db->delete('tbl_instruksi_pengiriman_detail');
	}

	// ---------------------------Fungsi get all procedure
	public function get_all_procedure()
	{

		$query = ('
			SELECT * FROM getip()
			');

		/*$query = ('select * from pengukuran');	*/

		return $this->db->query($query)->result();
	}

	//--------------------------Searching Store Procedure
	public function searching_store($date_from, $date_until, $keyword)
	{

		$new_keywords = '%'.$keyword.'%';

		$query = ('
			SELECT * FROM cari_ip(?, ?, ?)
			');

		/*$query = ('select * from pengukuran');	*/

		return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
	}
	
		//--------------------------Searching Store Procedure
	public function searching_store_like($keyword)
	{

		$new_keywords = '%'.$keyword.'%';

		$query = ('
			SELECT * FROM cari_ip_like(?)
			');

		/*$query = ('select * from pengukuran');	*/

		return $this->db->query($query, array($new_keywords))->result();
	}
	function get_IDPO($po)
	{
		return $this->db->get_where("tbl_purchase_order", array('Nomor' => $po))->row();
	}
	function no_instruksi($tahun)
	{
		return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
		->where('extract(year from "Tanggal") =', $tahun)
		->from("tbl_instruksi_pengiriman")
		->get()->row();
	}
	public function exsport_excel() {
		$this->db->select(array("tbl_instruksi_pengiriman.Tanggal", "tbl_instruksi_pengiriman.Nomor", "tbl_suplier.Nama", "tbl_instruksi_pengiriman.Nomor_sj", "tbl_instruksi_pengiriman_detail.Barcode","tbl_instruksi_pengiriman_detail.NoSO","tbl_instruksi_pengiriman_detail.Party","tbl_barang.Nama_Barang","tbl_corak.Corak", "tbl_warna.Warna", "tbl_merk.Merk", "tbl_instruksi_pengiriman_detail.Saldo_yard", "tbl_instruksi_pengiriman_detail.Saldo_meter", "tbl_instruksi_pengiriman_detail.Grade", "tbl_instruksi_pengiriman_detail.Lebar"));
		$this->db->from('tbl_instruksi_pengiriman');
		$this->db->join('tbl_instruksi_pengiriman_detail', "tbl_instruksi_pengiriman_detail.IDIP=tbl_instruksi_pengiriman.IDIP");
		$this->db->join('tbl_corak', "tbl_instruksi_pengiriman_detail.IDCorak=tbl_corak.IDCorak");
		$this->db->join('tbl_barang', "tbl_instruksi_pengiriman_detail.IDBarang=tbl_barang.IDBarang");
		$this->db->join('tbl_warna', "tbl_instruksi_pengiriman_detail.IDWarna=tbl_warna.IDWarna");
		$this->db->join('tbl_merk', "tbl_instruksi_pengiriman_detail.IDMerk=tbl_merk.IDMerk");
		$this->db->join('tbl_satuan', "tbl_instruksi_pengiriman_detail.IDSatuan=tbl_satuan.IDSatuan");
		$this->db->join('tbl_suplier', "tbl_suplier.IDSupplier=tbl_instruksi_pengiriman.IDSupplier");
		$this->db->where("tbl_instruksi_pengiriman.Batal", 'aktif');
		$this->db->order_by("tbl_instruksi_pengiriman.Nomor", 'ASC');
		$query = $this->db->get();
		return $query->result_array();
	}
}