<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* 
*/
class M_harga_jual_barang extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_harga_jual_barang()
	{
		$this->db->select("*")
		->from("tbl_harga_jual_barang thjb")
		->join("tbl_barang tb", "thjb.IDBarang=tb.IDBarang")
		->join("tbl_satuan ts", "thjb.IDSatuan=ts.IDSatuan")
		->join("tbl_group_customer tgc", "thjb.IDGroupCustomer=tgc.IDGroupCustomer");
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function tampilkan_id_harga_jual_barang()
	{
		$this->db->select_max("IDHargaJual")
		->from("tbl_harga_jual_barang");
		$query = $this->db->get();

		return $query->row();
	}

	function tampil_barang()
	{
		$query=$this->db->query("SELECT * FROM tbl_barang");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function get_corak_merk($barang)
	{
	$this->db->select("*")
    ->from("tbl_barang")
    ->join("tbl_corak", "tbl_barang.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_merk", "tbl_barang.IDMerk=tbl_merk.IDMerk")
    ->where("tbl_barang.IDBarang", $barang);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
	}

	function tampil_satuan()
	{
		$query=$this->db->query("SELECT * FROM tbl_satuan");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampil_group_customer()
	{
		$query=$this->db->query("SELECT * FROM tbl_group_customer");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/*hapus data*/
	function hapus_data_jual_harga_barang($id)
	{
		$this->db->where('IDHargaJual', $id);
		$this->db->delete('tbl_harga_jual_barang');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function tambah_harga_jual_barang($data)
	{
		$this->db->insert('tbl_harga_jual_barang', $data);
		return TRUE;
	}

		function ambil_harga_jual_barang_byid($id)
	{
		return $this->db->get_where('tbl_harga_jual_barang', array('IDHargaJual'=>$id))->row();
	}

	function aksi_edit_harga_jual_barang($id, $data)
	{
		$this->db->where('IDHargaJual', $id);
		$this->db->update('tbl_harga_jual_barang', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		 $this->db->select("*")
    ->from("tbl_harga_jual_barang thjb")
    ->join("tbl_barang tb", "thjb.IDBarang=tb.IDBarang")
    ->join("tbl_satuan ts", "thjb.IDSatuan=ts.IDSatuan")
    ->join("tbl_group_customer tgc", "thjb.IDGroupCustomer=tgc.IDGroupCustomer")
    ->like("$kolom", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
	}


	function cari_by_keyword($keyword){
		 $this->db->select("*")
    ->from("tbl_harga_jual_barang thjb")
    ->join("tbl_barang tb", "thjb.IDBarang=tb.IDBarang")
    ->join("tbl_satuan ts", "thjb.IDSatuan=ts.IDSatuan")
    ->join("tbl_group_customer tgc", "thjb.IDGroupCustomer=tgc.IDGroupCustomer")
    ->like("Nama_Barang", $keyword)
    ->or_like("Kode_Barang", $keyword)
    ->or_like("Satuan", $keyword)
    ->or_like("Kode_Group_Customer", $keyword)
    ->or_like("Nama_Group_Customer", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }

	}

}
?>