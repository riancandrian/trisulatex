<?php

class M_setting_lr extends CI_Model
{

    function tampilkan_id_setting()
    {
        $this->db->select("IDSettingLR")
        ->from("tbl_setting_lr");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_setting_pl()
    {
        $this->db->select("IDSettingPL")
        ->from("tbl_setting_pl");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_setting_cf()
    {
        $this->db->select("IDSettingCF")
        ->from("tbl_setting_cf");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_setting_notes()
    {
        $this->db->select("IDSettingNotes")
        ->from("tbl_setting_notes");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function sum_cari_set_lr()
    {
        $this->db->select_sum("tbl_jurnal.Debet")
        ->from("tbl_jurnal")
        ->join("tbl_setting_lr", "tbl_jurnal.IDCOA=tbl_setting_lr.IDCoa")
        ->where("tbl_setting_lr.Group", '2');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function get_all_coa()
    {
       $query = $this->db->query('SELECT DISTINCT(tbl_jurnal."IDCOA"), tbl_coa."Nama_COA", tbl_coa."Kode_COA" FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa"');
       if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function get_coa_tampil($id)
    {
       return $this->db->select("*")
      ->from("tbl_coa")
      ->where("IDCoa", $id)
      ->get()->row();
    }

    function ambil_byid_sub() {
        $query = $this->db->query('SELECT * FROM tbl_setting_lr ORDER BY tbl_setting_lr."IDSettingLR" ASC');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function ambil_byid_sub_pl() {
        $query = $this->db->query('SELECT * FROM tbl_setting_pl ORDER BY tbl_setting_pl."IDSettingPL" ASC');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function ambil_byid_sub_cf() {
        $query = $this->db->query('SELECT * FROM tbl_setting_cf ORDER BY tbl_setting_cf."IDSettingCF" ASC');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function ambil_byid_sub_notes() {
        $query = $this->db->query('SELECT * FROM tbl_setting_notes ORDER BY tbl_setting_notes."IDSettingNotes" ASC');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function delete()
    {
        $this->db->where('IDUser', $this->session->userdata("id"));
        $this->db->delete('tbl_setting_lr');
    }

    function delete_pl()
    {
        $this->db->where('IDUser', $this->session->userdata("id"));
        $this->db->delete('tbl_setting_pl');
    }

    function delete_cf()
    {
        $this->db->where('IDUser', $this->session->userdata("id"));
        $this->db->delete('tbl_setting_cf');
    }

    function delete_notes()
    {
        $this->db->where('IDUser', $this->session->userdata("id"));
        $this->db->delete('tbl_setting_notes');
    }

    function set_lr()
    {
        $query = ('SELECT * FROM bs()');

        return $this->db->query($query)->result();
    }

    function cari_set_lr($bulan, $tahun)
    {
       $new_keywords = '%'.$bulan.'%';
       $new_keywords2 = '%'.$tahun.'%';

       if($bulan=='1')
       {
        $query = ('
        SELECT * FROM bs()
          ');
       }else{
       $query = ('
        SELECT * FROM bs_cari(?, ?)
          ');
   }
       return $this->db->query($query, array($bulan, $tahun))->result();
   }

   function cari_set_pl($bulan, $tahun)
    {
       $new_keywords = '%'.$bulan.'%';
       $new_keywords2 = '%'.$tahun.'%';

       if($bulan=='1')
       {
        $query = ('
        SELECT * FROM pl()
          ');
       }else{
       $query = ('
        SELECT * FROM pl_cari(?, ?)
          ');
   }
       return $this->db->query($query, array($bulan, $tahun))->result();
   }

   function cari_set_cf($bulan, $tahun)
    {
       $new_keywords = '%'.$bulan.'%';
       $new_keywords2 = '%'.$tahun.'%';
       $query = ('
        SELECT * FROM cf_cari(?, ?)
          ');
       return $this->db->query($query, array($bulan, $tahun))->result();
   }

   function cari_set_notes($bulan)
    {
       $new_keywords = '%'.$bulan.'%';

       if($bulan=='1')
       {
        $query = ('
        SELECT * FROM notes()
          ');
       }else{
       $query = ('
        SELECT * FROM notes_cari(?)
          ');
   }
       return $this->db->query($query, array($bulan))->result();
   }

   function cari_set_aging_schedule($bulan)
    {
       $new_keywords = '%'.$bulan.'%';

       if($bulan=='1')
       {
        $query = ('
        SELECT * FROM ar()
          ');
       }else{
       $query = ('
        SELECT * FROM ar_cari(?)
          ');
   }
       return $this->db->query($query, array($bulan))->result();
   }

   function cari_set_aging_schedule_tanggal($bulan)
    {
       $new_keywords = '%'.$bulan.'%';

       if($bulan=='1')
       {
        $query = ('
        SELECT * FROM ar2()
          ');
       }else{
       $query = ('
        SELECT * FROM ar_cari2(?)
          ');
   }
       return $this->db->query($query, array($bulan))->result();
   }

   function cari_set_mutasi_utang_usaha($bulan)
    {
       $new_keywords = '%'.$bulan.'%';

       if($bulan=='1')
       {
        $query = ('
        SELECT * FROM ap()
          ');
       }else{
       $query = ('
        SELECT * FROM ap_cari(?)
          ');
   }
       return $this->db->query($query, array($bulan))->result();
   }

   function cari_set_mutasi_utang_usaha_tanggal($bulan)
    {
       $new_keywords = '%'.$bulan.'%';

       if($bulan=='1')
       {
        $query = ('
        SELECT * FROM ap2()
          ');
       }else{
       $query = ('
        SELECT * FROM ap_cari2(?)
          ');
   }
       return $this->db->query($query, array($bulan))->result();
   }

   function cari_set_buku_besar_pembantu($bulan)
    {
       $new_keywords = '%'.$bulan.'%';

       $query = ('
        SELECT * FROM buku_besar_pembantu_cari(?)
          ');
       return $this->db->query($query, array($bulan))->result();
   }

   function cari_set_buku_besar($coa, $bulan)
    {
       $new_keywords = '%'.$coa.'%';
       $new_keywords = '%'.$bulan.'%';

       if($bulan=='1')
       {
        $query = ('
        SELECT * FROM buku_besar(?, ?)
          ');
       }else{
       $query = ('
        SELECT * FROM buku_besar_cari(?, ?)
          ');
   }
       return $this->db->query($query, array($coa, $bulan))->result();
   }

   function cari_set_mutasi_persediaan($tanggalmulai, $tanggalselesai)
    {
       $new_keywords = '%'.$tanggalmulai.'%';
       $new_keywords = '%'.$tanggalselesai.'%';

       $query = ('
        SELECT * FROM stock(?, ?)
          ');
       return $this->db->query($query, array($tanggalmulai, $tanggalselesai))->result();
   }

   function setting_lr2()
   {
    $query = $this->db->query('SELECT * FROM tbl_setting_lr ORDER BY tbl_setting_lr."IDSettingLR" ASC');
    if ($query->num_rows() > 0) {
        return $query->result();
    }
}

function setting_pl2()
{
    $query = $this->db->query('SELECT * FROM tbl_setting_pl ORDER BY tbl_setting_pl."IDSettingPL" ASC');
    if ($query->num_rows() > 0) {
        return $query->result();
    }
}

function setting_cf2()
{
    $query = $this->db->query('SELECT * FROM tbl_setting_cf ORDER BY tbl_setting_cf."IDSettingCF" ASC');
    if ($query->num_rows() > 0) {
        return $query->result();
    }
}

function setting_notes2()
{
    $query = $this->db->query('SELECT * FROM tbl_setting_notes ORDER BY tbl_setting_notes."IDSettingNotes" ASC');
    if ($query->num_rows() > 0) {
        return $query->result();
    }
}

function group_asset()
{
    return $this->db->get('tbl_group_asset')->result();
}

function simpan($data)
{
    $this->db->insert('tbl_setting_lr', $data);
    return TRUE;
}

function simpan_pl($data)
{
    $this->db->insert('tbl_setting_pl', $data);
    return TRUE;
}

function simpan_cf($data)
{
    $this->db->insert('tbl_setting_cf', $data);
    return TRUE;
}

function simpan_notes($data)
{
    $this->db->insert('tbl_setting_notes', $data);
    return TRUE;
}

function getById($id)
{
    return $this->db->get_where('tbl_asset', array('IDAsset' => $id))->row();
}

function update($data, $where)
{
    return $this->db->update('tbl_asset', $data, $where);
}

function cari($kolom,$status,$keyword){
    $this->db->select("a.*, ga.Group_Asset")
    ->from("tbl_asset a")
    ->join("tbl_group_asset ga", "a.IDGroupAsset=ga.IDGroupAsset")
    ->like("a.Aktif", $status)
    ->like("$kolom", $keyword);
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
        return $query->result();
    }
}

function cari_by_kolom($kolom,$keyword){
 $this->db->select("a.*, ga.Group_Asset")
 ->from("tbl_asset a")
 ->join("tbl_group_asset ga", "a.IDGroupAsset=ga.IDGroupAsset")
 ->like("$kolom", $keyword);
 $query = $this->db->get();
 if($query->num_rows()>0)
 {
    return $query->result();
}
}

function cari_by_status($status){
 $this->db->select("a.*, ga.Group_Asset")
 ->from("tbl_asset a")
 ->join("tbl_group_asset ga", "a.IDGroupAsset=ga.IDGroupAsset")
 ->where("a.Aktif", $status);
 $query = $this->db->get();
 if($query->num_rows()>0)
 {
    return $query->result();
}
}

function cari_by_keyword($keyword){
   $this->db->select("a.*, ga.Group_Asset")
   ->from("tbl_asset a")
   ->join("tbl_group_asset ga", "a.IDGroupAsset=ga.IDGroupAsset")
   ->like("ga.Group_Asset", $keyword)
   ->or_like("a.Kode_Asset", $keyword)
   ->or_like("a.Nama_Asset", $keyword)
   ->or_like("a.Aktif", $keyword);
   $query = $this->db->get();
   if($query->num_rows()>0)
   {
    return $query->result();
}
}
}