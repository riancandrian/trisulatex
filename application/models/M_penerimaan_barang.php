<?php

class M_penerimaan_barang extends CI_Model
{
    private $table1 = 'tbl_terima_barang_supplier';
    private $table2 = 'tbl_terima_barang_supplier_detail';
    private $table3 = 'tbl_corak';
    private $table4 = 'tbl_tampungan';
    private $table5 = 'tbl_in';
    private $table6 = 'tbl_stok';
    private $table7 = 'tbl_kartu_stok';
    private $table8 = 'tbl_merk';
    private $table9 = 'tbl_warna';
    private $table10 = 'tbl_satuan';
    private $table11 = 'tbl_suplier';
    private $table12 = 'tbl_barang';
    private $table13 = 'tbl_gudang';
    private $table14 = 'tbl_purchase_order';
    private $table15 = 'tbl_purchase_order_detail';
    private $table16 = 'tbl_jurnal';

    function all()
    {
        $this->db->distinct();
        $this->db->select($this->table1 . '.*,' . $this->table3 . '.Corak')
        ->from($this->table1)
        ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
        ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
        ->where($this->table1 . '.Jenis_TBS', 'PB');

        $query = $this->db->order_by($this->table1 . '.IDTBS', 'DESC')->get();
        return $query->result();
    }

    function tampilkan_get_invoice($id)
    {
       return $this->db->select("IDTBS")
       ->from("tbl_pembelian")
       ->where("IDTBS", $id)
       ->get()->row();
   }

   function getCodeReception($tahun)
   {
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
    ->where('extract(year from "Tanggal") =', $tahun)
    ->from("tbl_terima_barang_supplier")
    ->get()->row();
}

function check_SJC($no_sjc)
{
    $this->db->select('
        DISTINCT(tbl_tampungan."Barcode"),
        tbl_tampungan."Corak", tbl_tampungan."Warna", tbl_tampungan."Remark", tbl_tampungan."Qty_yard", tbl_tampungan."Qty_meter",
        tbl_tampungan."Grade", tbl_tampungan."Party", tbl_tampungan."Indent", tbl_tampungan."Lebar", tbl_tampungan."Satuan", tbl_tampungan."NoPO", tbl_tampungan."NoSO", tbl_tampungan."Harga", tbl_tampungan."Total_harga",
        tbl_merk."Merk"
        ')
    ->from("tbl_tampungan")
    ->join("tbl_purchase_order", "tbl_tampungan.NoPO=tbl_purchase_order.Nomor")
    ->join("tbl_warna", "tbl_warna.Warna=tbl_tampungan.Warna")
    ->join("tbl_corak", "tbl_tampungan.Corak=tbl_corak.Corak")
    ->join("tbl_merk", "tbl_corak.IDMerk=tbl_merk.IDMerk")
    ->where("tbl_tampungan.Nomor_sj", $no_sjc);
    $query = $this->db->get();

    return $query->result();
}

public function cek_ketersediaan_sjc()
{
  $query= $this->db->query('SELECT "Nomor_sj" from tbl_tampungan where "Nomor_sj" NOT IN (SELECT "Nomor_sj" FROM tbl_terima_barang_supplier) GROUP BY "Nomor_sj"');
  if ($query->num_rows() > 0)
  {
    return $query->result();
}
}

function tampilkan_id_terima_barang()
{
    $this->db->select_max("IDTBS")
    ->from("tbl_terima_barang_supplier");
    $query = $this->db->get();

    return $query->row();
}

function tampilkan_id_jurnal()
{
    $this->db->select_max("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_terima_barang_detail()
{
    $this->db->select_max("IDTBSDetail")
    ->from("tbl_terima_barang_supplier_detail");
     $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_stok()
{
    $this->db->select_max("IDStok")
    ->from("tbl_stok");
    $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_kartu_stok()
{
    $this->db->select_max("IDKartuStok")
    ->from("tbl_kartu_stok");
     $query = $this->db->get();

  return $query->row();
}

function get_nosjc()
{
 $this->db->select("Nomor_sj")
 ->from("tbl_tampungan")
 ->group_by("Nomor_sj");
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
    return $query->result();
}
}

function save($data)
{
    $this->db->insert($this->table1, $data);
    return TRUE;
}

function save_detail($data)
{
    $this->db->insert($this->table2, $data);
    return TRUE;
}

function save_stock($data)
{
    $this->db->insert($this->table6, $data);
    return TRUE;
}

function save_stock_card($data)
{
    $this->db->insert($this->table7, $data);
    return TRUE;
}

function save_jurnal($data)
{
    $this->db->insert($this->table16, $data);
    return TRUE;
}

function find($id)
{
    return $this->db->select($this->table1 . '.*,' . $this->table11 . '.Nama,' . $this->table14 . '.Nomor AS nomorPO')
    ->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
    ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
    ->join($this->table11, $this->table1 . '.IDSupplier = ' . $this->table11 . '.IDSupplier')
    ->join($this->table14, $this->table14 . '.IDPO = ' . $this->table1 . '.IDPO')
    ->where($this->table1 . '.IDTBS', $id)
    ->where($this->table1 . '.Jenis_TBS', 'PB')
    ->get()->row();
}

function findDetail($id)
{
        //return $this->db->get_where($this->table2, array('IDTBS' => $id))->result();
    $query = $this->db->select(
        $this->table2 . '.*,' .
        $this->table3 . '.Corak,' .
        $this->table3 . '.IDCorak,' .
        $this->table8 . '.IDMerk,' .
        $this->table8 . '.Merk,' .
        $this->table9 . '.Warna,' .
        $this->table9 . '.IDWarna,' .
        $this->table10 . '.Satuan,' .
        $this->table10 . '.IDSatuan')
    ->from($this->table2)
    ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
    ->join($this->table9, $this->table2 . '.IDWarna = ' . $this->table9 . '.IDWarna')
    ->join($this->table10, $this->table2 . '.IDSatuan = ' . $this->table10 . '.IDSatuan')
    ->join($this->table8, $this->table2 . '.IDMerk = ' . $this->table8 . '.IDMerk')
    ->where($this->table2 . '.IDTBS', $id)->get();
    return $query->result();
}

function soft_delete($id)
{
    return $this->db->update($this->table1, array('Batal' => 'Tidak Aktif'), array('IDTBS' => $id));
}

function soft_success($id)
{
    return $this->db->update($this->table1, array('Batal' => 'Aktif'), array('IDTBS' => $id));
}

function update($data, $id)
{
    return $this->db->update($this->table1, $data, array('IDTBS' => $id));
}

function update_detail($data, $id, $barcode)
{
    return $this->db->update($this->table2, $data, array('IDTBS' => $id, 'Barcode' => $barcode));
}

public function drop($idDetail, $id){

    $detail = $this->db->select('Qty_yard, Qty_meter')->from($this->table2)->where('IDTBSDetail', $idDetail)->get()->row();

    $main = $this->db->select('Total_qty_yard, Total_qty_meter, Saldo_yard, Saldo_meter')->from($this->table1)->where('IDTBS', $id)->get()->row();

    $Update_totalYard = ($main->Total_qty_yard - $detail->Qty_yard);
    $Update_totalMeter = ($main->Total_qty_meter - $detail->Qty_meter);

    $data = array(
        'Total_qty_yard' => $Update_totalYard,
        'Total_qty_meter' => $Update_totalMeter,
        'Saldo_yard' => $Update_totalYard,
        'Saldo_meter' => $Update_totalMeter
    );
    $this->db->update($this->table1, $data, array('IDTBS' => $id));

    $this->db->where('IDTBSDetail', $idDetail);
    $this->db->delete($this->table2);
    return TRUE;
}

function bulk($where)
{
    $this->db->where_in('Nomor', $where);
    return $this->db->update($this->table1, array('Batal' => 'Tidak Aktif'));
}

function searching($date_from, $date_until, $search_type, $keyword)
{
    $this->db->distinct()
    ->select($this->table1 . '.*,' . $this->table3 . '.Corak')
    ->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
    ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
    ->where('Jenis_TBS', 'PB');

    if ($date_from) {
        $this->db->where("'".$date_from."'"  . " <= tbl_terima_barang_supplier.Tanggal ");
    }

    if ($date_until) {
        $this->db->where("'".$date_until."'" ." >= tbl_terima_barang_supplier.Tanggal ");
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }

    $query = $this->db->order_by('IDTBS', 'DESC')->get();
    return $query->result();
}

function get_IDcorak($corak)
{
        //return $this->db->get_where($this->table3, array('Corak' => $corak))->row();
    return $this->db->select('*')->from($this->table3)->where('LOWER("Corak") = '."LOWER('$corak')")->get()->row();
}

function get_IDwarna($warna)
{
        //return $this->db->get_where($this->table9, array('Warna' => $warna))->row();
    return $this->db->select('*')->from($this->table9)->where('LOWER("Warna") = '."LOWER('$warna')")->get()->row();
}

function get_IDsatuan($satuan)
{
        //return $this->db->get_where($this->table10, array('Satuan' => $satuan))->row();
    return $this->db->select('*')->from($this->table10)->where('LOWER("Satuan") = '."LOWER('$satuan')")->get()->row();
}

function get_IDPO($noPO)
{
        // return $this->db->select('*')->from($this->table14)->where("Nomor"=>$nopo)->get()->row();
    return $this->db->get_where($this->table14, array('Nomor' => $noPO))->row();
}

function get_IDbarang($corak)
{
    $this->db->select('IDBarang')->from($this->table12)
    ->join($this->table3, $this->table12 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            //->where($this->table3 . '.Corak', $corak);
    ->where('LOWER("Corak") = '."LOWER('$corak')");
    return $this->db->get()->row();
}

function get_IDgudang()
{
    $this->db->select('IDGudang')->from($this->table13)
    ->where("(Nama_Gudang = 'PT. TRISULA' OR Nama_Gudang = 'PT. Trisula')");
    return $this->db->get()->row();
}

function check_noPB($no_pb)
{
    return $this->db->get_where($this->table1, array('Nomor' => $no_pb));
}

function getPrint_edit($sjc)
{
    return $this->db->select($this->table1 . '.*,' . $this->table11 . '.Nama')
    ->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
    ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
    ->join($this->table11, $this->table1 . '.IDSupplier = ' . $this->table11 . '.IDSupplier')
    ->where($this->table1 . '.IDTBS', $sjc)
    ->where($this->table1 . '.Jenis_TBS', 'PB')
    ->get()->row();
}

function getPrint()
{
    return $this->db->select($this->table1 . '.*,' . $this->table11 . '.Nama')
    ->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
    ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
    ->join($this->table11, $this->table1 . '.IDSupplier = ' . $this->table11 . '.IDSupplier')
    ->where($this->table1 . '.Jenis_TBS', 'PB')
    ->order_by($this->table1.'.IDTBS', 'DESC')
    ->limit(1)
    ->get()->row();
}

function getDetail($sjc)
{
        //return $this->db->get_where($this->table2, array('IDTBS' => $id))->result();
    $query = $this->db->select(
        $this->table2 . '.*,' .
        $this->table3 . '.Corak,' .
        $this->table3 . '.IDCorak,' .
        $this->table9 . '.Warna,' .
        $this->table9 . '.IDWarna,' .
        $this->table10 . '.Satuan,' .
        $this->table10 . '.IDSatuan')
    ->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
    ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
    ->join($this->table9, $this->table2 . '.IDWarna = ' . $this->table9 . '.IDWarna')
    ->join($this->table10, $this->table2 . '.IDSatuan = ' . $this->table10 . '.IDSatuan')
    ->where($this->table1 . '.Nomor_sj', $sjc)->get();
    return $query->result();
}

public function get_by_id_detail($id)
{
    $this->db->select("tbl_terima_barang_supplier_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan")
    ->from("tbl_terima_barang_supplier_detail")
    ->join("tbl_corak", "tbl_terima_barang_supplier_detail.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_terima_barang_supplier_detail.IDWarna=tbl_warna.IDWarna")
    ->join("tbl_merk", "tbl_terima_barang_supplier_detail.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_satuan", "tbl_terima_barang_supplier_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("IDTBS", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
  }


}

public function getDetail_tbs($id){
    $this->db->select("tbl_terima_barang_supplier_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan, tbl_barang.Nama_Barang")
    ->from("tbl_terima_barang_supplier_detail")
    ->join("tbl_corak", "tbl_terima_barang_supplier_detail.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_barang", "tbl_barang.IDBarang=tbl_terima_barang_supplier_detail.IDBarang")
    ->join("tbl_warna", "tbl_terima_barang_supplier_detail.IDWarna=tbl_warna.IDWarna")
    ->join("tbl_merk", "tbl_terima_barang_supplier_detail.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_satuan", "tbl_terima_barang_supplier_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("IDTBS", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
  }

}
}