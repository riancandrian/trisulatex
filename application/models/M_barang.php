<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_barang extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_barang()
	{
		// $query=$this->db->query("SELECT tgb.Group_Barang, b.Kode_Barang, b.Nama_Barang, b.Aktif, b.IDBarang FROM tbl_barang b join tbl_group_barang tgb on b.IDGroupBarang=tgb.IDGroupBarang join tbl_merk m on b.IDMerk=m.IDMerk join tbl_satuan s on b.IDSatuan=s.IDSatuan join tbl_corak c on b.IDCorak=c.IDCorak");

		$this->db->select("tgb.Group_Barang, b.Kode_Barang, b.Nama_Barang, m.Merk, c.Corak, b.Aktif, b.IDBarang")
		->from("tbl_barang b")
		->join("tbl_group_barang tgb", "b.IDGroupBarang=tgb.IDGroupBarang")
		->join("tbl_merk m", "b.IDMerk=m.IDMerk")
		->join("tbl_satuan s", "b.IDSatuan=s.IDSatuan")
		->join("tbl_corak c", "b.IDCorak=c.IDCorak");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function get_po($id)
	{
		return $this->db->select("IDBarang")
		->from("tbl_purchase_order")
		->where("IDBarang", $id)
		->get()->row();
	}

	function get_po_umum($id)
	{
		return $this->db->select("IDBarang")
		->from("tbl_purchase_order_umum_detail")
		->where("IDBarang", $id)
		->get()->row();
	}

	function tampilkan_urutan_barang()
	{

		$this->db->select_max("IDBarang")
		->from("tbl_barang");
		$query = $this->db->get();

		return $query->row();
	}

	function tampil_group_barang()
	{
		$query=$this->db->query("SELECT * FROM tbl_group_barang");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampil_merk()
	{
		$query=$this->db->query("SELECT * FROM tbl_merk");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampil_satuan()
	{
		$query=$this->db->query("SELECT * FROM tbl_satuan");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampil_corak()
	{
		$query=$this->db->query("SELECT * FROM tbl_corak");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/*hapus data*/
	function hapus_data_barang($id)
	{
		$this->db->where('IDBarang', $id);
		$this->db->delete('tbl_barang');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function tambah_barang($data)
	{
		$this->db->insert('tbl_barang', $data);
		return TRUE;
	}

	function ambil_barang_byid($id)
	{
		return $this->db->get_where('tbl_barang', array('IDBarang'=>$id))->row();
	}

	function aksi_edit_barang($id, $data)
	{
		$this->db->where('IDBarang', $id);
		$this->db->update('tbl_barang', $data);
		return TRUE;
	}
	function cari($kolom,$status,$keyword){
		$this->db->select("tgb.Group_Barang, b.Kode_Barang, b.Nama_Barang, b.Aktif, b.IDBarang")
		->from("tbl_barang b")
		->join("tbl_group_barang tgb", "b.IDGroupBarang=tgb.IDGroupBarang")
		->join("tbl_merk m", "b.IDMerk=m.IDMerk")
		->join("tbl_satuan s", "b.IDSatuan=s.IDSatuan")
		->join("tbl_corak c", "b.IDCorak=c.IDCorak")
		->like("b.Aktif", $status)
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_kolom($kolom,$keyword){
		$this->db->select("tgb.Group_Barang, b.Kode_Barang, b.Nama_Barang, b.Aktif, b.IDBarang")
		->from("tbl_barang b")
		->join("tbl_group_barang tgb", "b.IDGroupBarang=tgb.IDGroupBarang")
		->join("tbl_merk m", "b.IDMerk=m.IDMerk")
		->join("tbl_satuan s", "b.IDSatuan=s.IDSatuan")
		->join("tbl_corak c", "b.IDCorak=c.IDCorak")
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_status($status){
		$this->db->select("tgb.Group_Barang, b.Kode_Barang, b.Nama_Barang, b.Aktif, b.IDBarang")
		->from("tbl_barang b")
		->join("tbl_group_barang tgb", "b.IDGroupBarang=tgb.IDGroupBarang")
		->join("tbl_merk m", "b.IDMerk=m.IDMerk")
		->join("tbl_satuan s", "b.IDSatuan=s.IDSatuan")
		->join("tbl_corak c", "b.IDCorak=c.IDCorak")
		->where("b.Aktif", $status);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("tgb.Group_Barang, b.Kode_Barang, b.Nama_Barang, b.Aktif, b.IDBarang")
		->from("tbl_barang b")
		->join("tbl_group_barang tgb", "b.IDGroupBarang=tgb.IDGroupBarang")
		->join("tbl_merk m", "b.IDMerk=m.IDMerk")
		->join("tbl_satuan s", "b.IDSatuan=s.IDSatuan")
		->join("tbl_corak c", "b.IDCorak=c.IDCorak")
		->like("tgb.Group_Barang", $keyword)
		->or_like("b.Kode_Barang", $keyword)
		->or_like("b.Nama_Barang", $keyword)
		->or_like("m.Merk", $keyword)
		->or_like("s.Satuan", $keyword)
		->or_like("c.Corak", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function getDetailId($id)
	{
		return $this->db->select('*')
		->from('tbl_barang')
		->join('tbl_merk', 'tbl_merk.IDMerk = tbl_barang.IDMerk')
		->join('tbl_satuan', 'tbl_satuan.IDSatuan = tbl_barang.IDSatuan')
		->join('tbl_corak', 'tbl_corak.IDCorak = tbl_barang.IDCorak')
		->join('tbl_group_barang', 'tbl_group_barang.IDGroupBarang = tbl_barang.IDGroupBarang')
		->where('tbl_barang.IDBarang', $id)->get()->row();
	}

	function get_merk($id_corak)
	{
		$this->db->select('tbl_merk."Merk", tbl_merk."IDMerk"')
		->from("tbl_corak")
		->join("tbl_merk", "tbl_corak.IDMerk=tbl_merk.IDMerk")
		->where("tbl_corak.IDCorak", $id_corak);
		$query = $this->db->get();

		return $query->result();
	}

}
?>