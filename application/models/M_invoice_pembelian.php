<?php

class M_invoice_pembelian extends CI_Model
{

    function tampilkan_invoice_pembelian()
    {
        $this->db->select("tbl_pembelian.*, tbl_suplier.Nama, tbl_pembelian_grand_total.Grand_total")
        ->from("tbl_pembelian")
        ->join("tbl_suplier", "tbl_pembelian.IDSupplier=tbl_suplier.IDSupplier")
        ->join("tbl_pembelian_grand_total", "tbl_pembelian_grand_total.IDFB=tbl_pembelian.IDFB");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function update_status($data, $where)
    {
        return $this->db->update('tbl_pembelian', $data, $where);
    }
    function tampilkan_get_pembayaran_hutang($id)
    {
     return $this->db->select("IDFB")
     ->from("tbl_pembayaran_hutang_detail")
     ->where("IDFB", $id)
     ->get()->row();
 }

 function find($id)
 {
    return $this->db->select('*')
    ->from("tbl_pembelian")
    ->join("tbl_pembelian_grand_total", "tbl_pembelian_grand_total.IDFB=tbl_pembelian.IDFB")
    ->where("tbl_pembelian". '.IDFB', $id)
    ->get()->row();
}
function total_hargainv($id)
{
    return $this->db->select('SUM("Sub_total") AS total_harga')
     ->from("tbl_pembelian_detail")
     ->where("IDFB", $id)
     ->get()->row();
}
function searching($date_from, $date_until, $search_type, $keyword)
{
    $this->db->select("tbl_pembelian.*, tbl_suplier.Nama")
    ->from("tbl_pembelian")
    ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pembelian.IDSupplier");

    if ($date_from) {
        $this->db->where("Tanggal >= ", $date_from);
    }

    if ($date_until) {
        $this->db->where("Tanggal <= ", $date_until);
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }

    $query = $this->db->order_by('IDFB', 'DESC')->get();
    return $query->result();
}

function ceksupplier($PB)
{
    $this->db->select('tbl_suplier."IDSupplier", tbl_suplier."Nama"')
    ->from("tbl_terima_barang_supplier")
    ->join("tbl_suplier", "tbl_terima_barang_supplier.IDSupplier=tbl_suplier.IDSupplier")
    ->where("tbl_terima_barang_supplier.Nomor", $PB);
    $query = $this->db->get();

    return $query->result();
}

function tampilkan_id_invoice()
{
    $this->db->select_max("IDFB")
    ->from("tbl_pembelian");
     $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_pembayaran()
{
    $this->db->select_max("IDFBPembayaran")
    ->from("tbl_pembayaran");
     $query = $this->db->get();

  return $query->row();
}


function getnomor_pb($penerimaan)
{
    // $this->db->select("*")
    // ->from("tbl_terima_barang_supplier")
    // ->where("Jenis_TBS", $penerimaan);
        // $query = $this->db->get();
        // $query= $this->db->query('SELECT * from tbl_terima_barang_supplier where "Jenis_TBS"="PB" AND "IDTBS" NOT IN (SELECT "IDTBS" FROM tbl_pembelian)');
    $this->db->select('*')->from('tbl_terima_barang_supplier');
    $this->db->where('"IDTBS" NOT IN (SELECT "IDTBS" FROM tbl_pembelian)', NULL, FALSE);
    $this->db->where("Jenis_TBS", $penerimaan);
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function tampilkan_id_invoice_detail()
{
    $this->db->select_max("IDFBDetail")
    ->from("tbl_pembelian_detail");
     $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_invoice_grand_total()
{
    $this->db->select_max("IDFBGrandTotal")
    ->from("tbl_pembelian_grand_total");
     $query = $this->db->get();

  return $query->row();
}


function tampil_Supplier()
{
    return $this->db->get('tbl_suplier')->result();
}
function tampil_bank()
{
    return $this->db->get('tbl_coa')->result();
}
function check_terima_barang($nomor)
{
        // $this->db->distinct();
    $this->db->select("tbl_terima_barang_supplier.*, tbl_terima_barang_supplier_detail.*, tbl_merk.*, tbl_warna.*, tbl_corak.*, tbl_satuan.*, tbl_suplier.Nama, tbl_suplier.IDSupplier")
    ->from("tbl_terima_barang_supplier_detail")
    ->join("tbl_terima_barang_supplier", "tbl_terima_barang_supplier.IDTBS=tbl_terima_barang_supplier_detail.IDTBS")
    ->join("tbl_merk", "tbl_terima_barang_supplier_detail.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_corak", "tbl_terima_barang_supplier_detail.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_terima_barang_supplier_detail.IDWarna=tbl_warna.IDWarna")
    ->join("tbl_satuan", "tbl_terima_barang_supplier_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_terima_barang_supplier.IDSupplier")
    ->where("tbl_terima_barang_supplier.Nomor", $nomor)
    ->where("tbl_terima_barang_supplier.Batal", "Aktif");
    $query = $this->db->get();

    return $query->result();
}

public function add($data){
   $this->db->insert('tbl_pembelian', $data);
   return TRUE;
}
public function add_detail($data){
    $this->db->insert('tbl_pembelian_detail', $data);
    return TRUE;
}
public function add_grand_total($data4){
    $this->db->insert('tbl_pembelian_grand_total', $data4);
    return TRUE;
}
public function add_pembayaran($datas5){
    $this->db->insert('tbl_pembayaran', $datas5);
    return TRUE;
}
function get_IDTBS($Nomor)
{
    return $this->db->get_where("tbl_terima_barang_supplier", array('Nomor' => $Nomor))->row();
}
function get_IDcorak($corak)
{
    return $this->db->get_where("tbl_corak", array('Corak' => $corak))->row();
}

function get_IDwarna($warna)
{
    return $this->db->get_where("tbl_warna", array('Warna' => $warna))->row();
}

function get_IDsatuan($satuan)
{
    return $this->db->get_where("tbl_satuan", array('Satuan' => $satuan))->row();
}

function get_IDbarang($corak)
{
    $this->db->select('IDBarang')->from("tbl_barang")
    ->join("tbl_corak", "tbl_barang.IDCorak = tbl_corak.IDCorak")
    ->where("tbl_corak.Corak", $corak);
    return $this->db->get()->row();
}
function getById($id)
{
        // return $this->db->get_where('tbl_pembelian', array('IDFB' => $id))->row();
    $this->db->select('tbl_pembelian.*, tbl_terima_barang_supplier.Nomor AS NomorTBS, tbl_pembelian_grand_total.*, tbl_pembayaran.*, tbl_coa.Nama_COA')->from("tbl_pembelian")
    ->join("tbl_terima_barang_supplier", "tbl_pembelian.IDTBS = tbl_terima_barang_supplier.IDTBS")
    ->join("tbl_pembelian_grand_total", "tbl_pembelian_grand_total.IDFB = tbl_pembelian.IDFB")
    ->join("tbl_pembayaran", "tbl_pembayaran.IDFB = tbl_pembelian.IDFB")
    ->join("tbl_coa", "tbl_coa.IDCoa = tbl_pembayaran.IDCOA")
    ->where("tbl_pembelian.IDFB", $id);
    return $this->db->get()->row();
}

function getById_edit($id)
{
        // return $this->db->get_where('tbl_pembelian', array('IDFB' => $id))->row();
    $this->db->select('tbl_pembelian.*, tbl_terima_barang_supplier.Nomor AS NomorTBS')->from("tbl_pembelian")
    ->join("tbl_terima_barang_supplier", "tbl_pembelian.IDTBS = tbl_terima_barang_supplier.IDTBS")
    ->where("tbl_pembelian.IDFB", $id);
    return $this->db->get()->row();
}

function cek_group_supplier($id)
{
   $this->db->select('tbl_group_supplier.Group_Supplier')->from("tbl_suplier")
   ->join("tbl_group_supplier", "tbl_group_supplier.IDGroupSupplier = tbl_suplier.IDGroupSupplier")
   ->where("tbl_suplier.IDSupplier", $id);
   return $this->db->get()->row();
}

function cekcoa($id)
{
    $this->db->select('tbl_asset.Coa_Asset')->from("tbl_asset")
    ->join("tbl_terima_barang_supplier_asset_detail", "tbl_terima_barang_supplier_asset_detail.IDAsset=tbl_asset.IDAsset")
    ->join("tbl_terima_barang_supplier_asset", "tbl_terima_barang_supplier_asset.IDTBAsset=tbl_terima_barang_supplier_asset_detail.IDTBAsset")
    ->where("tbl_terima_barang_supplier_asset.Nomor", $id);
    return $this->db->get()->row();
}

function detail_invoice_pembelian($id)
{
 $this->db->select("*")
 ->from("tbl_pembelian")
 ->join("tbl_pembelian_detail", "tbl_pembelian.IDFB=tbl_pembelian_detail.IDFB")
 ->join("tbl_corak", "tbl_pembelian_detail.IDCorak=tbl_corak.IDCorak")
 ->join("tbl_warna", "tbl_pembelian_detail.IDWarna=tbl_warna.IDWarna")
 ->join("tbl_merk", "tbl_pembelian_detail.IDMerk=tbl_merk.IDMerk")
 ->join("tbl_satuan", "tbl_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_pembelian_detail.IDFB", $id);
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
    return $query->result();
}
}

public function update_master($id,$data){
    return $this->db->update('tbl_pembelian', $data, array('IDFB' => $id));
}

public function update_grand_total($id,$datas){
    return $this->db->update('tbl_pembelian_grand_total', $datas, array('IDFB' => $id));
}
public function update_pembayaran($id,$datas5){
    return $this->db->update('tbl_pembayaran', $datas5, array('IDFB' => $id));
}
public function get_by_id_detail($id)
{
    $this->db->select("tbl_pembelian_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan")
    ->from("tbl_pembelian_detail")
    ->join("tbl_corak", "tbl_pembelian_detail.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_pembelian_detail.IDWarna=tbl_warna.IDWarna")
    ->join("tbl_merk", "tbl_pembelian_detail.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_satuan", "tbl_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("IDFB", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
  }


}

function print_invoice($nomor)
{
    $this->db->select('*')->from("tbl_pembelian")
    ->where("IDFB", $nomor);
    return $this->db->get()->row();
}
function print_invoice_detail($id)
{
 $this->db->select("tbl_pembelian_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan, tbl_pembelian.Nomor")
 ->from("tbl_pembelian")
 ->join("tbl_pembelian_detail", "tbl_pembelian.IDFB=tbl_pembelian_detail.IDFB")
 ->join("tbl_corak", "tbl_pembelian_detail.IDCorak=tbl_corak.IDCorak")
 ->join("tbl_warna", "tbl_pembelian_detail.IDWarna=tbl_warna.IDWarna")
 ->join("tbl_merk", "tbl_pembelian_detail.IDMerk=tbl_merk.IDMerk")
 ->join("tbl_satuan", "tbl_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_pembelian_detail.IDFB", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function no_invoice($tahun)
{
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
    ->where('extract(year from "Tanggal") =', $tahun)
    ->from("tbl_pembelian")
    ->get()->row();
}

function tampilkan_id_jurnal()
{
    $this->db->select_max("IDJurnal")
    ->from("tbl_jurnal");
     $query = $this->db->get();

  return $query->row();
}
function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}



}