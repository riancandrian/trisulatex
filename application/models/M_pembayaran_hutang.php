<?php

class M_pembayaran_hutang extends CI_Model
{
    private $table1 = 'tbl_pembayaran_hutang';
    private $table2 = 'tbl_pembayaran_hutang_detail';
    private $table3 = 'tbl_pembayaran_hutang_bayar';
    private $table4 = 'tbl_suplier';
    private $table5 = 'tbl_hutang';

    function all()
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.IDBS,' .
            $this->table1 . '.Tanggal,' .
            $this->table1 . '.Nomor as Nomor_PH,' .
            $this->table1 . '.Total,' .
            $this->table1 . '.Keterangan,' .
            $this->table1 . '.Batal,' .
            $this->table2 . '.Telah_diterima,' .
            $this->table3 . '.*,' .
            $this->table4 . '.Nama'
        )
        ->from($this->table1)
        ->join($this->table2, $this->table1 . '.IDBS = ' . $this->table2 . '.IDBS')
        ->join($this->table3, $this->table1 . '.IDBS = ' . $this->table3 . '.IDBS')
        ->join($this->table4, $this->table1 . '.IDSupplier = ' . $this->table4 . '.IDSupplier');
            //->join($this->table5, $this->table4 . '.IDSupplier = ' . $this->table5 . '.IDSupplier')
            //->where($this->table5 . '.Saldo_Akhir', 0);

        return $this->db->get()->result();
    }

    function tampil_bank()
    {
    }

    function tampilkan_umsupplier()
    {
        $this->db->select("IDUMSupplier")->from("tbl_um_supplier");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function getcoa_transfer($jenis)
    {
       $this->db->select("*")
       ->from("tbl_coa")
       ->where('substring("Nama_COA" from 1 for 4)=', 'Bank');
       $query= $this->db->get();
       if($query->num_rows()>0)
       {
          return $query->result();
      }
  }

  function getcoa_giro($jenis)
  {
    $this->db->select("*")->from("tbl_coa")
    ->where("IDCoa", 'P000624');
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function getcoa_cash($jenis)
  {
    $this->db->select("*")
       ->from("tbl_coa")
       ->where('substring("Nama_COA" from 1 for 3)=', 'Kas');
       $query= $this->db->get();
       if($query->num_rows()>0)
       {
          return $query->result();
      }
}

function getcoa_pembulatan($jenis)
  {
    $this->db->select("*")
       ->from("tbl_coa");
       $query= $this->db->get();
       if($query->num_rows()>0)
       {
          return $query->result();
      }
}

function getcoa_nota_kredit($supplier)
  {
    $this->db->select_sum("Nominal")->from("tbl_um_supplier")
    ->where("IDSupplier", $supplier);
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

public function add_um_supplier($dataumsupplier){
    $this->db->insert("tbl_um_supplier", $dataumsupplier);
    return TRUE;
}

function tampil_supplier()
{
    $query = $this->db->select(
        $this->table4 . '.IDSupplier,' .
        $this->table4 . '.Nama'
    )
    ->from($this->table4)
    ->join($this->table5, $this->table4 . '.IDSupplier = ' . $this->table5 . '.IDSupplier')
    ->where($this->table5 . '.Saldo_Akhir !=', 0)
    ->group_by($this->table4 . '.IDSupplier,' .
        $this->table4 . '.Nama')
    ->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function searching_laporan_pembayaran_hutang($date_from, $date_until, $search_type, $keyword)
{


  $this->db->join("tbl_pembayaran_hutang_detail", "tbl_pembayaran_hutang_detail.IDBS=tbl_pembayaran_hutang.IDBS");
  $this->db->join("tbl_pembayaran_hutang_bayar", "tbl_pembayaran_hutang_bayar.IDBS=tbl_pembayaran_hutang.IDBS");
  $this->db->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pembayaran_hutang.IDSupplier");
  $this->db->join("tbl_hutang", "tbl_hutang.IDHutang=tbl_pembayaran_hutang_detail.IDHutang");
  $this->db->join("tbl_coa", "tbl_coa.IDCoa=tbl_pembayaran_hutang_bayar.IDCOA");

  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
}

if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
}

if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
}

$this->db->where("tbl_hutang.Saldo_Akhir !=", 0);

$query = $this->db->order_by("tbl_pembayaran_hutang.IDBS", 'DESC')->get("tbl_pembayaran_hutang");
return $query->result();
}

function last_code($tahun)
{
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
    ->where('extract(year from "Tanggal") =', $tahun)
    ->from($this->table1)
    ->get()->row();
}

function check_hutang($IDSupplier)
{
    $this->db->select('*')
    ->from("tbl_hutang")
    ->join("tbl_pembelian", "tbl_pembelian.IDFB=tbl_hutang.IDFaktur")
    ->where('tbl_hutang.IDSupplier', $IDSupplier)
    ->where('tbl_hutang.Saldo_Akhir !=', 0)
    ->where('tbl_pembelian.Batal', 'aktif');

    $query = $this->db->get();

    return $query->result();
}

function tampil_coa_semua()
{
    return $this->db->get('tbl_coa')->result();
}

function tampilkan_id_hutang()
{
    $this->db->select("IDBS")->from($this->table1);
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function tampilkan_id_hutang_detail()
{
    $this->db->select("IDBSDet")->from($this->table2);
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function tampilkan_id_hutang_bayar()
{
    $this->db->select("IDBSBayar")->from($this->table3);
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

public function add($data){
    $this->db->insert($this->table1, $data);
    return TRUE;
}

public function add_detail($data){
    $this->db->insert($this->table2, $data);
    return TRUE;
}

public function add_bayar($data){
    $this->db->insert($this->table3, $data);
    return TRUE;
}

function get_saldo_akhir($nomor)
{
   return $this->db->get_where("tbl_hutang", array('No_Faktur' => $nomor))->row();
}

public function update_data_hutang($id,$data){
  return $this->db->update('tbl_hutang', $data, array('No_Faktur' => $id));
}

public function find($id, $nomor)
{
    $this->db->select(
        $this->table1 . '.*,' .
        $this->table4 . '.Nama as Nama_supplier'
    );
    $this->db->from($this->table1)
            //->join($this->table2, $this->table1 . '.IDBS = ' . $this->table2 . '.IDBS')
            //->join($this->table3, $this->table1 . '.IDBS = ' . $this->table3 . '.IDBS')
    ->join($this->table4, $this->table1 . '.IDSupplier = ' . $this->table4 . '.IDSupplier');

    if ($id != '_') {
        $this->db->where($this->table1 . '.IDBS', $id);
    } else {
        $this->db->where($this->table1 . '.Nomor', $nomor);
    }

    return $this->db->get()->row();
}

public function find_detail($id, $nomor)
{
    $this->db->distinct();
    $this->db->select(
        $this->table2 . '.*'
    );
    $this->db->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDBS = ' . $this->table2 . '.IDBS');
            //->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak')
            //->join($this->table6, $this->table2 . '.IDBarang = ' . $this->table6 . '.IDBarang')
            //->join($this->table7, $this->table2 . '.IDWarna = ' . $this->table7 . '.IDWarna');

    if ($id != '_') {
        $this->db->where($this->table1 . '.IDBS', $id);
    } else {
        $this->db->where($this->table1 . '.Nomor', $nomor);
    }

    return $this->db->get()->result();
}

function find_bayar($id)
{
    $this->db->select('*');
    $this->db->from($this->table3);
    $this->db->where($this->table3 . '.IDBS', $id);

    return $this->db->get()->row();
}

function update_status($data, $where)
{
    return $this->db->update($this->table1, $data, $where);
}

function searching($date_from, $date_until, $search_type, $keyword)
{
    $this->db->distinct();
    $this->db->select(
        $this->table1 . '.IDBS,' .
        $this->table1 . '.Tanggal,' .
        $this->table1 . '.Nomor as Nomor_PH,' .
        $this->table1 . '.Total,' .
        $this->table1 . '.Pembayaran,' .
        $this->table1 . '.Keterangan,' .
        $this->table1 . '.Batal,' .
        $this->table3 . '.*,' .
        $this->table4 . '.Nama'
    )
    ->from($this->table1)
    ->join($this->table3, $this->table1 . '.IDBS = ' . $this->table3 . '.IDBS')
    ->join($this->table4, $this->table1 . '.IDSupplier = ' . $this->table4 . '.IDSupplier');

    if ($date_from) {
        $this->db->where("Tanggal >= ", $date_from);
    }

    if ($date_until) {
        $this->db->where("Tanggal <= ", $date_until);
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }

    $query = $this->db->order_by($this->table1 . '.IDBS', 'DESC')->get();
    return $query->result();
}

function hutang_bayar($IDSupplier)
{
    $this->db->select('*')
    ->from($this->table5)
    ->join($this->table1, $this->table5 . '.IDSupplier = ' . $this->table1 . '.IDSupplier')
    ->where($this->table5 . '.IDSupplier', $IDSupplier)
    ->where($this->table5 . '.Saldo_Akhir', 0);

    $query = $this->db->get();

    return $query->result();
}

public function update_master($id,$data){
    return $this->db->update($this->table1, $data, array('IDBS' => $id));
}

public function update_bayar($id,$datas){
    return $this->db->update($this->table3, $datas, array('IDBS' => $id));
}

public function get_pembelian($nomor)
{
    return $this->db->get_where('tbl_hutang', array('No_Faktur' => $nomor))->row();
}
function tampilkan_id_jurnal()
{
    $this->db->select("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}
function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}
}