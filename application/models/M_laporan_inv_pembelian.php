<?php

class M_laporan_inv_pembelian extends CI_Model
{
    function all()
    {
        /*$this->db->select(
            "tbl_pembelian.Tanggal," .
            "tbl_pembelian.Nomor," .
            "tbl_suplier.Nama," .
            "tbl_pembelian.Tanggal_jatuh_tempo," .
            "tbl_pembelian_grand_total.Grand_total," .
            "tbl_pembelian_grand_total.Sisa"
        )
            ->from("tbl_pembelian")
            ->join("tbl_pembelian_detail", "tbl_pembelian.IDFB = tbl_pembelian_detail.IDFB")
            ->join("tbl_suplier", "tbl_pembelian.IDSupplier = tbl_suplier.IDSupplier")
            ->join("tbl_pembelian_grand_total", "tbl_pembelian.IDFB = tbl_pembelian_grand_total.IDFB");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }*/
        $query = ('SELECT * FROM laporan_inv_pembelian()');

        return $this->db->query($query)->result();
    }

   public function searching_store($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_inv_pembelian(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_inv_pembelian_like(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }

  function all_umum()
    {
        $query = ('SELECT * FROM laporan_invoice_pembelian_umum()');

        return $this->db->query($query)->result();
    }

   public function searching_store_umum($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_invoice_pembelian_umum(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like_umum($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_invoice_pembelian_umum_like(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }

    public function exsport_excel() {
        $this->db->select(array("tbl_pembelian.Tanggal", "tbl_pembelian.Nomor", "tbl_suplier.Nama", "tbl_pembelian.No_sj_supplier","tbl_pembelian.Status_ppn","tbl_pembelian_detail.Barcode","tbl_pembelian_detail.NoSO","tbl_pembelian_detail.Party","tbl_barang.Nama_Barang","tbl_corak.Corak", "tbl_warna.Warna", "tbl_merk.Merk", "tbl_pembelian_detail.Saldo_yard", "tbl_pembelian_detail.Saldo_meter", "tbl_pembelian_detail.Grade", "tbl_pembelian_detail.Lebar", "tbl_satuan.Satuan","tbl_pembelian_detail.Harga", "tbl_pembelian_detail.Sub_total", "tbl_pembelian.Tanggal_jatuh_tempo"));
        $this->db->from('tbl_pembelian');
        $this->db->join('tbl_pembelian_detail', "tbl_pembelian.IDFB=tbl_pembelian_detail.IDFB");
        $this->db->join('tbl_corak', "tbl_pembelian_detail.IDCorak=tbl_corak.IDCorak");
         $this->db->join('tbl_barang', "tbl_pembelian_detail.IDBarang=tbl_barang.IDBarang");
        $this->db->join('tbl_warna', "tbl_pembelian_detail.IDWarna=tbl_warna.IDWarna");
        $this->db->join('tbl_merk', "tbl_pembelian_detail.IDMerk=tbl_merk.IDMerk");
        $this->db->join('tbl_satuan', "tbl_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan");
        $this->db->join('tbl_suplier', "tbl_suplier.IDSupplier=tbl_pembelian.IDSupplier");
        $this->db->where("tbl_pembelian.Batal", 'aktif');
        $this->db->order_by("tbl_pembelian.Nomor", 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
}