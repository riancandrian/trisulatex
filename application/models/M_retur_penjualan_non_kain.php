<?php

class M_retur_penjualan_non_kain extends CI_Model
{
    private $table1 = 'tbl_retur_penjualan_seragam';
    private $table2 = 'tbl_retur_penjualan_detail';
    private $table3 = 'tbl_retur_penjualan_seragam_grand_total';
    private $table4 = 'tbl_suplier';
    private $table5 = 'tbl_corak';
    private $table6 = 'tbl_penjualan_seragam';
    private $table7 = 'tbl_penjualan_seragam_detail';
    private $table8 = 'tbl_penjualan_seragam_grand_total';
    private $table10 = 'tbl_customer';

    public function all()
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.*,' .
            $this->table3 . '.Grand_total,' .
            $this->table10 . '.Nama,' .
            $this->table5 . '.Corak'
        )
        ->from($this->table1)
        ->join($this->table2, $this->table1 . '.IDRPS = ' . $this->table2 . '.IDRPS')
        ->join($this->table3, $this->table1 . '.IDRPS = ' . $this->table3 . '.IDRPS')
        ->join($this->table10, $this->table1 . '.IDCustomer = ' . $this->table10 . '.IDCustomer')
        ->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak');

        $query = $this->db->get()->result();
        return $query;
    }

    function tampil_inv()
    {
        $this->db->select("IDFJS, Nomor")
        ->from($this->table6);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampil_supplier()
    {
        $query = $this->db->select("*")->from($this->table4)->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function no_retur($tahun)
    {
        return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
        ->where('extract(year from "Tanggal") =', $tahun)
        ->from($this->table1)
        ->get()->row();
    }

    function tampil_invoice_grand_total()
    {
        $this->db->select("*")
        ->from("tbl_pembelian_grand_total");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function check_invoice_penjualan($nomor)
    {
        $this->db->distinct();
        $this->db->select("
            tbl_penjualan_seragam_detail.*, 
            tbl_penjualan_seragam.*, 
            tbl_corak.Corak,
            tbl_warna.Warna,
            tbl_satuan.Satuan,
            tbl_merk.Merk,
            tbl_merk.IDMerk,
            tbl_barang.Nama_Barang")
        ->from("tbl_penjualan_seragam_detail")
        ->join("tbl_penjualan_seragam", "tbl_penjualan_seragam_detail.IDFJS = tbl_penjualan_seragam.IDFJS")
            //->join("tbl_penjualan_kain_grand_total", "tbl_penjualan_kain_grand_total.IDFJK = tbl_penjualan_kain.IDFJK")
        ->join("tbl_corak", "tbl_penjualan_seragam_detail.IDCorak = tbl_corak.IDCorak")
        ->join("tbl_warna", "tbl_penjualan_seragam_detail.IDWarna = tbl_warna.IDWarna", "left")
        ->join("tbl_satuan", "tbl_penjualan_seragam_detail.IDSatuan = tbl_satuan.IDSatuan")
        ->join("tbl_barang", "tbl_penjualan_seragam_detail.IDBarang = tbl_barang.IDBarang")
        ->join("tbl_merk", "tbl_barang.IDMerk = tbl_merk.IDMerk")
        ->where("tbl_penjualan_seragam.Nomor", $nomor);
        $query = $this->db->get();

        return $query->result();
    }

    function cek_grand_total($Nomor)
    {
        $this->db->select("*")
        ->from($this->table6)
        ->join($this->table8, $this->table6 . '.IDFJS = ' . $this->table8 . '.IDFJS')
        ->join($this->table10, $this->table10 . '.IDCustomer = ' . $this->table6 . '.IDCustomer')
        ->where($this->table6 . '.Nomor', $Nomor);
        $query = $this->db->get();

        return $query->result();
    }

    function tampilkan_id_retur()
    {
        $this->db->select_max("IDRPS")->from($this->table1);
        $query = $this->db->get();

   return $query->row();
    }

    function tampilkan_id_retur_detail()
    {
        $this->db->select_max("IDRPSDetail")->from($this->table2);
        $query = $this->db->get();

   return $query->row();
    }

    function tampilkan_id_retur_grand_total()
    {
        $this->db->select_max("IDRPSGrandTotal")->from($this->table3);
        $query = $this->db->get();

   return $query->row();
    }

    function get_TPKbyNomor($Nomor)
    {
        return $this->db->get_where($this->table6, array('Nomor' => $Nomor))->row();
    }

    // function get_pembayaran($Nomor)
    // {
    //     return $this->db->select("Pembayaran")->from("tbl_penjualan_seragam_grand_total")
    //     ->join("tbl_penjualan_seragam", "tbl_penjualan_seragam.IDFJS", "=", "tbl_penjualan_seragam_grand_total.IDFJS")
    //     ->where("tbl_penjualan_seragam.Nomor", $nomor)->row();
    // }

    public function add($data){
        $this->db->insert($this->table1, $data);
        return TRUE;
    }

    public function add_detail($data){
        $this->db->insert($this->table2, $data);
        return TRUE;
    }

    public function add_grand_total($data){
        $this->db->insert($this->table3, $data);
        return TRUE;
    }

    function print_retur_penjualan($nomor)
    {
        $this->db->select('*')->from($this->table1)->where("Nomor", $nomor);
        return $this->db->get()->row();
    }

    function print_retur_penjualan_detail($id)
    {
        $this->db->select("
            tbl_retur_penjualan_detail.*, 
            tbl_corak.Corak, 
            tbl_warna.Warna, 
            tbl_merk.Merk, 
            tbl_satuan.Satuan, 
            tbl_barang.Nama_Barang, 
            tbl_retur_penjualan_seragam.Nomor")
        ->from("tbl_retur_penjualan_detail")
        ->join("tbl_retur_penjualan_seragam", "tbl_retur_penjualan_detail.IDRPS = tbl_retur_penjualan_seragam.IDRPS")
        ->join("tbl_corak", "tbl_retur_penjualan_detail.IDCorak=tbl_corak.IDCorak")
        ->join("tbl_barang", "tbl_barang.IDBarang=tbl_retur_penjualan_detail.IDBarang")
        ->join("tbl_warna", "tbl_retur_penjualan_detail.IDWarna=tbl_warna.IDWarna", "left")
        ->join("tbl_merk", "tbl_retur_penjualan_detail.IDMerk=tbl_merk.IDMerk")
        ->join("tbl_satuan", "tbl_retur_penjualan_detail.IDSatuan=tbl_satuan.IDSatuan")
        ->where("tbl_retur_penjualan_detail.IDRPS", $id);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function print_grand_total($id)
    {
        $this->db->select('*')->from($this->table3)
        ->where("IDRPS", $id);
        return $this->db->get()->row();
    }

    function find($id)
    {
        return $this->db->select(
            $this->table1 . '.*,' .
            $this->table10 . '.Nama,' .
            $this->table10 . '.IDCustomer,' .
            $this->table6 . '.Nomor AS Nomor_inv')
        ->from($this->table1)
        ->join($this->table10, $this->table1 . ".IDCustomer = " . $this->table10 .".IDCustomer")
        ->join($this->table6, $this->table6 . ".IDFJS = " . $this->table1 . ".IDFJS")
        ->where($this->table1 . '.IDRPS', $id)
        ->get()->row();
    }

    function find_detail($id)
    {
        $this->db->select("
            tbl_retur_penjualan_detail.*, 
            tbl_merk.Merk, 
            tbl_barang.Nama_Barang, 
            tbl_corak.Corak, 
            tbl_satuan.Satuan,
            tbl_warna.Warna")
        ->from("tbl_retur_penjualan_detail")
        ->join("tbl_barang", "tbl_barang.IDBarang=tbl_retur_penjualan_detail.IDBarang")
        ->join("tbl_merk", "tbl_retur_penjualan_detail.IDMerk=tbl_merk.IDMerk")
        ->join("tbl_corak", "tbl_retur_penjualan_detail.IDCorak=tbl_corak.IDCorak")
        ->join("tbl_warna", "tbl_retur_penjualan_detail.IDWarna=tbl_warna.IDWarna", "left")
        ->join("tbl_satuan", "tbl_retur_penjualan_detail.IDSatuan=tbl_satuan.IDSatuan")
        ->where("tbl_retur_penjualan_detail". '.IDRPS', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.*,' .
            $this->table3 . '.Grand_total,' .
            $this->table4 . '.Nama,' .
            $this->table5 . '.Corak'
        )
        ->from($this->table1)
        ->join($this->table2, $this->table1 . '.IDRPS = ' . $this->table2 . '.IDRPS')
        ->join($this->table3, $this->table1 . '.IDRPS = ' . $this->table3 . '.IDRPS')
        ->join($this->table4, $this->table1 . '.IDSupplier = ' . $this->table4 . '.IDSupplier')
        ->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak');


        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => trim($keyword));
            $this->db->like($array);
        }

        $query = $this->db->order_by('IDRPS', 'DESC')->get();
        return $query->result();
    }

    function update_status($data, $where)
    {
        return $this->db->update($this->table1, $data, $where);
    }

    public function update_master($id,$data){
        return $this->db->update($this->table1, $data, array('IDRPS' => $id));
    }

    public function drop($id){
        $this->db->where('IDRPS', $id);
        $this->db->delete($this->table2);
        return TRUE;
    }

    public function update_grand_total($id, $datas){
        return $this->db->update($this->table3, $datas, array('IDRPS' => $id));
    }

    public function get_all_procedure()
    {

        $query = ('
            SELECT * FROM laporan_retur_penjualan_seragam()
          ');

        return $this->db->query($query)->result();
    }
}