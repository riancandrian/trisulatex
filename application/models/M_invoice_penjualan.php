<?php

class M_invoice_penjualan extends CI_Model
{
    private $table1 = 'tbl_penjualan_kain';
    private $table2 = 'tbl_penjualan_kain_detail';
    private $table3 = 'tbl_penjualan_kain_grand_total';
    private $table4 = 'tbl_customer';
    private $table5 = 'tbl_corak';
    private $table6 = 'tbl_barang';
    private $table7 = 'tbl_warna';
    private $table8 = 'tbl_satuan';
    private $table9 = 'tbl_merk';
    private $table10 = 'tbl_surat_jalan_customer_kain';
    private $table11 = 'tbl_kota';

    function no_invoice($tahun)
    {
        return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
        ->where('extract(year from "Tanggal") =', $tahun)
        ->from($this->table1)
        ->get()->row();
    }

    public function all()
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.IDFJK,' .
            $this->table1 . '.Tanggal,' .
            $this->table1 . '.Nomor,' .
            $this->table1 . '.Batal,' .
            $this->table5 . '.Corak,' .
            $this->table4 . '.Nama,' .
            $this->table3 . '.DPP,' .
            $this->table3 . '.PPN,' .
            $this->table3 . '.Grand_total'
        );
        $this->db->from($this->table1)
        ->join($this->table2, $this->table1 . '.IDFJK = ' . $this->table2 . '.IDFJK')
        ->join($this->table3, $this->table1 . '.IDFJK = ' . $this->table3 . '.IDFJK')
        ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
        ->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak');

        return $this->db->get()->result();
    }

    function tampil_sj()
    {
       $this->db->select("IDSJCK, Nomor")
       ->from("tbl_surat_jalan_customer_kain");
       $query = $this->db->get();

       if ($query->num_rows() > 0)
       {
          return $query->result();
      }
  }

  public function get_customer()
  {
    return $this->db->select('IDCustomer, Nama')->from($this->table4)->get()->result();
}

function cekcustomer($SJ)
{
    $this->db->select('tbl_customer."IDCustomer", tbl_customer."Nama", tbl_sales_order_kain."Tanggal_jatuh_tempo"')
    ->from("tbl_surat_jalan_customer_kain")
    ->join("tbl_customer", "tbl_surat_jalan_customer_kain.IDCustomer=tbl_customer.IDCustomer")
    ->join("tbl_packing_list", "tbl_packing_list.IDPAC=tbl_surat_jalan_customer_kain.IDPAC")
    ->join("tbl_sales_order_kain", "tbl_sales_order_kain.IDSOK=tbl_packing_list.IDSOK")
    ->where("tbl_surat_jalan_customer_kain.Nomor", $SJ);
    $query = $this->db->get();

    return $query->result();
}

function cek_group_customer($id)
{
   $this->db->select('tbl_group_customer.Nama_Group_Customer')->from("tbl_customer")
   ->join("tbl_group_customer", "tbl_group_customer.IDGroupCustomer = tbl_customer.IDGroupCustomer")
   ->where("tbl_customer.IDCustomer", $id);
   return $this->db->get()->row();
}

function tampilkan_get_penerimaan_piutang($id)
{
 return $this->db->select("IDFJ")
 ->from("tbl_penerimaan_piutang_detail")
 ->where("IDFJ", $id)
 ->get()->row();
}

public function check_surat_jalan($nomor)
{
    $this->db->select('tbl_sales_order_kain_detail."Harga", tbl_corak."Corak", tbl_barang."Nama_Barang", tbl_merk."Merk", tbl_warna."Warna", tbl_surat_jalan_customer_kain_detail."Grade", tbl_surat_jalan_customer_kain_detail."Qty_roll", tbl_surat_jalan_customer_kain_detail."Qty_yard", tbl_barang."IDBarang", tbl_corak."IDCorak", tbl_warna."IDWarna", tbl_satuan."IDSatuan"')
    ->from("tbl_surat_jalan_customer_kain_detail")
    ->join("tbl_surat_jalan_customer_kain", "tbl_surat_jalan_customer_kain.IDSJCK = tbl_surat_jalan_customer_kain_detail.IDSJCK")
    ->join("tbl_merk", "tbl_surat_jalan_customer_kain_detail.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_corak", "tbl_surat_jalan_customer_kain_detail.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_surat_jalan_customer_kain_detail.IDWarna=tbl_warna.IDWarna")
    ->join("tbl_satuan", "tbl_surat_jalan_customer_kain_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->join("tbl_barang", "tbl_surat_jalan_customer_kain_detail.IDBarang=tbl_barang.IDBarang")
    ->join("tbl_packing_list" , "tbl_packing_list.IDPAC=tbl_surat_jalan_customer_kain.IDPAC")
    ->join("tbl_sales_order_kain_detail" , "tbl_sales_order_kain_detail.IDSOK=tbl_packing_list.IDSOK")
    ->where("tbl_surat_jalan_customer_kain.Nomor", $nomor);
    $query = $this->db->get();

    return $query->result();
}

function tampilkan_id_invoice()
{
    $this->db->select_max("IDFJK")
    ->from($this->table1);
    $query = $this->db->get();

   return $query->row();
}

function tampilkan_id_invoice_detail()
{
    $this->db->select_max("IDFJKDetail")
    ->from($this->table2);
    $query = $this->db->get();

   return $query->row();
}

function tampilkan_id_invoice_grand_total()
{
    $this->db->select_max("IDFJKGrandTotal")
    ->from($this->table3);
    $query = $this->db->get();

   return $query->row();
}

function tampilkan_id_pembayaran()
{
    $this->db->select("IDFJKPembayaran")
    ->from("tbl_penjualan_kain_pembayaran");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function get_TPKbyNomor($Nomor)
{
    return $this->db->get_where('tbl_surat_jalan_customer_kain', array('Nomor' => $Nomor))->row();
}

public function add($data){
    $this->db->insert($this->table1, $data);
    return TRUE;
}

public function add_detail($data){
    $this->db->insert($this->table2, $data);
    return TRUE;
}

public function add_grand_total($grand_total){
    $this->db->insert($this->table3, $grand_total);
    return TRUE;
}

public function add_pembayaran($datas5){
    $this->db->insert('tbl_penjualan_kain_pembayaran', $datas5);
    return TRUE;
}

public function find($id, $nomor)
{
    $this->db->select(
        $this->table1 . '.*,' .
        $this->table4 . '.*,' .
        $this->table11 . '.Kota,' .
        $this->table10 . '.Nomor as nomor_sjck'
    );
    $this->db->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDFJK = ' . $this->table2 . '.IDFJK')
    ->join($this->table3, $this->table1 . '.IDFJK = ' . $this->table3 . '.IDFJK')
    ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
    ->join($this->table10, $this->table1 . '.IDSJCK = ' . $this->table10 . '.IDSJCK')
    ->join($this->table11, $this->table4 . '.IDKota = ' . $this->table11 . '.IDKota');

    if ($id != '_') {
        $this->db->where($this->table1 . '.IDFJK', $id);
    } else {
        $this->db->where($this->table1 . '.Nomor', $nomor);
    }

    return $this->db->get()->row();
}

public function find_edit($id, $nomor)
{
    $this->db->select(
        $this->table1 . '.*,' .
        $this->table4 . '.*,' .
        $this->table10 . '.Nomor as nomor_sjck'
    );
    $this->db->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDFJK = ' . $this->table2 . '.IDFJK')
    ->join($this->table3, $this->table1 . '.IDFJK = ' . $this->table3 . '.IDFJK')
    ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
    ->join($this->table10, $this->table1 . '.IDSJCK = ' . $this->table10 . '.IDSJCK');

    if ($id != '_') {
        $this->db->where($this->table1 . '.IDFJK', $id);
    } else {
        $this->db->where($this->table1 . '.Nomor', $nomor);
    }

    return $this->db->get()->row();
}

public function detail_invoice_penjualan($id, $nomor)
{
    $this->db->select(
        $this->table2 . '.*,' .
        $this->table1 . '.Discount,' .
        $this->table5 . '.Corak,' .
        $this->table6 . '.Nama_Barang,' .
        $this->table7 . '.Warna,' .
        $this->table8 . '.Satuan,' .
        $this->table9 . '.Merk'
    );
    $this->db->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDFJK = ' . $this->table2 . '.IDFJK')
    ->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak')
    ->join($this->table6, $this->table2 . '.IDBarang = ' . $this->table6 . '.IDBarang')
    ->join($this->table7, $this->table2 . '.IDWarna = ' . $this->table7 . '.IDWarna')
    ->join($this->table8, $this->table2 . '.IDSatuan = ' . $this->table8 . '.IDSatuan')
    ->join($this->table9, $this->table6 . '.IDMerk = ' . $this->table9 . '.IDMerk');

    if ($id != '_') {
        $this->db->where($this->table1 . '.IDFJK', $id);
    } else {
        $this->db->where($this->table1 . '.Nomor', $nomor);
    }

    return $this->db->get()->result();
}

function print_invoice($nomor)
{
    $this->db->select('*')->from($this->table1)->where("Nomor", $nomor);
    return $this->db->get()->row();
}

function update_status($data, $where)
{
    return $this->db->update($this->table1, $data, $where);
}

function searching($date_from, $date_until, $search_type, $keyword)
{
    $this->db->distinct();
    $this->db->select(
        $this->table1 . '.IDFJK,' .
        $this->table1 . '.Tanggal,' .
        $this->table1 . '.Nomor,' .
        $this->table1 . '.Batal,' .
        $this->table5 . '.Corak,' .
        $this->table4 . '.Nama,' .
        $this->table3 . '.DPP,' .
        $this->table3 . '.PPN,' .
        $this->table3 . '.Grand_total'
    )
    ->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDFJK = ' . $this->table2 . '.IDFJK')
    ->join($this->table3, $this->table1 . '.IDFJK = ' . $this->table3 . '.IDFJK')
    ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
    ->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak');

    if ($date_from) {
        $this->db->where("Tanggal >= ", $date_from);
    }

    if ($date_until) {
        $this->db->where("Tanggal <= ", $date_until);
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }

    $query = $this->db->order_by('IDFJK', 'DESC')->get();
    return $query->result();
}

// <<<<<<< Updated upstream
function tampil_bank()
{
    return $this->db->get('tbl_coa')->result();
}

function grand_total($id)
{
    $this->db->select('*')
    ->from($this->table3)
    ->join('tbl_penjualan_kain_pembayaran', $this->table3 . '.IDFJK = tbl_penjualan_kain_pembayaran.IDFJK')
    ->where($this->table3 . '.IDFJK', $id);
    return $this->db->get()->row();
}

public function update_master($id,$data){
    return $this->db->update($this->table1, $data, array('IDFJK' => $id));
}

public function update_grand_total($id,$datas){
    return $this->db->update($this->table3, $datas, array('IDFJK' => $id));
}

public function update_pembayaran($id,$datas5){
    return $this->db->update('tbl_penjualan_kain_pembayaran', $datas5, array('IDFJK' => $id));
}
// =======
public function get_all_procedure()
{

    $query = ('
      SELECT * FROM laporan_inv_penjualan_kain()
      ');

    /*$query = ('select * from pengukuran');  */

    return $this->db->query($query)->result();
}

public function searching_store($date_from, $date_until, $keyword)
{

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_inv_penjualan_kain(?, ?, ?)
      ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
}

  //--------------------------Searching Store Procedure
public function searching_store_like($keyword)
{

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_inv_penjualan_kain_like(?)
      ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
}

function tampilkan_id_jurnal()
{
    $this->db->select_max("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

   return $query->row();
}
function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}
// >>>>>>> Stashed changes
}