<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* 
*/
class M_groupasset extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_groupasset()
	{
		$query=$this->db->query("SELECT * FROM tbl_group_asset");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function tampilkan_id_groupasset()
	{
		$this->db->select_max("IDGroupAsset")
		->from("tbl_group_asset");
		$query = $this->db->get();

		return $query->row();
	}
	/*hapus data*/
	function hapus_data_groupasset($id)
	{
		$this->db->where('IDGroupAsset', $id);
		$this->db->delete('tbl_group_asset');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	function get_groupasset($id)
    {
        return $this->db->select("IDGroupAsset")
      ->from("tbl_pembelian_asset_detail")
      ->where("IDGroupAsset", $id)
      ->get()->row();
    }
	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_group_asset', $data);
		return TRUE;
	}

	function get_groupasset_byid($id)
	{
		return $this->db->get_where('tbl_group_asset', array('IDGroupAsset'=>$id))->row();
	}

	function update_groupasset($id, $data)
	{
		$this->db->where('IDGroupAsset', $id);
		$this->db->update('tbl_group_asset', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
    	->from("tbl_group_asset")
    	->like("$kolom", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
    	->from("tbl_group_asset")
    	->like("Kode_Group_Asset", $keyword)
    	->or_like("Group_Asset", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari($kolom,$status,$keyword){
		$this->db->select("*")
    	->from("tbl_group_asset")
    	->like("Aktif", $status)
    	->like("$kolom", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
function cari_by_status($status){
		$this->db->select("*")
		->from("tbl_group_asset")
		->where("Aktif", $status);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	

}
?>