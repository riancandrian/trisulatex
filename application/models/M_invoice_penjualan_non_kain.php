<?php

class M_invoice_penjualan_non_kain extends CI_Model
{
    private $table1 = 'tbl_penjualan_seragam';
    private $table2 = 'tbl_penjualan_seragam_detail';
    private $table3 = 'tbl_penjualan_seragam_grand_total';
    private $table4 = 'tbl_customer';
    private $table5 = 'tbl_corak';
    private $table6 = 'tbl_barang';
    private $table7 = 'tbl_warna';
    private $table8 = 'tbl_satuan';
    private $table9 = 'tbl_penjualan_seragam_pembayaran';
    private $table10 = 'tbl_surat_jalan_customer_seragam';
    private $table11 = 'tbl_kota';

    function no_invoice($tahun)
    {
        return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
        ->where('extract(year from "Tanggal") =', $tahun)
        ->from($this->table1)
        ->get()->row();
    }

    public function all()
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.IDFJS,' .
            $this->table1 . '.Tanggal,' .
            $this->table1 . '.Nomor,' .
            $this->table1 . '.Batal,' .
            $this->table6 . '.Nama_Barang,' .
            $this->table4 . '.Nama,' .
            $this->table3 . '.DPP,' .
            $this->table3 . '.PPN,' .
            $this->table3 . '.Grand_total'
        );
        $this->db->from($this->table1)
        ->join($this->table2, $this->table1 . '.IDFJS = ' . $this->table2 . '.IDFJS')
        ->join($this->table3, $this->table1 . '.IDFJS = ' . $this->table3 . '.IDFJS')
        ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
        ->join($this->table6, $this->table2 . '.IDBarang = ' . $this->table6 . '.IDBarang');

        return $this->db->get()->result();
    }

    public function get_customer()
    {
        return $this->db->select('IDCustomer, Nama')->from($this->table4)->get()->result();
    }

    function tampil_sj()
    {
     $this->db->select("IDSJCS, Nomor")
     ->from("tbl_surat_jalan_customer_seragam");
     $query = $this->db->get();

     if ($query->num_rows() > 0)
     {
      return $query->result();
  }
}

function cekcustomer($SJ)
{
    $this->db->select('tbl_customer."IDCustomer", tbl_customer."Nama", tbl_sales_order_seragam."Tanggal_jatuh_tempo"')
    ->from("tbl_surat_jalan_customer_seragam")
    ->join("tbl_customer", "tbl_surat_jalan_customer_seragam.IDCustomer=tbl_customer.IDCustomer")
    ->join("tbl_sales_order_seragam", "tbl_sales_order_seragam.IDSOS=tbl_surat_jalan_customer_seragam.IDSOS")
    ->where("tbl_surat_jalan_customer_seragam.Nomor", $SJ);
    $query = $this->db->get();

    return $query->result();
}

public function check_surat_jalan($nomor)
{
    $this->db->select('DISTINCT(tbl_sales_order_seragam_detail."Harga"), tbl_barang."Nama_Barang", tbl_surat_jalan_customer_seragam_detail."Qty", tbl_barang."IDBarang", tbl_satuan."IDSatuan", tbl_satuan."Satuan", tbl_corak.IDCorak')
    ->from("tbl_surat_jalan_customer_seragam_detail")
    ->join("tbl_surat_jalan_customer_seragam", "tbl_surat_jalan_customer_seragam.IDSJCS = tbl_surat_jalan_customer_seragam_detail.IDSJCS")
    ->join("tbl_merk", "tbl_surat_jalan_customer_seragam_detail.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_corak", "tbl_surat_jalan_customer_seragam_detail.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_surat_jalan_customer_seragam_detail.IDWarna=tbl_warna.IDWarna", "left")
    ->join("tbl_satuan", "tbl_surat_jalan_customer_seragam_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->join("tbl_barang", "tbl_surat_jalan_customer_seragam_detail.IDBarang=tbl_barang.IDBarang")
    ->join("tbl_sales_order_seragam_detail", "tbl_sales_order_seragam_detail.IDSOS=tbl_surat_jalan_customer_seragam.IDSOS")
    ->where("tbl_surat_jalan_customer_seragam.Nomor", $nomor);
    $query = $this->db->get();

    return $query->result();
}

function tampilkan_id_invoice()
{
    $this->db->select_max("IDFJS")
    ->from($this->table1);
    $query = $this->db->get();

    return $query->row();
}

function tampilkan_id_invoice_detail()
{
    $this->db->select_max("IDFJSDetail")
    ->from($this->table2);
    $query = $this->db->get();

   return $query->row();
}

function tampilkan_id_invoice_grand_total()
{
    $this->db->select_max("IDFJSGrandTotal")
    ->from($this->table3);
    $query = $this->db->get();

   return $query->row();
}

function tampilkan_id_pembayaran()
{
    $this->db->select("IDFJSPembayaran")
    ->from("tbl_penjualan_seragam_pembayaran");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function get_TPKbyNomor($Nomor)
{
    return $this->db->get_where('tbl_surat_jalan_customer_seragam', array('Nomor' => $Nomor))->row();
}

public function add($data){
    $this->db->insert($this->table1, $data);
    return TRUE;
}

public function add_detail($data){
    $this->db->insert($this->table2, $data);
    return TRUE;
}

public function add_grand_total($grand_total){
    $this->db->insert($this->table3, $grand_total);
    return TRUE;
}

public function add_pembayaran($datas5){
    $this->db->insert('tbl_penjualan_seragam_pembayaran', $datas5);
    return TRUE;
}

public function find($id, $nomor)
{
    $this->db->select(
        $this->table1 . '.*,' .
        $this->table4 . '.*,' .
        $this->table11 . '.Kota,' .
        $this->table10 . '.Nomor as Nomor_sjcs'
    );
    $this->db->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDFJS = ' . $this->table2 . '.IDFJS')
    ->join($this->table3, $this->table1 . '.IDFJS = ' . $this->table3 . '.IDFJS')
    ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
    ->join($this->table10, $this->table1 . '.IDSJCS = ' . $this->table10 . '.IDSJCS')
    ->join($this->table11, $this->table4 . '.IDKota = ' . $this->table11 . '.IDKota');;

    if ($id != '_') {
        $this->db->where($this->table1 . '.IDFJS', $id);
    } else {
        $this->db->where($this->table1 . '.Nomor', $nomor);
    }

    return $this->db->get()->row();
}

public function find_edit($id, $nomor)
{
    $this->db->select(
        $this->table1 . '.*,' .
        $this->table4 . '.*,' .
        $this->table10 . '.Nomor as Nomor_sjcs'
    );
    $this->db->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDFJS = ' . $this->table2 . '.IDFJS')
    ->join($this->table3, $this->table1 . '.IDFJS = ' . $this->table3 . '.IDFJS')
    ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
    ->join($this->table10, $this->table1 . '.IDSJCS = ' . $this->table10 . '.IDSJCS');

    if ($id != '_') {
        $this->db->where($this->table1 . '.IDFJS', $id);
    } else {
        $this->db->where($this->table1 . '.Nomor', $nomor);
    }

    return $this->db->get()->row();
}

public function detail_invoice_penjualan($id, $nomor)
{
    $this->db->select(
        $this->table2 . '.*,' .
        $this->table5 . '.Corak,' .
        $this->table6 . '.Nama_Barang,' .
        $this->table7 . '.Warna,' .
        $this->table8 . '.Satuan'
    );
    $this->db->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDFJS = ' . $this->table2 . '.IDFJS')
    ->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak')
    ->join($this->table6, $this->table2 . '.IDBarang = ' . $this->table6 . '.IDBarang')
    ->join($this->table7, $this->table2 . '.IDWarna = ' . $this->table7 . '.IDWarna', 'left')
    ->join($this->table8, $this->table2 . '.IDSatuan = ' . $this->table8 . '.IDSatuan');

    if ($id != '_') {
        $this->db->where($this->table1 . '.IDFJS', $id);
    } else {
        $this->db->where($this->table1 . '.Nomor', $nomor);
    }

    return $this->db->get()->result();
}

function update_status($data, $where)
{
    return $this->db->update($this->table1, $data, $where);
}

function searching($date_from, $date_until, $search_type, $keyword)
{
    $this->db->distinct();
    $this->db->select(
        $this->table1 . '.IDFJS,' .
        $this->table1 . '.Tanggal,' .
        $this->table1 . '.Nomor,' .
        $this->table1 . '.Batal,' .
        $this->table6 . '.Nama_Barang,' .
        $this->table4 . '.Nama,' .
        $this->table3 . '.DPP,' .
        $this->table3 . '.PPN,' .
        $this->table3 . '.Grand_total'
    );
    $this->db->from($this->table1)
    ->join($this->table2, $this->table1 . '.IDFJS = ' . $this->table2 . '.IDFJS')
    ->join($this->table3, $this->table1 . '.IDFJS = ' . $this->table3 . '.IDFJS')
    ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer')
    ->join($this->table6, $this->table2 . '.IDBarang = ' . $this->table6 . '.IDBarang');

    if ($date_from) {
        $this->db->where("Tanggal >= ", $date_from);
    }

    if ($date_until) {
        $this->db->where("Tanggal <= ", $date_until);
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }

    $query = $this->db->order_by('IDFJS', 'DESC')->get();
    return $query->result();
}

public function searching_store($date_from, $date_until, $keyword)
{

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_inv_penjualan_seragam(?, ?, ?)
      ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
}

  //--------------------------Searching Store Procedure
public function searching_store_like($keyword)
{

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_inv_penjualan_seragam_like(?)
      ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
}

function tampil_bank()
{
    return $this->db->get('tbl_coa')->result();
}

function grand_total($id)
{
    $this->db->select('*')
    ->from($this->table3)
    ->join($this->table9, $this->table3 . '.IDFJS = ' . $this->table9 . '.IDFJS')
    ->where($this->table3 . '.IDFJS', $id);
    return $this->db->get()->row();
}

public function update_master($id, $data){
    return $this->db->update($this->table1, $data, array('IDFJS' => $id));
}

public function update_grand_total($id,$datas){
    return $this->db->update($this->table3, $datas, array('IDFJS' => $id));
}

public function update_pembayaran($id,$datas5){
    return $this->db->update($this->table9, $datas5, array('IDFJS' => $id));
}

public function get_all_procedure()
{

    $query = ('
        SELECT * FROM laporan_inv_penjualan_seragam()
        ');

    return $this->db->query($query)->result();
}
}