<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_coa extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_coa()
	{
		$query=$this->db->query("SELECT * FROM tbl_coa");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function tampilkan_groupcoa()
	{
		$query=$this->db->query("SELECT * FROM tbl_group_coa");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function get_coa_jurnal($id)
	{
		return $this->db->select("IDCOA")
		->from("tbl_jurnal")
		->where("IDCOA", $id)
		->get()->row();
	}

	function tampilkan_id_coa()
	{
		$this->db->select_max("IDCoa")
		->from("tbl_coa");
		$query = $this->db->get();

		return $query->row();
	}

	/*hapus data*/
	function hapus_data_coa($id)
	{
		$this->db->where('IDCoa', $id);
		$this->db->delete('tbl_coa');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_coa', $data);
		return TRUE;
	}

	function get_coa_byid($id)
	{
		return $this->db->get_where('tbl_coa', array('IDCoa'=>$id))->row();
	}

	function update_coa($id, $data)
	{
		$this->db->where('IDCoa', $id);
		$this->db->update('tbl_coa', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_coa")
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_coa")
		->like("Kode_COA", $keyword)
		->or_like("Nama_COA", $keyword)
		->or_like("Status", $keyword)
		->or_like("Nama_Group", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari($kolom,$status,$keyword){
		$this->db->select("tbl_coa.*")
		->from("tbl_coa")
		->like("Aktif", $status)
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function cari_by_status($status){
		$this->db->select("*")
		->from("tbl_coa")
		->where("Aktif", $status);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>