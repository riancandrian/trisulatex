<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_saldoawalacount extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_saldoawalacount()
	{
		$this->db->select("tbl_coa_saldo_awal.*, tbl_group_coa.Nama_Group, tbl_coa.Nama_COA")
		->from("tbl_coa_saldo_awal")
		->join("tbl_coa", "tbl_coa.IDCoa=tbl_coa_saldo_awal.IDCoa")
		->join("tbl_group_coa", "tbl_group_coa.IDGroupCOA=tbl_coa_saldo_awal.IDGroupCOA");

		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampil_coa()
	{
		$this->db->select("*")
		->from("tbl_coa");
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function tampilkan_id_saldoawalacount()
	{
		$this->db->select_max("IDCOASaldoAwal")
		->from("tbl_coa_saldo_awal");
		$query = $this->db->get();

		return $query->row();
	}

	/*hapus data*/
	function hapus_data_coasaldoawal($id)
	{
		$this->db->where('IDCOASaldoAwal', $id);
		$this->db->delete('tbl_coa_saldo_awal');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_coa_saldo_awal', $data);
		return TRUE;
	}

	function get_saldoawalcoa_byid($id)
	{
		return $this->db->get_where('tbl_coa_saldo_awal', array('IDCOASaldoAwal'=>$id))->row();
	}

	function update_saldoawalacount($id, $data)
	{
		$this->db->where('IDCOASaldoAwal', $id);
		$this->db->update('tbl_coa_saldo_awal', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("tbl_coa_saldo_awal.*, tbl_group_coa.Nama_Group, tbl_coa.Nama_COA")
		->from("tbl_coa_saldo_awal")
		->join("tbl_coa", "tbl_coa.IDCoa=tbl_coa_saldo_awal.IDCoa")
		->join("tbl_group_coa", "tbl_group_coa.IDGroupCOA=tbl_coa_saldo_awal.IDGroupCOA")
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("tbl_coa_saldo_awal.*, tbl_group_coa.Nama_Group, tbl_coa.Nama_COA")
		->from("tbl_coa_saldo_awal")
		->join("tbl_coa", "tbl_coa.IDCoa=tbl_coa_saldo_awal.IDCoa")
		->join("tbl_group_coa", "tbl_group_coa.IDGroupCOA=tbl_coa_saldo_awal.IDGroupCOA")
		->like("tbl_coa.Nama_COA", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari($kolom,$status,$keyword){
		$this->db->select("tbl_coa_saldo_awal.*, tbl_group_coa.Nama_Group, tbl_coa.Nama_COA")
		->from("tbl_coa_saldo_awal")
		->join("tbl_coa", "tbl_coa.IDCoa=tbl_coa_saldo_awal.IDCoa")
		->join("tbl_group_coa", "tbl_group_coa.IDGroupCOA=tbl_coa_saldo_awal.IDGroupCOA")
		->like("tbl_coa_saldo_awal.Aktif", $status)
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function cari_by_status($status){
		$this->db->select("tbl_coa_saldo_awal.*, tbl_group_coa.Nama_Group, tbl_coa.Nama_COA")
		->from("tbl_coa_saldo_awal")
		->join("tbl_coa", "tbl_coa.IDCoa=tbl_coa_saldo_awal.IDCoa")
		->join("tbl_group_coa", "tbl_group_coa.IDGroupCOA=tbl_coa_saldo_awal.IDGroupCOA")
		->where("tbl_coa_saldo_awal.Aktif", $status);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function get_asset($id)
	{
		return $this->db->select('tbl_coa.*, tbl_group_coa.Nama_Group, tbl_group_coa.Normal_Balance')
		->from("tbl_coa")
		->join("tbl_group_coa", "tbl_coa.IDGroupCOA=tbl_group_coa.IDGroupCOA")
		->where('tbl_coa.IDCoa', $id)

		->get()->row();
	}
	function get_IDgroupcoa($groupcoaid)
	{
		return $this->db->get_where("tbl_group_coa", array('Nama_Group' => $groupcoaid))->row();
	}

	function save_jurnal($data)
	{
		$this->db->insert("tbl_jurnal", $data);
		return TRUE;
	}

	function tampilkan_id_jurnal()
	{
		$this->db->select_max("IDJurnal")
		->from("tbl_jurnal");
		$query = $this->db->get();

		return $query->row();
	}

	function update_jurnal($id, $data)
	{
		$this->db->where('Nomor', $id);
		$this->db->update('tbl_jurnal', $data);
		return TRUE;
	}

}
?>