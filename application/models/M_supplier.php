
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_supplier extends CI_Model {

  // Set nama table sebagai const
  private $table = 'tbl_suplier';
  private $order = 'DESC';
  private $id = 'IDSupplier';
  private $kode = 'Kode_Suplier';
  // Fungsi Select semua data giro
  public function get_all()
  {
    $this->db->order_by($this->id, $this->order);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
   } 
 }

 public function get_trisula()
 {
  $this->db->order_by($this->id, $this->order);
  $this->db->where($this->kode, 'SRLT-01');
  $data = $this->db->get($this->table);

  $hitung = $this->db->count_all_results($this->table);
  if ( $hitung > 0) {
   return $data->result();
 } 
}

function get_po($id)
{
  return $this->db->select("IDSupplier")
  ->from("tbl_purchase_order")
  ->where("IDSupplier", $id)
  ->get()->row();
}

function get_po_umum($id)
{
  return $this->db->select("IDSupplier")
  ->from("tbl_purchase_order_umum")
  ->where("IDSupplier", $id)
  ->get()->row();
}

function tampilkan_id_supplier()
{
  $this->db->select_max("IDSupplier")
  ->from("tbl_suplier");
  $query = $this->db->get();

  return $query->row();
}

    //kebutuhan get data kota
public function get_all_kota()
{
  $this->db->order_by('IDKota', $this->order);
  $data = $this->db->get('tbl_kota');

  $hitung = $this->db->count_all_results('tbl_kota');
  if ( $hitung > 0) {
   return $data->result();
 } 
}

    //kebutuhan get data group supplier
public function get_all_group_suuplier()
{
  $this->db->order_by('IDGroupSupplier', $this->order);
  $data = $this->db->get('tbl_group_supplier');

  $hitung = $this->db->count_all_results('tbl_group_supplier');
  if ( $hitung > 0) {
   return $data->result();
 } 
}

  //Get Data by ID
public function get_by_id($id)
{
  $this->db->select('tbl_suplier.IDGroupSupplier as IDGroupSupplier, tbl_suplier.IDSupplier as IDSupplier, tbl_suplier.Kode_Suplier as Kode_Suplier, tbl_suplier.Nama as Nama, tbl_suplier.Alamat as Alamat, tbl_suplier.No_Telpon as No_Telpon, tbl_suplier.IDKota as IDKota, tbl_suplier.Fax as Fax, tbl_suplier.Email as Email, tbl_suplier.NPWP as NPWP, tbl_suplier.No_KTP as No_KTP, tbl_suplier.Aktif as Aktif, tbl_kota.Kota as Kota, tbl_group_supplier.Kode_Group_Supplier as Kode_Group_Supplier, tbl_group_supplier.Group_Supplier as Group_Suplier, tbl_group_supplier.Aktif as Aktif2');
  
  $this->db->join('tbl_group_supplier', 'tbl_group_supplier.IDGroupSupplier = tbl_suplier.IDGroupSupplier', 'left');
  $this->db->join('tbl_kota', 'tbl_kota.IDKota = tbl_suplier.IDKota', 'left');
  $this->db->where($this->id, $id);
  $data = $this->db->get($this->table);

  $hitung = $this->db->count_all_results($this->table);
  if ( $hitung > 0) {
   return $data->row();
 } 
 
 
}

  //Fungsi Tambah Data
public function insert($data)
{
  $this->db->insert($this->table, $data);
}

  //Fungsi Ubah Data
public function update($id, $data)
{
  $this->db->where($this->id, $id);
  $this->db->update($this->table, $data);
}

  //Fungsi Hapus Data
public function delete($id)
{
  $this->db->where($this->id, $id);
  $this->db->delete($this->table);
}

function cari($kolom,$status,$keyword){
 $this->db->select("*")
 ->from("tbl_suplier c")
 ->join("tbl_group_supplier tbc", "c.IDGroupSupplier=tbc.IDGroupSupplier")
 ->join("tbl_kota tb", "c.IDKota=tb.IDKota")
 ->like("c.Aktif", $status)
 ->like("$kolom", $keyword);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_kolom($kolom,$keyword){
  $this->db->select("*")
  ->from("tbl_suplier c")
  ->join("tbl_group_supplier tbc", "c.IDGroupSupplier=tbc.IDGroupSupplier")
  ->join("tbl_kota tb", "c.IDKota=tb.IDKota")
  ->like("$kolom", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function cari_by_status($status){
  $this->db->select("*")
  ->from("tbl_suplier c")
  ->join("tbl_group_supplier tbc", "c.IDGroupSupplier=tbc.IDGroupSupplier")
  ->join("tbl_kota tb", "c.IDKota=tb.IDKota")
  ->where("c.Aktif", $status);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function cari_by_keyword($keyword){
  $this->db->select("*")
  ->from("tbl_suplier c")
  ->join("tbl_group_supplier tbc", "c.IDGroupSupplier=tbc.IDGroupSupplier")
  ->join("tbl_kota tb", "c.IDKota=tb.IDKota")
  ->like("Kode_Suplier", $keyword)
  ->or_like("Nama", $keyword)
  ->or_like("Alamat", $keyword)
  ->or_like("Email", $keyword)
  ->or_like("NPWP", $keyword)
  ->or_like("No_KTP", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}



}

/* End of file M_supplier.php */
