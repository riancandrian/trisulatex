<?php

class M_retur_pembelian extends CI_Model
{

  function tampilkan_retur_pembelian()
  {
    $this->db->select("tbl_retur_pembelian.*, tbl_suplier.Nama")
    ->from("tbl_retur_pembelian")
    ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_retur_pembelian.IDSupplier");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
      return $query->result();
    }
  }

  function tampil_supplier()
  {
   $this->db->select("*")
   ->from("tbl_suplier");
   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
  }
}

public function getstokbyid($id)
{
  $this->db->select('IDStok, Saldo_yard, Saldo_meter, Barcode');

  $this->db->where('Barcode', $id);
  return $this->db->get("tbl_stok")->row();
}

function tampil_inv()
{
 $this->db->select("IDFB, Nomor")
 ->from("tbl_pembelian");
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
  return $query->result();
}
}

function tampilkan_id_kartu_stok()
{
    $this->db->select_max("IDKartuStok")
    ->from("tbl_kartu_stok");
    $query = $this->db->get();

  return $query->row();
}

function save_stock_card($data)
{
    $this->db->insert("tbl_kartu_stok", $data);
    return TRUE;
}

public function update_stok($id,$data)
{
  $this->db->where('Barcode', $id);
  $this->db->update("tbl_stok", $data);
}

function no_retur($tahun)
{
  return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
  ->where('extract(year from "Tanggal") =', $tahun)
  ->from("tbl_retur_pembelian")
  ->get()->row();
}

function get_saldo_akhir($nomor)
{
 return $this->db->get_where("tbl_hutang", array('No_Faktur' => $nomor))->row();
}

function searching($date_from, $date_until, $search_type, $keyword)
{
  $this->db->select("tbl_retur_pembelian.*, tbl_suplier.Nama")
  ->from("tbl_retur_pembelian")
  ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_retur_pembelian.IDSupplier");


  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
  }

  if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
  }

  if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
  }

  $query = $this->db->order_by('IDRB', 'DESC')->get();
  return $query->result();
}



function check_invoice($nomor)
{
        // $this->db->distinct();
  $this->db->select("*")
  ->from("tbl_pembelian_detail")
  ->join("tbl_pembelian", "tbl_pembelian_detail.IDFB=tbl_pembelian.IDFB")
  ->join("tbl_merk", "tbl_pembelian_detail.IDMerk=tbl_merk.IDMerk")
  ->join("tbl_corak", "tbl_pembelian_detail.IDCorak=tbl_corak.IDCorak")
  ->join("tbl_warna", "tbl_pembelian_detail.IDWarna=tbl_warna.IDWarna")
  ->join("tbl_satuan", "tbl_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan")
  ->where("tbl_pembelian.Nomor", $nomor)
  ->where('tbl_pembelian_detail.Barcode NOT IN (SELECT "Barcode" FROM tbl_retur_pembelian_detail)');
  $query = $this->db->get();

  return $query->result();

    // $query= $this->db->query('select * from tbl_pembelian_detail join tbl_pembelian on tbl_pembelian_detail."IDFB"=tbl_pembelian."IDFB" join tbl_merk on tbl_pembelian_detail."IDMerk"=tbl_merk."IDMerk" join tbl_corak on tbl_pembelian_detail."IDCorak"=tbl_corak."IDCorak" join tbl_warna on tbl_pembelian_detail."IDWarna"=tbl_warna."IDWarna" join tbl_satuan on tbl_pembelian_detail."IDSatuan"=tbl_satuan."IDSatuan" where tbl_pembelian."Nomor"=INV/18/PMAK/00001');
    // if ($query->num_rows() > 0)
    //     {
    //         return $query->result();
    //     }
}

function check_grandtotal($IDFB)
{
  $this->db->select("*")
  ->from("tbl_pembelian")
  ->join("tbl_pembelian_grand_total", "tbl_pembelian.IDFB=tbl_pembelian_grand_total.IDFB")
  ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pembelian.IDSupplier")
  ->where("tbl_pembelian.Nomor", $IDFB);
  $query = $this->db->get();

  return $query->result();
}

function tampil_invoice_grand_total()
{
 $this->db->select("*")
 ->from("tbl_pembelian_grand_total");
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
  return $query->result();
}
}

function getpembayaranhutang($id)
{
  $this->db->select('*')->from("tbl_pembayaran_hutang_detail")
  ->where("IDFB", $id);
  return $this->db->get()->row();
}

function gethutang($nomor)
{
  $this->db->select('*')->from("tbl_hutang")
  ->where("No_Faktur", $nomor);
  return $this->db->get()->row();
}

function gethutangsupplier($nomor)
{
  $this->db->select('*')->from("tbl_hutang")
  ->where("No_Faktur", $nomor);
  $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
  return $query->result();
}
}

public function add($data){
 $this->db->insert('tbl_retur_pembelian', $data);
 return TRUE;
}
function get_IDFB($Nomor)
{
  return $this->db->get_where("tbl_pembelian", array('Nomor' => $Nomor))->row();
}
public function add_detail($data){
  $this->db->insert('tbl_retur_pembelian_detail', $data);
  return TRUE;
}
public function add_grand_total($data4){
  $this->db->insert('tbl_retur_pembelian_grand_total', $data4);
  return TRUE;
}

function getById($id)
{
  $this->db->select('tbl_retur_pembelian.*, tbl_pembelian.Nomor AS nomorFB, tbl_suplier.Nama')->from("tbl_retur_pembelian")
  ->join("tbl_retur_pembelian_grand_total", "tbl_retur_pembelian.IDRB = tbl_retur_pembelian_grand_total.IDRB")
  ->join("tbl_pembelian", "tbl_pembelian.IDFB = tbl_retur_pembelian.IDFB")
  ->join("tbl_suplier", "tbl_suplier.IDSupplier = tbl_retur_pembelian.IDSupplier")
  ->where("tbl_retur_pembelian.IDRB", $id);
  return $this->db->get()->row();
}

function tampilkan_id_retur()
{
  $this->db->select_max("IDRB")
  ->from("tbl_retur_pembelian");
  $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_retur_detail()
{
  $this->db->select_max("IDRBDetail")
  ->from("tbl_retur_pembelian_detail");
  $query = $this->db->get();

  return $query->row();
}

function tampilkan_id_retur_grand_total()
{
  $this->db->select_max("IDRBGrandTotal")
  ->from("tbl_retur_pembelian_grand_total");
  $query = $this->db->get();

  return $query->row();
}

function detail_retur_pembelian($id)
{
 $this->db->select("*")
 ->from("tbl_retur_pembelian")
 ->join("tbl_retur_pembelian_detail", "tbl_retur_pembelian.IDRB=tbl_retur_pembelian_detail.IDRB")
 ->join("tbl_corak", "tbl_retur_pembelian_detail.IDCorak=tbl_corak.IDCorak")
 ->join("tbl_warna", "tbl_retur_pembelian_detail.IDWarna=tbl_warna.IDWarna")
 ->join("tbl_merk", "tbl_retur_pembelian_detail.IDMerk=tbl_merk.IDMerk")
 ->join("tbl_satuan", "tbl_retur_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_retur_pembelian_detail.IDRB", $id);
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
  return $query->result();
}
}
public function update_master($id,$data){
  return $this->db->update('tbl_retur_pembelian', $data, array('IDRB' => $id));
}

public function update_data_hutang($id,$data){
  return $this->db->update('tbl_hutang', $data, array('No_Faktur' => $id));
}

function update_status($data, $where)
{
  return $this->db->update('tbl_retur_pembelian', $data, $where);
}

public function drop($id){
  $this->db->where('IDRBDetail', $id);
  $this->db->delete('tbl_retur_pembelian_detail');
  return TRUE;
}
function get_IDcorak($corak)
{
  return $this->db->get_where("tbl_corak", array('Corak' => $corak))->row();
}

function get_IDwarna($warna)
{
  return $this->db->get_where("tbl_warna", array('Warna' => $warna))->row();
}

function get_IDsatuan($satuan)
{
  return $this->db->get_where("tbl_satuan", array('Satuan' => $satuan))->row();
}

function get_IDbarang($corak)
{
  $this->db->select('IDBarang')->from("tbl_barang")
  ->join("tbl_corak", "tbl_barang.IDCorak = tbl_corak.IDCorak")
  ->where("tbl_corak.Corak", $corak);
  return $this->db->get()->row();
}
public function update_grand_total($id,$datas){
  return $this->db->update('tbl_retur_pembelian_grand_total', $datas, array('IDRB' => $id));
}

function find($id)
{
  return $this->db->select('tbl_retur_pembelian.*, tbl_suplier.Nama, tbl_pembelian.Nomor AS Nomor_inv')
  ->from("tbl_retur_pembelian")
  ->from("tbl_suplier", "tbl_retur_pembelian.IDSuplier=tbl_suplier.IDSupplier")
  ->from("tbl_pembelian", "tbl_pembelian.IDFB=tbl_retur_pembelian.IDFB")
  ->where("tbl_retur_pembelian". '.IDRB', $id)
  ->get()->row();
}
function find_detail($id)
{
  $this->db->select("tbl_retur_pembelian_detail.*, tbl_merk.Merk, tbl_barang.Nama_Barang, tbl_corak.Corak, tbl_warna.Warna, tbl_satuan.Satuan")
  ->from("tbl_retur_pembelian_detail")
  ->join("tbl_barang", "tbl_barang.IDBarang=tbl_retur_pembelian_detail.IDBarang")
  ->join("tbl_merk", "tbl_retur_pembelian_detail.IDMerk=tbl_merk.IDMerk")
  ->join("tbl_corak", "tbl_retur_pembelian_detail.IDCorak=tbl_corak.IDCorak")
  ->join("tbl_warna", "tbl_retur_pembelian_detail.IDWarna=tbl_warna.IDWarna")
  ->join("tbl_satuan", "tbl_retur_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan")
  ->where("tbl_retur_pembelian_detail". '.IDRB', $id);

  $query = $this->db->get();

  if ($query->num_rows() > 0)
  {
    return $query->result();
  }
}

function print_retur_pembelian($nomor)
{
  $this->db->select('*')->from("tbl_retur_pembelian")
  ->where("IDRB", $nomor);
  return $this->db->get()->row();
}
function print_retur_pembelian_detail($id)
{
 $this->db->select("tbl_retur_pembelian_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan, tbl_barang.Nama_Barang, tbl_retur_pembelian.Nomor")
 ->from("tbl_retur_pembelian_detail")
 ->join("tbl_retur_pembelian", "tbl_retur_pembelian_detail.IDRB=tbl_retur_pembelian.IDRB")
 ->join("tbl_corak", "tbl_retur_pembelian_detail.IDCorak=tbl_corak.IDCorak")
 ->join("tbl_barang", "tbl_barang.IDBarang=tbl_retur_pembelian_detail.IDBarang")
 ->join("tbl_warna", "tbl_retur_pembelian_detail.IDWarna=tbl_warna.IDWarna")
 ->join("tbl_merk", "tbl_retur_pembelian_detail.IDMerk=tbl_merk.IDMerk")
 ->join("tbl_satuan", "tbl_retur_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_retur_pembelian_detail.IDRB", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function print_grand_total($id)
{
  $this->db->select('*')->from("tbl_retur_pembelian_grand_total")
  ->where("IDRB", $id);
  return $this->db->get()->row();
}

public function get_all_procedure_retur()
{

  $query = ('
    SELECT * FROM laporan_retur_pembelian()
    ');

  /*$query = ('select * from pengukuran');  */

  return $this->db->query($query)->result();
}

public function searching_store($date_from, $date_until, $keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_laporan_retur_pembelian(?, ?, ?)
    ');

  /*$query = ('select * from pengukuran');    */

  return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
}

  //--------------------------Searching Store Procedure
public function searching_store_like($keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_laporan_retur_pembelian_like(?)
    ');

  /*$query = ('select * from pengukuran');    */

  return $this->db->query($query, array($new_keywords))->result();
}

function tampilkan_id_jurnal()
{
    $this->db->select_max("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

  return $query->row();
}
function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}

public function exsport_excel() {
        $this->db->select(array("tbl_retur_pembelian.Tanggal", "tbl_retur_pembelian.Nomor", "tbl_suplier.Nama", "tbl_retur_pembelian_detail.Barcode","tbl_retur_pembelian_detail.NoSO","tbl_retur_pembelian_detail.Party","tbl_barang.Nama_Barang","tbl_corak.Corak", "tbl_warna.Warna", "tbl_merk.Merk", "tbl_retur_pembelian_detail.Saldo_yard", "tbl_retur_pembelian_detail.Saldo_meter", "tbl_retur_pembelian_detail.Grade", "tbl_retur_pembelian_detail.Lebar", "tbl_satuan.Satuan","tbl_retur_pembelian_detail.Harga", "tbl_retur_pembelian_detail.Subtotal"));
        $this->db->from('tbl_retur_pembelian');
        $this->db->join('tbl_retur_pembelian_detail', "tbl_retur_pembelian.IDRB=tbl_retur_pembelian_detail.IDRB");
        $this->db->join('tbl_corak', "tbl_retur_pembelian_detail.IDCorak=tbl_corak.IDCorak");
         $this->db->join('tbl_barang', "tbl_retur_pembelian_detail.IDBarang=tbl_barang.IDBarang");
        $this->db->join('tbl_warna', "tbl_retur_pembelian_detail.IDWarna=tbl_warna.IDWarna");
        $this->db->join('tbl_merk', "tbl_retur_pembelian_detail.IDMerk=tbl_merk.IDMerk");
        $this->db->join('tbl_satuan', "tbl_retur_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan");
        $this->db->join('tbl_suplier', "tbl_suplier.IDSupplier=tbl_retur_pembelian.IDSupplier");
        $this->db->where("tbl_retur_pembelian.Batal", 'aktif');
        $this->db->order_by("tbl_retur_pembelian.Nomor", 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

}
?>