<?php

class M_laporan_bo extends CI_Model
{
    public $table1 = 'tbl_booking_order';
    public $table2 = 'tbl_corak';

    function all()
    {
        /*$this->db->select('*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDCorak = ' . $this->table2 . '.IDCorak');

        $query = $this->db->order_by('IDBO', 'DESC')->get();
        return $query->result();*/
        $query = ('SELECT * FROM laporan_bo()');

        return $this->db->query($query)->result();
    }

    public function searching_store($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_bo(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_bo_like(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }

  public function exsport_excel() {
        $this->db->select(array("tbl_booking_order.Tanggal", "tbl_booking_order.Nomor", "tbl_booking_order.Tanggal_selesai", "tbl_corak.Corak", "tbl_booking_order.Qty", "tbl_booking_order.Batal"));
        $this->db->from('tbl_booking_order');
        $this->db->join('tbl_corak', "tbl_booking_order.IDCorak=tbl_corak.IDCorak");
        $this->db->where("tbl_booking_order.Batal", 'Aktif');
        $this->db->order_by("tbl_booking_order.Nomor", 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
}