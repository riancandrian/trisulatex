<?php

class M_pembelian_asset extends CI_Model
{
    private $table = 'tbl_pembelian_asset';
    private $table2 = 'tbl_pembelian_asset_detail';

    function tampilkan_pembelian_asset()
    {
        $this->db->select("tbl_pembelian_asset.Nomor, tbl_pembelian_asset.IDFBA, tbl_pembelian_asset.Tanggal, tbl_pembelian_asset_detail.IDAsset, tbl_asset.Nama_Asset, tbl_pembelian_asset.Batal, tbl_group_asset.Group_Asset")
        ->from("tbl_pembelian_asset")
        ->join("tbl_pembelian_asset_detail", "tbl_pembelian_asset.IDFBA=tbl_pembelian_asset_detail.IDFBA")
        ->join("tbl_asset", "tbl_asset.IDAsset=tbl_pembelian_asset_detail.IDAsset")
        ->join("tbl_group_asset", "tbl_group_asset.IDGroupAsset=tbl_pembelian_asset_detail.IDGroupAsset")
        ->group_by("tbl_pembelian_asset.Nomor, tbl_pembelian_asset.IDFBA, tbl_pembelian_asset.Tanggal, tbl_pembelian_asset_detail.IDAsset, tbl_asset.Nama_Asset,tbl_pembelian_asset.Batal, tbl_group_asset.Group_Asset");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_pembelian_asset()
    {
        $this->db->select_max("IDFBA")
        ->from("tbl_pembelian_asset");
       $query = $this->db->get();

  return $query->row();

    }

    function getasset($id_group_asset)
    {
        $this->db->select("*")
        ->from("tbl_group_asset")
        ->join("tbl_asset", "tbl_group_asset.IDGroupAsset=tbl_asset.IDGroupAsset")
        ->where("tbl_group_asset.Group_Asset", $id_group_asset);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
   }

   function tampilkan_id_pembelian_asset_detail()
   {
    $this->db->select_max("IDFBADetail")
    ->from("tbl_pembelian_asset_detail");
    $query = $this->db->get();

  return $query->row();

}

function group_asset()
{
    return $this->db->get('tbl_group_asset')->result();
}

function tampil_asset()
{
    return $this->db->get('tbl_asset')->result();
}

function no_pembelian_asset()
{
         // $q = $this->db->query("SELECT MAX(RIGHT(no_invoice,4)) AS kd_max FROM tbl_invoice WHERE DATE(tanggal)=CURDATE()");
    $this->db->select("*")
    ->from("tbl_agen")
    ->where("IDAgen", 10);
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        foreach ($query->result() as $a) {
         $agen= $a->Initial;
     }
 }

 $this->db->select_max("IDFBA", "kd_max")
 ->from("tbl_pembelian_asset");
 $q= $this->db->get();
 $kd = "";
 if($q->num_rows()>0){
    foreach($q->result() as $k){
        $tmp = ((int)$k->kd_max)+1;
        $kd = sprintf("%04s", $tmp);
    }
}else{
    $kd = "0001";
}
        // $agen= $this->session->userdata("Nama");
date_default_timezone_set('Asia/Jakarta');
return 'PA'.date('y').$agen.$kd;
}

function simpan($data)
{
    $this->db->insert('tbl_asset', $data);
    return TRUE;
}

function getById($id)
{
    return $this->db->select('*')
    ->from("tbl_pembelian_asset")
    ->join("tbl_pembelian_asset_detail", "tbl_pembelian_asset.IDFBA=tbl_pembelian_asset_detail.IDFBA")
    ->join("tbl_group_asset", "tbl_group_asset.IDGroupAsset=tbl_pembelian_asset_detail.IDGroupAsset")
    ->where("tbl_pembelian_asset". '.IDFBA', $id)
    ->get()->row();
}

function detail_pembelian_asset($id)
{
   $this->db->select("*")
   ->from("tbl_pembelian_asset")
   ->join("tbl_pembelian_asset_detail", "tbl_pembelian_asset.IDFBA=tbl_pembelian_asset_detail.IDFBA")
   ->join("tbl_asset", "tbl_asset.IDAsset=tbl_pembelian_asset_detail.IDAsset")
   ->join("tbl_group_asset", "tbl_group_asset.IDGroupAsset=tbl_pembelian_asset_detail.IDGroupAsset")
   ->where("tbl_pembelian_asset_detail.IDFBA", $id);
   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}


function update_status($data, $where)
{
    return $this->db->update('tbl_pembelian_asset', $data, $where);
}

function delete($id)
{
    $this->db->where('IDAsset', $id);
    $this->db->delete('tbl_asset');
}

function searching($date_from, $date_until, $search_type, $keyword)
{
    $this->db->select("tbl_pembelian_asset.Nomor, tbl_pembelian_asset.IDFBA, tbl_pembelian_asset.Tanggal, tbl_pembelian_asset_detail.IDAsset, tbl_asset.Nama_Asset, tbl_pembelian_asset.Batal, tbl_group_asset.Group_Asset")
    ->from("tbl_pembelian_asset")
    ->join("tbl_pembelian_asset_detail", "tbl_pembelian_asset.IDFBA=tbl_pembelian_asset_detail.IDFBA")
    ->join("tbl_asset", "tbl_asset.IDAsset=tbl_pembelian_asset_detail.IDAsset")
    ->join("tbl_group_asset", "tbl_group_asset.IDGroupAsset=tbl_pembelian_asset_detail.IDGroupAsset")
    ->group_by("tbl_pembelian_asset.Nomor, tbl_pembelian_asset.IDFBA, tbl_pembelian_asset.Tanggal, tbl_pembelian_asset_detail.IDAsset, tbl_asset.Nama_Asset,tbl_pembelian_asset.Batal, tbl_group_asset.Group_Asset");

    if ($date_from) {
        $this->db->where("Tanggal >= ", $date_from);
    }

    if ($date_until) {
        $this->db->where("Tanggal <= ", $date_until);
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }

    $query = $this->db->order_by('IDFBA', 'DESC')->get();
    return $query->result();
}

public function add($data){
 $this->db->insert('tbl_pembelian_asset', $data);
 return TRUE;
}

public function add_detail($data){
    $this->db->insert('tbl_pembelian_asset_detail', $data);
    return TRUE;
}
function find($id)
{
    return $this->db->select('*')
    ->from("tbl_pembelian_asset")
    ->where("tbl_pembelian_asset". '.IDFBA', $id)
    ->get()->row();
}
function find_detail($id)
{
   $this->db->select("tbl_pembelian_asset_detail.*, tbl_asset.Nama_Asset, tbl_group_asset.Group_Asset, tbl_group_asset.Tarif_Penyusutan")
   ->from("tbl_pembelian_asset_detail")
   ->join("tbl_asset", "tbl_asset.IDAsset=tbl_pembelian_asset_detail.IDAsset")
   ->join("tbl_group_asset", "tbl_group_asset.IDGroupAsset=tbl_pembelian_asset_detail.IDGroupAsset")
   ->where("tbl_pembelian_asset_detail". '.IDFBA', $id);

   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}
public function drop($id){
    $this->db->where('IDFBADetail', $id);
    $this->db->delete('tbl_pembelian_asset_detail');
    return TRUE;
}

public function update_master($id,$data){
    return $this->db->update('tbl_pembelian_asset', $data, array('IDFBA' => $id));
}
public function update_detail($id,$data){
    return $this->db->update('tbl_pembelian_asset_detail', $data, array('IDFBADetail' => $id));
}
function update($data, $where)
{
    return $this->db->update('tbl_pembelian_asset_detail', $data, $where);
}

public function get_asset()
{
    return $this->db->get("tbl_asset")->result();
}

public function get_group_asset()
{
    return $this->db->get("tbl_group_asset")->result();
}
function get_IDAsset($assetid)
{
    return $this->db->get_where("tbl_asset", array('Nama_Asset' => $assetid))->row();
}
function get_IDgroupasset($groupassetid)
{
    return $this->db->get_where("tbl_group_asset", array('Group_Asset' => $groupassetid))->row();
}
function print_pembelian_asset($nomor)
{
    $this->db->select('*')->from("tbl_pembelian_asset")
    ->where("IDFBA", $nomor);
    return $this->db->get()->row();
}
function print_pembelian_asset_detail($id)
{
   $this->db->select("tbl_pembelian_asset_detail.*, tbl_asset.Nama_Asset, tbl_group_asset.Group_Asset")
   ->from("tbl_pembelian_asset_detail")
   ->join("tbl_asset", "tbl_asset.IDAsset=tbl_pembelian_asset_detail.IDAsset")
   ->join("tbl_group_asset", "tbl_group_asset.IDGroupAsset=tbl_pembelian_asset_detail.IDGroupAsset")
   ->where("tbl_pembelian_asset_detail.IDFBA", $id);
   $query= $this->db->get();
   if($query->num_rows()>0)
   {
      return $query->result();
  }
}

function no_asset($tahun)
{
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
    ->where('extract(year from "Tanggal") =', $tahun)
    ->from("tbl_pembelian_asset")
    ->get()->row();
}

function ambil_pembelian_asset()
    {
        $this->db->select("*")
            ->from("tbl_pembelian_asset")
            ->join("tbl_pembelian_asset_detail", "tbl_pembelian_asset.IDFBA=tbl_pembelian_asset_detail.IDFBA")
            ->join("tbl_asset", "tbl_pembelian_asset_detail.IDAsset=tbl_asset.IDAsset");
            
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
 function add_penyusutan_asset_kredit($data)
{
    $this->db->insert('tbl_jurnal', $data);
    return TRUE;
}

 function add_penyusutan_asset_debet($data)
{
    $this->db->insert('tbl_jurnal', $data);
    return TRUE;
}

function tampilkan_id_jurnal()
{
    $this->db->select("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}


}