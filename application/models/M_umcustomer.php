<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* 
*/
class M_umcustomer extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_umcustomer()
	{
		$this->db->select("*")
		->from("tbl_um_customer um")
		->join("tbl_customer c", "um.IDCustomer=c.IDCustomer");
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampilkan_urutan_umcustomer()
    {
        $this->db->select("IDUMCustomer")
        ->from("tbl_um_customer");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
	/*hapus data*/
	function hapus_data_umcustomer($id)
	{
		$this->db->where('IDUMCustomer', $id);
		$this->db->delete('tbl_um_customer');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	function get_customer()
	{
		$query=$this->db->query("SELECT * FROM tbl_customer");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_um_customer', $data);
		return TRUE;
	}

	function get_umcustomer_byid($id)
	{
		return $this->db->get_where('tbl_um_customer', array('IDUMCustomer'=>$id))->row();
	}

	function update_umcustomer($id, $data)
	{
		$this->db->where('IDUMCustomer', $id);
		$this->db->update('tbl_um_customer', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_um_customer um")
		->join("tbl_customer c", "um.IDCustomer=c.IDCustomer")
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_um_customer um")
		->join("tbl_customer c", "um.IDCustomer=c.IDCustomer")
		->like("Nomor_Faktur", $keyword)
		->or_like("Nama", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>