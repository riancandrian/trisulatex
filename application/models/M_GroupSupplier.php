<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* 
*/
class M_GroupSupplier extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_groupsupplier()
	{
		$query=$this->db->query("SELECT * FROM tbl_group_supplier");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/*hapus data*/
	function hapus_data_groupsupplier($id)
	{
		$this->db->where('IDGroupSupplier', $id);
		$this->db->delete('tbl_group_supplier');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}

	function tampilkan_id_GroupSupplier()
	{
		$this->db->select_max("IDGroupSupplier")
		->from("tbl_group_supplier");
		$query = $this->db->get();

		return $query->row();
	}

	function get_po($id)
    {
        return $this->db->select("tbl_suplier.IDGroupSupplier")
      ->from("tbl_purchase_order")
      ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_purchase_order.IDSupplier")
      ->where("tbl_suplier.IDGroupSupplier", $id)
      ->get()->row();
    }

    function get_po_umum($id)
    {
        return $this->db->select("tbl_suplier.IDGroupSupplier")
      ->from("tbl_purchase_order_umum")
      ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_purchase_order_umum.IDSupplier")
      ->where("tbl_suplier.IDGroupSupplier", $id)
      ->get()->row();
    }

	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_group_supplier', $data);
		return TRUE;
	}

	function get_groupsupplier_byid($id)
	{
		return $this->db->get_where('tbl_group_supplier', array('IDGroupSupplier'=>$id))->row();
	}

	function update_groupsupplier($id, $data)
	{
		$this->db->where('IDGroupSupplier', $id);
		$this->db->update('tbl_group_supplier', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
			$this->db->select("*")
		->from("tbl_group_supplier")
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
			$this->db->select("*")
		->from("tbl_group_supplier")
		->like("Kode_Group_Supplier", $keyword)
		->or_like("Group_Supplier", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari($kolom,$status,$keyword){
		$this->db->select("*")
		->from("tbl_group_supplier")
		->like("Aktif", $status)
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
function cari_by_status($status){
		$this->db->select("*")
		->from("tbl_group_supplier")
		->where("Aktif", $status);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>