<?php

class M_laporan_penerimaan extends CI_Model
{
    private $table1 = 'tbl_terima_barang_supplier';
    private $table2 = 'tbl_terima_barang_supplier_detail';
    private $table3 = 'tbl_corak';
    private $table4 = 'tbl_tampungan';
    private $table5 = 'tbl_in';
    private $table6 = 'tbl_stok';
    private $table7 = 'tbl_kartu_stok';
    private $table8 = 'tbl_merk';
    private $table9 = 'tbl_warna';
    private $table10 = 'tbl_satuan';
    private $table11 = 'tbl_suplier';
    private $table12 = 'tbl_barang';
    private $table13 = 'tbl_gudang';

    function all($jenis_tbs)
    {
        /*$this->db->distinct();
        $this->db->select(
            $this->table1 . '.*,' .
            $this->table2 . '.Barcode,' .
            $this->table2 . '.Grade,' .
            $this->table3 . '.Corak,' .
            $this->table9 . '.Warna,' .
            $this->table8 . '.Merk')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
            ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            ->join($this->table9, $this->table2 . '.IDWarna = ' . $this->table9 . '.IDWarna')
            ->join($this->table8, $this->table2 . '.IDMerk = ' . $this->table8 . '.IDMerk')
            ->where($this->table1 . '.Jenis_TBS', 'PB');

        $query = $this->db->order_by($this->table1 . '.IDTBS', 'DESC')->get();
        return $query->result();*/
        $query = ('SELECT * FROM laporan_penerimaan_barang(?)');

        return $this->db->query($query, array($jenis_tbs))->result();
    }

   public function searching_store($date_from, $date_until, $keyword, $jenis_tbs)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_penerimaan_barang(?, ?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords, $jenis_tbs))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like($keyword, $jenis_tbs)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_penerimaan_barang_like(?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords, $jenis_tbs))->result();
  }

  function all_umum()
    {
        $query = ('SELECT * FROM laporan_penerimaan_umum()');

        return $this->db->query($query)->result();
    }

   public function searching_store_umum($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_penerimaan_umum(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like_umum($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_penerimaan_umum_like(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }

   function all_jahit()
    {
        $query = ('SELECT * FROM laporan_penerimaan_jahit()');

        return $this->db->query($query)->result();
    }

   public function searching_store_jahit($date_from, $date_until, $keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_penerimaan_jahit(?, ?, ?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($date_from, $date_until, $new_keywords))->result();
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like_jahit($keyword)
  {

    $new_keywords = '%'.$keyword.'%';

    $query = ('
      SELECT * FROM cari_laporan_penerimaan_jahit_like(?)
    ');

    /*$query = ('select * from pengukuran');    */

    return $this->db->query($query, array($new_keywords))->result();
  }

  public function exsport_excel_umum() {
        $this->db->select(array("tbl_terima_barang_supplier_umum.Tanggal", "tbl_terima_barang_supplier_umum.Nomor", "tbl_suplier.Nama", "tbl_barang.Nama_Barang", "tbl_terima_barang_supplier_umum_detail.Qty", "tbl_satuan.Satuan", "tbl_terima_barang_supplier_umum_detail.Harga_satuan", "tbl_terima_barang_supplier_umum_detail.Sub_total"));
        $this->db->from('tbl_terima_barang_supplier_umum');
        $this->db->join('tbl_terima_barang_supplier_umum_detail', "tbl_terima_barang_supplier_umum.IDTBSUmum=tbl_terima_barang_supplier_umum_detail.IDTBSUmum");
        $this->db->join('tbl_barang', "tbl_barang.IDBarang=tbl_terima_barang_supplier_umum_detail.IDBarang");
        $this->db->join('tbl_suplier', "tbl_suplier.IDSupplier=tbl_terima_barang_supplier_umum.IDSupplier");
        $this->db->join('tbl_satuan', "tbl_satuan.IDSatuan=tbl_terima_barang_supplier_umum_detail.IDSatuan");
        $this->db->where("tbl_terima_barang_supplier_umum.Batal", 'aktif');
        $this->db->order_by("tbl_terima_barang_supplier_umum.Nomor", 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
}