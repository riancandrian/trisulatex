<!-- Programmer : Rais Naufal Hawari
     Date       : 14-07-2018 -->

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_MenuDetail extends CI_Model {

  // Set nama table sebagai const
  private $table = 'tbl_menu_detail';
  private $order = 'DESC';
  private $id = 'IDMenuDetail';
  // Fungsi Select semua data Menu
  public function get_all()
  {
    $this->db->order_by($this->id, $this->order);
    $this->db->select('IDMenuDetail, Menu_Detail, Menu');
    $this->db->join('tbl_menu', 'tbl_menu.IDMenu = tbl_menu_detail.IDMenu', 'left');
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  //Get Data by ID
  public function get_by_id($id)
  {
    $this->db->where($this->id, $id);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->row();
    } 
    
    
  }

  //Fungsi Tambah Data
  public function insert($data)
  {
    $this->db->insert($this->table, $data);
  }

  //Fungsi Ubah Data
  public function update($id, $data)
  {
    $this->db->where($this->id, $id);
    $this->db->update($this->table, $data);
  }

  //Fungsi Hapus Data
  public function delete($id)
  {
    $this->db->where($this->id, $id);
    $this->db->delete($this->table);
  }

  function cari_by_kolom($kolom,$keyword){
     $this->db->select("*")
    ->from("tbl_menu_detail md")
    ->join("tbl_menu m", "md.IDMenu=m.IDMenu")
    ->like("$kolom", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }


  function cari_by_keyword($keyword){
    $this->db->select("*")
    ->from("tbl_menu_detail md")
    ->join("tbl_menu m", "md.IDMenu=m.IDMenu")
    ->like("Menu", $keyword)
    ->or_like("Menu_Detail", $keyword);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }



}

/* End of file M_MenuDetail.php */
