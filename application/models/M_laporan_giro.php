<?php

class M_laporan_giro extends CI_Model
{

    public function searching_store($tanggalmulai, $tanggalselesai, $jenisgiro, $statusgiro, $keyword, $detailkeyword)
  {

    $this->db->select("tbl_pembayaran_hutang.Tanggal, tbl_pembayaran_hutang.Nomor, tbl_giro.Tanggal_Cair, tbl_mutasi_giro.Total, tbl_agen.Nama_Perusahaan, tbl_giro.Nomor_Giro")
    ->from("tbl_mutasi_giro")
    ->join("tbl_mutasi_giro_detail", "tbl_mutasi_giro.IDMG=tbl_mutasi_giro_detail.IDMG")
    ->join("tbl_giro", "tbl_giro.IDGiro=tbl_mutasi_giro_detail.IDGiro")
    ->join("tbl_agen", "tbl_giro.IDPerusahaan=tbl_agen.IDAgen")
    ->join("tbl_pembayaran_hutang_bayar", "tbl_giro.IDBank=tbl_pembayaran_hutang_bayar.IDCOA")
    ->join("tbl_pembayaran_hutang", "tbl_pembayaran_hutang.IDBS=tbl_pembayaran_hutang_bayar.IDBS")
    ->where("tbl_mutasi_giro.Tanggal >=", $tanggalmulai)
    ->where("tbl_mutasi_giro.Tanggal <=", $tanggalselesai)
    ->where("tbl_mutasi_giro_detail.Jenis_giro =", $jenisgiro)
    ->where("tbl_giro.Status_Giro =", $statusgiro)
    ->like("$keyword", $detailkeyword);
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  //--------------------------Searching Store Procedure
  public function searching_store_like($tanggalmulai, $tanggalselesai, $jenisgiro, $statusgiro)
  {

    $query = ('
      SELECT * FROM cari_laporan_giro_like(?, ?, ?, ?)
    ');


    return $this->db->query($query, array($tanggalmulai, $tanggalselesai, $jenisgiro, $statusgiro))->result();
  }

}