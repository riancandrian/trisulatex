<?php

class M_sample_kain extends CI_Model
{
    private $table1 = 'tbl_penjualan_sample';
    private $table2 = 'tbl_penjualan_sample_detail';
    private $table3 = 'tbl_corak';
    private $table4 = 'tbl_satuan';
    private $table5 = 'tbl_customer';
    private $table6 = 'tbl_barang';
    private $table7 = 'tbl_warna';
    private $table8 = 'tbl_merk';

    public function all()
    {
        $this->db->select(
            $this->table1 . '.*,' .
            $this->table2 . '.*,' .
            $this->table3 . '.Corak,' .
            $this->table4 . '.Satuan'
        )
        ->from($this->table1)
        ->join($this->table2, $this->table2 . '.IDFJSP = ' . $this->table1 . '.IDFJSP')
        ->join($this->table3, $this->table3 . '.IDCorak = ' . $this->table2 . '.IDCorak')
        ->join($this->table4, $this->table4 . '.IDSatuan = '. $this->table2 .'.IDSatuan')
        ->order_by($this->table1 . '.IDFJSP', 'DESC');
        $data = $this->db->get()->result();
        return $data;
    }

    function last_id($tahun)
    {
        return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
        ->where('extract(year from "Tanggal") =', $tahun)
        ->from($this->table1)
        ->get()->row();
    }

    public function getstok($barcode)
    {
        $this->db->where('Barcode', $barcode);
        $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_stok.IDCorak', 'left');
        $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk', 'left');
        $this->db->join('tbl_warna', 'tbl_warna.IDWarna = tbl_stok.IDWarna', 'left');
        $this->db->join('tbl_satuan', 'tbl_satuan.IDSatuan = tbl_stok.IDSatuan', 'left');

        return $this->db->get('tbl_stok')->result_array();
    }

    function tampilkan_id_sample_kain()
    {
        $this->db->select("IDFJSP")
        ->from($this->table1);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_sample_kain_detail()
    {
        $this->db->select("IDFJSPDetail")
        ->from($this->table2);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function add($data){
        $this->db->insert($this->table1, $data);
        return TRUE;
    }

    public function add_detail($data){
        $this->db->insert($this->table2, $data);
        return TRUE;
    }

    public function find($id, $nomor)
    {
        $this->db->select('*')
        ->from($this->table1)
        ->join($this->table5, $this->table1 . '.IDCustomer = ' . $this->table5 . '.IDCustomer');

        if ($nomor != '_') {
            $this->db->where($this->table1 . '.Nomor', $nomor);
        } else {
            $this->db->where($this->table1 . '.IDFJSP', $id);
        }

        return $this->db->get()->row();
    }

    public function find_detail($id, $nomor)
    {
        $this->db->select(
            $this->table2 . '.*,' .
            $this->table3 . '.Corak,' .
            $this->table4 . '.Satuan,' .
            $this->table6 . '.Nama_Barang,' .
            $this->table7 . '.Warna,' .
            $this->table8 . '.Merk')
        ->from($this->table1)
        ->join($this->table2, $this->table1 . '.IDFJSP = ' . $this->table2 . '.IDFJSP')
        ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
        ->join($this->table4, $this->table2 . '.IDSatuan = ' . $this->table4 . '.IDSatuan')
        ->join($this->table6, $this->table2 . '.IDBarang = ' . $this->table6 . '.IDBarang')
        ->join($this->table7, $this->table2 . '.IDWarna = ' . $this->table7 . '.IDWarna', 'left')
        ->join($this->table8, $this->table2 . '.IDMerk = ' . $this->table8 . '.IDMerk');

        if ($nomor != '_') {
            $this->db->where($this->table1 . '.Nomor', $nomor);
        } else {
            $this->db->where($this->table1 . '.IDFJSP', $id);
        }

        return $this->db->get()->result();
    }

    function update_status($data, $where)
    {
        return $this->db->update($this->table1, $data, $where);
    }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.*,' .
            $this->table2 . '.*,' .
            $this->table3 . '.Corak,' .
            $this->table4 . '.Satuan'
        )
        ->from($this->table1)
        ->join($this->table2, $this->table2 . '.IDFJSP = ' . $this->table1 . '.IDFJSP')
        ->join($this->table3, $this->table3 . '.IDCorak = ' . $this->table2 . '.IDCorak')
        ->join($this->table4, $this->table4 . '.IDSatuan = '. $this->table2 .'.IDSatuan');

        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => trim($keyword));
            $this->db->like($array);
        }

        return $this->db->get()->result();
    }

    public function update_stock($stock, $barcode)
    {
        return $this->db->update('tbl_stok', $stock, array('Barcode' => $barcode));
    }
    function tampilkan_id_jurnal()
    {
        $this->db->select("IDJurnal")
        ->from("tbl_jurnal");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
    function save_jurnal($data)
    {
        $this->db->insert("tbl_jurnal", $data);
        return TRUE;
    }
}