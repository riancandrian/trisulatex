<?php

/**
 *
 */
class M_bank extends CI_Model
{
    function tampilkan_bank()
    {
        $this->db->select("TB.*, TC.Nama_COA")
            ->from("tbl_bank TB")
            ->join("tbl_coa TC", "TB.IDCoa = TC.IDCoa");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_bank()
    {
        $this->db->select_max("IDBank")
            ->from("tbl_bank");
        $query = $this->db->get();
        return $query->row();
    }

    function get_coa_inv_umum($id)
    {
        return $this->db->select("IDCOA")
      ->from("tbl_pembelian_umum_pembayaran")
      ->where("IDCOA", $id)
      ->get()->row();
    }

    function get_coa_jurnal($id)
    {
        return $this->db->select("IDCOA")
      ->from("tbl_jurnal")
      ->where("IDCOA", $id)
      ->get()->row();
    }

    function simpan($data)
    {
        $this->db->insert('tbl_bank', $data);
        return TRUE;
    }

    function coa()
    {
        return $this->db->get('tbl_coa')->result();
    }

    function getById($id)
    {
        return $this->db->get_where('tbl_bank', array('IDBank' => $id))->row();
    }

    function update($data, $where)
    {
        return $this->db->update('tbl_bank', $data, $where);
    }

    function getDetailId($id)
    {
        return $this->db->select('*')
            ->from('tbl_bank')
            ->join('tbl_coa', 'tbl_bank.IDCoa = tbl_coa.IDCoa')
            ->where('tbl_bank.IDBank', $id)->get()->row();
    }

    function delete($id)
    {
        $this->db->where('IDBank', $id);
        $this->db->delete('tbl_bank');
    }

    function cari($kolom,$status,$keyword){
         $this->db->select("TB.*, TC.Nama_COA, TC.Status")
        ->from("tbl_bank TB")
        ->join("tbl_coa TC", "TB.IDCoa = TC.IDCoa")
        ->like("TB.Aktif", $status)
        ->like("$kolom", $keyword);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_kolom($kolom,$keyword){
       $this->db->select("TB.*, TC.Nama_COA, TC.Status")
            ->from("tbl_bank TB")
            ->join("tbl_coa TC", "TB.IDCoa = TC.IDCoa")
            ->like("$kolom", $keyword);
            $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_status($status){
    $this->db->select("TB.*, TC.Nama_COA, TC.Status")
        ->from("tbl_bank TB")
        ->join("tbl_coa TC", "TB.IDCoa = TC.IDCoa")
        ->where("TB.Aktif", $status);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    function cari_by_keyword($keyword){
          $this->db->select("TB.*, TC.Nama_COA, TC.Status")
        ->from("tbl_bank TB")
        ->join("tbl_coa TC", "TB.IDCoa = TC.IDCoa")
        ->like("TB.Nomor_Rekening", $keyword)
        ->or_like("TC.Nama_COA", $keyword)
        ->or_like("TB.Atas_Nama", $keyword);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }
}