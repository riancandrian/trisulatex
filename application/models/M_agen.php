<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_agen extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_agen()
	{
		$this->db->select("TA.*, TK.Kota")
		->from("tbl_agen TA")
		->join("tbl_kota TK", "TA.IDKota = TK.IDKota");
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function tampil_kota()
	{
		$query= $this->db->query("SELECT * FROM tbl_kota");

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/*hapus data*/
	function hapus_data_agen($id)
	{
		$this->db->where('IDAgen', $id);
		$this->db->delete('tbl_agen');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function tambah_agen($data)
	{
		$this->db->insert('tbl_agen', $data);
		return TRUE;
	}

	function ambil_agen_byid($id)
	{
		return $this->db->get_where('tbl_agen', array('IDAgen'=>$id))->row();
	}

	function aksi_edit_agen($id, $data)
	{
		$this->db->where('IDAgen', $id);
		$this->db->update('tbl_agen', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("a.*, k.Kota")
		->from("tbl_agen a")
		->join("tbl_kota k", "a.IDKota=k.IDKota")
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_status($status){
		$this->db->select("a.*, k.Kota")
		->from("tbl_agen a")
		->join("tbl_kota k", "a.IDKota=k.IDKota")
		->where("a.Aktif", $status);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("a.*, k.Kota")
		->from("tbl_agen a")
		->join("tbl_kota k", "a.IDKota=k.IDKota")
		->like("Kode_Perusahaan", $keyword)
		->or_like("Nama_Perusahaan", $keyword)
		->or_like("Initial", $keyword)
		->or_like("Alamat", $keyword)
		->or_like("No_Tlp", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari($kolom,$status,$keyword){
		$this->db->select("a.*, k.Kota")
		->from("tbl_agen a")
		->join("tbl_kota k", "a.IDKota=k.IDKota")
		->like("a.Aktif", $status)
		->like("$kolom", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function getDetailId($id)
	{
		return $this->db->select('*')
		->from('tbl_agen')
		->join('tbl_kota', 'tbl_kota.IDKota = tbl_agen.IDKota')
		->where('tbl_agen.IDAgen', $id)->get()->row();
	}


}
?>