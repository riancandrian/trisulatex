<?php

class M_penerimaan_piutang extends CI_Model
{
    private $table1 = 'tbl_penerimaan_piutang';
    private $table2 = 'tbl_penerimaan_piutang_detail';
    private $table3 = 'tbl_penerimaan_piutang_bayar';
    private $table4 = 'tbl_customer';
    private $table5 = 'tbl_piutang';

    function all()
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.Nomor as Nomor_PP,'.
            $this->table1 . '.*,' .
            $this->table2 . '.*,' .
            $this->table3 . '.*,' .
            $this->table4 . '.*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTP = ' . $this->table2 . '.IDTP')
            ->join($this->table3, $this->table1 . '.IDTP = ' . $this->table3 . '.IDTP')
            ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer');

        return $this->db->get()->result();
    }

    function get_customer()
    {
        $query = $this->db->select(
            $this->table4 . '.IDCustomer,' .
            $this->table4 . '.Nama'
        )
            ->from($this->table4)
            ->join($this->table5, $this->table4 . '.IDCustomer = ' . $this->table5 . '.IDCustomer')
            ->where($this->table5 . '.Saldo_Akhir !=', 0)
            ->group_by( $this->table4 . '.IDCustomer,' .
            $this->table4 . '.Nama')
            ->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampil_customer()
    {
        return $this->db->get($this->table4)->result();
    }

    function last_code($tahun)
    {
        return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
            ->where('extract(year from "Tanggal") =', $tahun)
            ->from($this->table1)
            ->get()->row();
    }

    function check_piutang($IDCustomer)
    {
        $this->db->select('*')
            ->from($this->table5)
            ->where('IDCustomer', $IDCustomer)
            ->where('Saldo_Akhir !=', 0);

        $query = $this->db->get();

        return $query->result();
    }


        function getcoa_transfer($jenis)
    {
       $this->db->select("*")
       ->from("tbl_coa")
       ->where('substring("Nama_COA" from 1 for 4)=', 'Bank');
       $query= $this->db->get();
       if($query->num_rows()>0)
       {
          return $query->result();
      }
  }

  function getcoa_giro($jenis)
  {
    $this->db->select("*")->from("tbl_coa")
    ->where("IDCoa", 'P000624');
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function getcoa_cash($jenis)
  {
    $this->db->select("*")
       ->from("tbl_coa")
       ->where('substring("Nama_COA" from 1 for 3)=', 'Kas');
       $query= $this->db->get();
       if($query->num_rows()>0)
       {
          return $query->result();
      }
}

function getcoa_pembulatan($jenis)
  {
    $this->db->select("*")
       ->from("tbl_coa");
       $query= $this->db->get();
       if($query->num_rows()>0)
       {
          return $query->result();
      }
}

function getcoa_nota_kredit($supplier)
  {
    $this->db->select_sum("Nominal")->from("tbl_um_supplier")
    ->where("IDSupplier", $supplier);
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}


        function searching_laporan_penerimaan_piutang($date_from, $date_until, $search_type, $keyword)
    {


      $this->db->join("tbl_penerimaan_piutang_detail", "tbl_penerimaan_piutang_detail.IDTP=tbl_penerimaan_piutang.IDTP");
      $this->db->join("tbl_penerimaan_piutang_bayar", "tbl_penerimaan_piutang_bayar.IDTP=tbl_penerimaan_piutang.IDTP");
      $this->db->join("tbl_piutang", "tbl_piutang.IDPiutang=tbl_penerimaan_piutang_detail.IDPiutang");
      $this->db->join("tbl_customer", "tbl_customer.IDCustomer=tbl_penerimaan_piutang.IDCustomer");
      $this->db->join("tbl_coa", "tbl_coa.IDCoa=tbl_penerimaan_piutang_bayar.IDCOA");

      if ($date_from) {
        $this->db->where("Tanggal >= ", $date_from);
    }

    if ($date_until) {
        $this->db->where("Tanggal <= ", $date_until);
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }
    $this->db->where("tbl_piutang.Saldo_Akhir !=", 0);
    $query = $this->db->order_by("tbl_penerimaan_piutang.IDTP", 'DESC')->get("tbl_penerimaan_piutang");
    return $query->result();
}

    function tampil_bank()
    {
        return $this->db->get('tbl_coa')->result();
    }

    function tampilkan_id_hutang()
    {
        $this->db->select("IDTP")->from($this->table1);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_hutang_detail()
    {
        $this->db->select("IDTPDet")->from($this->table2);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_id_hutang_bayar()
    {
        $this->db->select("IDTPBayar")->from($this->table3);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function add($data){
        $this->db->insert($this->table1, $data);
        return TRUE;
    }

    public function add_um_customer($dataum){
        $this->db->insert("tbl_um_customer", $dataum);
        return TRUE;
    }

    public function add_detail($data){
        $this->db->insert($this->table2, $data);
        return TRUE;
    }

    public function add_bayar($data){
        $this->db->insert($this->table3, $data);
        return TRUE;
    }

    public function update_piutang($Pembayaran, $saldo_akhir, $id_piutang)
    {
        $set = array(
            'Pembayaran' => $Pembayaran,
            'Saldo_Akhir' => $saldo_akhir
        );

        return $this->db->update($this->table5, $set, array('IDPiutang' => $id_piutang));
    }

    public function find($id, $nomor)
    {
        $this->db->select(
            $this->table1 . '.*,' .
            $this->table4 . '.Nama as Nama_customer'
        );
        $this->db->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTP = ' . $this->table2 . '.IDTP')
            ->join($this->table3, $this->table1 . '.IDTP = ' . $this->table3 . '.IDTP')
            ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer');

        if ($id != '_') {
            $this->db->where($this->table1 . '.IDTP', $id);
        } else {
            $this->db->where($this->table1 . '.Nomor', $nomor);
        }

        return $this->db->get()->row();
    }

    public function find_detail($id, $nomor)
    {
        $this->db->distinct();
        $this->db->select(
            $this->table2 . '.*'
        );
        $this->db->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTP = ' . $this->table2 . '.IDTP');
            //->join($this->table5, $this->table2 . '.IDCorak = ' . $this->table5 . '.IDCorak')
            //->join($this->table6, $this->table2 . '.IDBarang = ' . $this->table6 . '.IDBarang')
            //->join($this->table7, $this->table2 . '.IDWarna = ' . $this->table7 . '.IDWarna');

        if ($id != '_') {
            $this->db->where($this->table1 . '.IDTP', $id);
        } else {
            $this->db->where($this->table1 . '.Nomor', $nomor);
        }

        return $this->db->get()->result();
    }

    function find_bayar($id)
    {
        $this->db->select('*');
        $this->db->from($this->table3);
        $this->db->where($this->table3 . '.IDTP', $id);

        return $this->db->get()->row();
    }

    function update_status($data, $where)
    {
        return $this->db->update($this->table1, $data, $where);
    }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->distinct();
        $this->db->select(
            $this->table1 . '.IDTP,' .
            $this->table1 . '.Tanggal,' .
            $this->table1 . '.Nomor as Nomor_PP,' .
            $this->table1 . '.Total,' .
            $this->table1 . '.Pembayaran,' .
            $this->table1 . '.Keterangan,' .
            $this->table1 . '.Batal,' .
            $this->table3 . '.*,' .
            $this->table4 . '.Nama'
        )
            ->from($this->table1)
            ->join($this->table3, $this->table1 . '.IDTP = ' . $this->table3 . '.IDTP')
            ->join($this->table4, $this->table1 . '.IDCustomer = ' . $this->table4 . '.IDCustomer');

        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by($this->table1 . '.IDTP', 'DESC')->get();
        return $query->result();
    }

    function piutang_bayar($IDCustomer)
    {
        $this->db->select('*')
            ->from($this->table5)
            ->join($this->table1, $this->table5 . '.IDCustomer = ' . $this->table1 . '.IDCustomer')
            ->where($this->table5 . '.IDCustomer', $IDCustomer)
            ->where($this->table5 . '.Saldo_Akhir', 0);

        $query = $this->db->get();

        return $query->result();
    }

    public function update_master($id,$data){
        return $this->db->update($this->table1, $data, array('IDTP' => $id));
    }

    public function update_bayar($id,$datas){
        return $this->db->update($this->table3, $datas, array('IDTP' => $id));
    }

    public function get_penjualan($nomor)
    {
        return $this->db->get_where('tbl_piutang', array('No_Faktur' => $nomor))->row();
    }
        function tampilkan_id_jurnal()
{
    $this->db->select("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}
function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}
}