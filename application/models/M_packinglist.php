<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

/**
* 
*/
class M_packinglist extends CI_Model
{
	
  private $table = 'tbl_packing_list';
  private $table_detail = 'tbl_packing_list_detail';
  private $table_customer = 'tbl_customer';
  private $table_barang = 'tbl_barang';
  private $table_harga_jual = 'tbl_harga_jual_barang';  
  private $table_group_barang = 'tbl_group_barang';
  private $table_merk = 'tbl_merk';
  private $table_sales_order_kain = 'tbl_sales_order_kain';
  private $table_satuan = 'tbl_satuan';
  private $table_corak = 'tbl_corak';
  private $table_stok = 'tbl_stok';
  private $table_warna = 'tbl_warna';
  private $table_mata_uang = 'tbl_mata_uang';
  private $order = 'DESC';
  private $id = 'IDPAC';
  private $id_detail = 'IDPACDetail';

  // Fungsi Select semua data Menu
  public function get_all()
  {

    $this->db->order_by($this->id, $this->order);
    // $this->db->where('Jenis_packinglist', $id);
    //$this->db->join($this->table_customer, $this->table_customer.'.IDCustomer ='. $this->table.'.IDCustomer');
    
    // $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_purchase_order.IDCorak', 'left');
    // $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_purchase_order.IDMerk', 'left');
    
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
   } 
 }
function tampilkan_get_packing()
{
  $this->db->select("IDPAC")
  ->from("tbl_surat_jalan_customer_kain");
  $query = $this->db->get();

  if ($query->num_rows() > 0)
  {
    return $query->result();
  }
}

function save_stock_card($data)
{
    $this->db->insert("tbl_kartu_stok", $data);
    return TRUE;
}

 function tampilkan_id_packing_list()
 {
  $this->db->select_max("IDPAC")
   ->from("tbl_packing_list");
   $query = $this->db->get();

   return $query->row();
}
function tampilkan_id_kartu_stok()
{
    $this->db->select_max("IDKartuStok")
    ->from("tbl_kartu_stok");
    $query = $this->db->get();

   return $query->row();
}

function tampilkan_id_packing_list_detail()
{
  $this->db->select_max($this->id_detail)
  ->from($this->table_detail);
  $query = $this->db->get();

   return $query->row();
}

public function get_all_procedure_packing()
{

  $query = ('
    SELECT * FROM laporan_packing_list()
    ');

  /*$query = ('select * from pengukuran');  */

  return $this->db->query($query)->result();
}

  //--------------Get Data by ID
public function get_by_id($id)
{
  $this->db->join($this->table_customer, $this->table_customer.'.IDCustomer ='. $this->table.'.IDCustomer');
  $this->db->join($this->table_mata_uang, $this->table_mata_uang.'.IDMataUang ='. $this->table.'.IDMataUang');
  $this->db->where($this->id, $id);
  $data = $this->db->get($this->table);

  $hitung = $this->db->count_all_results($this->table);
  if ( $hitung > 0) {
   return $data->row();
 } 


}

public function get_packinglist_detail($id)
{
  $this->db->join($this->table_barang, $this->table_barang.'.IDBarang ='. $this->table_detail.'.IDBarang');
  $this->db->join($this->table_satuan, $this->table_satuan.'.IDSatuan ='. $this->table_detail.'.IDSatuan');
  $this->db->join($this->table_corak, $this->table_corak.'.IDCorak ='. $this->table_detail.'.IDCorak');
  $this->db->join($this->table_warna, $this->table_warna.'.IDWarna ='. $this->table_detail.'.IDWarna');
  $this->db->join($this->table_merk, $this->table_merk.'.IDMerk ='. $this->table_detail.'.IDMerk');
  $this->db->where($this->id, $id);
  $data = $this->db->get($this->table_detail);
  $hitung = $this->db->count_all_results($this->table);
  if ( $hitung > 0) {
   return $data->result();
 } 


}


public function getstok($barcode, $IDSOK)
{
  $this->db->where('Barcode', $barcode);
  $hasil = $this->db->get('tbl_stok')->row();
  if ($hasil) {
    $where = array(
      'IDBarang' => $hasil->IDBarang,
      'IDSOK' => $IDSOK
    );
    $this->db->where($where);
    $data = $this->db->get('tbl_sales_order_kain_detail')->result();
      //$hitung = $this->db->count_all_results('tbl_sales_order_kain_detail');
    if ( count($data) > 0) {
      $this->db->where('Barcode', $barcode);
        //Get Merk, Corak
      $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_stok.IDCorak', 'left');
      $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk', 'left');
        //Get Warna
      $this->db->join('tbl_warna', 'tbl_warna.IDWarna = tbl_stok.IDWarna', 'left');
        //get Saruan
      $this->db->join('tbl_satuan', 'tbl_satuan.IDSatuan = tbl_stok.IDSatuan', 'left');
      $hasil2 = $this->db->get('tbl_stok')->result_array();
      return $hasil2;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

public function getstokbyid($id)
{
  $this->db->select('IDStok, Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Harga, Total');

  $this->db->where('Barcode', $id);
  return $this->db->get($this->table_stok)->row();
}

public function updatestok($data,$id)
{
  $this->db->where('Barcode', $id);
  $this->db->update($this->table_stok, $data);
}

  //Fungsi Tambah Data
public function insert($data)
{
  $this->db->insert($this->table, $data);
}

  //Fungsi Ubah Data
public function update($id, $data)
{
  $this->db->where($this->id, $id);
  $this->db->update($this->table, $data);
}

  //Fungsi Hapus Data
public function delete($id)
{
  $this->db->where($this->id, $id);
  $this->db->delete($this->table);
}

function cari($kolom,$status,$keyword){
  $this->db->select("*")
  ->from("tbl_merk")
  ->like("Aktif", $status)
  ->like("$kolom", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function cari_by_kolom($kolom,$keyword){
 $this->db->select("*")
 ->from("tbl_merk")
 ->like("$kolom", $keyword);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_status($status){
 $this->db->select("*")
 ->from("tbl_merk")
 ->where("Aktif", $status);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function cari_by_keyword($keyword){
  $this->db->select("*")
  ->from("tbl_merk")
  ->like("Kode_Merk", $keyword)
  ->or_like("Merk", $keyword);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

function get_supplier(){
  $this->db->select("IDSupplier, Nama")
  ->from("tbl_suplier");
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }
}

  //-------------------NO Purchase Order
function no_packinglist($tahun)
{
  return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
  ->where('extract(year from "Tanggal") =', $tahun)
  ->from($this->table)
  ->get()->row();
}

  //-------------------Get Trisula
function get_customer()
{
  $data = $this->db->get($this->table_customer);

  $hitung = $this->db->count_all_results($this->table_customer);
  if ( $hitung > 0) {
   return $data->result();
 } 
}

  //-------------------Get SOK
function get_sok()
{
  $data = $this->db->get($this->table_sales_order_kain);

  $hitung = $this->db->count_all_results($this->table_sales_order_kain);
  if ( $hitung > 0) {
   return $data->result();
 } 
}

  //---------------------GetMerk
function getmerk($id)
{
  $this->db->from('tbl_corak');

  $this->db->join('tbl_barang', 'tbl_barang.IDMerk = tbl_corak.IDMerk');
  $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk');
  $this->db->where('tbl_corak.IDCorak', $id);
    //$this->db->join('tbl_barang', 'tbl_barang.IDMerk = tbl_corak.IDMerk');

  $data = $this->db->get();

  $hitung = $this->db->count_all_results('tbl_corak');
  if ( $hitung > 0) {
   return $data->row();
 } 
}

   //---------------------Getwarna
function getwarna($id)
{
 $this->db->where('Kode_Corak', $id);
 $this->db->join('tbl_corak', 'tbl_warna.IDCorak = tbl_corak.IDCorak', 'left');
 $data = $this->db->get('tbl_warna');
 $hitung = $this->db->count_all_results('tbl_warna');
 if ( $hitung > 0) {
  return $data->result();
} 
}


   //Insert data Atas
public function add($data){
    //$this->db->insert($this->table, $data);
  $this->db->insert($this->table, $data);
  return TRUE;
}

  //Insert Detail nya
public function add_detail($data){
  $this->db->insert($this->table_detail, $data);
  return TRUE;
} 

  //Ubah Data Master
public function update_master($id,$data){
  return $this->db->update($this->table, $data, array($this->id => $id));
}

public function drop($id){
  $this->db->where($this->id_detail, $id);
  $this->db->delete($this->table_detail);
  return TRUE;
}

function searching($date_from, $date_until, $search_type, $keyword)
{


  $this->db->join($this->table_customer, $this->table_customer.'.IDCustomer ='. $this->table.'.IDCustomer');

  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
  }

  if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
  }

  if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
  }

  $query = $this->db->order_by($this->id, 'DESC')->get($this->table);
  return $query->result();
}

  //--------------------------Searching Store Procedure
public function searching_store($id, $date_from, $date_until, $keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_packinglist(?, ?, ?, ?)
    ');

  /*$query = ('select * from pengukuran');	*/

  return $this->db->query($query, array($id, $date_from, $date_until, $new_keywords))->result();
}

  //--------------------------Searching Store Procedure
public function searching_store_like($id, $keyword)
{

  $new_keywords = '%'.$keyword.'%';

  $query = ('
    SELECT * FROM cari_packinglist_like(?, ?)
    ');

  /*$query = ('select * from pengukuran');	*/

  return $this->db->query($query, array($id, $new_keywords))->result();
}

function print_packinglist($nomor)
{
  $this->db->select('*')->from("tbl_packing_list_detail")
  ->join("tbl_packing_list", "tbl_packing_list.IDPAC = tbl_packing_list_detail.IDPAC")
  ->join("tbl_corak", "tbl_packing_list_detail.IDCorak = tbl_corak.IDCorak")
  ->join("tbl_warna", "tbl_packing_list_detail.IDWarna = tbl_warna.IDWarna")
  ->where("tbl_packing_list.IDPAC", $nomor);
 $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
}
  // function print_packinglist_detail($id)
  // {
  //    $this->db->select("*")
  //   ->from($this->table_detail)
  //   ->where("IDPO", $id);
  //   $query= $this->db->get();
  //   if($query->num_rows()>0)
  //   {
  //     return $query->result();
  //   }
  // }
function last_Id()
{
  $this->db->select('IDFBA');
  $this->db->from('tbl_pembelian_asset');
  $this->db->order_by('IDFBA','DESC');
  $this->db->limit(1);
  $query = $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->row();
  }else{
    return "null";
  }
}


  //---------------------get Satuan
function get_satuan()
{
  $query = ('
    select DISTINCT(s."IDSatuan"),s."Satuan" from tbl_satuan s
    JOIN tbl_barang b ON 
    s."IDSatuan" = b."IDSatuan"	
    ');
  $hitung = $this->db->query($query)->result();
  if ( $hitung > 0) {
    return $hitung;
  } 
}




  //----------------Get barang seragam
public function get_barang_seragam()
{
  $this->db->join($this->table_group_barang, $this->table_group_barang.'.IDGroupBarang ='. $this->table_barang.'.IDGroupBarang');
  $this->db->where('Group_Barang !=', 'kain');
  return $this->db->get($this->table_barang)->result();

}




  //-------------------Get Harga Satuan
public function get_harga($id)
{
  $this->db->from($this->table_harga_jual);
  $this->db->join($this->table_barang, $this->table_barang.'.IDBarang ='. $this->table_harga_jual.'.IDBarang');
    // $this->db->join($this->table_corak, $this->table_corak.'.IDCorak ='. $this->table_harga_jual.'.IDCorak');
    // $this->db->join($this->table_merk, $this->table_merk.'.IDMerk ='. $this->table_harga_jual.'.IDMerk');
  $this->db->where($this->table_barang.'.IDBarang', $id);
  $data = $this->db->get();
  $hitung = $this->db->count_all_results('tbl_corak');
  if ( $hitung > 0) {
   return $data->row();
 } 
}

function cekcustomer($SO)
    {
        $this->db->select('tbl_customer."IDCustomer", tbl_customer."Nama"')
        ->from("tbl_sales_order_kain")
        ->join("tbl_customer", "tbl_sales_order_kain.IDCustomer=tbl_customer.IDCustomer")
        ->where("tbl_sales_order_kain.IDSOK", $SO);
        $query = $this->db->get();

        return $query->result();
    }
}
?>