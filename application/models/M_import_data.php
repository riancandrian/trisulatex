<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_import_data extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_importdata()
	{
		$this->db->select("*")
		->from("tbl_import");
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	function insert_csv($data) {
        $this->db->insert('tbl_import', $data);
    }
    function getDetailId($id)
    {
        return $this->db->select("*")
        ->from("tbl_import")
        ->where("IDImport", $id)->get()->row();
    }
    function getDataImport($id)
    {
        return $this->db->get_where('tbl_import', array('IDImport'=>$id))->row();
    }

     function cari_by_kolom($kolom,$keyword){
        $this->db->select("*")
        ->from("tbl_import")
        ->like("$kolom", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }


    function cari_by_keyword($keyword){
         $this->db->select("*")
        ->from("tbl_import")
        ->like("Barcode", $keyword)
        ->or_like("NoSO", $keyword)
        ->or_like("Party", $keyword)
        ->or_like("Indent", $keyword)
        ->or_like("CustDes", $keyword)
        ->or_like("WarnaCust", $keyword)
        ->or_like("Panjang_Yard", $keyword)
        ->or_like("Panjang_Meter", $keyword)
        ->or_like("Grade", $keyword)
        ->or_like("Remark", $keyword)
        ->or_like("Lebar", $keyword)
        ->or_like("Satuan", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

     function simpan($data)
    {
        $this->db->insert('tbl_import', $data);
        return $this->db->insert_id();
    }

    function getById($ambil)
    {
        return $this->db->get_where('tbl_import', array('IDImport'=>$ambil))->row();
    }

    function getDataBarcode($id)
    {
        return $this->db->get_where('tbl_import', array('IDImport'=>$id))->row();
    }


}
?>