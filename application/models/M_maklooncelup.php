<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class M_maklooncelup extends CI_Model {

  private $table = 'tbl_surat_jalan_makloon_celup';
  private $order = 'DESC';
  private $id = 'IDSJM';

  //Get all data + join Supplier
  public function get_all()
  {
    $this->db->join('tbl_suplier', 'tbl_suplier.IDSupplier = '.$this->table.'.IDSupplier');
    $this->db->order_by($this->id, $this->order);
    $data = $this->db->get($this->table);
    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
   } 

 }

 function tampilkan_id_makloon_celup()
 {
  $this->db->select_max("IDSJM")
  ->from("tbl_surat_jalan_makloon_celup");
 $query = $this->db->get();

  return $query->row();
}

    function tampilkan_id_kartu_stok()
{
    $this->db->select_max("IDKartuStok")
    ->from("tbl_kartu_stok");
    $query = $this->db->get();

  return $query->row();
}

function save_stock_card($data)
{
    $this->db->insert("tbl_kartu_stok", $data);
    return TRUE;
}

function tampilkan_id_makloon_celup_detail()
{
  $this->db->select_max("IDSJMDetail")
  ->from("tbl_surat_jalan_makloon_celup_detail");
  $query = $this->db->get();

  return $query->row();
}

  //Get Data by ID
public function get_by_id($id)
{
  return $this->db->select('tbl_surat_jalan_makloon_celup.*, tbl_suplier.Nama, tbl_purchase_order.Nomor AS NomorPO')
  ->from("tbl_surat_jalan_makloon_celup")
  ->join("tbl_suplier", "tbl_surat_jalan_makloon_celup.IDSupplier=tbl_suplier.IDSupplier")
  ->join("tbl_purchase_order", "tbl_purchase_order.IDPO=tbl_surat_jalan_makloon_celup.IDPO")
  ->where("tbl_surat_jalan_makloon_celup". '.IDSJM', $id)
  ->get()->row();
}

public function get_by_id_detail($id)
{
  $this->db->select("tbl_surat_jalan_makloon_celup_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan")
  ->from("tbl_surat_jalan_makloon_celup_detail")
  ->join("tbl_corak", "tbl_surat_jalan_makloon_celup_detail.IDCorak=tbl_corak.IDCorak")
  ->join("tbl_warna", "tbl_surat_jalan_makloon_celup_detail.IDWarna=tbl_warna.IDWarna")
  ->join("tbl_merk", "tbl_surat_jalan_makloon_celup_detail.IDMerk=tbl_merk.IDMerk")
  ->join("tbl_satuan", "tbl_surat_jalan_makloon_celup_detail.IDSatuan=tbl_satuan.IDSatuan")
  ->where("tbl_surat_jalan_makloon_celup_detail.IDSJM", $id);
  $query= $this->db->get();
  if($query->num_rows()>0)
  {
    return $query->result();
  }


}

public function get_by_id2($id)
{
  $this->db->join('tbl_suplier', 'tbl_suplier.IDSupplier = '.$this->table.'.IDSupplier');
    //$this->db->join('tbl_purchase_order', 'tbl_purchase_order.IDPO = '.$this->table.'.IDPO');
  $this->db->where($this->id, $id);
  $data = $this->db->get($this->table);
  $hitung = $this->db->count_all_results($this->table);
  if ( $hitung > 0) {
   return $data->row();
 } 
}

  //Fungsi Ubah Data
public function update($id, $data)
{
  $this->db->where($this->id, $id);
  $this->db->update($this->table, $data);
}

public function update_stok($barcode, $data_stok)
{
  $this->db->where("Barcode", $barcode);
  $this->db->update("tbl_stok", $data_stok);
}

  //-------------------NO Surat Jalan Makloon Celup
function no_makloon($tahun)
{
  return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
  ->where('extract(year from "Tanggal") =', $tahun)
  ->from("tbl_surat_jalan_makloon_celup")
  ->get()->row();
}


  //Get Detail makloon kebutuhan ajax
public function get_makloon_detail($id)
{

    //Get Merk, Corak
  $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_surat_jalan_makloon_celup_detail.IDCorak', 'left');
  $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk', 'left');

    //Get Warna
  $this->db->join('tbl_warna', 'tbl_warna.IDWarna = tbl_surat_jalan_makloon_celup_detail.IDWarna', 'left');

    //get Saruan
  $this->db->join('tbl_satuan', 'tbl_satuan.IDSatuan = tbl_surat_jalan_makloon_celup_detail.IDSatuan', 'left');

  $this->db->where('IDSJM', $id);
  return $this->db->get('tbl_surat_jalan_makloon_celup_detail')->result_array();
}

  //Get stok kebutuhan ajax
public function getstok($barcode)
{
  $this->db->where('Barcode', $barcode);
    //Get Merk, Corak
  $this->db->join('tbl_corak', 'tbl_corak.IDCorak = tbl_stok.IDCorak', 'left');
  $this->db->join('tbl_merk', 'tbl_merk.IDMerk = tbl_corak.IDMerk', 'left');

    //Get Warna
  $this->db->join('tbl_warna', 'tbl_warna.IDWarna = tbl_stok.IDWarna', 'left');

    //get Saruan
  $this->db->join('tbl_satuan', 'tbl_satuan.IDSatuan = tbl_stok.IDSatuan', 'left');


  return $this->db->get('tbl_stok')->result_array();
}


  //Add Data ajax umum
public function add($data){
  $this->db->insert($this->table, $data);
  return TRUE;
}

  //Insert Detail nya
public function add_detail($data){
  $this->db->insert('tbl_surat_jalan_makloon_celup_detail', $data);
  return TRUE;
}

function tampilkan_id_jurnal()
{
    $this->db->select_max("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

  return $query->row();
} 

  //Ubah Data Master
public function update_master($id,$data){
  return $this->db->update($this->table, $data, array('IDSJM' => $id));
}

public function drop($id){
  $this->db->where('IDSJMDetail', $id);
  $this->db->delete('tbl_surat_jalan_makloon_celup_detail');

}

function save_jurnal($data)
{
  $this->db->insert("tbl_jurnal", $data);
  return TRUE;
}


  //------------------pencarian
function searching($date_from, $date_until, $search_type, $keyword)
{


  $this->db->join('tbl_suplier', 'tbl_suplier.IDSupplier = '.$this->table.'.IDSupplier');

  if ($date_from) {
    $this->db->where("Tanggal >= ", $date_from);
  }

  if ($date_until) {
    $this->db->where("Tanggal <= ", $date_until);
  }

  if ($search_type) {
    $array = array($search_type => $keyword);
    $this->db->like($array);
  }

  $query = $this->db->order_by($this->id, $this->order)->get($this->table);
  return $query->result();
}

function print_makloon_celup($nomor)
{
  $this->db->select('*')->from("tbl_surat_jalan_makloon_celup")
  ->where("IDSJM", $nomor);
  return $this->db->get()->row();
}
function print_makloon_celup_detail($id)
{
 $this->db->select("tbl_surat_jalan_makloon_celup_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan, tbl_barang.Kode_Barang")
 ->from("tbl_surat_jalan_makloon_celup_detail")
 ->join("tbl_corak", "tbl_surat_jalan_makloon_celup_detail.IDCorak=tbl_corak.IDCorak")
 ->join("tbl_barang", "tbl_barang.IDBarang=tbl_surat_jalan_makloon_celup_detail.IDBarang")
 ->join("tbl_warna", "tbl_surat_jalan_makloon_celup_detail.IDWarna=tbl_warna.IDWarna")
 ->join("tbl_merk", "tbl_surat_jalan_makloon_celup_detail.IDMerk=tbl_merk.IDMerk")
 ->join("tbl_satuan", "tbl_surat_jalan_makloon_celup_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_surat_jalan_makloon_celup_detail.IDSJM", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function get_harga_po($po)
{
  $this->db->select('Grand_total')->from("tbl_purchase_order")
  ->where("IDPO", $po);
  return $this->db->get()->row();
}

}

/* End of file M_maklooncelup.php */
