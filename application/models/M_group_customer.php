<?php

class M_group_customer extends CI_Model
{
    function tampilkan_group_customer()
    {
        return $this->db->get("tbl_group_customer")->result();
    }

    function simpan($data)
    {
        $this->db->insert('tbl_group_customer', $data);
        return TRUE;
    }

    function getById($id)
    {
        return $this->db->get_where('tbl_group_customer', array('IDGroupCustomer' => $id))->row();
    }

    function update($data, $where)
    {
        return $this->db->update('tbl_group_customer', $data, $where);
    }

    function tampilkan_id_group_customer()
    {
        $this->db->select_max("IDGroupCustomer")
        ->from("tbl_group_customer");
        $query = $this->db->get();

        return $query->row();
    }

    function delete($id)
    {
        $this->db->where('IDGroupCustomer', $id);
        $this->db->delete('tbl_group_customer');
        if($this->db->affected_rows()==1)
        {
            return TRUE;
        }
        return FALSE;
    }

    function get_so_customer($id)
    {
        return $this->db->select("tbl_customer.IDGroupCustomer")
      ->from("tbl_sales_order_kain")
      ->join("tbl_customer", "tbl_customer.IDCustomer=tbl_sales_order_kain.IDCustomer")
      ->where("tbl_customer.IDGroupCustomer", $id)
      ->get()->row();
    }

    function get_sos_customer($id)
    {
        return $this->db->select("tbl_customer.IDGroupCustomer")
      ->from("tbl_sales_order_seragam")
      ->join("tbl_customer", "tbl_customer.IDCustomer=tbl_sales_order_seragam.IDCustomer")
      ->where("tbl_customer.IDGroupCustomer", $id)
      ->get()->row();
    }

    function cari_by_kolom($kolom,$keyword){
        $this->db->select("*")
        ->from("tbl_group_customer")
        ->like("$kolom", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }


    function cari_by_keyword($keyword){
        $this->db->select("*")
        ->from("tbl_group_customer")
        ->like("Kode_Group_Customer", $keyword)
        ->or_like("Nama_Group_Customer", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    
}