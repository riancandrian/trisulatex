<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class M_customer extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_customer()
	{
		// $query=$this->db->query("SELECT tgc.Nama_Group_Customer, tc.Kode_Customer, tc.Nama, tc.Aktif, tc.IDCustomer FROM tbl_customer tc join tbl_group_customer tgc on tc.IDGroupCustomer=tgc.IDGroupCustomer");
		$this->db->select("tgc.Nama_Group_Customer, tc.Kode_Customer, tc.Nama, tc.Aktif, tc.IDCustomer")
		->from("tbl_customer tc")
		->join("tbl_group_customer tgc", "tc.IDGroupCustomer = tgc.IDGroupCustomer");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function tampilkan_id_customer()
	{
		$this->db->select_max("IDCustomer")
		->from("tbl_customer");
		$query = $this->db->get();

		return $query->row();
	}

	function tampil_group_customer()
	{
		$query=$this->db->query("SELECT * FROM tbl_group_customer");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	function get_so_kain($id)
	{
		return $this->db->select("IDCustomer")
		->from("tbl_sales_order_kain")
		->where("IDCustomer", $id)
		->get()->row();
	}
	function get_so_no_kain($id)
	{
		return $this->db->select("IDCustomer")
		->from("tbl_sales_order_seragam")
		->where("IDCustomer", $id)
		->get()->row();
	}
	function tampil_kota()
	{
		$query=$this->db->query("SELECT * FROM tbl_kota");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/*hapus data*/
	function hapus_data_customer($id)
	{
		$this->db->where('IDCustomer', $id);
		$this->db->delete('tbl_customer');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function tambah_customer($data)
	{
		$this->db->insert('tbl_customer', $data);
		return TRUE;
	}

	function ambil_customer_byid($id)
	{
		return $this->db->get_where('tbl_customer', array('IDCustomer'=>$id))->row();
	}

	function aksi_edit_customer($id, $data)
	{
		$this->db->where('IDCustomer', $id);
		$this->db->update('tbl_customer', $data);
		return TRUE;
	}



	function cari($kolom,$status,$keyword){
		$this->db->select("*")
		->from("tbl_customer c")
		->join("tbl_group_customer tbc", "c.IDGroupCustomer=tbc.IDGroupCustomer")
		->join("tbl_kota tb", "c.IDKota=tb.IDKota")
		->like("c.Aktif", $status)
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
		->from("tbl_customer c")
		->join("tbl_group_customer tbc", "c.IDGroupCustomer=tbc.IDGroupCustomer")
		->join("tbl_kota tb", "c.IDKota=tb.IDKota")
		->like("$kolom", $keyword);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_status($status){
		$this->db->select("*")
		->from("tbl_customer c")
		->join("tbl_group_customer tbc", "c.IDGroupCustomer=tbc.IDGroupCustomer")
		->join("tbl_kota tb", "c.IDKota=tb.IDKota")
		->where("c.Aktif", $status);
		$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
		->from("tbl_customer c")
		->join("tbl_group_customer tbc", "c.IDGroupCustomer=tbc.IDGroupCustomer")
		->join("tbl_kota tb", "c.IDKota=tb.IDKota")
		->like("Nama_Group_Customer", $keyword)
		->or_like("Kode_Customer", $keyword)
		->or_like("Nama", $keyword)
		->or_like("Alamat", $keyword)
		->or_like("No_Telpon", $keyword)
		->or_like("Kota", $keyword)
		->or_like("Fax", $keyword)
		->or_like("Email", $keyword)
		->or_like("NPWP", $keyword)
		->or_like("No_KTP", $keyword);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function getDetailId($id)
	{
		return $this->db->select('*')
		->from('tbl_customer')
		->join('tbl_group_customer', 'tbl_group_customer.IDGroupCustomer = tbl_customer.IDGroupCustomer')
		->join('tbl_kota', 'tbl_kota.IDKota = tbl_customer.IDKota')
		->where('tbl_customer.IDCustomer', $id)->get()->row();
	}

}
?>