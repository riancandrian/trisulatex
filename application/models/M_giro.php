<!-- Programmer : Rais Naufal Hawari
     Date       : 12-07-2018 -->

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_giro extends CI_Model {

  // Set nama table sebagai const
  private $table = 'tbl_giro';
  private $order = 'DESC';
  private $id = 'IDGiro';
  // Fungsi Select semua data giro
  public function get_all()
  {
    $this->db->join("tbl_customer","tbl_customer.IDCustomer=tbl_giro.IDPerusahaan");
    $this->db->where("Jenis_Giro", "giro masuk");
    $this->db->order_by($this->id, $this->order);

    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  function tampilkan_id_giro()
    {
        $this->db->select("IDGiro")
            ->from("tbl_giro");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

   public function get_all_keluar()
  {
    $this->db->join("tbl_suplier","tbl_suplier.IDSupplier=tbl_giro.IDPerusahaan");
    $this->db->where("Jenis_Giro", "giro keluar");
    $this->db->order_by($this->id, $this->order);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  public function get_all_bank()
  {
    $this->db->order_by('IDBank', $this->order);
    $data = $this->db->get('tbl_bank');

    $hitung = $this->db->count_all_results('tbl_bank');
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  public function get_all_supplier()
  {
    $this->db->order_by('IDSupplier', $this->order);
    $data = $this->db->get('tbl_suplier');

    $hitung = $this->db->count_all_results('tbl_suplier');
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  public function get_all_customer()
  {
    $this->db->order_by('IDCustomer', $this->order);
    $data = $this->db->get('tbl_customer');

    $hitung = $this->db->count_all_results('tbl_customer');
    if ( $hitung > 0) {
     return $data->result();
    } 
  }

  //Get Data by ID
  public function get_by_id($id)
  {
    $this->db->join('tbl_bank', 'tbl_bank.IDBank = tbl_giro.IDBank', 'left');
    $this->db->where($this->id, $id);
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->row();
    } 
    
    
  }

  //Fungsi Tambah Data
  public function insert($data)
  {
    $this->db->insert($this->table, $data);
  }

  //Fungsi Ubah Data
  public function update($id, $data)
  {
    $this->db->where($this->id, $id);
    $this->db->update($this->table, $data);
  }

  //Fungsi Hapus Data
  public function delete($id)
  {
    $this->db->where($this->id, $id);
    $this->db->delete($this->table);
  }

  function cari_by_kolom($kolom,$keyword){
    $this->db->select("*")
    ->from("tbl_giro g")
    ->join("tbl_bank b", "g.IDBank = b.IDBank")
    ->join("tbl_customer c", "c.IDCustomer = g.IDPerusahaan")
    ->like("$kolom", $keyword);
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }

  function cari_by_keyword($keyword){
    $this->db->select("*")
    ->from("tbl_giro g")
    ->join("tbl_bank b", "g.IDBank = b.IDBank")
    ->join("tbl_customer c", "c.IDCustomer = g.IDPerusahaan")
    ->like("g.Jenis_Faktur", $keyword)
    ->or_like("c.Nama", $keyword)
    ->or_like("g.Nomor_Giro", $keyword)
    ->or_like("g.Status_Giro", $keyword)
    ->or_like("g.Jenis_Giro", $keyword);
    $query = $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }



}

/* End of file M_giro.php */
