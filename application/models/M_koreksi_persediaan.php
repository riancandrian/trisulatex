<?php

class M_koreksi_persediaan extends CI_Model
{

    function tampilkan_koreksi_persediaan()
    {
        $this->db->select("tbl_koreksi_persediaan.Nomor, tbl_koreksi_persediaan.IDKP, tbl_koreksi_persediaan.Tanggal, tbl_corak.Corak, tbl_koreksi_persediaan_detail.Qty_yard, tbl_koreksi_persediaan.Batal")
        ->from("tbl_koreksi_persediaan")
        ->join("tbl_koreksi_persediaan_detail", "tbl_koreksi_persediaan.IDKP=tbl_koreksi_persediaan_detail.IDKP")
        ->join("tbl_corak", "tbl_koreksi_persediaan_detail.IDCorak=tbl_corak.IDCorak")
        ->group_by("tbl_koreksi_persediaan.Nomor, tbl_koreksi_persediaan.IDKP, tbl_koreksi_persediaan.Tanggal, tbl_corak.Corak, tbl_koreksi_persediaan_detail.Qty_yard, tbl_koreksi_persediaan.Batal");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }


    function tampilkan_koreksi_persediaan_scan()
    {
        $this->db->select("tbl_koreksi_persediaan_scan.Nomor, tbl_koreksi_persediaan_scan.IDKPScan, tbl_koreksi_persediaan_scan.Tanggal, tbl_corak.Corak, tbl_koreksi_persediaan_scan_detail.Qty_yard, tbl_koreksi_persediaan_scan.Batal")
        ->from("tbl_koreksi_persediaan_scan")
        ->join("tbl_koreksi_persediaan_scan_detail", "tbl_koreksi_persediaan_scan.IDKPScan=tbl_koreksi_persediaan_scan_detail.IDKPScan")
        ->join("tbl_corak", "tbl_koreksi_persediaan_scan_detail.IDCorak=tbl_corak.IDCorak")
        ->group_by("tbl_koreksi_persediaan_scan.Nomor, tbl_koreksi_persediaan_scan.IDKPScan, tbl_koreksi_persediaan_scan.Tanggal, tbl_corak.Corak, tbl_koreksi_persediaan_scan_detail.Qty_yard, tbl_koreksi_persediaan_scan.Batal");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }


    function update_status($data, $where)
    {
        return $this->db->update('tbl_koreksi_persediaan', $data, $where);
    }

    function update_status_scan($data, $where)
    {
        return $this->db->update('tbl_koreksi_persediaan_scan', $data, $where);
    }

    function detail_koreksi_persediaan($id)
    {
       $this->db->select("tbl_koreksi_persediaan_detail.*, tbl_corak.Corak, tbl_warna.Warna")
       ->from("tbl_koreksi_persediaan")
       ->join("tbl_koreksi_persediaan_detail", "tbl_koreksi_persediaan.IDKP=tbl_koreksi_persediaan_detail.IDKP")
       ->join("tbl_corak", "tbl_koreksi_persediaan_detail.IDCorak=tbl_corak.IDCorak")
       ->join("tbl_warna", "tbl_koreksi_persediaan_detail.IDWarna=tbl_warna.IDWarna")
       ->where("tbl_koreksi_persediaan_detail.IDKP", $id);
       $query = $this->db->get();

       if ($query->num_rows() > 0)
       {
        return $query->result();
    }
}

function detail_koreksi_persediaan_scan($id)
{
   $this->db->select("tbl_koreksi_persediaan_scan_detail.*, tbl_corak.Corak, tbl_warna.Warna")
   ->from("tbl_koreksi_persediaan_scan")
   ->join("tbl_koreksi_persediaan_scan_detail", "tbl_koreksi_persediaan_scan.IDKPScan=tbl_koreksi_persediaan_scan_detail.IDKPScan")
   ->join("tbl_corak", "tbl_koreksi_persediaan_scan_detail.IDCorak=tbl_corak.IDCorak")
   ->join("tbl_warna", "tbl_koreksi_persediaan_scan_detail.IDWarna=tbl_warna.IDWarna")
   ->where("tbl_koreksi_persediaan_scan_detail.IDKPScan", $id);
   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}

function getById($id)
{
    return $this->db->get_where('tbl_koreksi_persediaan', array('IDKP' => $id))->row();
}

function getByIdScan($id)
{
    return $this->db->get_where('tbl_koreksi_persediaan_scan', array('IDKPScan' => $id))->row();
}

function searching($date_from, $date_until, $search_type, $keyword)
{
    
    $this->db->select("tbl_koreksi_persediaan.Nomor, tbl_koreksi_persediaan.IDKP, tbl_koreksi_persediaan.Tanggal, tbl_corak.Corak, tbl_koreksi_persediaan_detail.Qty_yard, tbl_koreksi_persediaan.Batal")
    ->from("tbl_koreksi_persediaan")
    ->join("tbl_koreksi_persediaan_detail", "tbl_koreksi_persediaan.IDKP=tbl_koreksi_persediaan_detail.IDKP")
    ->join("tbl_corak", "tbl_koreksi_persediaan_detail.IDCorak=tbl_corak.IDCorak")
    ->group_by("tbl_koreksi_persediaan.Nomor, tbl_koreksi_persediaan.IDKP, tbl_koreksi_persediaan.Tanggal, tbl_corak.Corak, tbl_koreksi_persediaan_detail.Qty_yard");
    
    if ($date_from) {
        $this->db->where("Tanggal >= ", $date_from);
    }

    if ($date_until) {
        $this->db->where("Tanggal <= ", $date_until);
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }

    $query = $this->db->order_by('IDKP', 'DESC')->get();
    return $query->result();
}

function searching_scan($date_from, $date_until, $search_type, $keyword)
{
    
    $this->db->select("tbl_koreksi_persediaan_scan.Nomor, tbl_koreksi_persediaan_scan.IDKPScan, tbl_koreksi_persediaan_scan.Tanggal, tbl_corak.Corak, tbl_koreksi_persediaan_scan_detail.Qty_yard, tbl_koreksi_persediaan_scan.Batal")
    ->from("tbl_koreksi_persediaan_scan")
    ->join("tbl_koreksi_persediaan_scan_detail", "tbl_koreksi_persediaan_scan.IDKPScan=tbl_koreksi_persediaan_scan_detail.IDKPScan")
    ->join("tbl_corak", "tbl_koreksi_persediaan_scan_detail.IDCorak=tbl_corak.IDCorak")
    ->group_by("tbl_koreksi_persediaan_scan.Nomor, tbl_koreksi_persediaan_scan.IDKPScan, tbl_koreksi_persediaan_scan.Tanggal, tbl_corak.Corak, tbl_koreksi_persediaan_scan_detail.Qty_yard");
    
    if ($date_from) {
        $this->db->where("Tanggal >= ", $date_from);
    }

    if ($date_until) {
        $this->db->where("Tanggal <= ", $date_until);
    }

    if ($search_type) {
        $array = array($search_type => $keyword);
        $this->db->like($array);
    }

    $query = $this->db->order_by('IDKPScan', 'DESC')->get();
    return $query->result();
}
function tampilkan_id_koreksi_persediaan()
{
    $this->db->select("IDKP")
    ->from("tbl_koreksi_persediaan");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function tampilkan_id_koreksi_persediaan_scan()
{
    $this->db->select("IDKPScan")
    ->from("tbl_koreksi_persediaan_scan");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function tampilkan_id_koreksi_persediaan_detail()
{
    $this->db->select("IDKPDetail")
    ->from("tbl_koreksi_persediaan_detail");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function tampilkan_id_koreksi_persediaan_scan_detail()
{
    $this->db->select("IDKPScanDetail")
    ->from("tbl_koreksi_persediaan_scan_detail");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function tampilkan_id_stok()
{
    $this->db->select("IDStok")
    ->from("tbl_stok")
    ->order_by("IDStok", "ASC");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

function tampilkan_id_kartu_stok()
{
    $this->db->select("IDKartuStok")
    ->from("tbl_kartu_stok");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}

public function add($data){
 $this->db->insert('tbl_koreksi_persediaan', $data);
 return TRUE;
}

public function add_scan($data){
 $this->db->insert('tbl_koreksi_persediaan_scan', $data);
 return TRUE;
}

public function add_detail($data){
    $this->db->insert('tbl_koreksi_persediaan_detail', $data);
    return TRUE;
}

public function add_detail_scan($data){
    $this->db->insert('tbl_koreksi_persediaan_scan_detail', $data);
    return TRUE;
}
public function add_kartu_stok($data6){
    $this->db->insert('tbl_kartu_stok', $data6);
    return TRUE;
}
public function add_stok($data5){
    $this->db->insert('tbl_stok', $data5);
    return TRUE;
}
function find_tbl_stok($barcode)
{
    return $this->db->select('*')
    ->from("tbl_stok")
    ->where("tbl_stok". '.Barcode', $barcode)
    ->get()->row();
}

function find_tbl_stok_edit($barcode)
{
    $this->db->select("*")
   ->from("tbl_stok")
   ->where("Barcode", $barcode);

   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}

function edit_kartu_stok($data6, $where)
{
   return $this->db->update('tbl_kartu_stok', $data6, array('Barcode' => $where));
}

function find_tbl_scan_detail($barcode)
{
    return $this->db->select('*')
    ->from("tbl_koreksi_persediaan_scan_detail")
    ->where("tbl_koreksi_persediaan_scan_detail". '.Barcode', $barcode)
    ->get()->row();
}

function find_tbl_scan_detail_edit($barcode)
{
    $this->db->select("*")
   ->from("tbl_koreksi_persediaan_scan_detail")
   ->where("Barcode", $barcode);

   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}

function find_show($id)
{
    return $this->db->select('*')
    ->from("tbl_koreksi_persediaan")
    ->where("tbl_koreksi_persediaan". '.IDKP', $id)
    ->get()->row();
}

function find_show_scan($id)
{
    return $this->db->select('*')
    ->from("tbl_koreksi_persediaan_scan")
    ->where("tbl_koreksi_persediaan_scan". '.IDKPScan', $id)
    ->get()->row();
}

function find_detail($id)
{
   $this->db->select("tbl_koreksi_persediaan_detail.*, tbl_corak.Corak, tbl_warna.Warna")
   ->from("tbl_koreksi_persediaan_detail")
   ->join("tbl_corak", "tbl_corak.IDCorak=tbl_koreksi_persediaan_detail.IDCorak")
   ->join("tbl_warna", "tbl_warna.IDWarna=tbl_koreksi_persediaan_detail.IDWarna")
   ->where("tbl_koreksi_persediaan_detail". '.IDKP', $id);

   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}

function find_detail_scan($id)
{
   $this->db->select("tbl_koreksi_persediaan_scan_detail.*, tbl_corak.Corak, tbl_warna.Warna")
   ->from("tbl_koreksi_persediaan_scan_detail")
   ->join("tbl_corak", "tbl_corak.IDCorak=tbl_koreksi_persediaan_scan_detail.IDCorak")
   ->join("tbl_warna", "tbl_warna.IDWarna=tbl_koreksi_persediaan_scan_detail.IDWarna")
   ->where("tbl_koreksi_persediaan_scan_detail". '.IDKPScan', $id);

   $query = $this->db->get();

   if ($query->num_rows() > 0)
   {
    return $query->result();
}
}
public function drop($id){
    $this->db->where('IDKPDetail', $id);
    $this->db->delete('tbl_koreksi_persediaan_detail');
    return TRUE;
}

public function drop_scan($id){
    $this->db->where('IDKPScanDetail', $id);
    $this->db->delete('tbl_koreksi_persediaan_scan_detail');
    return TRUE;
}

public function update_tbl_stok($data5, $barcode){
    return $this->db->update('tbl_stok', $data5, array('Barcode' => $barcode));
}
public function update_tbl_kartu_stok($data6, $barcode){
    return $this->db->update('tbl_kartu_stok', $data6, array('Barcode' => $barcode));
}
public function update_detail($id,$data){
    return $this->db->update('tbl_koreksi_persediaan_detail', $data, array('IDKPDetail' => $id));
}
public function update_detail_scan($id,$data){
    return $this->db->update('tbl_koreksi_persediaan_scan_detail', $data, array('IDKPScanDetail' => $id));
}
function update($data, $where)
{
    return $this->db->update('tbl_pembelian_asset_detail', $data, $where);
}

function get_IDbarang($barangid)
{
 $this->db->select('IDBarang, IDSatuan')->from("tbl_barang")
 ->join("tbl_corak", "tbl_barang.IDCorak = tbl_corak.IDCorak")
 ->where("tbl_barang.IDCorak", $barangid);
 return $this->db->get()->row();
}
function get_IDgroupasset($groupassetid)
{
    return $this->db->get_where("tbl_group_asset", array('Group_Asset' => $groupassetid))->row();
}
function print_pembelian_asset($nomor)
{
    $this->db->select('*')->from("tbl_pembelian_asset")
    ->where("Nomor", $nomor);
    return $this->db->get()->row();
}
function print_pembelian_asset_detail($id)
{
   $this->db->select("tbl_pembelian_asset_detail.*, tbl_asset.Nama_Asset, tbl_group_asset.Group_Asset")
   ->from("tbl_pembelian_asset_detail")
   ->join("tbl_asset", "tbl_asset.IDAsset=tbl_pembelian_asset_detail.IDAsset")
   ->join("tbl_group_asset", "tbl_group_asset.IDGroupAsset=tbl_pembelian_asset_detail.IDGroupAsset")
   ->where("tbl_pembelian_asset_detail.IDFBA", $id);
   $query= $this->db->get();
   if($query->num_rows()>0)
   {
      return $query->result();
  }
}


function no_asset($tahun)
{
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
    ->where('extract(year from "Tanggal") =', $tahun)
    ->from("tbl_koreksi_persediaan")
    ->get()->row();
}

function no_asset_scan($tahun)
{
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
    ->where('extract(year from "Tanggal") =', $tahun)
    ->from("tbl_koreksi_persediaan_scan")
    ->get()->row();
}

public function update_master($id,$data){
    return $this->db->update('tbl_koreksi_persediaan', $data, array('IDKP' => $id));
}
public function update_master_scan($id,$data){
    return $this->db->update('tbl_koreksi_persediaan_scan', $data, array('IDKPScan' => $id));
}
function tampilkan_id_jurnal()
{
    $this->db->select("IDJurnal")
    ->from("tbl_jurnal");
    $query = $this->db->get();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
}
function save_jurnal($data)
{
    $this->db->insert("tbl_jurnal", $data);
    return TRUE;
}

}