<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

/**
* 
*/
class M_poasset extends CI_Model
{
	
  private $table = 'tbl_po_asset';
  private $order = 'DESC';
  private $id = 'IDPOAsset';

  // Fungsi Select semua data Menu
  public function get_all($id)
  {

    //$this->db->order_by($this->id, $this->order);
    $this->db->where('Jenis_PO', $id);
    
    
    $data = $this->db->get($this->table);

    $hitung = $this->db->count_all_results($this->table);
    if ( $hitung > 0) {
     return $data->result();
   } 
 }

 function no_po($id,$tahun)
{
  return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
  ->where('Jenis_PO', $id)
  ->where('extract(year from "Tanggal") =', $tahun)
  ->from($this->table)
  ->get()->row();
}

function getasset($IDGroupAsset){

    $asset="<option value='0'>--pilih--</pilih>";

    $this->db->order_by('Nama_Asset','ASC');
    $rw= $this->db->get_where('tbl_asset',array('IDGroupAsset'=>$IDGroupAsset));

    foreach ($rw->result_array() as $data ){
    $asset.= "<option value='$data[IDAsset]'>$data[Nama_Asset]</option>";
    }

    return $asset;

}

}
?>