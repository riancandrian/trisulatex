<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_penerimaan_barang_umum extends CI_Model
{
    private $table1 = 'tbl_terima_barang_supplier';
    private $table2 = 'tbl_terima_barang_supplier_detail';
    private $table3 = 'tbl_corak';
    private $table4 = 'tbl_tampungan';
    private $table5 = 'tbl_in';
    private $table6 = 'tbl_stok';
    private $table7 = 'tbl_kartu_stok';
    private $table8 = 'tbl_merk';
    private $table9 = 'tbl_warna';
    private $table10 = 'tbl_satuan';
    private $table11 = 'tbl_suplier';
    private $table12 = 'tbl_barang';
    private $table13 = 'tbl_gudang';

    function all()
    {
        $this->db->distinct();
        $this->db->select($this->table1 . '.*')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
            // ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            ->where($this->table1 . '.Jenis_TBS', 'PBU');

        $query = $this->db->order_by($this->table1 . '.IDTBS', 'DESC')->get();
        return $query->result();
    }

    function tampilkan_id_kartu_stok()
    {
        $this->db->select("IDKartuStok")
            ->from("tbl_kartu_stok")
            ->order_by("IDKartuStok", "ASC");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

        function bulk($where)
    {
        $this->db->where_in('Nomor', $where);
        return $this->db->update($this->table1, array('Batal' => 'Tidak Aktif'));
    }

    function getCodeReception($tahun)
    {
       return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
                ->where('extract(year from "Tanggal") =', $tahun)
                ->from("tbl_terima_barang_supplier")
                ->get()->row();
    }

    function check_noPB($no_pb)
    {
        return $this->db->get_where($this->table1, array('Nomor' => $no_pb));
    }

    function save($data)
    {
        $this->db->insert($this->table1, $data);
        return TRUE;
    }

    function save_detail($data)
    {
        $this->db->insert($this->table2, $data);
        return TRUE;
    }

    public function add_stok($stok){
    $this->db->insert($this->table6, $stok);
    return TRUE;
    }

    public function add_kartu_stok($kartustok){
    $this->db->insert($this->table7, $kartustok);
    return TRUE;
    }

    function find_tbl_stok($nofaktur, $id_tbs, $id_tbs_d)
    {
     $this->db->select('IDStok')->from($this->table6)->where('Nomor_faktur', $nofaktur)->and_where('IDFaktur', $id_tbs)->and_where('IDFakturDetail', $id_tbs_d);
     return $this->db->get()->row();
    }

    function getColorByStyle($id)
    {

        $this->db->select($this->table9 . '.IDWarna,' . $this->table9 . '.Warna')
            ->from($this->table9)
            ->join($this->table3, $this->table9 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            ->where($this->table9 . '.IDCorak', $id);

        return $this->db->get()->result();
    }

    function get_IDmerk($merk)
    {
        return $this->db->select('IDMerk')->from($this->table8)->where('LOWER("Merk") = '."LOWER('$merk')")->get()->row();
    }

    function get_IDcorak($corak)
    {
        //return $this->db->get_where($this->table3, array('Corak' => $corak))->row();
        return $this->db->select('*')->from($this->table3)->where('LOWER("Corak") = '."LOWER('$corak')")->get()->row();
    }

    function get_IDwarna($warna)
    {
        //return $this->db->get_where($this->table9, array('Warna' => $warna))->row();
        return $this->db->select('*')->from($this->table9)->where('LOWER("Warna") = '."LOWER('$warna')")->get()->row();
    }

    function get_IDsatuan($satuan)
    {
        //return $this->db->get_where($this->table10, array('Satuan' => $satuan))->row();
        return $this->db->select('*')->from($this->table10)->where('LOWER("Satuan") = '."LOWER('$satuan')")->get()->row();
    }

    function get_IDbarang($corak)
    {

        $this->db->select('IDBarang')->from($this->table12)
            ->join($this->table3, $this->table12 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            //->where('LOWER("Corak") = '."LOWER('$corak')");
            ->where($this->table3 . ".IDCorak", $corak);
        return $this->db->get()->row();
    }

    function get_IDbarang_edit($corak)
    {

        $this->db->select('IDBarang')->from($this->table12)
            ->join($this->table3, $this->table12 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            //->where('LOWER("Corak") = '."LOWER('$corak')");
            ->where($this->table3 . ".Corak", $corak);
        return $this->db->get()->row();
    }

    function get_IDgudang()
    {
        $this->db->select('IDGudang')->from($this->table13)
            ->where("(Nama_Gudang = 'PT. TRISULA' OR Nama_Gudang = 'PT. Trisula')");
        return $this->db->get()->row();
    }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->select($this->table1 . '.*,' . $this->table3 . '.Corak')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
            ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak');

        if ($date_from) {
            $this->db->where("'".$date_from."'"  . " <= tbl_terima_barang_supplier.Tanggal ");
        }

        if ($date_until) {
            $this->db->where("'".$date_until."'" ." >= tbl_terima_barang_supplier.Tanggal ");
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $this->db->where($this->table1 . '.Jenis_TBS', 'PBU');

        $query = $this->db->order_by('IDTBS', 'DESC')->get();
        return $query->result();
    }

    function find($id)
    {
        return $this->db->select($this->table1 . '.*,' . $this->table11 . '.Nama')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
            ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            ->join($this->table11, $this->table1 . '.IDSupplier = ' . $this->table11 . '.IDSupplier')
            ->where($this->table1 . '.IDTBS', $id)
            ->where($this->table1 . '.Jenis_TBS', 'PBU')
            ->get()->row();
    }

    function findDetail($id)
    {
        //return $this->db->get_where($this->table2, array('IDTBS' => $id))->result();
        $query = $this->db->select(
            $this->table2 . '.*,' .
            $this->table3 . '.Corak,' .
            $this->table3 . '.IDCorak,' .
            $this->table9 . '.Warna,' .
            $this->table9 . '.IDWarna,' .
            $this->table8 . '.Merk,' .
            $this->table8 . '.IDMerk,' .
            $this->table10 . '.Satuan,' .
            $this->table10 . '.IDSatuan')
            ->from($this->table2)
            ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            ->join($this->table9, $this->table2 . '.IDWarna = ' . $this->table9 . '.IDWarna')
            ->join($this->table10, $this->table2 . '.IDSatuan = ' . $this->table10 . '.IDSatuan')
            ->join($this->table8, $this->table2 . '.IDMerk = ' . $this->table8 . '.IDMerk')
            ->where($this->table2 . '.IDTBS', $id)->get();
        return $query->result();
    }

    function soft_delete($id)
    {
        return $this->db->update($this->table1, array('Batal' => 'Tidak Aktif'), array('IDTBS' => $id));
    }

    function soft_success($id)
    {
        return $this->db->update($this->table1, array('Batal' => 'Aktif'), array('IDTBS' => $id));
    }

    function update($data, $id)
    {
        return $this->db->update($this->table1, $data, array('IDTBS' => $id));
    }

    function update_detail($data, $id)
    {
        return $this->db->update($this->table2, $data, array('IDTBSDetail' => $id));
    }

    function update_stok($stok, $faktur, $idtbs, $idtbsd)
    {
        return $this->db->update($this->table6, $stok, array('Nomor_faktur' => $faktur, 'IDFaktur' => $idtbs, 'IDFakturDetail' => $idtbsd));
    }

    function update_kartu_stok($kartustok, $faktur, $idtbs, $idtbsd)
    {
        return $this->db->update($this->table7, $kartustok, array('Nomor_faktur' => $faktur, 'IDFaktur' => $idtbs, 'IDFakturDetail' => $idtbsd));
    }

    public function drop($idDetail, $id){

        $detail = $this->db->select('Qty_yard, Qty_meter')->from($this->table2)->where('IDTBSDetail', $idDetail)->get()->row();

        $main = $this->db->select('Total_qty_yard, Total_qty_meter, Saldo_yard, Saldo_meter')->from($this->table1)->where('IDTBS', $id)->get()->row();

        $Update_totalYard = ($main->Total_qty_yard - $detail->Qty_yard);
        $Update_totalMeter = ($main->Total_qty_meter - $detail->Qty_meter);

        $data = array(
            'Total_qty_yard' => $Update_totalYard,
            'Total_qty_meter' => $Update_totalMeter,
            'Saldo_yard' => $Update_totalYard,
            'Saldo_meter' => $Update_totalMeter
        );
        $this->db->update($this->table1, $data, array('IDTBS' => $id));

        $this->db->where('IDTBSDetail', $idDetail);
        $this->db->delete($this->table2);
        return TRUE;
    }

    public function drop_detail($idDetail, $id)
    {
        $this->db->where('IDTBSDetail', $idDetail);
        $this->db->where('IDTBS', $id);
        $this->db->delete($this->table2);
        return TRUE;
    }

    public function get_satuan()
    {
        return $this->db->get($this->table10)->result();
    }

    public function get_corak()
    {
        return $this->db->get($this->table3)->result();
    }

    public function get_corak_by_id($id)
    {
        return $this->db->get_where($this->table3, array('IDCorak' => $id))->row();
    }



    function update_total($id)
    {
        $yard = 0;
        $meter = 0;
        $q = $this->db->select('Qty_yard, Qty_meter')->from($this->table2)->where('IDTBS', $id)->get()->result();
        foreach ($q as $row) {
            $yard = ($yard + $row->Qty_yard);
            $meter = ($meter + $row->Qty_meter);
        }

        $data = array(
            'Total_qty_yard' => $yard,
            'Total_qty_meter' => $meter,
            'Saldo_yard' => $yard,
            'Saldo_meter' => $meter,
        );

        $this->db->update($this->table1, $data, array('IDTBS' => $id));
    }

    function save_stock($stok)
    {
        $this->db->insert("tbl_stok", $stok);
        return TRUE;
    }

     function tampilkan_id_stok()
    {
        $this->db->select("IDStok")
            ->from("tbl_stok")
            ->order_by("IDStok", "ASC");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

  //   public function get_by_id_detail($id)
  // {
  //   $this->db->select("tbl_terima_barang_supplier_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan")
  //   ->from("tbl_terima_barang_supplier_detail")
  //   ->join("tbl_corak", "tbl_terima_barang_supplier_detail.IDCorak=tbl_corak.IDCorak")
  //   ->join("tbl_warna", "tbl_terima_barang_supplier_detail.IDWarna=tbl_warna.IDWarna")
  //   ->join("tbl_merk", "tbl_terima_barang_supplier_detail.IDMerk=tbl_merk.IDMerk")
  //   ->join("tbl_satuan", "tbl_terima_barang_supplier_detail.IDSatuan=tbl_satuan.IDSatuan")
  //   ->where("IDTBS", $id);
  //   $query= $this->db->get();
  //   if($query->num_rows()>0)
  //   {
  //     return $query->result();
  //   }
    
    
  // }

       function getPrint_edit($sjc)
    {
        return $this->db->select($this->table1 . '.*,' . $this->table11 . '.Nama')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
            ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            ->join($this->table11, $this->table1 . '.IDSupplier = ' . $this->table11 . '.IDSupplier')
            ->where($this->table1 . '.IDTBS', $sjc)
            ->where($this->table1 . '.Jenis_TBS', 'PBU')
            ->get()->row();
    }

     function getPrint()
    {
        return $this->db->select($this->table1 . '.*,' . $this->table11 . '.Nama')
            ->from($this->table1)
            ->join($this->table2, $this->table1 . '.IDTBS = ' . $this->table2 . '.IDTBS')
            ->join($this->table3, $this->table2 . '.IDCorak = ' . $this->table3 . '.IDCorak')
            ->join($this->table11, $this->table1 . '.IDSupplier = ' . $this->table11 . '.IDSupplier')
            ->where($this->table1 . '.Jenis_TBS', 'PBU')
            ->order_by($this->table1.'.IDTBS', 'DESC')
            ->limit(1)
            ->get()->row();
    }
    public function getDetail_tbs($id)
  {
     $this->db->select("tbl_terima_barang_supplier_detail.*, tbl_corak.Corak, tbl_barang.Nama_Barang, tbl_merk.Merk, tbl_warna.Warna")
    ->from("tbl_terima_barang_supplier_detail")
    ->join("tbl_corak", "tbl_terima_barang_supplier_detail.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_barang", "tbl_barang.IDBarang=tbl_terima_barang_supplier_detail.IDBarang")
    ->join("tbl_warna", "tbl_terima_barang_supplier_detail.IDWarna=tbl_warna.IDWarna")
    ->join("tbl_merk", "tbl_terima_barang_supplier_detail.IDMerk=tbl_merk.IDMerk")
    ->where("tbl_terima_barang_supplier_detail.IDTBS", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
    
  }

  function ceksupplier($PO)
    {
        $this->db->select('tbl_suplier."IDSupplier", tbl_suplier."Nama"')
        ->from("tbl_purchase_order")
        ->join("tbl_suplier", "tbl_purchase_order.IDSupplier=tbl_suplier.IDSupplier")
        ->where("tbl_purchase_order.IDPO", $PO);
        $query = $this->db->get();

        return $query->result();
    }

  public function get_all_po_umum()
  {
    $this->db->select("tbl_purchase_order.*")
    ->from("tbl_purchase_order")
    ->where("Jenis_PO", 'PO Umum');
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
    }
  }
}