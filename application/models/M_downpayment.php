<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* 
*/
class M_downpayment extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	/*menmapilkan table*/
	function tampilkan_downpayment()
	{
		$query=$this->db->query("SELECT * FROM tbl_down_payment");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function getCodeDP()
    {
       return $this->db->select('MAX(RIGHT("NoDP",5)) as curr_number')
                ->from("tbl_down_payment")
                ->get()->row();
    }
	/*hapus data*/
	function hapus_data_downpayment($id)
	{
		$this->db->where('IDDownPayment', $id);
		$this->db->delete('tbl_down_payment');
		if($this->db->affected_rows()==1)
		{
			return TRUE;
		}
		return FALSE;
	}
	/*tambah*/
	function simpan($data)
	{
		$this->db->insert('tbl_down_payment', $data);
		return TRUE;
	}

	function get_downpayment_byid($id)
	{
		return $this->db->get_where('tbl_down_payment', array('IDDownPayment'=>$id))->row();
	}

	function update_downpayment($id, $data)
	{
		$this->db->where('IDDownPayment', $id);
		$this->db->update('tbl_down_payment', $data);
		return TRUE;
	}


	function cari_by_kolom($kolom,$keyword){
		$this->db->select("*")
    	->from("tbl_down_payment")
    	->like("$kolom", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari_by_keyword($keyword){
		$this->db->select("*")
    	->from("tbl_down_payment")
    	->like("NoDP", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

	function cari($kolom,$status,$keyword){
		$this->db->select("*")
    	->from("tbl_down_payment")
    	->like("Aktif", $status)
    	->like("$kolom", $keyword);
    	$query= $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
function cari_by_status($status){
		$this->db->select("*")
		->from("tbl_down_payment")
		->where("Aktif", $status);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}

}
?>