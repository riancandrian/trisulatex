<?php

class M_group_user extends CI_Model
{
    function tampilkan_groupUser()
    {
        return $this->db->get("tbl_group_user")->result();
    }

    function simpan($data)
    {
        $this->db->insert('tbl_group_user', $data);
        return $this->db->insert_id();
    }

    function getById($id)
    {
        return $this->db->get_where('tbl_group_user', array('IDGroupUser' => $id))->row();
    }

    function update($data, $where)
    {
        return $this->db->update('tbl_group_user', $data, $where);
    }

    function delete($id)
    {
        $this->db->where('IDGroupUser', $id);
        $this->db->delete('tbl_group_user');
        if($this->db->affected_rows()==1)
        {
            return TRUE;
        }
        return FALSE;
    }

    function cari_by_kolom($kolom,$keyword){
        $this->db->select("*")
        ->from("tbl_group_user")
        ->like("$kolom", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }


    function cari_by_keyword($keyword){
         $this->db->select("*")
        ->from("tbl_group_user")
        ->like("Kode_Group_User", $keyword)
        ->or_like("Group_User", $keyword);
        $query= $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
    }

    
}