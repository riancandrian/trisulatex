<?php

class M_invoice_pembelian_umum extends CI_Model
{

    function tampilkan_invoice_pembelian()
    {
        $this->db->select("tbl_pembelian_umum.*, tbl_suplier.Nama")
        ->from("tbl_pembelian_umum")
        ->join("tbl_suplier", "tbl_pembelian_umum.IDSupplier=tbl_suplier.IDSupplier");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampilkan_invoice_pembelianasset()
    {
        $this->db->select("tbl_pu_asset.*, tbl_suplier.Nama")
        ->from("tbl_pu_asset")
        ->join("tbl_suplier", "tbl_pu_asset.IDSupplier=tbl_suplier.IDSupplier");
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function tampil_pb()
    {
        // $this->db->select("Nomor")
        // ->from("tbl_terima_barang_supplier_umum");
        // $query = $this->db->get();

        // if ($query->num_rows() > 0)
        // {
        //     return $query->result();
        // }
        $query= $this->db->query('SELECT * from tbl_terima_barang_supplier_umum where "IDTBSUmum" NOT IN (SELECT "IDTBSUmum" FROM tbl_pembelian_umum)');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    function update_status($data, $where)
    {
        return $this->db->update('tbl_pembelian_umum', $data, $where);
    }

    function update_statusasset($data, $where)
    {
        return $this->db->update('tbl_pu_asset', $data, $where);
    }

    function find($id)
    {
        return $this->db->select('tbl_pembelian_umum.*, tbl_suplier.Nama, tbl_terima_barang_supplier_umum.Nomor AS nomorpb')
        ->from("tbl_pembelian_umum")
        ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pembelian_umum.IDSupplier")
        ->join("tbl_terima_barang_supplier_umum", "tbl_terima_barang_supplier_umum.IDTBSUmum=tbl_pembelian_umum.IDTBSUmum")
        ->where("tbl_pembelian_umum". '.IDFBUmum', $id)
        ->get()->row();
    }

    function findasset($id)
    {
        return $this->db->select('tbl_pu_asset.*, tbl_suplier.Nama, tbl_terima_barang_supplier_asset.Nomor AS nomorpb')
        ->from("tbl_pu_asset")
        ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pu_asset.IDSupplier")
        ->join("tbl_terima_barang_supplier_asset", "tbl_terima_barang_supplier_asset.IDTBAsset=tbl_pu_asset.IDTBAsset")
        ->where("tbl_pu_asset". '.IDFBAsset', $id)
        ->get()->row();
    }

    function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->select("tbl_pembelian_umum.*, tbl_suplier.Nama")
        ->from("tbl_pembelian_umum")
        ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pembelian_umum.IDSupplier");

        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by('IDFBUmum', 'DESC')->get();
        return $query->result();
    }

    function searchingasset($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->select("tbl_pu_asset.*, tbl_suplier.Nama")
        ->from("tbl_pu_asset")
        ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pu_asset.IDSupplier");

        if ($date_from) {
            $this->db->where("Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by('IDFBAsset', 'DESC')->get();
        return $query->result();
    }

    function ceksupplier($PB)
    {
        $this->db->select('tbl_suplier."IDSupplier", tbl_suplier."Nama"')
        ->from("tbl_terima_barang_supplier_umum")
        ->join("tbl_suplier", "tbl_terima_barang_supplier_umum.IDSupplier=tbl_suplier.IDSupplier")
        ->where("tbl_terima_barang_supplier_umum.Nomor", $PB);
        $query = $this->db->get();

        return $query->result();
    }

  
    function tampilkan_id_invoice()
    {
        $this->db->select_max("IDFBUmum")
        ->from("tbl_pembelian_umum");
        $query = $this->db->get();

        return $query->row();
    }

    function tampilkan_id_invoiceasset()
    {
        $this->db->select_max("IDFBAsset")
        ->from("tbl_pu_asset");
        $query = $this->db->get();

        return $query->row();
    }

    function tampilkan_id_pembayaran()
    {
        $this->db->select_max("IDFBUmumPembayaran")
        ->from("tbl_pembelian_umum_pembayaran");
       $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_invoice_detail()
    {
        $this->db->select_max("IDFBUmumDetail")
        ->from("tbl_pembelian_umum_detail");
        $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_invoice_detailasset()
    {
        $this->db->select_max("IDFBAssetDetail")
        ->from("tbl_pu_asset_detail");
        $query = $this->db->get();

        return $query->row();
    }

    function tampilkan_id_invoice_grand_total()
    {
        $this->db->select_max("IDFBUmumGrandTotal")
        ->from("tbl_pembelian_umum_grand_total");
        $query = $this->db->get();

  return $query->row();
    }

    function tampilkan_id_invoice_grand_totalasset()
    {
            $this->db->select_max("IDFBAssetGrandTotal")
            ->from("tbl_pu_asset_grandtotal");
            $query = $this->db->get();

      return $query->row();
    }


    function tampil_Supplier()
    {
        return $this->db->get('tbl_suplier')->result();
    }
    function tampil_bank()
    {
        return $this->db->get('tbl_coa')->result();
    }
    function check_terima_barang($nomor)
    {
        // $this->db->distinct();
        $this->db->select("*")
        ->from("tbl_terima_barang_supplier_umum_detail")
        ->join("tbl_terima_barang_supplier_umum", "tbl_terima_barang_supplier_umum.IDTBSUmum=tbl_terima_barang_supplier_umum_detail.IDTBSUmum")
        ->join("tbl_barang", "tbl_barang.IDBarang=tbl_terima_barang_supplier_umum_detail.IDBarang")
        ->join("tbl_satuan", "tbl_terima_barang_supplier_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
        ->where("tbl_terima_barang_supplier_umum.Nomor", $nomor);
        $query = $this->db->get();

        return $query->result();
    }


    public function add($data){
       $this->db->insert('tbl_pembelian_umum', $data);
       return TRUE;
    }

    public function addasset($data){
       $this->db->insert('tbl_pu_asset', $data);
       return TRUE;
    }

   public function add_detail($data){
    $this->db->insert('tbl_pembelian_umum_detail', $data);
    return TRUE;
}

    public function add_detailasset($data){
        $this->db->insert('tbl_pu_asset_detail', $data);
        return TRUE;
    }
public function add_grand_total($data4){
    $this->db->insert('tbl_pembelian_umum_grand_total', $data4);
    return TRUE;
}

public function add_grand_totalasset($data4){
    $this->db->insert('tbl_pu_asset_grandtotal', $data4);
    return TRUE;
}

public function add_pembayaran($datas5){
    $this->db->insert('tbl_pembelian_umum_pembayaran', $datas5);
    return TRUE;
}
function get_IDTBS($Nomor)
{
    return $this->db->get_where("tbl_terima_barang_supplier", array('Nomor' => $Nomor))->row();
}
function get_IDcorak($corak)
{
    return $this->db->get_where("tbl_corak", array('Corak' => $corak))->row();
}

function get_IDwarna($warna)
{
    return $this->db->get_where("tbl_warna", array('Warna' => $warna))->row();
}

function get_IDsatuan($satuan)
{
    return $this->db->get_where("tbl_satuan", array('Satuan' => $satuan))->row();
}

function get_IDbarang($corak)
{
    $this->db->select('IDBarang')->from("tbl_barang")
    ->join("tbl_corak", "tbl_barang.IDCorak = tbl_corak.IDCorak")
    ->where("tbl_corak.Corak", $corak);
    return $this->db->get()->row();
}
function getById($id)
{
        // return $this->db->get_where('tbl_pembelian', array('IDFB' => $id))->row();
    $this->db->select('tbl_pembelian_umum.*, tbl_terima_barang_supplier_umum.Nomor AS NomorTBS, tbl_pembelian_umum_grand_total.*, tbl_pembelian_umum_pembayaran.*, tbl_coa.Nama_COA')->from("tbl_pembelian_umum")
    ->join("tbl_terima_barang_supplier_umum", "tbl_pembelian_umum.IDTBSUmum = tbl_terima_barang_supplier_umum.IDTBSUmum")
    ->join("tbl_pembelian_umum_grand_total", "tbl_pembelian_umum_grand_total.IDFBUmum = tbl_pembelian_umum.IDFBUmum")
    ->join("tbl_pembelian_umum_pembayaran", "tbl_pembelian_umum_pembayaran.IDFBUmum = tbl_pembelian_umum.IDFBUmum")
    ->join("tbl_coa", "tbl_coa.IDCoa = tbl_pembelian_umum_pembayaran.IDCOA")
    ->where("tbl_pembelian_umum.IDFBUmum", $id);
    return $this->db->get()->row();
}

function getByIdAsset($id)
{
        // return $this->db->get_where('tbl_pembelian', array('IDFB' => $id))->row();
    $this->db->select('tbl_pu_asset.*, tbl_terima_barang_supplier_asset.Nomor AS NomorTBS, tbl_pu_asset_grandtotal.*')->from("tbl_pu_asset")
    ->join("tbl_terima_barang_supplier_asset", "tbl_pu_asset.IDTBAsset = tbl_terima_barang_supplier_asset.IDTBAsset")
    ->join("tbl_pu_asset_grandtotal", "tbl_pu_asset_grandtotal.IDFBAsset = tbl_pu_asset.IDFBAsset")
    //->join("tbl_pembelian_umum_pembayaran", "tbl_pembelian_umum_pembayaran.IDFBUmum = tbl_pembelian_umum.IDFBUmum")
    //->join("tbl_coa", "tbl_coa.IDCoa = tbl_pembelian_umum_pembayaran.IDCOA")
    ->where("tbl_pu_asset.IDFBAsset", $id);
    return $this->db->get()->row();
}

function detail_invoice_pembelian($id)
{
 $this->db->select("*")
 ->from("tbl_pembelian_umum")
 ->join("tbl_pembelian_umum_detail", "tbl_pembelian_umum.IDFBUmum=tbl_pembelian_umum_detail.IDFBUmum")
 ->join("tbl_barang", "tbl_barang.IDBarang=tbl_pembelian_umum_detail.IDBarang")
 ->join("tbl_satuan", "tbl_pembelian_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_pembelian_umum.IDFBUmum", $id);
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
    return $query->result();
}
}



function detail_invoice_pembelianasset($id)
{
 $this->db->select("*")
 ->from("tbl_pu_asset")
 ->join("tbl_pu_asset_detail", "tbl_pu_asset.IDFBAsset=tbl_pu_asset_detail.IDFBAsset")
 ->join("tbl_asset", "tbl_asset.IDAsset=tbl_pu_asset_detail.IDAsset")
 //->join("tbl_satuan", "tbl_pembelian_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_pu_asset.IDFBAsset", $id);
 $query = $this->db->get();

 if ($query->num_rows() > 0)
 {
    return $query->result();
}
}

function detail_invoice_pembelian_header($id)
{
 $this->db->select("tbl_pembelian_umum.*, tbl_terima_barang_supplier_umum.Nomor as Nomorpb, tbl_suplier.Nama")
 ->from("tbl_pembelian_umum")
 ->join("tbl_terima_barang_supplier_umum", "tbl_terima_barang_supplier_umum.IDTBSUmum=tbl_pembelian_umum.IDTBSUmum")
 ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pembelian_umum.IDSupplier")
 ->where("tbl_pembelian_umum.IDFBUmum", $id);
 return $this->db->get()->row();
}

function detail_invoice_pembelian_headerasset($id)
{
 $this->db->select("tbl_pu_asset.*, tbl_terima_barang_supplier_asset.Nomor as Nomorpb, tbl_suplier.Nama")
 ->from("tbl_pu_asset")
 ->join("tbl_terima_barang_supplier_asset", "tbl_terima_barang_supplier_asset.IDTBAsset=tbl_pu_asset.IDTBAsset")
 ->join("tbl_suplier", "tbl_suplier.IDSupplier=tbl_pu_asset.IDSupplier")
 ->where("tbl_pu_asset.IDFBAsset", $id);
 return $this->db->get()->row();
}

public function update_master($id,$data){
    return $this->db->update('tbl_pembelian_umum', $data, array('IDFBUmum' => $id));
}

public function update_masterasset($id,$data){
    return $this->db->update('tbl_pu_asset', $data, array('IDFBAsset' => $id));
}

public function update_grand_total($id,$datas){
    return $this->db->update('tbl_pembelian_umum_grand_total', $datas, array('IDFBUmum' => $id));
}

public function update_grand_totalasset($id,$datas){
    return $this->db->update('tbl_pu_asset_grandtotal', $datas, array('IDFBAsset' => $id));
}

public function update_pembayaran($id,$datas5){
    return $this->db->update('tbl_pembelian_umum_pembayaran', $datas5, array('IDFBUmum' => $id));
}
public function get_by_id_detail($id)
{
    $this->db->select("tbl_pembelian_detail.*, tbl_corak.Corak, tbl_warna.Warna, tbl_merk.Merk, tbl_satuan.Satuan")
    ->from("tbl_pembelian_detail")
    ->join("tbl_corak", "tbl_pembelian_detail.IDCorak=tbl_corak.IDCorak")
    ->join("tbl_warna", "tbl_pembelian_detail.IDWarna=tbl_warna.IDWarna")
    ->join("tbl_merk", "tbl_pembelian_detail.IDMerk=tbl_merk.IDMerk")
    ->join("tbl_satuan", "tbl_pembelian_detail.IDSatuan=tbl_satuan.IDSatuan")
    ->where("IDFB", $id);
    $query= $this->db->get();
    if($query->num_rows()>0)
    {
      return $query->result();
  }


}

function print_invoice($nomor)
{
    $this->db->select('*')->from("tbl_pembelian_umum")
    ->where("IDFBUmum", $nomor);
    return $this->db->get()->row();
}
function print_invoiceasset($nomor)
{
    $this->db->select('*')->from("tbl_pu_asset")
    ->where("IDFBAsset", $nomor);
    return $this->db->get()->row();
}
function print_invoice_detail($id)
{
 $this->db->select("tbl_pembelian_umum_detail.*, tbl_barang.Nama_Barang, tbl_satuan.Satuan, tbl_pembelian_umum.Nomor")
 ->from("tbl_pembelian_umum")
 ->join("tbl_pembelian_umum_detail", "tbl_pembelian_umum.IDFBUmum=tbl_pembelian_umum_detail.IDFBUmum")
 ->join("tbl_barang", "tbl_pembelian_umum_detail.IDBarang=tbl_barang.IDBarang")
 ->join("tbl_satuan", "tbl_pembelian_umum_detail.IDSatuan=tbl_satuan.IDSatuan")
 ->where("tbl_pembelian_umum_detail.IDFBUmum", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function print_invoice_detailasset($id)
{
 $this->db->select("tbl_pu_asset_detail.*, tbl_asset.Nama_Asset, tbl_pu_asset.Nomor")
 ->from("tbl_pu_asset")
 ->join("tbl_pu_asset_detail", "tbl_pu_asset.IDFBAsset=tbl_pu_asset_detail.IDFBAsset")
 ->join("tbl_asset", "tbl_pu_asset_detail.IDAsset=tbl_asset.IDAsset")
 ->where("tbl_pu_asset_detail.IDFBAsset", $id);
 $query= $this->db->get();
 if($query->num_rows()>0)
 {
  return $query->result();
}
}

function no_invoice($tahun)
{
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
    ->where('extract(year from "Tanggal") =', $tahun)
    ->from("tbl_pembelian_umum")
    ->get()->row();
}

function no_invoiceasset($tahun)
{
    return $this->db->select('MAX(RIGHT("Nomor",5)) as curr_number')
    ->where('extract(year from "Tanggal") =', $tahun)
    ->from("tbl_pu_asset")
    ->get()->row();
}



}