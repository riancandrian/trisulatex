<?php

// class M_laporan_umur_persediaan extends CI_Model
// {
//   function lapumur()
//   {
//    $this->db->select("tbl_stok.Tanggal, tbl_stok.IDCorak, tbl_stok.IDWarna")
//             ->from("tbl_stok")
//             ->join("tbl_corak", "tbl_corak.IDCorak=tbl_stok.IDCorak")
//             ->join("tbl_warna", "tbl_warna.IDWarna=tbl_stok.IDWarna");

//     $query = $this->db->get();
//         return $query->result();
//   }

//   function searching($date_from, $date_until, $search_type, $keyword)
//     {
//         $this->db->select("tbl_stok.IDCorak, tbl_stok.IDWarna")
//             ->from("tbl_stok")
//             ->select_sum("tbl_stok.Qty_yard")
//             ->select_sum("tbl_stok.Qty_meter")
//             ->join("tbl_corak", "tbl_corak.IDCorak=tbl_stok.IDCorak")
//             ->join("tbl_warna", "tbl_warna.IDWarna=tbl_stok.IDWarna")
//             ->group_by("tbl_stok.IDCorak, tbl_stok.IDWarna");

//         if ($date_from) {
//             $this->db->where("Tanggal >= ", $date_from);
//         }

//         if ($date_until) {
//             $this->db->where("Tanggal <= ", $date_until);
//         }

//         if ($search_type) {
//             $array = array($search_type => $keyword);
//             $this->db->like($array);
//         }

//         $query = $this->db->order_by("tbl_stok.IDCorak, tbl_stok.IDWarna", 'ASC')->get();
//         return $query->result();
//     }


// }
?>


<?php

class M_laporan_umur_persediaan extends CI_Model
{
  function lapumur($date_from, $date_until, $search_type, $keyword)
  {
   $this->db->select("tbl_stok.Tanggal, tbl_stok.IDCorak, tbl_corak.Corak, tbl_warna.Warna, tbl_stok.IDWarna, tbl_stok.Qty_yard,tbl_stok.Qty_meter, tbl_stok.Harga")
            ->from("tbl_stok")
            ->join("tbl_corak", "tbl_corak.IDCorak=tbl_stok.IDCorak")
            ->join("tbl_warna", "tbl_warna.IDWarna=tbl_stok.IDWarna");

         if ($date_from) {
            $this->db->where("tbl_stok.Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("tbl_stok.Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

    $query = $this->db->order_by("tbl_stok.IDCorak, tbl_stok.IDWarna", 'ASC')->get();
        return $query->result();
  }

    function lapumur2()
  {
   $this->db->select("tbl_stok.Tanggal, tbl_stok.IDCorak, tbl_stok.IDWarna, tbl_corak.Corak, tbl_warna.Warna, tbl_stok.Qty_yard,tbl_stok.Qty_meter, tbl_stok.Harga")
            ->from("tbl_stok")
            ->join("tbl_corak", "tbl_corak.IDCorak=tbl_stok.IDCorak")
            ->join("tbl_warna", "tbl_warna.IDWarna=tbl_stok.IDWarna");
            
    $query = $this->db->order_by("tbl_stok.IDCorak, tbl_stok.IDWarna", 'ASC')->get();
        return $query->result();
  }

  function searching($date_from, $date_until, $search_type, $keyword)
    {
        $this->db->select("tbl_stok.IDCorak, tbl_stok.IDWarna, tbl_corak.Corak, tbl_warna.Warna,tbl_stok.Qty_yard,tbl_stok.Qty_meter, tbl_stok.Harga")
            ->from("tbl_stok")
            // ->select_sum("tbl_stok.Qty_yard")
            // ->select_sum("tbl_stok.Qty_meter")
            ->join("tbl_corak", "tbl_corak.IDCorak=tbl_stok.IDCorak")
            ->join("tbl_warna", "tbl_warna.IDWarna=tbl_stok.IDWarna");
            // ->order_by("")
            // ->group_by("tbl_stok.IDCorak, tbl_stok.IDWarna");

        if ($date_from) {
            $this->db->where("tbl_stok.Tanggal >= ", $date_from);
        }

        if ($date_until) {
            $this->db->where("tbl_stok.Tanggal <= ", $date_until);
        }

        if ($search_type) {
            $array = array($search_type => $keyword);
            $this->db->like($array);
        }

        $query = $this->db->order_by("tbl_stok.IDCorak, tbl_stok.IDWarna", 'ASC')->get();

        return $query->result();
    }


}
?>