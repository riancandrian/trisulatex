<?php

class Invoice_pembelian_umum extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_invoice_pembelian_umum');
		$this->load->model('M_invoice_pembelian');
		$this->load->model('M_hutang');
		$this->load->model('M_agen');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index() {
		$data['invoice'] = $this->M_invoice_pembelian_umum->tampilkan_invoice_pembelian();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_pembelian_umum/V_invoice_pembelian.php', $data);
	}

	function cek_supplier() {
		$Nomor = $this->input->post('Nomor');
		$data = $this->M_invoice_pembelian_umum->ceksupplier($Nomor);
		echo json_encode($data);
	}

	public function status_gagal($id) {
		$data = array(
			'Batal' => 'tidak aktif',
		);

		$this->M_invoice_pembelian_umum->update_status($data, array('IDFBUmum' => $id));
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		redirect('Invoice_pembelian_umum/index');
	}

	public function status_berhasil($id) {
		$data = array(
			'Batal' => 'aktif',
		);

		$this->M_invoice_pembelian_umum->update_status($data, array('IDFBUmum' => $id));
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		redirect('Invoice_pembelian_umum/index');
	}

	public function delete_multiple() {
		$ID_att = $this->input->post('msg');
		if ($ID_att != "") {
			$result = array();
			foreach ($ID_att AS $key => $val) {
				$result[] = array(
					"IDFBUmum" => $ID_att[$key],
					"Batal" => 'tidak aktif',
				);
			}
			$this->db->update_batch('tbl_pembelian_umum', $result, 'IDFBUmum');
			redirect("Invoice_pembelian_umum/index");
		} else {
			redirect("Invoice_pembelian_umum/index");
		}
	}

	function pencarian() {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['invoice'] = $this->M_invoice_pembelian_umum->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
		$this->load->view('administrator/invoice_pembelian_umum/V_invoice_pembelian', $data);
	}

	function show($id) {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->M_invoice_pembelian_umum->find($id);
		$data['invdetail'] = $this->M_invoice_pembelian_umum->detail_invoice_pembelian($id);
		$this->load->view('administrator/invoice_pembelian_umum/V_show', $data);
	}
	public function tambah_invoice_pembelian() {
		$data['pb'] = $this->M_invoice_pembelian_umum->tampil_pb();
		$data['kodenoinv'] = $this->M_invoice_pembelian_umum->no_invoice(date('Y'));
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['bank'] = $this->M_invoice_pembelian_umum->tampil_bank();
		$data['supplier'] = $this->M_invoice_pembelian_umum->tampil_supplier();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_pembelian_umum/V_tambah_invoice_pembelian', $data);
	}
	function check_penerimaan_barang() {
		$return['data'] = $this->M_invoice_pembelian_umum->check_terima_barang($this->input->post('Nomor'));
		if ($return['data']) {
			$return['error'] = false;
		} else {
			$return['error'] = true;
		}

		echo json_encode($return);
	}

	function simpan_invoice_pembelian() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		$data3 = $this->input->post('data3');
		$data6 = $this->input->post('data6');
		$data['id_invoice'] = $this->M_invoice_pembelian_umum->tampilkan_id_invoice();
		if ($data['id_invoice'] != "") {
			// foreach ($data['id_invoice'] as $value) {
				$urutan = substr($data['id_invoice']->IDFBUmum, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id = 'P000001';
		}

		$data = array(
			'IDFBUmum' => $urutan_id,
			'Tanggal' => $data1['Tanggal'],
			'Jatuh_tempo' => $data1['Jatuh_tempo'],
			'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
			'Nomor' => $data1['Nomor_invoice'],
			'IDTBSUmum' => $data1['IDTBSUmum'],
			'IDSupplier' => $data1['IDSupplier'],
			'IDMataUang' => 1,
			'Kurs' => 14000,
			'Total_qty' => $data1['total_qty'],
			'Saldo_qty' => $data1['total_qty'],
			'Grand_total' => str_replace(".", "", $data3['total_invoice_pembayaran']),
			'Disc' => $data1['Discount'],
			'Persen_disc' => 0,
			'Status_ppn' => $data1['Status_ppn'],
			'Keterangan' => $data1['Keterangan'],
			'Batal' => 'aktif',
		);
		//$this->M_invoice_pembelian_umum->add($data);

		if ($this->M_invoice_pembelian_umum->add($data)) {
			$data = null;
			foreach ($data2 as $row) {
				$data['id_invoice_detail'] = $this->M_invoice_pembelian_umum->tampilkan_id_invoice_detail();
				if ($data['id_invoice_detail'] != "") {
					// foreach ($data['id_invoice_detail'] as $value) {
						$urutan = substr($data['id_invoice_detail']->IDFBUmumDetail, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_detail_inv = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_detail_inv = 'P000001';
				}

				$data = array(
					'IDFBUmumDetail' => $urutan_id_detail_inv,
					'IDFBUmum' => $urutan_id,
					'IDTBSUmumDetail' => '1',
					'IDBarang' => $row['IDBarang'],
					'Qty' => $row['Qty'],
					'Saldo' => $row['Qty'],
					'IDSatuan' => $row['IDSatuan'],
					'Harga_satuan' => $row['Harga_satuan'],
					'IDMataUang' => 1,
					'Kurs' => 14000,
					'Sub_total' => $row['Sub_total'],
				);
				$this->M_invoice_pembelian_umum->add_detail($data);

				//save jurnal
				$data['id_jurnal'] = $this->M_invoice_pembelian->tampilkan_id_jurnal();
				if ($data['id_jurnal'] != "") {
					// foreach ($data['id_jurnal'] as $value) {
						$urutan = substr($data['id_jurnal']->IDJurnal, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_jurnal = 'P000001';
				}

				$save_jurnal_debet_hpp = array(
					'IDJurnal' => $urutan_id_jurnal,
					'Tanggal' => $data1['Tanggal'],
					'Nomor' => $data1['Nomor_invoice'],
					'IDFaktur' => $urutan_id,
					'IDFakturDetail' => $urutan_id_detail_inv,
					'Jenis_faktur' => 'INV',
					'IDCOA' => 'P000018',
					'Debet' => str_replace(".", "", $row['Harga_satuan']),
					'Kredit' => 0,
					'IDMataUang' => 1,
					'Kurs' => 14000,
					'Total_debet' => str_replace(".", "", $row['Harga_satuan']),
					'Total_kredit' => 0,
					'Keterangan' => $data1['Keterangan'],
					'Saldo' => str_replace(".", "", $row['Harga_satuan']),
				);
				$this->M_invoice_pembelian->save_jurnal($save_jurnal_debet_hpp);

				$data['id_jurnal'] = $this->M_invoice_pembelian->tampilkan_id_jurnal();
				if ($data['id_jurnal'] != "") {
					// foreach ($data['id_jurnal'] as $value) {
						$urutan = substr($data['id_jurnal']->IDJurnal, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_jurnal_ppn = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_jurnal_ppn = 'P000001';
				}

				$save_jurnal_debet_ppn = array(
					'IDJurnal' => $urutan_id_jurnal_ppn,
					'Tanggal' => $data1['Tanggal'],
					'Nomor' => $data1['Nomor_invoice'],
					'IDFaktur' => $urutan_id,
					'IDFakturDetail' => $urutan_id_detail_inv,
					'Jenis_faktur' => 'INV',
					'IDCOA' => 'P000022',
					'Debet' => str_replace(".", "", $row['Harga_satuan']) * 0.1,
					'Kredit' => 0,
					'IDMataUang' => 1,
					'Kurs' => 14000,
					'Total_debet' => str_replace(".", "", $row['Harga_satuan']) * 0.1,
					'Total_kredit' => 0,
					'Keterangan' => $data1['Keterangan'],
					'Saldo' => str_replace(".", "", $row['Harga_satuan']) * 0.1,
				);
				$this->M_invoice_pembelian->save_jurnal($save_jurnal_debet_ppn);

				$data['id_jurnal'] = $this->M_invoice_pembelian->tampilkan_id_jurnal();
				if ($data['id_jurnal'] != "") {
					// foreach ($data['id_jurnal'] as $value) {
						$urutan = substr($data['id_jurnal']->IDJurnal, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_jurnal_kredit = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_jurnal_kredit = 'P000001';
				}

				$data['cekgroupsupplier'] = $this->M_invoice_pembelian->cek_group_supplier($data1['IDSupplier']);
				if ($data['cekgroupsupplier']->Group_Supplier == "PIHAK BERELASI") {
					$groupsupplier = 'P000628';
				} elseif ($data['cekgroupsupplier']->Group_Supplier == "PIHAK KETIGA") {
					$groupsupplier = 'P000627';
				}

				$save_jurnal_kredit = array(
					'IDJurnal' => $urutan_id_jurnal_kredit,
					'Tanggal' => $data1['Tanggal'],
					'Nomor' => $data1['Nomor_invoice'],
					'IDFaktur' => $urutan_id,
					'IDFakturDetail' => $urutan_id_detail_inv,
					'Jenis_faktur' => 'INV',
					'IDCOA' => $groupsupplier,
					'Debet' => 0,
					'Kredit' => str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
					'IDMataUang' => 1,
					'Kurs' => 14000,
					'Total_debet' => 0,
					'Total_kredit' => str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
					'Keterangan' => $data1['Keterangan'],
					'Saldo' => str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
				);
				$this->M_invoice_pembelian->save_jurnal($save_jurnal_kredit);
			}
		}
		$data['id_invoice_grand_total'] = $this->M_invoice_pembelian_umum->tampilkan_id_invoice_grand_total();
		if ($data['id_invoice_grand_total'] != "") {
			// foreach ($data['id_invoice_grand_total'] as $value) {
				$urutan = substr($data['id_invoice_grand_total']->IDFBUmumGrandTotal, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id_grand_total = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id_grand_total = 'P000001';
		}
		$data4 = array(
			'IDFBUmumGrandTotal' => $urutan_id_grand_total,
			'IDFBUmum' => $urutan_id,
			'Pembayaran' => $data3['jenis'],
			'IDMataUang' => 1,
			'Kurs' => 14000,
			'DPP' => str_replace(".", "", $data3['DPP']),
			'Discount' => str_replace(".", "", $data3['Discount']),
			'PPN' => str_replace(".", "", $data3['PPN']),
			'Grand_total' => str_replace(".", "", $data3['total_invoice_pembayaran']),
			'Sisa' => str_replace(".", "", $data3['total_invoice_pembayaran']) - str_replace(".", "", $data3['nominal']),
		);
		$this->M_invoice_pembelian_umum->add_grand_total($data4);

		// $data['id_pembayaran'] = $this->M_invoice_pembelian_umum->tampilkan_id_pembayaran();
		// if ($data['id_pembayaran'] != "") {
		// 	foreach ($data['id_pembayaran'] as $value) {
		// 		$urutan = substr($value->IDFBUmumPembayaran, 1);
		// 	}
		// 	$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
		// 	$urutan_id_pembayaran = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		// } else {
		// 	$urutan_id_pembayaran = 'P000001';
		// }
		// $datas5 = array(
		// 	'IDFBUmumPembayaran' => $urutan_id_pembayaran,
		// 	'IDFBUmum' => $urutan_id,
		// 	'Jenis_pembayaran' => $data3['jenis'],
		// 	'IDCOA' => $data3['namacoa'],
		// 	'NominalPembayaran' => $data3['nominal'],
		// 	'IDMataUang' => 1,
		// 	'Kurs' => '14000',
		// 	// 'Tanggal_Giro' => $data3['tgl_giro']
		// );
		// $this->M_invoice_pembelian_umum->add_pembayaran($datas5);

		// if (str_replace(".", "", $data3['nominal']) != str_replace(".", "", $data3['total_invoice_pembayaran'])) {
		$data['id_hutang'] = $this->M_hutang->tampilkan_id_hutang();
		if ($data['id_hutang'] != "") {
			// foreach ($data['id_hutang'] as $value) {
				$urutan = substr($data['id_hutang']->IDHutang, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id = 'P000001';
		}

		$datahutang = array(
			'IDHutang' => $urutan_id,
			'IDFaktur' => $urutan_id,
			'Tanggal_Hutang' => $data1['Tanggal'],
			'Jatuh_Tempo' => $data1['Tanggal_jatuh_tempo'],
			'No_Faktur' => $data1['Nomor_invoice'],
			'Nilai_Hutang' => str_replace(".", "", $data3['total_invoice_pembayaran']) - $data3['nominal'],
			'Saldo_Awal' => str_replace(".", "", $data3['total_invoice_pembayaran']) - $data3['nominal'],
			'Pembayaran' => 0,
			'Saldo_Akhir' => str_replace(".", "", $data3['total_invoice_pembayaran']) - $data3['nominal'],
			'IDSupplier' => $data1['IDSupplier'],
			'Jenis_Faktur' => 'INVU',
		);

		$this->M_hutang->simpan($datahutang);
		// }

		echo json_encode($data);
		//  }else{
		//   echo false;
		// }
	}

	public function edit($id) {
		$data['bank'] = $this->M_invoice_pembelian_umum->tampil_bank();
		$data['detail'] = $this->M_invoice_pembelian_umum->detail_invoice_pembelian($id);
		$data['detailheader'] = $this->M_invoice_pembelian_umum->detail_invoice_pembelian_header($id);
		$data['supplier'] = $this->M_invoice_pembelian_umum->tampil_supplier();
		$data['invoice'] = $this->M_invoice_pembelian_umum->getById($id);
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_pembelian_umum/V_edit', $data);
	}

	public function ubah_invoice_pembelian() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		// $data3 = $this->input->post('data3');

		$data = array(
			'Tanggal' => $data1['Tanggal'],
			'Nomor' => $data1['Nomor_invoice'],
			'IDSupplier' => $data1['IDSupplier'],
			'Jatuh_tempo' => $data1['Jatuh_tempo'],
			'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
			'IDMataUang' => 1,
			'Kurs' => 14000,
			'Total_qty' => $data1['total_qty'],
			'Saldo_qty' => $data1['total_qty'],
			'Grand_total' => str_replace(".", "", $data2['total_invoice_pembayaran']),
			'Disc' => $data1['Discount'],
			'Persen_disc' => 0,
			'Status_ppn' => $data1['Status_ppn'],
			'Keterangan' => $data1['Keterangan'],
			'Batal' => 'aktif',
		);
		$this->M_invoice_pembelian_umum->update_master($data1['IDFB'], $data);

		$datas = array(
			'IDFBUmum' => $data1['IDFB'],
			'Pembayaran' => $data2['jenis'],
			'IDMataUang' => 1,
			'Kurs' => 14000,
			'DPP' => str_replace(".", "", $data2['DPP']),
			'Discount' => str_replace(".", "", $data2['Discount']),
			'PPN' => str_replace(".", "", $data2['PPN']),
			'Grand_total' => str_replace(".", "", $data2['total_invoice_pembayaran']),
			'Sisa' => str_replace(".", "", $data2['total_invoice_pembayaran']),

		);
		$this->M_invoice_pembelian_umum->update_grand_total($data1['IDFB'], $datas);

		if ($data2['namacoa'] == "Cash") {
			$datas5 = array(
				'IDFBUmum' => $data1['IDFB'],
				'Jenis_pembayaran' => $data2['jenis'],
				'IDCOA' => 0,
				'NominalPembayaran' => $data2['nominal'],
				'IDMataUang' => 1,
				'Kurs' => '14000',
			);
		} else {
			$datas5 = array(
				'IDFBUmum' => $data1['IDFB'],
				'Jenis_pembayaran' => $data2['jenis'],
				'IDCOA' => $data2['namacoa'],
				'NominalPembayaran' => $data2['nominal'],
				'IDMataUang' => 1,
				'Kurs' => '14000',
			);
		}
		$this->M_invoice_pembelian_umum->update_pembayaran($data1['IDFB'], $datas5);

		echo json_encode($datas);
	}

	function printc($nomor) {

		
		$data['print'] = $this->M_invoice_pembelian_umum->print_invoice($nomor);
		$id = $data['print']->IDFBUmum;

		$data['printdetail'] = $this->M_invoice_pembelian_umum->print_invoice_detail($id);
		$this->load->view('administrator/invoice_pembelian_umum/V_print', $data);
	}
}