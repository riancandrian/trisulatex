<?php

/**
 *
 */
class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
        $data['users'] = $this->M_user->tampilkan_user();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/user/V_user.php', $data);
    }

    public function tambah_user()
    {
        $data['group_users'] = $this->M_user->group_user();
        $data['menu'] = $this->M_user->menu();
        $data['details_menu'] = $this->M_user->details_menu();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/user/V_tambah_user', $data);
    }

    public function simpan()
    {
        $simpan= $this->input->post('simpan');
        $simtam= $this->input->post('simtam');
        $data['id_user'] = $this->M_user->tampilkan_user();
        if($data['id_user']!=""){
            foreach ($data['id_user'] as $value) {
              $urutan= substr($value->IDUser, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
      $data = array(
        'IDUser' => $urutan_id,
        'Nama' => $this->input->post('nama'),
        'Username' => $this->input->post('username'),
        'Email' => $this->input->post('email'),
        'Password' => md5($this->input->post('password')),
        'IDGroupUser' => $this->input->post('group_user'),
        'Aktif' => $this->input->post('status')
    );

      $this->M_user->simpan($data);

      if ($this->input->post('details')) {
        $details = array();
        foreach ($this->input->post('details') as $detail) {
            $details[] = array(
                'IDGroupUser' => $this->input->post('group_user'),
                'IDMenuDetail' => $detail
            );
        }
        $this->M_user->save_batch($details);
    }

    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
    if($simpan){
        redirect('User/index');
    }elseif($simtam){
        redirect('User/tambah_user');
    }
}

public function edit($id)
{
    $data['menu'] = $this->M_user->menu();
    $data['details_menu'] = $this->M_user->details_menu();
    $data['group_users'] = $this->M_user->group_user();
    $data['users'] = $this->M_user->getById($id);
    $data['role'] = $this->M_user->getRoleById($data['users']->IDGroupUser);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/user/V_edit_user', $data);
}

public function update()
{
    $data = array(
        'Nama' => $this->input->post('nama'),
        'Username' => $this->input->post('username'),
        'Email' => $this->input->post('email'),
        'IDGroupUser' => $this->input->post('group_user'),
        'Aktif' => $this->input->post('status')
    );

    $this->M_user->update($data, array('IDUser' => $this->input->post('id')));

    if ($this->input->post('details')) {
        $this->M_user->delete_batch($this->input->post('group_user'));
        $details = array();
        foreach ($this->input->post('details') as $detail) {
            $details[] = array(
                'IDGroupUser' => $this->input->post('group_user'),
                'IDMenuDetail' => $detail
            );
        }
        $this->M_user->save_batch($details);
    }

    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('User/index');
}

public function delete($id)
{
    $this->M_user->delete($id);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil dihapus</div></center>");
    redirect('User');
}

public function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();

    $kolom= addslashes($this->input->post('jenispencarian'));
    $status= addslashes($this->input->post('statuspencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $status!="" && $keyword!="") {
        $data['users']=$this->M_user->cari($kolom,$status,$keyword);
    }elseif ($kolom!="" && $keyword!="") {
        $data['users']=$this->M_user->cari_by_kolom($kolom,$keyword);
    }elseif ($status!="") {
        $data['users']=$this->M_user->cari_by_status($status);
    }elseif ($keyword!="") {
        $data['users']=$this->M_user->cari_by_keyword($keyword);
    }else {
        $data['users']=$this->M_user->tampilkan_user();
    }
    $this->load->view('administrator/user/V_user.php', $data);
}
}