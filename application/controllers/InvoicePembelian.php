<?php

class InvoicePembelian extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_invoice_pembelian');
		$this->load->model('M_agen');
		$this->load->model('M_login');
		$this->load->model('M_hutang');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
		$this->load->library('subquery');
	}

	public function index() {
		$data['invoice'] = $this->M_invoice_pembelian->tampilkan_invoice_pembelian();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_pembelian/V_invoice_pembelian.php', $data);
	}

	function cek_supplier() {
		$Nomor = $this->input->post('Nomor');
		$data = $this->M_invoice_pembelian->ceksupplier($Nomor);
		echo json_encode($data);
	}

	public function status_gagal($id) {
		// $data['pembayaranget'] = $this->M_invoice_pembelian->tampilkan_get_pembayaran_hutang($id);

		// $data = array(
		//   'Batal' => 'tidak aktif',
		// );

		// $this->M_invoice_pembelian->update_status($data, array('IDFB' => $id));
		// $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");

		$data['pembayaranget'] = $this->M_invoice_pembelian->tampilkan_get_pembayaran_hutang($id);

		if (count($data['pembayaranget']) > 0) {
			// foreach($data['invoiceget'] as $valueinv) {
			if (($data['pembayaranget']->IDFB != $id)) {
				$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
				$data = array(
					'Batal' => 'tidak aktif',
				);

				$this->M_invoice_pembelian->update_status($data, array('IDFB' => $id));
			} else {
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data PB Sudah Digunakan</div></center>");
			}
// }
		} else {
			$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
			$data = array(
				'Batal' => 'tidak aktif',
			);

			$this->M_invoice_pembelian->update_status($data, array('IDFB' => $id));
		}
		redirect('InvoicePembelian/index');
	}

	public function status_berhasil($id) {
		$data = array(
			'Batal' => 'aktif',
		);

		$this->M_invoice_pembelian->update_status($data, array('IDFB' => $id));
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		redirect('InvoicePembelian/index');
	}

	public function delete_multiple() {
		$ID_att = $this->input->post('msg');
		if ($ID_att != "") {
			$result = array();
			foreach ($ID_att AS $key => $val) {
				$result[] = array(
					"IDFB" => $ID_att[$key],
					"Batal" => 'tidak aktif',
				);
			}
			$this->db->update_batch('tbl_pembelian', $result, 'IDFB');
			redirect("InvoicePembelian/index");
		} else {
			redirect("InvoicePembelian/index");
		}
	}

	function pencarian() {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['invoice'] = $this->M_invoice_pembelian->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
		$this->load->view('administrator/invoice_pembelian/V_invoice_pembelian', $data);
	}

	function show($id) {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->M_invoice_pembelian->find($id);
		$data['total_harga_inv'] = $this->M_invoice_pembelian->total_hargainv($id);
		$data['invdetail'] = $this->M_invoice_pembelian->detail_invoice_pembelian($id);
		$this->load->view('administrator/invoice_pembelian/V_show', $data);
	}
	public function tambah_invoice_pembelian() {
		$data['kodenoinv'] = $this->M_invoice_pembelian->no_invoice(date('Y'));
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['bank'] = $this->M_invoice_pembelian->tampil_bank();
		$data['supplier'] = $this->M_invoice_pembelian->tampil_supplier();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_pembelian/V_tambah_invoice_pembelian', $data);
	}
	function check_penerimaan_barang() {
		$return['data'] = $this->M_invoice_pembelian->check_terima_barang($this->input->post('Nomor'));
		if ($return['data']) {
			$return['error'] = false;
		} else {
			$return['error'] = true;
		}

		echo json_encode($return);
	}

	function getnomorpb() {
		$penerimaan = $this->input->post('jenis_tbs');
		$data = $this->M_invoice_pembelian->getnomor_pb($penerimaan);
		echo json_encode($data);
	}

	function simpan_invoice_pembelian() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		$data3 = $this->input->post('data3');
		$data6 = $this->input->post('data6');
		$data['id_invoice'] = $this->M_invoice_pembelian->tampilkan_id_invoice();
		if ($data['id_invoice'] != "") {
			// foreach ($data['id_invoice'] as $value) {
				$urutan = substr($data['id_invoice']->IDFB, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id = 'P000001';
		}

		$data = array(
			'IDFB' => $urutan_id,
			'Tanggal' => $data1['Tanggal'],
			'Nomor' => $data1['Nomor_invoice'],
			'IDTBS' => $this->M_invoice_pembelian->get_IDTBS($data1['Nomor'])->IDTBS ? $this->M_invoice_pembelian->get_IDTBS($data1['Nomor'])->IDTBS : 0,
			'IDSupplier' => $data1['IDSupplier'],
			'No_sj_supplier' => $data1['No_supplier'],
			'TOP' => $data1['Jatuh_tempo'],
			'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
			'IDMataUang' => '6',
			'Kurs' => '-',
			'Total_qty_yard' => $data1['Total_qty_yard'],
			'Total_qty_meter' => $data1['Total_qty_meter'],
			'Saldo_yard' => $data1['Total_qty_yard'],
			'Saldo_meter' => $data1['Total_qty_meter'],
			'Discount' => str_replace(".", "", $data1['Discount']),
			'Status_ppn' => $data1['Status_ppn'],
			'Keterangan' => $data1['Keterangan'],
			'Batal' => 'aktif',
		);
		$this->M_invoice_pembelian->add($data);

		// if($this->M_invoice_pembelian->add($data)){
		// $last_insert_id = $this->db->insert_id();
		$data = null;
		foreach ($data2 as $row) {
			$data['id_invoice_detail'] = $this->M_invoice_pembelian->tampilkan_id_invoice_detail();
			if ($data['id_invoice_detail'] != "") {
				// foreach ($data['id_invoice_detail'] as $value) {
					$urutan = substr($data['id_invoice_detail']->IDFBDetail, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail_inv = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail_inv = 'P000001';
			}

			$data = array(
				'IDFBDetail' => $urutan_id_detail_inv,
				'IDFB' => $urutan_id,
				'IDTBSDetail' => '1',
				'Barcode' => $row['Barcode'],
				'NoSO' => $row['NoSO'],
				'Party' => $row['Party'],
				'Indent' => $row['Indent'],
				'IDBarang' => $this->M_invoice_pembelian->get_IDbarang($row['Corak'])->IDBarang ? $this->M_invoice_pembelian->get_IDbarang($row['Corak'])->IDBarang : 0,
				'IDCorak' => $this->M_invoice_pembelian->get_IDcorak($row['Corak'])->IDCorak ? $this->M_invoice_pembelian->get_IDcorak($row['Corak'])->IDCorak : 0,
				'IDWarna' => $this->M_invoice_pembelian->get_IDwarna($row['Warna'])->IDWarna ? $this->M_invoice_pembelian->get_IDwarna($row['Warna'])->IDWarna : 0,
				'IDMerk' => $this->M_invoice_pembelian->get_IDcorak($row['Corak'])->IDMerk ? $this->M_invoice_pembelian->get_IDcorak($row['Corak'])->IDMerk : 0,
				'Qty_yard' => $row['Qty_yard'],
				'Qty_meter' => $row['Qty_meter'],
				'Saldo_yard' => $row['Qty_yard'],
				'Saldo_meter' => $row['Qty_meter'],
				'Grade' => $row['Grade'],
				'Remark' => $row['Remark'],
				'Lebar' => $row['Lebar'],
				'IDSatuan' => $this->M_invoice_pembelian->get_IDsatuan($row['Satuan'])->IDSatuan ? $this->M_invoice_pembelian->get_IDsatuan($row['Satuan'])->IDSatuan : 0,
				'Harga' => str_replace(".", "", $row['harga_satuan']),
				'Sub_total' => $row['harga_total'],
			);
			$this->M_invoice_pembelian->add_detail($data);

			//save jurnal
			$data['id_jurnal'] = $this->M_invoice_pembelian->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal = 'P000001';
			}

			$save_jurnal_debet_hpp = array(
				'IDJurnal' => $urutan_id_jurnal,
				'Tanggal' => $data1['Tanggal'],
				'Nomor' => $data1['Nomor_invoice'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail_inv,
				'Jenis_faktur' => 'INV',
				'IDCOA' => 'P000021',
				'Debet' => str_replace(".", "", $row['harga_total']),
				'Kredit' => 0,
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => str_replace(".", "", $row['harga_total']),
				'Total_kredit' => 0,
				'Keterangan' => $data1['Keterangan'],
				'Saldo' => str_replace(".", "", $row['harga_total']),
			);
			$this->M_invoice_pembelian->save_jurnal($save_jurnal_debet_hpp);

			$data['id_jurnal'] = $this->M_invoice_pembelian->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal_ppn = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal_ppn = 'P000001';
			}

			$save_jurnal_debet_ppn = array(
				'IDJurnal' => $urutan_id_jurnal_ppn,
				'Tanggal' => $data1['Tanggal'],
				'Nomor' => $data1['Nomor_invoice'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail_inv,
				'Jenis_faktur' => 'INV',
				'IDCOA' => 'P000025',
				'Debet' => str_replace(".", "", $row['harga_total']) * 0.1,
				'Kredit' => 0,
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => str_replace(".", "", $row['harga_total']) * 0.1,
				'Total_kredit' => 0,
				'Keterangan' => $data1['Keterangan'],
				'Saldo' => str_replace(".", "", $row['harga_total']) * 0.1,
			);
			$this->M_invoice_pembelian->save_jurnal($save_jurnal_debet_ppn);

			$data['id_jurnal'] = $this->M_invoice_pembelian->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal_kredit = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal_kredit = 'P000001';
			}

			$data['cekgroupsupplier'] = $this->M_invoice_pembelian->cek_group_supplier($data1['IDSupplier']);
			if ($data['cekgroupsupplier']->Group_Supplier == "PIHAK BERELASI") {
				$groupsupplier = 'P000626';
			} elseif ($data['cekgroupsupplier']->Group_Supplier == "PIHAK KETIGA") {
				$groupsupplier = 'P000625';
			}

			$save_jurnal_kredit = array(
				'IDJurnal' => $urutan_id_jurnal_kredit,
				'Tanggal' => $data1['Tanggal'],
				'Nomor' => $data1['Nomor_invoice'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail_inv,
				'Jenis_faktur' => 'INV',
				'IDCOA' => $groupsupplier,
				'Debet' => 0,
				'Kredit' => str_replace(".", "", $row['harga_total']) + str_replace(".", "", $row['harga_total']) * 0.1,
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => 0,
				'Total_kredit' => str_replace(".", "", $row['harga_total']) + str_replace(".", "", $row['harga_total']) * 0.1,
				'Keterangan' => $data1['Keterangan'],
				'Saldo' => str_replace(".", "", $row['harga_total']),
			);
			$this->M_invoice_pembelian->save_jurnal($save_jurnal_kredit);

		}
		$data['id_invoice_grand_total'] = $this->M_invoice_pembelian->tampilkan_id_invoice_grand_total();
		if ($data['id_invoice_grand_total'] != "") {
			// foreach ($data['id_invoice_grand_total'] as $value) {
				$urutan = substr($data['id_invoice_grand_total']->IDFBGrandTotal, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id_grand_total = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id_grand_total = 'P000001';
		}
		$data4 = array(
			'IDFBGrandTotal' => $urutan_id_grand_total,
			'IDFB' => $urutan_id,
			'Pembayaran' => '-',
			'DPP' => str_replace(".", "", $data3['DPP']),
			'Discount' => str_replace(".", "", $data3['Discount']),
			'PPN' => str_replace(".", "", $data3['PPN']),
			'Grand_total' => str_replace(".", "", $data3['total_invoice_pembayaran']),
			'Sisa' => str_replace(".", "", $data3['total_invoice_pembayaran']),
		);
		$this->M_invoice_pembelian->add_grand_total($data4);
		// $data['id_pembayaran'] = $this->M_invoice_pembelian->tampilkan_id_pembayaran();
		// if($data['id_pembayaran']!=""){
		//   foreach ($data['id_pembayaran'] as $value) {
		//     $urutan= substr($value->IDFBPembayaran, 1);
		//   }
		//   $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
		//   $urutan_id_pembayaran= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		// }else{
		//   $urutan_id_pembayaran = 'P000001';
		// }
		// $datas5 = array(
		//   'IDFBPembayaran' => $urutan_id_pembayaran,
		//   'IDFB'=>$urutan_id,
		//   'Jenis_pembayaran' => $data3['jenis'],
		//   'IDCOA' => $data3['namacoa'],
		//   'NominalPembayaran' => $data3['nominal'],
		//   'IDMataUang' => 1,
		//   'Kurs' => '14000',
		//   'Tanggal_Giro' => $data3['tgl_giro']
		// );
		// $this->M_invoice_pembelian->add_pembayaran($datas5);
		$data['id_hutang'] = $this->M_hutang->tampilkan_id_hutang();
		if ($data['id_hutang'] != "") {
			// foreach ($data['id_hutang'] as $value) {
				$urutan_hutang = substr($data['id_hutang']->IDHutang, 1);
			// }
			$hasil_hutang = base_convert(base_convert($urutan_hutang, 36, 10) + 1, 10, 36);
			$urutan_id_hutang = 'P' . str_pad($hasil_hutang, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id_hutang = 'P000001';
		}
		// $simpan= $this->input->post('simpan');
		// $simtam= $this->input->post('simtam');
		// $nilai= str_replace(".", "", $this->input->post('nilai_hutang'));
		// $pembayaran= str_replace(".", "", $this->input->post('pembayaran'));

		$datahutang = array(
			'IDHutang' => $urutan_id_hutang,
			'IDFaktur' => $urutan_id,
			'Tanggal_Hutang' => $data1['Tanggal'],
			'Jatuh_Tempo' => $data1['Tanggal_jatuh_tempo'],
			'No_Faktur' => $data1['Nomor_invoice'],
			'Nilai_Hutang' => str_replace(".", "", $data3['total_invoice_pembayaran']),
			'Saldo_Awal' => str_replace(".", "", $data3['total_invoice_pembayaran']),
			'Pembayaran' => 0,
			'Saldo_Akhir' => str_replace(".", "", $data3['total_invoice_pembayaran']),
			'IDSupplier' => $data1['IDSupplier'],
			'Jenis_Faktur' => 'INV',
		);

		$this->M_hutang->simpan($datahutang);

		echo json_encode($data);
		//  }else{
		//   echo false;
		// }
	}

	public function edit($id) {
		$data['bank'] = $this->M_invoice_pembelian->tampil_bank();
		$data['detail'] = $this->M_invoice_pembelian->detail_invoice_pembelian($id);
		$data['supplier'] = $this->M_invoice_pembelian->tampil_supplier();
		$data['invoice'] = $this->M_invoice_pembelian->getById_edit($id);
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_pembelian/V_edit', $data);
	}

	public function ubah_invoice_pembelian() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		// $data3 = $this->input->post('data3');

		$data = array(
			'Tanggal' => $data1['Tanggal'],
			'Nomor' => $data1['Nomor_invoice'],
			'IDTBS' => $this->M_invoice_pembelian->get_IDTBS($data1['Nomor'])->IDTBS ? $this->M_invoice_pembelian->get_IDTBS($data1['Nomor'])->IDTBS : 0,
			'IDSupplier' => $data1['IDSupplier'],
			'No_sj_supplier' => $data1['No_supplier'],
			'TOP' => $data1['Jatuh_tempo'],
			'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
			'IDMataUang' => '6',
			'Kurs' => '-',
			'Total_qty_yard' => $data1['Total_qty_yard'],
			'Total_qty_meter' => $data1['Total_qty_meter'],
			'Saldo_yard' => $data1['Total_qty_yard'],
			'Saldo_meter' => $data1['Total_qty_meter'],
			'Discount' => $data1['Discount'],
			'Status_ppn' => $data1['Status_ppn'],
			'Keterangan' => $data1['Keterangan'],
			'Batal' => 'aktif',
		);
		$this->M_invoice_pembelian->update_master($data1['IDFB'], $data);

		$datas = array(
			'IDFB' => $data1['IDFB'],
			'Pembayaran' => $data2['jenis'],
			'DPP' => $data2['DPP'],
			'Discount' => $data2['Discount'],
			'PPN' => $data2['PPN'],
			'Grand_total' => $data2['total_invoice_pembayaran'],
			'Sisa' => $data2['nominal'] - $data2['total_invoice_pembayaran'],

		);
		$this->M_invoice_pembelian->update_grand_total($data1['IDFB'], $datas);

//   if($data2['namacoa']=="Cash")
		//   {
		//    $datas5 = array(
		//     'IDFB' => $data1['IDFB'],
		//     'Jenis_pembayaran' => $data2['jenis'],
		//     'IDCOA' => 0,
		//     'NominalPembayaran' => $data2['nominal'],
		//     'IDMataUang' => 1,
		//     'Kurs' => '14000',
		//     'Tanggal_Giro' => $data2['tgl_giro']
		//   );
		//  }else{
		//   $datas5 = array(
		//     'IDFB' => $data1['IDFB'],
		//     'Jenis_pembayaran' => $data2['jenis'],
		//     'IDCOA' => $data2['namacoa'],
		//     'NominalPembayaran' => $data2['nominal'],
		//     'IDMataUang' => 1,
		//     'Kurs' => '14000',
		//     'Tanggal_Giro' => $data2['tgl_giro']
		//   );
		// }
		// $this->M_invoice_pembelian->update_pembayaran($data1['IDFB'],$datas5);

	}

	function print($nomor) {

		$data['print'] = $this->M_invoice_pembelian->print_invoice($nomor);
		$id = $data['print']->IDFB;
		$data['printdetail'] = $this->M_invoice_pembelian->print_invoice_detail($id);
		$this->load->view('administrator/invoice_pembelian/V_print', $data);
	}

}