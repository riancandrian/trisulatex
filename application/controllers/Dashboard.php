
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Dashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/index.php', $data);
	}
}

?>