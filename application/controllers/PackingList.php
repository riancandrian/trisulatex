<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PackingList extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('M_packinglist', 'M');
		$this->load->model('M_suratjalan');
		$this->load->model('M_agen');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index() {
		$data = array(
			'title_master' => "Master Packing List", //Judul
			'title' => "List Data Packing List", //Judul Tabel
			'action' => site_url('PackingList/create'), //Alamat Untuk Action Form
			'button' => "Tambah Data", //Nama Button
			'packinglist' => $this->M->get_all(), //Load Data Packing List
		);
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/packinglist/V_packinglist', $data);
	}

	function cek_customer() {
		$Nomor = $this->input->post('Nomor');
		$data = $this->M->cekcustomer($Nomor);
		echo json_encode($data);
	}

	//----------------------Aksi Ubah Batal single
	public function set_edit($id) {
		$cek = $this->M->get_by_id($id);
		$data['packingget'] = $this->M->tampilkan_get_packing();

		if (count($data['packingget']) > 0) {
			foreach ($data['packingget'] as $valuepac) {
				if (($valuepac->IDPAC != $id)) {

					$batal = "-";
					if ($cek->Batal == "aktif") {
						$batal = "tidak aktif";
					} else {
						$batal = "aktif";
					}
					$data = array(
						'Batal' => $batal,
					);

					$this->M->update($id, $data);
					$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
				} else {
					$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Packing List Sudah Digunakan</div></center>");
				}
			}
		} else {
			$batal = "-";
			if ($cek->Batal == "aktif") {
				$batal = "tidak aktif";
			} else {
				$batal = "aktif";
			}
			$data = array(
				'Batal' => $batal,
			);

			$this->M->update($id, $data);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		}
		redirect('PackingList/index');
	}

	//----------------------Aksi Ubah Batal multiple
	public function delete_multiple() {
		$ID_att = $this->input->post('msg');
		if (empty($ID_att)) {
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
			redirect("PackingList/index");
		}
		$result = array();
		foreach ($ID_att AS $key => $val) {
			$result[] = array(
				"IDPAC" => $ID_att[$key],
				"Batal" => 'tidak aktif',
			);
		}
		$this->db->update_batch('tbl_packing_list', $result, 'IDPAC');
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		redirect("PackingList/index");
	}

	//Tambah Data Packing List
	public function create() {
		$this->load->model('M_supplier');
		//$this->load->model('M_po');

		$data['sok'] = $this->M->get_sok();
		$data['kodepackinglist'] = $this->M->no_packinglist(date('Y'));
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['trisula'] = $this->M->get_customer();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/packinglist/V_tambah_packinglist', $data);
	}

	//Ajax get data stok
	public function getstok() {
		$barcode = $this->input->post('barcode');
		$IDSOK = $this->input->post('IDSOK');

		$data = $this->M->getstok($barcode, $IDSOK);
		echo json_encode($data);

	}

	public function laporan_packing_list() {

		$data['pl'] = $this->M->get_all_procedure_packing(); //Load Data Purchase Order
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/packinglist/V_laporan', $data);
	}

	//SImpan Data Ajax
	public function simpan_packinglist() {

		$data1 = $this->input->post('_data1');
		$data2 = $this->input->post('_data2');
		$data['id_packing_list'] = $this->M->tampilkan_id_packing_list();
		if ($data['id_packing_list'] != "") {
			// foreach ($data['id_packing_list'] as $value) {
				$urutan = substr($data['id_packing_list']->IDPAC, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id = 'P000001';
		}
		$data_atas = array(
			'IDPAC' => $urutan_id,
			'Tanggal' => $data1['Tanggal'],
			'Nomor' => $data1['Nomor'],
			'IDSOK' => $data1['IDSOK'],
			'IDCustomer' => $data1['IDCustomer'],
			'IDMataUang' => $data1['IDMataUang'],
			'Kurs' => $data1['Kurs'],
			'Total_qty_grade_a' => $data1['Total_qty_grade_a'],
			'Total_pcs_grade_a' => $data1['Total_pcs_grade_a'],
			'Total_qty_grade_b' => $data1['Total_qty_grade_b'],
			'Total_pcs_grade_b' => $data1['Total_pcs_grade_b'],
			'Total_qty_grade_s' => $data1['Total_qty_grade_s'],
			'Total_pcs_grade_s' => $data1['Total_pcs_grade_s'],
			'Total_qty_grade_e' => $data1['Total_qty_grade_e'],
			'Total_pcs_grade_e' => $data1['Total_pcs_grade_e'],
			'Keterangan' => $data1['Keterangan'],
			'Batal' => $data1['Batal'],
		);
		$last_insert_id = $this->M->add($data_atas);
		$data_bawah = null;
		foreach ($data2 as $row) {
			$data['id_packing_list_detail'] = $this->M->tampilkan_id_packing_list_detail();
			if ($data['id_packing_list_detail'] != "") {
				// foreach ($data['id_packing_list_detail'] as $value) {
					$urutan = substr($data['id_packing_list_detail']->IDPACDetail, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail = 'P000001';
			}
			$data_bawah = array(
				'IDPACDetail' => $urutan_id_detail,
				'IDPAC' => $urutan_id,
				'Barcode' => $row['Barcode'],
				'IDBarang' => $row['IDBarang'],
				'IDCorak' => $row['IDCorak'],
				'IDWarna' => $row['IDWarna'],
				'IDMerk' => $row['IDMerk'],
				'Qty_yard' => $row['yardnew'],
				'Qty_meter' => $row['meternew'],
				'Saldo_yard' => $row['Saldo_yard'],
				'Saldo_meter' => $row['Saldo_meter'],
				'Grade' => $row['Grade'],
				'IDSatuan' => $row['IDSatuan'],
			);
			$this->M->add_detail($data_bawah);
			$getstokbyid = $this->M->getstokbyid($row['Barcode']);
			//die(print_r($getstokbyid));
			$ubahstok = array(
				'Saldo_yard' => $getstokbyid->Saldo_yard - $row['yardnew'],
				'Saldo_meter' => $getstokbyid->Saldo_meter - $row['meternew'],
			);
			$this->M->updatestok($ubahstok, $row['Barcode']);

			//save stock card
			$data['id_kartu_stok'] = $this->M->tampilkan_id_kartu_stok();
			if ($data['id_kartu_stok'] != "") {
				// foreach ($data['id_kartu_stok'] as $value) {
					$urutan = substr($data['id_kartu_stok']->IDKartuStok, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail_kartustok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail_kartustok = 'P000001';
			}
			$save_stock_card = array(
				'IDKartuStok' => $urutan_id_detail_kartustok,
				'Tanggal' => $data1['Tanggal'],
				'Nomor_faktur' => $data1['Nomor'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'IDStok' => '-',
				'Jenis_faktur' => 'PL',
				'Barcode' => $row['Barcode'],
				'IDBarang' => $row['IDBarang'],
				'IDCorak' => $row['IDCorak'],
				'IDWarna' => $row['IDWarna'],
				'IDGudang' => 0,
				'Masuk_yard' => 0,
				'Masuk_meter' => 0,
				'Keluar_yard' => $row['yardnew'],
				'Keluar_meter' => $row['meternew'],
				'Grade' => $row['Grade'],
				'IDSatuan' => $row['IDSatuan'],
				'Harga' => 0,
				'IDMataUang' => 1,
				'Kurs' => '',
				'Total' => 0,
			);

			$this->M->save_stock_card($save_stock_card);

//save jurnal
			$data['id_jurnal'] = $this->M_suratjalan->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal = 'P000001';
			}

			$save_jurnal_kredit = array(
				'IDJurnal' => $urutan_id_jurnal,
				'Tanggal' => $data1['Tanggal'],
				'Nomor' => $data1['Nomor'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'Jenis_faktur' => 'PL',
				'IDCOA' => 'P000016',
				'Debet' => 0,
				'Kredit' => $getstokbyid->Total,
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => 0,
				'Total_kredit' => $getstokbyid->Total,
				'Keterangan' => $data1['Keterangan'],
				'Saldo' => $getstokbyid->Total,
			);
			$this->M_suratjalan->save_jurnal($save_jurnal_kredit);

			$data['id_jurnal'] = $this->M_suratjalan->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal_debet = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal_debet = 'P000001';
			}

			$save_jurnal_debet = array(
				'IDJurnal' => $urutan_id_jurnal_debet,
				'Tanggal' => $data1['Tanggal'],
				'Nomor' => $data1['Nomor'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'Jenis_faktur' => 'PL',
				'IDCOA' => 'P000612',
				'Debet' => $getstokbyid->Total,
				'Kredit' => 0,
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => $getstokbyid->Total,
				'Total_kredit' => 0,
				'Keterangan' => $data1['Keterangan'],
				'Saldo' => $getstokbyid->Total,
			);
			$this->M_suratjalan->save_jurnal($save_jurnal_debet);
		}
		//$this->M->add($data_atas);
		echo json_encode($data_bawah);

	}

	//----------------------Detail per data
	public function show($id) {
		$data['packinglist'] = $this->M->get_by_id($id);
		$data['packinglistdetail'] = $this->M->get_packinglist_detail($id);
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/packinglist/V_show', $data);
	}

	//Ubah data packinglist
	public function edit($id) {
		$this->load->model('M_supplier');
		//$this->load->model('M_po');

		$data['packinglist'] = $this->M->get_by_id($id);
		$data['datapackinglistdetail'] = $this->M->get_packinglist_detail($id);
		// $data['kodepackinglist'] = $this->M->no_packinglist();
		//$data['po'] = $this->M_po->get_all('PO Celup');
		$data['trisula'] = $this->M->get_customer();
		$data['sok'] = $this->M->get_sok();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/packinglist/V_edit_packinglist', $data);
	}

	//aksi Ubah packinglist
	public function ubah_packinglist() {

		$data1 = $this->input->post('_data1');
		$data2 = $this->input->post('_data2');
		$data3 = $this->input->post('_data3');
		$IDPAC = $this->input->post('_id');

		$data_atas = array(
			'Tanggal' => $data1['Tanggal'],
			'Nomor' => $data1['Nomor'],
			'IDSOK' => $data1['IDSOK'],
			'IDCustomer' => $data1['IDCustomer'],
			'IDMataUang' => $data1['IDMataUang'],
			'Kurs' => $data1['Kurs'],
			'Total_qty_grade_a' => $data1['Total_qty_grade_a'],
			'Total_pcs_grade_a' => $data1['Total_pcs_grade_a'],
			'Total_qty_grade_b' => $data1['Total_qty_grade_b'],
			'Total_pcs_grade_b' => $data1['Total_pcs_grade_b'],
			'Total_qty_grade_s' => $data1['Total_qty_grade_s'],
			'Total_pcs_grade_s' => $data1['Total_pcs_grade_s'],
			'Total_qty_grade_e' => $data1['Total_qty_grade_e'],
			'Total_pcs_grade_e' => $data1['Total_pcs_grade_e'],
			'Keterangan' => $data1['Keterangan'],
			'Batal' => $data1['Batal'],
		);

		if ($this->M->update_master($IDPAC, $data_atas)) {
			$data_atas = null;
			if ($data3 > 0) {
				foreach ($data3 as $row) {
					$this->M->drop($row['IDPACDetail']);
					// console.log('delete temp');
				}
			}
			if ($data2 != null) {
				foreach ($data2 as $row) {
					$data['id_packing_list_detail'] = $this->M->tampilkan_id_packing_list_detail();
					if ($data['id_packing_list_detail'] != "") {
						// foreach ($data['id_packing_list_detail'] as $value) {
							$urutan = substr($data['id_packing_list_detail']->IDPACDetail, 1);
						// }
						$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
						$urutan_id_detail = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
					} else {
						$urutan_id_detail = 'P000001';
					}
					$data_atas = array(
						'IDPACDetail' => $urutan_id_detail,
						'IDPAC' => $IDPAC,
						'Barcode' => $row['Barcode'],
						'IDBarang' => $row['IDBarang'],
						'IDCorak' => $row['IDCorak'],
						'IDWarna' => $row['IDWarna'],
						'IDMerk' => $row['IDMerk'],
						'Qty_yard' => $row['yardnew'],
						'Qty_meter' => $row['meternew'],
						'Saldo_yard' => $row['Saldo_yard'],
						'Saldo_meter' => $row['Saldo_meter'],
						'Grade' => $row['Grade'],
						'IDSatuan' => $row['IDSatuan'],
					);
					if ($row['IDPACDetail'] == '') {
						$this->M->add_detail($data_atas);
						$getstokbyid = $this->M->getstokbyid($row['IDStok']);
						//die(print_r($getstokbyid));
						$ubahstok = array(
							'Qty_yard' => $getstokbyid->Qty_yard - $row['yardnew'],
							'Qty_meter' => $getstokbyid->Qty_meter - $row['meternew'],
						);
						$this->M->updatestok($ubahstok, $row['IDStok']);
					}
				}
			}
			echo true;
		} else {
			echo false;
			// echo json_encode($data_atas);
		}

		//$this->M->update_master($IDPAC,$data_atas);
		//echo json_encode($data3);

	}

	//--------------------------------pencarian
	function pencarian() {
		$data = array(
			'title_master' => "Master Packing List", //Judul
			'title' => "List Data Packing List", //Judul Tabel
			'action' => site_url('PackingList/create'), //Alamat Untuk Action Form
			'button' => "Tambah Data", //Nama Button
			//'packinglist' => $this->M->get_all() //Load Data Packing List
		);
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['packinglist'] = $this->M->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
		$this->load->view('administrator/packinglist/V_packinglist', $data);
	}

	function print_packing_list($nomor) {

		$data['print'] = $this->M->print_packinglist($nomor);
		$this->load->view('administrator/packinglist/V_print', $data);
	}

}

/* End of file packinglist.php */
