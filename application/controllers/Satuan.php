<!-- Programmer : Rais Naufal Hawari
 Date       : 12-07-2018 -->

 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Satuan extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_satuan');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
  }

  public function index()
  {
    $data = array(
        'title_master' => "Master Satuan", //Judul 
        'title' => "List Data Satuan", //Judul Tabel
        'action' => site_url('Satuan/create'), //Alamat Untuk Action Form
        'button' => "Tambah Satuan", //Nama Button
        'satuan' => $this->M_satuan->get_all() //Load data Satuan
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/satuan/satuan_list.php', $data);
  }
  
  //Tambah Data Satuan
  public function create()
  {
    $data = array(
        'title_master' => "Master Satuan", //Judul 
        'title' => "Tambah Data Satuan" , //Judul Tabel
        'action' => site_url('Satuan/create_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Satuan'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'Kode_Satuan' => set_value('Kode_Satuan'), //Kode Satuan
        'IDSatuan' => set_value('IDSatuan'), //Id 
        'Satuan' => set_value('Satuan'), //Satuan
        'Aktif' => set_value('Aktif') //Aktif
      );
    
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/satuan/satuan_form.php', $data);
  }
  
  //Tambah Data Satuan
  public function create_action()
  {
    $data['id_satuan'] = $this->M_satuan->tampilkan_id_satuan();
    if($data['id_satuan']!=""){
      // foreach ($data['id_satuan'] as $value) {
        $urutan= substr($data['id_satuan']->IDSatuan, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data = array(
      'IDSatuan' => $urutan_id,
      'Kode_Satuan' => $this->input->post('Kode_Satuan'),
      'Satuan' => $this->input->post('Satuan'),
      'Aktif' => 'aktif'
      
    );
    $this->M_satuan->insert($data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
    redirect('Satuan/create');
  }
  
  //Edit Data Satuan
  public function update($id)
  {
    $cari = $this->M_satuan->get_by_id($id);//Cari apakah data yang dimaksud ada di database
    if($cari) {
      $data = array(
        'title_master' => "Master Satuan", //Judul 
        'title' => "Ubah Data Satuan" , //Judul Tabel
        'action' => site_url('Satuan/update_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Satuan'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'Kode_Satuan' => set_value('Kode_Satuan', $cari->Kode_Satuan), //Kode Satuan
        'IDSatuan' => set_value('IDSatuan', $cari->IDSatuan), //Id 
        'Satuan' => set_value('Satuan', $cari->Satuan), //Satuan
        'Aktif' => set_value('Aktif', $cari->Aktif) //Aktif
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/satuan/satuan_form.php', $data);
    } else {
     redirect('Satuan/index');
   }
 }

  //Aksi Ubah Data
 public function update_action()
 {

    $id = $this->input->post('IDSatuan'); //Get id sebagai penanda record yang akan dirubah
    $data = array(
      'Kode_Satuan' => $this->input->post('Kode_Satuan'),
      'Satuan' => $this->input->post('Satuan')
    );
    $this->M_satuan->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
    redirect('Satuan/index');
  }

  //Hapus Data Satuan
    public function delete($id)
  {
     //  $cari = $this->M_satuan->get_by_id($id); 

     //  if ($cari) {
     //    $this->M_satuan->delete($id);
     //    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
     //    redirect('Satuan/index');
     //  } else {
     //   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Gagal di Hapus</div></center>");
     //   redirect('Satuan/index');
     // }


     $data['barangget']= $this->M_satuan->get_barang($id);
    if(count($data['barangget']) > 0){
        if(($data['barangget']->IDSatuan!=$id)){   
            $this->M_satuan->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
        redirect('Satuan/index');
        }else{
            $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
            redirect('Satuan/index');
        }
    }else{
        $this->M_satuan->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
        redirect('Satuan/index');
    }
     
   }

   public function pencarian()
   {
    $data = array(
        'title_master' => "Master Satuan", //Judul 
        'title' => "List Data Satuan", //Judul Tabel
        'action' => site_url('Satuan/create'), //Alamat Untuk Action Form
        'button' => "Tambah Satuan", //Nama Button
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $kolom= addslashes($this->input->post('jenispencarian'));
    $status= addslashes($this->input->post('statuspencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $status!="" && $keyword!="") {
      $data['satuan']=$this->M_satuan->cari($kolom,$status,$keyword);
    }elseif ($kolom!="" && $keyword!="") {
      $data['satuan']=$this->M_satuan->cari_by_kolom($kolom,$keyword);
    }elseif ($status!="") {
      $data['satuan']=$this->M_satuan->cari_by_status($status);
    }elseif ($keyword!="") {
      $data['satuan']=$this->M_satuan->cari_by_keyword($keyword);
    }else {
      $data['satuan']=$this->M_satuan->get_all();
    }
    $this->load->view('administrator/satuan/satuan_list.php', $data);
  }

}

/* End of file Satuan.php */
