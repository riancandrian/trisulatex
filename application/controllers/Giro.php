<!-- Programmer : Rais Naufal Hawari
 Date       : 12-07-2018 -->

 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Giro extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_giro');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
  }

  public function index()
  {
    $data = array(
        'title_master' => "Data Giro Masuk", //Judul 
        'title' => "List Data Giro Masuk", //Judul Tabel
        'action' => site_url('Giro/create'), //Alamat Untuk Action Form
        'button' => "Tambah Data", //Nama Button
        'Giro' => $this->M_giro->get_all() //Load data Giro
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/giro/giro_list.php', $data);
  }

  public function index_giro_keluar()
  {
    $data = array(
        'title_master' => "Data Giro Keluar", //Judul 
        'title' => "List Data Giro Keluar", //Judul Tabel
        'action' => site_url('Giro/create_keluar'), //Alamat Untuk Action Form
        'button' => "Tambah Data", //Nama Button
        'Giro' => $this->M_giro->get_all_keluar() //Load data Giro
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/giro/giro_list_keluar.php', $data);
  }
  
  //Tambah Data Giro
  public function create()
  {
    $data = array(
        'title_master' => "Data Giro", //Judul 
        'title' => "Tambah Data Giro" , //Judul Tabel
        'action' => site_url('Giro/create_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Giro'),//Alamat Untuk back
        'button' => "Tambah", //Nama Button
        'IDGiro' => set_value('IDGiro'),
        'IDFaktur' => set_value('IDFaktur'),
        'IDPerusahaan' => $this->M_giro->get_all_customer(),
        'Jenis_Faktur' => set_value('Jenis_Faktur'),
        'Nomor_Faktur' => set_value('Nomor_Faktur'),
        'IDBank' => set_value('IDBank'),
        'DataBank' => $this->M_giro->get_all_bank(),
        'Nomor_Giro' => set_value('Nomor_Giro'),
        'Tanggal_Faktur' => set_value('Tanggal_Faktur'),
        'Tanggal_Cair' => set_value('Tanggal_Cair'),
        'Nilai' => set_value('Nilai'),
        'Status_Giro' => set_value('Status_Giro'),
        'Jenis_Giro' => set_value('Jenis_Giro'),
      );

    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/giro/giro_form.php', $data);
  }

  public function create_keluar()
  {
    $data = array(
        'title_master' => "Data Giro Keluar", //Judul 
        'title' => "Tambah Data Giro Keluar" , //Judul Tabel
        'action' => site_url('Giro/create_action_keluar'), //Alamat Untuk Action Form
        'action_back' => site_url('Giro/index_giro_keluar'),//Alamat Untuk back
        'button' => "Tambah", //Nama Button
        'IDGiro' => set_value('IDGiro'),
        'IDFaktur' => set_value('IDFaktur'),
        'IDPerusahaan' => $this->M_giro->get_all_supplier(),
        'Jenis_Faktur' => set_value('Jenis_Faktur'),
        'Nomor_Faktur' => set_value('Nomor_Faktur'),
        'IDBank' => set_value('IDBank'),
        'DataBank' => $this->M_giro->get_all_bank(),
        'Nomor_Giro' => set_value('Nomor_Giro'),
        'Tanggal_Faktur' => set_value('Tanggal_Faktur'),
        'Tanggal_Cair' => set_value('Tanggal_Cair'),
        'Nilai' => set_value('Nilai'),
        'Status_Giro' => set_value('Status_Giro'),
        'Jenis_Giro' => set_value('Jenis_Giro'),
      );

    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/giro/giro_form_keluar.php', $data);
  }
  
  //Tambah Data Giro
  public function create_action()
  {
   $data['id_giro'] = $this->M_giro->tampilkan_id_giro();
   if($data['id_giro']!=""){
    foreach ($data['id_giro'] as $value) {
      $urutan= substr($value->IDGiro, 1);
    }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }
  $simpan= $this->input->post('simpan');
  $simtam= $this->input->post('simtam');
  $nilai= str_replace(".", "", $this->input->post('Nilai'));
  $data = array(
    'IDGiro' => $urutan_id,
    'IDFaktur' => $urutan_id,
    'IDPerusahaan' => $this->input->post('customer'),
    'Jenis_Faktur' => $this->input->post('Jenis_Faktur'),
    'Nomor_Faktur' => $this->input->post('Nomor_Faktur'),
    'IDBank' => $this->input->post('IDBank'),
    'Nomor_Giro' => $this->input->post('Nomor_Giro'),
    'Tanggal_Faktur' => $this->input->post('Tanggal_Faktur'),
    'Tanggal_Cair' => $this->input->post('Tanggal_Cair'),
    'Nilai' => $nilai,
    'Status_Giro' => $this->input->post('Status_Giro'),
    'Jenis_Giro' => $this->input->post('Jenis_Giro'),
  );
  $this->M_giro->insert($data);
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
  if($simpan){
    redirect('Giro/index');
  }elseif($simtam){
    redirect('Giro/create');
  }
}

public function create_action_keluar()
{
  $data['id_giro'] = $this->M_giro->tampilkan_id_giro();
  if($data['id_giro']!=""){
foreach ($data['id_giro'] as $value) {
  $urutan= substr($value->IDGiro, 1);
}
$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
}else{
  $urutan_id = 'P000001';
}
  $simpan= $this->input->post('simpan');
  $simtam= $this->input->post('simtam');
  $nilai= str_replace(".", "", $this->input->post('Nilai'));

  $data = array(
    'IDGiro' => $urutan_id,
    'IDFaktur' => $urutan_id,
    'IDPerusahaan' => $this->input->post('supplier'),
    'Jenis_Faktur' => $this->input->post('Jenis_Faktur'),
    'Nomor_Faktur' => $this->input->post('Nomor_Faktur'),
    'IDBank' => $this->input->post('IDBank'),
    'Nomor_Giro' => $this->input->post('Nomor_Giro'),
    'Tanggal_Faktur' => $this->input->post('Tanggal_Faktur'),
    'Tanggal_Cair' => $this->input->post('Tanggal_Cair'),
    'Nilai' => $nilai,
    'Status_Giro' => $this->input->post('Status_Giro'),
    'Jenis_Giro' => $this->input->post('Jenis_Giro'),
  );
  $this->M_giro->insert($data);
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
  if($simpan){
    redirect('Giro/index_giro_keluar');
  }elseif($simtam){
    redirect('Giro/create_keluar');
  }
}

  //Edit Data Giro
public function update($id)
{
    $cari = $this->M_giro->get_by_id($id);//Cari apakah data yang dimaksud ada di database

    if($cari) {
      $data = array(
        'title_master' => "Data Giro", //Judul 
        'title' => "Ubah Data Giro" , //Judul Tabel
        'action' => site_url('giro/update_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Giro'),//Alamat Untuk back
        'button' => "Ubah", //Nama Button
        'IDGiro' => set_value('IDGiro', $cari->IDGiro),
        'IDFaktur' => set_value('IDFaktur', $cari->IDFaktur),
        'IDCustomer' => set_value('IDPerusahaan', $cari->IDPerusahaan),
        'IDPerusahaan' => $this->M_giro->get_all_customer(),
        'Jenis_Faktur' => set_value('Jenis_Faktur', $cari->Jenis_Faktur),
        'Nomor_Faktur' => set_value('Nomor_Faktur', $cari->Nomor_Faktur),
        'IDBank' => set_value('IDBank', $cari->IDBank),
        'DataBank' => $this->M_giro->get_all_bank(),
        'Nomor_Giro' => set_value('Nomor_Giro', $cari->Nomor_Giro),
        'Tanggal_Faktur' => set_value('Tanggal_Faktur', $cari->Tanggal_Faktur),
        'Tanggal_Cair' => set_value('Tanggal_Cair', $cari->Tanggal_Cair),
        'Nilai' => set_value('Nilai', $cari->Nilai),
        'Status_Giro' => set_value('Status_Giro', $cari->Status_Giro),
        'Jenis_Giro' => set_value('Jenis_Giro', $cari->Jenis_Giro),
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/giro/giro_form.php', $data);
    } else {
      $this->session->set_flashdata('message', 'Maaf, Data Tidak Ditemukan');
      redirect('Giro','refresh');
    }
  }

  public function update_keluar($id)
  {
    $cari = $this->M_giro->get_by_id($id);//Cari apakah data yang dimaksud ada di database

    if($cari) {
      $data = array(
        'title_master' => "Data Giro Keluar", //Judul 
        'title' => "Ubah Data Giro Keluar" , //Judul Tabel
        'action' => site_url('giro/update_action_keluar'), //Alamat Untuk Action Form
        'action_back' => site_url('Giro/index_giro_keluar'),//Alamat Untuk back
        'button' => "Ubah", //Nama Button
        'IDGiro' => set_value('IDGiro', $cari->IDGiro),
        'IDFaktur' => set_value('IDFaktur', $cari->IDFaktur),
        'IDSupplier' => set_value('IDPerusahaan', $cari->IDPerusahaan),
        'IDPerusahaan' => $this->M_giro->get_all_supplier(),
        'Jenis_Faktur' => set_value('Jenis_Faktur', $cari->Jenis_Faktur),
        'Nomor_Faktur' => set_value('Nomor_Faktur', $cari->Nomor_Faktur),
        'IDBank' => set_value('IDBank', $cari->IDBank),
        'DataBank' => $this->M_giro->get_all_bank(),
        'Nomor_Giro' => set_value('Nomor_Giro', $cari->Nomor_Giro),
        'Tanggal_Faktur' => set_value('Tanggal_Faktur', $cari->Tanggal_Faktur),
        'Tanggal_Cair' => set_value('Tanggal_Cair', $cari->Tanggal_Cair),
        'Nilai' => set_value('Nilai', $cari->Nilai),
        'Status_Giro' => set_value('Status_Giro', $cari->Status_Giro),
        'Jenis_Giro' => set_value('Jenis_Giro', $cari->Jenis_Giro),
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/giro/giro_form_keluar.php', $data);
    } else {
      $this->session->set_flashdata('message', 'Maaf, Data Tidak Ditemukan');
      redirect('Giro','refresh');
    }
  }

  //Aksi Ubah Data
  public function update_action()
  {

    $id = $this->input->post('IDGiro'); //Get id sebagai penanda record yang akan dirubah
    $nilai= str_replace(".", "", $this->input->post('Nilai'));
    $data = array(
      // 'IDFaktur' => $this->input->post('IDFaktur'),
      'IDPerusahaan' => $this->input->post('customer'),
      'Jenis_Faktur' => $this->input->post('Jenis_Faktur'),
      'Nomor_Faktur' => $this->input->post('Nomor_Faktur'),
      'IDBank' => $this->input->post('IDBank'),
      'Nomor_Giro' => $this->input->post('Nomor_Giro'),
      'Tanggal_Faktur' => $this->input->post('Tanggal_Faktur'),
      'Tanggal_Cair' => $this->input->post('Tanggal_Cair'),
      'Nilai' => $nilai,
      'Status_Giro' => $this->input->post('Status_Giro'),
      'Jenis_Giro' => $this->input->post('Jenis_Giro'),
    );
    $this->M_giro->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('Giro');
  }
  public function update_action_keluar()
  {

    $id = $this->input->post('IDGiro'); //Get id sebagai penanda record yang akan dirubah
    $nilai= str_replace(".", "", $this->input->post('Nilai'));
    $data = array(
      // 'IDFaktur' => $this->input->post('IDFaktur'),
      'IDPerusahaan' => $this->input->post('supplier'),
      'Jenis_Faktur' => $this->input->post('Jenis_Faktur'),
      'Nomor_Faktur' => $this->input->post('Nomor_Faktur'),
      'IDBank' => $this->input->post('IDBank'),
      'Nomor_Giro' => $this->input->post('Nomor_Giro'),
      'Tanggal_Faktur' => $this->input->post('Tanggal_Faktur'),
      'Tanggal_Cair' => $this->input->post('Tanggal_Cair'),
      'Nilai' => $nilai,
      'Status_Giro' => $this->input->post('Status_Giro'),
      'Jenis_Giro' => $this->input->post('Jenis_Giro'),
    );
    $this->M_giro->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('Giro/index_giro_keluar');
  }

  //MElihat detail data
  public function read($id)
  {
    $cari = $this->M_giro->get_by_id($id);//Cari apakah data yang dimaksud ada di database

    if($cari) {
      $data = array(
        'title_master' => "Data Giro", //Judul 
        'title' => "Detail Data Giro" , //Judul Tabel
        'action_back' => site_url('Giro'),//Alamat Untuk back
        'button' => "Ubah", //Nama Button
        'IDGiro' => set_value('IDGiro', $cari->IDGiro),
        'IDFaktur' => set_value('IDFaktur', $cari->IDFaktur),
        'Nomor_Faktur' => set_value('Nomor_Faktur', $cari->Nomor_Faktur),
        'IDPerusahaan' => set_value('IDPerusahaan', $cari->IDPerusahaan),
        'Jenis_Faktur' => set_value('Jenis_Faktur', $cari->Jenis_Faktur),
        'Nomor_Rekening' => set_value('Nomor_Rekening', $cari->Nomor_Rekening),
        'Atas_Nama' => set_value('Atas_Nama', $cari->Atas_Nama),
        'DataBank' => $this->M_giro->get_all_bank(),
        'Nomor_Giro' => set_value('Nomor_Giro', $cari->Nomor_Giro),
        'Tanggal_Faktur' => set_value('Tanggal_Faktur', $cari->Tanggal_Faktur),
        'Tanggal_Cair' => set_value('Tanggal_Cair', $cari->Tanggal_Cair),
        'Nilai' => set_value('Nilai', $cari->Nilai),
        'Status_Giro' => set_value('Status_Giro', $cari->Status_Giro),
        'Jenis_Giro' => set_value('Jenis_Giro', $cari->Jenis_Giro),
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/giro/giro_read.php', $data);
    } else {
      $this->session->set_flashdata('message', 'Maaf, Data Tidak Ditemukan');
      redirect('Giro','refresh');
    }
  }

  public function read_keluar($id)
  {
    $cari = $this->M_giro->get_by_id($id);//Cari apakah data yang dimaksud ada di database

    if($cari) {
      $data = array(
        'title_master' => "Data Giro", //Judul 
        'title' => "Detail Data Giro" , //Judul Tabel
        'action_back' => site_url('Giro'),//Alamat Untuk back
        'button' => "Ubah", //Nama Button
        'IDGiro' => set_value('IDGiro', $cari->IDGiro),
        'IDFaktur' => set_value('IDFaktur', $cari->IDFaktur),
        'Nomor_Faktur' => set_value('Nomor_Faktur', $cari->Nomor_Faktur),
        'IDPerusahaan' => set_value('IDPerusahaan', $cari->IDPerusahaan),
        'Jenis_Faktur' => set_value('Jenis_Faktur', $cari->Jenis_Faktur),
        'Nomor_Rekening' => set_value('Nomor_Rekening', $cari->Nomor_Rekening),
        'Atas_Nama' => set_value('Atas_Nama', $cari->Atas_Nama),
        'DataBank' => $this->M_giro->get_all_bank(),
        'Nomor_Giro' => set_value('Nomor_Giro', $cari->Nomor_Giro),
        'Tanggal_Faktur' => set_value('Tanggal_Faktur', $cari->Tanggal_Faktur),
        'Tanggal_Cair' => set_value('Tanggal_Cair', $cari->Tanggal_Cair),
        'Nilai' => set_value('Nilai', $cari->Nilai),
        'Status_Giro' => set_value('Status_Giro', $cari->Status_Giro),
        'Jenis_Giro' => set_value('Jenis_Giro', $cari->Jenis_Giro),
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/giro/giro_read_keluar.php', $data);
    } else {
      $this->session->set_flashdata('message', 'Maaf, Data Tidak Ditemukan');
      redirect('Giro/index_giro_keluar');
    }
  }

  //Hapus Data Giro
  public function delete($id)
  {
      $cari = $this->M_giro->get_by_id($id); //Cari apakah data yang dimaksud ada di database

      if ($cari) {
        $this->M_giro->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil dihapus</div></center>");
        redirect('Giro');
      } else {
       $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Gagal dihapus</div></center>");
       redirect('Giro');
     }

   }

   public function delete_keluar($id)
   {
      $cari = $this->M_giro->get_by_id($id); //Cari apakah data yang dimaksud ada di database

      if ($cari) {
        $this->M_giro->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil dihapus</div></center>");
        redirect('Giro/index_giro_keluar');
      } else {
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Gagal dihapus</div></center>");
        redirect('Giro/index_giro_keluar');
      }
      
    }

    public function pencarian()
    {
      $data = array(
        'title_master' => "Data Giro", //Judul 
        'title' => "List Data Giro", //Judul Tabel
        'action' => site_url('Giro/create'), //Alamat Untuk Action Form
        'button' => "Tambah Giro", //Nama Button
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $kolom= addslashes($this->input->post('jenispencarian'));
      $keyword= addslashes($this->input->post('keyword'));

      if ($kolom!="" && $keyword!="") {
        $data['Giro']=$this->M_giro->cari_by_kolom($kolom,$keyword);
      }elseif ($keyword!="") {
        $data['Giro']=$this->M_giro->cari_by_keyword($keyword);
      }else {
        $data['Giro']=$this->M_giro->get_all();
      }
      $this->load->view('administrator/giro/giro_list', $data);
    }

    public function pencarian_keluar()
    {
      $data = array(
        'title_master' => "Data Giro", //Judul 
        'title' => "List Data Giro", //Judul Tabel
        'action' => site_url('Giro/create'), //Alamat Untuk Action Form
        'button' => "Tambah Giro", //Nama Button
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $kolom= addslashes($this->input->post('jenispencarian'));
      $keyword= addslashes($this->input->post('keyword'));

      if ($kolom!="" && $keyword!="") {
        $data['Giro']=$this->M_giro->cari_by_kolom($kolom,$keyword);
      }elseif ($keyword!="") {
        $data['Giro']=$this->M_giro->cari_by_keyword($keyword);
      }else {
        $data['Giro']=$this->M_giro->get_all_keluar();
      }
      $this->load->view('administrator/giro/giro_list_keluar', $data);
    }


  }

  /* End Giro.php */
