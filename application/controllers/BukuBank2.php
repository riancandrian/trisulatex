<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class BukuBank extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_bukubank', 'M');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
  }

  public function index()
  {
    $data = array(
      'title_master' => "Master Buku Bank", //Judul 
      'title' => "List Data Buku Bank", //Judul Tabel
      'action' => site_url('BukuBank/create'), //Alamat Untuk Action Form
      'button' => "Tambah Data", //Nama Button
      'bukubank' => $this->M->get_all() //Load Data Jurnal Umum
    );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/buku_bank/V_bukubank', $data);
  }

  public function laporan_buku_bank()
  {
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['coa_group_bank'] = $this->M->get_coa_group_bank();
    $this->load->view('administrator/buku_bank/V_laporan_buku_bank', $data);
  }

  public function pencarian_buku_bank()
  {
    $date_from= $this->input->post('date_from');
    $date_until= $this->input->post('date_until');
    $search_type= $this->input->post('jenispencarian');
    $keyword= $this->input->post('keyword');
    $data['coa_group_bank'] = $this->M->get_coa_group_bank();
    $data['laporanbukubank'] = $this->M->searching_laporan_buku_bank($date_from, $date_until, $search_type, $keyword);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/buku_bank/V_laporan_buku_bank', $data);
  }


  //----------------------Aksi Ubah Batal single
  public function set_edit($id)
  {
    $cek = $this->M->get_by_id($id);
    //die(print_r($cek));
    $batal = "-";
    if ($cek->Batal == "aktif") {
      $batal ="tidak aktif";
    } else {
      $batal = "aktif";
    }
    $data = array(
      'Batal' => $batal
    );

    $this->M->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('BukuBank/index');
  }

  //----------------------Aksi Ubah Batal multiple
  public function delete_multiple()
  {
    $ID_att = $this->input->post('msg');
    if (empty($ID_att)) {
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
      redirect("BukuBank/index");
    }
    $result = array();
    foreach($ID_att AS $key => $val){
      $result[] = array(
        "IDJU" => $ID_att[$key],
        "Batal"  => 'tidak aktif'
      );
    }
    $this->db->update_batch('tbl_jurnal_umum', $result, 'IDJU');
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect("BukuBank/index");
  }

  //Tambah Data Jurnal Umum
  public function create()
  {
    $this->load->model('M_supplier');
    
    $data['kodebukubank'] = $this->M->no_bukubank(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $data['coa_group'] = $this->M->get_coa_group();
    $data['coa_group_bank'] = $this->M->get_coa_group_bank();
    $data['coa'] = $this->M->get_coa();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/buku_bank/V_tambah_bukubank', $data);
  }

  //Ajax get data stok
  public function getstok()
  {
    $barcode = $this->input->post('barcode');
    $data = $this->M->getstok($barcode);
    echo json_encode($data);
    
  }


 //SImpan Data Ajax
  public function simpan_bukubank(){

    $data1 = $this->input->post('_data1');
    $data2 = $this->input->post('_data2');
    $data['id_buku_bank'] = $this->M->tampilkan_id_buku_bank();
    if($data['id_buku_bank']!=""){
      foreach ($data['id_buku_bank'] as $value) {
        $urutan= substr($value->IDBukuBank, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data_atas = array(
      'IDBukuBank' => $urutan_id,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDCoa' => $data1['IDCOA'],
      'Posisi' => $data1['Posisi'],
      'Keterangan' => $data1['Keterangan'],
      'Batal' => $data1['Batal'],
    );
    $last_insert_id = $this->M->add($data_atas); 
    $data_bawah = null;
    foreach($data2 as $row){
     $data['id_buku_bank_detail'] = $this->M->tampilkan_id_buku_bank_detail();
     if($data['id_buku_bank_detail']!=""){
      foreach ($data['id_buku_bank_detail'] as $value) {
        $urutan= substr($value->IDBukuBankDetail, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }
    $data_bawah = array(
      'IDBukuBankDetail'=> $urutan_id_detail,
      'IDBukuBank'=>$urutan_id,
      'IDCoa' => $row['IDCOA'],
      'Posisi' => $row['Posisi'],
      'Debet' => str_replace(".", "", $row['Debet']),
      'Kredit' => str_replace(".", "", $row['Kredit']),
      'IDMataUang' => $row['IDMataUang'],
      'Kurs' => $row['Kurs'],
      'Keterangan' => $row['Keterangan'],
    );
    $this->M->add_detail($data_bawah);

      //save jurnal

    if($row['Debet']!=0){
      $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($value->IDJurnal, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_debet = array(
        'IDJurnal'=>$urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'BB',
        'IDCOA' => $row['IDCOA'],
        'Debet' => str_replace(".", "", $row['Debet']),
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Debet']),
        'Total_kredit' => 0,
        'Keterangan' => $row['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Debet']),
      );
      $this->M->save_jurnal($save_jurnal_debet);
    }else if($row['Kredit'] != 0){

      $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($value->IDJurnal, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_kredit = array(
        'IDJurnal'=>$urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'BB',
        'IDCOA' => $row['IDCOA'],
        'Debet' => 0,
        'Kredit' => str_replace(".", "", $row['Kredit']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => 0,
        'Total_kredit' => str_replace(".", "", $row['Kredit']),
        'Keterangan' => $row['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Kredit']),
      );
      $this->M->save_jurnal($save_jurnal_kredit);
    }
  }
  if($data1['Posisi']=="Debet"){
    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($value->IDJurnal, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal = 'P000001';
    }

    $save_jurnal_kredit = array(
      'IDJurnal'=>$urutan_id_jurnal,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'BB',
      'IDCOA' => $data1['IDCOA'],
      'Debet' => str_replace(".", "", $data1['total_debet']),
      'Kredit' => 0,
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => str_replace(".", "", $data1['total_debet']),
      'Total_kredit' => 0,
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => str_replace(".", "", $data1['total_debet']),
    );
    $this->M->save_jurnal($save_jurnal_kredit);
  }else if($data1['Posisi']=="Kredit"){
    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($value->IDJurnal, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal = 'P000001';
    }

    $save_jurnal_kredit = array(
      'IDJurnal'=>$urutan_id_jurnal,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'BB',
      'IDCOA' => $data1['IDCOA'],
      'Debet' => 0,
      'Kredit' => str_replace(".", "", $data1['total_kredit']),
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => str_replace(".", "", $data1['total_kredit']),
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => str_replace(".", "", $data1['total_kredit']),
    );
    $this->M->save_jurnal($save_jurnal_kredit);
  }
  echo json_encode($data2);

}


  //----------------------Detail per data
public function show($id)
{
  $data['bukubank'] = $this->M->get_by_id($id);
  $data['bukubankdetail'] = $this->M->get_bukubank_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/buku_bank/V_show', $data);
}

public function edit($id)
{
   $this->load->model('M_supplier');
  $data['bukubank'] = $this->M->get_by_id($id);
  $data['databukubankdetail'] = $this->M->get_bukubank_detail_edit($id);
  $data['coa'] = $this->M->get_coa();
  $data['coa_group'] = $this->M->get_coa_group();
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/buku_bank/V_edit_bukubank', $data);
}

  //aksi Ubah jurnalumum
public function ubah_bukubank(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDBB = $this->input->post('_id');

  $data_atas = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCoa' => $data1['IDCOA'],
    'Posisi' => $data1['Posisi'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );

  $this->M->update_master($IDBB,$data_atas);
  if($this->M->update_master($IDBB,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M->drop($row['IDBukuBankDetail']);
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
       $data['id_jurnal_umum_detail'] = $this->M->tampilkan_id_buku_bank_detail();
       if($data['id_jurnal_umum_detail']!=""){
        foreach ($data['id_jurnal_umum_detail'] as $value) {
          $urutan= substr($value->IDBukuBankDetail, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_detail = 'P000001';
      }
      $data_atas = array(
        'IDBukuBankDetail'=> $urutan_id_detail,
        'IDBukuBank'=>$IDBB,
        'IDCoa' => $row['IDCOA'],
        'Posisi' => $row['Posisi'],
        'Debet' => str_replace(".", "", $row['Debet']),
        'Kredit' => str_replace(".", "", $row['Kredit']),
        'IDMataUang' => $row['IDMataUang'],
        'Kurs' => $row['Kurs'],
        'Keterangan' => $row['Keterangan'],
      );
      if($row['IDBukuBankDetail'] == ''){
        $this->M->add_detail($data_atas);

        //save jurnal

        if($data1['Posisi']=="Debet"){
          $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
          if($data['id_jurnal']!=""){
            foreach ($data['id_jurnal'] as $value) {
              $urutan= substr($value->IDJurnal, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
          }else{
            $urutan_id_jurnal = 'P000001';
          }

          $save_jurnal_debet = array(
            'IDJurnal'=>$urutan_id_jurnal,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['Nomor'],
            'IDFaktur' => $IDBB,
            'IDFakturDetail' => $urutan_id_detail,
            'Jenis_faktur' => 'BB',
            'IDCOA' => $row['IDCOA'],
            'Debet' => 0,
            'Kredit' => str_replace(".", "", $row['Debet']),
            'IDMataUang' => 1,
            'Kurs' => 14000,
            'Total_debet' => 0,
            'Total_kredit' => str_replace(".", "", $row['Debet']),
            'Keterangan' => $row['Keterangan'],
            'Saldo' => str_replace(".", "", $row['Debet']),
          );
          $this->M->save_jurnal($save_jurnal_debet);
        }else if($data1['Posisi']=="Kredit"){

          $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
          if($data['id_jurnal']!=""){
            foreach ($data['id_jurnal'] as $value) {
              $urutan= substr($value->IDJurnal, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
          }else{
            $urutan_id_jurnal = 'P000001';
          }

          $save_jurnal_kredit = array(
            'IDJurnal'=>$urutan_id_jurnal,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['Nomor'],
            'IDFaktur' => $IDBB,
            'IDFakturDetail' => $urutan_id_detail,
            'Jenis_faktur' => 'BB',
            'IDCOA' => $row['IDCOA'],
            'Debet' => str_replace(".", "", $row['Kredit']),
            'Kredit' => 0,
            'IDMataUang' => 1,
            'Kurs' => 14000,
            'Total_debet' => 0,
            'Total_kredit' => str_replace(".", "", $row['Kredit']),
            'Keterangan' => $row['Keterangan'],
            'Saldo' => str_replace(".", "", $row['Kredit']),
          );
          $this->M->save_jurnal($save_jurnal_kredit);
        }
      }
    }

    if($data1['Posisi']=="Debet"){
      $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($value->IDJurnal, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_kredit = array(
        'IDJurnal'=>$urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $IDBB,
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'BB',
        'IDCOA' => $data1['IDCOA'],
        'Debet' => str_replace(".", "", $data1['total_debet']),
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $data1['total_debet']),
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $data1['total_debet']),
      );
      $this->M->save_jurnal($save_jurnal_kredit);
    }else if($data1['Posisi']=="Kredit"){
      $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($value->IDJurnal, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_kredit = array(
        'IDJurnal'=>$urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $IDBB,
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'BB',
        'IDCOA' => $data1['IDCOA'],
        'Debet' => 0,
        'Kredit' => str_replace(".", "", $data1['total_kredit']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => 0,
        'Total_kredit' => str_replace(".", "", $data1['total_kredit']),
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $data1['total_kredit']),
      );
      $this->M->save_jurnal($save_jurnal_kredit);
    }
  }
  echo true;
}else{
  echo false;  
      // echo json_encode($data_atas);
}

      //$this->M->update_master($IDJU,$data_atas);
      //echo json_encode($data3);

}

  //--------------------------------pencarian
function pencarian()
{
  $data = array(
      'title_master' => "Master Buku Bank", //Judul 
      'title' => "List Data Buku Bank", //Judul Tabel
      'action' => site_url('BukuBank/create'), //Alamat Untuk Action Form
      'button' => "Tambah Data", //Nama Button
      //'jurnalumum' => $this->M->get_all() //Load Data Jurnal Umum
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['bukubank'] = $this->M->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/buku_bank/V_bukubank', $data);
}

function print($nomor)
{

  $data['print'] = $this->M->print_jurnal_umum($nomor);
  $id= $data['print']->IDJU;
  $data['printdetail'] = $this->M->print_jurnal_umum_detail($id);
  $this->load->view('administrator/jurnalumum/V_print', $data);
}

}

/* End of file jurnalumum.php */
