<?php

class Laporan_giro extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_laporan_giro');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_giro/V_laporan_giro', $data);
    }

    function pencarian()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        if ($this->input->post('detail_keyword') == '') {
            $data['laporangiro'] = $this->M_laporan_giro->searching_store_like($this->input->post('tanggal_mulai'), $this->input->post('tanggal_selesai'), $this->input->post('jenis_giro'), $this->input->post('status_giro'));
        } else {
            $data['laporangiro'] = $this->M_laporan_giro->searching_store($this->input->post('tanggal_mulai'), $this->input->post('tanggal_selesai'), $this->input->post('jenis_giro'), $this->input->post('status_giro'), $this->input->post('keyword'), $this->input->post('detail_keyword'));
        }
        $this->load->view('administrator/laporan_giro/V_laporan_giro', $data);
    }
}