<?php

class Pembelian_asset extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_pembelian_asset');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
  }

  public function index()
  {
    $data['p_asset'] = $this->M_pembelian_asset->tampilkan_pembelian_asset();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/pembelian_asset/V_pembelian_asset.php', $data);
  }


  public function tambah_pembelian_asset()
  {

    $data['group_asset'] = $this->M_pembelian_asset->group_asset();
    $data['asset'] = $this->M_pembelian_asset->tampil_asset();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        // $data['kodenoasset'] = $this->M_pembelian_asset->no_pembelian_asset();
    $data['kodenoasset'] = $this->M_pembelian_asset->no_asset(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $this->load->view('administrator/pembelian_asset/V_tambah_pembelian_asset', $data);
  }

  public function simpan_pembelian_asset(){

    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');
    $data['id_pembelian_asset'] = $this->M_pembelian_asset->tampilkan_id_pembelian_asset();
    if($data['id_pembelian_asset']!=""){
      // foreach ($data['id_pembelian_asset'] as $value) {
        $urutan= substr($data['id_pembelian_asset']->IDFBA, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data = array(
      'IDFBA' => $urutan_id,
      'Tanggal' => $this->input->post('Tanggal'),
      'Nomor' => $this->input->post('Nomor'),
      'IDMataUang' => '6',
      'Kurs' => '-',
      'Total_nilai_perolehan' => str_replace(".", "",$this->input->post('Nilai_perolehan')),
      'Total_akumulasi_penyusutan' =>  str_replace(".", "",$this->input->post('Akumulasi_penyusutan')),
      'Total_nilai_buku' => str_replace(".", "", $this->input->post('Nilai_buku')),
      'Batal' => 'aktif',
    );
    $this->M_pembelian_asset->add($data);

    $data = null;
    $data['id_pembelian_asset_detail'] = $this->M_pembelian_asset->tampilkan_id_pembelian_asset_detail();
    if($data['id_pembelian_asset_detail']!=""){
      // foreach ($data['id_pembelian_asset_detail'] as $value) {
        $urutan= substr($data['id_pembelian_asset_detail']->IDFBADetail, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }
    $datas = array(
     'IDFBADetail' => $urutan_id_detail,
     'IDFBA' => $urutan_id,
     'IDAsset' => $this->input->post('IDAsset'),
     'IDGroupAsset' => $this->M_pembelian_asset->get_IDgroupasset($this->input->post('IDGroupAsset'))->IDGroupAsset ? $this->M_pembelian_asset->get_IDgroupasset($this->input->post('IDGroupAsset'))->IDGroupAsset : 0,
     'Nilai_perolehan' => str_replace(".", "",$this->input->post('Nilai_perolehan')),
     'Akumulasi_penyusutan' =>  str_replace(".", "",$this->input->post('Akumulasi_penyusutan')),
     'Nilai_buku' => str_replace(".", "",$this->input->post('Nilai_buku')),
     'Metode_penyusutan' => $this->input->post('Metode_penyusutan'),
     'Tanggal_penyusutan' => $this->input->post('Tanggal_penyusutan'),
     'Umur' => $this->input->post('Umur')
   );
    $this->M_pembelian_asset->add_detail($datas);

    redirect("Pembelian_asset/tambah_pembelian_asset");


  }

  public function cari_asset()
  {
    $id_group_asset = $this->input->post('id_group_asset');
    $data = $this->M_pembelian_asset->getasset($id_group_asset);
    echo json_encode($data);
  }

  public function edit($id)
  {
    $data['detail'] = $this->M_pembelian_asset->detail_pembelian_asset($id);
    $data['group_asset'] = $this->M_pembelian_asset->group_asset();
    $data['asset'] = $this->M_pembelian_asset->tampil_asset();
    $data['p_asset'] = $this->M_pembelian_asset->getById($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/pembelian_asset/V_edit', $data);
  }

  public function ubah_pembelian_asset()
  {
    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');
    $data3 = $this->input->post('data3');

    $data = array(
      'Tanggal' => $this->input->post('Tanggal'),
      'Nomor' => $this->input->post('Nomor'),
      'IDMataUang' => '6',
      'Kurs' => '-',
      'Total_nilai_perolehan' => str_replace(".", "",$this->input->post('Nilai_perolehan')),
      'Total_akumulasi_penyusutan' =>  str_replace(".", "",$this->input->post('Akumulasi_penyusutan')),
      'Total_nilai_buku' => str_replace(".", "", $this->input->post('Nilai_buku')),
      'Batal' => 'aktif',
    );

  // if($this->M_pembelian_asset->update_master($data1['IDFBA'],$data)){
  //   $data = null;
  //   if($data3 > 0){
  //    foreach($data3 as $row){
  //      $this->M_pembelian_asset->drop($row['IDFBADetail']);
  //    }
  //  }
  //  if($data2 != null){
  //   foreach($data2 as $row){
      // $data['id_pembelian_asset_detail'] = $this->M_pembelian_asset->tampilkan_id_pembelian_asset_detail();
      // if($data['id_pembelian_asset_detail']!=""){
      //   foreach ($data['id_pembelian_asset_detail'] as $value) {
      //     $urutan= substr($value->IDFBADetail, 1);
      //   }
      //   $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      //   $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      // }else{
      //   $urutan_id_detail = 'P000001';
      // }
    $data = array(
      'IDFBA'=>$this->input->post('IDFBA'),
        // 'IDAsset' => 1,
        // 'IDGroupAsset' => 1,
      'Nilai_perolehan' => str_replace(".", "",$this->input->post('Nilai_perolehan')),
      'Akumulasi_penyusutan' =>  str_replace(".", "",$this->input->post('Akumulasi_penyusutan')),
      'Nilai_buku' => str_replace(".", "",$this->input->post('Nilai_buku')),
      'Metode_penyusutan' => $this->input->post('Metode_penyusutan'),
      'Tanggal_penyusutan' => $this->input->post('Tanggal_penyusutan'),
      'Umur' => $this->input->post('Umur')
    );
    $this->M_pembelian_asset->update_detail($this->input->post('IDFBADetail'), $data);

      // if($row['IDFBADetail'] ==''){
      //   $this->M_pembelian_asset->add_detail($data);

      // }
      // else
      // {
      //   $this->M_pembelian_asset->update_detail($row['IDFBADetail'], $data);                       
      // }
    redirect("Pembelian_asset");

  }

  public function status_gagal($id)
  {
    $data = array(
      'Batal' => 'tidak aktif',
    );

    $this->M_pembelian_asset->update_status($data, array('IDFBA' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('Pembelian_asset/index');
  }

  public function status_berhasil($id)
  {
    $data = array(
      'Batal' => 'aktif',
    );

    $this->M_pembelian_asset->update_status($data, array('IDFBA' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('Pembelian_asset/index');
  }

  public function delete_multiple()
  {
    $ID_att = $this->input->post('msg');
    if($ID_att != ""){
     $result = array();
     foreach($ID_att AS $key => $val){
       $result[] = array(
        "IDFBA" => $ID_att[$key],
        "Batal"  => 'tidak aktif'
      );
     }
     $this->db->update_batch('tbl_pembelian_asset', $result, 'IDFBA');
     redirect("Pembelian_asset/index");
   }else{
     redirect("Pembelian_asset/index");
   }

 }

 function pencarian()
 {
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['p_asset'] = $this->M_pembelian_asset->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/Pembelian_asset/V_pembelian_asset', $data);
}

function show($id)
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['data'] = $this->M_pembelian_asset->find($id);
  $data['datadetail'] = $this->M_pembelian_asset->find_detail($id);
  $this->load->view('administrator/pembelian_asset/V_show', $data);
}

function ubah_pembelian_tabel_asset()
{
 $data = array(
  'IDAsset' => $this->input->post('IDAsset'),
  'IDGroupAsset' => $this->input->post('IDGroupAsset'),
  'Nilai_perolehan' => $this->input->post('Nilai_perolehan')
);

 $this->M_pembelian_asset->update($data, array('IDFBADetail' => $this->input->post('IDFBADetail')));
}

public function get_asset()
{
  $data = $this->M_pembelian_asset->get_asset();
  echo json_encode($data);
}

public function get_group_asset()
{
  $data = $this->M_pembelian_asset->get_group_asset();
  echo json_encode($data);
}

function print($nomor)
{

  $data['print'] = $this->M_pembelian_asset->print_pembelian_asset($nomor);
  $id= $data['print']->IDFBA;
  $data['printdetail'] = $this->M_pembelian_asset->print_pembelian_asset_detail($id);
  $this->load->view('administrator/Pembelian_asset/V_print', $data);
}

public function penyusutan_asset_update()
{
  // $tahun_awal = date_format(date_create("2019-01-01"), "Y");
  // $tahun_sekarang = date("Y");
  // $hitungselisih = $tahun_sekarang - $tahun_awal;
  $tgl= date("Y-m-d");
  $month= date_format(date_create($tgl), "M");
  $sisabulan= 12-$month;


 $getdata= $this->M_pembelian_asset->ambil_pembelian_asset();
  foreach ($getdata as $datas2) {
  $data = array(
    'Total_nilai_buku' => $datas2->Total_nilai_buku
  );

  $data = array(
    'Akumulasi_penyusutan' =>  $datas2->Akumulasi_penyusutan,
    'Nilai_buku' => ($datas2->Nilai_buku-($datas2->Akumulasi_penyusutan*$sisabulan))*$datas2->Tarif_penyusutan/12
  );
  $this->M_pembelian_asset->update_detail($datas2->IDFBADetail, $data);
}
}

public function penyusutan_asset()
{
 $hari_ini = date("Y-m-d");
 $tgl_terakhir = date('Y-m-t', strtotime($hari_ini));

 // if($hari_ini == $tgl_terakhir){
 $getdata= $this->M_pembelian_asset->ambil_pembelian_asset();

 foreach ($getdata as $datas2) {
   $data['id_jurnal'] = $this->M_pembelian_asset->tampilkan_id_jurnal();
   if($data['id_jurnal']!=""){
    foreach ($data['id_jurnal'] as $value) {
      $urutan= substr($value->IDJurnal, 1);
    }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_jurnal_debet= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id_jurnal_debet = 'P000001';
  }

  $debet = array(
   'IDJurnal'=>$urutan_id_jurnal_debet,
   'Tanggal' =>  $datas2->Tanggal,
   'Nomor' =>  $datas2->Nomor,
   'IDFaktur' => $datas2->IDFBA,
   'IDFakturDetail' => $datas2->IDFBADetail,
   'Jenis_faktur' => 'ASSET',
   'IDCOA' => $datas2->Coa_Beban_Asset,
   'Debet' => $datas2->Akumulasi_penyusutan,
   'Kredit' => 0,
   'IDMataUang' => 1,
   'Kurs' => 14000,
   'Total_debet' => $datas2->Akumulasi_penyusutan,
   'Total_kredit' => 0,
   'Keterangan' => '-',
   'Saldo' => $datas2->Akumulasi_penyusutan,
 );
  $this->M_pembelian_asset->add_penyusutan_asset_debet($debet);

  $data['id_jurnal'] = $this->M_pembelian_asset->tampilkan_id_jurnal();
  if($data['id_jurnal']!=""){
    foreach ($data['id_jurnal'] as $value) {
      $urutan= substr($value->IDJurnal, 1);
    }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_jurnal_kredit= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id_jurnal_kredit = 'P000001';
  }

  $kredit = array(
   'IDJurnal'=>$urutan_id_jurnal_kredit,
   'Tanggal' =>  $datas2->Tanggal,
   'Nomor' =>  $datas2->Nomor,
   'IDFaktur' => $datas2->IDFBA,
   'IDFakturDetail' => $datas2->IDFBADetail,
   'Jenis_faktur' => 'ASSET',
   'IDCOA' => $datas2->Coa_Akumulasi_Asset,
   'Debet' => 0,
   'Kredit' => $datas2->Akumulasi_penyusutan,
   'IDMataUang' => 1,
   'Kurs' => 14000,
   'Total_debet' => 0,
   'Total_kredit' => $datas2->Akumulasi_penyusutan,
   'Keterangan' => '-',
   'Saldo' => $datas2->Akumulasi_penyusutan,
 );
  $this->M_pembelian_asset->add_penyusutan_asset_kredit($kredit);
}
// }
}

}