
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class GroupAsset extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_groupasset');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index()
	{
		$data['groupasset']=$this->M_groupasset->tampilkan_groupasset();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/groupasset/V_groupasset', $data);
	}

	public function tambah_groupasset()
	{
		
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/groupasset/V_tambah_groupasset', $data);
	}
	public function simpan()
	{
		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$simbar= $this->input->post('simbar');
		$tarif= str_replace(".", "", $this->input->post('tarif'));
		$umur= $this->input->post('umur');
		$data['id_group_asset']=$this->M_groupasset->tampilkan_id_groupasset();
		if($data['id_group_asset']!=""){
			// foreach ($data['id_group_asset'] as $value) {
				$urutan= substr($data['id_group_asset']->IDGroupAsset, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$data = array(
			'IDGroupAsset' => $urutan_id,
			'Kode_Group_Asset' => $this->input->post('Kode_Asset'),
			'Group_Asset' => $this->input->post('Group_Asset'),
			'Tarif_Penyusutan' => $tarif/100,
			'Umur' => $umur,
			'Aktif' => 'aktif'
		);

		$this->M_groupasset->simpan($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('GroupAsset/index');
		}elseif($simtam){
			redirect('GroupAsset/tambah_groupasset');
		}elseif($simbar){
			redirect('Assets/tambah_asset');
		}
	}
	public function edit($id)
	{
		$data['groupasset'] = $this->M_groupasset->get_groupasset_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/groupasset/V_edit_groupasset', $data);
	}
	public function update()
	{
		$tarif= str_replace(".", "", $this->input->post('tarif'));
		$umur= $this->input->post('umur');
		$data = array(
			'Kode_Group_Asset' => $this->input->post('Kode_Asset'),
			'Group_Asset' => $this->input->post('Group_Asset'),
			'Tarif_Penyusutan' => $tarif/100,
			'Umur' => $umur
		);
		echo $this->input->post('id');
		$this->M_groupasset->update_groupasset($this->input->post('id'),$data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Ubah</div></center>");
		redirect('GroupAsset/index');
	}
	public function hapus_groupasset($id)
	{
		$data['groupassetget']= $this->M_groupasset->get_groupasset($id);
		if(count($data['groupassetget']) > 0){
			if(($data['groupassetget']->IDGroupAsset!=$id)){   
				$this->M_groupasset->hapus_data_groupasset($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
				redirect('GroupAsset/index');
			}else{
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
				redirect('GroupAsset/index');
			}
		}else{
			$this->M_groupasset->hapus_data_groupasset($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
				redirect('GroupAsset/index');
		}
	}
	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['groupasset']=$this->M_groupasset->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['groupasset']=$this->M_groupasset->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['groupasset']=$this->M_groupasset->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['groupasset']=$this->M_groupasset->cari_by_keyword($keyword);
		}else {
			$data['groupasset']=$this->M_groupasset->tampilkan_groupasset();
		}
		$this->load->view('administrator/groupasset/V_groupasset', $data);
	}
}

?>