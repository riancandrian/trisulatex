<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class BukuKas extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_bukukas', 'M');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
  }

  public function index()
  {
    $data = array(
      'title_master' => "Master Buku Kas", //Judul 
      'title' => "List Data Buku Kas", //Judul Tabel
      'action' => site_url('BukuKas/create'), //Alamat Untuk Action Form
      'button' => "Tambah Data", //Nama Button
      'bukukas' => $this->M->get_all() //Load Data Jurnal Umum
    );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/buku_kas/V_bukukas', $data);
  }

  public function laporan_buku_kas()
  {
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['coa_kas'] = $this->M->get_coa_kas();
    $this->load->view('administrator/buku_kas/V_laporan_buku_kas', $data);
  }

  public function pencarian_buku_kas()
  {
    $date_from= $this->input->post('date_from');
    $date_until= $this->input->post('date_until');
    $search_type= $this->input->post('jenispencarian');
    $keyword= $this->input->post('keyword');
    $data['coa_kas'] = $this->M->get_coa_kas();
    $data['laporanbukukas'] = $this->M->searching_laporan_buku_kas($date_from, $date_until, $search_type, $keyword);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/buku_kas/V_laporan_buku_kas', $data);
  }


  //----------------------Aksi Ubah Batal single
  public function set_edit($id)
  {
    $cek = $this->M->get_by_id($id);
    //die(print_r($cek));
    $batal = "-";
    if ($cek->Batal == "aktif") {
      $batal ="tidak aktif";
    } else {
      $batal = "aktif";
    }
    $data = array(
      'Batal' => $batal
    );

    $this->M->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('BukuKas/index');
  }

  //----------------------Aksi Ubah Batal multiple
  public function delete_multiple()
  {
    $ID_att = $this->input->post('msg');
    if (empty($ID_att)) {
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
      redirect("BukuKas/index");
    }
    $result = array();
    foreach($ID_att AS $key => $val){
      $result[] = array(
        "IDJU" => $ID_att[$key],
        "Batal"  => 'tidak aktif'
      );
    }
    $this->db->update_batch('tbl_jurnal_umum', $result, 'IDJU');
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect("BukuKas/index");
  }

  //Tambah Data Jurnal Umum
  public function create()
  {
    $this->load->model('M_supplier');

    $data['kodebukukas'] = $this->M->no_bukukas(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $data['coa_kas'] = $this->M->get_coa_kas();
    $data['coa'] = $this->M->get_coa();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/buku_kas/V_tambah_bukukas', $data);
  }

  //Ajax get data stok
  public function getstok()
  {
    $barcode = $this->input->post('barcode');
    $data = $this->M->getstok($barcode);
    echo json_encode($data);
    
  }


 //SImpan Data Ajax
  public function simpan_bukukas(){

    $data1 = $this->input->post('_data1');
    $data2 = $this->input->post('_data2');
    $data['id_buku_kas'] = $this->M->tampilkan_id_buku_kas();
    if($data['id_buku_kas']!=""){
      foreach ($data['id_buku_kas'] as $value) {
        $urutan= substr($value->IDBukuKas, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data_atas = array(
      'IDBukuKas' => $urutan_id,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDCoa' => $data1['IDCOA'],
      'Posisi' => $data1['Posisi'],
      'Batal' => $data1['Batal'],
    // 'Total_uang' => $data1['Total_uang']
    );
    $last_insert_id = $this->M->add($data_atas); 
    $data_bawah = null;
    foreach($data2 as $row){
     $data['id_buku_kas_detail'] = $this->M->tampilkan_id_buku_kas_detail();
     if($data['id_buku_kas_detail']!=""){
      foreach ($data['id_buku_kas_detail'] as $value) {
        $urutan= substr($value->IDBukuKasDetail, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }
    $data_bawah = array(
      'IDBukuKasDetail'=> $urutan_id_detail,
      'IDBukuKas'=>$urutan_id,
      'IDCoa' => $row['IDCOA'],
      'Debet' => str_replace(".", "", $row['Debet']),
      'Kredit' => str_replace(".", "", $row['Kredit']),
      'IDMataUang' => $row['IDMataUang'],
      'Kurs' => $row['Kurs'],
      'Keterangan' => $row['Keterangan'],
    );
    $this->M->add_detail($data_bawah);

    //save jurnal
    if($row['Debet'] != 0){
      $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($value->IDJurnal, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_debet = array(
        'IDJurnal'=>$urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'BK',
        'IDCOA' => $row['IDCOA'],
        'Debet' => str_replace(".", "", $row['Debet']),
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Debet']),
        'Total_kredit' =>0,
        'Keterangan' => $row['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Debet']),
      );
      $this->M->save_jurnal($save_jurnal_debet);
    }elseif($row['Kredit'] != 0){
      $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($value->IDJurnal, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_debet = array(
        'IDJurnal'=>$urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'BK',
        'IDCOA' => $row['IDCOA'],
        'Debet' => 0,
        'Kredit' => str_replace(".", "", $row['Kredit']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' =>  0,
        'Total_kredit' => str_replace(".", "", $row['Kredit']),
        'Keterangan' => $row['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Kredit']),
      );
      $this->M->save_jurnal($save_jurnal_debet);
    }
  }

  // if($data1['Posisi']=="kas masuk"){
  //   $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
  //   if($data['id_jurnal']!=""){
  //     foreach ($data['id_jurnal'] as $value) {
  //       $urutan= substr($value->IDJurnal, 1);
  //     }
  //     $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
  //     $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  //   }else{
  //     $urutan_id_jurnal = 'P000001';
  //   }

  //   $save_jurnal_debet = array(
  //     'IDJurnal'=>$urutan_id_jurnal,
  //     'Tanggal' => $data1['Tanggal'],
  //     'Nomor' => $data1['Nomor'],
  //     'IDFaktur' => $urutan_id,
  //     'IDFakturDetail' => $urutan_id_detail,
  //     'Jenis_faktur' => 'BK',
  //     'IDCOA' => $data1['Nama_COA'],
  //     'Debet' => str_replace(".", "", $data1['Total_uang']),
  //     'Kredit' => 0,
  //     'IDMataUang' => 1,
  //     'Kurs' => 14000,
  //     'Total_debet' => str_replace(".", "", $data1['Total_uang']),
  //     'Total_kredit' => 0,
  //     'Keterangan' => '-',
  //     'Saldo' => str_replace(".", "", $data1['Total_uang']),
  //   );
  //   $this->M->save_jurnal($save_jurnal_debet);
  // }elseif($data1['Posisi']=="kas keluar"){
  //   $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
  //   if($data['id_jurnal']!=""){
  //     foreach ($data['id_jurnal'] as $value) {
  //       $urutan= substr($value->IDJurnal, 1);
  //     }
  //     $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
  //     $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  //   }else{
  //     $urutan_id_jurnal = 'P000001';
  //   }

  //   $save_jurnal_debet = array(
  //     'IDJurnal'=>$urutan_id_jurnal,
  //     'Tanggal' => $data1['Tanggal'],
  //     'Nomor' => $data1['Nomor'],
  //     'IDFaktur' => $urutan_id,
  //     'IDFakturDetail' => $urutan_id_detail,
  //     'Jenis_faktur' => 'BK',
  //     'IDCOA' => $data1['Nama_COA'],
  //     'Debet' => 0,
  //     'Kredit' => str_replace(".", "", $data1['Total_uang']),
  //     'IDMataUang' => 1,
  //     'Kurs' => 14000,
  //     'Total_debet' => 0,
  //     'Total_kredit' => str_replace(".", "", $data1['Total_uang']),
  //     'Keterangan' => '-',
  //     'Saldo' => str_replace(".", "", $data1['Total_uang']),
  //   );
  //   $this->M->save_jurnal($save_jurnal_debet);
  // }

  echo json_encode($data2);

}


  //----------------------Detail per data
public function show($id)
{
  $data['bukukas'] = $this->M->get_by_id($id);
  $data['bukukasdetail'] = $this->M->get_bukukas_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/buku_kas/V_show', $data);
}

  //Ubah data bukukas
public function edit($id)
{
  $this->load->model('M_supplier');

  $data['bukukas'] = $this->M->bukukas($id);
  $data['databukukasdetail'] = $this->M->get_bukukas_detail($id);
  $data['coa_kas'] = $this->M->get_coa_kas();
  $data['coa'] = $this->M->get_coa();
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/buku_kas/V_edit_bukukas', $data);
}

  //aksi Ubah jurnalumum
public function ubah_bukukas(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDBukuKas = $this->input->post('_id');

  $data_atas = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCoa' => $data1['IDCOA'],
    'Posisi' => $data1['Posisi'],
    'Batal' => $data1['Batal'],
  );


  if($this->M->update_master($IDBukuKas,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M->drop($row['IDBukuKasDetail']);
        $this->M->drop_jurnal($data1['Nomor'], $IDBukuKas, $row['IDBukuKasDetail']);
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
       $data['id_buku_kas_detail'] = $this->M->tampilkan_id_buku_kas_detail();
       if($data['id_buku_kas_detail']!=""){
        foreach ($data['id_buku_kas_detail'] as $value) {
          $urutan= substr($value->IDBukuKasDetail, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_detail = 'P000001';
      }
      $data_atas = array(
        'IDBukuKasDetail'=> $urutan_id_detail,
        'IDBukuKas'=>$IDBukuKas,
        'IDCoa' => $row['IDCOA'],
        'Debet' => str_replace(".", "", $row['Debet']),
        'Kredit' => str_replace(".", "", $row['Kredit']),
        'IDMataUang' => $row['IDMataUang'],
        'Kurs' => $row['Kurs'],
        'Keterangan' => $row['Keterangan'],
      );
      if($row['IDBukuKasDetail'] == ''){
        $this->M->add_detail($data_atas);
        //save jurnal

        if($row['Debet']!=0){
          $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
          if($data['id_jurnal']!=""){
            foreach ($data['id_jurnal'] as $value) {
              $urutan= substr($value->IDJurnal, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
          }else{
            $urutan_id_jurnal = 'P000001';
          }

          $save_jurnal_debet = array(
            'IDJurnal'=>$urutan_id_jurnal,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['Nomor'],
            'IDFaktur' => $IDBukuKas,
            'IDFakturDetail' => $urutan_id_detail,
            'Jenis_faktur' => 'BK',
            'IDCOA' => $row['IDCOA'],
            'Debet' => str_replace(".", "", $row['Debet']),
            'Kredit' => 0,
            'IDMataUang' => 1,
            'Kurs' => 14000,
            'Total_debet' => str_replace(".", "", $row['Debet']),
            'Total_kredit' => 0,
            'Keterangan' => $row['Keterangan'],
            'Saldo' =>str_replace(".", "", $row['Debet']),
          );
          $this->M->save_jurnal($save_jurnal_debet);
        }else if($row['Kredit'] != 0){

          $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
          if($data['id_jurnal']!=""){
            foreach ($data['id_jurnal'] as $value) {
              $urutan= substr($value->IDJurnal, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
          }else{
            $urutan_id_jurnal = 'P000001';
          }

          $save_jurnal_kredit = array(
            'IDJurnal'=>$urutan_id_jurnal,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['Nomor'],
            'IDFaktur' => $IDBukuKas,
            'IDFakturDetail' => $urutan_id_detail,
            'Jenis_faktur' => 'BK',
            'IDCOA' => $row['IDCOA'],
            'Debet' => 0,
            'Kredit' => str_replace(".", "", $row['Kredit']),
            'IDMataUang' => 1,
            'Kurs' => 14000,
            'Total_debet' => 0,
            'Total_kredit' => str_replace(".", "", $row['Kredit']),
            'Keterangan' => $row['Keterangan'],
            'Saldo' => str_replace(".", "", $row['Kredit']),
          );
          $this->M->save_jurnal($save_jurnal_kredit);
        }
      }
    }
  }
  echo true;
}else{
  echo false;  
}

}

  //--------------------------------pencarian
function pencarian()
{
  $data = array(
      'title_master' => "Master Buku Kas", //Judul 
      'title' => "List Data Buku Kas", //Judul Tabel
      'action' => site_url('BukuKas/create'), //Alamat Untuk Action Form
      'button' => "Tambah Data", //Nama Button
      //'jurnalumum' => $this->M->get_all() //Load Data Jurnal Umum
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['bukukas'] = $this->M->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/buku_kas/V_bukukas', $data);
}

function print($nomor)
{

  $data['print'] = $this->M->print_jurnal_umum($nomor);
  $id= $data['print']->IDJU;
  $data['printdetail'] = $this->M->print_jurnal_umum_detail($id);
  $this->load->view('administrator/BukuKas/V_print', $data);
}

}

/* End of file jurnalumum.php */
