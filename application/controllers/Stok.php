
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Stok extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('M_stok');
        $this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
          $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$data['stok']=$this->M_stok->tampilkan_stok();
		$this->load->view('administrator/stok/V_stok.php', $data);
	}

	 public function pencarian()
    {
        $kolom= addslashes($this->input->post('jenispencarian'));
        $keyword= addslashes($this->input->post('keyword'));
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        if ($kolom!="" && $keyword!="") {
            $data['stok']=$this->M_stok->cari_by_kolom($kolom,$keyword);
        }elseif ($keyword!="") {
            $data['stok']=$this->M_stok->cari_by_keyword($keyword);
        }else {
            redirect('Stok/index');
        }
        $this->load->view('administrator/stok/V_stok.php', $data);
    }

    public function pencariangudang()
    {
        $jenisgudang= $this->input->post('jenisgudang');
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        if($jenisgudang=="")
        {
        $data['stok']=$this->M_stok->tampilkan_stok();
        }else{
        $data['stok']=$this->M_stok->pencariangudang($jenisgudang);
        }
       
        $this->load->view('administrator/stok/V_stok.php', $data);
    }


}


?>