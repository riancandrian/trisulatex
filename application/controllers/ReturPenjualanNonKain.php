<?php

class ReturPenjualanNonKain extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_retur_penjualan_non_kain');
        $this->load->model('M_invoice_penjualan');
        $this->load->model('M_agen');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        $data['retur'] = $this->M_retur_penjualan_non_kain->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/retur_penjualan_nonkain/V_index', $data);
    }

    public function tambah_retur_penjualan()
    {
        $data['inv'] = $this->M_retur_penjualan_non_kain->tampil_inv();
        $data['koderetur'] = $this->M_retur_penjualan_non_kain->no_retur(date('Y'));
        $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $data['supplier'] = $this->M_retur_penjualan_non_kain->tampil_supplier();
        $data['grandtotal'] = $this->M_retur_penjualan_non_kain->tampil_invoice_grand_total();
        $this->load->view('administrator/retur_penjualan_nonkain/V_create', $data);
    }

    function check_invoice_penjualan()
    {
        $return['data']  = $this->M_retur_penjualan_non_kain->check_invoice_penjualan($this->input->post('Nomor'));
        if ($return['data']) {
            $return['error'] = false;
        }else{
            $return['error'] = true;
        }

        echo json_encode($return);
    }

    function cek_grand_total()
    {
        $Nomor = $this->input->post('Nomor');
        $data = $this->M_retur_penjualan_non_kain->cek_grand_total($Nomor);
        echo json_encode($data);
    }

    function simpan_retur_penjualan()
    {
        $data1 = $this->input->post('data1');
        $data2 = $this->input->post('data2');
        // $data3 = $this->input->post('data3');
        $data['id_retur'] = $this->M_retur_penjualan_non_kain->tampilkan_id_retur();

        if($data['id_retur']!=""){
            // foreach ($data['id_retur'] as $value) {
                $urutan= substr($data['id_retur']->IDRPS, 1);
            // }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
            $urutan_id = 'P000001';
        }

        $data = array(
            'IDRPS' => $urutan_id,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['RJ'],
            'IDFJS' => $this->M_retur_penjualan_non_kain->get_TPKbyNomor($data1['Nomor'])->IDFJS ? $this->M_retur_penjualan_non_kain->get_TPKbyNomor($data1['Nomor'])->IDFJS : 0,
            'IDCustomer' => $data1['IDSupplier'],
            'Tanggal_fj' => $data1['TanggalInv'],
            'Total_qty' => $data1['totalqty'],
            'Saldo_qty' => $data1['totalqty'],
            'Discount' => $data1['Discount_total'],
            'Status_ppn' => $this->M_retur_penjualan_non_kain->get_TPKbyNomor($data1['Nomor'])->Status_ppn ? $this->M_retur_penjualan_non_kain->get_TPKbyNomor($data1['Nomor'])->Status_ppn : 0,
            'Keterangan' => $data1['Keterangan'],
            'Batal' => 'aktif',
        );

        if($this->M_retur_penjualan_non_kain->add($data)){
            $data = null;
            foreach($data2 as $row){
                $data['id_retur_detail'] = $this->M_retur_penjualan_non_kain->tampilkan_id_retur_detail();
                if($data['id_retur_detail']!=""){
                    // foreach ($data['id_retur_detail'] as $value) {
                        $urutan= substr($data['id_retur_detail']->IDRPSDetail, 1);
                    // }
                    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
                }else{
                    $urutan_id_detail = 'P000001';
                }
                $data = array(
                    'IDRPSDetail' => $urutan_id_detail,
                    'IDRPS' => $urutan_id,
                    'IDBarang' => $row['IDBarang'],
                    'IDCorak' => $row['IDCorak'],
                    'IDMerk' => $row['IDMerk'],
                    //'IDWarna' => $row['IDWarna'],
                    'Qty' => $row['Qty_yard'],
                    'Saldo_qty' => $row['Qty_yard'],
                    'IDSatuan' => $row['IDSatuan'],
                    'Harga' => str_replace(".", "",$row['Harga']),
                    'Sub_total' => str_replace(".", "",$row['Sub_total']),
                );
                $this->M_retur_penjualan_non_kain->add_detail($data);


                $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
                if ($data['id_jurnal'] != "") {
                    // foreach ($data['id_jurnal'] as $value) {
                      $urutan = substr($data['id_jurnal']->IDJurnal, 1);
                  // }
                  $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                  $urutan_id_jurnal_debet_retur = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
              } else {
                $urutan_id_jurnal_debet_retur = 'P000001';
            }
            $save_jurnal_debet_retur = array(
                'IDJurnal' => $urutan_id_jurnal_debet_retur,
                'Tanggal' => $data1['Tanggal'],
                'Nomor' => $data1['Nomor_invoice'],
                'IDFaktur' => $urutan_id,
                'IDFakturDetail' => $urutan_id_detail_inv,
                'Jenis_faktur' => 'RJN',
                'IDCOA' => 'P000651',
                'Debet' => str_replace(".", "", $row['Sub_total']),
                'Kredit' => 0,
                'IDMataUang' => 1,
                'Kurs' => 14000,
                'Total_debet' => str_replace(".", "", $row['Sub_total']),
                'Total_kredit' => 0,
                'Keterangan' => $data1['Keterangan'],
                'Saldo' => str_replace(".", "", $row['Sub_total']),
            );
            $this->M_retur_penjualan_non_kain->save_jurnal($save_jurnal_debet_retur);


            $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
            if ($data['id_jurnal'] != "") {
                // foreach ($data['id_jurnal'] as $value) {
                  $urutan = substr($data['id_jurnal']->IDJurnal, 1);
              // }
              $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
              $urutan_id_jurnal_debet_ppn = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
          } else {
            $urutan_id_jurnal_debet_ppn = 'P000001';
        }
        $save_jurnal_debet_ppn = array(
            'IDJurnal' => $urutan_id_jurnal_debet_ppn,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['Nomor_invoice'],
            'IDFaktur' => $urutan_id,
            'IDFakturDetail' => $urutan_id_detail_inv,
            'Jenis_faktur' => 'RJN',
            'IDCOA' => 'P000630',
            'Debet' => str_replace(".", "", $row['Sub_total']),
            'Kredit' => 0,
            'IDMataUang' => 1,
            'Kurs' => 14000,
            'Total_debet' => str_replace(".", "", $row['Sub_total']),
            'Total_kredit' => 0,
            'Keterangan' => $data1['Keterangan'],
            'Saldo' => str_replace(".", "", $row['Sub_total']),
        );
        $this->M_retur_penjualan_non_kain->save_jurnal($save_jurnal_debet_ppn);

        $data['cekgroupcustomer'] = $this->M_invoice_penjualan->cek_group_customer($data1['IDSupplier']);
        $data['cekgroupbarang'] = $this->M_invoice_penjualan->cek_group_barang($row['IDBarang']);

        if ($data['cekgroupcustomer']->Group_Customer == "PIHAK BERELASI" && $data['cekgroupbarang']->Group_Barang== "aksesoris") {
            $data['id_jurnal'] = $this->M_retur_penjualan_non_kain->tampilkan_id_jurnal();
            if ($data['id_jurnal'] != "") {
                // foreach ($data['id_jurnal'] as $value) {
                  $urutan = substr($data['id_jurnal']->IDJurnal, 1);
              // }
              $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
              $urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
          } else {
            $urutan_id_jurnal = 'P000001';
        }

        $save_jurnal_kredit_piutang = array(
            'IDJurnal' => $urutan_id_jurnal,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['Nomor_invoice'],
            'IDFaktur' => $urutan_id,
            'IDFakturDetail' => $urutan_id_detail_inv,
            'Jenis_faktur' => 'RJN',
            'IDCOA' =>  'P000011',
            'Debet' => 0,
            'Kredit' => str_replace(".", "", $row['Sub_total']),
            'IDMataUang' => 1,
            'Kurs' => 14000,
            'Total_debet' => 0,
            'Total_kredit' => str_replace(".", "", $row['Sub_total']),
            'Keterangan' => $data1['Keterangan'],
            'Saldo' => str_replace(".", "", $row['Sub_total']),
        );
        $this->M_retur_penjualan_non_kain->save_jurnal($save_jurnal_kredit_piutang);

        $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
        if ($data['id_jurnal'] != "") {
            // foreach ($data['id_jurnal'] as $value) {
              $urutan = substr($data['id_jurnal']->IDJurnal, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_jurnal_debet_pers = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
      } else {
        $urutan_id_jurnal_debet_pers = 'P000001';
    }
    $save_jurnal_debet_pers = array(
        'IDJurnal' => $urutan_id_jurnal_debet_pers,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor_invoice'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail_inv,
        'Jenis_faktur' => 'RJN',
        'IDCOA' => 'P000020',
        'Debet' => str_replace(".", "", $row['Sub_total']),
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Sub_total']),
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Sub_total']),
    );
    $this->M_retur_penjualan_non_kain->save_jurnal($save_jurnal_debet_pers);
} elseif ($data['cekgroupcustomer']->Group_Customer == "PIHAK KETIGA" && $data['cekgroupbarang']->Group_Barang== "seragam") {
   $data['id_jurnal'] = $this->M_retur_penjualan_non_kain->tampilkan_id_jurnal();
   if ($data['id_jurnal'] != "") {
    // foreach ($data['id_jurnal'] as $value) {
      $urutan = substr($data['id_jurnal']->IDJurnal, 1);
  // }
  $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
  $urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
} else {
    $urutan_id_jurnal = 'P000001';
}

$save_jurnal_kredit_piutang = array(
    'IDJurnal' => $urutan_id_jurnal,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor_invoice'],
    'IDFaktur' => $urutan_id,
    'IDFakturDetail' => $urutan_id_detail_inv,
    'Jenis_faktur' => 'RJN',
    'IDCOA' =>  'P000009',
    'Debet' => 0,
    'Kredit' => str_replace(".", "", $row['Sub_total']),
    'IDMataUang' => 1,
    'Kurs' => 14000,
    'Total_debet' => 0,
    'Total_kredit' => str_replace(".", "", $row['Sub_total']),
    'Keterangan' => $data1['Keterangan'],
    'Saldo' => str_replace(".", "", $row['Sub_total']),
);
$this->M_retur_penjualan_non_kain->save_jurnal($save_jurnal_kredit_piutang);

$data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
if ($data['id_jurnal'] != "") {
    // foreach ($data['id_jurnal'] as $value) {
      $urutan = substr($data['id_jurnal']->IDJurnal, 1);
  // }
  $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
  $urutan_id_jurnal_debet_pers = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
} else {
    $urutan_id_jurnal_debet_pers = 'P000001';
}
$save_jurnal_debet_pers = array(
    'IDJurnal' => $urutan_id_jurnal_debet_pers,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor_invoice'],
    'IDFaktur' => $urutan_id,
    'IDFakturDetail' => $urutan_id_detail_inv,
    'Jenis_faktur' => 'RJN',
    'IDCOA' => 'P000019',
    'Debet' => str_replace(".", "", $row['Sub_total']),
    'Kredit' => 0,
    'IDMataUang' => 1,
    'Kurs' => 14000,
    'Total_debet' => str_replace(".", "", $row['Sub_total']),
    'Total_kredit' => 0,
    'Keterangan' => $data1['Keterangan'],
    'Saldo' => str_replace(".", "", $row['Sub_total']),
);
$this->M_retur_penjualan_non_kain->save_jurnal($save_jurnal_debet_pers);
}
$data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
if ($data['id_jurnal'] != "") {
    // foreach ($data['id_jurnal'] as $value) {
      $urutan = substr($data['id_jurnal']->IDJurnal, 1);
  // }
  $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
  $urutan_id_jurnal_kredit_hpp = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
} else {
    $urutan_id_jurnal_kredit_hpp = 'P000001';
}
$save_jurnal_kredit_hpp = array(
    'IDJurnal' => $urutan_id_jurnal_kredit_hpp,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor_invoice'],
    'IDFaktur' => $urutan_id,
    'IDFakturDetail' => $urutan_id_detail_inv,
    'Jenis_faktur' => 'RJN',
    'IDCOA' => 'P000612',
    'Debet' => 0,
    'Kredit' => str_replace(".", "", $row['Sub_total']),
    'IDMataUang' => 1,
    'Kurs' => 14000,
    'Total_debet' => 0,
    'Total_kredit' => str_replace(".", "", $row['Sub_total']),
    'Keterangan' => $data1['Keterangan'],
    'Saldo' => str_replace(".", "", $row['Sub_total']),
);
$this->M_retur_penjualan_non_kain->save_jurnal($save_jurnal_kredit_hpp);

}

$data['id_retur_grand_total'] = $this->M_retur_penjualan_non_kain->tampilkan_id_retur_grand_total();
if($data['id_retur_grand_total']!=""){
    // foreach ($data['id_retur_grand_total'] as $value) {
        $urutan= substr($data['id_jurnal']->IDRPSGrandTotal, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_grand_total= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
}else{
    $urutan_id_grand_total = 'P000001';
}

$data4 = array(
    'IDRPSGrandTotal' => $urutan_id_grand_total,
    'IDRPS' => $urutan_id,
    'IDMataUang' => 1,
    'Kurs' => '14000',
    'Pembayaran' => '-',
    'DPP' => str_replace(".", "",$data1['DPP']),
    'Discount' => $data1['Discount_total'],
    'PPN' => str_replace(".", "",$data1['PPN']),
    'Grand_total' => str_replace(".", "",$data1['total_invoice']),
    'Sisa' => 1
);
$this->M_retur_penjualan_non_kain->add_grand_total($data4);

echo true;
}else{
    echo false;
}
}

function print_($nomor)
{

    $data['print'] = $this->M_retur_penjualan_non_kain->print_retur_penjualan($nomor);
    $id= $data['print']->IDRPS;
    $data['printdetail'] = $this->M_retur_penjualan_non_kain->print_retur_penjualan_detail($id);
    $data['printgrandtotal'] = $this->M_retur_penjualan_non_kain->print_grand_total($id);
    $this->load->view('administrator/retur_penjualan_nonkain/V_print', $data);
}

function show($id)
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['data'] = $this->M_retur_penjualan_non_kain->find($id);
    $data['datadetail'] = $this->M_retur_penjualan_non_kain->find_detail($id);
    $this->load->view('administrator/retur_penjualan_nonkain/V_show', $data);
}

function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['retur'] = $this->M_retur_penjualan_non_kain->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
    $this->load->view('administrator/retur_penjualan_nonkain/V_index', $data);
}

public function status_gagal($id)
{
    $data = array(
        'Batal' => 'tidak aktif',
    );

    $this->M_retur_penjualan_non_kain->update_status($data, array('IDRPS' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('ReturPenjualanNonKain/index');
}

public function status_berhasil($id)
{
    $data = array(
        'Batal' => 'aktif',
    );

    $this->M_retur_penjualan_non_kain->update_status($data, array('IDRPS' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('ReturPenjualanNonKain/index');
}

public function delete_multiple()
{
    $ID_att = $this->input->post('msg');
    if($ID_att != ""){
        $result = array();
        foreach($ID_att AS $key => $val){
            $result[] = array(
                "IDRPS" => $ID_att[$key],
                "Batal"  => 'tidak aktif'
            );
        }
        $this->db->update_batch('tbl_retur_penjualan_seragam', $result, 'IDRPS');
        redirect("ReturPenjualanNonKain/index");
    }else{
        redirect("ReturPenjualanNonKain/index");
    }

}

public function edit($id)
{
    $data['detail'] = $this->M_retur_penjualan_non_kain->find_detail($id);
    $data['supplier'] = $this->M_retur_penjualan_non_kain->tampil_supplier();
    $data['retur'] = $this->M_retur_penjualan_non_kain->find($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/retur_penjualan_nonkain/V_edit', $data);
}

public function ubah_retur_penjualan()
{
    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');
    $data3 = $this->input->post('data3');

    $data = array(
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['RJ'],
        'IDFJS' => $this->M_retur_penjualan_non_kain->get_TPKbyNomor($data1['Nomor'])->IDFJS ? $this->M_retur_penjualan_non_kain->get_TPKbyNomor($data1['Nomor'])->IDFJS : 0,
        'IDCustomer' => $data1['IDSupplier'],
        'Tanggal_fj' => $data1['TanggalInv'],
        'Total_qty' => $data1['totalqty'],
        'Saldo_qty' => $data1['totalqty'],
        'Discount' => $data1['Discount_total'],
        'Status_ppn' => $this->M_retur_penjualan_non_kain->get_TPKbyNomor($data1['Nomor'])->Status_ppn ? $this->M_retur_penjualan_non_kain->get_TPKbyNomor($data1['Nomor'])->Status_ppn : 0,
        'Keterangan' => $data1['Keterangan']
    );

    if($this->M_retur_penjualan_non_kain->update_master($data1['IDRPS'],$data)){
        $data = null;
        if($data3 > 0){
            foreach($data3 as $row){
                $this->M_retur_penjualan_non_kain->drop($row['IDRPSDetail']);
            }
        }
        $this->M_retur_penjualan_non_kain->drop($data1['IDRPS'], $data);

        if($data2 != null){
            foreach($data2 as $row){

                $data['id_retur_detail'] = $this->M_retur_penjualan_non_kain->tampilkan_id_retur_detail();
                if($data['id_retur_detail']!=""){
                    // foreach ($data['id_retur_detail'] as $value) {
                        $urutan= substr($data['id_retur_detail']->IDRPSDetail, 1);
                    // }
                    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
                }else{
                    $urutan_id_detail = 'P000001';
                }
                $data = array(
                    'IDRPSDetail' => $urutan_id_detail,
                    'IDRPS' => $data1['IDRPS'],
                    'IDBarang' => $row['IDBarang'],
                    'IDCorak' => $row['IDCorak'],
                    'IDMerk' => $row['IDMerk'],
                        //'IDWarna' => $row['IDWarna'],
                    'Qty' => $row['Qty'],
                    'Saldo_qty' => $row['Saldo_qty'],
                    'IDSatuan' => $row['IDSatuan'],
                    'Harga' => $row['Harga'],
                    'Sub_total' => $row['Sub_total'],
                );

                    //if($row['IDRPKDetail'] == ''){
                $this->M_retur_penjualan_non_kain->add_detail($data);
                    //}elseif($row['IDRPKDetail'] != ''){
                    //    $this->M_retur_penjualan->drop($row['IDRPKDetail'], $data);
                    //}
            }
        }

        $datas = array(
            'IDRPS' => $data1['IDRPS'],
            'IDMataUang' => 1,
            'Kurs' => '14000',
            'Pembayaran' => $data1['Pembayaran'],
            'DPP' => $data1['DPP'],
            'Discount' => $data1['Discount_total'],
            'PPN' => $data1['PPN'],
            'Grand_total' => $data1['total_invoice'],
            'Sisa' => 2
        );
        $this->M_retur_penjualan_non_kain->update_grand_total($data1['IDRPS'],$datas);

        echo true;

    }else{
        echo false;
    }
}

public function laporan_retur_nonkain()
{
    $data = array(
        'title_master' => "Laporan Retur Penjualan Non Kain", //Judul 
        'title' => "Laporan Retur Penjualan Non Kain", //Judul Tabel
        'retur' => $this->M_retur_penjualan_non_kain->get_all_procedure()
    );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/retur_penjualan_nonkain/V_laporan', $data);
}
}