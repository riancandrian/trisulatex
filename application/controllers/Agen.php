
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Agen extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_agen');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['agen']=$this->M_agen->tampilkan_agen();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/agen/V_agen.php', $data);
	}

	public function tambah_agen()
	{
		
		$data['kota'] = $this->M_agen->tampil_kota();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/agen/V_tambah_agen', $data);
	}

	public function proses_agen()
	{
		$data['id_agen']=$this->M_agen->tampilkan_agen();
		if($data['id_agen']!=""){
			foreach ($data['id_agen'] as $value) {
				$urutan= substr($value->IDAgen, 1);
			}
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$id_agen= $urutan_id;
		$kode= $this->input->post('kode');
		$nama = $this->input->post('nama');
		$initial = $this->input->post('initial');
		$alamat = $this->input->post('alamat');
		$kota = $this->input->post('kota');
		$telp = $this->input->post('telp');
		$status = $this->input->post('status');
		$npwp = $this->input->post('npwp');

		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');



		if($_FILES['image']['name']!=null || $_FILES['image']['name']!="") {
			unset($config);
			$configFile['upload_path'] = 'upload/';
			$configFile['max_size'] = '60000';
			$configFile['allowed_types'] = 'png|jpg|PNG|JPG|jpeg';
			$configFile['overwrite'] = FALSE;
			$configFile['remove_spaces'] = TRUE;
			$file_name = time()."_".rand().".png";
			$configFile['file_name'] = $file_name;

			$this->load->library('upload', $configFile);
			$this->upload->initialize($configFile);
			if(!$this->upload->do_upload('image')) {
				echo $this->upload->display_errors();
			}else{
				$fileDetails = $this->upload->data();
				$data= array(
					'IDAgen'=> $id_agen,
					'Kode_Perusahaan'=>$kode,
					'Nama_Perusahaan'=>$nama,
					'NPWP'=>$npwp,
					'Initial'=>$initial,
					'Alamat' => $alamat,
					'IDKota' => $kota,
					'No_Tlp'=>$telp,
					'Image' => $file_name,
					'Aktif'=>$status
				);

				$this->M_agen->tambah_agen($data);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
				if($simpan){
					redirect('Agen/index');
				}elseif($simtam){
					redirect('Agen/tambah_agen');
				}
			}
		}elseif($_FILES['image']['name']==null || $_FILES['image']['name']=="") {

			$data= array(
				'IDAgen'=> $id_agen,
				'Kode_Perusahaan'=>$kode,
				'Nama_Perusahaan'=>$nama,
				'NPWP'=>$npwp,
				'Initial'=>$initial,
				'Alamat' => $alamat,
				'IDKota' => $kota,
				'No_Tlp'=>$telp,
				'Aktif'=>$status
			);

			$this->M_agen->tambah_agen($data);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
			redirect('Agen/index');
		}
		else{
			echo "<script>alert('Gagal Menyimpan Data');</script>";
		}
			// $this->session->set_flashdata("Pesan", "<div class=\"alert alert-success alert-bordered\">
			// 	<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span>×</span><span class=\"sr-only\">Close</span></button>
			// 	<span class=\"text-semibold\">Berhasil!</span> Data berhasil disimpan
			// 	</div>");
		if($simpan){
			redirect('Agen/index');
		}elseif($simtam){
			redirect('Agen/tambah_agen');
		}
	}
	public function hapus_agen($id)
	{

		$this->M_agen->hapus_data_agen($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
		redirect('Agen/index');
	}

	public function edit_agen($id)
	{
		$data['edit']=$this->M_agen->ambil_agen_byid($id);
		$data['kota'] = $this->M_agen->tampil_kota();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/agen/V_edit_agen', $data);
	}

	public function proses_edit_agen($id)
	{

		$kode= $this->input->post('kode');
		$nama = $this->input->post('nama');
		$initial = $this->input->post('initial');
		$alamat = $this->input->post('alamat');
		$kota = $this->input->post('kota');
		$telp = $this->input->post('telp');
		$status = $this->input->post('status');
		$npwp = $this->input->post('npwp');


		if($_FILES['image']['name']!=null || $_FILES['image']['name']!="") {
			unset($config);
			$configFile['upload_path'] = 'upload/';
			$configFile['max_size'] = '60000';
			$configFile['allowed_types'] = 'png|jpg|PNG|JPG|jpeg';
			$configFile['overwrite'] = FALSE;
			$configFile['remove_spaces'] = TRUE;
			$file_name = time()."_".rand().".png";
			$configFile['file_name'] = $file_name;

			$this->load->library('upload', $configFile);
			$this->upload->initialize($configFile);
			if(!$this->upload->do_upload('image')) {
				echo $this->upload->display_errors();
			}else{
				$fileDetails = $this->upload->data();
				$data= array(
					'Kode_Perusahaan'=>$kode,
					'Nama_Perusahaan'=>$nama,
					'NPWP'=>$npwp,
					'Initial'=>$initial,
					'Alamat' => $alamat,
					'IDKota' => $kota,
					'No_Tlp'=>$telp,
					'Image' => $file_name,
					'Aktif'=>$status
				);

				$this->M_agen->aksi_edit_agen($id, $data);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
				redirect('Agen/index');
			}
		}elseif($_FILES['image']['name']==null || $_FILES['image']['name']=="") {

			$data= array(
				'Kode_Perusahaan'=>$kode,
				'Nama_Perusahaan'=>$nama,
				'NPWP'=>$npwp,
				'Initial'=>$initial,
				'Alamat' => $alamat,
				'IDKota' => $kota,
				'No_Tlp'=>$telp,
				'Aktif'=>$status
			);

			$this->M_agen->aksi_edit_agen($id, $data);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
			redirect('Agen/index');
		}
		else{
			echo "<script>alert('Gagal Menyimpan Data');</script>";
		}
		
	}

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['agen']=$this->M_agen->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['agen']=$this->M_agen->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['agen']=$this->M_agen->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['agen']=$this->M_agen->cari_by_keyword($keyword);
		}else {
			$data['agen']=$this->M_agen->tampilkan_agen();
		}
		$this->load->view('administrator/agen/V_agen', $data);
	}

	public function show($id)
	{
		$data['agen'] = $this->M_agen->getDetailId($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/agen/V_show', $data);
	}
}

?>