
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class HargaJualBarang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_harga_jual_barang');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index()
	{
		$data['hargajual']=$this->M_harga_jual_barang->tampilkan_harga_jual_barang();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/harga_jual_barang/V_harga_jual_barang.php', $data);
	}
	public function tambah_harga_jual_barang()
	{
		
		$data['barang']= $this->M_harga_jual_barang->tampil_barang();
		$data['satuan']= $this->M_harga_jual_barang->tampil_satuan();
		$data['groupcustomer']= $this->M_harga_jual_barang->tampil_group_customer();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/harga_jual_barang/V_tambah_harga_jual_barang', $data);
	}
	public function proses_harga_jual_barang()
	{
		$data['id_harga_jual']=$this->M_harga_jual_barang->tampilkan_id_harga_jual_barang();
		if($data['id_harga_jual']!=""){
			// foreach ($data['id_harga_jual'] as $value) {
				$urutan= substr($data['id_harga_jual']->IDHargaJual, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$id_harga_jual= $urutan_id;
		$barang= $this->input->post('barang');
		$satuan= $this->input->post('satuan');
		$groupcustomer = $this->input->post('groupcustomer');
		$harga_jual= str_replace(".", "", $this->input->post('harga_jual'));
		$modal= str_replace(".", "", $this->input->post('modal'));

		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');


		$data=array(
			'IDHargaJual'=> $id_harga_jual,
			'IDBarang'=>$barang,
			'IDSatuan'=>$satuan,
			'Modal'=>floatval($modal),
			'Harga_Jual'=>floatval($harga_jual),
			'IDGroupCustomer'=>$groupcustomer
		);
		$this->M_harga_jual_barang->tambah_harga_jual_barang($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('HargaJualBarang/index');
		}elseif($simtam){
			redirect('HargaJualBarang/tambah_harga_jual_barang');
		}

		
	}
	public function hapus_harga_jual_barang($id)
	{

		$this->M_harga_jual_barang->hapus_data_jual_harga_barang($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
		redirect('HargaJualBarang/index');
	}

	public function edit_harga_jual_barang($id)
	{
		$data['barang']= $this->M_harga_jual_barang->tampil_barang();
		$data['satuan']= $this->M_harga_jual_barang->tampil_satuan();
		$data['groupcustomer']= $this->M_harga_jual_barang->tampil_group_customer();
		$data['edit']=$this->M_harga_jual_barang->ambil_harga_jual_barang_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/harga_jual_barang/V_edit_harga_jual_barang', $data);
	}

	public function proses_edit_harga_jual_barang($id)
	{

		$barang= $this->input->post('barang');
		$satuan= $this->input->post('satuan');
		$harga_jual= str_replace(".", "", $this->input->post('harga_jual'));
		$modal= str_replace(".", "", $this->input->post('modal'));
		$groupcustomer = $this->input->post('groupcustomer');


		$data=array(
			'IDBarang'=>$barang,
			'IDSatuan'=>$satuan,
			'Modal'=>floatval($modal),
			'Harga_Jual'=>floatval($harga_jual),
			'IDGroupCustomer'=>$groupcustomer
		);
		$this->M_harga_jual_barang->aksi_edit_harga_jual_barang($id, $data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		redirect('HargaJualBarang/index');
	}

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		
		$kolom= addslashes($this->input->post('jenispencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $keyword!="") {
			$data['hargajual']=$this->M_harga_jual_barang->cari_by_kolom($kolom,$keyword);
		}elseif ($keyword!="") {
			$data['hargajual']=$this->M_harga_jual_barang->cari_by_keyword($keyword);
		}else {
			$data['hargajual']=$this->M_harga_jual_barang->tampilkan_harga_jual_barang();
		}
		$this->load->view('administrator/harga_jual_barang/V_harga_jual_barang', $data);
	}

	public function get_corakmerk()
	{
		$barang = $this->input->post('id_barang');

		$data = $this->M_harga_jual_barang->get_corak_merk($barang);
		echo json_encode($data);
	}
}

?>