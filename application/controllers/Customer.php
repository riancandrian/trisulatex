
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Customer extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_customer');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['customer']=$this->M_customer->tampilkan_customer();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/customer/V_customer.php', $data);
	}
	public function tambah_customer()
	{
		
		$data['groupcustomer']= $this->M_customer->tampil_group_customer();
		$data['kota']= $this->M_customer->tampil_kota();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/customer/V_tambah_customer', $data);
	}
	public function proses_customer()
	{
		$data['id_customer']=$this->M_customer->tampilkan_id_customer();
		if($data['id_customer']!=""){
			// foreach ($data['id_customer'] as $value) {
				$urutan= substr($data['id_customer']->IDCustomer, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$id_customer= $urutan_id;
		$group= $this->input->post('group');
		$kode= $this->input->post('kode');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$status = $this->input->post('status');
		$telp = $this->input->post('telp');
		$kota = $this->input->post('kota');
		$fax = $this->input->post('fax');
		$email = $this->input->post('email');
		$npwp = $this->input->post('npwp');
		$ktp= $this->input->post('ktp');

		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');


		$data=array(
			'IDCustomer' => $id_customer,
			'IDGroupCustomer'=>$group,
			'Kode_Customer'=>$kode,
			'Nama'=>$nama,
			'Alamat'=>$alamat,
			'No_Telpon'=>$telp,
			'IDKota'=>$kota,
			'Fax'=>$fax,
			'Email'=>$email,
			'NPWP'=>$npwp,
			'No_KTP'=> $ktp,
			'Aktif'=> 'aktif'
		);
		$this->M_customer->tambah_customer($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('Customer/index');
		}elseif($simtam){
			redirect('Customer/tambah_customer');
		}

		
	}
	public function hapus_customer($id)
	{
		$data['sogetkain']= $this->M_customer->get_so_kain($id);
		$data['sogetnonkain']= $this->M_customer->get_so_no_kain($id);

		if(count($data['sogetkain']) > 0){
			if(($data['sogetkain']->IDCustomer!=$id)){   
				$this->M_customer->hapus_data_customer($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
				redirect('Customer/index');
			}else{
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
				redirect('Customer/index');
			}
		}elseif(count($data['sogetnonkain']) > 0){
			if(($data['sogetnonkain']->IDCustomer!=$id)){   
				$this->M_customer->hapus_data_customer($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
				redirect('Customer/index');
			}else{
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
				redirect('Customer/index');
			}
		}else{
			$this->M_customer->hapus_data_customer($id);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
			redirect('Customer/index');
		}
	}

	public function edit_customer($id)
	{
		$data['edit']=$this->M_customer->ambil_customer_byid($id);
		$data['groupcustomer']= $this->M_customer->tampil_group_customer();
		$data['kota']= $this->M_customer->tampil_kota();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/customer/V_edit_customer', $data);
	}

	public function proses_edit_customer($id)
	{

		$group= $this->input->post('group');
		$kode= $this->input->post('kode');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$status = $this->input->post('status');
		$telp = $this->input->post('telp');
		$kota = $this->input->post('kota');
		$fax = $this->input->post('fax');
		$email = $this->input->post('email');
		$npwp = $this->input->post('npwp');
		$ktp= $this->input->post('ktp');
		
		
		$data=array(
			'IDGroupCustomer'=>$group,
			'Kode_Customer'=>$kode,
			'Nama'=>$nama,
			'Alamat'=>$alamat,
			'No_Telpon'=>$telp,
			'IDKota'=>$kota,
			'Fax'=>$fax,
			'Email'=>$email,
			'NPWP'=>$npwp,
			'No_KTP'=> $ktp
		);
		$this->M_customer->aksi_edit_customer($id, $data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil diubah</div></center>");
		redirect('Customer/index');
	}

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['customer']=$this->M_customer->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['customer']=$this->M_customer->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['customer']=$this->M_customer->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['customer']=$this->M_customer->cari_by_keyword($keyword);
		}else {
			$data['customer']=$this->M_customer->tampilkan_customer();
		}
		$this->load->view('administrator/customer/V_customer', $data);
	}
	public function show($id)
	{
		$data['customer'] = $this->M_customer->getDetailId($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/customer/V_show', $data);
	}
}

?>