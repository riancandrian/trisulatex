<?php

/**
 *
 */
class Bank extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_bank');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
        $data['banks'] = $this->M_bank->tampilkan_bank();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/bank/V_bank.php', $data);
    }

    public function tambah_bank()
    {

        $data['coa'] = $this->M_bank->coa();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/bank/V_tambah_bank', $data);
    }

    public function simpan()
    {
        $simpan= $this->input->post('simpan');
        $simtam= $this->input->post('simtam');

        $data['id_bank'] = $this->M_bank->tampilkan_id_bank();
        if($data['id_bank']!=""){
            // foreach ( $data['id_bank'] as $value) {
              $urutan= substr($data['id_bank']->IDBank, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
      $data = array(
        'IDBank' => $urutan_id,
        'Nomor_Rekening' => $this->input->post('no_rek'),
        'Atas_Nama' => $this->input->post('an'),
        'IDCoa' => $this->input->post('coa'),
        'Aktif' => 'aktif'
    );

      $this->M_bank->simpan($data);
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");

      if($simpan){
        redirect('Bank/index');
    }elseif($simtam){
        redirect('Bank/tambah_bank');
    }
}

public function edit($id)
{
    $data['coa'] = $this->M_bank->coa();
    $data['banks'] = $this->M_bank->getById($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/bank/V_edit_bank', $data);
}

public function update()
{
    $data = array(
        'Nomor_Rekening' => $this->input->post('no_rek'),
        'Atas_Nama' => $this->input->post('an'),
        'IDCoa' => $this->input->post('coa')
    );

    $this->M_bank->update($data, array('IDBank' => $this->input->post('id')));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data berhasil diubah </div></center>");
    redirect('Bank/index');
}

public function show($id)
{
    $data['banks'] = $this->M_bank->getDetailId($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/bank/V_show', $data);
}

public function delete($id)
{
    $data['coagetinvumum']= $this->M_bank->get_coa_inv_umum($id);
    $data['coagetpembayaran']= $this->M_bank->get_coa_jurnal($id);

        if(count($data['coagetinvumum']) > 0){
            if(($data['coagetinvumum']->IDCOA != $id)){   
                $this->M_bank->delete($id);
                $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
                 redirect('Bank');
            }else{
                $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
                 redirect('Bank');
            }
        }elseif(count($data['coagetpembayaran']) > 0){
            if(($data['coagetpembayaran']->IDCOA != $id)){   
                $this->M_bank->delete($id);
                $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
                 redirect('Bank');
            }else{
                $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
                 redirect('Bank');
            }
        }else{
            $this->M_bank->delete($id);
            $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
             redirect('Bank');
        }
}

public function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();

    $kolom= addslashes($this->input->post('jenispencarian'));
    $status= addslashes($this->input->post('statuspencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $status!="" && $keyword!="") {
        $data['banks']=$this->M_bank->cari($kolom,$status,$keyword);
    }elseif ($kolom!="" && $keyword!="") {
        $data['banks']=$this->M_bank->cari_by_kolom($kolom,$keyword);
    }elseif ($status!="") {
        $data['banks']=$this->M_bank->cari_by_status($status);
    }elseif ($keyword!="") {
        $data['banks']=$this->M_bank->cari_by_keyword($keyword);
    }else {
        $data['banks']=$this->M_bank->tampilkan_bank();
    }
    $this->load->view('administrator/bank/V_bank', $data);
}
}