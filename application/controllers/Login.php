<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$this->load->view('administrator/index.php');
	}
	public function keluar()
	{
		$this->load->view('welcome_message');
	}
	public function welcome()
	{
		$this->load->view('welcome_message');
	}
	public function aksi_login()
	{
	// $data = array('username' => $this->input->post('username', TRUE),
	// 		'password' => md5($this->input->post('password', TRUE))
	// 		);
		$user=$this->input->post('username');
		$pass=$this->input->post('password');
		$this->load->model('M_login'); // load model_user
		$hasil = $this->M_login->cek_user($user, $pass);
		if ($hasil) {
			foreach ($hasil as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['id'] = $sess->IDUser;
				$sess_data['nama'] = $sess->Nama;
				$sess_data['username'] = $sess->Username;
				$sess_data['email'] = $sess->Email;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata()) {
				$data['aksesmenu']= $this->M_login->aksesmenu();
				$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
				$data['aksessetting']= $this->M_login->aksessetting();

				$iduser = $this->session->userdata('id');
				$tgl 		= date('Y-m-d h:i:s');
				$query 	= 'INSERT INTO tbl_loguser("IDUser", "Aksi", "Tanggal")';
				$query .= " VALUES('".$iduser."', 'Login', current_timestamp )";

				$insert = $this->db->query($query);

				if($insert){
					$this->load->view('administrator/index.php', $data);
				}

			}

		}else {
			echo "<script>alert('Username dan Password Tidak Sesuai');history.go(-1);</script>"; 
			
		}
	}

	public function logout(){

		$iduser = $this->session->userdata('id');
		$tgl 		= date('Y-m-d h:i:s');
		$query 	= 'INSERT INTO tbl_loguser("IDUser", "Aksi", "Tanggal")';
		$query .= " VALUES('".$iduser."', 'Logout', current_timestamp )";

		$insert = $this->db->query($query);
		if($insert){
			$this->session->sess_destroy();
			$this->load->view('welcome_message');
		}
	}
}
?>

