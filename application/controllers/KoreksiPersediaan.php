<?php

class KoreksiPersediaan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_koreksi_persediaan');
    $this->load->model('M_agen');
    $this->load->model('M_corak');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
  }

  public function index()
  {
    $data['persediaan'] = $this->M_koreksi_persediaan->tampilkan_koreksi_persediaan();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/koreksi_persediaan/V_koreksi_persediaan.php', $data);
  }


  public function tambah_koreksi_persediaan()
  {
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    
    $data['corak'] = $this->M_corak->all();
    $data['kodenoasset'] = $this->M_koreksi_persediaan->no_asset(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $this->load->view('administrator/koreksi_persediaan/V_tambah_koreksi_persediaan', $data);
  }

  public function simpan_koreksi_persediaan(){

    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');
    $data['id_koreksi_persediaan'] = $this->M_koreksi_persediaan->tampilkan_id_koreksi_persediaan();
    if($data['id_koreksi_persediaan']!=""){
      foreach ($data['id_koreksi_persediaan'] as $value) {
        $urutan= substr($value->IDKP, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data = array(
      'IDKP' => $urutan_id,
      'Tanggal' => $data1['tanggal'],
      'Nomor' => $data1['no_kp'],
      'Total_yard' => $data1['total_yard'],
      'Total_meter' => $data1['total_meter'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Jenis' => $data1['jenis'],
      'Keterangan' => $data1['keterangan'],
      'Batal' => 'aktif'
    );
        // $this->M_pembelian_asset->add($data);


    if($this->M_koreksi_persediaan->add($data)){
      // $last_insert_id = $this->db->insert_id(); 
      $data = null;
      foreach($data2 as $row){
        $data['id_koreksi_persediaan_detail'] = $this->M_koreksi_persediaan->tampilkan_id_koreksi_persediaan_detail();
        if($data['id_koreksi_persediaan_detail']!=""){
          foreach ($data['id_koreksi_persediaan_detail'] as $value) {
            $urutan= substr($value->IDKPDetail, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail = 'P000001';
        }
        $data = array(
         'IDKPDetail' => $urutan_id_detail,
         'IDKP' => $urutan_id,
         'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDSatuan : 0,
         'Barcode' => $row['barcode'],
         'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDBarang : 0,
         'IDCorak' => $row['corak'],
         'IDWarna' => $row['warna'],
         'IDMataUang' => 1,
         'Kurs' => 14000,
         'Qty_yard' => $row['qty_yard'],
         'Qty_meter' => $row['qty_meter'],
         'Saldo_yard' => $row['qty_yard'],
         'Saldo_meter' => $row['qty_meter'],
         'Grade' => $row['grade'],
         'Harga' =>  $row['harga']
       );
        $this->M_koreksi_persediaan->add_detail($data);

        // $stok = $this->M_koreksi_persediaan->find_tbl_stok($row['barcode']);

                 //  $yard = $stok->Saldo_yard;
                 //  $meter = $stok->Saldo_meter;

                 //  if($data1['jenis']=='plus'){
                 //   $data5 = array(
                 //       'Saldo_yard' => ($yard + $row['qty_yard']),
                 //      'Saldo_meter' => ($meter + $row['qty_meter']),
                 //   );
                 // }elseif($data1['jenis']=='minus')
                 // {
                 //  $data5 = array(
                 //       'Saldo_yard' => ($yard - $row['qty_yard']),
                 //      'Saldo_meter' => ($meter - $row['qty_meter']),
                 //   );
                 // }
                 //    $this->M_koreksi_persediaan->update_tbl_stok($data5, $row['barcode']);
        $data['id_stok'] = $this->M_koreksi_persediaan->tampilkan_id_stok();
        if($data['id_stok']!=""){
          foreach ($data['id_stok'] as $value) {
            $urutan= substr($value->IDStok, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_stok= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_stok = 'P000001';
        }
        $data5 = array(
         'IDStok' => $urutan_id_stok,
         'Tanggal' => $data1['tanggal'],
         'Nomor_faktur' => $data1['no_kp'],
         'IDFaktur' => $urutan_id,
         'IDFakturDetail' => $urutan_id_detail,
         'Barcode' => $row['barcode'],
         'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDBarang : 0,
         'IDGudang' => 0,
         'IDCorak' => $row['corak'],
         'IDWarna' => $row['warna'],
         'IDMataUang' => 1,
         'Kurs' => 14000,
         'Qty_yard' => $row['qty_yard'],
         'Qty_meter' => $row['qty_meter'],
         'Saldo_yard' => $row['qty_yard'],
         'Saldo_meter' => $row['qty_meter'],
         'Grade' => $row['grade'],
         'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDSatuan : 0,
         'Harga' =>  $row['harga'],
         'IDMataUang' => 1,
         'Kurs' => 14000,
         'Total' => 0
       );

        $this->M_koreksi_persediaan->add_stok($data5);

        $data['id_kartu_stok'] = $this->M_koreksi_persediaan->tampilkan_id_kartu_stok();
        if($data['id_kartu_stok']!=""){
          foreach ($data['id_kartu_stok'] as $value) {
            $urutan= substr($value->IDKartuStok, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_kartu_stok= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_kartu_stok = 'P000001';
        }
        $data6 = array(
          'IDKartuStok' => $urutan_id_kartu_stok,
          'Tanggal' => $data1['tanggal'],
          'Nomor_faktur' => $data1['no_kp'],
          'IDFaktur' => $urutan_id,
          'IDFakturDetail' => $urutan_id_detail,
          'IDStok' => $urutan_id_stok,
          'Jenis_faktur' => 'SA',
          'Barcode' => $row['barcode'],
          'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDBarang : 0,
          'IDGudang' => 0,
          'IDCorak' => $row['corak'],
          'IDWarna' => $row['warna'],
          'IDMataUang' => 1,
          'Kurs' => 14000,
          'Masuk_yard' => $row['qty_yard'],
          'Masuk_meter' => $row['qty_meter'],
          'Keluar_yard' => $row['qty_yard'],
          'Keluar_meter' => $row['qty_meter'],
          'Grade' => $row['grade'],
          'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['corak'])->IDSatuan : 0,
          'Harga' =>  $row['harga'],
          'IDMataUang' => 1,
          'Kurs' => 14000,
          'Total' => 0
        );

        $this->M_koreksi_persediaan->add_kartu_stok($data6);

        //save jurnal
        $data['id_jurnal'] = $this->M_koreksi_persediaan->tampilkan_id_jurnal();
        if($data['id_jurnal']!=""){
          foreach ($data['id_jurnal'] as $value) {
            $urutan= substr($value->IDJurnal, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_jurnal = 'P000001';
        }

        $save_jurnal_debet = array(
          'IDJurnal'=>$urutan_id_jurnal,
          'Tanggal' => $data1['tanggal'],
          'Nomor' =>$data1['no_kp'],
          'IDFaktur' => $urutan_id,
          'IDFakturDetail' => $urutan_id_detail,
          'Jenis_faktur' => 'KP',
          'IDCOA' => 'P000413',
          'Debet' => $row['harga'],
          'Kredit' => 0,
          'IDMataUang' => 1,
          'Kurs' => 14000,
          'Total_debet' => $row['harga'],
          'Total_kredit' => 0,
          'Keterangan' => $data1['keterangan'],
          'Saldo' => $row['harga'],
        );
        $this->M_koreksi_persediaan->save_jurnal($save_jurnal_debet);

        $data['id_jurnal'] = $this->M_koreksi_persediaan->tampilkan_id_jurnal();
        if($data['id_jurnal']!=""){
          foreach ($data['id_jurnal'] as $value) {
            $urutan= substr($value->IDJurnal, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_jurnal_kredit= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_jurnal_kredit = 'P000001';
        }

        $save_jurnal_kredit = array(
          'IDJurnal'=>$urutan_id_jurnal_kredit,
          'Tanggal' => $data1['tanggal'],
          'Nomor' => $data1['no_kp'],
          'IDFaktur' => $urutan_id,
          'IDFakturDetail' => $urutan_id_detail,
          'Jenis_faktur' => 'KP',
          'IDCOA' => 'P000016',
          'Debet' => 0,
          'Kredit' => $row['harga'],
          'IDMataUang' => 1,
          'Kurs' => 14000,
          'Total_debet' => 0,
          'Total_kredit' => $row['harga'],
          'Keterangan' => $data1['keterangan'],
          'Saldo' => $row['harga'],
        );
        $this->M_koreksi_persediaan->save_jurnal($save_jurnal_kredit);

        // $data['id_jurnal'] = $this->M_koreksi_persediaan->tampilkan_id_jurnal();
        // if($data['id_jurnal']!=""){
        //   foreach ($data['id_jurnal'] as $value) {
        //     $urutan= substr($value->IDJurnal, 1);
        //   }
        //   $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        //   $urutan_id_jurnal_per_debet= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        // }else{
        //   $urutan_id_jurnal_per_debet = 'P000001';
        // }

        // $save_jurnal_per_debet = array(
        //   'IDJurnal'=>$urutan_id_jurnal_per_debet,
        //   'Tanggal' => $data1['tanggal'],
        //   'Nomor' => $data1['no_kp'],
        //   'IDFaktur' => $urutan_id,
        //   'IDFakturDetail' => $urutan_id_detail,
        //   'Jenis_faktur' => 'KP',
        //   'IDCOA' => 'P000139',
        //   'Debet' => $row['harga'],
        //   'Kredit' => 0,
        //   'IDMataUang' => 1,
        //   'Kurs' => 14000,
        //   'Total_debet' => $row['harga'],
        //   'Total_kredit' => 0,
        //   'Keterangan' => $data1['keterangan'],
        //   'Saldo' => $row['harga'],
        // );
        // $this->M_koreksi_persediaan->save_jurnal($save_jurnal_per_debet);

        // $data['id_jurnal'] = $this->M_koreksi_persediaan->tampilkan_id_jurnal();
        // if($data['id_jurnal']!=""){
        //   foreach ($data['id_jurnal'] as $value) {
        //     $urutan= substr($value->IDJurnal, 1);
        //   }
        //   $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        //   $urutan_id_jurnal_pend_debet= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        // }else{
        //   $urutan_id_jurnal_pend_debet = 'P000001';
        // }

        // $save_jurnal_pend_debet = array(
        //   'IDJurnal'=>$urutan_id_jurnal_pend_debet,
        //   'Tanggal' => $data1['tanggal'],
        //   'Nomor' => $data1['no_kp'],
        //   'IDFaktur' => $urutan_id,
        //   'IDFakturDetail' => $urutan_id_detail,
        //   'Jenis_faktur' => 'KP',
        //   'IDCOA' => 'P000141',
        //   'Debet' => 0,
        //   'Kredit' => $row['harga'],
        //   'IDMataUang' => 1,
        //   'Kurs' => 14000,
        //   'Total_debet' => 0,
        //   'Total_kredit' => $row['harga'],
        //   'Keterangan' => $data1['keterangan'],
        //   'Saldo' => $row['harga'],
        // );
        // $this->M_koreksi_persediaan->save_jurnal($save_jurnal_pend_debet);

      }


      echo true;
    }else{
     echo false;
   }

 }

 public function edit($id)
 {
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $data['corak'] = $this->M_corak->all();
  $data['koreksi'] = $this->M_koreksi_persediaan->getById($id);
  $data['detail'] = $this->M_koreksi_persediaan->detail_koreksi_persediaan($id);
  $this->load->view('administrator/koreksi_persediaan/V_edit_koreksi_persediaan', $data);
}

public function ubah_koreksi_persediaan()
{
  $data1 = $this->input->post('data1');
  $data2 = $this->input->post('data2');
  $data3 = $this->input->post('data3');

  $data = array(
    'Tanggal' => $data1['tanggal'],
    'Nomor' => $data1['no_kp'],
    'Total_yard' => $data1['total_yard'],
    'Total_meter' => $data1['total_meter'],
    'IDMataUang' => 1,
    'Kurs' => 14000,
    'Jenis' => $data1['jenis'],
    'Keterangan' => $data1['keterangan']
  );

  if($this->M_koreksi_persediaan->update_master($data1['IDKP'],$data)){
    $data = null;
    if($data3 > 0){
     foreach($data3 as $row){
       $this->M_koreksi_persediaan->drop($row['IDKPDetail']);
     }
   }
   if($data2 != null){
    foreach($data2 as $row){
      $data['id_koreksi_persediaan_detail'] = $this->M_koreksi_persediaan->tampilkan_id_koreksi_persediaan_detail();
      if($data['id_koreksi_persediaan_detail']!=""){
        foreach ($data['id_koreksi_persediaan_detail'] as $value) {
          $urutan= substr($value->IDKPDetail, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_detail = 'P000001';
      }
      $data = array(
        'IDKPDetail' => $urutan_id_detail,
        'IDKP'=>$data1['IDKP'],
        'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['Corak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['Corak'])->IDSatuan : 0,
        'Barcode' => $row['Barcode'],
        'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['Corak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['Corak'])->IDBarang : 0,
        'IDCorak' => $row['Corak'],
        'IDWarna' => $row['Warna'],
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Qty_yard' => $row['Qty_yard'],
        'Qty_meter' => $row['Qty_meter'],
        'Saldo_yard' => $row['Qty_yard'],
        'Saldo_meter' => $row['Qty_meter'],
        'Grade' => $row['Grade'],
        'Harga' =>  $row['Harga']
      );

      if($row['IDKPDetail'] ==''){
        $this->M_koreksi_persediaan->add_detail($data);

      }
                    // else
                    // {
                    //   $this->M_koreksi_persediaan->update_detail($row['IDKPDetail'], $data);                       
                    // }
    }
  }

  echo true;

}else{
  echo false;
}
}

public function status_gagal($id)
{
  $data = array(
    'Batal' => 'tidak aktif',
  );

  $this->M_koreksi_persediaan->update_status($data, array('IDKP' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('KoreksiPersediaan/index');
}

public function status_berhasil($id)
{
  $data = array(
    'Batal' => 'aktif',
  );

  $this->M_koreksi_persediaan->update_status($data, array('IDKP' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('KoreksiPersediaan/index');
}

public function delete_multiple()
{
  $ID_att = $this->input->post('msg');
  if($ID_att != ""){
   $result = array();
   foreach($ID_att AS $key => $val){
     $result[] = array(
      "IDKP" => $ID_att[$key],
      "Batal"  => 'tidak aktif'
    );
   }
   $this->db->update_batch('tbl_koreksi_persediaan', $result, 'IDKP');
   redirect("KoreksiPersediaan/index");
 }else{
   redirect("KoreksiPersediaan/index");
 }

}

function pencarian()
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['persediaan'] = $this->M_koreksi_persediaan->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/koreksi_persediaan/V_koreksi_persediaan', $data);
}

function show($id)
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['data'] = $this->M_koreksi_persediaan->find_show($id);
  $data['koreksidetail'] = $this->M_koreksi_persediaan->find_detail($id);
  $this->load->view('administrator/koreksi_persediaan/V_show', $data);
}

function ubah_pembelian_tabel_asset()
{
 $data = array(
  'IDAsset' => $this->input->post('IDAsset'),
  'IDGroupAsset' => $this->input->post('IDGroupAsset'),
  'Nilai_perolehan' => $this->input->post('Nilai_perolehan')
);

 $this->M_pembelian_asset->update($data, array('IDFBADetail' => $this->input->post('IDFBADetail')));
}

function print($nomor)
{

  $data['print'] = $this->M_pembelian_asset->print_pembelian_asset($nomor);
  $id= $data['print']->IDFBA;
  $data['printdetail'] = $this->M_pembelian_asset->print_pembelian_asset_detail($id);
  $this->load->view('administrator/Pembelian_asset/V_print', $data);
}
}