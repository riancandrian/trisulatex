<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SuratJalanSeragam extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_suratjalanseragam');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
    date_default_timezone_set('Asia/Jakarta');
  }
  
  public function index()
  {
    $data = array(
          'title_master' => "Master Surat Jalan Non Kain", //Judul 
          'title' => "List Data Surat Jalan Non Kain", //Judul Tabel
          'action' => site_url('SuratJalanSeragam/create'), //Alamat Untuk Action Form
          'button' => "Tambah Suerat Jalan", //Nama Button
          'suratjalanseragam' => $this->M_suratjalanseragam->get_all() //Load Data Surat Jalan Seragam
        );

    //die(print_r($data['SuratJalanSeragam']));
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/suratjalanseragam/V_suratjalanseragam', $data);
  }

    //--------------------Store Laporan So
  public function laporan_surat_jalan_non_kain()
    {
        $data = array(
        'title_master' => "Laporan Surat Jalan Non Kain", //Judul 
        'title' => "Laporan Surat Jalan Non Kain", //Judul Tabel
        'sj' => $this->M_suratjalanseragam->get_all_procedure()
    );
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/suratjalanseragam/V_laporan_suratjalanseragam', $data);
    }

    //Tambah Data Surat Jalan Seragam
  public function create()
  {
    $this->load->model('M_corak');
    
    $data['kodesuratjalan'] = $this->M_suratjalanseragam->no_suratjalanseragam(date('Y'));
   
    $data['sos'] = $this->M_suratjalanseragam->get_sos();
    //die(print_r($data['SuratJalanSeragam']));
    //$data['satuan'] = $this->M_suratjalanseragam->get_satuan();
    $data['corak'] = $this->M_corak->all();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $this->load->view('administrator/suratjalanseragam/V_tambah_suratjalanseragam', $data);
  }


    //Ubah data Surat Jalan Seragam
  public function edit($id)
  {
    $this->load->model('M_corak');
    
    $data['datasuratjalan'] = $this->M_suratjalanseragam->get_by_id($id);
    $data['datasuratjalandetail'] = $this->M_suratjalanseragam->get_suratjalanseragam_detail($id);
    $data['kodesuratjalan'] = $this->M_suratjalanseragam->no_suratjalanseragam(date('Y'));
    $data['sos'] = $this->M_suratjalanseragam->get_sos();
    $data['satuan'] = $this->M_suratjalanseragam->get_satuan();
    $data['corak'] = $this->M_corak->all();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/suratjalanseragam/V_edit_suratjalanseragam', $data);
  }


    //----------------------Aksi Ubah Batal single
  public function set_edit($id)
  {
   $cek = $this->M_suratjalanseragam->get_by_id($id);
   $data['invoiceget'] = $this->M_suratjalanseragam->tampilkan_get_invoice();

  if(count($data['invoiceget']) > 0){
    foreach($data['invoiceget'] as $valueinv) {
   if(($valueinv->IDSJCS != $id)){   

   $batal = "-";
   if ($cek->Batal == "aktif") {
     $batal ="tidak aktif";
   } else {
     $batal = "aktif";
   }
   $data = array(
     'Batal' => $batal
   );

   $this->M_suratjalanseragam->update($id,$data);
   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    }else{
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Surat Jalan Sudah Digunakan</div></center>");
    }
  }
  }else{
     $batal = "-";
   if ($cek->Batal == "aktif") {
     $batal ="tidak aktif";
   } else {
     $batal = "aktif";
   }
   $data = array(
     'Batal' => $batal
   );

   $this->M_suratjalanseragam->update($id,$data);
   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  }
   redirect('SuratJalanSeragam/index');
 }

   //----------------------Aksi Ubah Batal multiple
 public function delete_multiple()
 {
  $ID_att = $this->input->post('msg');
  if (empty($ID_att)) {
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
    redirect("SuratJalanSeragam/index");
  }
  $result = array();
  foreach($ID_att AS $key => $val){
   $result[] = array(
    "IDSJCS" => $ID_att[$key],
    "Batal"  => 'tidak aktif'
  );
 }
 $this->db->update_batch('tbl_purchase_order', $result, 'IDSJCS');
 $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
 redirect("SuratJalanSeragam/index");
}

    //----------------------Detail per data
public function show($id)
{
  $data['suratjalanseragam'] = $this->M_suratjalanseragam->get_by_id($id);
  $data['suratjalanseragamdetail'] = $this->M_suratjalanseragam->get_suratjalanseragam_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/suratjalanseragam/V_show', $data);
}

    //------------------------Get Merk
public function getmerk()
{
  $id_corak = $this->input->post('id_corak');
  $data = $this->M_suratjalanseragam->getmerk($id_corak);
  echo json_encode($data);
}

    //------------------------Get warna
public function getwarna()
{
  $id_kodecorak = $this->input->post('id_kodecorak');
  $data = $this->M_suratjalanseragam->getwarna($id_kodecorak);
  echo json_encode($data);
}

//----------------------------Get Detail SOS
public function get_detailsos()
{
    $IDSOS = $this->input->post('idSOS');
    $data = $this->M_suratjalanseragam->get_detailsos($IDSOS);
    echo json_encode($data);
    
}


public function simpan_suratjalanseragam(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');

  $data['id_suratjalanseragam'] = $this->M_suratjalanseragam->tampilkan_id_suratjalanseragam();
  if($data['id_suratjalanseragam']!=""){
    // foreach ($data['id_suratjalanseragam'] as $value) {
      $urutan= substr($data['id_suratjalanseragam']->IDSJCS, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }
  $data_atas = array(
    'IDSJCS' => $urutan_id,
    'IDSOS' => $data1['IDSOS'],
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCustomer' => $data1['IDCustomer'],
    'Tanggal_kirim' => $data1['Tanggal_kirim'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo_qty' => $data1['Saldo_qty'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );
  $this->M_suratjalanseragam->add($data_atas); 
  $data_bawah = null;
  foreach($data2 as $row){
   $data['id_suratjalanseragam_detail'] = $this->M_suratjalanseragam->tampilkan_id_suratjalanseragam_detail();
   if($data['id_suratjalanseragam_detail']!=""){
    // foreach ($data['id_suratjalanseragam_detail'] as $value) {
      $urutan= substr($data['id_suratjalanseragam_detail']->IDSJCSDetail, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
     $urutan_id_detail = 'P000001';
  }
  $data_bawah = array(
    'IDSJCSDetail' => $urutan_id_detail,
    'IDSJCS'=>$urutan_id,
    'IDBarang' => $row['IDBarang'],
    'IDCorak' => $row['IDCorak'],
    'IDMerk' => $row['IDMerk'],
    'IDWarna' => $row['IDWarna'],
    'Qty' => $row['Qty'],
    'Saldo_qty' => $row['Qty'],
    'IDSatuan' => $row['IDSatuan'],
  );
  $this->M_suratjalanseragam->add_detail($data_bawah);
 
}
      // $this->M_suratjalanseragam->add($data_atas);
      echo json_encode($data2);

}

public function ubah_suratjalanseragam(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDSJCS = $this->input->post('_id');

  $data_atas = array(
    'IDSOS' => $data1['IDSOS'],
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCustomer' => $data1['IDCustomer'],
    'Tanggal_kirim' => $data1['Tanggal_kirim'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo_qty' => $data1['Saldo_qty'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );

  if($this->M_suratjalanseragam->update_master($IDSJCS,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M_suratjalanseragam->drop($row['IDSJCSDetail']);
                  // console.log('delete temp');
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
        $data['id_suratjalanseragam_detail'] = $this->M_suratjalanseragam->tampilkan_id_suratjalanseragam_detail();
        if($data['id_suratjalanseragam_detail']!=""){
          // foreach ($data['id_suratjalanseragam_detail'] as $value) {
            $urutan= substr($data['id_suratjalanseragam_detail']->IDSJCSDetail, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail = 'P000001';
        }
        $data_atas = array(
          'IDSJCSDetail' => $urutan_id_detail,
          'IDSJCS'=>$IDSJCS,
          'IDBarang' => $row['IDBarang'],
          'IDCorak' => $row['IDCorak'],
          'IDMerk' => $row['IDMerk'],
          'IDWarna' => $row['IDWarna'],
          'Qty' => $row['Qty'],
          'Saldo_qty' => $row['Qty'],
          'IDSatuan' => $row['IDSatuan'],
        );
        if($row['IDSJCSDetail'] ==''){
          $this->M_suratjalanseragam->add_detail($data_atas);
        }
      }
    }

    echo true;

  }else{
    echo false;  
        // echo json_encode($data_atas);
  }
    
        // echo json_encode($data2);
}

  //--------------------------------pencarian
function pencarian()
{
  $data = array(
        'title_master' => "Master Surat Jalan Non Kain", //Judul 
        'title' => "List Data Surat Jalan Non Kain", //Judul Tabel
        'action' => site_url('SuratJalanSeragam/create'), //Alamat Untuk Action Form
        'button' => "Tambah Suerat Jalan", //Nama Button
        //'po' => $this->M_suratjalanseragam->get_all('PO TTI') //Load Data Surat Jalan Seragam
      );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['suratjalanseragam'] = $this->M_suratjalanseragam->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/suratjalanseragam/V_suratjalanseragam', $data);
}

function print_data_suratjalanseragam($nomor)
{

  $data['print'] = $this->M_suratjalanseragam->print_suratjalanseragam($nomor);
  $id= $data['print']->IDSJCS;
  $data['printdetail'] = $this->M_suratjalanseragam->print_suratjalanseragam_detail($id);
  $this->load->view('administrator/suratjalanseragam/V_print', $data);
}
    //------------------------------------Pencarian Store
     //--------------------------------pencarian
function pencarian_store()
{
  $data = array(
      'title_master' => "Laporan Surat Jalan Non Kain", //Judul 
      'title' => "Laporan Surat Jalan Non Kain", //Judul Tabel
      //'po' => $this->M_suratjalanseragam->get_all('PO TTI') //Load Data Surat Jalan Seragam
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  if ($this->input->post('date_from') == '') {
    $data['sj'] = $this->M_suratjalanseragam->searching_store_like($this->input->post('keyword'));
  } else {
    $data['sj'] = $this->M_suratjalanseragam->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
  }
  $this->load->view('administrator/suratjalanseragam/V_laporan_suratjalanseragam', $data);
}
}

/* End of file So */
