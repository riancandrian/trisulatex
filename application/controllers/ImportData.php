
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class ImportData extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('M_import_data');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['importdata']=$this->M_import_data->tampilkan_importdata();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/import_data/V_import_data.php', $data);
	}

	public function proses_import_data()
	{
		 $filename=$_FILES["file"]["tmp_name"];
        if($_FILES["file"]["size"] > 0)
        {
             $file = fopen($filename, "r");
             $baris = 0;
             while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE)
             {
                $data = array(
                   'Buyer' => $importdata[0],
                   'Barcode' => $importdata[1],
                   'NoSO' => $importdata[2],
                   'Party' => $importdata[3],
                   'Indent' => $importdata[4],
                   'CustDes' => $importdata[5],
                   'WarnaCust' => $importdata[6],
                   'Panjang_Yard' => floatval($importdata[7]),
                   'Panjang_Meter' => floatval($importdata[8]),
                   'Grade' => $importdata[9],
                   'Remark' => $importdata[10],
                   'Lebar' => $importdata[11],
                   'Satuan' => $importdata[12],
                );
                if($baris > 0){  //jika dimulai dari baris kedua
                   $insert = $this->M_import_data->insert_csv($data);
                }
                $baris++;
             }                    
             fclose($file);
             // $this->session->set_flashdata('message', 'Import berhasil !');
             // redirect('uploadcsv/index');
             echo "berhasil";
        }else{
             // $this->session->set_flashdata('message', 'Import gagal !');
             // redirect('uploadcsv/index');
        	echo "gagal";
        }
     }

     public function show($id)
    {
        $data['edit'] = $this->M_import_data->getDetailId($id);
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/import_data/V_show', $data);
    }

  function printData($id)
 {
  $data['tampilprint']=$this->M_import_data->getDataBarcode($id);
    $this->load->view('administrator/import_data/V_print_data', $data);
 } 

    public function pencarian()
    {
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();

        $kolom= addslashes($this->input->post('jenispencarian'));
        $keyword= addslashes($this->input->post('keyword'));

        if ($kolom!="" && $keyword!="") {
            $data['importdata']=$this->M_import_data->cari_by_kolom($kolom,$keyword);
        }elseif ($keyword!="") {
            $data['importdata']=$this->M_import_data->cari_by_keyword($keyword);
        }else {
            $data['importdata']=$this->M_import_data->tampilkan_importdata();
        }
          $this->load->view('administrator/import_data/V_import_data.php', $data);
    }

    public function form()
    {
      $this->load->view('administrator/import_data/V_tambah_data');
    }

    public function proses_tambah_barcode()
    {

      $data = array(
      'Buyer'=>"PT. SINAR ABADI CITRANUSA",
      'Barcode' => $this->input->post('barcode'),
      'NoSO' => $this->input->post('noso'),
      'Party' => $this->input->post('party'),
      'Indent' => $this->input->post('indent'),
      'CustDes' => $this->input->post('custdes'),
      'WarnaCust' => $this->input->post('warnacust'),
      'Panjang_Yard' => floatval($this->input->post('panjang')),
      'Panjang_Meter' => floatval($this->input->post('panjangm')),
      'Grade' => $this->input->post('grade'),
      'Remark' => $this->input->post('remark'),
      'Lebar' => $this->input->post('lebar'),
      'Satuan' => $this->input->post('satuan'),
    );

    $this->M_import_data->simpan($data);
    $ambil= $this->db->insert_id();
    $tes['tampilprinttambah'] = $this->M_import_data->getById($ambil);
    // $this->session->set_flashdata("Pesan", "<div class=\"alert alert-success pesan\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div>");
    // redirect('ImportData/proses_tambah_print');
    $this->load->view('administrator/import_data/V_tampil_print', $tes);
            
    }

    // public function proses_tambah_print()
    // {
    //   //  $this->M_import_data->tampilkan_importdata();
    //   // echo $this->db->insert_id();
    //   $data['tampilprint'] = $this->M_import_data->getById();
    //   $this->load->view('administrator/import_data/V_tampil_print', $data);
    // }
}


?>