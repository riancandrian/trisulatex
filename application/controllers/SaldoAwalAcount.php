
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class SaldoAwalAcount extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_saldoawalacount');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index()
	{
		$data['saldoawalacount']=$this->M_saldoawalacount->tampilkan_saldoawalacount();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/saldoawalacount/V_saldoawalaccount', $data);
	}

	public function tambah_saldoawalacount()
	{
		
		$data['coa']= $this->M_saldoawalacount->tampil_coa();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/saldoawalacount/V_tambahsaldoawalacount', $data);
	}
	public function getasset()
	{
		$IDAsset = $this->input->post('IDAsset');
		$data = $this->M_saldoawalacount->get_asset($IDAsset);
		echo json_encode($data);
	}
	public function simpan()
	{
		$tanggal_sekarang = date('Y-m-d');
		$data['id_saldo_awal_account']=$this->M_saldoawalacount->tampilkan_id_saldoawalacount();
		if($data['id_saldo_awal_account']!=""){
			// foreach ($data['id_saldo_awal_account'] as $value) {
				$urutan= substr($data['id_saldo_awal_account']->IDCOASaldoAwal, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$simbar= $this->input->post('simbar');
		$nilai= str_replace(".", "", $this->input->post('nilai'));
		$data = array(
			'IDCOASaldoAwal' => $urutan_id,
			'IDCoa' => $this->input->post('kode'),
			'IDGroupCOA' => $this->M_saldoawalacount->get_IDgroupcoa($this->input->post('Group'))->IDGroupCOA ? $this->M_saldoawalacount->get_IDgroupcoa($this->input->post('Group'))->IDGroupCOA : 0,
			'Nilai_Saldo_Awal' => $nilai,
			'Aktif' =>'aktif'
		);

		$this->M_saldoawalacount->simpan($data);

		if($this->input->post('Normal_balance')=='Debit'){
		$data['id_jurnal'] = $this->M_saldoawalacount->tampilkan_id_jurnal();
		if($data['id_jurnal']!=""){
			// foreach ($data['id_jurnal'] as $value) {
				$urutan= substr($data['id_jurnal']->IDJurnal, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id_jurnal_saldo= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id_jurnal_saldo = 'P000001';
		}

		$save_jurnal_saldo = array(
			'IDJurnal'=>$urutan_id_jurnal_saldo,
			'Tanggal' =>  $tanggal_sekarang,
			'Nomor' =>  $urutan_id,
			'IDFaktur' => $urutan_id,
			'IDFakturDetail' => $urutan_id,
			'Jenis_faktur' => 'SA',
			'IDCOA' => $this->input->post('kode'),
			'Debet' => str_replace(".", "", $this->input->post('nilai')),
			'Kredit' => 0,
			'IDMataUang' => 1,
			'Kurs' => 14000,
			'Total_debet' => str_replace(".", "", $this->input->post('nilai')),
			'Total_kredit' => 0,
			'Keterangan' => '-',
			'Saldo' => str_replace(".", "", $this->input->post('nilai')),
		);
		$this->M_saldoawalacount->save_jurnal($save_jurnal_saldo);
	}else if($this->input->post('Normal_balance')=='Kredit'){
		$data['id_jurnal'] = $this->M_saldoawalacount->tampilkan_id_jurnal();
		if($data['id_jurnal']!=""){
			// foreach ($data['id_jurnal'] as $value) {
				$urutan= substr($data['id_jurnal']->IDJurnal, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id_jurnal_saldo= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id_jurnal_saldo = 'P000001';
		}

		$save_jurnal_saldo = array(
			'IDJurnal'=>$urutan_id_jurnal_saldo,
			'Tanggal' =>  $tanggal_sekarang,
			'Nomor' =>  $urutan_id,
			'IDFaktur' => $urutan_id,
			'IDFakturDetail' => $urutan_id,
			'Jenis_faktur' => 'SA',
			'IDCOA' => $this->input->post('kode'),
			'Debet' => 0,
			'Kredit' => str_replace(".", "", $this->input->post('nilai')),
			'IDMataUang' => 1,
			'Kurs' => 14000,
			'Total_debet' => 0,
			'Total_kredit' => str_replace(".", "", $this->input->post('nilai')),
			'Keterangan' => '-',
			'Saldo' => str_replace(".", "", $this->input->post('nilai')),
		);
		$this->M_saldoawalacount->save_jurnal($save_jurnal_saldo);
		}

		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('SaldoAwalAcount/index');
		}elseif($simtam){
			redirect('SaldoAwalAcount/tambah_saldoawalacount');
		}
	}
	public function edit($id)
	{
		$data['saldoawal'] = $this->M_saldoawalacount->get_saldoawalcoa_byid($id);
		$data['coa']= $this->M_saldoawalacount->tampil_coa();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/saldoawalacount/V_editsaldoawalacount', $data);
	}
	public function update()
	{

		$nilai= str_replace(".", "", $this->input->post('nilai'));
		$data = array(
			'IDCoa' => $this->input->post('kode'),
			'IDGroupCOA' => $this->M_saldoawalacount->get_IDgroupcoa($this->input->post('Group'))->IDGroupCOA ? $this->M_saldoawalacount->get_IDgroupcoa($this->input->post('Group'))->IDGroupCOA : 0,
			'Nilai_Saldo_Awal' => $nilai
		);
		$this->M_saldoawalacount->update_saldoawalacount($this->input->post('id'),$data);
		if($this->input->post('Normal_balance')=='Kredit'){
		$save_jurnal_saldo = array(
			'IDCOA' => $this->input->post('kode'),
			'Debet' => 0,
			'Kredit' => str_replace(".", "", $this->input->post('nilai')),
			'Total_debet' => 0,
			'Total_kredit' => str_replace(".", "", $this->input->post('nilai')),
			'Saldo' => str_replace(".", "", $this->input->post('nilai')),
		);
		$this->M_saldoawalacount->update_jurnal($this->input->post('id'),$save_jurnal_saldo);
		}else if($this->input->post('Normal_balance')=='Debit'){
		$save_jurnal_saldo = array(
			'IDCOA' => $this->input->post('kode'),
			'Debet' => str_replace(".", "", $this->input->post('nilai')),
			'Kredit' => 0,
			'Total_debet' => str_replace(".", "", $this->input->post('nilai')),
			'Total_kredit' => 0,
			'Saldo' => str_replace(".", "", $this->input->post('nilai')),
		);
		$this->M_saldoawalacount->update_jurnal($this->input->post('id'),$save_jurnal_saldo);
		}

		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Ubah</div></center>");
		redirect('SaldoAwalAcount/index');
	}
	public function hapus_saldo_awal_coa($id)
	{

		$this->M_saldoawalacount->hapus_data_coasaldoawal($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
		redirect('SaldoAwalAcount/index');
	}
	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['saldoawalacount']=$this->M_saldoawalacount->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['saldoawalacount']=$this->M_saldoawalacount->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['saldoawalacount']=$this->M_saldoawalacount->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['saldoawalacount']=$this->M_saldoawalacount->cari_by_keyword($keyword);
		}else {
			$data['saldoawalacount']=$this->M_saldoawalacount->tampilkan_saldoawalacount();
		}
		$this->load->view('administrator/saldoawalacount/V_saldoawalaccount', $data);
	}
}

?>