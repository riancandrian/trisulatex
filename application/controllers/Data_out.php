<?php

class Data_out extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_data_out');
    }

    public function index()
    {
        $this->load->view('data_out');
    }

    public function getItem()
    {
        $barcode = $this->input->post('barcode');
        $data = $this->M_data_out->getItem_in($barcode);
        echo json_encode($data);
    }

    public function setDataOut()
    {
        $data = array(
            'Tanggal' => date('Y-m-d'),
            'Buyer' => $this->input->post('buyer'),
            'Barcode' => $this->input->post('barcod'),
            'NoSO' => $this->input->post('noSo'),
            'Party' => $this->input->post('party'),
            'Indent' => $this->input->post('indent'),
            'CustDes' => $this->input->post('CustDes'),
            'WarnaCust' => $this->input->post('WarnaCust'),
            'Panjang_Yard' => floatval($this->input->post('yard_stock')),
            'Panjang_Meter' => floatval($this->input->post('meter_stock')),
            'Grade' => $this->input->post('Grade'),
            'Remark' => $this->input->post('Remark'),
            'Lebar' => $this->input->post('Lebar'),
            'Satuan' => $this->input->post('Satuan')
        );
        $this->M_data_out->save($data);

        $where = array(
            'Barcode' => $this->input->post('barcod'),
            'CustDes' => $this->input->post('CustDes'),
            'WarnaCust' => $this->input->post('WarnaCust'),
            'Grade' => $this->input->post('Grade'),
        );
        $this->M_data_out->updateStock($where, $this->input->post('yard_stock'), $this->input->post('meter_stock'));

        $dataCard = array(
            'IDFaktur' => $this->input->post('idImport'),
            'Tanggal' => date('Y-m-d'),
            'Barcode' => $this->input->post('barcod'),
            'NoSO' => $this->input->post('noSo'),
            'CustDes' => $this->input->post('CustDes'),
            'WarnaCust' => $this->input->post('WarnaCust'),
            'Masuk_Yard' => 0,
            'Masuk_Meter' => 0,
            'Keluar_Yard' => $this->input->post('yard_stock'),
            'Keluar_Meter' => $this->input->post('meter_stock'),
            'Grade' => $this->input->post('Grade'),
            'Lebar' => $this->input->post('Lebar'),
            'Satuan' => $this->input->post('Satuan')
        );
        $insert = $this->M_data_out->saveStockCard($dataCard);

        if($insert){
            echo 1;
        } else {
            echo 0;
        }
    }
}