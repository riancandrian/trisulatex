
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Piutang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_piutang');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$data['piutang']=$this->M_piutang->tampilkan_piutang();
		$this->load->view('administrator/piutang/V_piutang', $data);
	}

	public function tambah_piutang()
	{		
		$data['customer']=$this->M_piutang->get_customer();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/piutang/V_tambah_piutang', $data);
	}
	public function simpan()
	{
		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');

		$data['id_piutang']=$this->M_piutang->tampilkan_piutang();
		if($data['id_piutang']!=""){
			foreach ($data['id_piutang'] as $value) {
				$urutan= substr($value->IDPiutang, 1);
			}
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		} 
		$piutang= str_replace(".", "", $this->input->post('Nilai_Piutang'));
		$pemb= str_replace(".", "", $this->input->post('Pembayaran'));
		$data = array(
			'IDPiutang' =>$urutan_id,
			'IDFaktur' =>$urutan_id,
			'IDCustomer' => $this->input->post('IDCustomer'),
			'Tanggal_Piutang' => $this->input->post('Tanggal_Piutang'),
			'Jatuh_Tempo' => $this->input->post('Jatuh_Tempo'),
			'No_Faktur' => $this->input->post('No_Faktur'),
			'Nilai_Piutang' => $piutang,
			'Saldo_Awal' => $piutang,
			'Pembayaran' => $pemb,
			'Saldo_Akhir' => $piutang-$pemb,
			'Jenis_Faktur' => $this->input->post('Jenis_Faktur')
		);

		$this->M_piutang->simpan($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('piutang/index');
		}elseif($simtam){
			redirect('piutang/tambah_piutang');
		}
	}
	public function edit($id)
	{
		$data['customer']=$this->M_piutang->get_customer();
		$data['piutang'] = $this->M_piutang->get_piutang_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/piutang/V_edit_piutang', $data);
	}
	public function update()
	{
		$piutang= str_replace(".", "", $this->input->post('Nilai_Piutang'));
		$pemb= str_replace(".", "", $this->input->post('Pembayaran'));
		$data = array(
			'IDCustomer' => $this->input->post('IDCustomer'),
			'Tanggal_Piutang' => $this->input->post('Tanggal_Piutang'),
			'Jatuh_Tempo' => $this->input->post('Jatuh_Tempo'),
			'No_Faktur' => $this->input->post('No_Faktur'),
			'Nilai_Piutang' => $piutang,
			'Pembayaran' => $pemb,
			'Saldo_Akhir' => $piutang-$pemb,
			'Jenis_Faktur' => $this->input->post('Jenis_Faktur')
		);
		echo $this->input->post('id');
		$this->M_piutang->update_piutang($this->input->post('id'),$data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
		redirect('piutang/index');
	}
	public function hapus_piutang($id)
	{

		$this->M_piutang->hapus_data_piutang($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
		redirect('piutang/index');
	}
	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$kolom= addslashes($this->input->post('jenispencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $keyword!="") {
			$data['piutang']=$this->M_piutang->cari_by_kolom($kolom,$keyword);
		}elseif ($keyword!="") {
			$data['piutang']=$this->M_piutang->cari_by_keyword($keyword);
		}else {
			$data['piutang']=$this->M_piutang->tampilkan_piutang();
		}
		$this->load->view('administrator/piutang/V_piutang', $data);
	}
}

?>