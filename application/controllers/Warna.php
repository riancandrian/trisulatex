
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Warna extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_warna');
		$this->load->model('M_login');
		$this->load->model('M_barang');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['warna']=$this->M_warna->tampilkan_warna();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/warna/V_warna', $data);
	}

	public function tambah_warna()
	{
		
		$data['corak']= $this->M_barang->tampil_corak();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/warna/V_tambah_warna', $data);
	}
	public function simpan()
	{
		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$data['id_warna']=$this->M_warna->tampilkan_id_warna();
		if($data['id_warna']!=""){
			// foreach ($data['id_warna'] as $value) {
				$urutan= substr($data['id_warna']->IDWarna, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$data = array(
			'IDWarna'=> $urutan_id,
			'Kode_Warna' => $this->input->post('Kode_Warna'),
			'Warna' => $this->input->post('Warna'),
			'IDCorak' => $this->input->post('corak'),
			'Aktif' => 'aktif'
		);

		$this->M_warna->simpan($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('warna/index');
		}elseif($simtam){
			redirect('warna/tambah_warna');
		}
	}
	public function edit($id)
	{
		$data['corak']= $this->M_barang->tampil_corak();
		$data['warna'] = $this->M_warna->get_warna_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/warna/V_edit_warna', $data);
	}
	public function update()
	{
		$data = array(
			'Kode_Warna' => $this->input->post('Kode_Warna'),
			'Warna' => $this->input->post('Warna'),
			'IDCorak' => $this->input->post('corak'),
		);
		echo $this->input->post('id');
		$this->M_warna->update_warna($this->input->post('id'),$data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
		redirect('warna/index');
	}
	public function hapus_warna($id)
	{

		$this->M_warna->hapus_data_warna($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
		redirect('warna/index');
	}
	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $keyword!="") {
			$data['warna']=$this->M_warna->cari_by_kolom($kolom,$keyword);
		}elseif ($keyword!="") {
			$data['warna']=$this->M_warna->cari_by_keyword($keyword);
		}else {
			$data['warna']=$this->M_warna->tampilkan_warna();
		}
		$this->load->view('administrator/warna/V_warna', $data);
	}
}

?>