<?php

class SampleKain extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_sample_kain');
        $this->load->model('M_agen');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
        $data = array(
            'title_master' => "Sample Kain", //Judul
            'title' => "Penjualan Sample Kain", //Judul Tabel
            'action' => site_url('SampleKain/create'), //Alamat Untuk Action Form
            'button' => "Tambah Data", //Nama Button
            'samples' => $this->M_sample_kain->all() //Load Data Surat Jalan Makloon Jahit
        );
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/sample_kain/V_index', $data);
    }

    public function create()
    {
        $this->load->model('M_supplier');

        $data['kodesample'] = $this->M_sample_kain->last_id(date('Y'));
        $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
        $data['supplier'] = $this->M_supplier->get_all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/sample_kain/V_create', $data);
    }

    public function getstok()
    {
        $barcode = $this->input->post('barcode');
        $data = $this->M_sample_kain->getstok($barcode);
        echo json_encode($data);
    }

    public function store()
    {
        $data1 = $this->input->post('_data1');
        $data2 = $this->input->post('_data2');
        $data['id_fjsp'] = $this->M_sample_kain->tampilkan_id_sample_kain();
        if($data['id_fjsp']!=""){
            foreach ($data['id_fjsp'] as $value) {
                $urutan= substr($value->IDFJSP, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
            $urutan_id = 'P000001';
        }
        $data = array(
            'IDFJSP' => $urutan_id,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['Nomor'],
            'IDCustomer' => $data1['IDCustomer'],
            'Keterangan' => $data1['keterangan']
        );

        $this->M_sample_kain->add($data);
        foreach($data2 as $row){
            $data['id_fjsp_detail'] = $this->M_sample_kain->tampilkan_id_sample_kain_detail();
            if($data['id_fjsp_detail']!=""){
                foreach ($data['id_fjsp_detail'] as $value) {
                    $urutan= substr($value->IDFJSPDetail, 1);
                }
                $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
            }else{
                $urutan_id_detail = 'P000001';
            }
            $detail = array(
                'IDFJSPDetail'=> $urutan_id_detail,
                'IDFJSP'=>$urutan_id,
                'Barcode' => $row['Barcode'],
                //'NoSO' => '',
                //'Party' => '',
                //'Indent' => '',
                'IDBarang' => $row['IDBarang'],
                'IDCorak' => $row['IDCorak'],
                'IDMerk' => $row['IDMerk'],
                'IDWarna' => $row['IDWarna'],
                'Qty_yard' => $row['Qty_jual'],
                'Qty_meter' => $row['Qty_meter_jual'],
                'Saldo_yard' => $row['Qty_jual'],
                'Saldo_meter' => $row['Qty_meter_jual'],
                'Grade' => $row['Grade'],
                //'Remark' => '',
                'IDSatuan' => $row['IDSatuan'],
                'Harga' => $row['Harga'],
                'Sub_total' => 0
            );
            $this->M_sample_kain->add_detail($detail);

            $stock = array(
                'Saldo_yard' => $row['Saldo_yard'] - $row['Qty_jual'],
                'Saldo_meter' => $row['Saldo_meter'] - $row['Qty_meter_jual']
            );
            $this->M_sample_kain->update_stock($stock, $row['Barcode']);

             //save jurnal
            $data['id_jurnal'] = $this->M_sample_kain->tampilkan_id_jurnal();
            if($data['id_jurnal']!=""){
              foreach ($data['id_jurnal'] as $value) {
                $urutan= substr($value->IDJurnal, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_debet = array(
          'IDJurnal'=>$urutan_id_jurnal,
          'Tanggal' => $data1['Tanggal'],
          'Nomor' => $data1['Nomor'],
          'IDFaktur' => $urutan_id,
          'IDFakturDetail' => $urutan_id_detail,
          'Jenis_faktur' => 'SK',
          'IDCOA' => 'P000417',
          'Debet' => $row['Harga'],
          'Kredit' => 0,
          'IDMataUang' => 1,
          'Kurs' => 14000,
          'Total_debet' => $row['Harga'],
          'Total_kredit' => 0,
          'Keterangan' => $data1['keterangan'],
          'Saldo' => $row['Harga'],
      );
      $this->M_sample_kain->save_jurnal($save_jurnal_debet);

      $data['id_jurnal'] = $this->M_sample_kain->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
          foreach ($data['id_jurnal'] as $value) {
            $urutan= substr($value->IDJurnal, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal_kredit= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit = 'P000001';
  }

  $save_jurnal_kredit = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'SK',
      'IDCOA' => 'P000016',
      'Debet' => 0,
      'Kredit' => $row['Harga'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => $row['Harga'],
      'Keterangan' => $data1['keterangan'],
      'Saldo' => $row['Harga'],
  );
  $this->M_sample_kain->save_jurnal($save_jurnal_kredit);
        }
        echo json_encode($data2);
    }

    public function show($id)
    {
        $nomor = '_';
        $data['samples'] = $this->M_sample_kain->find($id, $nomor);
        $data['samplesdetail'] = $this->M_sample_kain->find_detail($id, $nomor);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/sample_kain/V_show', $data);
    }

    public function status_gagal($id)
    {
        $data = array(
            'Batal' => 'tidak aktif',
        );

        $this->M_sample_kain->update_status($data, array('IDFJSP' => $id));
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
        redirect('SampleKain/index');
    }

    public function status_berhasil($id)
    {
        $data = array(
            'Batal' => 'aktif',
        );

        $this->M_sample_kain->update_status($data, array('IDFJSP' => $id));
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
        redirect('SampleKain/index');
    }

    public function delete_multiple()
    {
        $ID_att = $this->input->post('msg');
        if (empty($ID_att)) {
            $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
            redirect("SampleKain/index");
        }
        $result = array();
        foreach($ID_att AS $key => $val){
            $result[] = array(
                "IDFJSP" => $ID_att[$key],
                "Batal"  => 'tidak aktif'
            );
        }
        $this->db->update_batch('tbl_penjualan_sample', $result, 'IDFJSP');
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
        redirect("SampleKain/index");
    }

    function print_($nomor)
    {
        $id = '_';
        $data['print'] = $this->M_sample_kain->find($id, $nomor);
        $data['printdetail'] = $this->M_sample_kain->find_detail($id, $nomor);
        $this->load->view('administrator/sample_kain/V_print', $data);
    }

    function pencarian()
    {
        $data = array(
            'title_master' => "Sample Kain",
            'title' => "Penjualan Sample Kain",
            'action' => site_url('SampleKain/create'),
            'button' => "Tambah Data",
            'samples' => $this->M_sample_kain->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'))
        );
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();

        $this->load->view('administrator/sample_kain/V_index', $data);
    }
}