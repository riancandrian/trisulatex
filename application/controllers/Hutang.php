<?php

class Hutang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_hutang');
        $this->load->helper('form', 'url');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        $data['hutang'] = $this->M_hutang->tampilkan_hutang();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/hutang/V_hutang.php', $data);
    }

    public function tambah_hutang()
    {
        
        $data['suppliers'] = $this->M_hutang->supplier();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/hutang/V_tambah_hutang', $data);
    }

    public function simpan()
    {
        $data['id_hutang'] = $this->M_hutang->tampilkan_id_hutang();
        if($data['id_hutang']!=""){
            // foreach ($data['id_hutang'] as $value) {
              $urutan= substr($data['id_hutang']->IDHutang, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
      $simpan= $this->input->post('simpan');
      $simtam= $this->input->post('simtam');
      $nilai= str_replace(".", "", $this->input->post('nilai_hutang'));
      $pembayaran= str_replace(".", "", $this->input->post('pembayaran'));

      $data = array(
        'IDHutang' => $urutan_id,
        'IDFaktur' => $urutan_id,
        'Tanggal_Hutang' => $this->input->post('tanggal_hutang'),
        'Jatuh_Tempo' => $this->input->post('jatuh_tempo'),
        'No_Faktur' => $this->input->post('no_faktur'),
        'Nilai_Hutang' => $nilai,
        'Saldo_Awal' => $nilai,
        'Pembayaran' => $pembayaran,
        'Saldo_Akhir' =>  $nilai-$pembayaran,
        'IDSupplier' => $this->input->post('supplier'),
        'Jenis_Faktur' => 'SA',
    );

      $this->M_hutang->simpan($data);
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
      if($simpan){
        redirect('Hutang/index');
    }elseif($simtam){
        redirect('Hutang/tambah_hutang');
    }
}

public function edit($id)
{
    $data['suppliers'] = $this->M_hutang->supplier();
    $data['hutang'] = $this->M_hutang->getById($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/hutang/V_edit_hutang', $data);
}

public function update()
{
    $nilai= str_replace(".", "", $this->input->post('nilai_hutang'));
    $pembayaran= str_replace(".", "", $this->input->post('pembayaran'));

    $data = array(
        'Tanggal_Hutang' => $this->input->post('tanggal_hutang'),
        'Jatuh_Tempo' => $this->input->post('jatuh_tempo'),
        'No_Faktur' => $this->input->post('no_faktur'),
        'Nilai_Hutang' => $nilai,
        'Pembayaran' => $pembayaran,
        'Saldo_Akhir' =>  $nilai-$pembayaran,
        'IDSupplier' => $this->input->post('supplier'),
        'Jenis_Faktur' => 'FA',
    );

    $this->M_hutang->update($data, array('IDHutang' => $this->input->post('id')));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('Hutang/index');
}

public function show($id)
{
    $data['hutang'] = $this->M_hutang->getDetailId($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/hutang/V_show', $data);
}

public function delete($id)
{
    $this->M_hutang->delete($id);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil dihapus</div></center>");
    redirect('Hutang');
}

public function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();

    $kolom= addslashes($this->input->post('jenispencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $keyword!="") {
        $data['hutang']=$this->M_hutang->cari_by_kolom($kolom,$keyword);
    }elseif ($keyword!="") {
        $data['hutang']=$this->M_hutang->cari_by_keyword($keyword);
    }else {
        $data['hutang']=$this->M_hutang->tampilkan_hutang();
    }
    $this->load->view('administrator/hutang/V_hutang', $data);
}
}