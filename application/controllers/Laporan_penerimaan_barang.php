<?php

class Laporan_penerimaan_barang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_laporan_penerimaan');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        $data['PenerimaanBarang'] = $this->M_laporan_penerimaan->all('PB');
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_penerimaan_barang/V_index', $data);
    }

    public function index_pbnt()
    {
        $data['PenerimaanBarang'] = $this->M_laporan_penerimaan->all('PBU');
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_penerimaan_barang/V_index_pbnt', $data);
    }

    public function index_jahit()
    {
        $data['PenerimaanBarang'] = $this->M_laporan_penerimaan->all_jahit();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_penerimaan_barang/V_index_jahit', $data);
    }

    function pencarian()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        if ($this->input->post('date_from') == '') {
            $data['PenerimaanBarang'] = $this->M_laporan_penerimaan->searching_store_like($this->input->post('keyword'));
        } else {
            $data['PenerimaanBarang'] = $this->M_laporan_penerimaan->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
        }
        $this->load->view('administrator/laporan_penerimaan_barang/V_index', $data);
    }

    public function index_umum()
    {
        $data['PenerimaanBarangUmum'] = $this->M_laporan_penerimaan->all_umum();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_penerimaan_barang/V_index_umum', $data);
    }

    function pencarian_umum()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        if ($this->input->post('date_from') == '') {
            $data['PenerimaanBarangUmum'] = $this->M_laporan_penerimaan->searching_store_like_umum($this->input->post('keyword'));
        } else {
            $data['PenerimaanBarangUmum'] = $this->M_laporan_penerimaan->searching_store_umum($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
        }
        $this->load->view('administrator/laporan_penerimaan_barang/V_index_umum', $data);
    }

    function pencarian_jahit()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        if ($this->input->post('date_from') == '') {
            $data['PenerimaanBarang'] = $this->M_laporan_penerimaan->searching_store_like_jahit($this->input->post('keyword'));
        } else {
            $data['PenerimaanBarang'] = $this->M_laporan_penerimaan->searching_store_jahit($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
        }
        $this->load->view('administrator/laporan_penerimaan_barang/V_index_jahit', $data);
    }

    public function exporttoexcel_umum(){
      // create file name
        $fileName = 'PBU '.time().'.xlsx';  
    // load excel library
        $this->load->library('excel');
        $empInfo = $this->M_laporan_penerimaan->exsport_excel_umum();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nomor');  
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Supplier'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Barang'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Qty'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Satuan'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Harga Satuan'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Sub Total'); 
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['Tanggal']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['Nomor']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['Nama']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['Nama_Barang']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Qty']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['Satuan']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['Harga_satuan']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['Sub_total']);
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
    // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);        
    }
}