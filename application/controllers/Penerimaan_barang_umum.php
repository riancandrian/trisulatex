<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_barang_umum extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('M_penerimaan_barang_umum');
		$this->load->model('M_penerimaan_barang');
		$this->load->model('M_login');
		$this->load->model('M_agen');
		$this->load->helper('convert_function');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		$data['PenerimaanBU'] = $this->M_penerimaan_barang_umum->all();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/penerimaan_barang_umum/V_index', $data);
	}

	public function create() {
		$this->load->model('M_supplier');
		$this->load->model('M_corak');
		$this->load->model('M_warna');
		$this->load->model('M_satuan');

		$data['satuan'] = $this->M_satuan->get_yard_meter();
		$data['po'] = $this->M_penerimaan_barang_umum->get_all_po_umum();
		$data['warna'] = $this->M_warna->tampilkan_warna();
		$data['corak'] = $this->M_corak->all();
		$data['supplier'] = $this->M_supplier->get_all();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['last_number'] = $this->M_penerimaan_barang_umum->getCodeReception(date('Y'));
		$this->load->view('administrator/penerimaan_barang_umum/V_create', $data);
	}

	function cek_supplier() {
		$Nomor = $this->input->post('Nomor');
		$data = $this->M_penerimaan_barang_umum->ceksupplier($Nomor);
		echo json_encode($data);
	}

	function getColorByStyle() {
		$id_corak = $this->input->post('id_corak');
		if ($id_corak) {
			$return['data'] = $this->M_penerimaan_barang_umum->getColorByStyle($id_corak);
		} else {
			$return['data'] = '';
		}

		$return['error'] = false;
		echo json_encode($return);
	}

	function store() {
		if (!empty($this->input->post('data'))) {
			$data = $this->input->post('data');
		}

		if (!empty($this->input->post('data2'))) {
			$data2 = $this->input->post('data2');
		}

		$data['id_terima_barang'] = $this->M_penerimaan_barang->tampilkan_id_terima_barang();
		if ($data['id_terima_barang'] != "") {
			foreach ($data['id_terima_barang'] as $value) {
				$urutan = substr($value->IDTBS, 1);
			}
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id = 'P000001';
		}

		if ($this->M_penerimaan_barang_umum->check_noPB($data2['NoPB'])->num_rows() == 0) {
			$save = array(
				'IDTBS' => $urutan_id,
				'Tanggal' => $data2['Tanggal'],
				'Nomor' => $data2['NoPB'],
				'IDSupplier' => $data2['ID_Supplier'],
				'Nomor_sj' => $data2['NoSJ'],
				'IDPO' => $data2['NoPO'],
				'NoSO' => $data2['NoSO'] ? $data2['NoSO'] : '-',
				'Total_qty_yard' => $data2['Total_yard'],
				'Total_qty_meter' => $data2['Total_meter'],
				'Saldo_yard' => $data2['Total_yard'],
				'Saldo_meter' => $data2['Total_meter'],
				'Keterangan' => $data2['Keterangan'] ? $data2['Keterangan'] : null,
				'Jenis_TBS' => 'PBU',
				'Batal' => 'Aktif',
			);

			//save barang supplier
			$this->M_penerimaan_barang_umum->save($save);
		}
		foreach ($data as $val) {
			$data['id_terima_barang_detail'] = $this->M_penerimaan_barang->tampilkan_id_terima_barang_detail();
			if ($data['id_terima_barang_detail'] != "") {
				// foreach ($data['id_terima_barang_detail'] as $value) {
					$urutan = substr($data['id_terima_barang_detail']->IDTBSDetail, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail = 'P000001';
			}

			$save_detail = array(
				'IDTBSDetail' => $urutan_id_detail,
				'IDTBS' => $urutan_id,
				'Barcode' => $val['Barcode'],
				'NoSO' => $val['NoSO'],
				'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : 0,
				//'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : 0,
				'IDCorak' => $val['Corak'],
				'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : 0,
				'IDMerk' => $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk ? $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk : 0,
				'Qty_yard' => $val['Qty_yard'],
				'Qty_meter' => $val['Qty_meter'],
				'Saldo_yard' => $val['Qty_yard'],
				'Saldo_meter' => $val['Qty_meter'],
				'Grade' => $val['Grade'],
				'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
				'Harga' => $val['Harga'],
			);

			$this->M_penerimaan_barang_umum->save_detail($save_detail);

			$data['id_stok'] = $this->M_penerimaan_barang->tampilkan_id_stok();
			if ($data['id_stok'] != "") {
				// foreach ($data['id_stok'] as $value) {
					$urutan = substr($data['id_stok']->IDStok, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail_stok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail_stok = 'P000001';
			}

			$stok = array(
				'IDStok' => $urutan_id_detail_stok,
				'Tanggal' => $data2['Tanggal'],
				'Nomor_faktur' => $data2['NoPB'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'Jenis_faktur' => 'PBNT',
				'Barcode' => $val['Barcode'],
				'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : 0,
				'IDCorak' => $val['Corak'],
				'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : 0,
				'IDGudang' => 0,
				'Qty_yard' => $val['Qty_yard'],
				'Qty_meter' => $val['Qty_meter'],
				'Saldo_yard' => $val['Qty_yard'],
				'Saldo_meter' => $val['Qty_meter'],
				'Grade' => $val['Grade'],
				'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
				'Harga' => $val['Total_harga'],
				'IDMataUang' => 1,
				'Kurs' => '14000',
				'Total' => $val['Total_harga'],

			);

			$this->M_penerimaan_barang_umum->add_stok($stok);

			$data['id_kartu_stok'] = $this->M_penerimaan_barang->tampilkan_id_kartu_stok();
			if ($data['id_kartu_stok'] != "") {
				// foreach ($data['id_kartu_stok'] as $value) {
					$urutan = substr($data['id_kartu_stok']->IDKartuStok, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail_kartu_stok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail_kartu_stok = 'P000001';
			}
			$kartustok = array(
				'IDKartuStok' => $urutan_id_detail_kartu_stok,
				'Tanggal' => $data2['Tanggal'],
				'Nomor_faktur' => $data2['NoPB'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'IDStok' => $urutan_id_detail_stok,
				'Jenis_faktur' => 'PBNT',
				'Barcode' => $val['Barcode'],
				'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : 0,
				'IDCorak' => $val['Corak'],
				'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : 0,
				'IDGudang' => 0,
				'Masuk_yard' => $val['Qty_yard'],
				'Masuk_meter' => $val['Qty_meter'],
				'Keluar_yard' => 0,
				'Keluar_meter' => 0,
				'Grade' => $val['Grade'],
				'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
				'Harga' => $val['Total_harga'],
				'IDMataUang' => 1,
				'Kurs' => '14000',
				'Total' => $val['Total_harga'],

			);

			$this->M_penerimaan_barang_umum->add_kartu_stok($kartustok);

			//save jurnal
			$data['id_jurnal'] = $this->M_penerimaan_barang->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal = 'P000001';
			}

			$save_jurnal_debet = array(
				'IDJurnal' => $urutan_id_jurnal,
				'Tanggal' => $data2['Tanggal'],
				'Nomor' => $data2['NoPB'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'Jenis_faktur' => 'PBNT',
				'IDCOA' => 'P000018',
				'Debet' => str_replace(".", "", $val['Total_harga']),
				'Kredit' => 0,
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => str_replace(".", "", $val['Total_harga']),
				'Total_kredit' => 0,
				'Keterangan' => $val['Keterangan'] ? $val['Keterangan'] : $this->input->post('Keterangan'),
				'Saldo' => str_replace(".", "", $val['Total_harga']),
			);
			$this->M_penerimaan_barang->save_jurnal($save_jurnal_debet);

			$data['id_jurnal'] = $this->M_penerimaan_barang->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal_kredit = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal_kredit = 'P000001';
			}

			$save_jurnal_kredit = array(
				'IDJurnal' => $urutan_id_jurnal_kredit,
				'Tanggal' => $data2['Tanggal'],
				'Nomor' => $data2['NoPB'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'Jenis_faktur' => 'PBNT',
				'IDCOA' => 'P000021',
				'Debet' => 0,
				'Kredit' => $val['Total_harga'],
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => 0,
				'Total_kredit' => str_replace(".", "", $val['Total_harga']),
				'Keterangan' => $val['Keterangan'] ? $val['Keterangan'] : $this->input->post('Keterangan'),
				'Saldo' => str_replace(".", "", $val['Total_harga']),
			);
			$this->M_penerimaan_barang->save_jurnal($save_jurnal_kredit);
		}

	}

	function show($id) {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->M_penerimaan_barang_umum->find($id);
		// $data['detail'] = $this->M_penerimaan_barang_umum->findDetail($id);
		$data['pbudetail'] = $this->M_penerimaan_barang_umum->findDetail($id);
		$this->load->view('administrator/penerimaan_barang_umum/V_show', $data);
	}

	function soft_delete($id) {
		$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> <center>Data berhasil di-non-aktifkan</center></div>");
		$this->M_penerimaan_barang_umum->soft_delete($id);
		redirect(base_url('Penerimaan_barang_umum'));
	}

	function soft_success($id) {
		$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> <center>Data berhasil diaktifkan</center></div>");
		$this->M_penerimaan_barang_umum->soft_success($id);
		redirect(base_url('Penerimaan_barang_umum'));
	}

	function bulk() {
		if ($this->input->post('bulk') == "") {
			$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i><center> Tidak Ada Data Yang Dipilih</center></div>");
		} else {
			$this->M_penerimaan_barang_umum->bulk($this->input->post('bulk'));
			$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> <center>Data berhasil di-non-aktifkan</center></div>");
		}
		redirect(base_url('Penerimaan_barang_umum'));
	}

	function pencarian() {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['PenerimaanBU'] = $this->M_penerimaan_barang_umum->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
		$this->load->view('administrator/penerimaan_barang_umum/V_index', $data);
	}

	function edit($id) {
		$this->load->model('M_supplier');
		$this->load->model('M_corak');
		$this->load->model('M_warna');
		$this->load->model('M_satuan');

		$data['satuan'] = $this->M_satuan->get_all();
		$data['warna'] = $this->M_warna->tampilkan_warna();
		$data['corak'] = $this->M_corak->all();
		$data['supplier'] = $this->M_supplier->get_all();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->M_penerimaan_barang_umum->find($id);
		$data['detail'] = $this->M_penerimaan_barang_umum->findDetail($id);
		$this->load->view('administrator/penerimaan_barang_umum/V_edit', $data);
	}

	public function update() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		$data3 = $this->input->post('data3');

		$data = array(
			'Tanggal' => $data1['Tanggal'],
			'IDSupplier' => $data1['IDSupplier'],
			'Keterangan' => $data1['Keterangan'],
		);

		if ($this->M_penerimaan_barang_umum->update($data, $data1['IDTBS'])) {
			$data = null;
			if ($data2 > 0) {
				foreach ($data2 as $row) {
					$this->M_penerimaan_barang_umum->drop($row['IDTBSDetail'], $data1['IDTBS']);
				}
			}

			$tbs = $this->M_penerimaan_barang_umum->find($data1['IDTBS']);

			$total_yard = $tbs->Total_qty_yard;
			$total_meter = $tbs->Total_qty_meter;

			foreach ($data3 as $val) {

				//update table tbs
				$data_total = array(
					'Total_qty_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
					'Total_qty_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
					'Saldo_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
					'Saldo_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
				);
				$this->M_penerimaan_barang_umum->update($data_total, $data1['IDTBS']);

				//update barang supplier detail
				$save_detail = array(
					'Barcode' => $val['Barcode'],
					'NoSO' => $val['NoSO'] ? $val['NoSO'] : '-',
					'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : $val['IDBarang'],
					'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
					'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : $val['IDWarna'],
					'IDMerk' => $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk ? $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk : $val['IDMerk'],
					'Qty_yard' => $val['Qty_yard'],
					'Qty_meter' => $val['Qty_meter'],
					'Saldo_yard' => $val['Qty_yard'],
					'Saldo_meter' => $val['Qty_meter'],
					'Grade' => $val['Grade'],
					'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : $val['IDSatuan'],
					'Harga' => $val['Harga'],
				);

				$this->M_penerimaan_barang_umum->update_detail($save_detail, $val['IDTBSDetail']);

			}

			echo true;
		} else {
			echo false;
		}
	}

	public function updateChange() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		$data3 = $this->input->post('data3');

		$data = array(
			'Tanggal' => $data1['Tanggal'],
			'IDSupplier' => $data1['IDSupplier'],
			'Keterangan' => $data1['Keterangan'],
		);

		if ($this->M_penerimaan_barang_umum->update($data, $data1['IDTBS'])) {
			$data = null;
			//delete detail
			if ($data2 > 0) {
				foreach ($data2 as $row) {
					$this->M_penerimaan_barang_umum->drop($row['IDTBSDetail'], $data1['IDTBS']);
				}
			}

			$tbs = $this->M_penerimaan_barang_umum->find($data1['IDTBS']);

			$total_yard = 10;
			$total_meter = 10;

			//print_r($data3);
			foreach ($data3 as $val) {

				//jika data detail baru
				if ($val['IDTBSDetail'] == null || $val['IDTBSDetail'] == '') {
					//update table tbs
					$data_total = array(
						'Total_qty_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
						'Total_qty_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
						'Saldo_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
						'Saldo_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
					);
					$this->M_penerimaan_barang_umum->update($data_total, $data1['IDTBS']);

					$data['id_terima_barang_detail'] = $this->M_penerimaan_barang->tampilkan_id_terima_barang_detail();
					if ($data['id_terima_barang_detail'] != "") {
						foreach ($data['id_terima_barang_detail'] as $value) {
							$urutan = substr($value->IDTBSDetail, 1);
						}
						$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
						$urutan_id_detail = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
					} else {
						$urutan_id_detail = 'P000001';
					}

					//update barang supplier detail
					$save_detail = array(
						'IDTBSDetail' => $urutan_id_detail,
						'IDTBS' => $data1['IDTBS'],
						'Barcode' => $val['Barcode'],
						'NoSO' => $val['NoSO'],
						'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang_edit($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang_edit($val['Corak'])->IDBarang : $val['IDBarang'],
						'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
						'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna != null ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : $val['IDWarna'],
						'IDMerk' => $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk ? $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk : NULL,
						'Qty_yard' => $val['Qty_yard'],
						'Qty_meter' => $val['Qty_meter'],
						'Saldo_yard' => $val['Qty_yard'],
						'Saldo_meter' => $val['Qty_meter'],
						'Grade' => $val['Grade'],
						'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : $val['IDSatuan'],
						'Harga' => $val['Harga'],
					);

					$this->M_penerimaan_barang_umum->save_detail($save_detail);

					//stok
					$data['id_stok'] = $this->M_penerimaan_barang_umum->tampilkan_id_stok();
					if ($data['id_stok'] != "") {
						foreach ($data['id_stok'] as $value) {
							$urutan = substr($value->IDStok, 1);
						}
						$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
						$urutan_id_detail_stok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
					} else {
						$urutan_id_detail_stok = 'P000001';
					}
					$stok = array(
						'IDStok' => $urutan_id_detail_stok,
						'Tanggal' => $data1['Tanggal'],
						'Nomor_faktur' => $data1['NoBO'],
						'IDFaktur' => $data1['IDTBS'],
						'IDFakturDetail' => $urutan_id_detail,
						'Jenis_faktur' => 'SA',
						'Barcode' => $val['Barcode'],
						'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang_edit($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang_edit($val['Corak'])->IDBarang : 0,
						'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
						'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : 0,
						'IDGudang' => 0,
						'Qty_yard' => $val['Qty_yard'],
						'Qty_meter' => $val['Qty_meter'],
						'Saldo_yard' => $val['Qty_yard'],
						'Saldo_meter' => $val['Qty_meter'],
						'Grade' => $val['Grade'],
						'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
						'Harga' => $val['Harga'],
						'IDMataUang' => 1,
						'Kurs' => '14000',
						'Total' => 0,

					);

					$this->M_penerimaan_barang_umum->add_stok($stok);

					$data['id_kartu_stok'] = $this->M_penerimaan_barang_umum->tampilkan_id_kartu_stok();
					if ($data['id_kartu_stok'] != "") {
						foreach ($data['id_kartu_stok'] as $value) {
							$urutan = substr($value->IDKartuStok, 1);
						}
						$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
						$urutan_id_detail_kartu_stok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
					} else {
						$urutan_id_detail_kartu_stok = 'P000001';
					}
					$kartustok = array(
						'IDKartuStok' => $urutan_id_detail_kartu_stok,
						'Tanggal' => $data1['Tanggal'],
						'Nomor_faktur' => $data1['NoBO'],
						'IDFaktur' => $data1['IDTBS'],
						'IDFakturDetail' => $urutan_id_detail,
						'IDStok' => $urutan_id_detail_stok,
						'Jenis_faktur' => 'SA',
						'Barcode' => $val['Barcode'],
						'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang_edit($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang_edit($val['Corak'])->IDBarang : 0,
						'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
						'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : 0,
						'IDGudang' => 0,
						'Masuk_yard' => $val['Qty_yard'],
						'Masuk_meter' => $val['Qty_meter'],
						'Keluar_yard' => 0,
						'Keluar_meter' => 0,
						'Grade' => $val['Grade'],
						'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
						'Harga' => $val['Harga'],
						'IDMataUang' => 1,
						'Kurs' => '14000',
						'Total' => 0,

					);

					$this->M_penerimaan_barang_umum->add_kartu_stok($kartustok);
				}

				//list detail sudah ada
				if ($val['IDTBSDetail'] != null || $val['IDTBSDetail'] != '') {

					//update barang supplier detail
					$save_detail = array(
						'NoSO' => $val['NoSO'] ? $val['NoSO'] : '-',
						'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : $val['IDBarang'],
						'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
						'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna != null ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : $val['IDWarna'],
						'IDMerk' => $val['IDMerk'],
						'Qty_yard' => $val['Qty_yard'],
						'Qty_meter' => $val['Qty_meter'],
						'Saldo_yard' => $val['Qty_yard'],
						'Saldo_meter' => $val['Qty_meter'],
						'Grade' => $val['Grade'],
						'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : $val['IDSatuan'],
					);
					//print_r($save_detail);

					$this->M_penerimaan_barang_umum->update_detail($save_detail, $val['IDTBSDetail']);

					/*$data_total = array(
						                        'Total_qty_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
						                        'Total_qty_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
						                        'Saldo_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
						                        'Saldo_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
					*/
					//print_r($data_total);
					//$this->M_penerimaan_barang_umum->update($data_total, $data1['IDTBS']);
					$stok = array(
						'Tanggal' => $data1['Tanggal'],
						'Jenis_faktur' => 'SA',
						'Barcode' => $val['Barcode'],
						'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : $val['IDBarang'],
						'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
						'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna != null ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : $val['IDWarna'],
						'IDGudang' => 0,
						'Qty_yard' => $val['Qty_yard'],
						'Qty_meter' => $val['Qty_meter'],
						'Saldo_yard' => $val['Qty_yard'],
						'Saldo_meter' => $val['Qty_meter'],
						'Grade' => $val['Grade'],
						'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
						'Harga' => $val['Harga'],
						'IDMataUang' => 1,
						'Kurs' => '14000',
						'Total' => 0,

					);

					$this->M_penerimaan_barang_umum->update_stok($stok, $data1['NoBO'], $val['IDTBS'], $val['IDTBSDetail']);

					$kartustok = array(
						'Tanggal' => $data1['Tanggal'],
						'Jenis_faktur' => 'SA',
						'Barcode' => $val['Barcode'],
						'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : $val['IDBarang'],
						'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
						'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna != null ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : $val['IDWarna'],
						'IDGudang' => 0,
						'Masuk_yard' => $val['Qty_yard'],
						'Masuk_meter' => $val['Qty_meter'],
						'Keluar_yard' => 0,
						'Keluar_meter' => 0,
						'Grade' => $val['Grade'],
						'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
						'Harga' => $val['Harga'],
						'IDMataUang' => 1,
						'Kurs' => '14000',
						'Total' => 0,

					);

					$this->M_penerimaan_barang_umum->update_kartu_stok($kartustok, $data1['NoBO'], $val['IDTBS'], $val['IDTBSDetail']);
				}

			}
			//update total
			$this->M_penerimaan_barang_umum->update_total($data1['IDTBS']);

			//die();
			echo true;
		} else {
			echo false;
		}
	}

	public function get_satuan() {
		$data = $this->M_penerimaan_barang_umum->get_satuan();
		echo json_encode($data);
	}

	public function get_corak() {
		$data = $this->M_penerimaan_barang_umum->get_corak();
		echo json_encode($data);
	}

	public function get_corak_by_id() {
		$data = $this->M_penerimaan_barang_umum->get_corak_by_id($this->input->post('id'));
		echo json_encode($data);
	}

	public function get_warna() {
		$data = $this->M_penerimaan_barang_umum->get_warna();
		echo json_encode($data);
	}

	public function print_data() {
		$data['data'] = $this->M_penerimaan_barang_umum->getPrint();
		$id = $data['data']->IDTBS;
		$data['detail'] = $this->M_penerimaan_barang_umum->getDetail_tbs($id);
		$this->load->view('administrator/penerimaan_barang_umum/V_print', $data);
	}

	public function print_data_edit() {
		$sjc = $this->uri->segment('3');
		$data['data'] = $this->M_penerimaan_barang_umum->getPrint_edit($sjc);
		$id = $data['data']->IDTBS;
		$data['detail'] = $this->M_penerimaan_barang_umum->getDetail_tbs($id);
		$this->load->view('administrator/penerimaan_barang_umum/V_print', $data);
	}

}