<?php

class PenerimaanBarangJahit extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_penerimaan_barang_jahit');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
  }

  public function index()
  {
    $data['pb_jahit'] = $this->M_penerimaan_barang_jahit->tampilkan_pb_jahit();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/penerimaan_barang_jahit/V_penerimaan_barang_jahit.php', $data);
  }

  public function printdata($nomor)
  {

    $data['printdata'] = $this->M_penerimaan_barang_jahit->print_penerimaan_barang($nomor);
    $id= $data['printdata']->IDTBSH;
    $data['printdatadetail'] = $this->M_penerimaan_barang_jahit->print_penerimaan_barang_detail($id);
    $this->load->view('administrator/penerimaan_barang_jahit/V_print', $data);
  }

  function cek_supplier()
  {
    $Nomor = $this->input->post('Nomor');
    $data = $this->M_penerimaan_barang_jahit->ceksupplier($Nomor);
    echo json_encode($data);
  }
  
  function cek_surat()
  {
	$Nomor = $this->input->post('Nomor');
    $data = $this->M_penerimaan_barang_jahit->ceksurat($Nomor);
    echo json_encode($data);
  }

  function cek_barang()
  {
    $Nomor = $this->input->post('Nomor');
    $data = $this->M_penerimaan_barang_jahit->cekbarang($Nomor);
    echo json_encode($data);
  }

  public function tambah_penerimaan_barang_jahit()
  {
    // $data['id_terima_barang_jahit'] = $this->M_penerimaan_barang_jahit->tampilkan_id_terima_barang_jahit();
    $data['last_number'] = $this->M_penerimaan_barang_jahit->no_pb_jahit(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $data['nomakloonjahit'] = $this->M_penerimaan_barang_jahit->tampilkan_no_makloon();
    $data['supplier'] = $this->M_penerimaan_barang_jahit->tampil_supplier();
    $data['barang'] = $this->M_penerimaan_barang_jahit->tampil_barang();
    $data['merk'] = $this->M_penerimaan_barang_jahit->tampil_merk();
    $data['satuan'] = $this->M_penerimaan_barang_jahit->tampil_satuan();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/penerimaan_barang_jahit/V_tambah_penerimaan_barang_jahit', $data);
  }

  public function simpan_penerimaan_barang_jahit(){

    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');
    
    $data['id_terima_barang_jahit'] = $this->M_penerimaan_barang_jahit->tampilkan_id_terima_barang_jahit();
    if($data['id_terima_barang_jahit']!=""){
      // foreach ($data['id_terima_barang_jahit'] as $value) {
        $urutan= substr($data['id_terima_barang_jahit']->IDTBSH, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data = array(
      'IDTBSH' => $urutan_id,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDSupplier' => $data1['IDSupplier'],
      'IDSJH' => $this->M_penerimaan_barang_jahit->get_IDSJH($data1['IDSJH'])->IDSJH ? $this->M_penerimaan_barang_jahit->get_IDSJH($data1['IDSJH'])->IDSJH : 0,
      'Nomor_supplier' => $data1['Nomor_supplier'],
      'Total_qty' => $data1['Total_qty'],
      'Saldo' => $data1['Total_qty'],
      'Keterangan' => $data1['Keterangan'],
      'Batal' => 'aktif',
    );
    $this->M_penerimaan_barang_jahit->add($data);


    // if($this->M_penerimaan_barang_jahit->add($data)){
            // $last_insert_id = $this->db->insert_id(); 
    $data = null;
    foreach($data2 as $row){
     $data['id_terima_barang_jahit_detail'] = $this->M_penerimaan_barang_jahit->tampilkan_id_terima_barang_jahit_detail();
     if($data['id_terima_barang_jahit_detail']!=""){
      // foreach ($data['id_terima_barang_jahit_detail'] as $value) {
        $urutan= substr($data['id_terima_barang_jahit_detail']->IDTBSHDetail, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }

    $data = array(
      'IDTBSHDetail' => $urutan_id_detail,
      'IDTBSH' => $urutan_id,
      'IDBarang' => $row['IDBarang'],
      'IDMerk' => $row['IDMerk'],
      'Qty' => $row['Qty'],
      'Saldo' => $row['Qty'],
      'IDSatuan' => $row['IDSatuan'],
      'Harga' => $row['Harga'],
      'Total_harga' =>  $row['Total_harga'],
    );
    $this->M_penerimaan_barang_jahit->add_detail($data);

      //save jurnal
     $data['cekgroupsupplier'] = $this->M_penerimaan_barang_jahit->cek_group_supplier($data1['IDSupplier']);
    if ($data['cekgroupsupplier']->Group_Supplier == "PIHAK KETIGA") {
       $data['id_jurnal'] = $this->M_penerimaan_barang_jahit->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal = 'P000001';
    }

    $save_jurnal_debet_dagang = array(
      'IDJurnal'=>$urutan_id_jurnal,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'PBMJ',
      'IDCOA' => 'P000467',
      'Debet' =>  $row['Total_harga'],
      'Kredit' => 0,
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' =>  $row['Total_harga'],
      'Total_kredit' => 0,
      'Keterangan' => $data1['Keterangan'],
      'Saldo' =>  $row['Total_harga'],
    );
    $this->M_penerimaan_barang_jahit->save_jurnal($save_jurnal_debet_dagang);

    $data['id_jurnal'] = $this->M_penerimaan_barang_jahit->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_kredit_utang= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit_utang = 'P000001';
    }

    $save_jurnal_kredit_utang = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit_utang,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'PBMJ',
      'IDCOA' => 'P000015',
      'Debet' => 0,
      'Kredit' =>  $row['Total_harga'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' =>  $row['Total_harga'],
      'Keterangan' => $data1['Keterangan'],
      'Saldo' =>  $row['Total_harga'],
    );
    $this->M_penerimaan_barang_jahit->save_jurnal($save_jurnal_kredit_utang);

    $data['id_jurnal'] = $this->M_penerimaan_barang_jahit->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_kredit_wip= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit_wip = 'P000001';
    }

    $save_jurnal_kredit_wip = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit_wip,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'PBMJ',
      'IDCOA' => 'P000018',
      'Debet' => 0,
      'Kredit' =>  $row['Total_harga'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => $row['Total_harga'],
      'Keterangan' => $data1['Keterangan'],
      'Saldo' =>  $row['Total_harga'],
    );
    $this->M_penerimaan_barang_jahit->save_jurnal($save_jurnal_kredit_wip);
    }
   
  }
  echo json_encode($data);
 //  }else{
 //   echo false;
 // }

}

public function edit($id)
{
  $data['nomakloonjahit'] = $this->M_penerimaan_barang_jahit->tampilkan_no_makloon();
  $data['detail'] = $this->M_penerimaan_barang_jahit->detail_pb_jahit($id);
  $data['supplier'] = $this->M_penerimaan_barang_jahit->tampil_supplier();
  $data['barang'] = $this->M_penerimaan_barang_jahit->tampil_barang();
  $data['merk'] = $this->M_penerimaan_barang_jahit->tampil_merk();
  $data['satuan'] = $this->M_penerimaan_barang_jahit->tampil_satuan();
  $data['pbjahit'] = $this->M_penerimaan_barang_jahit->getById($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/penerimaan_barang_jahit/V_edit', $data);
}

public function ubah_pb_jahit()
{
  $data1 = $this->input->post('data1');
  $data2 = $this->input->post('data2');
  $data3 = $this->input->post('data3');
  
  $data = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDSupplier' => $data1['IDSupplier'],
    'IDSJH' => $this->M_penerimaan_barang_jahit->get_IDSJH($data1['IDSJH'])->IDSJH ? $this->M_penerimaan_barang_jahit->get_IDSJH($data1['IDSJH'])->IDSJH : 0,
    'Nomor_supplier' => $data1['Nomor_supplier'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo' => $data1['Total_qty'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => 'aktif',
  );

  if($this->M_penerimaan_barang_jahit->update_master($data1['IDTBSH'],$data)){
    $data = null;
    if($data3 > 0){
     foreach($data3 as $row){
       $this->M_penerimaan_barang_jahit->drop($row['IDTBSHDetail']);
     }
   }
   if($data2 != null){
    foreach($data2 as $row){
     $data['id_terima_barang_jahit_detail'] = $this->M_penerimaan_barang_jahit->tampilkan_id_terima_barang_jahit_detail();
     if($data['id_terima_barang_jahit_detail']!=""){
      // foreach ($data['id_terima_barang_jahit_detail'] as $value) {
        $urutan= substr($data['id_terima_barang_jahit_detail']->IDTBSHDetail, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }

    $data = array(
      'IDTBSHDetail' => $urutan_id_detail,
      'IDTBSH'=> $data1['IDTBSH'],
      'IDBarang' => $row['IDBarang'],
      'IDMerk' => $row['NamaMerk'],
      'Qty' => $row['Qty'],
      'Saldo' => $row['Qty'],
      'IDSatuan' => $row['NamaSatuan'],
      'Harga' => str_replace(".", "",$row['Harga']),
      'Total_harga' =>  str_replace(".", "",$row['Total_harga']),
    );

    if($row['IDTBSHDetail'] ==''){
      $this->M_penerimaan_barang_jahit->add_detail($data);

       //save jurnal
      $data['id_jurnal'] = $this->M_penerimaan_barang_jahit->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        // foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_debet_dagang = array(
        'IDJurnal'=>$urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $data1['IDTBSH'],
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'PBMJ',
        'IDCOA' => 'P000467',
        'Debet' =>  str_replace(".", "",$row['Total_harga']),
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' =>  str_replace(".", "",$row['Total_harga']),
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' =>  str_replace(".", "",$row['Total_harga']),
      );
      $this->M_penerimaan_barang_jahit->save_jurnal($save_jurnal_debet_dagang);

      $data['id_jurnal'] = $this->M_penerimaan_barang_jahit->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        // foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal_kredit_utang= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal_kredit_utang = 'P000001';
      }

      $save_jurnal_kredit_utang = array(
        'IDJurnal'=>$urutan_id_jurnal_kredit_utang,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $data1['IDTBSH'],
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'PBMJ',
        'IDCOA' => 'P000015',
        'Debet' => 0,
        'Kredit' =>  str_replace(".", "",$row['Total_harga']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => 0,
        'Total_kredit' =>  str_replace(".", "",$row['Total_harga']),
        'Keterangan' => $data1['Keterangan'],
        'Saldo' =>  str_replace(".", "",$row['Total_harga']),
      );
      $this->M_penerimaan_barang_jahit->save_jurnal($save_jurnal_kredit_utang);

      $data['cekgroupsupplier'] = $this->M_penerimaan_barang_jahit->cek_group_supplier($data1['IDSupplier']);
      if ($data['cekgroupsupplier']->Group_Supplier == "PIHAK BERELASI") {
        $groupsupplier = 'P000440';
      } elseif ($data['cekgroupsupplier']->Group_Supplier == "PIHAK KETIGA") {
        $groupsupplier = 'P000439';
      }

      $data['id_jurnal'] = $this->M_penerimaan_barang_jahit->tampilkan_id_jurnal();
      if($data['id_jurnal']!=""){
        // foreach ($data['id_jurnal'] as $value) {
          $urutan= substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal_kredit_wip= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_jurnal_kredit_wip = 'P000001';
      }

      $save_jurnal_kredit_wip = array(
        'IDJurnal'=>$urutan_id_jurnal_kredit_wip,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor'],
        'IDFaktur' => $data1['IDTBSH'],
        'IDFakturDetail' => $urutan_id_detail,
        'Jenis_faktur' => 'PBMJ',
        'IDCOA' => $groupsupplier,
        'Debet' => 0,
        'Kredit' =>  str_replace(".", "",$row['Total_harga']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => 0,
        'Total_kredit' => str_replace(".", "",$row['Total_harga']),
        'Keterangan' => $data1['Keterangan'],
        'Saldo' =>  str_replace(".", "",$row['Total_harga']),
      );
      $this->M_penerimaan_barang_jahit->save_jurnal($save_jurnal_kredit_wip);
    }else
    {
      $this->M_penerimaan_barang_jahit->update_detail($row['IDTBSHDetail'], $data);                       
    }
  }
}

echo true;

}else{
  echo false;
}
}

public function status_gagal($id)
{
  $data = array(
    'Batal' => 'tidak aktif',
  );

  $this->M_penerimaan_barang_jahit->update_status($data, array('IDTBSH' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('PenerimaanBarangJahit/index');
}

public function status_berhasil($id)
{
  $data = array(
    'Batal' => 'aktif',
  );

  $this->M_penerimaan_barang_jahit->update_status($data, array('IDTBSH' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('PenerimaanBarangJahit/index');
}

public function delete_multiple()
{
  $ID_att = $this->input->post('msg');
  $result = array();
  foreach($ID_att AS $key => $val){
   $result[] = array(
    "IDTBSH" => $ID_att[$key],
    "Batal"  => 'tidak aktif'
  );
 }
 $this->db->update_batch('tbl_terima_barang_supplier_jahit', $result, 'IDTBSH');
 redirect("PenerimaanBarangJahit/index");
}

public function pencarian()
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['pb_jahit'] = $this->M_penerimaan_barang_jahit->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/penerimaan_barang_jahit/V_penerimaan_barang_jahit', $data);
}

function show($id)
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['data'] = $this->M_penerimaan_barang_jahit->find($id);
  $data['pbjahitdetail'] = $this->M_penerimaan_barang_jahit->get_by_id_detail($id);
  $this->load->view('administrator/penerimaan_barang_jahit/V_show', $data);
}

public function get_barang()
{
  $data = $this->M_penerimaan_barang_jahit->get_barang();
  echo json_encode($data);
}

public function get_merk()
{
  $data = $this->M_penerimaan_barang_jahit->get_merk();
  echo json_encode($data);
}

public function get_satuan()
{
  $data = $this->M_penerimaan_barang_jahit->get_satuan();
  echo json_encode($data);
}

}