<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PO_Asset extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_po');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->model('M_poasset');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
    date_default_timezone_set('Asia/Jakarta');
  }
  
  public function index()
  {
    $data = array(
          'title_master' => "Master Purchase Order Asset", //Judul 
          'title' => "List Data Purchase Order Asset", //Judul Tabel
          'action' => site_url('PO_Asset/create'), //Alamat Untuk Action Form
          'button' => "Tambah po" //Nama Button
        );
    $data['po']=  $this->db->query('SELECT * FROM tbl_po_asset')->result();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/po_asset/V_po', $data);
  }

  public function show($id)
  {
       
    $cekid  = $this->uri->segment(3);
    //var_dump($cekid);
    $data['po']             =  $this->M_po->get_byid_asset($id);
    $data['podetail']       =  $this->db->query('SELECT tbl_po_asset_detail.*,
                                                        tbl_asset."Nama_Asset",
                                                        tbl_group_asset."Group_Asset"
                                                  FROM tbl_po_asset_detail, 
                                                        tbl_asset, 
                                                        tbl_group_asset,
                                                        tbl_po_asset
                                                  WHERE tbl_po_asset_detail."IDAsset"=tbl_asset."IDAsset" 
                                                  AND
                                                        tbl_asset."IDGroupAsset"=tbl_group_asset."IDGroupAsset"
                                                  AND
                                                        tbl_po_asset."IDPOAsset"=tbl_po_asset_detail."IDPOAsset"
                                                  AND
                                                        tbl_po_asset."IDPOAsset"=\''.$cekid.'\'
                                                  ')->result();
    $data['aksessetting']   = $this->M_login->aksessetting();
    $data['aksesmenu']      = $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/po_asset/V_show', $data);
  }

  public function create()
  {	
  	
    $this->load->model('M_corak');
    $this->load->model('M_supplier');

    
    $data['supplier'] = $this->M_supplier->get_all();
    $data['kodepo'] = $this->M_po->no_poasset('PO Asset',date('Y'));
  	$data['group'] = $this->db->get_where('tbl_group_asset')->result();
  	$data['asset'] = $this->db->get_where('tbl_asset')->result();
  	$data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/po_asset/V_tambah_po', $data);
  }

  function getAsset()
    {
        $IDGroupAsset = $this->input->post('IDGroupAsset');
        $data = $this->M_po->cek_group_barang_asset2($IDGroupAsset);
        echo json_encode($data);
  }

  public function simpan_po(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data['id_po'] = $this->M_po->tampilkan_id_po_asset();
  if($data['id_po']!=""){
    // foreach ($data['id_po'] as $value) {
      $urutan= substr($data['id_po']->IDPOAsset, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'POA'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'POA000001';
  }
  $data_atas = array(
    'IDPOAsset'     => $data1['IDPOAsset'],
    'Total_Qty'     => str_replace(".", "", $data1['Total_Qty']),
    'Keterangan'    => $data1['Keterangan'],
    'Tanggal'       => $data1['Tanggal'],
    'CID'           => $this->session->userdata('id'),
    'CTime'         => date("Y-m-d H:i:s"),//$data1['Tanggal'],
    'Status'        => 'aktif',
    'Jenis_PO'      => 'PO Asset',
    'Nomor'         => $data1['Nomor'],
    'IDSupplier'    => $data1['IDSupplier'],
    //'PPN'
    //'Status_PPN'
    //'Percen_Disc'
    //'Disc'
    'IDMataUang'    => '1',
    'Kurs'          => '14000',
    'Saldo_Qty'     => str_replace(".", "", $data1['Total_Qty']),
    'Grand_Total'   => str_replace(".", "", $data1['Grand_total']),
    'Batal'         => 'aktif',
    
  );
    $this->M_po->add_asset($data_atas);
  
    $data_bawah = null;
      foreach($data2 as $row){
        $data['id_po_detail'] = $this->M_po->tampilkan_id_po_asset_detail();
        if($data['id_po_detail']!=""){
          // foreach ($data['id_po_detail'] as $value) {
            $urutan= substr($data['id_po_detail']->IDPOAssetDetail, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail = 'P000001';
        }
        $data_bawah = array(
          'IDPOAssetDetail'    => $urutan_id_detail,
          'IDPOAsset'          => $data1['IDPOAsset'],
          'IDAsset'            => $row['Nama'],
          'Qty'                => $row['Qty'],
          'Saldo'              => $row['Saldo'],
          'Harga_Satuan'       => str_replace(".", "", $row['Harga']),
          'IDMataUang'         => '1',
          'Kurs'               => '14000',
          'Subtotal'           => str_replace(".", "", $row['Total']),          
        );
        $this->M_po->add_detail_asset($data_bawah);
      }


  
  //}
      // $this->M_po->add($data_atas);
  echo json_encode($data_atas);
  echo json_encode($data_bawah);
  redirect('po_asset/create');

}
public function set_edit($id)
  {
   $cek = $this->M_po->get_byid_asset($id);
   $cekuser = $this->session->userdata('id');
   die($cekuser);    
         if ($cek->Batal == "aktif") {
           $batal ="tidak aktif";
         } else {
           $batal = "aktif";
         }
         $data = array(
           'Batal' => $batal
         );

         //$this->M_po->update_po_asset($id,$data);
         
        $this->db->query('UPDATE tbl_po_asset 
              SET "Batal"=\''.$batal.'\' 
              WHERE "IDPOAsset" = \''.$id.'\'');
         
         $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('PO_Asset/index');
}

public function edit($id)
{
  $data['group'] = $this->db->get_where('tbl_group_asset')->result();
  $data['asset'] = $this->db->get_where('tbl_asset')->result();
  $data['datapo']           =  $this->M_po->get_byid_asset($id);
  $data['datapodetail']     =  $this->db->query('SELECT tbl_po_asset_detail.*,
                                                        tbl_asset."Nama_Asset",
                                                        tbl_group_asset."Group_Asset"
                                                  FROM tbl_po_asset_detail, 
                                                        tbl_asset, 
                                                        tbl_group_asset,
                                                        tbl_po_asset
                                                  WHERE tbl_po_asset_detail."IDAsset"=tbl_asset."IDAsset" 
                                                  AND
                                                        tbl_asset."IDGroupAsset"=tbl_group_asset."IDGroupAsset"
                                                  AND
                                                        tbl_po_asset."IDPOAsset"=tbl_po_asset_detail."IDPOAsset"
                                                  AND
                                                        tbl_po_asset."IDPOAsset"=\''.$id.'\'
                                                  ')->result();
  $data['trisula']          = $this->M_po->get_supplier();
  $data['aksessetting']     = $this->M_login->aksessetting();
  $data['aksesmenu']        = $this->M_login->aksesmenu();
  $data['aksesmenudetail']  = $this->M_login->aksesmenudetail();
  $this->load->view('administrator/po_asset/V_edit_po', $data);
}
public function ubah_po()
{
  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDPO = $this->input->post('_id');


  //======================Update Header PO
  $this->db->query('UPDATE tbl_po_asset 
                    SET "Total_Qty"     = '.$data1['Total_Qty'].',
                        "Keterangan"    = \''.$data1['Keterangan'].'\',
                        "IDSupplier"    = \''.$data1['IDSupplier'].'\',
                        "Saldo_Qty"     = \''.$data1['Saldo'].'\',
                        "Grand_Total"   = '.str_replace(".", "", $data1['Grand_total']).',
                        "MID"           = \''.$this->session->userdata('id').'\',
                        "MTime"         = \''.date("Y-m-d H:i:s").'\'
                    WHERE "IDPOAsset" = \''.$data1['IDPOAsset'].'\'');
  
  //======================Delete Dulu Detail PO
  $this->db->query('DELETE FROM tbl_po_asset_detail WHERE "IDPOAsset" = \''.$data1['IDPOAsset'].'\'');
  // //die("TEST");
  // //======================Insertin Data yang Baru ke Detail PO
  $data_bawah = null;
      foreach($data2 as $row){
        $data['id_po_detail'] = $this->M_po->tampilkan_id_po_asset_detail();
        if($data['id_po_detail']!=""){
          // foreach ($data['id_po_detail'] as $value) {
            $urutan= substr($data['id_po_detail']->IDPOAssetDetail, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail = 'P000001';
        }
        $data_bawah = array(
          "IDPOAssetDetail"    => $urutan_id_detail,
          "IDPOAsset"          => $data1['IDPOAsset'],
          "IDAsset"            => $row['IDAsset'],
          "Qty"                => $row['Qty'],
          "Saldo"              => $row['Saldo'],
          "Harga_Satuan"       => str_replace(".", "", $row['Harga_Satuan']),
          "IDMataUang"         => '1',
          "Kurs"               => '14000',
          "Subtotal"           => str_replace(".", "", $row['Subtotal']),          
        );
        // var_dump($data_bawah);
        // die("CIK");
        // $this->db->query('INSERT INTO tbl_po_asset_detail ("IDPOAssetDetail",
        //                                           "IDPOAsset", 
        //                                           "IDAsset", 
        //                                           "Qty",
        //                                           "Saldo",
        //                                           "Harga_Satuan",
        //                                           "IDMataUang",
        //                                           "Kurs",
        //                                           "Subtotal") 
        //           VALUES (\''.$urutan_id_detail.'\', 
        //                   \''.$data1['IDPOAsset'].'\', 
        //                   \''.$row['Nama'].'\', 
        //                   \''.$row['Qty'].'\',
        //                   \''.$row['Saldo'].'\',
        //                   '.str_replace(".", "", $row['Harga_satuan']).',
        //                   1,
        //                   14000,
        //                   '.str_replace(".", "", $row['Subtotal']).'

        //                 )'

        //         );
        $this->M_po->add_detail_asset($data_bawah);
        //die('TEST');
      } 
  echo json_encode($data_bawah);
  //die('ASUPP');
}
 

} 

/* End of file Po */
