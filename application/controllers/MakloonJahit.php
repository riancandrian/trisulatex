<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class MakloonJahit extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_makloonjahit', 'M');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
  }

  public function index()
  {
    $data = array(
      'title_master' => "Master Surat Jalan Makloon Jahit", //Judul 
      'title' => "List Data Surat Jalan Makloon Jahit", //Judul Tabel
      'action' => site_url('MakloonJahit/create'), //Alamat Untuk Action Form
      'button' => "Tambah Data", //Nama Button
      'makloon' => $this->M->get_all() //Load Data Surat Jalan Makloon Jahit
    );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/makloonjahit/V_makloonjahit', $data);
  }


  //----------------------Aksi Ubah Batal single
  public function set_edit($id)
  {
    $cek = $this->M->get_by_id($id);
    //die(print_r($cek));
    $batal = "-";
    if ($cek->Batal == "aktif") {
      $batal ="tidak aktif";
    } else {
      $batal = "aktif";
    }
    $data = array(
      'Batal' => $batal
    );

    $this->M->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('MakloonJahit/index');
  }

  //----------------------Aksi Ubah Batal multiple
  public function delete_multiple()
  {
    $ID_att = $this->input->post('msg');
    if (empty($ID_att)) {
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
      redirect("MakloonJahit/index");
    }
    $result = array();
    foreach($ID_att AS $key => $val){
      $result[] = array(
        "IDSJH" => $ID_att[$key],
        "Batal"  => 'tidak aktif'
      );
    }
    $this->db->update_batch('tbl_surat_jalan_makloon_jahit', $result, 'IDSJH');
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect("MakloonJahit/index");
  }

  //Tambah Data Surat Jalan Makloon Jahit
  public function create()
  {
    $this->load->model('M_supplier');
    //$this->load->model('M_po');
    
    
    $data['kodemakloon'] = $this->M->no_makloon(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $data['supplier'] = $this->M_supplier->get_all();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/makloonjahit/V_tambah_makloon', $data);
  }

  //Ajax get data stok
  public function getstok()
  {
    $barcode = $this->input->post('barcode');
    $data = $this->M->getstok($barcode);
    echo json_encode($data);
    
  }


 //SImpan Data Ajax
  public function simpan_makloon(){

    $data1 = $this->input->post('_data1');
    $data2 = $this->input->post('_data2');
    $data['id_makloon_jahit'] = $this->M->tampilkan_id_makloon_jahit();
    if($data['id_makloon_jahit']!=""){
      // foreach ($data['id_makloon_jahit'] as $value) {
        $urutan= substr($data['id_makloon_jahit']->IDSJH, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data_atas = array(
      'IDSJH' 			=> $urutan_id,
      'Tanggal' 		=> $data1['Tanggal'],
      'Nomor' 			=> $data1['Nomor'],
      'IDSupplier' 		=> $data1['IDSupplier'],
    //'IDPO' => $data1['IDPO'],
      'Total_qty_yard' 	=> $data1['Total_qty_yard'],
      'Total_qty_meter' => $data1['Total_qty_meter'],
      'Saldo_yard' 		=> $data1['Saldo_yard'],
      'Saldo_meter' 	=> $data1['Saldo_meter'],
      'Keterangan'		=> $data1['Keterangan'],
      'Batal' 			=> $data1['Batal'],
    );

    
    $last_insert_id = $this->M->add($data_atas); 
    $data_bawah = null;
    foreach($data2 as $row){
     $data['id_makloon_jahit_detail'] = $this->M->tampilkan_id_makloon_jahit_detail();
     if($data['id_makloon_jahit_detail']!=""){
      // foreach ($data['id_makloon_jahit_detail'] as $value) {
        $urutan= substr($data['id_makloon_jahit_detail']->IDSJHDetail, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }
    $data_bawah = array(
      'IDSJHDetail'=> $urutan_id_detail,
      'IDSJH'=>$urutan_id,
      'Barcode' => $row['Barcode'],
      'IDBarang' => $row['IDBarang'],
      'IDCorak' => $row['IDCorak'],
      'IDWarna' => $row['IDWarna'],
      'IDMerk' => $row['IDMerk'],
      'Qty_yard' => $row['Qty_yard'],
      'Qty_meter' => $row['Qty_meter'],
      'Saldo_yard' => $row['Saldo_yard'],
      'Saldo_meter' => $row['Saldo_meter'],
      'Grade' => $row['Grade'],
      'Lebar' => $row['Lebar'],
      'IDSatuan' => $row['IDSatuan'],
    );
    $this->M->add_detail($data_bawah);

    $data_stok = array(
      'Saldo_yard' => 0,
      'Saldo_meter' => 0,
    );
    $this->M->update_stok($row['Barcode'], $data_stok);

    //kartu stok
    $data['id_kartu_stok'] = $this->M->tampilkan_id_kartu_stok();
    if($data['id_kartu_stok']!=""){
      // foreach ($data['id_kartu_stok'] as $value) {
        $urutan= substr($data['id_kartu_stok']->IDKartuStok, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail_kartustok= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail_kartustok = 'P000001';
    }
    $save_stock_card = array(
      'IDKartuStok'=>$urutan_id_detail_kartustok,
      'Tanggal' => $data1['Tanggal'],
      'Nomor_faktur' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'IDStok' => '-',
      'Jenis_faktur' => 'MJ',
      'Barcode' => $row['Barcode'],
      'IDBarang' => $row['IDBarang'],
      'IDCorak' => $row['IDCorak'],
      'IDWarna' => $row['IDWarna'],
      'IDGudang' => 0,
      'Masuk_yard' => 0,
      'Masuk_meter' => 0,
      'Keluar_yard' => $row['Qty_yard'],
      'Keluar_meter' => $row['Qty_meter'],
      'Grade' => $row['Grade'],
      'IDSatuan' => $row['IDSatuan'],
      'Harga' => 0,
      'IDMataUang' => 1,
      'Kurs' => '',
      'Total' => 0
    );

    $this->M->save_stock_card($save_stock_card);

      //save jurnal
    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal = 'P000001';
    }

    $save_jurnal_debet = array(
      'IDJurnal'=>$urutan_id_jurnal,
      'Tanggal' =>  $data1['Tanggal'],
      'Nomor' =>  $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'SJMJ',
      'IDCOA' => 'P000017',
      'Debet' => $row['Harga'],
      'Kredit' => 0,
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => $row['Harga'],
      'Total_kredit' => 0,
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => 0,
    );
    $this->M->save_jurnal($save_jurnal_debet);

    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_kredit= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit = 'P000001';
    }

    $save_jurnal_kredit = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit,
      'Tanggal' =>  $data1['Tanggal'],
      'Nomor' =>  $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'SJMJ',
      'IDCOA' => 'P000018',
      'Debet' => 0,
      'Kredit' =>$row['Harga'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => $row['Harga'],
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => 0,
    );
    $this->M->save_jurnal($save_jurnal_kredit);

  }

  //$this->M->add($data_atas);
  echo json_encode($data_bawah);

}


  //----------------------Detail per data
public function show($id)
{
  $data['makloon'] = $this->M->get_by_id($id);
  $data['makloondetail'] = $this->M->get_by_id_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/makloonjahit/V_show', $data);
}

  //Ubah data Makloon
public function edit($id)
{
  $this->load->model('M_supplier');
    //$this->load->model('M_po');

  $data['datamakloon'] = $this->M->get_by_id2($id);
  $data['datamakloondetail'] = $this->M->get_makloon_detail($id);
    // $data['kodemakloon'] = $this->M->no_makloon();
    //$data['po'] = $this->M_po->get_all('PO Celup');
  $data['supplier'] = $this->M_supplier->get_all();
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/makloonjahit/V_edit_makloon', $data);
}

  //aksi Ubah Makloon
public function ubah_makloon(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDSJH = $this->input->post('_id');
  
  $data_atas = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDSupplier' => $data1['IDSupplier'],
      //'IDSJH' => $IDSJH,
    'Total_qty_yard' => $data1['Total_qty_yard'],
    'Total_qty_meter' => $data1['Total_qty_meter'],
    'Saldo_yard' => $data1['Saldo_yard'],
    'Saldo_meter' => $data1['Saldo_meter'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );


  if($this->M->update_master($IDSJH,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M->drop($row['IDSJHDetail']);
                // console.log('delete temp');
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
       $data['id_makloon_jahit_detail'] = $this->M->tampilkan_id_makloon_jahit_detail();
       if($data['id_makloon_jahit_detail']!=""){
        // foreach ($data['id_makloon_jahit_detail'] as $value) {
          $urutan= substr($data['id_makloon_jahit_detail']->IDSJHDetail, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_detail = 'P000001';
      }
      $data_atas = array(
        'IDSJHDetail' => $urutan_id_detail,
        'IDSJH'=> $IDSJH,
        'Barcode' => $row['Barcode'],
        'IDBarang' => $row['IDBarang'],
        'IDCorak' => $row['IDCorak'],
        'IDWarna' => $row['IDWarna'],
        'IDMerk' => $row['IDMerk'],
        'Qty_yard' => $row['Qty_yard'],
        'Qty_meter' => $row['Qty_meter'],
        'Saldo_yard' => $row['Saldo_yard'],
        'Saldo_meter' => $row['Saldo_meter'],
        'Grade' => $row['Grade'],
        'Lebar' => $row['Lebar'],
        'IDSatuan' => $row['IDSatuan'],
      );
      if($row['IDSJHDetail'] == ''){
        $this->M->add_detail($data_atas);

        //stok
        $data_stok = array(
          'Saldo_yard' => 0,
          'Saldo_meter' => 0,
        );
        $this->M->update_stok($row['Barcode'], $data_stok);

        //kartu stok
        $data['id_kartu_stok'] = $this->M->tampilkan_id_kartu_stok();
        if($data['id_kartu_stok']!=""){
          // foreach ($data['id_kartu_stok'] as $value) {
            $urutan= substr($data['id_kartu_stok']->IDKartuStok, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail_kartustok= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail_kartustok = 'P000001';
        }
        $save_stock_card = array(
          'IDKartuStok'=>$urutan_id_detail_kartustok,
          'Tanggal' => $data1['Tanggal'],
          'Nomor_faktur' => $data1['Nomor'],
          'IDFaktur' => $IDSJH,
          'IDFakturDetail' => $urutan_id_detail,
          'IDStok' => '-',
          'Jenis_faktur' => 'MJ',
          'Barcode' => $row['Barcode'],
          'IDBarang' => $row['IDBarang'],
          'IDCorak' => $row['IDCorak'],
          'IDWarna' => $row['IDWarna'],
          'IDGudang' => 0,
          'Masuk_yard' => 0,
          'Masuk_meter' => 0,
          'Keluar_yard' => $row['Qty_yard'],
          'Keluar_meter' => $row['Qty_meter'],
          'Grade' => $row['Grade'],
          'IDSatuan' => $row['IDSatuan'],
          'Harga' => 0,
          'IDMataUang' => 1,
          'Kurs' => '',
          'Total' => 0
        );

        $this->M->save_stock_card($save_stock_card);

              //save jurnal
    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal = 'P000001';
    }

    $save_jurnal_debet = array(
      'IDJurnal'=>$urutan_id_jurnal,
      'Tanggal' =>  $data1['Tanggal'],
      'Nomor' =>  $data1['Nomor'],
      'IDFaktur' => $IDSJH,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'SJMJ',
      'IDCOA' => 'P000015',
      'Debet' => $row['Harga'],
      'Kredit' => 0,
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => $row['Harga'],
      'Total_kredit' => 0,
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => 0,
    );
    $this->M->save_jurnal($save_jurnal_debet);

    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_kredit= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit = 'P000001';
    }

    $save_jurnal_kredit = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit,
      'Tanggal' =>  $data1['Tanggal'],
      'Nomor' =>  $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'SJMJ',
      'IDCOA' => 'P000016',
      'Debet' => 0,
      'Kredit' =>$row['Harga'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => $row['Harga'],
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => 0,
    );
    $this->M->save_jurnal($save_jurnal_kredit);
      }
    }
  }
  echo true;
}else{
  echo false;  
      // echo json_encode($data_atas);
}

      //$this->M->update_master($IDSJH,$data_atas);
      //echo json_encode($data3);

}

  //--------------------------------pencarian
function pencarian()
{
  $data = array(
      'title_master' => "Master Surat Jalan Makloon Jahit", //Judul 
      'title' => "List Data Surat Jalan Makloon Jahit", //Judul Tabel
      'action' => site_url('MakloonJahit/create'), //Alamat Untuk Action Form
      'button' => "Tambah Data", //Nama Button
      //'makloon' => $this->M->get_all() //Load Data Surat Jalan Makloon Jahit
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['makloon'] = $this->M->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/makloonjahit/V_makloonjahit', $data);
}

function printc($nomor)
{

  $data['print'] = $this->M->print_makloon_jahit($nomor);
  $id= $data['print']->IDSJH;
  $data['printdetail'] = $this->M->print_makloon_jahit_detail($id);
  $this->load->view('administrator/makloonjahit/V_print', $data);
}

}

/* End of file MakloonJahit.php */
