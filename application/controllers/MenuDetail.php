<!-- Programmer : Rais Naufal Hawari
     Date       : 12-07-2018 -->

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MenuDetail extends CI_Controller {

  function __construct()
	{
		parent::__construct();
		$this->load->model('M_menudetail');
    $this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
      $data = array(
        'title_master' => "Data Menu Detail", //Judul 
        'title' => "List Menu Detail", //Judul Tabel
        'action' => site_url('MenuDetail/create'), //Alamat Untuk Action Form
        'button' => "Tambah Menu Detail", //Nama Button
        'MenuDetail' => $this->M_menudetail->get_all() //Load data MenuDetail
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/menu_detail/menudetail_list.php', $data);
  }
  //Tambah Data MenuDetail
  public function create()
	{
      $this->load->model('M_Menu'); //Kebutuhan untuk Load Model Menu
    
      $data = array(
        'title_master' => "Data Menu Detail", //Judul 
        'title' => "Tambah Data MenuDetail" , //Judul Tabel
        'action' => site_url('MenuDetail/create_action'), //Alamat Untuk Action Form
        'action_back' => site_url('MenuDetail'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'IDMenu' => set_value('IDMenu'),
        'IDMenuDetail' => set_value('IDMenuDetail'), //Id 
        'MenuDetail' => set_value('MenuDetail'), //MenuDetail
        'Url' => set_value('Url'),
        'Urutan' => set_value('Urutan'),
        'data_menu' => $this->M_Menu->get_all() //Load Data Menu
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/menu_detail/menudetail_form.php', $data);
     
  }
  
  //Tambah Data MenuDetail
  public function create_action()
	{
      $data = array(
        'IDMenu' => $this->input->post('IDMenu'),
        'Menu_Detail' => $this->input->post('MenuDetail'),
        'Url' => $this->input->post('Url'),
        'Urutan' => $this->input->post('Urutan')
      );
      $this->M_menudetail->insert($data);
       $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
     redirect('MenuDetail/create');
  }
  
  //Edit Data MenuDetail
  public function update($id)
  {
    $this->load->model('M_menu'); //Kebutuhan untuk Load Model Menu
    $cari = $this->M_menudetail->get_by_id($id);//Cari apakah data yang dimaksud ada di database
    if($cari) {
      $data = array('title_master' => "Data Menu Detail", //Judul 
      'title' => "Ubah Data MenuDetail" , //Judul Tabel
        'action' => site_url('MenuDetail/update_action'), //Alamat Untuk Action Form
        'action_back' => site_url('MenuDetail'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'IDMenu' => set_value('IDMenu', $cari->IDMenu),
        'IDMenuDetail' => set_value('IDMenuDetail', $cari->IDMenuDetail), //Id 
        'MenuDetail' => set_value('MenuDetail', $cari->Menu_Detail), //MenuDetail
        'Url' => set_value('Url', $cari->Url),
        'Urutan' => set_value('Urutan', $cari->Urutan),
        'data_menu' => $this->M_menu->get_all() //Load Data Menu 
      );
      $data['aksessetting']= $this->M_login->aksessetting();
     $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/menu_detail/menudetail_form.php', $data);

    } else {
      redirect('MenuDetail/index');
    }
  }

  //Aksi Ubah Data
  public function update_action()
  {

    $id = $this->input->post('IDMenuDetail'); //Get id sebagai penanda record yang akan dirubah
    $data = array(
      'IDMenu' => $this->input->post('IDMenu'),
      'Menu_Detail' => $this->input->post('MenuDetail'),
      'Url' => $this->input->post('Url'),
      'Urutan' => $this->input->post('Urutan') 
    );
    $this->M_menudetail->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
    redirect('MenuDetail/index');
  }

  //Hapus Data MenuDetail
  public function delete($id)
  {
      $cari = $this->M_menudetail->get_by_id($id); //Cari apakah data yang dimaksud ada di database

      if ($cari) {
        $this->M_menudetail->delete($id);
         $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
        redirect('MenuDetail/index');
      } else {
         $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Gagal di Hapus</div></center>");
        redirect('MenuDetail/index');
      }
      
  }

  public function pencarian()
  {
    $kolom= addslashes($this->input->post('jenispencarian'));
    $keyword= addslashes($this->input->post('keyword'));
 $data = array(
        'title_master' => "Master Menu Detail", //Judul 
        'title' => "List Data Menu Detail", //Judul Tabel
        'action' => site_url('MenuDetail/create'), //Alamat Untuk Action Form
        'button' => "Tambah Menu Detail", //Nama Button
      );
 $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
 

    if ($kolom!="" && $keyword!="") {
      $data['MenuDetail']=$this->M_menudetail->cari_by_kolom($kolom,$keyword);
    }elseif ($keyword!="") {
      $data['MenuDetail']=$this->M_menudetail->cari_by_keyword($keyword);
    }else {
      $data['MenuDetail']=$this->M_menudetail->get_all();
    }
   
      $this->load->view('administrator/menu_detail/menudetail_list.php', $data);

  }

}

/* End of file MenuDetail.php */
