<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class InstruksiPengiriman extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_supplier');
		$this->load->model('M_login');
		$this->load->model('M_agen');
		$this->load->model('M_instruksi_pengiriman');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['kategori'] = '';
		$data['keyword'] = '';
		$data['daftar_instruksi'] = $this->M_instruksi_pengiriman->semua_instruksi();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/instruksi/index',$data);
	}


	 //--------------------Store Laporan PO
	public function laporan_instruksi()
	{
		$data = array(
			 'title_master' => "Laporan Instruksi Pengiriman", //Judul 
			 'title' => "Laporan Instruksi Pengiriman", //Judul Tabel
			 'instruksi' => $this->M_instruksi_pengiriman->get_all_procedure() //Load Data Instruksi Pengiriman
			);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/instruksi/V_laporan_instruksi', $data);
	}

	public function input_instruksi(){
		
		$data['kodenoins'] = $this->M_instruksi_pengiriman->no_instruksi(date('Y'));
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['data_supplier'] = $this->M_supplier->get_trisula();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['daftarsjc'] = $this->M_instruksi_pengiriman->getAll_sjc();
		$this->load->view('administrator/instruksi/tambah_instruksi', $data);
	}
	public function cari_sjc(){

		$data['dataPOSO'] = $this->M_instruksi_pengiriman->getID_sjc($this->input->post('id_sjc'));
		$data['dataTabel'] = $this->M_instruksi_pengiriman->getID_sjc_tabel($this->input->post('id_sjc'));
		echo json_encode($data);
	}
	public function store(){
		$data['id_instruksi'] = $this->M_instruksi_pengiriman->tampilkan_id_instruksi();
		if($data['id_instruksi']!=""){
			// foreach ($data['id_instruksi'] as $value) {
				$urutan= substr($data['id_instruksi']->IDIP, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$data = array(
			'IDIP' => $urutan_id,
			'Tanggal' => $this->input->post('tanggal'),
			'Nomor' => $this->input->post('no_instruksi'),
			'IDSupplier' => $this->input->post('supplier'),
			'Nomor_sj' => $this->input->post('in_id_sjc'),
			'IDPO' => $this->M_instruksi_pengiriman->get_IDPO($this->input->post('no_po'))->IDPO ? $this->M_instruksi_pengiriman->get_IDPO($this->input->post('no_po'))->IDPO : 0,
			'NoSO' => $this->input->post('no_so'),
			'Tanggal_kirim' => $this->input->post('tanggal_kirim'),
			'Total_yard' => $this->input->post('total_yard'),
			'Total_meter' => $this->input->post('total_meter'),
			'Keterangan' => $this->input->post('keterangan'),
			'Batal' => 'Aktif'
		);
		$this->M_instruksi_pengiriman->tambah_instruksi($data);

		$last_id = $urutan_id;
		foreach ($this->input->post('IDT') as $key) {
			$data['id_instruksi_detail'] = $this->M_instruksi_pengiriman->tampilkan_id_instruksi_detail();
			if($data['id_instruksi_detail']!=""){
				// foreach ($data['id_instruksi_detail'] as $value) {
					$urutan= substr($data['id_instruksi_detail']->IDIPDetail, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
			}else{
				$urutan_id_detail = 'P000001';
			}
			$this->M_instruksi_pengiriman->pindah_tampungan_detail_instruksi($key,$last_id, $urutan_id_detail);
		}
		$lempar['id_print'] = $last_id;
		$lempar['pesan'] = "sukses";
		echo json_encode($lempar);
		$this->session->set_flashdata("alert", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		// $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		// redirect('InstruksiPengiriman');
	}
	public function hapus($id){
		// $this->M_instruksi_pengiriman->delete($id);
		$data = array(
			'Batal' => 'Tidak Aktif'
		);
		$this->M_instruksi_pengiriman->update($data, array('IDIP' => $id));
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
		redirect('InstruksiPengiriman');
	}
	public function aktif($id){
		// $this->M_instruksi_pengiriman->delete($id);
		$data = array(
			'Batal' => 'Aktif'
		);
		$this->M_instruksi_pengiriman->update($data, array('IDIP' => $id));
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
		redirect('InstruksiPengiriman');
	}
	public function batal(){
		foreach ($this->input->post('do') as $key) {
			$data = array(
				'Batal' => 'Tidak Aktif'
			);
			$this->M_instruksi_pengiriman->update($data, array('IDIP' => $key));
		}
		echo "<pre>";
		print_r ($_POST);
		echo "</pre>";
		$this->M_instruksi_pengiriman->delete($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
		redirect('InstruksiPengiriman');
	}
	public function filter(){
		$data['daftar_instruksi'] = $this->M_instruksi_pengiriman->filter_instruksi($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['kategori'] = $this->input->post('jenispencarian');
		$data['keyword'] = $this->input->post('keyword');
		$this->load->view('administrator/instruksi/index',$data);
	}
	public function edit($id){
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['data_supplier'] = $this->M_supplier->get_all();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['daftarsjc'] = $this->M_instruksi_pengiriman->getAll_sjc();
		$data['instruksi'] = $this->M_instruksi_pengiriman->edit_instruksi($id);
		$data['data_tabel'] = $this->M_instruksi_pengiriman->edit_instruksi_tabel($id);
		$this->load->view('administrator/instruksi/edit_instruksi',$data);
	}
	public function update($id){
		$data = array(
			'Tanggal' => $this->input->post('tanggal'),
			'Nomor' => $this->input->post('no_instruksi'),
			'IDSupplier' => $this->input->post('supplier'),
			'Nomor_sj' => $this->input->post('in_id_sjc'),
			'IDPO' => $this->input->post('no_po'),
			'NoSO' => $this->input->post('no_so'),
			'Tanggal_kirim' => $this->input->post('tanggal_kirim'),
			'Total_yard' => $this->input->post('total_yard'),
			'Total_meter' => $this->input->post('total_meter'),
			'Keterangan' => $this->input->post('keterangan')
		);
		$this->M_instruksi_pengiriman->update($data, array('IDIP' => $id));
		if ($this->input->post('hapusIDIPDetail')!=null) {
			foreach ($this->input->post('hapusIDIPDetail') as $key) {
				$this->M_instruksi_pengiriman->delete_detail($key);
			}
			echo "hapusIDIPDetail";
		}
		if ($this->input->post('IDT')) {
			$this->M_instruksi_pengiriman->delete_detail_IDIP($id);
			foreach ($this->input->post('IDT') as $key) {
				$this->M_instruksi_pengiriman->pindah_tampungan_detail_instruksi($key,$id);
			}
			echo "pake idt";
		}
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		redirect('InstruksiPengiriman');
	}
	public function detail($id){
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['data_supplier'] = $this->M_supplier->get_all();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['daftarsjc'] = $this->M_instruksi_pengiriman->getAll_sjc();
		$data['instruksi'] = $this->M_instruksi_pengiriman->edit_instruksi($id);
		$data['data_tabel'] = $this->M_instruksi_pengiriman->edit_instruksi_tabel($id);
		$data['data_tabel_detail'] = $this->M_instruksi_pengiriman->edit_instruksi_tabel_detail($id);
		$this->load->view('administrator/instruksi/detail_instruksi',$data);
	}
	public function print($id){
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['data_supplier'] = $this->M_supplier->get_all();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['daftarsjc'] = $this->M_instruksi_pengiriman->getAll_sjc();
		$data['instruksi'] = $this->M_instruksi_pengiriman->edit_instruksi($id);
		$data['data_tabel'] = $this->M_instruksi_pengiriman->edit_instruksi_tabel($id);
		echo "<pre>";
		print_r ($data);
		echo "</pre>";
	}

	 //------------------------------------Pencarian Store
	function pencarian_store()
	{
		$data = array(
				'title_master' => "Laporan Instruksi Pengiriman", //Judul 
				'title' => "Laporan Instruksi Pengiriman", //Judul Tabel
				//'po' => $this->M_po->get_all('PO TTI') //Load Data Instruksi Pengiriman
			);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		if ($this->input->post('date_from') == '') {
			$data['instruksi'] = $this->M_instruksi_pengiriman->searching_store_like($this->input->post('keyword'));
		} else {
			$data['instruksi'] = $this->M_instruksi_pengiriman->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
		}
		$this->load->view('administrator/instruksi/V_laporan_instruksi', $data);
	}

	public function exporttoexcel(){
      // create file name
        $fileName = 'IPB '.time().'.xlsx';  
    // load excel library
        $this->load->library('excel');
        $empInfo = $this->M_instruksi_pengiriman->exsport_excel();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nomor');  
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Supplier'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'No SJC'); 
         $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Barcode'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'NoSO'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Party'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Barang'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Corak'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Merk'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Warna'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Qty Yard'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Qty Meter'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Grade'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Remark'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Lebar'); 
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['Tanggal']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['Nomor']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['Nama']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['Nomor_sj']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Barcode']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['NoSO']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['Party']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['Nama_Barang']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['Corak']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['Merk']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['Warna']);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['Saldo_yard']);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['Saldo_meter']);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['Grade']);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['Remark']);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['Lebar']);
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
    // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);        
    }
}
?>