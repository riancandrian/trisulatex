<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BookingOrder extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_booking_order');
        $this->load->model('M_agen');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        $data['booking_order'] = $this->M_booking_order->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/booking_order/V_booking_order', $data);
    }

    public function create()
    {
        $this->load->model('M_corak');
        
        $data['last_number'] = $this->M_booking_order->getCodeBooking(date('Y'));
        $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
        $data['corak'] = $this->M_corak->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/booking_order/V_tambah_booking_order', $data);
    }

    function store()
    {
        $data['id_booking_order'] = $this->M_booking_order->tampilkan_id_booking_order();
        if($data['id_booking_order']!=""){
            foreach ($data['id_booking_order'] as $value) {
              $urutan= substr($value->IDBO, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
      $data = array(
        'IDBO' => $urutan_id,
        'Tanggal' => $this->input->post('tanggal'),
        'Nomor' => $this->input->post('no_bo'),
        'Tanggal_selesai' => $this->input->post('tanggal_selesai'),
        'IDCorak' => $this->input->post('id_corak'),
        'IDMataUang' => 0,
        'Qty' => $this->input->post('qty'),
        'Saldo' => $this->input->post('qty'),
        'Keterangan' => $this->input->post('Keterangan'),
        'Batal' => 'Aktif'
    );
      $this->session->set_flashdata("alert", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
      $this->M_booking_order->save($data);
  }

  function show($id)
  {
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['data'] = $this->M_booking_order->find($id);
    $this->load->view('administrator/booking_order/V_show', $data);
}

function edit($id)
{
    $this->load->model('M_corak');

    $data['corak'] = $this->M_corak->all();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['data'] = $this->M_booking_order->find($id);
    $this->load->view('administrator/booking_order/V_edit', $data);
}

function update()
{
    $data = array(
        'Tanggal' => $this->input->post('Tanggal'),
        'Nomor' => $this->input->post('no_bo'),
        'Tanggal_selesai' => $this->input->post('Tanggal_selesai'),
        'IDCorak' => $this->input->post('id_corak'),
        'Qty' => $this->input->post('qty'),
        'Keterangan' => $this->input->post('Keterangan')
    );

    $this->M_booking_order->update($data, $this->input->post('id'));
    $this->session->set_flashdata("alert", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Ubah</div></center>");
    redirect(base_url('BookingOrder'));
}

function soft_delete($id)
{
    $this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
    $this->M_booking_order->soft_delete($id);
    redirect(base_url('BookingOrder'));
}

function soft_success($id)
{
    $this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diaktifkan</div>");
    $this->M_booking_order->soft_success($id);
    redirect(base_url('BookingOrder'));
}

function bulk()
{
    if($this->input->post('bulk')=="")
    {
         $this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i><center> Tidak Ada Data Yang Dipilih</center></div>");
    }else{
    $this->M_booking_order->bulk($this->input->post('bulk'));
    $this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> <center>Data berhasil di-non-aktifkan</center></div>");
    }
    redirect(base_url('BookingOrder'));
}

function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['booking_order'] = $this->M_booking_order->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
    $this->load->view('administrator/booking_order/V_booking_order', $data);
}

public function print_data(){
    $bo = $this->uri->segment('3');
    $data['data'] = $this->M_booking_order->getBO($bo);
    $this->load->view('administrator/booking_order/V_print', $data);
}
}