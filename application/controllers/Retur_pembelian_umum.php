<?php

class Retur_pembelian_umum extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_retur_pembelian_umum');
    $this->load->model('M_hutang');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
  }

  public function index()
  {
    $data['retur'] = $this->M_retur_pembelian_umum->tampilkan_retur_pembelian();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/retur_pembelian_umum/V_retur_pembelian.php', $data);
  }

  public function tambah_retur_pembelian()
  {
    $data['inv'] = $this->M_retur_pembelian_umum->tampil_inv();
    $data['koderetur'] = $this->M_retur_pembelian_umum->no_retur(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['supplier'] = $this->M_retur_pembelian_umum->tampil_supplier();
    $data['grandtotal'] = $this->M_retur_pembelian_umum->tampil_invoice_grand_total();
    $this->load->view('administrator/retur_pembelian_umum/V_tambah_retur_pembelian.php', $data);
  }

  function check_invoice_pembelian_umum()
  {
    $return['data']  = $this->M_retur_pembelian_umum->check_invoice($this->input->post('Nomor'));
    if ($return['data']) {
      $return['error'] = false;
    }else{
      $return['error'] = true;
    }

    echo json_encode($return);
  }
  public function delete_multiple()
  {
    $ID_att = $this->input->post('msg');
    if($ID_att != ""){
     $result = array();
     foreach($ID_att AS $key => $val){
       $result[] = array(
        "IDRBUmum" => $ID_att[$key],
        "Batal"  => 'tidak aktif'
      );
     }
     $this->db->update_batch('tbl_retur_pembelian_umum', $result, 'IDRBUmum');
     redirect("Retur_pembelian_umum/index");
   }else{
     redirect("Retur_pembelian_umum/index");
   }
   
 }
 function cek_grand_total()
 {
  $IDFB = $this->input->post('Nomor');
  $data = $this->M_retur_pembelian_umum->check_grandtotal($IDFB);
  echo json_encode($data);
}

function simpan_retur_pembelian()
{
  $data1 = $this->input->post('data1');
  $data2 = $this->input->post('data2');
    // $data3 = $this->input->post('data3');
  $data['id_retur'] = $this->M_retur_pembelian_umum->tampilkan_id_retur();

  if($data['id_retur']!=""){
    // foreach ($data['id_retur'] as $value) {
      $urutan= substr(if($data['id_retur']->IDRBUmum, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }

  $data = array(
    'IDRBUmum' => $urutan_id,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['RB'],
    'IDFBUmum' => $data1['IDFBUmum'],
    'IDSupplier' => $data1['IDSupplier'],
    'Tanggal_fb' => $data1['TanggalInv'],
    'Total_qty' => $data1['totalqty'],
    'Saldo_qty' => $data1['totalqty'],
    'PPN' => 0,
    'Disc' => $data1['Discount_total'],
    'Status_ppn' => $data1['Status_ppn'],
    'Persen_disc' => 0,
    'IDMataUang' => 1,
    'Kurs' => 14000,
    'Grand_total' => 0,
    'Keterangan' => $data1['Keterangan'],
    'Batal' => 'aktif',
  );
    // $this->M_retur_pembelian_umum->add($data);


  if($this->M_retur_pembelian_umum->add($data)){
    $data = null;
    foreach($data2 as $row){
      $data['id_retur_detail'] = $this->M_retur_pembelian_umum->tampilkan_id_retur_detail();
      if($data['id_retur_detail']!=""){
        // foreach ($data['id_retur_detail'] as $value) {
          $urutan= substr($data['id_retur_detail']->IDRBUmumDetail, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_detail = 'P000001';
      }
      $data = array(
        'IDRBUmumDetail' => $urutan_id_detail,
        'IDRBUmum' => $urutan_id,
        'IDFBUmumDetail' => '1',
        'IDBarang' => $row['IDBarang'],
        'Qty' => $row['Qty'],
        'Saldo' => $row['Qty'],
        'IDSatuan' => $row['IDSatuan'],
        'Harga_satuan' => str_replace(".", "",$row['Harga_satuan']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Sub_total' => str_replace(".", "",$row['Sub_total']),
      );
      $this->M_retur_pembelian_umum->add_detail($data);

      //save jurnal
      $data['cekgroupsupplier'] = $this->M_retur_pembelian_umum->cek_group_supplier($data1['IDSupplier']);
      $data['cekgroupbarang'] = $this->M_retur_pembelian_umum->cek_group_barang($row['IDBarang']);

      if ($data['cekgroupsupplier']->Group_Supplier == "PIHAK BERELASI" && $data['cekgroupbarang']->Group_Barang== "aksesoris") {
        $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
      if ($data['id_jurnal'] != "") {
        // foreach ($data['id_jurnal'] as $value) {
          $urutan = substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
      } else {
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_debet_utang = array(
        'IDJurnal' => $urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor_invoice'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail_inv,
        'Jenis_faktur' => 'RBU',
        'IDCOA' =>  'P000628',
        'Debet' => str_replace(".", "", $row['Sub_total']),
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Sub_total']),
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Sub_total']),
      );
      $this->M_retur_pembelian_umum->save_jurnal($save_jurnal_debet_utang);

      $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
      if ($data['id_jurnal'] != "") {
        // foreach ($data['id_jurnal'] as $value) {
          $urutan = substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal_ppn = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
      } else {
        $urutan_id_jurnal_ppn = 'P000001';
      }

      $save_jurnal_kredit_ppn = array(
        'IDJurnal' => $urutan_id_jurnal_ppn,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor_invoice'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail_inv,
        'Jenis_faktur' => 'RBU',
        'IDCOA' => 'P000025',
        'Debet' => str_replace(".", "", $row['Sub_total']) * 0.1,
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Sub_total']) * 0.1,
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Sub_total']) * 0.1,
      );
      $this->M_retur_pembelian_umum->save_jurnal($save_jurnal_kredit_ppn);

      $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
      if ($data['id_jurnal'] != "") {
        // foreach ($data['id_jurnal'] as $value) {
          $urutan = substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal_kredit_pers = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
      } else {
        $urutan_id_jurnal_kredit_pers = 'P000001';
      }
      $save_jurnal_kredit_pers = array(
        'IDJurnal' => $urutan_id_jurnal_kredit_pers,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor_invoice'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail_inv,
        'Jenis_faktur' => 'RBU',
        'IDCOA' => 'P000020',
        'Debet' => 0,
        'Kredit' => str_replace(".", "", $row['Sub_total']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Sub_total']),
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Sub_total']),
      );
      $this->M_retur_pembelian_umum->save_jurnal($save_jurnal_kredit_pers);
      } elseif ($data['cekgroupsupplier']->Group_Supplier == "PIHAK KETIGA" && $data['cekgroupbarang']->Group_Barang== "aksesoris") {
        $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
      if ($data['id_jurnal'] != "") {
        // foreach ($data['id_jurnal'] as $value) {
          $urutan = substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
      } else {
        $urutan_id_jurnal = 'P000001';
      }

      $save_jurnal_debet_utang = array(
        'IDJurnal' => $urutan_id_jurnal,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor_invoice'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail_inv,
        'Jenis_faktur' => 'RBU',
        'IDCOA' =>  'P000627',
        'Debet' => str_replace(".", "", $row['Sub_total']),
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Sub_total']),
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Sub_total']),
      );
      $this->M_retur_pembelian_umum->save_jurnal($save_jurnal_debet_utang);

      $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
      if ($data['id_jurnal'] != "") {
        // foreach ($data['id_jurnal'] as $value) {
          $urutan = substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal_ppn = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
      } else {
        $urutan_id_jurnal_ppn = 'P000001';
      }

      $save_jurnal_kredit_ppn = array(
        'IDJurnal' => $urutan_id_jurnal_ppn,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor_invoice'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail_inv,
        'Jenis_faktur' => 'RBU',
        'IDCOA' => 'P000025',
        'Debet' => str_replace(".", "", $row['Sub_total']) * 0.1,
        'Kredit' => 0,
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Sub_total']) * 0.1,
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Sub_total']) * 0.1,
      );
      $this->M_retur_pembelian_umum->save_jurnal($save_jurnal_kredit_ppn);

      $data['id_jurnal'] = $this->M_retur_pembelian_umum->tampilkan_id_jurnal();
      if ($data['id_jurnal'] != "") {
        // foreach ($data['id_jurnal'] as $value) {
          $urutan = substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal_kredit_pers = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
      } else {
        $urutan_id_jurnal_kredit_pers = 'P000001';
      }
      $save_jurnal_kredit_pers = array(
        'IDJurnal' => $urutan_id_jurnal_kredit_pers,
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['Nomor_invoice'],
        'IDFaktur' => $urutan_id,
        'IDFakturDetail' => $urutan_id_detail_inv,
        'Jenis_faktur' => 'RBU',
        'IDCOA' => 'P000020',
        'Debet' => 0,
        'Kredit' => str_replace(".", "", $row['Sub_total']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Total_debet' => str_replace(".", "", $row['Sub_total']),
        'Total_kredit' => 0,
        'Keterangan' => $data1['Keterangan'],
        'Saldo' => str_replace(".", "", $row['Sub_total']),
      );
      $this->M_retur_pembelian_umum->save_jurnal($save_jurnal_kredit_pers);
      }


    }
  }
    $data['id_retur_grand_total'] = $this->M_retur_pembelian_umum->tampilkan_id_retur_grand_total();
    if($data['id_retur_grand_total']!=""){
      // foreach ($data['id_retur_grand_total'] as $value) {
        $urutan= substr($data['id_retur_grand_total']->IDRBUmumGrandTotal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_grand_total= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_grand_total = 'P000001';
    }
    
    $data4 = array(
      'IDRBUmumGrandTotal' => $urutan_id_grand_total,
      'IDRBUmum' => $urutan_id,
      'IDMataUang' =>1,
      'Kurs' => 14000,
      'Pembayaran' => $data1['Pembayaran'],
      'DPP' => str_replace(".", "",$data1['DPP']),
      'Discount' => str_replace(".", "",$data1['Discount_total']),
      'PPN' => str_replace(".", "",$data1['PPN']),
      'Grand_total' => str_replace(".", "",$data1['total_invoice']),
      'Sisa' => str_replace(".", "",$data1['total_invoice'])
    );
    $this->M_retur_pembelian_umum->add_grand_total($data4);

    $hutang= $this->M_retur_pembelian_umum->data_hutang($data1['Nomor']);
    $datahutang = array(
      'Pembayaran' => $hutang->Sisa,
      'Saldo_Akhir' => 0
    );
    $this->M_retur_pembelian_umum->update_data_hutang($data1['Nomor'], $datahutang);

    echo json_encode($data);
 //  }else{
 //   echo false;
 // }
}

public function edit($id)
{
  $data['detail'] = $this->M_retur_pembelian_umum->detail_retur_pembelian($id);
  $data['supplier'] = $this->M_retur_pembelian_umum->tampil_supplier();
  $data['retur'] = $this->M_retur_pembelian_umum->getById($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/retur_pembelian_umum/V_edit_retur_pembelian', $data);
}

public function ubah_retur_pembelian()
{
  $data1 = $this->input->post('data1');
  $data2 = $this->input->post('data2');
  $data3 = $this->input->post('data3');

  $data = array(
   'Tanggal' => $data1['Tanggal'],
   'Nomor' => $data1['RB'],
   'IDFBUmum' => $data1['id_fb_umum'],
   'IDSupplier' => $data1['IDSupplier'],
   'Tanggal_fb' => $data1['TanggalInv'],
   'Total_qty' => $data1['totalqty'],
   'Saldo_qty' => $data1['totalqty'],
   'Disc' => $data1['Discount_total'],
   'Status_ppn' => $data1['Status_ppn'],
   'Keterangan' => $data1['Keterangan'],
   'Batal' => 'aktif',
 );

  if($this->M_retur_pembelian_umum->update_master($data1['IDRB'],$data)){
    $data = null;
    if($data3 > 0){
     foreach($data3 as $row){
       $this->M_retur_pembelian_umum->drop($row['IDRBUmumDetail']);
     }
   }
   if($data2 != null){
    foreach($data2 as $row){
      $data = array(
        'IDRBUmum' => $data1['IDRB'],
        'IDFBUmumDetail' => '1',
        'IDBarang' => $row['IDBarang'],
        'Qty' => $row['Qty'],
        'Saldo' => $row['Qty'],
        'IDSatuan' => $row['IDSatuan'],
        'Harga_satuan' => $row['Harga_satuan'],
        'IDMataUang' => 1,
        'Kurs' => 14000,
        'Sub_total' => $row['Sub_total'],
      );

      if($row['IDRBUmumDetail'] ==''){
        $this->M_retur_pembelian_umum->add_detail($data);
      }elseif($row['IDRBUmumDetail'] !=''){
        $this->M_retur_pembelian_umum->update_detail($row['IDRBUmumDetail'], $data);      
      }
    }
  }

  $datas = array(
    'IDRB' => $data1['IDRB'],
    'Pembayaran' => $data1['Pembayaran'],
    'IDMataUang' => 1,
    'Kurs' => '14000',
    'DPP' => $data1['DPP'],
    'Discount' => $data1['Discount_total'],
    'PPN' => $data1['PPN'],
    'Grand_total' => $data1['total_invoice'],
    'Sisa' => 2,
  );
  $this->M_retur_pembelian_umum->update_grand_total($data1['IDRB'],$datas);

  echo true;

}else{
  echo false;
}
}
public function status_gagal($id)
{
  $data = array(
    'Batal' => 'tidak aktif',
  );

  $this->M_retur_pembelian_umum->update_status($data, array('IDRBUmum' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('Retur_pembelian_umum/index');
}

public function status_berhasil($id)
{
  $data = array(
    'Batal' => 'aktif',
  );

  $this->M_retur_pembelian_umum->update_status($data, array('IDRBUmum' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('Retur_pembelian_umum/index');
}
function pencarian()
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['retur'] = $this->M_retur_pembelian_umum->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/retur_pembelian_umum/V_retur_pembelian', $data);
}

function show($id)
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['data'] = $this->M_retur_pembelian_umum->find($id);
  $data['datadetail'] = $this->M_retur_pembelian_umum->find_detail($id);
  $this->load->view('administrator/retur_pembelian_umum/V_show', $data);
}

function print($nomor)
{

  $data['print'] = $this->M_retur_pembelian_umum->print_retur_pembelian($nomor);
  $id= $data['print']->IDRBUmum;
  $data['printdetail'] = $this->M_retur_pembelian_umum->print_retur_pembelian_detail($id);
  $data['printgrandtotal'] = $this->M_retur_pembelian_umum->print_grand_total($id);
  $this->load->view('administrator/retur_pembelian_umum/V_print', $data);
}

public function laporan_retur_pembelian()
{

      $data['laporanretur']= $this->M_retur_pembelian_umum->get_all_procedure_retur(); //Load Data Purchase Order
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/retur_pembelian_umum/V_laporan_retur_pembelian', $data);
    }

    function pencarian_store()
    {
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu'] = $this->M_login->aksesmenu();
      $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
      if ($this->input->post('date_from') == '') {
        $data['laporanretur'] = $this->M_retur_pembelian_umum->searching_store_like($this->input->post('keyword'));
      } else {
        $data['laporanretur'] = $this->M_retur_pembelian_umum->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
      }
      $this->load->view('administrator/retur_pembelian_umum/V_laporan_retur_pembelian', $data);
    }
  }

  ?>