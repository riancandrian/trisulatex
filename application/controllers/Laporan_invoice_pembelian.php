<?php

class Laporan_invoice_pembelian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_laporan_inv_pembelian');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        $data['invoice'] = $this->M_laporan_inv_pembelian->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_invoice_pembelian/V_index', $data);
    }

    function pencarian()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
         $data['aksesmenu'] = $this->M_login->aksesmenu();
      $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
      if ($this->input->post('date_from') == '') {
        $data['invoice'] = $this->M_laporan_inv_pembelian->searching_store_like($this->input->post('keyword'));
      } else {
        $data['invoice'] = $this->M_laporan_inv_pembelian->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
      }
        $this->load->view('administrator/laporan_invoice_pembelian/V_index', $data);
    }

    public function index_umum()
    {
        $data['invoice'] = $this->M_laporan_inv_pembelian->all_umum();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_invoice_pembelian/V_index_umum', $data);
    }

    function pencarian_umum()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
         $data['aksesmenu'] = $this->M_login->aksesmenu();
      $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
      if ($this->input->post('date_from') == '') {
        $data['invoice'] = $this->M_laporan_inv_pembelian->searching_store_like_umum($this->input->post('keyword'));
      } else {
        $data['invoice'] = $this->M_laporan_inv_pembelian->searching_store_umum($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
      }
        $this->load->view('administrator/laporan_invoice_pembelian/V_index_umum', $data);
    }
    public function exporttoexcel(){
      // create file name
        $fileName = 'INV '.time().'.xlsx';  
    // load excel library
        $this->load->library('excel');
        $empInfo = $this->M_laporan_inv_pembelian->exsport_excel();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nomor');  
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Supplier'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'No SJ Supplier'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Status PPN'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Barcode'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'NoSO'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Party'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Barang'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Corak'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Merk'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Warna'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Qty Yard'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Qty Meter'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Grade'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Remark'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Lebar'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Satuan'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Harga'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Sub Total'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Jatuh Tempo'); 
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['Tanggal']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['Nomor']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['Nama']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['No_sj_supplier']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Status_ppn']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['Barcode']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['NoSO']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['Party']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['Nama_Barang']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['Corak']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['Merk']);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['Warna']);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['Saldo_yard']);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['Saldo_meter']);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['Grade']);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['Lebar']);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['Satuan']);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $element['Harga']);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $element['Sub_total']);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $element['Tanggal_jatuh_tempo']);
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
    // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);        
    }
}