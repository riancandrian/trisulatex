
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LabelBarcode extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_label_barcode');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/label_barcode/create_label.php', $data);
	}

	public function tampil()
	{
		$data['labelbarcode']= $this->M_label_barcode->tampil_label_barcode();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/label_barcode/tampil_label.php', $data);
	}

	public function proses_tambah_barcode()
	{
		$data_cek = array(
			'Barcode' => $this->input->post('barcode'),
		);
		$row = $this->M_label_barcode->cek_ketersediaan_data($data_cek, 'tbl_barcode_print');
		if ($row =="") {
			$data = array(
				'Barcode' => strtoupper($this->input->post('barcode')),
				'NoSO' => $this->input->post('noso'),
				'Party' => $this->input->post('party'),
				'Indent' => $this->input->post('indent'),
				'CustDes' => $this->input->post('custdes'),
				'WarnaCust' => $this->input->post('warnacust'),
				'Panjang_Yard' => floatval($this->input->post('panjang')),
				'Panjang_Meter' => floatval($this->input->post('panjangm')),
				'Grade' => $this->input->post('grade'),
				'Remark' => $this->input->post('remark'),
				'Lebar' => $this->input->post('lebar'),
				'Satuan' => $this->input->post('satuan'),
			);

			$this->M_label_barcode->simpan($data);
			$ambil= $this->db->insert_id();
			$tes['tampilprinttambah'] = $this->M_label_barcode->getById($ambil);
			$this->load->view('administrator/label_barcode/V_tampil_print', $tes);

		}else{
			echo "<script>alert('Data Sudah Tersedia');history.go(-1);</script>";
		}
	}

	function printData($id)
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$data['tampilprint']=$this->M_label_barcode->getDataBarcode($id);
		$this->load->view('administrator/label_barcode/V_print_data', $data);
	} 

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		
		$kolom= addslashes($this->input->post('jenispencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $keyword!="") {
			$data['labelbarcode']=$this->M_label_barcode->cari_by_kolom($kolom,$keyword);
		}elseif ($keyword!="") {
			$data['labelbarcode']=$this->M_label_barcode->cari_by_keyword($keyword);
		}else {
			redirect('LabelBarcode/tampil');
		}
		$this->load->view('administrator/label_barcode/tampil_label.php', $data);
	}
}

?>