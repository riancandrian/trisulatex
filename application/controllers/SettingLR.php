<?php

class SettingLR extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_setting_lr');
        $this->load->model('M_coa');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
        $data['set_lr'] = $this->M_setting_lr->set_lr();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_index_setting_lr.php', $data);
    }

    public function cari_bulan_lr()
    {
        $bulan= $this->input->post("bulan");
        $tahun= $this->input->post("tahun");
        $data['bulan']= $this->input->post("bulan");
        $data['tahun']= $this->input->post("tahun");
        $bulan_dikurang= $this->input->post("bulan")-1;
        $data['bulan_ini']= $this->input->post("bulan");
        $data['bulan_ini_dikurang']= $bulan_dikurang;
        $data['set_lr'] = $this->M_setting_lr->cari_set_lr($bulan, $tahun);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_index_setting_lr.php', $data);
    }

    public function index_laba_rugi()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_pl/V_laba_rugi.php', $data);
    }

    public function cari_bulan_pl()
    {
        $bulan= $this->input->post("bulan");
        $tahun= $this->input->post("tahun");
        $data['bulan']= $this->input->post("bulan");
        $data['tahun']= $this->input->post("tahun");
        $bulan_dikurang= $this->input->post("bulan")-1;
        $data['bulan_ini']= $this->input->post("bulan");
        $data['bulan_ini_dikurang']= $bulan_dikurang;
        $data['set_lr'] = $this->M_setting_lr->cari_set_pl($bulan, $tahun);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_pl/V_laba_rugi.php', $data);
    }

    public function index_arus_kas()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_cf/V_arus_kas.php', $data);
    }

    public function cari_bulan_cf()
    {
        $bulan= $this->input->post("bulan");
        $tahun= $this->input->post("tahun");
        $data['bulan']= $this->input->post("bulan");
         $data['tahun']= $this->input->post("tahun");
        $bulan_dikurang= $this->input->post("bulan")-1;
        $data['bulan_ini']= $this->input->post("bulan");
        $data['bulan_ini_dikurang']= $bulan_dikurang;
        $data['set_lr'] = $this->M_setting_lr->cari_set_cf($bulan, $tahun);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_cf/V_arus_kas.php', $data);
    }

    public function index_catatan_lapkeu()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_notes/V_catatan_lapkeu.php', $data);
    }

    public function cari_bulan_notes()
    {
        $bulan= $this->input->post("bulan");
        $bulan_dikurang= $this->input->post("bulan")-1;
        $data['bulan_ini']= $this->input->post("bulan");
        $data['bulan_ini_dikurang']= $bulan_dikurang;
        $data['set_lr'] = $this->M_setting_lr->cari_set_notes($bulan);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_notes/V_catatan_lapkeu.php', $data);
    }

    public function index_aging_schedule()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_aging_schedule.php', $data);
    }

    public function cari_bulan_aging_schedule()
    {
        $bulan= $this->input->post("bulan");
        $bulan_dikurang= $this->input->post("bulan")-1;
        $data['bulan_ini']= $this->input->post("bulan");
        $data['bulan_ini_dikurang']= $bulan_dikurang;
        $data['set_lr_tanggal'] = $this->M_setting_lr->cari_set_aging_schedule_tanggal($bulan);
        $data['set_lr'] = $this->M_setting_lr->cari_set_aging_schedule($bulan);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_aging_schedule.php', $data);
    }

    public function index_dibayar_dimuka()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_dibayar_dimuka.php', $data);
    }

    public function index_asset_tetap()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_aset_tetap.php', $data);
    }

    public function index_mutasi_utang_usaha()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_mutasi_utang_usaha.php', $data);
    }

    public function cari_bulan_mutasi_utang_usaha()
    {
        $bulan= $this->input->post("bulan");
        $bulan_dikurang= $this->input->post("bulan")-1;
        $data['bulan_ini']= $this->input->post("bulan");
        $data['bulan_ini_dikurang']= $bulan_dikurang;
        $data['set_lr_tanggal'] = $this->M_setting_lr->cari_set_mutasi_utang_usaha_tanggal($bulan);
        $data['set_lr'] = $this->M_setting_lr->cari_set_mutasi_utang_usaha($bulan);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_mutasi_utang_usaha.php', $data);
    }

    public function index_beban_operasional()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_beban_operasional.php', $data);
    }

    public function index_buku_besar_pembantu()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_buku_besar_pembantu.php', $data);
    }

    public function cari_bulan_buku_besar_pembantu()
    {
        $bulan= $this->input->post("bulan");
        $bulan_dikurang= $this->input->post("bulan")-1;
        $data['bulan_ini']= $this->input->post("bulan");
        $data['bulan_ini_dikurang']= $bulan_dikurang;
        $data['set_lr'] = $this->M_setting_lr->cari_set_buku_besar_pembantu($bulan);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_buku_besar_pembantu.php', $data);
    }

    public function index_buku_besar()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['getcoa']= $this->M_setting_lr->get_all_coa();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_buku_besar.php', $data);
    }

    public function cari_bulan_buku_besar()
    {
        $data['coa1']= $this->input->post("coa");
        $coa= $this->input->post("coa");
        $bulan= $this->input->post("bulan");
         $data['getcoa']= $this->M_setting_lr->get_all_coa();
         $data['getcoatampil']= $this->M_setting_lr->get_coa_tampil($coa);
        $data['set_lr'] = $this->M_setting_lr->cari_set_buku_besar($coa, $bulan);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_buku_besar.php', $data);
    }

    public function index_mutasi_persediaan()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_mutasi_persediaan.php', $data);
    }

    public function cari_tanggal_mutasi_persediaan()
    {
        $tanggalmulai= $this->input->post("tanggal_mulai");
        $tanggalselesai= $this->input->post("tanggal_selesai");
        $data['set_lr'] = $this->M_setting_lr->cari_set_mutasi_persediaan($tanggalmulai, $tanggalselesai);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_mutasi_persediaan.php', $data);
    }

    public function tambah_setting_lr()
    {
        $data['setting_lr'] = $this->M_setting_lr->setting_lr2();
        $data['coa'] = $this->M_coa->tampilkan_coa();
        $data['edit_sub'] = $this->M_setting_lr->ambil_byid_sub();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_lr/V_create', $data);
    }

    public function tambah_setting_pl()
    {
        $data['setting_pl'] = $this->M_setting_lr->setting_pl2();
        $data['coa'] = $this->M_coa->tampilkan_coa();
        $data['edit_sub'] = $this->M_setting_lr->ambil_byid_sub_pl();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_pl/V_create', $data);
    }

    public function tambah_setting_cf()
    {
        $data['setting_cf'] = $this->M_setting_lr->setting_cf2();
        $data['coa'] = $this->M_coa->tampilkan_coa();
        $data['edit_sub'] = $this->M_setting_lr->ambil_byid_sub_cf();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_cf/V_create', $data);
    }

    public function tambah_setting_notes()
    {
        $data['setting_notes'] = $this->M_setting_lr->setting_notes2();
        $data['coa'] = $this->M_coa->tampilkan_coa();
        $data['edit_sub'] = $this->M_setting_lr->ambil_byid_sub_notes();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/setting_notes/V_create', $data);
    }

    function simpan()
    {
      $this->M_setting_lr->delete();

      $jml_data = count($this->input->post('coa'));
      for ($x = 0; $x < $jml_data; $x++) {
        $data['id_setting_lr'] = $this->M_setting_lr->tampilkan_id_setting();
        if($data['id_setting_lr'] !=""){
            foreach ($data['id_setting_lr'] as $value) {
              $urutan= substr($value->IDSettingLR, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
        $data = array(
            'IDSettingLR' => $urutan_id,
            'IDCoa' => $this->input->post('coa')[$x],
            'Keterangan' => $this->input->post('keterangan')[$x],
            'Kategori' => $this->input->post('kategori')[$x],
            'Perhitungan' => $this->input->post('perhitungan')[$x],
            'Group' => $this->input->post('group')[$x],
            'Urutan' => '1',
            'IDUser' => $this->session->userdata("id"),
        );
        $this->M_setting_lr->simpan($data);
    }

    
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data berhasil di Simpan</div></center>");
    redirect('SettingLR/tambah_setting_lr');
}

function simpan_pl()
    {
      $this->M_setting_lr->delete_pl();

      $jml_data = count($this->input->post('coa'));
      for ($x = 0; $x < $jml_data; $x++) {
        $data['id_setting_pl'] = $this->M_setting_lr->tampilkan_id_setting_pl();
        if($data['id_setting_pl'] !=""){
            foreach ($data['id_setting_pl'] as $value) {
              $urutan= substr($value->IDSettingPL, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
        $data = array(
            'IDSettingPL' => $urutan_id,
            'IDCoa' => $this->input->post('coa')[$x],
            'Keterangan' => $this->input->post('keterangan')[$x],
            'Kategori' => $this->input->post('kategori')[$x],
            'Perhitungan' => $this->input->post('perhitungan')[$x],
            'Group' => $this->input->post('group')[$x],
            'Urutan' => '1',
            'IDUser' => $this->session->userdata("id"),
        );
        $this->M_setting_lr->simpan_pl($data);
    }

    
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data berhasil di Simpan</div></center>");
    redirect('SettingLR/tambah_setting_pl');
}

function simpan_cf()
    {
      $this->M_setting_lr->delete_cf();

      $jml_data = count($this->input->post('coa'));
      for ($x = 0; $x < $jml_data; $x++) {
        $data['id_setting_cf'] = $this->M_setting_lr->tampilkan_id_setting_cf();
        if($data['id_setting_cf'] !=""){
            foreach ($data['id_setting_cf'] as $value) {
              $urutan= substr($value->IDSettingCF, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
        $data = array(
            'IDSettingCF' => $urutan_id,
            'IDCoa' => $this->input->post('coa')[$x],
            'Keterangan' => $this->input->post('keterangan')[$x],
            'Kategori' => $this->input->post('kategori')[$x],
            'Perhitungan' => $this->input->post('perhitungan')[$x],
            'Group' => $this->input->post('group')[$x],
            'Urutan' => '1',
            'IDUser' => $this->session->userdata("id"),
        );
        $this->M_setting_lr->simpan_cf($data);
    }

    
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data berhasil di Simpan</div></center>");
    redirect('SettingLR/tambah_setting_cf');
}

function simpan_notes()
    {
      $this->M_setting_lr->delete_notes();

      $jml_data = count($this->input->post('coa'));
      for ($x = 0; $x < $jml_data; $x++) {
        $data['id_setting_notes'] = $this->M_setting_lr->tampilkan_id_setting_notes();
        if($data['id_setting_notes'] !=""){
            foreach ($data['id_setting_notes'] as $value) {
              $urutan= substr($value->IDSettingNotes, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
        $data = array(
            'IDSettingNotes' => $urutan_id,
            'IDCoa' => $this->input->post('coa')[$x],
            'Keterangan' => $this->input->post('keterangan')[$x],
            'Kategori' => $this->input->post('kategori')[$x],
            'Perhitungan' => $this->input->post('perhitungan')[$x],
            'Group' => $this->input->post('group')[$x],
            'Urutan' => '1',
            'IDUser' => $this->session->userdata("id"),
        );
        $this->M_setting_lr->simpan_notes($data);
    }

    
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data berhasil di Simpan</div></center>");
    redirect('SettingLR/tambah_setting_notes');
}

public function edit($id)
{
    $data['group_asset'] = $this->M_asset->group_asset();
    $data['Assets'] = $this->M_asset->getById($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/asset/V_edit_asset', $data);
}

public function update()
{
    $data = array(
        'Kode_Asset' => $this->input->post('kode_asset'),
        'Nama_Asset' => $this->input->post('nama_asset'),
        'IDGroupAsset' => $this->input->post('group_asset')
    );

    $this->M_asset->update($data, array('IDAsset' => $this->input->post('id')));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('Assets/index');
}

public function delete($id)
{
    $this->M_asset->delete($id);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil dihapus</div></center>");
    redirect('Assets');
}

public function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $kolom= addslashes($this->input->post('jenispencarian'));
    $status= addslashes($this->input->post('statuspencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $status!="" && $keyword!="") {
        $data['assets']=$this->M_asset->cari($kolom,$status,$keyword);
    }elseif ($kolom!="" && $keyword!="") {
        $data['assets']=$this->M_asset->cari_by_kolom($kolom,$keyword);
    }elseif ($status!="") {
        $data['assets']=$this->M_asset->cari_by_status($status);
    }elseif ($keyword!="") {
        $data['assets']=$this->M_asset->cari_by_keyword($keyword);
    }else {
        $data['assets']=$this->M_asset->tampilkan_asset();
    }
    $this->load->view('administrator/asset/V_asset', $data);
}
}