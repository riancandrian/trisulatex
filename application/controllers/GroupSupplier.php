
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class GroupSupplier extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_GroupSupplier');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['groupsupplier']=$this->M_GroupSupplier->tampilkan_GroupSupplier();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/groupsupplier/V_groupsupplier', $data);
	}

	public function tambah_groupsupplier()
	{
		
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/groupsupplier/V_tambah_groupsupplier', $data);
	}
	public function simpan()
	{
		$data['id_group_supplier']=$this->M_GroupSupplier->tampilkan_id_GroupSupplier();
		if($data['id_group_supplier']!=""){
			// foreach ($data['id_group_supplier'] as $value) {
				$urutan= substr($data['id_group_supplier']->IDGroupSupplier, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$simbar= $this->input->post('simbar');

		$data = array(
			'IDGroupSupplier' => $urutan_id,
			'Kode_Group_Supplier' => $this->input->post('Kode_Group_Supplier'),
			'Group_Supplier' => $this->input->post('Group_Supplier'),
			'Aktif' => 'aktif'
		);

		$this->M_GroupSupplier->simpan($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('GroupSupplier/index');
		}elseif($simtam){
			redirect('GroupSupplier/tambah_groupsupplier');
		}elseif($simbar){
			redirect('Supplier/create');
		}
	}
	public function edit($id)
	{
		$data['groupsupplier'] = $this->M_GroupSupplier->get_GroupSupplier_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/groupsupplier/V_edit_groupsupplier', $data);
	}
	public function update()
	{
		$data = array(
			'Kode_Group_Supplier' => $this->input->post('Kode_Group_Supplier'),
			'Group_Supplier' => $this->input->post('Group_Supplier')
		);
		echo $this->input->post('id');
		$this->M_GroupSupplier->update_GroupSupplier($this->input->post('id'),$data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
		redirect('GroupSupplier/index');
	}
	public function hapus_groupsupplier($id)
	{
		$data['poget']= $this->M_GroupSupplier->get_po($id);
		$data['pogetumum']= $this->M_GroupSupplier->get_po_umum($id);

		if(count($data['poget']) > 0){
			if(($data['poget']->IDGroupSupplier!=$id)){   
				$this->M_GroupSupplier->hapus_data_GroupSupplier($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
				redirect('GroupSupplier/index');
			}else{
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
				redirect('GroupSupplier/index');
			}
		}elseif(count($data['pogetumum']) > 0){
			if(($data['pogetumum']->IDGroupSupplier!=$id)){   
				$this->M_GroupSupplier->hapus_data_GroupSupplier($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
				redirect('GroupSupplier/index');
			}else{
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
				redirect('GroupSupplier/index');
			}
		}else{
			$this->M_GroupSupplier->hapus_data_GroupSupplier($id);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
			redirect('GroupSupplier/index');
		}
	}
	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['groupsupplier']=$this->M_GroupSupplier->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['groupsupplier']=$this->M_GroupSupplier->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['groupsupplier']=$this->M_GroupSupplier->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['groupsupplier']=$this->M_GroupSupplier->cari_by_keyword($keyword);
		}else {
			$data['groupsupplier']=$this->M_GroupSupplier->tampilkan_GroupSupplier();
		}
		$this->load->view('administrator/groupsupplier/V_groupsupplier', $data);
	}
}

?>