
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class StokOpName extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('M_stok_opname');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}
	function index()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/stokopname/V_stokopname', $data);
	}

	public function getTampungan()
	{
		$barcode = $this->input->post('barcode');
		$data = $this->M_stok_opname->getimport($barcode);
		echo json_encode($data);
	}

	public function getin()
	{
		$barcode = $this->input->post('barcode');
		$data = $this->M_stok_opname->getin($barcode);
		echo json_encode($data);

	}

	public function input_data()
	{
		$data['id_stokopname'] = $this->M_stok_opname->tampilkan_id_stokopname(); 
		if($data['id_stokopname']!=""){
			foreach ($data['id_stokopname'] as $value) {
				$urutan= substr($value->IDStokOpname, 1);
			}
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}

		$data_cek = array(
			'Barcode' => $this->input->post('Barcode')
		);

        //Cek apakah ada data yang sama
		$cek1 = $this->M_stok_opname->cek_ketersediaan($data_cek, "tbl_stok_opname");
		if ($cek1) {
			$response[0] = "Proses Behasil, kasus data ada yang sama";
		}
		else {
			$data = array(
				'IDStokOpname' => $urutan_id,
				'Tanggal' => $this->input->post('Tanggal'),
				'Barcode' => $this->input->post('Barcode'),
				'IDBarang' => $this->input->post('Barang'),
				'IDCorak' => $this->input->post('Corak'),
				'IDWarna' => $this->input->post('Warna'),
				'IDGudang' => $this->input->post('Gudang'),
				'Qty_yard' => $this->input->post('Qty_yard'),
				'Qty_meter' => $this->input->post('Qty_meter'),
				'Grade' => $this->input->post('Grade'),
				'IDSatuan' => $this->input->post('Satuan'),
				'Harga' => $this->input->post('Harga')
			);

			$response[0] = "tidak ada sama";

			$this->M_stok_opname->save($data);

		}
		echo json_encode($response);
	}

	public function laporan_stokopname()
	{
		$data['stokopname']=$this->M_stok_opname->tampilkan_stokopname();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/stokopname/V_laporan_stokopname.php', $data);
	}

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		
		if ($this->input->post('date_from') == '') {
			$data['stokopname'] = $this->M_stok_opname->searching_store_like($this->input->post('keyword'));
		} else {
			$data['stokopname'] = $this->M_stok_opname->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
		}
		$this->load->view('administrator/stokopname/V_laporan_stokopname', $data);
	}
}


?>