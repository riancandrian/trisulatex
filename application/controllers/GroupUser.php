<?php

class GroupUser extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_group_user');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
        $data['groupUsers'] = $this->M_group_user->tampilkan_groupUser();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/group_user/V_group_user.php', $data);
    }

    public function tambah_groupUser()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/group_user/V_tambah_groupUser', $data);
    }

    public function simpan()
    {
        $simpan= $this->input->post('simpan');
        $simtam= $this->input->post('simtam');

        $data = array(
            'Kode_Group_User' => $this->input->post('kode_group'),
            'Group_User' => $this->input->post('groupUser')
        );

        $this->M_group_user->simpan($data);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
       if($simpan){
                redirect('GroupUser/index');
            }elseif($simtam){
                redirect('GroupUser/tambah_groupUser');
            }
    }

    public function edit($id)
    {
        $data['groupUsers'] = $this->M_group_user->getById($id);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/group_user/V_edit_groupUser.php', $data);
    }

    public function update()
    {
        $data = array(
            'Kode_Group_User' => $this->input->post('kode_group'),
            'Group_User' => $this->input->post('groupUser')
        );

        $this->M_group_user->update($data, array('IDGroupUser' => $this->input->post('id')));
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
        redirect('GroupUser/index');
    }

    public function pencarian()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();

        $kolom= addslashes($this->input->post('jenispencarian'));
        $keyword= addslashes($this->input->post('keyword'));

        if ($kolom!="" && $keyword!="") {
            $data['groupUsers']=$this->M_group_user->cari_by_kolom($kolom,$keyword);
        }elseif ($keyword!="") {
            $data['groupUsers']=$this->M_group_user->cari_by_keyword($keyword);
        }else {
            $data['groupUsers']=$this->M_group_user->tampilkan_groupUser();
        }
        $this->load->view('administrator/group_user/V_group_user', $data);
    }

    public function delete($id)
    {
        $this->M_group_user->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil dihapus</div></center>");
        redirect('GroupUser/index');
    }
}