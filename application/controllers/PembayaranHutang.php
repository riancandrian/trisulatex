<?php

class PembayaranHutang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_pembayaran_hutang');
        $this->load->model('M_invoice_pembelian');
        $this->load->model('M_agen');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        $data['datas'] = $this->M_pembayaran_hutang->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/pembayaran_hutang/V_index', $data);
    }

    public function create()
    {
        //$data['inv'] = $this->M_pembayaran_hutang->tampil_inv();
        $data['code'] = $this->M_pembayaran_hutang->last_code(date('Y'));
        $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $data['supplier'] = $this->M_pembayaran_hutang->tampil_supplier();
        $data['coasemua'] = $this->M_pembayaran_hutang->tampil_coa_semua();
        $this->load->view('administrator/pembayaran_hutang/V_create', $data);
    }

    public function laporan_pembayaran_hutang()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/pembayaran_hutang/V_laporan_pembayaran_hutang', $data);
    }

    public function pencarian_lap_pembayaran_hutang()
    {
        $date_from= $this->input->post('date_from');
        $date_until= $this->input->post('date_until');
        $search_type= $this->input->post('jenispencarian');
        $keyword= $this->input->post('keyword');

        $data['laporanpembayaranhutang'] = $this->M_pembayaran_hutang->searching_laporan_pembayaran_hutang($date_from, $date_until, $search_type, $keyword);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/pembayaran_hutang/V_laporan_pembayaran_hutang', $data);
    }

    function getcoa() {
        $jenis = $this->input->post('jenis_pembayaran');
        $supplier = $this->input->post('supplier');
        if($jenis=='transfer'){
            $data = $this->M_pembayaran_hutang->getcoa_transfer($jenis);
        }elseif($jenis=='giro')
        {
            $data = $this->M_pembayaran_hutang->getcoa_giro($jenis);
        }elseif($jenis=='cash')
        {
            $data = $this->M_pembayaran_hutang->getcoa_cash($jenis);
        }elseif($jenis=='nota_kredit')
        {
            $data = $this->M_pembayaran_hutang->getcoa_nota_kredit($supplier);
        }
        elseif($jenis=='pembulatan')
        {
            $data = $this->M_pembayaran_hutang->getcoa_pembulatan($jenis);
        }
        echo json_encode($data);
    }

    function check_hutang()
    {
        $return['data']  = $this->M_pembayaran_hutang->check_hutang($this->input->post('IDSupplier'));
        if ($return['data']) {
            $return['error'] = false;
        }else{
            $return['error'] = true;
        }

        echo json_encode($return);
    }

    function store()
    {
        $data1 = $this->input->post('data1');
        $data2 = $this->input->post('data2');
        $data3 = $this->input->post('data3');

        $data['id_hutang'] = $this->M_pembayaran_hutang->tampilkan_id_hutang();

        if($data['id_hutang']!=""){
            foreach ($data['id_hutang'] as $value) {
                $urutan= substr($value->IDBS, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
            $urutan_id = 'P000001';
        }

        $data = array(
            'IDBS' => $urutan_id,
            'Tanggal' => $data1['Tanggal_PH'],
            'Nomor' => $data1['nomor_PH'],
            'IDSupplier' => $data1['IDSupplier'],
            'IDMataUang' => 1,
            'Kurs' => 14000,
            'Total' => $data1['pembayaran'],
            'Uang_muka' => $data1['Uang_muka'] ? $data1['Uang_muka'] : 0,
            // //'Selisih' => ,
            'Kompensasi_um' => $data1['Kompensasi_um'] ? $data1['Kompensasi_um'] : 0,
            'Pembayaran' => $data1['sisa_pembayaran'],
            'Kelebihan_bayar' => 0,
            'Keterangan' => $data1['Keterangan'],
            'Batal' => 'aktif',
        );
// $this->M_pembayaran_hutang->add($data);
        if($this->M_pembayaran_hutang->add($data)){
            $data = null;
            foreach($data2 as $row){
                $data['id_hutang_detail'] = $this->M_pembayaran_hutang->tampilkan_id_hutang_detail();
                if($data['id_hutang_detail']!=""){
                    foreach ($data['id_hutang_detail'] as $value) {
                        $urutan= substr($value->IDBSDet, 1);
                    }
                    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
                }else{
                    $urutan_id_detail = 'P000001';
                }
                $data = array(
                    'IDBSDet' => $urutan_id_detail,
                    'IDBS' => $urutan_id,
                    'IDFB' => $this->M_pembayaran_hutang->get_pembelian($row['No_Faktur'])->IDFaktur,
                    'Nomor' => $this->M_pembayaran_hutang->get_pembelian($row['No_Faktur'])->No_Faktur,
                    'Tanggal_fb' => $this->M_pembayaran_hutang->get_pembelian($row['No_Faktur'])->Tanggal_Hutang,
                    //'Nilai_fb' => ,
                    'Telah_diterima' => $row['Telah_diterima'],
                    'Diterima' => 0,
                    //'UM' => ,
                    //'Retur' => ,
                    //'Saldo' => ,
                    //'Selisih' =>
                    'IDHutang' => $row['IDHutang'],
                );
                $this->M_pembayaran_hutang->add_detail($data);

                $data['saldoakhir']= $this->M_pembayaran_hutang->get_saldo_akhir($row['No_Faktur']);

                $datahutang = array(
                  'Pembayaran' => $data['saldoakhir']->Pembayaran+$row['Telah_diterima'],
                  'Saldo_Akhir' => $data['saldoakhir']->Saldo_Akhir-$row['Telah_diterima']
              );
                $this->M_pembayaran_hutang->update_data_hutang($row['No_Faktur'], $datahutang);

                //jurnal
                $data['cekgroupsupplier'] = $this->M_invoice_pembelian->cek_group_supplier($data1['IDSupplier']);
                if ($data['cekgroupsupplier']->Group_Supplier == "PIHAK BERELASI") {
                    $groupsupplier = 'P000440';
                } elseif ($data['cekgroupsupplier']->Group_Supplier == "PIHAK KETIGA") {
                    $groupsupplier = 'P000439';
                }

                $data['id_jurnal'] = $this->M_pembayaran_hutang->tampilkan_id_jurnal();
                if($data['id_jurnal']!=""){
                  foreach ($data['id_jurnal'] as $value) {
                    $urutan= substr($value->IDJurnal, 1);
                }
                $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                $urutan_id_jurnal_debet_utang= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
            }else{
              $urutan_id_jurnal_debet_utang = 'P000001';
          }

          $save_jurnal_debet_utang = array(
              'IDJurnal'=>$urutan_id_jurnal_debet_utang,
              'Tanggal' => $data1['Tanggal_PH'],
              'Nomor' => $data1['nomor_PH'],
              'IDFaktur' => $urutan_id,
              'IDFakturDetail' => $urutan_id_detail,
              'Jenis_faktur' => 'PH',
              'IDCOA' => $groupsupplier,
              'Debet' => $row['Telah_diterima'],
              'Kredit' => 0,
              'IDMataUang' => 1,
              'Kurs' => 14000,
              'Total_debet' => 0,
              'Total_kredit' => $row['Telah_diterima'],
              'Keterangan' => $data1['Keterangan'],
              'Saldo' => $row['Telah_diterima'],
          );
          $this->M_pembayaran_hutang->save_jurnal($save_jurnal_debet_utang);

          $data['id_jurnal'] = $this->M_pembayaran_hutang->tampilkan_id_jurnal();
          if($data['id_jurnal']!=""){
              foreach ($data['id_jurnal'] as $value) {
                $urutan= substr($value->IDJurnal, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id_jurnal_kredit_bank= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_jurnal_kredit_bank = 'P000001';
      }

      $save_jurnal_kredit_bank = array(
          'IDJurnal'=>$urutan_id_jurnal_kredit_bank,
          'Tanggal' => $data1['Tanggal_PH'],
          'Nomor' => $data1['nomor_PH'],
          'IDFaktur' => $urutan_id,
          'IDFakturDetail' => $urutan_id_detail,
          'Jenis_faktur' => 'PH',
          'IDCOA' => $data1['namacoa'],
          'Debet' => 0,
          'Kredit' => $row['Telah_diterima'],
          'IDMataUang' => 1,
          'Kurs' => 14000,
          'Total_debet' => 0,
          'Total_kredit' =>$row['Telah_diterima'],
          'Keterangan' => $data1['Keterangan'],
          'Saldo' => $row['Telah_diterima'],
      );
      $this->M_pembayaran_hutang->save_jurnal($save_jurnal_kredit_bank);

  }

  foreach($data3 as $row2){
    $data['id_hutang_bayar'] = $this->M_pembayaran_hutang->tampilkan_id_hutang_bayar();
    if($data['id_hutang_bayar']!=""){
        foreach ($data['id_hutang_bayar'] as $value) {
            $urutan= substr($value->IDBSBayar, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_grand_total= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
        $urutan_id_grand_total = 'P000001';
    }
    if($row2['jenis_bayar'] == "Giro"){
        $data4 = array(
            'IDBSBayar' => $urutan_id_grand_total,
            'IDBS' => $urutan_id,
            'Jenis_pembayaran' => $row2['jenis_bayar'],
            'IDCOA' => $row2['namacoa'],
            'Nominal_pembayaran' => $row2['nilai'],
            'IDMataUang' => 1,
            'Kurs' => 14000,
             'Nomor_giro' => $row2['no_giro'],
             'Tanggal_giro' => $row2['tgl_giro']
        );
    }else{
        $data4 = array(
            'IDBSBayar' => $urutan_id_grand_total,
            'IDBS' => $urutan_id,
            'Jenis_pembayaran' => $row2['jenis_bayar'],
            'IDCOA' => $row2['namacoa'],
            'Nominal_pembayaran' => $row2['nilai'],
            'IDMataUang' => 1,
            'Kurs' => 14000,
        );
    }
    $this->M_pembayaran_hutang->add_bayar($data4);
}

$data['id_um_supplier']=$this->M_pembayaran_hutang->tampilkan_umsupplier();
if($data['id_um_supplier']!=""){
    foreach ($data['id_um_supplier'] as $value) {
        $urutan_um_supplier= substr($value->IDUMSupplier, 1);
    }
    $hasil_um_supplier = base_convert(base_convert($urutan_um_supplier, 36, 10) + 1, 10, 36);
    $urutan_id_um_supplier= 'P'.str_pad($hasil_um_supplier, 6, 0, STR_PAD_LEFT);
}else{
    $urutan_id_um_supplier = 'P000001';
}
$dataumsupplier= array(
    'IDUMSupplier' => $urutan_id_um_supplier,
    'Nomor_Faktur' => $data1['nomor_PH'],
    'IDSupplier' => $data1['IDSupplier'],
    'Nominal' => $data1['Pembayaran'],
    'IDFaktur' => $urutan_id,
    'Tanggal_UM' => $data1['Tanggal_PH']
);
$this->M_pembayaran_hutang->add_um_supplier($dataumsupplier);


echo true;
}else{
    echo false;
}
}

function show($id)
{
    $nomor = '';
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['data'] = $this->M_pembayaran_hutang->find($id, $nomor);
        //$data['invdetail'] = $this->M_pembayaran_hutang->find_detail($id, $nomor);
    $data['invdetail'] = "";
    $this->load->view('administrator/pembayaran_hutang/V_show', $data);
}

function print_($nomor)
{
    $id = '_';
    $data['print'] = $this->M_pembayaran_hutang->find($id, $nomor);
    $data['printdetail'] = $this->M_pembayaran_hutang->find_detail($id, $nomor);
    $this->load->view('administrator/pembayaran_hutang/V_print', $data);
}

public function status_gagal($id)
{
    $data = array(
        'Batal' => 'tidak aktif',
    );

    $this->M_pembayaran_hutang->update_status($data, array('IDBS' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('PembayaranHutang/index');
}

public function status_berhasil($id)
{
    $data = array(
        'Batal' => 'aktif',
    );

    $this->M_pembayaran_hutang->update_status($data, array('IDBS' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('PembayaranHutang/index');
}

public function delete_multiple()
{
    $ID_att = $this->input->post('msg');
    if($ID_att != ""){
        $result = array();
        foreach($ID_att AS $key => $val){
            $result[] = array(
                "IDBS" => $ID_att[$key],
                "Batal"  => 'tidak aktif'
            );
        }
        $this->db->update_batch('tbl_pembayaran_hutang', $result, 'IDBS');
        redirect("PembayaranHutang/index");
    }else{
        redirect("PembayaranHutang/index");
    }
}

function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['datas'] = $this->M_pembayaran_hutang->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), trim($this->input->post('keyword')));
    $this->load->view('administrator/pembayaran_hutang/V_index', $data);
}

function edit($id)
{
    $nomor = '';
    $data['bank'] = $this->M_pembayaran_hutang->tampil_bank();
    $data['supplier'] = $this->M_pembayaran_hutang->tampil_supplier();
    $data['data'] = $this->M_pembayaran_hutang->find($id, $nomor);
        //$data['detail'] = $this->M_pembayaran_hutang->find_detail($id, $nomor);
    $data['bayar'] = $this->M_pembayaran_hutang->find_bayar($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/pembayaran_hutang/V_edit', $data);
}

function hutang_bayar()
{
    $return['data']  = $this->M_pembayaran_hutang->hutang_bayar($this->input->post('IDSupplier'));
    if ($return['data']) {
        $return['error'] = false;
    }else{
        $return['error'] = true;
    }

    echo json_encode($return);
}

function update()
{
    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');

    $data = array(
        'Tanggal' => $data1['Tanggal_PH'],
        'Nomor' => $data1['nomor_PH'],
            //'IDSupplier' => $data1['IDSupplier'],
            //'IDMataUang' => ,
            //'Kurs' =>,
            //'Total' => $data1['total'],
        'Uang_muka' => $data1['Uang_muka'],
            //'Selisih' => ,
        'Kompensasi_um' => $data1['Kompensasi_um'],
    );

    $this->M_pembayaran_hutang->update_master($data1['IDBS'], $data);

    if($data2['namacoa']=="Cash")
    {
        $datas5 = array(
            'Jenis_pembayaran' => $data1['jenis'],
            'IDCOA' => 0,
                //'Nomor_giro' => ,
            'Nominal_pembayaran' => $data1['nominal'],
            'IDMataUang' => 1,
            'Kurs' => '14000',
                //'Status_giro' => ,
            'Tanggal_giro' => $data1['tgl_giro']
        );
    }else{
        $datas5 = array(
            'Jenis_pembayaran' => $data1['jenis'],
            'IDCOA' => $data1['namacoa'],
                //'Nomor_giro' => ,
            'Nominal_pembayaran' => $data1['nominal'],
            'IDMataUang' => 1,
            'Kurs' => '14000',
                //'Status_giro' => ,
            'Tanggal_giro' => $data1['tgl_giro']
        );
    }
    $this->M_pembayaran_hutang->update_bayar($data1['IDBS'], $datas5);

}
}