<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_barang_umum extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_penerimaan_barang_umum');
        $this->load->model('M_penerimaan_barang');
        $this->load->model('M_agen');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        $data['PenerimaanBU'] = $this->M_penerimaan_barang_umum->all();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/penerimaan_barang_umum/V_index', $data);
    }

    public function create()
    {
        $this->load->model('M_supplier');
        $this->load->model('M_corak');
        $this->load->model('M_warna');
        $this->load->model('M_satuan');

        $data['satuan'] = $this->M_satuan->get_all();
        $data['warna'] = $this->M_warna->tampilkan_warna();
        $data['corak'] = $this->M_corak->all();
        $data['supplier'] = $this->M_supplier->get_all();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
       $data['namaagent'] = $this->M_agen->ambil_agen_byid(1);
        $data['last_number'] = $this->M_penerimaan_barang_umum->getCodeReception(date('Y'));
        $this->load->view('administrator/penerimaan_barang_umum/V_create', $data);
    }

    function getColorByStyle()
    {
        $id_corak = $this->input->post('id_corak');
        if ($id_corak) {
            $return['data'] = $this->M_penerimaan_barang_umum->getColorByStyle($id_corak);
        } else {
            $return['data'] = '';
        }

        $return['error'] = false;
        echo json_encode($return);
    }

    function store()
    {
       if(!empty($this->input->post('data'))){
            $data = $this->input->post('data');
        }

        if(!empty($this->input->post('Total'))){
            $total = $this->input->post('Total');
        }

        foreach($data as $val){

            if ($this->M_penerimaan_barang_umum->check_noPB($val['NoPB'])->num_rows() == 0) {
                $save = array(
                    'Tanggal' => $val['Tanggal'],
                    'Nomor' => $val['NoPB'],
                    'IDSupplier' => $val['ID_supplier'] ? $val['ID_supplier'] : $this->input->post('id_supplier'),
                    'Nomor_sj' => $val['NoSJ'],
                    'IDPO' => $this->M_penerimaan_barang->get_IDPO($val['NoPO'])->IDPO ? $this->M_penerimaan_barang->get_IDPO($val['NoPO'])->IDPO : 0,
                    'NoSO' => $val['NoSO'] ? $val['NoSO'] : '-',
                    'Total_qty_yard' => $total[0],
                    'Total_qty_meter' => $total[1],
                    'Saldo_yard' => $total[0],
                    'Saldo_meter' => $total[1],
                    'Keterangan' => $val['Keterangan'] ? $val['Keterangan'] : null,
                    'Jenis_TBS' => 'PBU',
                    'Batal' => 'Aktif'
                );

                //save barang supplier
                $IDTBS = $this->M_penerimaan_barang_umum->save($save);
            }
            echo $IDTBS;
            //save barang supplier detail
            $save_detail = array(
                'IDTBS' => $IDTBS,
                'Barcode' => $val['Barcode'],
                'NoSO' => $val['NoSO'] ? $val['NoSO'] : '-',
                'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : 0,
                //'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : 0,
                'IDCorak' => $val['Corak'],
                'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : 0,
                'IDMerk' => $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk ? $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk : 0,
                'Qty_yard' => $val['Qty_yard'],
                'Qty_meter' => $val['Qty_meter'],
                'Saldo_yard' => $val['Qty_yard'],
                'Saldo_meter' => $val['Qty_meter'],
                'Grade' => $val['Grade'],
                'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
                'Harga' => 0
            );

            $this->M_penerimaan_barang_umum->save_detail($save_detail);

            //save in
            /*$save_in = array(
                'Barcode' => $val['Barcode'],
                'NoSO' => $val['NoSO'],
                'Party' => $val['Party'],
                'Indent' => $val['Indent'],
                'IDBarang' => 1,
                'IDCorak' => $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak : 0,
                'IDWarna' => $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna : 0,
                'IDMerk' => 1,
                'Panjang_yard' => $val['Qty_yard'],
                'Panjang_meter' => $val['Qty_meter'],
                'Grade' => $val['Grade'],
                'Lebar' => $val['Lebar'],
                'IDSatuan' => $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan : 0
            );

            $this->M_penerimaan_barang->save_in($save_in);

            //save stok
            $save_stock = array(
                'Tanggal' => $val['Tanggal'],
                'Nomor_faktur' => '',
                'IDFaktur' => 1,
                'IDFakturDetail' => 1,
                'Jenis_faktur' => '',
                'Barcode' => $val['Barcode'],
                'IDBarang' => $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang : 0,
                'IDCorak' => $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak : 0,
                'IDWarna' => $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna : 0,
                'IDGudang' => $this->M_penerimaan_barang->get_IDgudang()->IDGudang ? $this->M_penerimaan_barang->get_IDgudang()->IDGudang : 0,
                'Qty_yard' => $val['Qty_yard'],
                'Qty_meter' => $val['Qty_meter'],
                'Saldo_yard' => $val['Qty_yard'],
                'Saldo_meter' => $val['Qty_meter'],
                'Grade' => $val['Grade'],
                'IDSatuan' => $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
                'Harga' => 0,
                'IDMataUang' => 1,
                'Kurs' => '',
                'Total' => 0
            );

            $ID_stock = $this->M_penerimaan_barang->save_stock($save_stock);

            //save stock card
            $save_stock = array(
                'Tanggal' => $val['Tanggal'],
                'Nomor_faktur' => '',
                'IDFaktur' => 1,
                'IDFakturDetail' => 1,
                'IDStok' => $ID_stock,
                'Jenis_faktur' => '',
                'Barcode' => $val['Barcode'],
                'IDBarang' => $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang : 0,
                'IDCorak' => $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak : 0,
                'IDWarna' => $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna : 0,
                'IDGudang' => $this->M_penerimaan_barang->get_IDgudang()->IDGudang ? $this->M_penerimaan_barang->get_IDgudang()->IDGudang : 0,
                'Masuk_yard' => $val['Qty_yard'],
                'Masuk_meter' => $val['Qty_meter'],
                'Keluar_yard' => $val['Qty_yard'],
                'Keluar_meter' => $val['Qty_meter'],
                'Grade' => $val['Grade'],
                'IDSatuan' => $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
                'Harga' => 0,
                'IDMataUang' => 1,
                'Kurs' => '',
                'Total' => 0
            );

            $this->M_penerimaan_barang->save_stock_card($save_stock);*/
        }
    }

    function show($id)
    {
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $data['data'] = $this->M_penerimaan_barang_umum->find($id);
        // $data['detail'] = $this->M_penerimaan_barang_umum->findDetail($id);
         $data['pbudetail'] = $this->M_penerimaan_barang_umum->findDetail($id);
        $this->load->view('administrator/penerimaan_barang_umum/V_show', $data);
    }

    function soft_delete($id)
    {
        $this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
        $this->M_penerimaan_barang_umum->soft_delete($id);
        redirect(base_url('Penerimaan_barang_umum'));
    }

    function soft_success($id)
    {
        $this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diaktifkan</div>");
        $this->M_penerimaan_barang_umum->soft_success($id);
        redirect(base_url('Penerimaan_barang_umum'));
    }

    function bulk()
    {
        if ($this->input->post('bulk')) {
            $this->M_penerimaan_barang_umum->bulk($this->input->post('bulk'));
            $this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
        }
        redirect(base_url('Penerimaan_barang_umum'));
    }

    function pencarian()
    {
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $data['PenerimaanBU'] = $this->M_penerimaan_barang_umum->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
        $this->load->view('administrator/penerimaan_barang_umum/V_index', $data);
    }

    function edit($id)
    {
        $this->load->model('M_supplier');
        $this->load->model('M_corak');
        $this->load->model('M_warna');
        $this->load->model('M_satuan');

        $data['satuan'] = $this->M_satuan->get_all();
        $data['warna'] = $this->M_warna->tampilkan_warna();
        $data['corak'] = $this->M_corak->all();
        $data['supplier'] = $this->M_supplier->get_all();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $data['data'] = $this->M_penerimaan_barang_umum->find($id);
        $data['detail'] = $this->M_penerimaan_barang_umum->findDetail($id);
        $this->load->view('administrator/penerimaan_barang_umum/V_edit', $data);
    }

    public function update()
    {
        $data1 = $this->input->post('data1');
        $data2 = $this->input->post('data2');
        $data3 = $this->input->post('data3');

        $data = array(
            'Tanggal' => $data1['Tanggal'],
            'IDSupplier' => $data1['IDSupplier'],
            'Keterangan' => $data1['Keterangan']
        );

        if ($this->M_penerimaan_barang_umum->update($data, $data1['IDTBS'])) {
            $data = null;
            if ($data2 > 0) {
                foreach ($data2 as $row) {
                    $this->M_penerimaan_barang_umum->drop($row['IDTBSDetail'], $data1['IDTBS']);
                }
            }

            $tbs = $this->M_penerimaan_barang_umum->find($data1['IDTBS']);

            $total_yard = $tbs->Total_qty_yard;
            $total_meter = $tbs->Total_qty_meter;

            foreach($data3 as $val){

                //update table tbs
                $data_total = array(
                    'Total_qty_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
                    'Total_qty_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
                    'Saldo_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
                    'Saldo_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
                );
                $this->M_penerimaan_barang_umum->update($data_total, $data1['IDTBS']);

                //update barang supplier detail
                $save_detail = array(
                    'Barcode' => $val['Barcode'],
                    'NoSO' => $val['NoSO'] ? $val['NoSO'] : '-',
                    'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : $val['IDBarang'],
                    'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
                    'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : $val['IDWarna'],
                    'IDMerk' => $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk ? $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk : $val['IDMerk'],
                    'Qty_yard' => $val['Qty_yard'],
                    'Qty_meter' => $val['Qty_meter'],
                    'Saldo_yard' => $val['Qty_yard'],
                    'Saldo_meter' => $val['Qty_meter'],
                    'Grade' => $val['Grade'],
                    'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : $val['IDSatuan'],
                    'Harga' => 0
                );

                $this->M_penerimaan_barang_umum->update_detail($save_detail, $val['IDTBSDetail']);
            }

            echo true;
        } else {
            echo false;
        }
    }

    public function updateChange()
    {
        $data1 = $this->input->post('data1');
        $data2 = $this->input->post('data2');
        $data3 = $this->input->post('data3');

        $data = array(
            'Tanggal' => $data1['Tanggal'],
            'IDSupplier' => $data1['IDSupplier'],
            'Keterangan' => $data1['Keterangan']
        );

        if ($this->M_penerimaan_barang_umum->update($data, $data1['IDTBS'])) {
            $data = null;
            //delete detail
            if ($data2 > 0) {
                foreach ($data2 as $row) {
                    $this->M_penerimaan_barang_umum->drop($row['IDTBSDetail'], $data1['IDTBS']);
                }
            }

            $tbs = $this->M_penerimaan_barang_umum->find($data1['IDTBS']);

            $total_yard = $tbs->Total_qty_yard;
            $total_meter = $tbs->Total_qty_meter;

            //save detail
            if ($data3 > 0) {
                foreach($data3 as $val) {
                    if ($val['IDTBSDetail'] != null || $val['IDTBSDetail'] != '') {
                        //$this->M_penerimaan_barang_umum->drop_detail($val['IDTBSDetail'], $data1['IDTBS']);
                    }
                }
            }
            //print_r($data3);
            foreach($data3 as $val){

                //jika data detail baru
                if ($val['IDTBSDetail'] == null || $val['IDTBSDetail'] == '') {
                    //update table tbs
                    $data_total = array(
                        'Total_qty_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
                        'Total_qty_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
                        'Saldo_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
                        'Saldo_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
                    );
                    $this->M_penerimaan_barang_umum->update($data_total, $data1['IDTBS']);

                    //update barang supplier detail
                    $save_detail = array(
                        'IDTBS' => $data1['IDTBS'],
                        'Barcode' => $val['Barcode'],
                        'NoSO' => $val['NoSO'] ? $val['NoSO'] : '-',
                        'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : $val['IDBarang'],
                        //'IDCorak' => $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak != null ? $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak : $val['IDCorak'],
                        'IDCorak' => is_numeric($val['Corak']) ? $val['Corak'] : $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak,
                        'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna != null ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : $val['IDWarna'],
                        'IDMerk' => $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk ? $this->M_penerimaan_barang_umum->get_IDmerk($val['Merk'])->IDMerk : NULL,
                        'Qty_yard' => $val['Qty_yard'],
                        'Qty_meter' => $val['Qty_meter'],
                        'Saldo_yard' => $val['Qty_yard'],
                        'Saldo_meter' => $val['Qty_meter'],
                        'Grade' => $val['Grade'],
                        'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : $val['IDSatuan'],
                        'Harga' => 0
                    );
                    //print_r($save_detail);

                    $this->M_penerimaan_barang_umum->save_detail($save_detail);
                }

                //list detail sudah ada
                if ($val['IDTBSDetail'] != null || $val['IDTBSDetail'] != '') {
                    $data_total = array(
                        'Total_qty_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
                        'Total_qty_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
                        'Saldo_yard' => ($total_yard - $val['Saldo_yard']) + $val['Qty_yard'],
                        'Saldo_meter' => ($total_meter - $val['Saldo_meter']) + $val['Qty_meter'],
                    );
                    $this->M_penerimaan_barang_umum->update($data_total, $data1['IDTBS']);
                    print_r($data_total);

                    //update barang supplier detail
                    $save_detail = array(
                        'NoSO' => $val['NoSO'] ? $val['NoSO'] : '-',
                        'IDBarang' => $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang_umum->get_IDbarang($val['Corak'])->IDBarang : $val['IDBarang'],
                        'IDCorak' => is_numeric($val['Corak']) ? $val['Corak'] : $this->M_penerimaan_barang_umum->get_IDcorak($val['Corak'])->IDCorak,
                        'IDWarna' => $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna != null ? $this->M_penerimaan_barang_umum->get_IDwarna($val['Warna'])->IDWarna : $val['IDWarna'],
                        'IDMerk' => $val['IDMerk'],
                        'Qty_yard' => $val['Qty_yard'],
                        'Qty_meter' => $val['Qty_meter'],
                        'Saldo_yard' => $val['Qty_yard'],
                        'Saldo_meter' => $val['Qty_meter'],
                        'Grade' => $val['Grade'],
                        'IDSatuan' => $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang_umum->get_IDsatuan($val['Satuan'])->IDSatuan : $val['IDSatuan']
                    );
                    //print_r($save_detail);

                    $this->M_penerimaan_barang_umum->update_detail($save_detail, $val['IDTBSDetail']);
                }
            }
            //print_r($save_detail);
            //die();
            echo true;
        } else {
            echo false;
        }
    }

    public function get_satuan()
    {
        $data = $this->M_penerimaan_barang_umum->get_satuan();
        echo json_encode($data);
    }

    public function get_corak()
    {
        $data = $this->M_penerimaan_barang_umum->get_corak();
        echo json_encode($data);
    }

    public function get_warna()
    {
        $data = $this->M_penerimaan_barang_umum->get_warna();
        echo json_encode($data);
    }

    public function print_data(){
        $sjc = $this->uri->segment('3');
        $data['data'] = $this->M_penerimaan_barang_umum->getPrint($sjc);
         $id= $data['data']->IDTBS;
        $data['detail'] = $this->M_penerimaan_barang_umum->getDetail_tbs($id);
        $this->load->view('administrator/penerimaan_barang_umum/V_print', $data);
    }

}