<?php

class ReturPembelian extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_retur_pembelian');
    $this->load->model('M_invoice_pembelian');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
  }

  public function index()
  {
    $data['retur'] = $this->M_retur_pembelian->tampilkan_retur_pembelian();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/retur_pembelian/V_retur_pembelian.php', $data);
  }

  public function tambah_retur_pembelian()
  {
    $data['inv'] = $this->M_retur_pembelian->tampil_inv();
    $data['koderetur'] = $this->M_retur_pembelian->no_retur(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['supplier'] = $this->M_retur_pembelian->tampil_supplier();
    $data['grandtotal'] = $this->M_retur_pembelian->tampil_invoice_grand_total();
    $this->load->view('administrator/retur_pembelian/V_tambah_retur_pembelian.php', $data);
  }

  function check_invoice_pembelian()
  {
    $return['data']  = $this->M_retur_pembelian->check_invoice($this->input->post('Nomor'));
    if ($return['data']) {
      $return['error'] = false;
    }else{
      $return['error'] = true;
    }

    echo json_encode($return);
  }
  public function delete_multiple()
  {
    $ID_att = $this->input->post('msg');
    if($ID_att != ""){
     $result = array();
     foreach($ID_att AS $key => $val){
       $result[] = array(
        "IDRB" => $ID_att[$key],
        "Batal"  => 'tidak aktif'
      );
     }
     $this->db->update_batch('tbl_retur_pembelian', $result, 'IDRB');
     redirect("ReturPembelian/index");
   }else{
     redirect("ReturPembelian/index");
   }
   
 }
 function cek_grand_total()
 {
  $IDFB = $this->input->post('Nomor');
  $data = $this->M_retur_pembelian->check_grandtotal($IDFB);
  echo json_encode($data);
}

function simpan_retur_pembelian()
{
  $data1 = $this->input->post('data1');
  $data2 = $this->input->post('data2');
    // $data3 = $this->input->post('data3');
  $data['id_retur'] = $this->M_retur_pembelian->tampilkan_id_retur();

  if($data['id_retur']!=""){
    // foreach ($data['id_retur'] as $value) {
      $urutan= substr($data['id_retur']->IDRB, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }

  $data = array(
    'IDRB' => $urutan_id,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['RB'],
    'IDFB' => $this->M_retur_pembelian->get_IDFB($data1['Nomor'])->IDFB ? $this->M_retur_pembelian->get_IDFB($data1['Nomor'])->IDFB : 0,
    'IDSupplier' => $data1['IDSupplier'],
    'Tanggal_fb' => $data1['TanggalInv'],
    'Total_qty_yard' => $data1['totalqty'],
    'Total_qty_meter' => $data1['totalmeter'],
    'Saldo_yard' => $data1['totalqty'],
    'Saldo_meter' => $data1['totalmeter'],
    'Discount' => $data1['Discount_total'],
    'Status_ppn' => $this->M_retur_pembelian->get_IDFB($data1['Nomor'])->Status_ppn ? $this->M_retur_pembelian->get_IDFB($data1['Nomor'])->Status_ppn : 0,
    'Keterangan' => $data1['Keterangan'],
    'Batal' => 'aktif',
  );
  $this->M_retur_pembelian->add($data);


  // if($this->M_retur_pembelian->add($data)){
    // $last_insert_id = $this->db->insert_id(); 
  $data = null;
  foreach($data2 as $row){
    $data['id_retur_detail'] = $this->M_retur_pembelian->tampilkan_id_retur_detail();
    if($data['id_retur_detail']!=""){
      // foreach ($data['id_retur_detail'] as $value) {
        $urutan= substr($data['id_retur_detail']->IDRBDetail, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }
    $data = array(
      'IDRBDetail' => $urutan_id_detail,
      'IDRB' => $urutan_id,
      'Barcode' => $row['Barcode'],
      'NoSO' => $row['NoSO'],
      'Party' => $row['Party'],
      'Indent' => $row['Indent'],
      'IDBarang' => $row['IDBarang'],
      'IDCorak' => $row['IDCorak'],
      'IDWarna' => $row['IDWarna'],
      'IDMerk' => $row['IDMerk'],
      'Qty_yard' => $row['Qty_yard'],
      'Qty_meter' => $row['Qty_meter'],
      'Saldo_yard' => $row['Qty_yard'],
      'Saldo_meter' => $row['Qty_meter'],
      'Grade' => $row['Grade'],
      'Remark' => $row['Remark'],
      'Lebar' => $row['Lebar'],
      'IDSatuan' => $row['IDSatuan'],
      'Harga' => str_replace(".", "",$row['Harga']),
      'Subtotal' => str_replace(".", "",$row['Sub_total']),
    );
    $this->M_retur_pembelian->add_detail($data);

      //save stock
    $getstokbyid = $this->M_retur_pembelian->getstokbyid($row['Barcode']);
    $data_stok = array(
      'Saldo_yard' => $getstokbyid->Saldo_yard - $row['Qty_yard'] ,
      'Saldo_meter' => $getstokbyid->Saldo_meter - $row['Qty_meter']
    );
    $this->M_retur_pembelian->update_stok($row['Barcode'], $data_stok);


            //save stock card
    $data['id_kartu_stok'] = $this->M_retur_pembelian->tampilkan_id_kartu_stok();
    if($data['id_kartu_stok']!=""){
      // foreach ($data['id_kartu_stok'] as $value) {
        $urutan= substr($data['id_kartu_stok']->IDKartuStok, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail_kartustok= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail_kartustok = 'P000001';
    }
    $save_stock_card = array(
      'IDKartuStok'=>$urutan_id_detail_kartustok,
      'Tanggal' => $data1['Tanggal'],
      'Nomor_faktur' => $data1['RB'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'IDStok' => '-',
      'Jenis_faktur' => 'RB',
      'Barcode' => $row['Barcode'],
      'IDBarang' => $row['IDBarang'],
      'IDCorak' => $row['IDCorak'],
      'IDWarna' => $row['IDWarna'],
      'IDGudang' => 0,
      'Masuk_yard' => 0,
      'Masuk_meter' => 0,
      'Keluar_yard' => $row['Qty_yard'],
      'Keluar_meter' => $row['Qty_meter'],
      'Grade' => $row['Grade'],
      'IDSatuan' => $row['IDSatuan'],
      'Harga' => str_replace(".", "", $row['Harga']),
      'IDMataUang' => 1,
      'Kurs' => '',
      'Total' => str_replace(".", "",$row['Sub_total']),
    );

    $this->M_retur_pembelian->save_stock_card($save_stock_card);

    // $datainvoice= $this->M_retur_pembelian->getinvoice($data1['FBID']);
    // debet
    $data['cekgroupsupplier'] = $this->M_invoice_pembelian->cek_group_supplier($data1['IDSupplier']);
      if ($data['cekgroupsupplier']->Group_Supplier == "PIHAK BERELASI") {
        $groupsupplier = 'P000440';
      } elseif ($data['cekgroupsupplier']->Group_Supplier == "PIHAK KETIGA") {
        $groupsupplier = 'P000439';
      }

    $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal = 'P000001';
    }

    $save_jurnal_debet_utang = array(
      'IDJurnal'=>$urutan_id_jurnal,
      'Tanggal' =>  $data1['Tanggal'],
      'Nomor' =>  $data1['RB'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'RB',
      'IDCOA' => $groupsupplier,
      'Debet' => str_replace(".", "", $row['Sub_total']) + str_replace(".", "", $row['Sub_total']) * 0.1,
      'Kredit' => 0,
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => str_replace(".", "", $row['Sub_total']) + str_replace(".", "", $row['Sub_total']) * 0.1,
      'Total_kredit' => 0,
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => str_replace(".", "", $row['Sub_total']) + str_replace(".", "", $row['Sub_total']) * 0.1,
    );
    $this->M_retur_pembelian->save_jurnal($save_jurnal_debet_utang);

      $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_kredit_ppn= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit_ppn = 'P000001';
    }

    $save_jurnal_kredit_ppn = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit_ppn,
      'Tanggal' =>  $data1['Tanggal'],
      'Nomor' =>  $data1['RB'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'RB',
      'IDCOA' => 'P000022',
      'Debet' => 0,
      'Kredit' =>str_replace(".", "",$row['Sub_total'])*0.1,
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => str_replace(".", "",$row['Sub_total'])*0.1,
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => str_replace(".", "",$row['Sub_total'])*0.1,
    );
    $this->M_retur_pembelian->save_jurnal($save_jurnal_kredit_ppn);

    $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_kredit_persediaan= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit_persediaan = 'P000001';
    }

    $save_jurnal_kredit_persediaan = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit_persediaan,
      'Tanggal' =>  $data1['Tanggal'],
      'Nomor' =>  $data1['RB'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'RB',
      'IDCOA' => 'P000016',
      'Debet' => 0,
      'Kredit' =>str_replace(".", "",$row['Sub_total']),
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => str_replace(".", "",$row['Sub_total']),
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => str_replace(".", "",$row['Sub_total']),
    );
    $this->M_retur_pembelian->save_jurnal($save_jurnal_kredit_persediaan);

    // $datapemhutang= $this->M_retur_pembelian->getpembayaranhutang($data1['FBID']);
    // $datahutang= $this->M_retur_pembelian->gethutang($data1['Nomor']);
    // $data['hutangsupplier']= $this->M_retur_pembelian->gethutangsupplier($data1['Nomor']);

    // if($datapemhutang['IDFB'] != $data1['FBID'] || $datahutang['Saldo_Akhir'] > 0)
    // {
    //   $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
    //   if($data['id_jurnal']!=""){
    //     foreach ($data['id_jurnal'] as $value) {
    //       $urutan= substr($value->IDJurnal, 1);
    //     }
    //     $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    //     $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    //   }else{
    //     $urutan_id_jurnal = 'P000001';
    //   }

    //   $save_jurnal_debet_utang = array(
    //     'IDJurnal'=>$urutan_id_jurnal,
    //     'Tanggal' =>  $data1['Tanggal'],
    //     'Nomor' =>  $data1['RB'],
    //     'IDFaktur' => $urutan_id,
    //     'IDFakturDetail' => $urutan_id_detail,
    //     'Jenis_faktur' => 'RB',
    //     'IDCOA' => 'P000057',
    //     'Debet' => str_replace(".", "", $row['Harga']),
    //     'Kredit' => 0,
    //     'IDMataUang' => 1,
    //     'Kurs' => 14000,
    //     'Total_debet' => str_replace(".", "", $row['Harga']),
    //     'Total_kredit' => 0,
    //     'Keterangan' => $data1['Keterangan'],
    //     'Saldo' => str_replace(".", "", $row['Harga']),
    //   );
    //   $this->M_retur_pembelian->save_jurnal($save_jurnal_debet_utang);
    // }


    //kredit
    // $perhitunganppn= str_replace(".", "",$row['Harga'])*0.1;
    // $perhitunganpersediaan= str_replace(".", "",$row['Harga'])-$perhitunganppn;

    // $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
    // if($data['id_jurnal']!=""){
    //   foreach ($data['id_jurnal'] as $value) {
    //     $urutan= substr($value->IDJurnal, 1);
    //   }
    //   $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    //   $urutan_id_jurnal_persediaan= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    // }else{
    //   $urutan_id_jurnal_persediaan = 'P000001';
    // }

    // $save_jurnal_debet_persediaan = array(
    //   'IDJurnal'=>$urutan_id_jurnal_persediaan,
    //   'Tanggal' =>  $data1['Tanggal'],
    //   'Nomor' =>  $data1['RB'],
    //   'IDFaktur' => $urutan_id,
    //   'IDFakturDetail' => $urutan_id_detail,
    //   'Jenis_faktur' => 'RB',
    //   'IDCOA' => 'P000022',
    //   'Debet' => 0,
    //   'Kredit' => str_replace(".", "",$row['Harga']),
    //   'IDMataUang' => 1,
    //   'Kurs' => 14000,
    //   'Total_debet' => 0,
    //   'Total_kredit' => str_replace(".", "",$row['Harga']),
    //   'Keterangan' => $data1['Keterangan'],
    //   'Saldo' => str_replace(".", "",$row['Harga']),
    // );
    // $this->M_retur_pembelian->save_jurnal($save_jurnal_debet_persediaan);


    // $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
    // if($data['id_jurnal']!=""){
    //   foreach ($data['id_jurnal'] as $value) {
    //     $urutan= substr($value->IDJurnal, 1);
    //   }
    //   $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    //   $urutan_id_jurnal_kredit_ppn= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    // }else{
    //   $urutan_id_jurnal_kredit_ppn = 'P000001';
    // }

    // $save_jurnal_kredit_ppn = array(
    //   'IDJurnal'=>$urutan_id_jurnal_kredit_ppn,
    //   'Tanggal' =>  $data1['Tanggal'],
    //   'Nomor' =>  $data1['RB'],
    //   'IDFaktur' => $urutan_id,
    //   'IDFakturDetail' => $urutan_id_detail,
    //   'Jenis_faktur' => 'RB',
    //   'IDCOA' => 'P000066',
    //   'Debet' => 0,
    //   'Kredit' =>str_replace(".", "",$row['Harga']),
    //   'IDMataUang' => 1,
    //   'Kurs' => 14000,
    //   'Total_debet' => 0,
    //   'Total_kredit' => str_replace(".", "",$row['Harga']),
    //   'Keterangan' => $data1['Keterangan'],
    //   'Saldo' => str_replace(".", "",$row['Harga']),
    // );
    // $this->M_retur_pembelian->save_jurnal($save_jurnal_kredit_ppn);

    // $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
    // if($data['id_jurnal']!=""){
    //   foreach ($data['id_jurnal'] as $value) {
    //     $urutan= substr($value->IDJurnal, 1);
    //   }
    //   $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    //   $urutan_id_jurnal_kredit_hpp= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    // }else{
    //   $urutan_id_jurnal_kredit_hpp = 'P000001';
    // }

    // $save_jurnal_kredit_hpp = array(
    //   'IDJurnal'=>$urutan_id_jurnal_kredit_hpp,
    //   'Tanggal' =>  $data1['Tanggal'],
    //   'Nomor' =>  $data1['RB'],
    //   'IDFaktur' => $urutan_id,
    //   'IDFakturDetail' => $urutan_id_detail,
    //   'Jenis_faktur' => 'RB',
    //   'IDCOA' => 'P000091',
    //   'Debet' => 0,
    //   'Kredit' =>str_replace(".", "",$row['Harga']),
    //   'IDMataUang' => 1,
    //   'Kurs' => 14000,
    //   'Total_debet' => 0,
    //   'Total_kredit' => str_replace(".", "",$row['Harga']),
    //   'Keterangan' => $data1['Keterangan'],
    //   'Saldo' => str_replace(".", "",$row['Harga']),
    // );
    // $this->M_retur_pembelian->save_jurnal($save_jurnal_kredit_hpp);

  }
  $data['id_retur_grand_total'] = $this->M_retur_pembelian->tampilkan_id_retur_grand_total();
  if($data['id_retur_grand_total']!=""){
    // foreach ($data['id_retur_grand_total'] as $value) {
      $urutan= substr($data['id_retur_grand_total']->IDRBGrandTotal, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_grand_total= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id_grand_total = 'P000001';
  }

  $data4 = array(
    'IDRBGrandTotal' => $urutan_id_grand_total,
    'IDRB' => $urutan_id,
    'IDMataUang' =>1,
    'Kurs' => '14000',
      // 'Pembayaran' => $data1['Pembayaran'],
    'DPP' => str_replace(".", "",$data1['DPP']),
    'Discount' => str_replace(".", "",$data1['Discount_total']),
    'PPN' => str_replace(".", "",$data1['PPN']),
    'Grand_total' => str_replace(".", "",$data1['total_invoice']),
    'Sisa' => 1
  );
  $this->M_retur_pembelian->add_grand_total($data4);

  $data['saldoakhir']= $this->M_retur_pembelian->get_saldo_akhir($data1['Nomor']);

  $datahutang = array(
    'Pembayaran' => $data['saldoakhir']->Pembayaran+str_replace(".", "",$data1['total_invoice']),
    'Saldo_Akhir' => str_replace(".", "",$data1['total_invoice'])-$data['saldoakhir']->Saldo_Akhir,
    'Retur' => str_replace(".", "",$data1['total_invoice'])
  );
  $this->M_retur_pembelian->update_data_hutang($data1['Nomor'], $datahutang);
  echo json_encode($data);
 //  }else{
 //   echo false;
 // }
}

public function edit($id)
{
  $data['detail'] = $this->M_retur_pembelian->detail_retur_pembelian($id);
  $data['supplier'] = $this->M_retur_pembelian->tampil_supplier();
  $data['retur'] = $this->M_retur_pembelian->getById($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/retur_pembelian/V_edit_retur_pembelian', $data);
}

public function ubah_retur_pembelian()
{
  $data1 = $this->input->post('data1');
  $data2 = $this->input->post('data2');
  $data3 = $this->input->post('data3');

  $data = array(
   'Tanggal' => $data1['Tanggal'],
   'Nomor' => $data1['RB'],
   'IDFB' => $this->M_retur_pembelian->get_IDFB($data1['Nomor'])->IDFB ? $this->M_retur_pembelian->get_IDFB($data1['Nomor'])->IDFB : 0,
   'IDSupplier' => $data1['IDSupplier'],
   'Tanggal_fb' => $data1['TanggalInv'],
   'Total_qty_yard' => $data1['totalqty'],
   'Total_qty_meter' => $data1['totalmeter'],
   'Saldo_yard' => $data1['totalqty'],
   'Saldo_meter' => $data1['totalmeter'],
   'Discount' => $data1['Discount_total'],
   'Status_ppn' => $this->M_retur_pembelian->get_IDFB($data1['Nomor'])->Status_ppn ? $this->M_retur_pembelian->get_IDFB($data1['Nomor'])->Status_ppn : 0,
   'Keterangan' => $data1['Keterangan'],
   'Batal' => 'aktif',
 );

  if($this->M_retur_pembelian->update_master($data1['IDRB'],$data)){
    $data = null;
    if($data3 > 0){
     foreach($data3 as $row){
       $this->M_retur_pembelian->drop($row['IDRBDetail']);
     }
   }
   if($data2 != null){
    foreach($data2 as $row){
      $data = array(
            // 'IDRB'=>$data1['IDFBA'],
        'IDRB' => $data1['IDRB'],
        'Barcode' => $row['Barcode'],
        'NoSO' => $row['NoSO'],
        'Party' => $row['Party'],
        'Indent' => $row['Indent'],
        'IDBarang' => $this->M_retur_pembelian->get_IDbarang($row['Corak'])->IDBarang ? $this->M_retur_pembelian->get_IDbarang($row['Corak'])->IDBarang : 0,
        'IDCorak' => $this->M_retur_pembelian->get_IDcorak($row['Corak'])->IDCorak ? $this->M_retur_pembelian->get_IDcorak($row['Corak'])->IDCorak : 0,
        'IDWarna' => $this->M_retur_pembelian->get_IDwarna($row['Warna'])->IDWarna ? $this->M_retur_pembelian->get_IDwarna($row['Warna'])->IDWarna : 0,
        'IDMerk' => $this->M_retur_pembelian->get_IDcorak($row['Corak'])->IDMerk ? $this->M_retur_pembelian->get_IDcorak($row['Corak'])->IDMerk : 0,
        'Qty_yard' => $row['Qty_yard'],
        'Qty_meter' => $row['Qty_meter'],
        'Saldo_yard' => $row['Qty_yard'],
        'Saldo_meter' => $row['Qty_meter'],
        'Grade' => $row['Grade'],
        'Remark' => $row['Remark'],
        'Lebar' => $row['Lebar'],
        'IDSatuan' => $this->M_retur_pembelian->get_IDsatuan($row['Satuan'])->IDSatuan ? $this->M_retur_pembelian->get_IDsatuan($row['Satuan'])->IDSatuan : 0,
        'Harga' => $row['harga_satuan'],
        'Subtotal' => $row['harga_total']
      );

      if($row['IDRBDetail'] ==''){
        $this->M_retur_pembelian->add_detail($data);
      }elseif($row['IDRBDetail'] !=''){
        $this->M_retur_pembelian->drop($row['IDRBDetail'], $data);      
      }
    }
  }

  $datas = array(
    'IDRB' => $data1['IDRB'],
    'Pembayaran' => $data1['Pembayaran'],
    'IDMataUang' => 1,
    'Kurs' => '14000',
    'DPP' => $data1['DPP'],
    'Discount' => $data1['Discount_total'],
    'PPN' => $data1['PPN'],
    'Grand_total' => $data1['total_invoice'],
    'Sisa' => 2,
  );
  $this->M_retur_pembelian->update_grand_total($data1['IDRB'],$datas);

  echo true;

}else{
  echo false;
}
}
public function status_gagal($id)
{
  $data = array(
    'Batal' => 'tidak aktif',
  );

  $this->M_retur_pembelian->update_status($data, array('IDRB' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('ReturPembelian/index');
}

public function status_berhasil($id)
{
  $data = array(
    'Batal' => 'aktif',
  );

  $this->M_retur_pembelian->update_status($data, array('IDRB' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('ReturPembelian/index');
}
function pencarian()
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['retur'] = $this->M_retur_pembelian->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/retur_pembelian/V_retur_pembelian', $data);
}

function show($id)
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['data'] = $this->M_retur_pembelian->find($id);
  $data['datadetail'] = $this->M_retur_pembelian->find_detail($id);
  $this->load->view('administrator/retur_pembelian/V_show', $data);
}

function print($nomor)
{

  $data['print'] = $this->M_retur_pembelian->print_retur_pembelian($nomor);
  $id= $data['print']->IDRB;
  $data['printdetail'] = $this->M_retur_pembelian->print_retur_pembelian_detail($id);
  $data['printgrandtotal'] = $this->M_retur_pembelian->print_grand_total($id);
  $this->load->view('administrator/retur_pembelian/V_print', $data);
}

public function laporan_retur_pembelian()
{

      $data['laporanretur']= $this->M_retur_pembelian->get_all_procedure_retur(); //Load Data Purchase Order
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/retur_pembelian/V_laporan_retur_pembelian', $data);
    }

    function pencarian_store()
    {
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu'] = $this->M_login->aksesmenu();
      $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
      if ($this->input->post('date_from') == '') {
        $data['laporanretur'] = $this->M_retur_pembelian->searching_store_like($this->input->post('keyword'));
      } else {
        $data['laporanretur'] = $this->M_retur_pembelian->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
      }
      $this->load->view('administrator/retur_pembelian/V_laporan_retur_pembelian', $data);
    }

    public function exporttoexcel(){
      // create file name
      $fileName = 'RB '.time().'.xlsx';  
    // load excel library
      $this->load->library('excel');
      $empInfo = $this->M_retur_pembelian->exsport_excel();
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->setActiveSheetIndex(0);
        // set Header
      $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Tanggal');
      $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nomor');  
      $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Supplier'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Barcode'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'NoSO'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Party'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Barang'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Corak'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Merk'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Warna'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Qty Yard'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Qty Meter'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Grade'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Remark'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Lebar'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Satuan'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Harga'); 
      $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Sub Total'); 
        // set Row
      $rowCount = 2;
      foreach ($empInfo as $element) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['Tanggal']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['Nomor']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['Nama']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['Barcode']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['NoSO']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['Party']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['Nama_Barang']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['Corak']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['Merk']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['Warna']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['Saldo_yard']);
        $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['Saldo_meter']);
        $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['Grade']);
        $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['Remark']);
        $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['Lebar']);
        $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['Satuan']);
        $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['Harga']);
        $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $element['Subtotal']);
        $rowCount++;
      }
      $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
      $objWriter->save($fileName);
    // download file
      header("Content-Type: application/vnd.ms-excel");
      redirect($fileName);        
    }
  }

  ?>