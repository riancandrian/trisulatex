<?php

class Laporan_pembelian_asset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_laporan_pembelian_asset');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        $data['p_asset'] = $this->M_laporan_pembelian_asset->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_pembelian_asset/V_index', $data);
    }

    function pencarian()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
            $data['aksesmenu'] = $this->M_login->aksesmenu();
      $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
      if ($this->input->post('date_from') == '') {
        $data['p_asset'] = $this->M_laporan_pembelian_asset->searching_store_like($this->input->post('keyword'));
      } else {
        $data['p_asset'] = $this->M_laporan_pembelian_asset->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
      }
        $this->load->view('administrator/laporan_pembelian_asset/V_index', $data);
    }
}