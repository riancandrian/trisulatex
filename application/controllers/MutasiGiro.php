<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class MutasiGiro extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_mutasi_giro', 'M');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
  }

  public function index()
  {
    $data = array(
      'title_master' => "Master Mutasi Giro", //Judul 
      'title' => "List Data Mutasi Giro", //Judul Tabel
      'action' => site_url('MutasiGiro/create'), //Alamat Untuk Action Form
      'button' => "Tambah Data", //Nama Button
      'mutasigiro' => $this->M->get_all() //Load Data Mutasi Giro
    );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/mutasigiro/V_mutasigiro', $data);
  }


  //----------------------Aksi Ubah Batal single
  public function set_edit($id)
  {
    $cek = $this->M->get_by_id($id);
    //die(print_r($cek));
    $batal = "-";
    if ($cek->Batal == "aktif") {
      $batal ="tidak aktif";
    } else {
      $batal = "aktif";
    }
    $data = array(
      'Batal' => $batal
    );

    $this->M->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('MutasiGiro/index');
  }

  //----------------------Aksi Ubah Batal multiple
  public function delete_multiple()
  {
    $ID_att = $this->input->post('msg');
    if (empty($ID_att)) {
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
      redirect("MutasiGiro/index");
    }
    $result = array();
    foreach($ID_att AS $key => $val){
      $result[] = array(
        "IDMG" => $ID_att[$key],
        "Batal"  => 'tidak aktif'
      );
    }
    $this->db->update_batch('tbl_mutasi_giro', $result, 'IDMG');
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect("MutasiGiro/index");
  }

  //Tambah Data Mutasi Giro
  public function create()
  {




    $data['kodemutasigiro'] = $this->M->no_mutasigiro(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $data['giro'] = $this->M->get_giro();
    $data['bank'] = $this->M->get_bank();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    //die(print_r($data['giro']));
    $this->load->view('administrator/mutasigiro/V_tambah_mutasigiro', $data);
  }

  //Ajax get data giro
  public function getgiro()
  {
    $giro = $this->input->post('id_giro');
    $data = $this->M->getgiro($giro);
    echo json_encode($data);
    
  }

  //Ajax get data bank
  public function getbank()
  {
    $giro = $this->input->post('id_bank');
    $data = $this->M->getbank($giro);
    echo json_encode($data);
    
  }


 //SImpan Data Ajax
  public function simpan_mutasigiro(){

    $data1 = $this->input->post('_data1');
    $data2 = $this->input->post('_data2');
    $data['id_mutasi_giro'] = $this->M->tampilkan_id_mutasi_giro();
    if($data['id_mutasi_giro']!=""){
      foreach ($data['id_mutasi_giro'] as $value) {
        $urutan= substr($value->IDMG, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data_atas = array(
      'IDMG' => $urutan_id,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDMataUang' => $data1['IDMataUang'],
      'Kurs' => $data1['Kurs'],
      'Total' => $data1['Total'],
      'Keterangan' => $data1['Keterangan'],
      'Batal' => $data1['Batal'],
    );
    $last_insert_id = $this->M->add($data_atas); 
    $data_bawah = null;
    foreach($data2 as $row){
     $data['id_mutasi_giro_detail'] = $this->M->tampilkan_id_mutasi_giro_detail();
     if($data['id_mutasi_giro_detail']!=""){
      foreach ($data['id_mutasi_giro_detail'] as $value) {
        $urutan= substr($value->IDMGDetail, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }
    $data_bawah = array(
      'IDMGDetail'=> $urutan_id_detail,
      'IDMG'=>$urutan_id,
      'Jenis_giro' => $row['Jenis_giro'],
      'IDGiro' => $row['IDGiro'],
      'Status' => $row['Status'],
      'Nomor_lama' => $row['Nomor_lama'],
      'Nomor_baru' => $row['Nomor_baru'],

    );
    $this->M->add_detail($data_bawah);

  //save jurnal
    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($value->IDJurnal, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal = 'P000001';
    }

    $save_jurnal_debet = array(
      'IDJurnal'=>$urutan_id_jurnal,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'MG',
      'IDCOA' => 'P000058',
      'Debet' => $row['Harga'],
      'Kredit' => 0,
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => $row['Harga'],
      'Total_kredit' => 0,
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => $row['Harga'],
    );
    $this->M->save_jurnal($save_jurnal_debet);

    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($value->IDJurnal, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_kredit_bank= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit_bank = 'P000001';
    }

    $save_jurnal_kredit_bank = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit_bank,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'MG',
      'IDCOA' => $row['NamaBank'],
      'Debet' => 0,
      'Kredit' => $row['NilaiGiro'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => $row['Harga'],
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => $row['Harga'],
    );
    $this->M->save_jurnal($save_jurnal_kredit_bank);

     $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($value->IDJurnal, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_debet_bank= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_debet_bank = 'P000001';
    }

    $save_jurnal_debet_bank = array(
      'IDJurnal'=>$urutan_id_jurnal_debet_bank,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'MG',
      'IDCOA' => $row['NamaBank'],
      'Debet' => 0,
      'Kredit' => $row['NilaiGiro'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => $row['Harga'],
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => $row['Harga'],
    );
    $this->M->save_jurnal($save_jurnal_debet_bank);

    $data['id_jurnal'] = $this->M->tampilkan_id_jurnal();
    if($data['id_jurnal']!=""){
      foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($value->IDJurnal, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_jurnal_kredit_piutang= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal_kredit_piutang = 'P000001';
    }

    $save_jurnal_kredit_piutang = array(
      'IDJurnal'=>$urutan_id_jurnal_kredit_piutang,
      'Tanggal' => $data1['Tanggal'],
      'Nomor' => $data1['Nomor'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'MG',
      'IDCOA' => 'P000016',
      'Debet' => 0,
      'Kredit' => $row['NilaiGiro'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => $row['Harga'],
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => $row['Harga'],
    );
    $this->M->save_jurnal($save_jurnal_kredit_piutang);

  }
  //$this->M->add($data_atas);
  echo json_encode($data_bawah);

}


  //----------------------Detail per data
public function show($id)
{
  $data['mutasigiro'] = $this->M->get_by_id($id);
  $data['mutasigirodetail'] = $this->M->get_mutasigiro_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/mutasigiro/V_show', $data);
}

  //Ubah data mutasigiro
public function edit($id)
{
  $data['mutasigiro'] = $this->M->get_by_id($id);
  $data['datamutasigirodetail'] = $this->M->get_mutasigiro_detail($id);
    // $data['kodemutasigiro'] = $this->M->no_mutasigiro();
    //$data['po'] = $this->M_po->get_all('PO Celup');
  $data['giro'] = $this->M->get_giro();
  $data['bank'] = $this->M->get_bank();
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/mutasigiro/V_edit_mutasigiro', $data);
}

  //aksi Ubah mutasigiro
public function ubah_mutasigiro(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDMG = $this->input->post('_id');

  $data_atas = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total' => $data1['Total'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );


  if($this->M->update_master($IDMG,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M->drop($row['IDMGDetail']);
                // console.log('delete temp');
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
       $data['id_mutasi_giro_detail'] = $this->M->tampilkan_id_mutasi_giro_detail();
       if($data['id_mutasi_giro_detail']!=""){
        foreach ($data['id_mutasi_giro_detail'] as $value) {
          $urutan= substr($value->IDMGDetail, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_detail = 'P000001';
      }
      $data_atas = array(
        'IDMGDetail'=> $urutan_id_detail,
        'IDMG'=>$IDMG,
        'Jenis_giro' => $row['Jenis_giro'],
        'IDGiro' => $row['IDGiro'],
        'Status' => $row['Status'],
        'Nomor_lama' => $row['Nomor_lama'],
        'Nomor_baru' => $row['Nomor_baru'],
      );
      if($row['IDMGDetail'] == ''){
        $this->M->add_detail($data_atas);
      }
    }
  }
  echo true;
}else{
  echo false;  
      // echo json_encode($data_atas);
}

      //$this->M->update_master($IDMG,$data_atas);
      //echo json_encode($data3);

}

  //--------------------------------pencarian
function pencarian()
{
  $data = array(
      'title_master' => "Master Mutasi Giro", //Judul 
      'title' => "List Data Mutasi Giro", //Judul Tabel
      'action' => site_url('MutasiGiro/create'), //Alamat Untuk Action Form
      'button' => "Tambah Data", //Nama Button
      //'mutasigiro' => $this->M->get_all() //Load Data Mutasi Giro
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['mutasigiro'] = $this->M->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/mutasigiro/V_mutasigiro', $data);
}

function print($nomor)
{

  $data['print'] = $this->M->print_mutasi_giro($nomor);
  $id= $data['print']->IDMG;
  $data['printdetail'] = $this->M->print_mutasi_giro_detail($id);
  $this->load->view('administrator/mutasigiro/V_print', $data);
}

}

/* End of file mutasigiro.php */
