<?php

class Laporan_bo extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_login');
        $this->load->model('M_booking_order');
        $this->load->model('M_laporan_bo');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        $data['booking_order'] = $this->M_laporan_bo->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/laporan_bo/V_index', $data);
    }

    function pencarian()
    {
      $data['aksessetting']= $this->M_login->aksessetting();
       $data['aksesmenu'] = $this->M_login->aksesmenu();
      $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
      if ($this->input->post('date_from') == '') {
        $data['booking_order'] = $this->M_laporan_bo->searching_store_like($this->input->post('keyword'));
      } else {
        $data['booking_order'] = $this->M_laporan_bo->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
      }
        $this->load->view('administrator/laporan_bo/V_index', $data);
    }

    public function exporttoexcel(){
      // create file name
        $fileName = 'Booking Order '.time().'.xlsx';  
    // load excel library
        $this->load->library('excel');
        $empInfo = $this->M_laporan_bo->exsport_excel();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nomor');  
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Tanggal Selesai'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Corak'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Qty'); 
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['Tanggal']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['Nomor']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['Tanggal_selesai']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['Corak']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Qty']);
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
    // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);        
    }

}