<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class So extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_so');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
    date_default_timezone_set('Asia/Jakarta');
  }
  
  public function index()
  {
    $data = array(
          'title_master' => "Master Sales Order", //Judul 
          'title' => "List Data Sales Order", //Judul Tabel
          'action' => site_url('so/create'), //Alamat Untuk Action Form
          'button' => "Tambah So", //Nama Button
          'so' => $this->M_so->get_all() //Load Data Sales Order
        );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/so/V_so', $data);
  }

  public function laporan_so()
  {
    $data = array(
        'title_master' => "Laporan Sales Order", //Judul 
        'title' => "Laporan Sales Order", //Judul Tabel
        'so' => $this->M_so->get_all_procedure() //Load Data Sales Order
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/so/V_laporan_so', $data);
  }

    //Tambah Data Sales Order
  public function create()
  {
    $this->load->model('M_corak');
    
    $data['kodeso'] = $this->M_so->no_so(date('Y'));
    $data['trisula'] = $this->M_so->get_customer();
    $data['satuan'] = $this->M_so->get_satuan();
    $data['corak'] = $this->M_corak->tampil_po_corak();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $this->load->view('administrator/so/V_tambah_so', $data);
  }

    //Tambah Data Sales Order
  // public function create_action()
  // {
  //   $data = array(
  //     'Kode_so' => $this->input->post('Kode_so'),
  //     'po' => $this->input->post('po'),
  //     'Aktif' => $this->input->post('Aktif')

  //   );
  //   $this->M_so->insert($data);
  //   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
  //   redirect('so/create');
  // }

    //Ubah data Sales Order
  public function edit($id)
  {
    $this->load->model('M_corak');
    $data['dataso'] = $this->M_so->get_by_id($id);
    $data['datasodetail'] = $this->M_so->get_so_detail($id);
    $data['kodeso'] = $this->M_so->no_so(date('Y'));
    $data['trisula'] = $this->M_so->get_customer();
    $data['satuan'] = $this->M_so->get_satuan();
    $data['corak'] = $this->M_corak->all();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/so/V_edit_so', $data);
  }


    //----------------------Aksi Ubah Batal single
  public function set_edit($id)
  {
   $cek = $this->M_so->get_by_id($id);
   $data['soget'] = $this->M_so->tampilkan_get_sj($id);

   if(count($data['soget']) > 0){
     if(($data['soget']->IDSOK!=$id)){   

       $batal = "-";
       if ($cek->Batal == "aktif") {
         $batal ="tidak aktif";
       } else {
         $batal = "aktif";
       }
       $data = array(
         'Batal' => $batal
       );

       $this->M_so->update($id,$data);
       $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
     }else{
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data SO Sudah Digunakan</div></center>");
    }
  }else{
   $batal = "-";
   if ($cek->Batal == "aktif") {
     $batal ="tidak aktif";
   } else {
     $batal = "aktif";
   }
   $data = array(
     'Batal' => $batal
   );

   $this->M_so->update($id,$data);
   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
 }
 redirect('so/index');
}

   //----------------------Aksi Ubah Batal multiple
public function delete_multiple()
{
  $ID_att = $this->input->post('msg');
  if (empty($ID_att)) {
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
    redirect("So/index");
  }
  $result = array();
  foreach($ID_att AS $key => $val){
   $result[] = array(
    "IDSOK" => $ID_att[$key],
    "Batal"  => 'tidak aktif'
  );
 }
 $this->db->update_batch('tbl_sales_order_kain', $result, 'IDSOK');
 $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
 redirect("So/index");
}

    //----------------------Detail per data
public function show($id)
{
  $data['so'] = $this->M_so->get_by_id($id);
  $data['sodetail'] = $this->M_so->get_so_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/so/V_show', $data);
}

    //------------------------Get Merk
public function getmerk()
{
  $id_corak = $this->input->post('id_corak');
  $data = $this->M_so->getmerk($id_corak);
  echo json_encode($data);
}

// ----------------------------- Get Harga
public function get_harga()
{
  $id_barang = $this->input->post('id_barang');
  $id_satuan = $this->input->post('id_satuan');
  
  $data = $this->M_so->get_harga($id_barang, $id_satuan);
  echo json_encode($data);
}


    //------------------------Get warna
public function getwarna()
{
  $id_kodecorak = $this->input->post('id_kodecorak');
  $data = $this->M_so->getwarna($id_kodecorak);
  echo json_encode($data);
}

public function simpan_so(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');

  $data['id_so'] = $this->M_so->tampilkan_id_so();
  if($data['id_so']!=""){
    // foreach ($data['id_so'] as $value) {
    $urutan= substr($data['id_so']->IDSOK, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }
  $data_atas = array(
    'IDSOK' => $urutan_id,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCustomer' => $data1['IDCustomer'],
    'TOP' => $data1['TOP'],
    'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo_qty' => $data1['Saldo'],
    'No_po_customer' => $data1['No_po_customer'],
    'Grand_total' => str_replace(",", "", $data1['Grand_total']),
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
    'Status_ppn' => $data1['Status_ppn'],
    'DPP' => str_replace(",", "", $data1['DPP']),
    'PPN' => str_replace(",", "", $data1['PPN'])
  );
  $this->M_so->add($data_atas); 
  $data_bawah = null;
  foreach($data2 as $row){
   $data['id_so_detail'] = $this->M_so->tampilkan_id_so_detail();
   if($data['id_so_detail']!=""){
    // foreach ($data['id_so_detail'] as $value) {
    $urutan= substr($data['id_so_detail']->IDSOKDetail, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
   $urutan_id_detail = 'P000001';
 }
 $data_bawah = array(
  'IDSOKDetail' => $urutan_id_detail,
  'IDSOK'=>$urutan_id,
  'IDBarang' => $row['IDBarang'],
  'IDCorak' => $row['Corak'],
  'IDMerk' => $row['IDMerk'],
  'IDWarna' => $row['IDWarna'],
  'Qty' => $row['Qty'],
  'IDSatuan' => $row['Satuan'],
  'Harga' => str_replace(",", "", $row['Harga']),
  'Sub_total' => str_replace(",", "", $row['Sub_total']),
);
 $this->M_so->add_detail($data_bawah);
 
}
      // $this->M_so->add($data_atas);
echo json_encode($data_bawah);

}

public function ubah_so(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDSOK = $this->input->post('_id');

  $data_atas = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCustomer' => $data1['IDCustomer'],
    // 'TOP' => $data1['TOP'],
    'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo_qty' => $data1['Saldo'],
    'No_po_customer' => $data1['No_po_customer'],
    'Grand_total' => str_replace(",", "", $data1['Grand_total']),
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );

  if($this->M_so->update_master($IDSOK,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M_so->drop($row['IDSOKDetail']);
                  // console.log('delete temp');
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
        $data['id_so_detail'] = $this->M_so->tampilkan_id_so_detail();
        if($data['id_so_detail']!=""){
          // foreach ($data['id_so_detail'] as $value) {
          $urutan= substr($data['id_so_detail']->IDSOKDetail, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail = 'P000001';
        }
        $data_atas = array(
          'IDSOKDetail' => $urutan_id_detail,
          'IDSOK'=>$IDSOK,
          'IDBarang' => $row['IDBarang'],
          'IDCorak' => $row['Corak'],
          'IDMerk' => $row['IDMerk'],
          'IDWarna' => $row['IDWarna'],
          'Qty' => $row['Qty'],
          'IDSatuan' => $row['Satuan'],
          'Harga' => str_replace(",", "", $row['Harga']),
          'Sub_total' => str_replace(",", "", $row['Sub_total']),
        );
        if($row['IDSOKDetail'] ==''){
          $this->M_so->add_detail($data_atas);
        }
      }
    }

    echo true;

  }else{
    echo false;  
        // echo json_encode($data_atas);
  }
      //   $this->M_so->update_master($IDSOK,$data_atas);
      //   if($data3 > 0){
      //       foreach($data3 as $row){
      //           $this->M_so->drop($row['IDSOKDetail']);
      //           //console.log('delete temp');
      //       }
      //   }


      //   foreach($data2 as $row2){
      //     $data_bawah = array(
      //         'IDSOK'=>$row2['IDSOK'],
      //         'IDWarna' => $row2['IDWarna'],
      //         'Qty' => $row2['Qty'],
      //         'Saldo' => $row2['Saldo'],
      //     );
      //     $this->M_so->add_detail($data_bawah);
      // }
        // echo json_encode($data2);
}

  //--------------------------------pencarian
function pencarian()
{
  $data = array(
        'title_master' => "Master Sales Order", //Judul 
        'title' => "List Data Sales Order", //Judul Tabel
        'action' => site_url('so/create'), //Alamat Untuk Action Form
        'button' => "Tambah So", //Nama Button
        //'po' => $this->M_so->get_all('PO TTI') //Load Data Sales Order
      );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['so'] = $this->M_so->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/so/V_so', $data);
}

function print_data_so($nomor)
{

  $data['print'] = $this->M_so->print_so($nomor);
  $id= $data['print']->IDSOK;
  $data['printdetail'] = $this->M_so->print_so_detail($id);
  $this->load->view('administrator/so/V_print', $data);
}
    //------------------------------------Pencarian Store
     //--------------------------------pencarian
function pencarian_store()
{
  $data = array(
      'title_master' => "Laporan Sales Order", //Judul 
      'title' => "Laporan Sales Order", //Judul Tabel
      //'po' => $this->M_so->get_all('PO TTI') //Load Data Sales Order
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  if ($this->input->post('date_from') == '') {
    $data['so'] = $this->M_so->searching_store_like($this->input->post('keyword'));
  } else {
    $data['so'] = $this->M_so->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
  }
  $this->load->view('administrator/so/V_laporan_so', $data);
}
}

/* End of file So */
