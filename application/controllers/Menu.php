<!-- Programmer : Rais Naufal Hawari
     Date       : 12-07-2018 -->

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

  function __construct()
	{
		parent::__construct();
		$this->load->model('M_menu');
    $this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
      $data = array(
        'title_master' => "Data Menu", //Judul 
        'title' => "List Data Menu", //Judul Tabel
        'action' => site_url('Menu/create'), //Alamat Untuk Action Form
        'button' => "Tambah Menu", //Nama Button
        'menu' => $this->M_menu->get_all() //Load data Menu
      );
      $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/menu/menu_list.php', $data);
  
  }
  
  //Tambah Data Menu
  public function create()
	{
      $data = array(
        'title_master' => "Master Menu", //Judul 
        'title' => "Tambah Data Menu" , //Judul Tabel
        'action' => site_url('Menu/create_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Menu'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'id_menu' => set_value('id_menu'), //Id 
        'menu' => set_value('menu'),
        'urutan_menu' => set_value('urutan_menu'), //Menu
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/menu/menu_form.php', $data);
  }
  
  //Tambah Data Menu
  public function create_action()
	{
      $data = array(
        'Menu' => $this->input->post('menu'),
        'Urutan_menu' => $this->input->post('urutan_menu') 
      );
      $this->M_menu->insert($data);
     $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
     redirect('Menu/create');
  }
  
  //Edit Data Menu
  public function update($id)
  {
  
    $cari = $this->M_menu->get_by_id($id);//Cari apakah data yang dimaksud ada di database

    if($cari) {
      $data = array(
        'title_master' => "Master Menu", //Judul 
        'title' => "Ubah Data Menu" , //Judul Tabel
        'action' => site_url('Menu/update_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Menu'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'id_menu' => set_value('id_menu', $cari->IDMenu) ,
        'menu' => set_value('menu', $cari->Menu) , 
        'urutan_menu' => set_value('urutan_menu', $cari->Urutan_menu) , 
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/menu/menu_form.php', $data);
    } else {
      redirect('Menu/index');
    }
  }

  //Aksi Ubah Data
  public function update_action()
  {

    $id = $this->input->post('id_menu'); //Get id sebagai penanda record yang akan dirubah
    $data = array(
      'Menu' => $this->input->post('menu'),
      'Urutan_menu' => $this->input->post('urutan_menu')
    );
    $this->M_menu->update($id,$data);
   
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
    redirect('Menu/index');
  }

  //Hapus Data Menu
  public function delete($id)
  {
      $cari = $this->M_menu->get_by_id($id); //Cari apakah data yang dimaksud ada di database

      if ($cari) {
        $this->M_menu->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
        redirect('Menu/index');
      } else {
       $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Gagal di Hapus</div></center>");
       redirect('Menu/index');
      }
      
  }

  public function pencarian()
  {

    $keyword= addslashes($this->input->post('keyword'));
    $data = array(
        'title_master' => "Master Menu", //Judul 
        'title' => "List Data Menu", //Judul Tabel
        'action' => site_url('Menu/create'), //Alamat Untuk Action Form
        'button' => "Tambah Menu", //Nama Button
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    if ($keyword!="") {
      $data['menu']=$this->M_menu->cari_by_keyword($keyword);
    }else {
      $data['menu']=$this->M_menu->get_all();
    }
      $this->load->view('administrator/menu/menu_list.php', $data);
  }
  

}

/* End Menu.php */
