<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SuratJalan extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_suratjalan');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
    date_default_timezone_set('Asia/Jakarta');
  }
  
  public function index()
  {
    $data = array(
          'title_master' => "Master Surat Jalan", //Judul 
          'title' => "List Data Surat Jalan", //Judul Tabel
          'action' => site_url('suratjalan/create'), //Alamat Untuk Action Form
          'button' => "Tambah Suerat Jalan", //Nama Button
          'suratjalan' => $this->M_suratjalan->get_all() //Load Data Surat Jalan
        );

    //die(print_r($data['suratjalan']));
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/suratjalan/V_suratjalan', $data);
  }

    //--------------------Store Laporan So
  public function laporan_surat_jalan_kain()
  {
    $data = array(
        'title_master' => "Laporan Surat Jalan Kain", //Judul 
        'title' => "Laporan Surat Jalan Kain", //Judul Tabel
        'sj' => $this->M_suratjalan->get_all_procedure()
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/suratjalan/V_laporan_suratjalan', $data);
  }

    //Tambah Data Surat Jalan
  public function create()
  {
    $this->load->model('M_corak');
    
    $data['kodesuratjalan'] = $this->M_suratjalan->no_suratjalan(date('Y'));
    
    $data['packinglist'] = $this->M_suratjalan->get_packinglist();
    $data['tbl_packinglist'] = $this->M_suratjalan->get_tbl_packinglist();
    //$data['satuan'] = $this->M_suratjalan->get_satuan();
    $data['corak'] = $this->M_corak->all();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $this->load->view('administrator/suratjalan/V_tambah_suratjalan', $data);
  }


    //Ubah data Surat Jalan
  public function edit($id)
  {
    $this->load->model('M_corak');
    
    $data['datasuratjalan'] = $this->M_suratjalan->get_by_id($id);
    $data['datasuratjalandetail'] = $this->M_suratjalan->get_suratjalan_detail($id);
    $data['kodesuratjalan'] = $this->M_suratjalan->no_suratjalan(date('Y'));
    $data['packinglist'] = $this->M_suratjalan->get_packinglist();
    $data['satuan'] = $this->M_suratjalan->get_satuan();
    $data['corak'] = $this->M_corak->all();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/suratjalan/V_edit_suratjalan', $data);
  }


    //----------------------Aksi Ubah Batal single
  public function set_edit($id)
  {
   $cek = $this->M_suratjalan->get_by_id($id);
   $data['invoiceget'] = $this->M_suratjalan->tampilkan_get_invoice();

  if(count($data['invoiceget']) > 0){
    foreach($data['invoiceget'] as $valueinv) {
   if(($valueinv->IDSJCK != $id)){   

   $batal = "-";
   if ($cek->Batal == "aktif") {
     $batal ="tidak aktif";
   } else {
     $batal = "aktif";
   }
   $data = array(
     'Batal' => $batal
   );

   $this->M_suratjalan->update($id,$data);
   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    }else{
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Surat Jalan Sudah Digunakan</div></center>");
    }
  }
  }else{
     $batal = "-";
   if ($cek->Batal == "aktif") {
     $batal ="tidak aktif";
   } else {
     $batal = "aktif";
   }
   $data = array(
     'Batal' => $batal
   );

   $this->M_suratjalan->update($id,$data);
   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  }
    redirect('SuratJalan/index');
 }

   //----------------------Aksi Ubah Batal multiple
 public function delete_multiple()
 {
  $ID_att = $this->input->post('msg');
  if (empty($ID_att)) {
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
    redirect("SuratJalan/index");
  }
  $result = array();
  foreach($ID_att AS $key => $val){
   $result[] = array(
    "IDSJCK" => $ID_att[$key],
    "Batal"  => 'tidak aktif'
  );
 }
 $this->db->update_batch('tbl_purchase_order', $result, 'IDSJCK');
 $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
 redirect("SuratJalan/index");
}

    //----------------------Detail per data
public function show($id)
{
  $data['suratjalan'] = $this->M_suratjalan->get_by_id($id);
  $data['suratjalandetail'] = $this->M_suratjalan->get_suratjalan_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/suratjalan/V_show', $data);
}

    //------------------------Get Merk
public function getmerk()
{
  $id_corak = $this->input->post('id_corak');
  $data = $this->M_suratjalan->getmerk($id_corak);
  echo json_encode($data);
}

    //------------------------Get warna
public function getwarna()
{
  $id_kodecorak = $this->input->post('id_kodecorak');
  $data = $this->M_suratjalan->getwarna($id_kodecorak);
  echo json_encode($data);
}

//----------------------------Get Detail Packinglist
public function get_detailpackinglist()
{
  $IDPAC = $this->input->post('idPAC');
  $data = $this->M_suratjalan->get_detailpackinglist($IDPAC);
  echo json_encode($data);
  
}


public function simpan_suratjalan(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');

  $data['id_suratjalan'] = $this->M_suratjalan->tampilkan_id_suratjalan();
  if($data['id_suratjalan']!=""){
    // foreach ($data['id_suratjalan'] as $value) {
      $urutan= substr($data['id_suratjalan']->IDSJCK, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }
  $data_atas = array(
    'IDSJCK' => $urutan_id,
    'IDPAC' => $data1['IDPAC'],
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCustomer' => $data1['IDCustomer'],
    'Tanggal_kirim' => $data1['Tanggal_kirim'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo_qty' => $data1['Saldo_qty'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );
  $this->M_suratjalan->add($data_atas); 
  $data_bawah = null;
  foreach($data2 as $row){
   $data['id_suratjalan_detail'] = $this->M_suratjalan->tampilkan_id_suratjalan_detail();
   if($data['id_suratjalan_detail']!=""){
    // foreach ($data['id_suratjalan_detail'] as $value) {
      $urutan= substr($data['id_suratjalan_detail']->IDSJCKDetail, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
   $urutan_id_detail = 'P000001';
 }
 $data_bawah = array(
  'IDSJCKDetail' => $urutan_id_detail,
  'IDSJCK'=>$urutan_id,
  'IDBarang' => $this->M_suratjalan->get_IDbarang($row['Nama_Barang'])->IDBarang ? $this->M_suratjalan->get_IDbarang($row['Nama_Barang'])->IDBarang : $row['Nama_Barang'],
  'IDCorak' => $this->M_suratjalan->get_IDcorak($row['Corak'])->IDCorak ? $this->M_suratjalan->get_IDcorak($row['Corak'])->IDCorak : $row['Corak'],
  'IDMerk' => $this->M_suratjalan->get_IDmerk($row['Merk'])->IDMerk ? $this->M_suratjalan->get_IDmerk($row['Merk'])->IDMerk : $row['Merk'],
  'IDWarna' => $this->M_suratjalan->get_IDwarna($row['Warna'])->IDWarna ? $this->M_suratjalan->get_IDwarna($row['Warna'])->IDWarna : $row['Warna'],
  'Qty_yard' => $row['yardnew'],
  'Qty_roll' => $row['meternew'],
  'Saldo_yard' => $row['yardnew'],
  'Saldo_roll' =>$row['meternew'],
  'IDSatuan' => $this->M_suratjalan->get_IDsatuan($row['Satuan'])->IDSatuan ? $this->M_suratjalan->get_IDsatuan($row['Satuan'])->IDSatuan : $row['Satuan'],
  'Grade' => $row['Grade']
);
 $this->M_suratjalan->add_detail($data_bawah);

 $getpacdetail= $this->M_suratjalan->getpacdetail($data1['IDPAC']);
 $data['getpld'] = $this->M_suratjalan->getpldbyid($getpacdetail['IDPACDetail']);
 foreach ($data['getpld'] as $valuepac) {

  $ubahstok = array(
    'Qty_yard' => $getpld->Qty_yard - $row['yardnew'] ,
    'Qty_meter' => $getpld->Qty_meter - $row['meternew']
  );
  $this->M_suratjalan->updatepld($ubahstok, $valuepac['IDPACDetail']);
}

}

      // $this->M_suratjalan->add($data_atas);
echo json_encode($data_bawah);

}

public function ubah_suratjalan(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDSJCK = $this->input->post('_id');

  $data_atas = array(
    'IDPAC' => $data1['IDPAC'],
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCustomer' => $data1['IDCustomer'],
    'Tanggal_kirim' => $data1['Tanggal_kirim'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo_qty' => $data1['Saldo_qty'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );

  if($this->M_suratjalan->update_master($IDSJCK,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M_suratjalan->drop($row['IDSJCKDetail']);
                  // console.log('delete temp');
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
        $data['id_suratjalan_detail'] = $this->M_suratjalan->tampilkan_id_suratjalan_detail();
        if($data['id_suratjalan_detail']!=""){
          // foreach ($data['id_suratjalan_detail'] as $value) {
            $urutan= substr($data['id_suratjalan_detail']->IDSJCKDetail, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail = 'P000001';
        }
        $data_atas = array(
          'IDSJCKDetail' => $urutan_id_detail,
          'IDSJCK'=>$IDSJCK,
          'IDBarang' => $row['IDBarang'],
          'IDCorak' => $row['IDCorak'],
          'IDMerk' => $row['IDMerk'],
          'IDWarna' => $row['IDWarna'],
          'Qty_yard' => $row['yardnew'],
          'Qty_roll' => $row['meternew'],
          'Saldo_yard' => $row['Saldo_yard'],
          'Saldo_roll' => $row['Saldo_meter'],
          'IDSatuan' => $row['IDSatuan'],
// <<<<<<< Updated upstream
          'Grade' => $row['Grade'],
// =======
//           'Grade' => $row['Grade']
// >>>>>>> Stashed changes
        );
        if($row['IDSJCKDetail'] ==''){
          $getpacdetail= $this->M_suratjalan->getpacdetail($data1['IDPAC']);
          $data['getpld'] = $this->M_suratjalan->getpldbyid($getpacdetail['IDPACDetail']);
          foreach ($data['getpld'] as $valuepac) {

            $ubahstok = array(
              'Qty_yard' => $getpld->Qty_yard - $row['yardnew'] ,
              'Qty_meter' => $getpld->Qty_meter - $row['meternew']
            );
            $this->M_suratjalan->updatepld($ubahstok, $valuepac['IDPACDetail']);
          }
        }
      }
    }

    echo true;

  }else{
    echo false;  
        // echo json_encode($data_atas);
  }
      //   $this->M_suratjalan->update_master($IDSJCK,$data_atas);
      //   if($data3 > 0){
      //       foreach($data3 as $row){
      //           $this->M_suratjalan->drop($row['IDSJCKDetail']);
      //           //console.log('delete temp');
      //       }
      //   }


      //   foreach($data2 as $row2){
      //     $data_bawah = array(
      //         'IDSJCK'=>$row2['IDSJCK'],
      //         'IDWarna' => $row2['IDWarna'],
      //         'Qty' => $row2['Qty'],
      //         'Saldo' => $row2['Saldo'],
      //     );
      //     $this->M_suratjalan->add_detail($data_bawah);
      // }
        // echo json_encode($data2);
}

  //--------------------------------pencarian
function pencarian()
{
  $data = array(
        'title_master' => "Master Surat Jalan", //Judul 
        'title' => "List Data Surat Jalan", //Judul Tabel
        'action' => site_url('suratjalan/create'), //Alamat Untuk Action Form
        'button' => "Tambah Suerat Jalan", //Nama Button
        //'po' => $this->M_suratjalan->get_all('PO TTI') //Load Data Surat Jalan
      );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['suratjalan'] = $this->M_suratjalan->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/suratjalan/V_suratjalan', $data);
}

function print_data_suratjalan($nomor)
{

  $data['print'] = $this->M_suratjalan->print_suratjalan($nomor);
  $id= $data['print']->IDSJCK;
  $data['printdetail'] = $this->M_suratjalan->print_suratjalan_detail($nomor);
  $this->load->view('administrator/suratjalan/V_print', $data);
}
    //------------------------------------Pencarian Store
     //--------------------------------pencarian
function pencarian_store()
{
  $data = array(
      'title_master' => "Laporan Surat Jalan", //Judul 
      'title' => "Laporan Surat Jalan", //Judul Tabel
      //'po' => $this->M_suratjalan->get_all('PO TTI') //Load Data Surat Jalan
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  if ($this->input->post('date_from') == '') {
    $data['sj'] = $this->M_suratjalan->searching_store_like($this->input->post('keyword'));
  } else {
    $data['sj'] = $this->M_suratjalan->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
  }
    $this->load->view('administrator/suratjalan/V_laporan_suratjalan', $data);
}
}

/* End of file So */
