<?php

class InvoicePenjualan extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_invoice_penjualan');
		$this->load->model('M_agen');
		$this->load->model('M_login');
		$this->load->model('M_piutang');
		$this->load->helper('convert_function');
		$this->load->helper('form', 'url');
	}

	public function index() {
		$data['invoice'] = $this->M_invoice_penjualan->all();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_penjualan/V_invoice_penjualan', $data);
	}

	public function tambah_invoice_penjualan() {
		$data['sj'] = $this->M_invoice_penjualan->tampil_sj();
		$data['customers'] = $this->M_invoice_penjualan->get_customer();
		$data['bank'] = $this->M_invoice_penjualan->tampil_bank();
		$data['kodenoinv'] = $this->M_invoice_penjualan->no_invoice(date('Y'));
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_penjualan/V_tambah_invoice_penjualan', $data);
	}

	function cek_customer() {
		$Nomor = $this->input->post('Nomor');
		$data = $this->M_invoice_penjualan->cekcustomer($Nomor);
		echo json_encode($data);
	}

	public function check_surat_jalan() {
		$return['data'] = $this->M_invoice_penjualan->check_surat_jalan($this->input->post('Nomor'));
		if ($return['data']) {
			$return['error'] = false;
		} else {
			$return['error'] = true;
		}

		echo json_encode($return);
	}

	public function simpan_invoice_penjualan() {
		$this->load->model('M_invoice_pembelian');

		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		$data3 = $this->input->post('data3');

		$data['id_invoice'] = $this->M_invoice_penjualan->tampilkan_id_invoice();
		if ($data['id_invoice'] != "") {
			// foreach ($data['id_invoice'] as $value) {
				$urutan = substr($data['id_invoice']->IDFJK, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id = 'P000001';
		}

		$data_penjualan_kain = array(
			'IDFJK' => $urutan_id,
			'Tanggal' => $data1['Tanggal'],
			'Nomor' => $data1['Nomor'],
			'IDCustomer' => $data1['IDCustomer'],
			'Nama_di_faktur' => $data1['Nama_di_faktur'],
			'IDSJCK' => $this->M_invoice_penjualan->get_TPKbyNomor($data1['Nomor_sj'])->IDSJCK ? $this->M_invoice_penjualan->get_TPKbyNomor($data1['Nomor_sj'])->IDSJCK : 0,
			//'TOP' => $data1['Jatuh_tempo'],
			'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
			'IDMataUang' => 1,
			'Kurs' => 14000,
			'Total_pieces' => $data1['Total_pieces'],
			'Total_yard' => $data1['Total_yard'],
			'Saldo_pieces' => $data1['Total_pieces'],
			'Saldo_yard' => $data1['Total_yard'],
			'Discount' => $data1['Discount'],
			'Status_ppn' => $data1['Status_ppn'],
			'Keterangan' => $data1['Keterangan'],
			'Batal' => 'aktif',
			'Total_harga' => str_replace(".", "", $data3['total_invoice']),
		);

		if ($this->M_invoice_penjualan->add($data_penjualan_kain)) {
			$data = null;
			foreach ($data2 as $row) {
				$data['id_invoice_detail'] = $this->M_invoice_penjualan->tampilkan_id_invoice_detail();
				if ($data['id_invoice_detail'] != "") {
					// foreach ($data['id_invoice_detail'] as $value) {
						$urutan = substr($data['id_invoice_detail']->IDFJKDetail, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_detail_inv = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_detail_inv = 'P000001';
				}

				$data = array(
					'IDFJKDetail' => $urutan_id_detail_inv,
					'IDFJK' => $urutan_id,
					'IDBarang' => $row['IDBarang'],
					'IDCorak' => $row['IDCorak'],
					'IDWarna' => $row['IDWarna'],
					'Qty_roll' => $row['Qty_roll'],
					'Saldo_qty_roll' => $row['Qty_roll'],
					'Qty_yard' => $row['Qty_yard'],
					'Saldo_yard' => $row['Qty_yard'],
					'IDSatuan' => $row['IDSatuan'],
					'Harga' => str_replace(".", "", $row['Harga_satuan']),
					'Sub_total' => str_replace(".", "", $row['Subtotal']),
				);
				$this->M_invoice_penjualan->add_detail($data);

				$perhitunganppn = $row['Subtotal'] * 0.1;
				$penguranganppn = $row['Subtotal'] + $perhitunganppn;

				$data['id_jurnal'] = $this->M_invoice_penjualan->tampilkan_id_jurnal();
				if ($data['id_jurnal'] != "") {
					// foreach ($data['id_jurnal'] as $value) {
						$urutan = substr($data['id_jurnal']->IDJurnal, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_jurnal_kredit_pen = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_jurnal_kredit_pen = 'P000001';
				}

				$save_jurnal_kredit_penj = array(
					'IDJurnal' => $urutan_id_jurnal_kredit_pen,
					'Tanggal' => $data1['Tanggal'],
					'Nomor' => $data1['Nomor'],
					'IDFaktur' => $urutan_id,
					'IDFakturDetail' => $urutan_id_detail_inv,
					'Jenis_faktur' => 'INVP',
					'IDCOA' => 'P000465',
					'Debet' => 0,
					'Kredit' => str_replace(".", "", $row['Subtotal']),
					'IDMataUang' => 1,
					'Kurs' => 14000,
					'Total_debet' => 0,
					'Total_kredit' => str_replace(".", "", $row['Subtotal']),
					'Keterangan' => $data1['Keterangan'],
					'Saldo' => str_replace(".", "", $row['Subtotal']),
				);
				$this->M_invoice_penjualan->save_jurnal($save_jurnal_kredit_penj);

				$data['id_jurnal'] = $this->M_invoice_penjualan->tampilkan_id_jurnal();
				if ($data['id_jurnal'] != "") {
					// foreach ($data['id_jurnal'] as $value) {
						$urutan = substr($data['id_jurnal']->IDJurnal, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_jurnal_kredit_ppn = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_jurnal_kredit_ppn = 'P000001';
				}

				$save_jurnal_kredit_ppn = array(
					'IDJurnal' => $urutan_id_jurnal_kredit_ppn,
					'Tanggal' => $data1['Tanggal'],
					'Nomor' => $data1['Nomor'],
					'IDFaktur' => $urutan_id,
					'IDFakturDetail' => $urutan_id_detail_inv,
					'Jenis_faktur' => 'INVP',
					'IDCOA' => 'P000445',
					'Debet' => 0,
					'Kredit' => str_replace(".", "", $perhitunganppn),
					'IDMataUang' => 1,
					'Kurs' => 14000,
					'Total_debet' => 0,
					'Total_kredit' => str_replace(".", "", $perhitunganppn),
					'Keterangan' => $data1['Keterangan'],
					'Saldo' => str_replace(".", "", $perhitunganppn),
				);
				$this->M_invoice_penjualan->save_jurnal($save_jurnal_kredit_ppn);
			}

			$data['cekgroupcustomer'] = $this->M_invoice_penjualan->cek_group_customer($data1['IDCustomer']);
			if ($data['cekgroupcustomer']->Nama_Group_Customer == "PIHAK BERELASI") {
				$groupcustomer = 'P000011';
			} elseif ($data['cekgroupcustomer']->Nama_Group_Customer == "PIHAK KETIGA") {
				$groupcustomer = 'P000009';
			}

			$data['id_jurnal'] = $this->M_invoice_penjualan->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal_ppn = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal_ppn = 'P000001';
			}

			$save_jurnal_debet_ppn = array(
				'IDJurnal' => $urutan_id_jurnal_ppn,
				'Tanggal' => $data1['Tanggal'],
				'Nomor' => $data1['Nomor'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail_inv,
				'Jenis_faktur' => 'INVP',
				'IDCOA' => $groupcustomer,
				'Debet' => str_replace(".", "", $data3['total_invoice']),
				'Kredit' => 0,
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => str_replace(".", "", $data3['total_invoice']),
				'Total_kredit' => 0,
				'Keterangan' => $data1['Keterangan'],
				'Saldo' => str_replace(".", "", $data3['total_invoice']),
			);
			$this->M_invoice_penjualan->save_jurnal($save_jurnal_debet_ppn);

			//grand total
			$data['id_invoice_grand_total'] = $this->M_invoice_penjualan->tampilkan_id_invoice_grand_total();
			if ($data['id_invoice_grand_total'] != "") {
				// foreach ($data['id_invoice_grand_total'] as $value) {
					$urutan = substr($data['id_invoice_grand_total']->IDFJKGrandTotal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_grand_total = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_grand_total = 'P000001';
			}

			$grand_total = array(
				'IDFJKGrandTotal' => $urutan_id_grand_total,
				'IDFJK' => $urutan_id,
				'Pembayaran' => '-',
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'DPP' => str_replace(".", "", $data3['DPP']),
				'Discount' => $data3['Discount'],
				'PPN' => str_replace(".", "", $data3['PPN']),
				'Grand_total' => str_replace(".", "", $data3['total_invoice']),
				'Sisa' => 0,
			);
			$this->M_invoice_penjualan->add_grand_total($grand_total);

			$data['id_piutang'] = $this->M_piutang->tampilkan_id_piutang();
			if ($data['id_piutang'] != "") {
				// foreach ($data['id_piutang'] as $value) {
					$urutan = substr($data['id_piutang']->IDPiutang, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_piutang = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_piutang = 'P000001';
			}
			// $piutang= str_replace(".", "", $this->input->post('Nilai_Piutang'));
			// $pemb= str_replace(".", "", $this->input->post('Pembayaran'));
			$datapiutang = array(
				'IDPiutang' => $urutan_id_piutang,
				'IDFaktur' => $urutan_id,
				'IDCustomer' => $data1['IDCustomer'],
				'Tanggal_Piutang' => $data1['Tanggal'],
				'Jatuh_Tempo' => $data1['Tanggal_jatuh_tempo'],
				'No_Faktur' => $data1['Nomor'],
				'Nilai_Piutang' => str_replace(".", "", $data3['total_invoice']),
				'Saldo_Awal' => str_replace(".", "", $data3['total_invoice']),
				'Pembayaran' => 0,
				'Saldo_Akhir' => str_replace(".", "", $data3['total_invoice']),
				'Jenis_Faktur' => 'INVP',
			);

			$this->M_piutang->simpan($datapiutang);
			//pembayaran kain jual
			// $data['id_pembayaran'] = $this->M_invoice_penjualan->tampilkan_id_pembayaran();
			// if($data['id_pembayaran']!=""){
			//     foreach ($data['id_pembayaran'] as $value) {
			//         $urutan= substr($value->IDFJKPembayaran, 1);
			//     }
			//     $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			//     $urutan_id_pembayaran= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
			// }else{
			//     $urutan_id_pembayaran = 'P000001';
			// }

			// $datas5 = array(
			//     'IDFJKPembayaran' => $urutan_id_pembayaran,
			//     'IDFJK' => $urutan_id,
			//     'Jenis_pembayaran' => $data3['jenis'],
			//     'IDCOA' => $data3['namacoa'],
			//     'NominalPembayaran' => $data3['nominal'],
			//     'IDMataUang' => $data1['IDMataUang'],
			//     'Kurs' => $data1['Kurs'],
			//     'Tanggal_giro' => $data3['tgl_giro']
			// );
			// $this->M_invoice_penjualan->add_pembayaran($datas5);

			echo true;
		} else {
			echo false;
		}
	}

	function show($id) {
		$nomor = '';
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->M_invoice_penjualan->find($id, $nomor);
		$data['invdetail'] = $this->M_invoice_penjualan->detail_invoice_penjualan($id, $nomor);
		$this->load->view('administrator/invoice_penjualan/V_show', $data);
	}

	function print_($nomor, $id) {
		$data['print'] = $this->M_invoice_penjualan->find($id, $nomor);
		$data['printdetail'] = $this->M_invoice_penjualan->detail_invoice_penjualan($id, $nomor);
		$this->load->view('administrator/invoice_penjualan/V_print', $data);
	}

	public function status_gagal($id) {
		// $data = array(
		//     'Batal' => 'tidak aktif',
		// );

		// $this->M_invoice_penjualan->update_status($data, array('IDFJK' => $id));
		// $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		// redirect('InvoicePenjualan/index');

		$data['penerimaanget'] = $this->M_invoice_penjualan->tampilkan_get_penerimaan_piutang($id);

		if (count($data['penerimaanget']) > 0) {
			// foreach($data['invoiceget'] as $valueinv) {
			if (($data['penerimaanget']->IDFJK != $id)) {
				$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
				$data = array(
					'Batal' => 'tidak aktif',
				);

				$this->M_invoice_penjualan->update_status($data, array('IDFJK' => $id));
			} else {
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Invoice Sudah Digunakan</div></center>");
			}
// }
		} else {
			$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
			$data = array(
				'Batal' => 'tidak aktif',
			);

			$this->M_invoice_penjualan->update_status($data, array('IDFJK' => $id));
		}
		redirect('InvoicePenjualan/index');
	}

	public function status_berhasil($id) {
		$data = array(
			'Batal' => 'aktif',
		);

		$this->M_invoice_penjualan->update_status($data, array('IDFJK' => $id));
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		redirect('InvoicePenjualan/index');
	}

	public function delete_multiple() {
		$ID_att = $this->input->post('msg');
		if ($ID_att != "") {
			$result = array();
			foreach ($ID_att AS $key => $val) {
				$result[] = array(
					"IDFJK" => $ID_att[$key],
					"Batal" => 'tidak aktif',
				);
			}
			$this->db->update_batch('tbl_penjualan_kain', $result, 'IDFJK');
			redirect("InvoicePenjualan/index");
		} else {
			redirect("InvoicePenjualan/index");
		}
	}

	function pencarian() {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['invoice'] = $this->M_invoice_penjualan->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
		$this->load->view('administrator/invoice_penjualan/V_invoice_penjualan', $data);
	}

// <<<<<<< Updated upstream
	public function edit($id) {
		$nomor = '';
		$data['bank'] = $this->M_invoice_penjualan->tampil_bank();
		$data['detail'] = $this->M_invoice_penjualan->detail_invoice_penjualan($id, $nomor);
		$data['grand'] = $this->M_invoice_penjualan->grand_total($id);
		$data['customers'] = $this->M_invoice_penjualan->get_customer();
		$data['invoice'] = $this->M_invoice_penjualan->find_edit($id, $nomor);
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_penjualan/V_edit', $data);
	}

	public function ubah_invoice_penjualan() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		// $data3 = $this->input->post('data3');

		$data = array(
			'Tanggal' => $data1['Tanggal'],
			'Nomor' => $data1['Nomor_invoice'],
			'IDCustomer' => $data1['IDCustomer'],
			'Nama_di_faktur' => $data1['Nama_di_faktur'],
			//'IDSJCK' => $this->M_invoice_penjualan->get_TPKbyNomor($data1['Nomor_sj'])->IDSJCK ? $this->M_invoice_penjualan->get_TPKbyNomor($data1['Nomor_sj'])->IDSJCK : 0,
			//'TOP' => $data1['Jatuh_tempo'],
			'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
			'IDMataUang' => '6',
			//'Kurs' => $data1['Kurs'],
			//'Total_pieces' => $data1['Total_pieces'],
			//'Total_yard' => $data1['Total_yard'],
			//'Saldo_pieces' => $data1['Total_pieces'],
			//'Saldo_yard' => $data1['Total_yard'],
			//'Discount' => $data1['Discount'],
			'Status_ppn' => $data1['Status_ppn'],
			'Keterangan' => $data1['Keterangan'],
		);
		$this->M_invoice_penjualan->update_master($data1['IDFJK'], $data);

		$datas = array(
			'IDFJK' => $data1['IDFJK'],
			'Pembayaran' => '-',
			'DPP' => str_replace(".", "", $data2['DPP']),
			//'Discount' => $data2['Discount'],
			'PPN' => str_replace(".", "", $data2['PPN']),
			'Grand_total' => str_replace(".", "", $data2['total_invoice_pembayaran']),
			'Sisa' => 0,

		);
		$this->M_invoice_penjualan->update_grand_total($data1['IDFJK'], $datas);

		// if($data2['namacoa']=="Cash")
		// {
		//     $datas5 = array(
		//         'IDFJK' => $data1['IDFJK'],
		//         'Jenis_pembayaran' => $data2['jenis'],
		//         'IDCOA' => 0,
		//         'NominalPembayaran' => $data2['nominal'],
		//         'IDMataUang' => 1,
		//         'Kurs' => '14000',
		//         'Tanggal_giro' => $data2['tgl_giro']
		//     );
		// }else{
		//     $datas5 = array(
		//         'IDFJK' => $data1['IDFJK'],
		//         'Jenis_pembayaran' => $data2['jenis'],
		//         'IDCOA' => $data2['namacoa'],
		//         'NominalPembayaran' => $data2['nominal'],
		//         'IDMataUang' => 6,
		//         'Kurs' => '14000',
		//         'Tanggal_giro' => $data2['tgl_giro']
		//     );
		// }
		// $this->M_invoice_penjualan->update_pembayaran($data1['IDFJK'],$datas5);
	}
// =======
	public function laporan_inv_kain() {
		$data = array(
			'title_master' => "Laporan Invoice Penjualan Kain", //Judul
			'title' => "Laporan Invoice Penjualan Kain", //Judul Tabel
			'inv' => $this->M_invoice_penjualan->get_all_procedure(),
		);
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/invoice_penjualan/V_laporan', $data);
	}
	function pencarian_store() {
		$data = array(
			'title_master' => "Laporan Invoice Penjualan Kain", //Judul
			'title' => "Laporan Invoice Penjualan Kain", //Judul Tabel
		);
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		if ($this->input->post('date_from') == '') {
			$data['inv'] = $this->M_invoice_penjualan->searching_store_like($this->input->post('keyword'));
		} else {
			$data['inv'] = $this->M_invoice_penjualan->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
		}
		$this->load->view('administrator/invoice_penjualan/V_laporan', $data);
	}
// >>>>>>> Stashed changes
}