<!-- Programmer : Rais Naufal Hawari
 Date       : 12-07-2018 -->

 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Supplier extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_supplier');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
  }

  public function index()
  {
    $data = array(
        'title_master' => "Master Supplier", //Judul 
        'title' => "List Data Supplier", //Judul Tabel
        'action' => site_url('Supplier/create'), //Alamat Untuk Action Form
        'button' => "Tambah Supplier", //Nama Button
        'Supplier' => $this->M_supplier->get_all() //Load data Supplier
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/supplier/supplier_list.php', $data);
  }
  
  //Tambah Data Supplier
  public function create()
  {
    $data = array(
        'title_master' => "Master Supplier", //Judul 
        'title' => "Tambah Data Supplier" , //Judul Tabel
        'action' => site_url('Supplier/create_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Supplier'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button  
        'DataGroup' => $this->M_supplier->get_all_group_suuplier(), 
        'DataKota' => $this->M_supplier->get_all_kota(),
        'IDGroupSupplier' => set_value('IDGroupSupplier'),
        'Kode_Suplier' => set_value('Kode_Suplier'),
        'IDSupplier' => set_value('IDSupplier'),
        'Nama' => set_value('Nama'),
        'Alamat' => set_value('Alamat'),  
        'No_Telpon' => set_value('No_Telpon'),  
        'IDKota' => set_value('IDKota'),  
        'Fax' => set_value('Fax'),
        'Email' => set_value('Email'),  
        'NPWP' => set_value('NPWP'),  
        'No_KTP' => set_value('No_KTP'),  
        'Aktif' => set_value('Aktif'),  
        
      );
    
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/supplier/supplier_form.php', $data);
  }
  
  //Tambah Data Supplier
  public function create_action()
  {
   $data['id_supplier']= $this->M_supplier->tampilkan_id_supplier();
   if($data['id_supplier']!=""){
    // foreach ($data['id_supplier'] as $value) {
      $urutan= substr($data['id_supplier']->IDSupplier, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }
  $data = array(
    'IDSupplier' => $urutan_id,
    'IDGroupSupplier' => $this->input->post('IDGroupSupplier'),
    'Kode_Suplier' =>  $this->input->post('Kode_Suplier'),
    'IDKota' =>  $this->input->post('IDKota'),
    'Nama' =>  $this->input->post('Nama'),
    'Alamat' =>  $this->input->post('Alamat'),  
    'No_Telpon' =>  $this->input->post('No_Telpon'),  
    'IDKota' =>  $this->input->post('IDKota'),  
    'Fax' =>  $this->input->post('Fax'),
    'Email' =>  $this->input->post('Email'),  
    'NPWP' =>  $this->input->post('NPWP'),  
    'No_KTP' =>  $this->input->post('No_KTP'),  
    'Aktif' =>  'aktif'
  );
  $this->M_supplier->insert($data);
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
  redirect('Supplier/create');
}

  //Edit Data Supplier
public function update($id)
{
    $cari = $this->M_supplier->get_by_id($id);//Cari apakah data yang dimaksud ada di database

    if($cari) {
      $data = array(
        'title_master' => "Master Supplier", //Judul 
        'title' => "Ubah Data Supplier" , //Judul Tabel
        'action' => site_url('Supplier/update_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Supplier'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'DataGroup' => $this->M_supplier->get_all_group_suuplier(), 
        'DataKota' => $this->M_supplier->get_all_kota(),
        'IDGroupSupplier' => set_value('IDGroupSupplier', $cari->IDGroupSupplier),
        'Kode_Suplier' => set_value('Kode_Suplier', $cari->Kode_Suplier),
        'IDSupplier' => set_value('IDSupplier', $cari->IDSupplier),
        'Nama' => set_value('Nama', $cari->Nama),
        'Alamat' => set_value('Alamat', $cari->Alamat),  
        'No_Telpon' => set_value('No_Telpon', $cari->No_Telpon),  
        'IDKota' => set_value('IDKota', $cari->IDKota),  
        'Fax' => set_value('Fax', $cari->Fax),
        'Email' => set_value('Email', $cari->Email),  
        'NPWP' => set_value('NPWP', $cari->NPWP),  
        'No_KTP' => set_value('No_KTP', $cari->No_KTP),  
        'Aktif' => set_value('Aktif', $cari->Aktif), 
        'Aktif2' => set_value('Aktif', $cari->Aktif2), 
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/supplier/supplier_form.php', $data);
    } else {
     redirect('Supplier/index');
   }
 }

  //Aksi Ubah Data
 public function update_action()
 {

    $id = $this->input->post('IDSupplier'); //Get id sebagai penanda record yang akan dirubah
    
    $data = array(
      'IDGroupSupplier' => $this->input->post('IDGroupSupplier'),
      'Kode_Suplier' =>  $this->input->post('Kode_Suplier'),
      'IDKota' =>  $this->input->post('IDKota'),
      'Nama' =>  $this->input->post('Nama'),
      'Alamat' =>  $this->input->post('Alamat'),  
      'No_Telpon' =>  $this->input->post('No_Telpon'),  
      'IDKota' =>  $this->input->post('IDKota'),  
      'Fax' =>  $this->input->post('Fax'),
      'Email' =>  $this->input->post('Email'),  
      'NPWP' =>  $this->input->post('NPWP'),  
      'No_KTP' =>  $this->input->post('No_KTP'), 
    );
    $this->M_supplier->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
    redirect('Supplier/index');
  }


  public function read($id)
  {
    $cari = $this->M_supplier->get_by_id($id);//Cari apakah data yang dimaksud ada di database

    if($cari) {
      $data = array(
        'title_master' => "Master Supplier", //Judul 
        'title' => "Detail Data Supplier" , //Judul Tabel
        'action_back' => site_url('Supplier'),//Alamat Untuk back
        'button' => "Detail", //Nama Button
        'DataGroup' => $this->M_supplier->get_all_group_suuplier(), 
        'DataKota' => $this->M_supplier->get_all_kota(),
        'IDGroupSupplier' => set_value('IDGroupSupplier', $cari->IDGroupSupplier),
        'Kode_Suplier' => set_value('Kode_Suplier', $cari->Kode_Suplier),
        'IDSupplier' => set_value('IDSupplier', $cari->IDSupplier),
        'Nama' => set_value('Nama', $cari->Nama),
        'Alamat' => set_value('Alamat', $cari->Alamat),  
        'No_Telpon' => set_value('No_Telpon', $cari->No_Telpon),  
        'Kota' => set_value('Kota', $cari->Kota),  
        'Fax' => set_value('Fax', $cari->Fax),
        'Email' => set_value('Email', $cari->Email),  
        'NPWP' => set_value('NPWP', $cari->NPWP),  
        'No_KTP' => set_value('No_KTP', $cari->No_KTP),  
        'Aktif' => set_value('Aktif', $cari->Aktif),  
        'Aktif2' => set_value('Aktif', $cari->Aktif2),
        'Kode_Group_Supplier' => set_value('Kode_Group_Supplier', $cari->Kode_Group_Supplier),
        'Group_Suplier' => set_value('Group_Suplier', $cari->Group_Suplier),
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/supplier/supplier_read.php', $data);
    } else {
      $this->session->set_flashdata('message', 'Maaf, Data Tidak Ditemukan');
      redirect('Giro','refresh');
    }
  }

  //Hapus Data Supplier
  public function delete($id)
  {
     //  $cari = $this->M_supplier->get_by_id($id); 

     //  if ($cari) {
     //    $this->M_supplier->delete($id);
     //    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
     //    redirect('Supplier/index');
     //  } else {
     //   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Gagal di Hapus</div></center>");
     //   redirect('Supplier/index');
     // }

    $data['poget']= $this->M_supplier->get_po($id);
    $data['pogetumum']= $this->M_supplier->get_po_umum($id);

    if(count($data['poget']) > 0){
      if(($data['poget']->IDSupplier!=$id)){   
       $this->M_supplier->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
        redirect('Supplier/index');
      }else{
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
       redirect('Supplier/index');
      }
    }elseif(count($data['pogetumum']) > 0){
      if(($data['pogetumum']->IDSupplier!=$id)){   
        $this->M_supplier->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
        redirect('Supplier/index');
      }else{
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
        redirect('Supplier/index');
      }
    }else{
      $this->M_supplier->delete($id);
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
      redirect('Supplier/index');
    }
     
   }

   public function pencarian()
   {
     
    $data = array(
        'title_master' => "Master Supplier", //Judul 
        'title' => "List Data Supplier", //Judul Tabel
        'action' => site_url('Supplier/create'), //Alamat Untuk Action Form
        'button' => "Tambah Supplier", //Nama Button
        'Supplier' => $this->M_supplier->get_all() //Load data Supplier
      );
    $kolom= addslashes($this->input->post('jenispencarian'));
    $status= addslashes($this->input->post('statuspencarian'));
    $keyword= addslashes($this->input->post('keyword'));
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    if ($kolom!="" && $status!="" && $keyword!="") {
      $data['Supplier']=$this->M_supplier->cari($kolom,$status,$keyword);
    }elseif ($kolom!="" && $keyword!="") {
      $data['Supplier']=$this->M_supplier->cari_by_kolom($kolom,$keyword);
    }elseif ($status!="") {
      $data['Supplier']=$this->M_supplier->cari_by_status($status);
    }elseif ($keyword!="") {
      $data['Supplier']=$this->M_supplier->cari_by_keyword($keyword);
    }else {
      $data['Supplier']=$this->M_supplier->get_all();
    }
    $this->load->view('administrator/supplier/supplier_list.php', $data);
  }
  

}

/* End Supplier.php */
