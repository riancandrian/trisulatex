<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenerimaanBarang extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('M_penerimaan_barang');
		$this->load->model('M_agen');
		$this->load->model('M_login');
		$this->load->helper('convert_function');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		$data['PenerimaanBarang'] = $this->M_penerimaan_barang->all();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/penerimaan_barang/V_index', $data);
	}

	public function create() {
		$this->load->model('M_supplier');

		$data['last_number'] = $this->M_penerimaan_barang->getCodeReception(date('Y'));
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['supplier'] = $this->M_supplier->get_trisula();
		// $data['nosjc'] = $this->M_penerimaan_barang->get_nosjc();
		$data['nosjc'] = $this->M_penerimaan_barang->cek_ketersediaan_sjc();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/penerimaan_barang/V_penerimaan_barang', $data);
	}

	function check_SJC() {
		$return['data'] = $this->M_penerimaan_barang->check_SJC($this->input->post('no_sjc'));
		if ($return['data']) {
			$return['error'] = false;
		} else {
			$return['error'] = true;
		}

		echo json_encode($return);
	}

	function store() {
		if (!empty($this->input->post('data'))) {
			$datas = $this->input->post('data');
		}

		if (!empty($this->input->post('Total'))) {
			$total = $this->input->post('Total');
		}
		$data['id_terima_barang'] = $this->M_penerimaan_barang->tampilkan_id_terima_barang();
		if ($data['id_terima_barang'] != "") {
			// foreach ($data['id_terima_barang'] as $value) {
				$urutan = substr($data['id_terima_barang']->IDTBS, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id = 'P000001';
		}
		foreach ($datas as $val) {

			if ($this->M_penerimaan_barang->check_noPB($val['NoPB'])->num_rows() == 0) {
				$save = array(
					'IDTBS' => $urutan_id,
					'Tanggal' => $val['Tanggal'],
					'Nomor' => $val['NoPB'],
					'IDSupplier' => $val['ID_supplier'] ? $val['ID_supplier'] : $this->input->post('id_supplier'),
					'Nomor_sj' => $val['NoSJ'],
					'IDPO' => $this->M_penerimaan_barang->get_IDPO($val['NoPO'])->IDPO ? $this->M_penerimaan_barang->get_IDPO($val['NoPO'])->IDPO : 0,
					'NoSO' => $val['NoSO'],
					'Total_qty_yard' => $total[0],
					'Total_qty_meter' => $total[1],
					'Saldo_yard' => $total[0],
					'Saldo_meter' => $total[1],
					'Total_harga' => str_replace(".", "", $this->input->post('total_harga')),
					'Keterangan' => $val['Keterangan'] ? $val['Keterangan'] : $this->input->post('Keterangan'),
					'Jenis_TBS' => 'PB',
					'Batal' => 'Aktif',
				);

				//save barang supplier
				$this->M_penerimaan_barang->save($save);
			}

			//save barang supplier detail
			$data['id_terima_barang_detail'] = $this->M_penerimaan_barang->tampilkan_id_terima_barang_detail();
			if ($data['id_terima_barang_detail'] != "") {
				// foreach ($data['id_terima_barang_detail'] as $value) {
					$urutan = substr($data['id_terima_barang_detail']->IDTBSDetail, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail = 'P000001';
			}

			$save_detail = array(
				'IDTBSDetail' => $urutan_id_detail,
				'IDTBS' => $urutan_id,
				'Barcode' => $val['Barcode'],
				'NoSO' => $val['NoSO'],
				'Party' => $val['Party'],
				'Indent' => $val['Indent'],
				'IDBarang' => $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang : 0,
				'IDCorak' => $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak : 0,
				'IDWarna' => $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna : 0,
				'IDMerk' => $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDMerk ? $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDMerk : 0,
				'Qty_yard' => $val['Qty_yard'],
				'Qty_meter' => $val['Qty_meter'],
				'Saldo_yard' => $val['Qty_yard'],
				'Saldo_meter' => $val['Qty_meter'],
				'Grade' => $val['Grade'],
				'Remark' => $val['Remark'],
				'Lebar' => $val['Lebar'],
				'IDSatuan' => $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
				'Harga' => str_replace(".", "", $val['Harga']),
			);

			$this->M_penerimaan_barang->save_detail($save_detail);

			//save in
			/*$save_in = array(
	                'Barcode' => $val['Barcode'],
	                'NoSO' => $val['NoSO'],
	                'Party' => $val['Party'],
	                'Indent' => $val['Indent'],
	                'IDBarang' => 1,
	                'IDCorak' => $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak : 0,
	                'IDWarna' => $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna : 0,
	                'IDMerk' => 1,
	                'Panjang_yard' => $val['Qty_yard'],
	                'Panjang_meter' => $val['Qty_meter'],
	                'Grade' => $val['Grade'],
	                'Lebar' => $val['Lebar'],
	                'IDSatuan' => $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan : 0
	            );

*/

			//save stok

			$data['id_stok'] = $this->M_penerimaan_barang->tampilkan_id_stok();
			if ($data['id_stok'] != "") {
				// foreach ($data['id_stok'] as $value) {
					$urutan = substr($data['id_stok']->IDStok, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail_stok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail_stok = 'P000001';
			}
			$save_stock = array(
				'IDStok' => $urutan_id_detail_stok,
				'Tanggal' => $val['Tanggal'],
				'Nomor_faktur' => $val['NoPB'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'Jenis_faktur' => 'PB',
				'Barcode' => $val['Barcode'],
				'IDBarang' => $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang : 0,
				'IDCorak' => $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak : 0,
				'IDWarna' => $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna : 0,
				'IDGudang' => $this->M_penerimaan_barang->get_IDgudang()->IDGudang ? $this->M_penerimaan_barang->get_IDgudang()->IDGudang : 0,
				'Qty_yard' => $val['Qty_yard'],
				'Qty_meter' => $val['Qty_meter'],
				'Saldo_yard' => $val['Qty_yard'],
				'Saldo_meter' => $val['Qty_meter'],
				'Grade' => $val['Grade'],
				'IDSatuan' => $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
				'Harga' => str_replace(".", "", $val['Harga']),
				'IDMataUang' => 1,
				'Kurs' => '',
				'Total' => str_replace(".", "", $val['Total_harga']),
			);

			$this->M_penerimaan_barang->save_stock($save_stock);

			//save stock card
			$data['id_kartu_stok'] = $this->M_penerimaan_barang->tampilkan_id_kartu_stok();
			if ($data['id_kartu_stok'] != "") {
				// foreach ($data['id_kartu_stok'] as $value) {
					$urutan = substr($data['id_kartu_stok']->IDKartuStok, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail_kartustok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail_kartustok = 'P000001';
			}
			$save_stock_card = array(
				'IDKartuStok' => $urutan_id_detail_kartustok,
				'Tanggal' => $val['Tanggal'],
				'Nomor_faktur' => $val['NoPB'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'IDStok' => $urutan_id_detail_stok,
				'Jenis_faktur' => 'PB',
				'Barcode' => $val['Barcode'],
				'IDBarang' => $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang ? $this->M_penerimaan_barang->get_IDbarang($val['Corak'])->IDBarang : 0,
				'IDCorak' => $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak ? $this->M_penerimaan_barang->get_IDcorak($val['Corak'])->IDCorak : 0,
				'IDWarna' => $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna ? $this->M_penerimaan_barang->get_IDwarna($val['Warna'])->IDWarna : 0,
				'IDGudang' => $this->M_penerimaan_barang->get_IDgudang()->IDGudang ? $this->M_penerimaan_barang->get_IDgudang()->IDGudang : 0,
				'Masuk_yard' => $val['Qty_yard'],
				'Masuk_meter' => $val['Qty_meter'],
				'Keluar_yard' => 0,
				'Keluar_meter' => 0,
				'Grade' => $val['Grade'],
				'IDSatuan' => $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan ? $this->M_penerimaan_barang->get_IDsatuan($val['Satuan'])->IDSatuan : 0,
				'Harga' => str_replace(".", "", $val['Harga']),
				'IDMataUang' => 1,
				'Kurs' => '',
				'Total' => str_replace(".", "", $val['Total_harga']),
			);

			$this->M_penerimaan_barang->save_stock_card($save_stock_card);

			//save jurnal
			$data['id_jurnal'] = $this->M_penerimaan_barang->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal = 'P000001';
			}

			$save_jurnal_debet = array(
				'IDJurnal' => $urutan_id_jurnal,
				'Tanggal' => $val['Tanggal'],
				'Nomor' => $val['NoPB'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'Jenis_faktur' => 'PB',
				'IDCOA' => 'P000018',
				'Debet' => $val['Total_harga'],
				'Kredit' => 0,
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => str_replace(".", "", $val['Total_harga']),
				'Total_kredit' => 0,
				'Keterangan' => $val['Keterangan'] ? $val['Keterangan'] : $this->input->post('Keterangan'),
				'Saldo' => str_replace(".", "", $val['Total_harga']),
			);
			$this->M_penerimaan_barang->save_jurnal($save_jurnal_debet);

			$data['id_jurnal'] = $this->M_penerimaan_barang->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal_kredit = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal_kredit = 'P000001';
			}

			$save_jurnal_kredit = array(
				'IDJurnal' => $urutan_id_jurnal_kredit,
				'Tanggal' => $val['Tanggal'],
				'Nomor' => $val['NoPB'],
				'IDFaktur' => $urutan_id,
				'IDFakturDetail' => $urutan_id_detail,
				'Jenis_faktur' => 'PB',
				'IDCOA' => 'P000021',
				'Debet' => 0,
				'Kredit' => $val['Total_harga'],
				'IDMataUang' => 1,
				'Kurs' => 14000,
				'Total_debet' => 0,
				'Total_kredit' => str_replace(".", "", $val['Total_harga']),
				'Keterangan' => $val['Keterangan'] ? $val['Keterangan'] : $this->input->post('Keterangan'),
				'Saldo' => str_replace(".", "", $val['Total_harga']),
			);
			$this->M_penerimaan_barang->save_jurnal($save_jurnal_kredit);

		}
	}

	function show($id) {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->M_penerimaan_barang->find($id);
		// $data['detail'] = $this->M_penerimaan_barang->findDetail($id);
		$data['pbdetail'] = $this->M_penerimaan_barang->get_by_id_detail($id);
		$this->load->view('administrator/penerimaan_barang/V_show', $data);
	}

	function edit($id) {
		$this->load->model('M_supplier');
		$this->load->model('M_corak');
		$this->load->model('M_warna');
		$this->load->model('M_satuan');

		$data['satuan'] = $this->M_satuan->get_all();
		$data['warna'] = $this->M_warna->tampilkan_warna();
		$data['corak'] = $this->M_corak->all();
		$data['supplier'] = $this->M_supplier->get_all();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->M_penerimaan_barang->find($id);
		$data['detail'] = $this->M_penerimaan_barang->findDetail($id);
		$this->load->view('administrator/penerimaan_barang/V_edit', $data);
	}

	function update() {
		$data = array(
			'Tanggal' => $this->input->post('Tanggal'),
			'IDSupplier' => $this->input->post('id_supplier'),
			'Keterangan' => $this->input->post('Keterangan'),
		);

		$this->M_penerimaan_barang->update($data, $this->input->post('id'));

		$details = $this->input->post('details');
		//echo $this->input->post('loop');
		//die();
		for ($i = 0; $i < $this->input->post('loop'); $i++) {
			$j = 0;
			foreach ($details['Party'][$i] as $key => $value) {
				$detail = array(
					'Party' => $details['Party'][$i][$j],
					'Indent' => $details['Indent'][$i][$j],
					'IDCorak' => $details['id_corak'][$i][$j],
					'IDWarna' => $details['id_warna'][$i][$j],
					'Qty_yard' => $details['Qty_yard'][$i][$j],
					'Qty_meter' => $details['Qty_meter'][$i][$j],
					'Saldo_yard' => $details['Qty_yard'][$i][$j],
					'Saldo_meter' => $details['Qty_meter'][$i][$j],
					'Grade' => $details['Grade'][$i][$j],
					'Remark' => $details['Remark'][$i][$j],
					'Lebar' => $details['Lebar'][$i][$j],
					'IDSatuan' => $details['id_satuan'][$i][$j],
				);

				$this->M_penerimaan_barang->update_detail($detail, $this->input->post('id'), $details['barcode'][$i][$j]);

				$j++;
			}
		}

		$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div>");
		redirect(base_url('PenerimaanBarang'));
	}

	public function update_() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');

		$data = array(
			'Tanggal' => $data1['Tanggal'],
			'IDSupplier' => $data1['IDSupplier'],
			'Keterangan' => $data1['Keterangan'],
		);

		if ($this->M_penerimaan_barang->update($data, $data1['IDTBS'])) {
			$data = null;
			if ($data2 > 0) {
				foreach ($data2 as $row) {
					$this->M_penerimaan_barang->drop($row['IDTBSDetail'], $data1['IDTBS']);
				}
			}

			echo true;
		} else {
			echo false;
		}
	}

	function soft_delete($id) {
		$data['invoiceget'] = $this->M_penerimaan_barang->tampilkan_get_invoice($id);

		if (count($data['invoiceget']) > 0) {
			// foreach($data['invoiceget'] as $valueinv) {
			if (($data['invoiceget']->IDTBS != $id)) {
				$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
				$this->M_penerimaan_barang->soft_delete($id);
			} else {
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data PB Sudah Digunakan</div></center>");
			}
// }
		} else {
			$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
			$this->M_penerimaan_barang->soft_delete($id);
		}
		redirect(base_url('PenerimaanBarang'));

	}

	function soft_success($id) {
		$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diaktifkan</div>");
		$this->M_penerimaan_barang->soft_success($id);
		redirect(base_url('PenerimaanBarang'));
	}

	function bulk() {
		if ($this->input->post('bulk')) {
			$this->M_penerimaan_barang->bulk($this->input->post('bulk'));
			$this->session->set_flashdata("Pesan", "<div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div>");
		}
		redirect(base_url('PenerimaanBarang'));
	}

	function pencarian() {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['PenerimaanBarang'] = $this->M_penerimaan_barang->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
		$this->load->view('administrator/penerimaan_barang/V_index', $data);
	}

	public function print_data() {
		$sjc = $this->uri->segment('3');
		$data['data'] = $this->M_penerimaan_barang->getPrint();
		$id = $data['data']->IDTBS;
		$data['detail'] = $this->M_penerimaan_barang->getDetail_tbs($id);
		$this->load->view('administrator/penerimaan_barang/V_print', $data);
	}

	public function print_data_edit() {
		$sjc = $this->uri->segment('3');
		$data['data'] = $this->M_penerimaan_barang->getPrint_edit($sjc);
		$id = $data['data']->IDTBS;
		$data['detail'] = $this->M_penerimaan_barang->getDetail_tbs($id);
		$this->load->view('administrator/penerimaan_barang/V_print', $data);
	}
}