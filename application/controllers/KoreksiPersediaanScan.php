<?php

class KoreksiPersediaanScan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_koreksi_persediaan');
    $this->load->model('M_agen');
    $this->load->model('M_corak');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
  }

  public function index()
  {
    $data['persediaan'] = $this->M_koreksi_persediaan->tampilkan_koreksi_persediaan_scan();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/koreksi_persediaan_scan/V_koreksi_persediaan_scan.php', $data);
  }


  public function tambah_koreksi_persediaan_scan()
  {
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['id_koreksi_persediaan'] = $this->M_koreksi_persediaan->tampilkan_id_koreksi_persediaan();
    $data['corak'] = $this->M_corak->all();
    $data['kodenoasset'] = $this->M_koreksi_persediaan->no_asset_scan(date('Y'));
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $this->load->view('administrator/koreksi_persediaan_scan/V_tambah_koreksi_persediaan_scan', $data);
  }

  public function simpan_koreksi_persediaan_scan(){

    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');

    $data['id_koreksi_persediaan_scan'] = $this->M_koreksi_persediaan->tampilkan_id_koreksi_persediaan_scan();
    if($data['id_koreksi_persediaan_scan']!=""){
      foreach ($data['id_koreksi_persediaan_scan'] as $value) {
        $urutan= substr($value->IDKP, 1);
      }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }

    $data = array(
      'IDKPScan' => $urutan_id,
      'Tanggal' => $data1['tanggal'],
      'Nomor' => $data1['no_kp'],
      'Total_yard' => $data1['total_yard'],
      'Total_meter' => $data1['total_meter'],
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Jenis' => $data1['jenis'],
      'Keterangan' => $data1['keterangan'],
      'Batal' => 'aktif'
    );
       // $this->M_koreksi_persediaan->add_scan($data)


    if($this->M_koreksi_persediaan->add_scan($data)){
      // $last_insert_id = $this->db->insert_id(); 
      $data = null;
      foreach($data2 as $row){
        $data['id_koreksi_persediaan_scan_detail'] = $this->M_koreksi_persediaan->tampilkan_id_koreksi_persediaan_scan_detail();
        if($data['id_koreksi_persediaan_scan_detail']!=""){
          foreach ($data['id_koreksi_persediaan_scan_detail'] as $value) {
            $urutan= substr($value->IDKPScanDetail, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail = 'P000001';
        }
       $data = array(
         'IDKPScanDetail'=>$urutan_id_detail,
         'IDKPScan' => $urutan_id,
         'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan : 0,
         'Barcode' => $row['Barcode'],
         'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang : 0,
         'IDCorak' => $row['IDCorak'],
         'IDWarna' => $row['IDWarna'],
         'IDMataUang' => 1,
         'Kurs' => 14000,
         'Qty_yard' => $row['q_yard'],
         'Qty_meter' => $row['q_meter'],
         'Saldo_yard' => $row['q_yard'],
         'Saldo_meter' => $row['q_meter'],
         'Grade' => $row['Grade'],
         'Harga' =>  0
       );
       $this->M_koreksi_persediaan->add_detail_scan($data);

       $stok = $this->M_koreksi_persediaan->find_tbl_stok($row['Barcode']);

       $yard = $stok->Saldo_yard;
       $meter = $stok->Saldo_meter;

       if($data1['jenis']=='plus'){
         $data5 = array(
          'Saldo_yard' => ($yard + $row['q_yard']),
          'Saldo_meter' => ($meter + $row['q_meter']),
        );
       }elseif($data1['jenis']=='minus')
       {
        $data5 = array(
          'Saldo_yard' => ($yard - $row['q_yard']),
          'Saldo_meter' => ($meter - $row['q_meter']),
        );
      }
      $this->M_koreksi_persediaan->update_tbl_stok($data5, $row['Barcode']);

      $data['id_kartu_stok'] = $this->M_koreksi_persediaan->tampilkan_id_kartu_stok();
        if($data['id_kartu_stok']!=""){
          foreach ($data['id_kartu_stok'] as $value) {
            $urutan= substr($value->IDKartuStok, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_kartu_stok= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_kartu_stok = 'P000001';
        }
      if($data1['jenis']=='plus'){
        $data6 = array(
          'IDKartuStok' => $urutan_id_kartu_stok,
         'Tanggal' => $data1['tanggal'],
         'Nomor_faktur' => $data1['no_kp'],
         'IDFaktur' => $urutan_id,
         'IDFakturDetail' => $urutan_id_detail,
         'IDStok' => $stok->IDStok,
         'Barcode' => $row['Barcode'],
         'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang : 0,
         'IDGudang' => 0,
         'IDCorak' => $row['IDCorak'],
         'IDWarna' => $row['IDWarna'],
         'IDMataUang' => 1,
         'Kurs' => 14000,
         'Masuk_yard' => $row['q_yard'],
         'Masuk_meter' => $row['q_meter'],
         'Keluar_yard' => 0,
         'Keluar_meter' => 0,
         'Grade' => $row['Grade'],
         'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan : 0,
         'Harga' =>  0,
         'IDMataUang' => 1,
         'Kurs' => 14000,
         'Total' => 0
       );
      }elseif($data1['jenis']=='minus'){
       $data6 = array(
          'IDKartuStok' => $urutan_id_kartu_stok,
         'Tanggal' => $data1['tanggal'],
         'Nomor_faktur' => $data1['no_kp'],
         'IDFaktur' => $urutan_id,
         'IDFakturDetail' => $urutan_id_detail,
         'IDStok' => $stok->IDStok,
         'Barcode' => $row['Barcode'],
         'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang : 0,
         'IDGudang' => 0,
         'IDCorak' => $row['IDCorak'],
         'IDWarna' => $row['IDWarna'],
         'IDMataUang' => 1,
         'Kurs' => 14000,
         'Masuk_yard' => 0,
         'Masuk_meter' => 0,
         'Keluar_yard' => $row['q_yard'],
         'Keluar_meter' => $row['q_meter'],
         'Grade' => $row['Grade'],
         'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan : 0,
         'Harga' =>  0,
         'IDMataUang' => 1,
         'Kurs' => 14000,
         'Total' => 0
       );
     }

     $this->M_koreksi_persediaan->add_kartu_stok($data6, $row['Barcode']);

   }


   echo true;
 }else{
   echo false;
 }

}

public function edit($id)
{
  $data['detail'] = $this->M_koreksi_persediaan->detail_koreksi_persediaan_scan($id);
  $data['koreksi'] = $this->M_koreksi_persediaan->getByIdScan($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/koreksi_persediaan_scan/V_edit_koreksi_persediaan', $data);
}

public function ubah_koreksi_persediaan_scan()
{
  $data1 = $this->input->post('data1');
  $data2 = $this->input->post('data2');
  $data3 = $this->input->post('data3');

  $data = array(
    'Tanggal' => $data1['tanggal'],
    'Nomor' => $data1['no_kp'],
    'Total_yard' => $data1['total_yard'],
    'Total_meter' => $data1['total_meter'],
    'IDMataUang' => 1,
    'Kurs' => 14000,
    'Jenis' => $data1['jenis'],
    'Keterangan' => $data1['keterangan']
  );

  if($this->M_koreksi_persediaan->update_master_scan($data1['IDKPScan'],$data)){
    $data = null;
    if($data3 > 0){
     foreach($data3 as $row){
       $this->M_koreksi_persediaan->drop_scan($row['IDKPScanDetail']);
     }
   }
   if($data2 != null){
    foreach($data2 as $row){
      $data = array(
       'IDKPScan'=> $data1['IDKPScan'],
       'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['Corak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['Corak'])->IDSatuan : 0,
       'Barcode' => $row['Barcode'],
       'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['Corak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['Corak'])->IDBarang : 0,
       'IDCorak' => $row['IDCorak'],
       'IDWarna' => $row['IDWarna'],
       'IDMataUang' => 1,
       'Kurs' => 14000,
       'Qty_yard' => $row['q_yard'],
       'Qty_meter' => $row['q_meter'],
       'Saldo_yard' => $row['q_yard'],
       'Saldo_meter' => $row['q_meter'],
       'Grade' => $row['Grade'],
       'Harga' =>  $row['Harga']
     );

      if($row['IDKPScanDetail'] ==''){
        $IDKP_detail=  $this->M_koreksi_persediaan->add_detail_scan($data);

      }else
      {
       $IDKP_detail= $this->M_koreksi_persediaan->update_detail_scan($row['IDKPScanDetail'], $data);                       
     }

                    // $IDKP_detail= $this->M_koreksi_persediaan->add_detail_scan($data);

     $data['stok'] = $this->M_koreksi_persediaan->find_tbl_stok_edit($row['Barcode']);
     $cek_detail['cekdetail'] = $this->M_koreksi_persediaan->find_tbl_scan_detail($row['Barcode']);
     foreach ($data['stok'] as $stoks) {
                    # code...

      $yard = $stoks->Saldo_yard;
      $meter = $stoks->Saldo_meter;
      foreach ($cek_detail['cekdetail'] as $cekdetail) {
      $cekyard= $cek_detail->Saldo_yard;
      $cekmeter= $cek_detail->Saldo_meter;
       

      if($data1['jenis']=='plus'){
       $data5 = array(
         'Saldo_yard' => (($yard - $cekyard) + $row['q_yard']),
         'Saldo_meter' => (($meter - $cekmeter ) + $row['q_meter']),
       );
     }elseif($data1['jenis']=='minus')
     {
      $data5 = array(
       'Saldo_yard' => (($yard + $cekyard) - $row['q_yard']),
       'Saldo_meter' => (($meter + $cekmeter) - $row['q_meter']),
     );
    }
    $this->M_koreksi_persediaan->update_tbl_stok($data5, $row['Barcode']);
                    // else
                    // {
                    //   $this->M_koreksi_persediaan->update_detail($row['IDKPDetail'], $data);                       
                    // }

    if($data1['jenis']=='plus'){
      $data6 = array(
       'Tanggal' => $data1['tanggal'],
       'Nomor_faktur' => $data1['no_kp'],
                       // 'IDFaktur' => $last_insert_id,
                       // 'IDFakturDetail' => $IDKP_detail,
                       // 'IDStok' => $stoks->IDStok,
       'Barcode' => $row['Barcode'],
       'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang : 0,
       'IDGudang' => 0,
       'IDCorak' => $row['IDCorak'],
       'IDWarna' => $row['IDWarna'],
       'IDMataUang' => 1,
       'Kurs' => 14000,
       'Masuk_yard' => $row['q_yard'],
       'Masuk_meter' => $row['q_meter'],
       'Keluar_yard' => 0,
       'Keluar_meter' => 0,
       'Grade' => $row['Grade'],
       'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan : 0,
       'Harga' =>  0,
       'IDMataUang' => 1,
       'Kurs' => 14000,
       'Total' => 0
     );
    }elseif($data1['jenis']=='minus'){
     $data6 = array(
       'Tanggal' => $data1['tanggal'],
       'Nomor_faktur' => $data1['no_kp'],
                       // 'IDFaktur' => $last_insert_id,
                       // 'IDFakturDetail' => $IDKP_detail,
                       // 'IDStok' => $stoks->IDStok,
       'Barcode' => $row['Barcode'],
       'IDBarang' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDBarang : 0,
       'IDGudang' => 0,
       'IDCorak' => $row['IDCorak'],
       'IDWarna' => $row['IDWarna'],
       'IDMataUang' => 1,
       'Kurs' => 14000,
       'Masuk_yard' => 0,
       'Masuk_meter' => 0,
       'Keluar_yard' => $row['q_yard'],
       'Keluar_meter' => $row['q_meter'],
       'Grade' => $row['Grade'],
       'IDSatuan' => $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan ? $this->M_koreksi_persediaan->get_IDbarang($row['IDCorak'])->IDSatuan : 0,
       'Harga' =>  0,
       'IDMataUang' => 1,
       'Kurs' => 14000,
       'Total' => 0
     );
   }

   $this->M_koreksi_persediaan->edit_kartu_stok($data6, $row['Barcode']);
 }

}
}
}

echo true;

}else{
  echo false;
}
}

public function status_gagal($id)
{
  $data = array(
    'Batal' => 'tidak aktif',
  );

  $this->M_koreksi_persediaan->update_status_scan($data, array('IDKPScan' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('KoreksiPersediaanScan/index');
}

public function status_berhasil($id)
{
  $data = array(
    'Batal' => 'aktif',
  );

  $this->M_koreksi_persediaan->update_status_scan($data, array('IDKPScan' => $id));
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
  redirect('KoreksiPersediaanScan/index');
}

public function delete_multiple()
{
  $ID_att = $this->input->post('msg');
  if($ID_att != ""){
   $result = array();
   foreach($ID_att AS $key => $val){
     $result[] = array(
      "IDKPScan" => $ID_att[$key],
      "Batal"  => 'tidak aktif'
    );
   }
   $this->db->update_batch('tbl_koreksi_persediaan_scan', $result, 'IDKPScan');
   redirect("KoreksiPersediaanScan/index");
 }else{
   redirect("KoreksiPersediaanScan/index");
 }

}

function pencarian()
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['persediaan'] = $this->M_koreksi_persediaan->searching_scan($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/koreksi_persediaan_scan/V_koreksi_persediaan_scan', $data);
}

function show($id)
{
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['data'] = $this->M_koreksi_persediaan->find_show_scan($id);
  $data['koreksidetail'] = $this->M_koreksi_persediaan->find_detail_scan($id);
  $this->load->view('administrator/koreksi_persediaan_scan/V_show', $data);
}

function print($nomor)
{

  $data['print'] = $this->M_pembelian_asset->print_pembelian_asset($nomor);
  $id= $data['print']->IDFBA;
  $data['printdetail'] = $this->M_pembelian_asset->print_pembelian_asset_detail($id);
  $this->load->view('administrator/Pembelian_asset/V_print', $data);
}
}