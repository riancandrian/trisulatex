<!-- Programmer : Rais Naufal Hawari
 Date       : 12-07-2018 -->

 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class SaldoAwalAsset extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_saldoawalasset');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
  }

  public function index()
  {
    $data = array(
        'title_master' => "Master Saldo Awal Asset Tetap", //Judul 
        'title' => "List Data Saldo Awal Asset Tetap", //Judul Tabel
        'action' => site_url('SaldoAwalAsset/create'), //Alamat Untuk Action Form
        'button' => "Tambah Data", //Nama Button
        'SaldoAwalAsset' => $this->M_saldoawalasset->get_all() //Load data SaldoAwalAsset
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/saldoawalasset/saldoawalasset_list.php', $data);
  }
  
  //Tambah Data SaldoAwalAsset
  public function create()
  {
    $data = array(
        'title_master' => "Master Saldo Awal Asset Tetap", //Judul 
        'title' => "Tambah Data Saldo Awal Asset Tetap" , //Judul Tabel
        'action' => site_url('SaldoAwalAsset/create_action'), //Alamat Untuk Action Form
        'action_back' => site_url('SaldoAwalAsset'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'IDSaldoAwalAsset' => set_value('IDSaldoAwalAsset'),  
        'DataAsset' => $this->M_saldoawalasset->get_all_asset(), 
        'IDAsset' => set_value('IDAsset'),
        'Tanggal_Perolehan' => set_value('Tanggal_Perolehan'),  
        'Qty' => set_value('Qty'),  
        'Umur' => set_value('Umur'),  
        'Tanggal_Penyusutan' => set_value('Tanggal_Penyusutan'),  
        'Metode' => set_value('Metode'),  
        'Nilai_Saldo_awal' => set_value('Nilai_Saldo_awal'),
        'Nilai_Penyusutan_Asset' => set_value('Nilai_Penyusutan_Asset'),  
        
      );
    
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/saldoawalasset/saldoawalasset_form.php', $data);
  }
  
  //Tambah Data SaldoAwalAsset
  public function create_action()
  {
    $data['id_saldo_awal_asset'] = $this->M_saldoawalasset->tampilkan_id_saldo_awal_asset();
    if($data['id_saldo_awal_asset']!=""){
      // foreach ($data['id_saldo_awal_asset'] as $value) {
        $urutan= substr($data['id_saldo_awal_asset']->IDSaldoAwalAsset, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $nilai= str_replace(".", "", $this->input->post('Nilai_Saldo_awal'));
    $susut= str_replace(".", "", $this->input->post('Nilai_Penyusutan_Asset'));

    $data = array(
      'IDSaldoAwalAsset' =>$urutan_id, 
      'IDAsset' => $this->input->post('IDAsset'), 
      'IDGroupAsset' => $this->M_saldoawalasset->get_IDasset($this->input->post('IDAsset'))->IDGroupAsset ? $this->M_saldoawalasset->get_IDasset($this->input->post('IDAsset'))->IDGroupAsset : 0,
      'Tanggal_Perolehan' => $this->input->post('Tanggal_Perolehan'),  
      'Tanggal_Penyusutan' => $this->input->post('Tanggal_Penyusutan'),  
      'Qty' => $this->input->post('Qty'), 
      'Umur' => $this->input->post('Umur'),
      'Metode' => $this->input->post('Metode'), 
      'Nilai_Saldo_awal' => $nilai,
      'Nilai_Penyusutan_Asset' => $susut, 
    );
    $this->M_saldoawalasset->insert($data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
    redirect('SaldoAwalAsset/create');
  }
  
  //Edit Data SaldoAwalAsset
  public function update($id)
  {
    $cari = $this->M_saldoawalasset->get_by_id($id);//Cari apakah data yang dimaksud ada di database

    if($cari) {
      $data = array(
        'title_master' => "Master Saldo Awal Asset Tetap", //Judul 
        'title' => "Ubah Data Saldo Awal Asset Tetap" , //Judul Tabel
        'action' => site_url('SaldoAwalAsset/update_action'), //Alamat Untuk Action Form
        'action_back' => site_url('SaldoAwalAsset'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'IDSaldoAwalAsset' => set_value('IDSaldoAwalAsset', $cari->IDSaldoAwalAsset),  
        'DataAsset' => $this->M_saldoawalasset->get_all_asset(), 
        'IDAsset' => set_value('IDAsset', $cari->IDAsset),
        'Tanggal_Perolehan' => set_value('Tanggal_Perolehan',$cari->Tanggal_Perolehan),  
        'Qty' => set_value('Qty', $cari->Qty),  
        'Umur' => set_value('Umur', $cari->Umur),  
        'Metode' => set_value('Metode', $cari->Metode),  
        'Tanggal_Penyusutan' => set_value('Tanggal_Penyusutan', $cari->Tanggal_Penyusutan),  
        'Nilai_Saldo_awal' => set_value('Nilai_Saldo_awal', $cari->Nilai_Saldo_awal),
        'Nilai_Penyusutan_Asset' => set_value('Nilai_Penyusutan_Asset', $cari->Nilai_Penyusutan_Asset),
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/saldoawalasset/saldoawalasset_form.php', $data);
    } else {
      redirect('SaldoAwalAsset/index');
    }
  }

  //Aksi Ubah Data
  public function update_action()
  {
    $nilai= str_replace(".", "", $this->input->post('Nilai_Saldo_awal'));
    $susut= str_replace(".", "", $this->input->post('Nilai_Penyusutan_Asset'));
    $id = $this->input->post('IDSaldoAwalAsset'); //Get id sebagai penanda record yang akan dirubah
    $data = array(
      'IDAsset' => $this->input->post('IDAsset'), 
      'IDGroupAsset' => $this->M_saldoawalasset->get_IDasset($this->input->post('IDAsset'))->IDGroupAsset ? $this->M_saldoawalasset->get_IDasset($this->input->post('IDAsset'))->IDGroupAsset : 0,
      'Tanggal_Perolehan' => $this->input->post('Tanggal_Perolehan'),  
      'Tanggal_Penyusutan' => $this->input->post('Tanggal_Penyusutan'),  
      'Qty' => $this->input->post('Qty'), 
      'Umur' => $this->input->post('Umur'),
      'Metode' => $this->input->post('Metode'), 
      'Nilai_Saldo_awal' => $nilai,
      'Nilai_Penyusutan_Asset' => $susut, 
    );
    $this->M_saldoawalasset->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
    redirect('SaldoAwalAsset/index');
  }

  //Hapus Data SaldoAwalAsset
  public function delete($id)
  {
      $cari = $this->M_saldoawalasset->get_by_id($id); //Cari apakah data yang dimaksud ada di database

      if ($cari) {
        $this->M_saldoawalasset->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
        redirect('SaldoAwalAsset/index');
      } else {
       $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data Gagal di Hapus</div></center>");
       redirect('SaldoAwalAsset/index');
     }
     
   }
   public function pencarian()
   {
    $data = array(
        'title_master' => "Master Saldo Awal Asset", //Judul 
        'title' => "List Data Saldo Awal Asset", //Judul Tabel
        'action' => site_url('SaldoAwalAsset/create'), //Alamat Untuk Action Form
        'button' => "Tambah Saldo Awal Asset", //Nama Button
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $kolom= addslashes($this->input->post('jenispencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $keyword!="") {
      $data['SaldoAwalAsset']=$this->M_saldoawalasset->cari_by_kolom($kolom,$keyword);
    }elseif ($keyword!="") {
      $data['SaldoAwalAsset']=$this->M_saldoawalasset->cari_by_keyword($keyword);
    }else {
      $data['SaldoAwalAsset']=$this->M_saldoawalasset->get_all();
    }
    $this->load->view('administrator/saldoawalasset/saldoawalasset_list', $data);
  }
  

}

/* End SaldoAwalAsset.php */
