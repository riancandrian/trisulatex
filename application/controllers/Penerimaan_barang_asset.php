<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_barang_asset extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('M_penerimaan_barang_umum2');
		$this->load->model('M_agen');
		$this->load->model('M_login');
		$this->load->helper('convert_function');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index() {
		$data['PenerimaanBarang'] = $this->db->query("SELECT * FROM tbl_terima_barang_supplier_asset")->result();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/Penerimaan_barang_asset/V_index', $data);
	}
	public function create() {
		$this->load->model('M_supplier');
		
		//$data['last_number'] = $this->M_penerimaan_barang_asset->getCodeReception(date('Y'));
		$data['last_number'] = $this->db->query('SELECT MAX(RIGHT("Nomor",5)) as curr_number 
			FROM tbl_terima_barang_supplier_asset
			WHERE extract(year from "Tanggal") =\''.date('Y').'\'')->result();
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['supplier'] = $this->M_supplier->get_trisula();
		
		$data['nosjc'] = $this->db->query('SELECT tbl_po_asset."IDPOAsset" 
											FROM tbl_po_asset
											WHERE tbl_po_asset."IDPOAsset" NOT IN(SELECT tbl_terima_barang_supplier_asset."IDPOAsset" FROM tbl_terima_barang_supplier_asset) GROUP BY tbl_po_asset."IDPOAsset" 
											ORDER BY "IDPOAsset" ASC')->result();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/Penerimaan_barang_asset/V_create', $data);
	}

	public function check_PO() {

		$return['data'] = $this->db->query('SELECT tbl_po_asset_detail.*, tbl_asset."Nama_Asset" FROM tbl_po_asset, tbl_po_asset_detail, tbl_asset
											WHERE tbl_po_asset."IDPOAsset" = tbl_po_asset_detail."IDPOAsset"
											AND
												tbl_asset."IDAsset" = tbl_po_asset_detail."IDAsset"
											AND 
												tbl_po_asset_detail."IDPOAsset" = \''.$this->input->post('IDPOAsset').'\'
											')->result();
		if ($return['data']) {
			$return['error'] = false;
		} else {
			$return['error'] = true;
		}

		echo json_encode($return);
	}

	function store() {
		if (!empty($this->input->post('data'))) {
			$datas = $this->input->post('data');
		}
		
		$dataidasset = $this->db->query('SELECT MAX("IDTBAsset") as "IDTBAsset" FROM tbl_terima_barang_supplier_asset')->result();
		//die($dataidasset);
		foreach ($dataidasset as $key) {
			if($key != ""){
				$urutan 	= substr($key->IDTBAsset, 1);
				$hasil 		= base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id 	= 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			}else{
				$urutan_id = 'P000001';
			}
			//die($urutan_id);			
		}
		//=======================================Simpan Header TB Asset
		$save = array(
			"IDTBAsset" 		=> $urutan_id,
			"Tanggal" 			=> $this->input->post('tanggal'),
			"Nomor" 			=> $this->input->post('no_pb'),
			"IDSupplier" 		=> $this->input->post('id_supplier'),
			"IDPOAsset" 		=> $this->input->post('id_po_umum'),
			"IDMataUang" 		=> 1,
			"Kurs" 				=> 14000,
			"Total_Qty" 		=> $this->input->post('Total_qty'),
			"Saldo" 			=> $this->input->post('Total_qty'),
			"Keterangan" 		=> $this->input->post('Keterangan'),
			"Batal" 			=> 'aktif',
			//"Total_harga" 		=> str_replace(".", "", $this->input->post('Total_harga')),
			"CID"           	=> $this->session->userdata('id'),
    		"CTime"         	=> date("Y-m-d H:i:s"),
		);
		$this->db->insert("tbl_terima_barang_supplier_asset", $save);
  		//=================================Simpan Detail PO
		foreach ($datas as $val) {

			$dataidassetd = $this->db->query('SELECT MAX("IDTBAssetDetail") as "IDTBAssetDetail" FROM tbl_terima_barang_supplier_asset_detail')->result();
			//die($dataidasset);
			foreach ($dataidassetd as $keys) {
				if($keys != ""){
					$urutand 			= substr($keys->IDTBAssetDetail, 1);
					$hasild				= base_convert(base_convert($urutand, 36, 10) + 1, 10, 36);
					$urutan_id_detail 	= 'P' . str_pad($hasild, 6, 0, STR_PAD_LEFT);
				}else{
					$urutan_id_detail = 'P000001';
				}			
			}
			//die($urutan_id_detail);
			//==============================Simpan Detail TB Asset
			$save_detail = array(
				'IDTBAssetDetail' 		=> $urutan_id_detail,
				'IDTBAsset' 			=> $urutan_id,
				'IDPOAssetDetail'		=> $val['IDPOAssetDetail'],
				'IDAsset' 				=> $val['IDAsset'],
				'Qty' 					=> $val['Qty'],
				'Saldo' 				=> $val['Qty'],
				'Harga_Satuan' 			=> str_replace(".", "", $val['Harga_Satuan']),
				'IDMataUang' 			=> 1,
				'Kurs' 					=> 14000,
				'Subtotal' 				=> str_replace(".", "", $val['Subtotal']),
			);

			$this->db->insert("tbl_terima_barang_supplier_asset_detail", $save_detail);
			
			

			$data['id_stok'] = $this->M_penerimaan_barang_umum2->tampilkan_id_stok();
			if ($data['id_stok'] != "") {
				// foreach ($data['id_stok'] as $value) {
					$urutan = substr($data['id_stok']->IDStok, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail_stok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail_stok = 'P000001';
			}
			$save_stock = array(
				'IDStok' 		=> $urutan_id_detail_stok,
				'Tanggal' 		=> $this->input->post('tanggal'),
				'Nomor_faktur' 	=> $this->input->post('no_pb'),
				'IDFaktur' 		=> $urutan_id,
				'IDFakturDetail'=> $urutan_id_detail,
				'Jenis_faktur' 	=> 'PBA',
				'Barcode' 		=> '-',
				'IDBarang' 		=> $val['IDAsset'],
				'IDCorak' 		=> '-',
				'IDWarna' 		=> '-',
				'IDGudang' 		=> 0,
				'Harga' 		=> str_replace(".", "", $val['Harga_Satuan']),
				'IDMataUang' 	=> 1,
				'Kurs' 			=> '',
				'Total' 		=> str_replace(".", "", $val['Subtotal']),
			);
			
			$this->db->insert("tbl_stok", $save_stock);

			$data['id_kartu_stok'] = $this->M_penerimaan_barang_umum2->tampilkan_id_kartu_stok();
			if ($data['id_kartu_stok'] != "") {
				// foreach ($data['id_kartu_stok'] as $value) {
					$urutan = substr($data['id_kartu_stok']->IDKartuStok, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_detail_kartustok = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_detail_kartustok = 'P000001';
			}
			$save_stock_card = array(
				'IDKartuStok' 		=> $urutan_id_detail_kartustok,
				'Tanggal' 			=> $this->input->post('tanggal'),
				'Nomor_faktur' 		=> $this->input->post('no_pb'),
				'IDFaktur' 			=> $urutan_id,
				'IDFakturDetail' 	=> $urutan_id_detail,
				'IDStok' 			=> $urutan_id_detail_stok,
				'Jenis_faktur' 		=> 'PBA',
				'Barcode' 			=> '-',
				'IDBarang' 			=> $val['IDAsset'],
				'IDCorak' 			=> '-',
				'IDWarna' 			=> '-',
				'IDGudang' 			=> 0,
				'Harga' 			=> str_replace(".", "", $val['Harga_Satuan']),
				'IDMataUang' 		=> 1,
				'Kurs' 				=> '',
				'Total' 			=> str_replace(".", "", $val['Subtotal']),
			);

			$this->db->insert("tbl_kartu_stok", $save_stock_card);
			
			$getpobyid = $this->db->query('SELECT * FROM tbl_po_asset_detail WHERE "IDPOAssetDetail"=\''.$val['IDPOAssetDetail'].'\'')->result();
			
			//$cekgroupbarang = $this->M_penerimaan_barang_umum2->cek_group_barang($val['IDBarang']);
			$cekcoa			= $this->db->query('SELECT "Coa_Asset" 
												FROM tbl_asset, tbl_po_asset_detail
												WHERE tbl_asset."IDAsset" = tbl_po_asset_detail."IDAsset"
												AND
													tbl_po_asset_detail."IDPOAssetDetail"=\''.$val['IDPOAssetDetail'].'\'
												')->result();
			foreach ($cekcoa as $cekcoapo) {
				
			//===============================================================save jurnal
			$data['id_jurnal'] = $this->M_penerimaan_barang_umum2->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal = 'P000001';
			}

			$save_jurnal_debet = array(
				'IDJurnal' 			=> $urutan_id_jurnal,
				'Tanggal' 			=> $this->input->post('tanggal'),
				'Nomor' 			=> $this->input->post('no_pb'),
				'IDFaktur' 			=> $urutan_id,
				'IDFakturDetail' 	=> $urutan_id_detail,
				'Jenis_faktur' 		=> 'PBA',
				'IDCOA' 			=> $cekcoapo->Coa_Asset,
				'Debet' 			=> str_replace(".", "", $val['Harga_Satuan']),
				'Kredit' 			=> 0,
				'IDMataUang' 		=> 1,
				'Kurs' 				=> 14000,
				'Total_debet' 		=> str_replace(".", "", $val['Harga_Satuan']),
				'Total_kredit' 		=> 0,
				'Keterangan' 		=> '-',
				'Saldo' 			=> str_replace(".", "", $val['Harga_Satuan']),
			);
			
			$this->M_penerimaan_barang_umum2->save_jurnal($save_jurnal_debet);

			$data['id_jurnal'] = $this->M_penerimaan_barang_umum2->tampilkan_id_jurnal();
			if ($data['id_jurnal'] != "") {
				// foreach ($data['id_jurnal'] as $value) {
					$urutan = substr($data['id_jurnal']->IDJurnal, 1);
				// }
				$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
				$urutan_id_jurnal_kredit = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
			} else {
				$urutan_id_jurnal_kredit = 'P000001';
			}

			$save_jurnal_kredit = array(
				'IDJurnal' 			=> $urutan_id_jurnal_kredit,
				'Tanggal' 			=> $this->input->post('tanggal'),
				'Nomor' 			=> $this->input->post('no_pb'),
				'IDFaktur' 			=> $urutan_id,
				'IDFakturDetail' 	=> $urutan_id_detail,
				'Jenis_faktur' 		=> 'PBA',
				'IDCOA' 			=> $cekcoapo->Coa_Asset,
				'Debet' 			=> 0,
				'Kredit' 			=> str_replace(".", "", $val['Harga_Satuan']),
				'IDMataUang' 		=> 1,
				'Kurs' 				=> 14000,
				'Total_debet' 		=> 0,
				'Total_kredit' 		=> str_replace(".", "", $val['Harga_Satuan']),
				'Keterangan' 		=> '-',
				'Saldo' 			=> str_replace(".", "", $val['Harga_Satuan']),
			);
			$this->M_penerimaan_barang_umum2->save_jurnal($save_jurnal_kredit);
			}

			
		}
	}

	function soft_delete($id) {
		$batal = 'tidak aktif';
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div></center>");
		$this->db->query('UPDATE tbl_terima_barang_supplier_asset SET "Batal"=\''.$batal.'\' WHERE "IDTBAsset" = \''.$id.'\'');
		redirect(base_url('Penerimaan_barang_asset'));
	}

	function soft_success($id){
		$batal = 'aktif';
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di-non-aktifkan</div></center>");
		$this->db->query('UPDATE tbl_terima_barang_supplier_asset SET "Batal"=\''.$batal.'\' WHERE "IDTBAsset" = \''.$id.'\'');
		redirect(base_url('Penerimaan_barang_asset'));
	}

	function show($id) {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		//$data['data'] = $this->M_penerimaan_barang_asset->find($id);
		$data['data'] = $this->db->query('SELECT tbl_terima_barang_supplier_asset.*, tbl_suplier."Nama" 
											FROM tbl_terima_barang_supplier_asset, tbl_suplier 
											WHERE tbl_terima_barang_supplier_asset."IDSupplier" = tbl_suplier."IDSupplier" 
											AND tbl_terima_barang_supplier_asset."IDTBAsset" = \''.$id.'\'
									    ')->result();
		$data['pbdetail'] = $this->db->query('SELECT tbl_terima_barang_supplier_asset_detail.*, tbl_asset."Nama_Asset" 
												FROM tbl_terima_barang_supplier_asset_detail, tbl_asset
												WHERE tbl_terima_barang_supplier_asset_detail."IDAsset" = tbl_asset."IDAsset"
												AND tbl_terima_barang_supplier_asset_detail."IDTBAsset" = \''.$id.'\'
											')->result();
		$this->load->view('administrator/Penerimaan_barang_asset/V_show', $data);
	}

	function edit($id) {
		$this->load->model('M_supplier');

		$data['supplier'] = $this->db->query('SELECT * FROM tbl_suplier')->result();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->db->query('SELECT tbl_terima_barang_supplier_asset.*, tbl_suplier."Nama" 
											FROM tbl_terima_barang_supplier_asset, tbl_suplier 
											WHERE tbl_terima_barang_supplier_asset."IDSupplier" = tbl_suplier."IDSupplier" 
											AND tbl_terima_barang_supplier_asset."IDTBAsset" = \''.$id.'\'
									    ')->result();
		$data['detail'] = $this->db->query('SELECT tbl_terima_barang_supplier_asset_detail.*, tbl_asset."Nama_Asset" 
												FROM tbl_terima_barang_supplier_asset_detail, tbl_asset
												WHERE tbl_terima_barang_supplier_asset_detail."IDAsset" = tbl_asset."IDAsset"
												AND tbl_terima_barang_supplier_asset_detail."IDTBAsset" = \''.$id.'\'
											')->result();
		// var_dump($data['detail']);
		// die($data['detail']);
		$this->load->view('administrator/Penerimaan_barang_asset/V_edit', $data);
	}

	public function update_() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		$this->db->query('UPDATE tbl_terima_barang_supplier_asset 
							SET
							"Tanggal" 		= \''.$data1['Tanggal'].'\',
							"IDSupplier" 	= \''.$data1['IDSupplier'].'\',
							"Keterangan" 	= \''.$data1['Keterangan'].'\',
							"MID"           = \''.$this->session->userdata('id').'\',
				    		"MTime"         = \''.date("Y-m-d H:i:s").'\'
				    		WHERE "IDTBAsset" = \''.$data1['IDTBS'].'\'
						');

	}


}
?>