<?php

/**
 *
 */
class Corak extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_corak');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
        $data['coraks'] = $this->M_corak->tampilkan_corak();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/corak/V_corak.php', $data);
    }

    public function tambah_corak()
    {

        $data['warna'] = $this->M_corak->getWarna();
        $data['merk'] = $this->M_corak->getMerk();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/corak/V_tambah_corak', $data);
    }

    public function simpan()
    {
        $simpan= $this->input->post('simpan');
        $simtam= $this->input->post('simtam');

        $data['id_corak'] = $this->M_corak->tampilkan_id_corak();
        if($data['id_corak']!=""){
            // foreach ($data['id_corak'] as $value) {
              $urutan= substr($data['id_corak']->IDCorak, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
      $data = array(
        'IDCorak' => $urutan_id,
        'Kode_Corak' => $this->input->post('kode_corak'),
        'Corak' => $this->input->post('corak'),
        'IDMerk' => $this->input->post('id_merk'),
        'Aktif' => 'aktif',
    );

      $this->M_corak->simpan($data);
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
      if($simpan){
        redirect('Corak/index');
    }elseif($simtam){
        redirect('Corak/tambah_corak');
    }
}

public function edit($id)
{
    $data['warna'] = $this->M_corak->getWarna();
    $data['corak'] = $this->M_corak->getById($id);
    $data['merk'] = $this->M_corak->getMerk();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/corak/V_edit_corak', $data);
}

public function update()
{
    $data = array(
        'Kode_Corak' => $this->input->post('kode_corak'),
        'Corak' => $this->input->post('corak'),
        'IDMerk' => $this->input->post('id_merk'),
    );

    $this->M_corak->update($data, array('IDCorak' => $this->input->post('id')));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil diubah</div></center>");
    redirect('Corak/index');
}

public function delete($id)
{

    $data['boget']= $this->M_corak->get_bo($id);
     $data['barangget']= $this->M_corak->get_barang($id);
    if(count($data['boget']) > 0){
        if(($data['boget']->IDCorak!=$id)){   
            $this->M_corak->delete($id);
            $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
            redirect('Corak');
        }else{
            $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
            redirect('Corak');
        }
    }else if(count($data['barangget']) > 0){

        if(($data['barangget']->IDCorak!=$id)){   
            $this->M_corak->delete($id);
            $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
            redirect('Corak');
        }else{
            $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
            redirect('Corak');
        }
    }else{
        $this->M_corak->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
        redirect('Corak');
    }
}

public function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $kolom= addslashes($this->input->post('jenispencarian'));
    $status= addslashes($this->input->post('statuspencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $status!="" && $keyword!="") {
        $data['coraks']=$this->M_corak->cari($kolom,$status,$keyword);
    }elseif ($kolom!="" && $keyword!="") {
        $data['coraks']=$this->M_corak->cari_by_kolom($kolom,$keyword);
    }elseif ($status!="") {
        $data['coraks']=$this->M_corak->cari_by_status($status);
    }elseif ($keyword!="") {
        $data['coraks']=$this->M_corak->cari_by_keyword($keyword);
    }else {
        $data['coraks']=$this->M_corak->tampilkan_corak();
    }
    $this->load->view('administrator/corak/V_corak', $data);
}
}