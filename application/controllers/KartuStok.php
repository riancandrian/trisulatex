
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class KartuStok extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();

        $this->load->model('M_kartu_stok');
        $this->load->model('M_login');
           $this->load->library('pagination');
           $this->load->library('session');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
         $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $data['kartustok']=$this->M_kartu_stok->tampilkan_kartustok();
        $this->load->view('administrator/kartu_stok/V_kartu_stok.php', $data);
    }

     public function show($id)
    {
         $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $data['edit'] = $this->M_kartu_stok->getDetailId($id);
        $this->load->view('administrator/kartu_stok/V_show', $data);
    }

    public function pencarian()
    {
       $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();

      if(addslashes($this->input->post('jenispencarian')) != ""){
       $this->session->set_userdata('jenispencarian', addslashes($this->input->post('jenispencarian')));
       $this->session->set_userdata('keyword', addslashes($this->input->post('keyword')));
        $this->session->set_userdata('tanggal_mulai', addslashes($this->input->post('tanggal_mulai')));
       $this->session->set_userdata('tanggal_akhir', addslashes($this->input->post('tanggal_akhir')));
     }

       $kolom= $this->session->userdata('jenispencarian');
       $keyword= $this->session->userdata('keyword');
       $mulai= $this->session->userdata('tanggal_mulai');
       $akhir= $this->session->userdata('tanggal_akhir');
       
        if($kolom != "" && $keyword != "" && $mulai != "" && $akhir != "")
        {
           $data['cust']= $this->session->userdata('keyword');
             $data['kartustok']=$this->M_kartu_stok->cari_by_full($kolom,$keyword, $mulai, $akhir);
        }elseif($mulai != "" && $akhir != ""){
          $data['cust']= $this->session->userdata('keyword');
            $data['kartustok']=$this->M_kartu_stok->cari_by_periode($mulai, $akhir);
        }elseif($mulai != "" && $akhir != ""){
          $data['cust']= $this->session->userdata('keyword');
            $data['kartustok']=$this->M_kartu_stok->cari_by_perkey($keyword, $mulai, $akhir);
        }elseif ($kolom!="" && $keyword!="") {
          $data['cust']= $this->session->userdata('keyword');
            $data['kartustok']=$this->M_kartu_stok->cari_by_kolom($kolom,$keyword);
        }elseif ($keyword!="") {
          $data['cust']= $this->session->userdata('keyword');
            $data['kartustok']=$this->M_kartu_stok->cari_by_keyword($keyword);
        }else {
            redirect('KartuStok/index');
        }
         $this->load->view('administrator/kartu_stok/V_pencarian_kartu_stok.php', $data);
    }

    public function pencarian_kedua()
    {
       $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        
        $kolom= addslashes($this->input->post('jenispencarian'));
        $keyword= addslashes($this->input->post('keyword'));
        $mulai= $this->input->post('tanggal_mulai');
        $akhir= $this->input->post('tanggal_akhir');
        $custdes= $this->input->post('custdes');

        

        if($kolom != "" && $keyword != "" && $mulai != "" && $akhir != "" && $custdes != "")
        {
            $data['cust']= $this->input->post('custdes');
           $data['kartustok']=$this->M_kartu_stok->cari_by_full_kedua($kolom,$keyword, $mulai, $akhir, $custdes);
        }elseif($mulai != "" && $akhir != "" && $custdes != ""){
          $data['cust']= $this->input->post('custdes');
          $data['kartustok']=$this->M_kartu_stok->cari_by_periode_kedua($mulai, $akhir, $custdes);
        }elseif($mulai != "" && $akhir != "" && $custdes != ""){
          $data['cust']= $this->input->post('custdes');
          $data['kartustok']=$this->M_kartu_stok->cari_by_perkey_kedua($keyword, $mulai, $akhir, $custdes);
        }elseif ($kolom!="" && $keyword!="" && $custdes !="") {
          $data['cust']= $this->input->post('custdes');
            $data['kartustok']=$this->M_kartu_stok->cari_by_kolom_kedua($kolom,$keyword, $custdes);
        }elseif ($keyword!="" && $custdes!="") {
          $data['cust']= $this->input->post('custdes');
            $data['kartustok']=$this->M_kartu_stok->cari_by_keyword_kedua($keyword, $custdes);
        }elseif ($custdes !="") {
          $data['cust']= $this->input->post('custdes');
            $data['kartustok']=$this->M_kartu_stok->cari_by_custdes($custdes);
        }else {
            redirect('KartuStok/index');
        }
         $this->load->view('administrator/kartu_stok/V_pencarian_kartu_stok.php', $data);
    }

  public function hapus_kartu_stok($id)
    {
        $IDKartuStok = $id;
        $barcode= $this->input->post('barcode');
        $masukyard= $this->input->post('masukyard');
        $masukmeter= $this->input->post('masukmeter');
        $keluaryard= $this->input->post('keluaryard');
        $keluarmeter= $this->input->post('keluarmeter');
        $data['cust']= $this->input->post('custdes');

        //-------------------------------Hapus Kartu Stok
        $this->M_kartu_stok->delete_kartustok($IDKartuStok);


        //--------------------------------Kurangin stok tabel out
        $data_cek = array(
          'Barcode' => $this->input->post('barcode'),
          'CustDes' => $this->input->post('custdes'),
          'Grade' => $this->input->post('grade'),
        );
        $row = $this->M_kartu_stok->cek_ketersediaan_out($data_cek, 'tbl_out');
        if ($row->Panjang_Yard - $keluaryard == 0) {
          $this->M_kartu_stok->delete_out($data_cek);
        } else {
          $ubah_stok = array(
            'Panjang_Yard' => $row->Panjang_Yard - $keluaryard ,
            'Panjang_Meter' => $row->Panjang_Meter - $keluarmeter,
           );
           $this->M_kartu_stok->kurangi_stok_out($data_cek, $ubah_stok);

        }
        

         
         //-----------------------------Tambahin Tabel Stok
         $row2 = $this->M_kartu_stok->cek_ketersediaan_out($data_cek, 'tbl_stok');
         $ubah_stok2 = array(
           'Saldo_Yard' => $row2->Saldo_Yard + $keluaryard ,
           'Saldo_Meter' => $row2->Saldo_Meter + $keluarmeter,
          );
          $this->M_kartu_stok->tambah_stok($data_cek, $ubah_stok2);

        

        redirect('KartuStok/pencarian/');
          // $this->load->view('administrator/kartu_stok/V_pencarian_kartu_stok', $data);
          // echo "<script>history.back(1);</script>";
    }

    public function print_kartu_stok($id)
    {
        $data['IDKartuStok'] = $id;
        $data['barcode']= $this->input->post('barcode');
         $data['noso']= $this->input->post('noso');
         $data['party']= $this->input->post('party');
        $data['custdes']= $this->input->post('custdes');
        $data['indent']= $this->input->post('indent');
        $data['lebar']= $this->input->post('lebar');
        $data['remark']= $this->input->post('remark');
        $data['merk']= $this->input->post('merk');
        $data['grade']= $this->input->post('grade');
        $data['warnacust']= $this->input->post('warnacust');
        $data['masukyard']= $this->input->post('masukyard');
        $data['masukmeter']= $this->input->post('masukmeter');
        $data['keluaryard']= $this->input->post('keluaryard');
        $data['keluarmeter']= $this->input->post('keluarmeter');
        $data['saldoyard']= $this->input->post('saldoyard');
        $data['saldometer']= $this->input->post('saldometer');
        $idfaktur= $this->input->post('IDKartuStok');

        $data['kartu']=$this->M_kartu_stok->get_kartu($idfaktur);

         $this->load->view('administrator/kartu_stok/V_print_kartu_stok', $data);
    }

    
}


?>