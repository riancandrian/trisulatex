<?php

class Pembelian_barang_asset extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_invoice_pembelian_umum');
		$this->load->model('M_invoice_pembelian');
		$this->load->model('M_hutang');
		$this->load->model('M_agen');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index() {
		$data['invoice'] = $this->M_invoice_pembelian_umum->tampilkan_invoice_pembelianasset();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/pembelian_barang_asset/V_invoice_pembelian.php', $data);
	}

	function cek_supplier() {
		$Nomor = $this->input->post('Nomor');

		$data = $this->db->query('SELECT tbl_suplier."IDSupplier", tbl_suplier."Nama"
									FROM tbl_suplier, tbl_terima_barang_supplier_asset
									WHERE tbl_suplier."IDSupplier" = tbl_terima_barang_supplier_asset."IDSupplier"
									AND tbl_terima_barang_supplier_asset."Nomor"=\''.$Nomor.'\'
								')->result();

		echo json_encode($data);
	}

	public function status_gagal($id) {
		
		$batal = 'tidak aktif';
		$this->db->query('UPDATE tbl_pu_asset SET "Batal"=\''.$batal.'\' WHERE "IDFBAsset"=\''.$id.'\'' );
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		redirect('Pembelian_barang_asset/index');
	}

	public function status_berhasil($id) {
		$batal = 'aktif';
		$this->db->query('UPDATE tbl_pu_asset SET "Batal"=\''.$batal.'\' WHERE "IDFBAsset"=\''.$id.'\'' );
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
		redirect('Invoice_pembelian_umum/index');
	}

	public function delete_multiple() {
		$ID_att = $this->input->post('msg');
		if ($ID_att != "") {
			$result = array();
			foreach ($ID_att AS $key => $val) {
				$result[] = array(
					"IDFBUmum" => $ID_att[$key],
					"Batal" => 'tidak aktif',
				);
			}
			$this->db->update_batch('tbl_pembelian_umum', $result, 'IDFBUmum');
			redirect("Invoice_pembelian_umum/index");
		} else {
			redirect("Invoice_pembelian_umum/index");
		}
	}

	function pencarian() {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['invoice'] = $this->M_invoice_pembelian_umum->searchingasset($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
		$this->load->view('administrator/pembelian_barang_asset/V_invoice_pembelian', $data);
	}

	function show($id) {
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$data['data'] = $this->M_invoice_pembelian_umum->findasset($id);
		$data['invdetail'] = $this->M_invoice_pembelian_umum->detail_invoice_pembelianasset($id);
		$this->load->view('administrator/pembelian_barang_asset/V_show', $data);
	}
	public function tambah_invoice_pembelian() {
		$data['pb'] = $this->db->query('SELECT * from tbl_terima_barang_supplier_asset where "IDTBAsset" NOT IN (SELECT "IDTBAsset" FROM tbl_pu_asset)')->result();
		$data['kodenoinv'] = $this->M_invoice_pembelian_umum->no_invoiceasset(date('Y'));
		$data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
		$data['bank'] = $this->M_invoice_pembelian_umum->tampil_bank();
		$data['supplier'] = $this->M_invoice_pembelian_umum->tampil_supplier();
		$data['aksessetting'] = $this->M_login->aksessetting();
		$data['aksesmenu'] = $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/pembelian_barang_asset/V_tambah_invoice_pembelian', $data);
	}
	function check_penerimaan_barang() {
		
		$Nomor = $this->input->post('Nomor');
		//die($this->input->post('Nomor'));
		$return['data'] = $this->db->query('SELECT *
        from tbl_terima_barang_supplier_asset_detail, 
							tbl_terima_barang_supplier_asset, 
							tbl_asset
        where tbl_terima_barang_supplier_asset."IDTBAsset"=tbl_terima_barang_supplier_asset_detail."IDTBAsset"
				and tbl_asset."IDAsset"=tbl_terima_barang_supplier_asset_detail."IDAsset" 
				and tbl_terima_barang_supplier_asset."Nomor" = \''.$Nomor.'\'')->result();
		if ($return['data']) {
			$return['error'] = false;
		} else {
			$return['error'] = true;
		}

		echo json_encode($return);
	}

	function simpan_invoice_pembelian() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		$data3 = $this->input->post('data3');
		$data6 = $this->input->post('data6');
		$data['id_invoice'] = $this->M_invoice_pembelian_umum->tampilkan_id_invoiceasset();
		if ($data['id_invoice'] != "") {
			// foreach ($data['id_invoice'] as $value) {
				$urutan = substr($data['id_invoice']->IDFBAsset, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id = 'P000001';
		}

		$data = array(
			'IDFBAsset'				=> $urutan_id,
			'Tanggal' 				=> $data1['Tanggal'],
			'Nomor'					=> $data1['Nomor_invoice'],
			'IDSupplier' 			=> $data1['IDSupplier'],
			'IDTBAsset' 			=> $data1['IDTBAsset'],
			'PPN'					=> str_replace(".", "", $data3['PPN']),
			'Status_PPN'			=> $data1['Status_ppn'],
			'Percen_Disc' 			=> 0,
			'Disc' 					=> $data1['Discount'],
			'IDMataUang' 			=> 1,
			'Kurs' 					=> 14000,
			'Total_Qty'				=> $data1['total_qty'],
			'Saldo_Qty' 			=> $data1['total_qty'],
			'Nomor' 				=> $data1['Nomor_invoice'],
			'Grand_Total' 			=> str_replace(".", "", $data3['total_invoice_pembayaran']),
			'Keterangan' 			=> $data1['Keterangan'],
			'Batal' 				=> 'aktif',
			'CID'           		=> $this->session->userdata('id'),
    		'CTime'         		=> date("Y-m-d H:i:s"),
    		'Jatuh_Tempo' 			=> $data1['Jatuh_tempo'],
			'Tanggal_Jatuh_Tempo' 	=> $data1['Tanggal_jatuh_tempo'],
			
		);
		
		if ($this->M_invoice_pembelian_umum->addasset($data)) {
			$data = null;
			foreach ($data2 as $row) {
				$data['id_invoice_detail'] = $this->M_invoice_pembelian_umum->tampilkan_id_invoice_detailasset();
				if ($data['id_invoice_detail'] != "") {
					// foreach ($data['id_invoice_detail'] as $value) {
						$urutan = substr($data['id_invoice_detail']->IDFBAssetDetail, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_detail_inv = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_detail_inv = 'P000001';
				}

				$data = array(
					'IDFBAssetDetail' 		=> $urutan_id_detail_inv,
					'IDFBAsset' 			=> $urutan_id,
					'IDTBAssetDetail' 		=> '1',
					'IDAsset' 				=> $row['IDBarang'],
					'Qty' 					=> $row['Qty'],
					'Saldo' 				=> $row['Qty'],
					'Harga_Satuan' 			=> $row['Harga_satuan'],
					'IDMataUang' 			=> 1,
					'Kurs' 					=> 14000,
					'Subtotal'	 			=> $row['Sub_total'],
				);
				
				$this->M_invoice_pembelian_umum->add_detailasset($data);

				
		}
		$data['id_invoice_grand_total'] = $this->M_invoice_pembelian_umum->tampilkan_id_invoice_grand_totalasset();
		if ($data['id_invoice_grand_total'] != "") {
			// foreach ($data['id_invoice_grand_total'] as $value) {
				$urutan = substr($data['id_invoice_grand_total']->IDFBAssetGrandTotal, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id_grand_total = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
		} else {
			$urutan_id_grand_total = 'P000001';
		}
		$data4 = array(
			'IDFBAssetGrandTotal' 	=> $urutan_id_grand_total,
			'IDFBAsset' 			=> $urutan_id,
			'Pembayaran' 			=> 'Asset',//$data3['jenis'],
			'IDMataUang' 			=> 1,
			'Kurs' 					=> 14000,
			'DPP' 					=> str_replace(".", "", $data3['DPP']),
			'Discount' 				=> str_replace(".", "", $data3['Discount']),
			'PPN' 					=> str_replace(".", "", $data3['PPN']),
			'Grand_Total' 			=> str_replace(".", "", $data3['total_invoice_pembayaran']),
			'Sisa' 					=> str_replace(".", "", $data3['total_invoice_pembayaran']),// - str_replace(".", "", $data3['nominal']),
		);
		$this->M_invoice_pembelian_umum->add_grand_totalasset($data4);

		
				$data['id_jurnal'] = $this->M_invoice_pembelian->tampilkan_id_jurnal();
				if ($data['id_jurnal'] != "") {
					// foreach ($data['id_jurnal'] as $value) {
						$urutan = substr($data['id_jurnal']->IDJurnal, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_jurnal_kredit = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_jurnal_kredit = 'P000001';
				}

				$data['cekgroupsupplier'] = $this->M_invoice_pembelian->cek_group_supplier($data1['IDSupplier']);
				if ($data['cekgroupsupplier']->Group_Supplier == "PIHAK BERELASI") {
					$groupsupplier = 'P000628';
				} elseif ($data['cekgroupsupplier']->Group_Supplier == "PIHAK KETIGA") {
					$groupsupplier = 'P000627';
				}

				$save_jurnal_kredit = array(
					'IDJurnal' 			=> $urutan_id_jurnal_kredit,
					'Tanggal' 			=> $data1['Tanggal'],
					'Nomor' 			=> $data1['Nomor_invoice'],
					'IDFaktur' 			=> $urutan_id,
					'IDFakturDetail' 	=> $urutan_id_detail_inv,
					'Jenis_faktur' 		=> 'INVAST',
					'IDCOA' 			=> $groupsupplier,
					'Debet' 			=> 0,
					'Kredit' 			=> str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
					'IDMataUang' 		=> 1,
					'Kurs' 				=> 14000,
					'Total_debet' 		=> 0,
					'Total_kredit' 		=> str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
					'Keterangan' 		=> $data1['Keterangan'],
					'Saldo' 			=> str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
				);
				$this->M_invoice_pembelian->save_jurnal($save_jurnal_kredit);

				$data['id_jurnal'] = $this->M_invoice_pembelian->tampilkan_id_jurnal();
				if ($data['id_jurnal'] != "") {
					// foreach ($data['id_jurnal'] as $value) {
						$urutan = substr($data['id_jurnal']->IDJurnal, 1);
					// }
					$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
					$urutan_id_jurnal_ppn = 'P' . str_pad($hasil, 6, 0, STR_PAD_LEFT);
				} else {
					$urutan_id_jurnal_ppn = 'P000001';
				}
				
				$cekcoa2	= $this->M_invoice_pembelian->cekcoa($data1['Nomor']);

				$save_jurnal_debet = array(
					'IDJurnal' 			=> $urutan_id_jurnal_ppn,
					'Tanggal' 			=> $data1['Tanggal'],
					'Nomor' 			=> $data1['Nomor_invoice'],
					'IDFaktur' 			=> $urutan_id,
					'IDFakturDetail' 	=> $urutan_id_detail_inv,
					'Jenis_faktur' 		=> 'INVAST',
					'IDCOA' 			=> $cekcoa2->Coa_Asset,
					'Debet' 			=> str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
					'Kredit' 			=> 0,
					'IDMataUang' 		=> 1,
					'Kurs' 				=> 14000,
					'Total_debet' 		=> 0,
					'Total_kredit' 		=> str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
					'Keterangan' 		=> $data1['Keterangan'],
					'Saldo' 			=> str_replace(".", "", $row['Harga_satuan']) + str_replace(".", "", $row['Harga_satuan']) * 0.1,
				
				);
				$this->M_invoice_pembelian->save_jurnal($save_jurnal_debet);
				//}
						
			}


		echo json_encode($data);
	}

	public function edit($id) {
		$data['bank'] 			= $this->M_invoice_pembelian_umum->tampil_bank();
		$data['detail'] 		= $this->db->query
		('SELECT * 
from tbl_pu_asset, tbl_pu_asset_detail, tbl_asset 
where tbl_pu_asset."IDFBAsset"=tbl_pu_asset_detail."IDFBAsset" 
AND tbl_asset."IDAsset"=tbl_pu_asset_detail."IDAsset" 
AND tbl_pu_asset."IDFBAsset" = \''.$id.'\'')->result();
		$data['detailheader'] 	= $this->M_invoice_pembelian_umum->detail_invoice_pembelian_headerasset($id);
		$data['supplier'] 		= $this->M_invoice_pembelian_umum->tampil_supplier();
		$data['invoice'] 		= $this->M_invoice_pembelian_umum->getByIdAsset($id);
		$data['aksessetting'] 	= $this->M_login->aksessetting();
		$data['aksesmenu'] 		= $this->M_login->aksesmenu();
		$data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
		$this->load->view('administrator/pembelian_barang_asset/V_edit', $data);
	}

	public function ubah_invoice_pembelian() {
		$data1 = $this->input->post('data1');
		$data2 = $this->input->post('data2');
		// $data3 = $this->input->post('data3');

		$data = array(
			'Tanggal' 		=> $data1['Tanggal'],
			'Nomor' 		=> $data1['Nomor_invoice'],
			'IDSupplier' 	=> $data1['IDSupplier'],
			'Jatuh_Tempo' 	=> $data1['Jatuh_tempo'],
			'Tanggal_Jatuh_Tempo' => $data1['Tanggal_jatuh_tempo'],
			'IDMataUang' 	=> 1,
			'Kurs' 			=> 14000,
			'Total_Qty' 	=> $data1['total_qty'],
			'Saldo_Qty' 	=> $data1['total_qty'],
			'Grand_Total' 	=> str_replace(".", "", $data2['total_invoice_pembayaran']),
			'Disc' 			=> $data1['Discount'],
			'Percen_Disc' 	=> 0,
			'Status_PPN' 	=> $data1['Status_ppn'],
			'Keterangan' 	=> $data1['Keterangan'],
			'Batal' 		=> 'aktif',
			'MID'           => $this->session->userdata('id'),
    		'MTime'         => date("Y-m-d H:i:s"),

		);
		$this->M_invoice_pembelian_umum->update_masterasset($data1['IDFB'], $data);

		$datas = array(
			'IDFBAsset' 	=> $data1['IDFB'],
			'IDMataUang' 	=> 1,
			'Kurs' 			=> 14000,
			'DPP' 			=> str_replace(".", "", $data2['DPP']),
			'Discount' 		=> str_replace(".", "", $data2['Discount']),
			'PPN' 			=> str_replace(".", "", $data2['PPN']),
			'Grand_Total' 	=> str_replace(".", "", $data2['total_invoice_pembayaran']),
			'Sisa' 			=> str_replace(".", "", $data2['total_invoice_pembayaran']),

		);
		$this->M_invoice_pembelian_umum->update_grand_totalasset($data1['IDFB'], $datas);
		echo json_encode($datas);
	}

	function print_invo($nomor) {

		
		$data['print'] = $this->M_invoice_pembelian_umum->print_invoiceasset($nomor);
		$id = $data['print']->IDFBAsset;

		$data['printdetail'] = $this->M_invoice_pembelian_umum->print_invoice_detailasset($id);
		$this->load->view('administrator/pembelian_barang_asset/V_print', $data);
	}
}