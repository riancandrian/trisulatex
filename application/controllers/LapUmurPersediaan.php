<?php

class LapUmurPersediaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_laporan_umur_persediaan');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        //$data['umurpersediaan'] = $this->M_laporan_umur_persediaan->tampil_laporan_umur_persediaan();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
         $data['tampungtanggal']= $this->M_laporan_umur_persediaan->lapumur2();
        $this->load->view('administrator/laporan_umur_persediaan/V_index', $data);
    }

    function pencarian()
    {
       $data['aksessetting']= $this->M_login->aksessetting();
       $data['aksesmenu'] = $this->M_login->aksesmenu();
       $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $data['lapumur'] = $this->M_laporan_umur_persediaan->lapumur($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
       $data['umurpersediaan'] = $this->M_laporan_umur_persediaan->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));

       // echo "<pre>";
       //  print_r($data);
       //  echo "</pre>";
       $this->load->view('administrator/laporan_umur_persediaan/V_pencarian', $data);
   }
}