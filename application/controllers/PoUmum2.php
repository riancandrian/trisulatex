<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PoUmum2 extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_po');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
    date_default_timezone_set('Asia/Jakarta');
  }
  
  public function index()
  {
    $data = array(
          'title_master' => "Master Purchase Order Umum", //Judul 
          'title' => "List Data Purchase Order Umum", //Judul Tabel
          'action' => site_url('po_umum/create'), //Alamat Untuk Action Form
          'button' => "Tambah po", //Nama Button
          'po' => $this->M_po->get_all_po_umum2() //Load Data Purchase Order Umum
        );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/po_umum2/V_po_umum', $data);
  }


     //--------------------Store Laporan PO
  public function laporan_po()
  {
    $data = array(
        'title_master' => "Laporan Purchase Order Umum", //Judul 
        'title' => "Laporan Purchase Order Umum", //Judul Tabel
        'po' => $this->M_po->get_all_procedure_umum() //Load Data Purchase Order
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/po_umum2/V_laporan_po_umum', $data);
  }


    //Tambah Data Purchase Order Umum
 public function create()
 {
  $this->load->model('M_corak');
  $this->load->model('M_supplier');

  $data['barang'] = $this->M_po->tampilkan_barang_non_kain();
  $data['satuan'] = $this->M_po->tampilkan_satuan();
  $data['groupbarang'] = $this->M_po->group_barang();
  $data['kodepo'] = $this->M_po->no_po_umum2(date('Y'));
  $data['supplier'] = $this->M_supplier->get_all();
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
  $this->load->view('administrator/po_umum2/V_tambah_po_umum', $data);
}

    //Tambah Data Purchase Order Umum
public function create_action()
{
  $data = array(
    'Kode_po' => $this->input->post('Kode_po'),
    'po' => $this->input->post('po'),
    'Aktif' => $this->input->post('Aktif')

  );
  $this->M_po->insert($data);
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
  redirect('po_umum/create');
}

  function getbarang()
    {
        $IDGroupBarang = $this->input->post('IDGroupBarang');
        if($IDGroupBarang=='Asset')
        {
         $data = $this->M_po->cek_group_barang_asset();
        }else{
        $data = $this->M_po->cek_group_barang($IDGroupBarang);
      }
        echo json_encode($data);
    }

    //Ubah data Purchase Order Umum
public function edit($id)
{
  $data['barang'] = $this->M_po->tampilkan_barang_non_kain();
  $data['groupbarang'] = $this->M_po->group_barang();
  $data['satuan'] = $this->M_po->tampilkan_satuan();
  $data['datapo'] = $this->M_po->get_by_id_umum($id);
  $data['datapodetail'] = $this->M_po->get_po_detail_umum($id);
  $data['trisula'] = $this->M_po->get_supplier();
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/po_umum2/v_edit_po_umum', $data);
}


    //----------------------Aksi Ubah Batal single
public function set_edit($id)
{
 $cek = $this->M_po->get_by_id_umum($id);
     //die(print_r($cek));
 $batal = "-";
 if ($cek->Batal == "aktif") {
   $batal ="tidak aktif";
 } else {
   $batal = "aktif";
 }
 $data = array(
   'Batal' => $batal
 );

 $this->M_po->update_umum($id,$data);
 $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
 redirect('PoUmum2/index');
}

   //----------------------Aksi Ubah Batal multiple
public function delete_multiple()
{
  $ID_att = $this->input->post('msg');
  if (empty($ID_att)) {
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
    redirect("PoUmum2/index");
  }
  $result = array();
  foreach($ID_att AS $key => $val){
   $result[] = array(
    "IDPOUmum" => $ID_att[$key],
    "Batal"  => 'tidak aktif'
  );
 }
 $this->db->update_batch('tbl_purchase_order_umum', $result, 'IDPOUmum');
 $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
 redirect("PoUmum2/index");
}


   //-----------------------Pencarian
function pencarian()
{
  $data = array(
      'title_master' => "Master Purchase Order Umum", //Judul 
      'title' => "List Data Purchase Order Umum", //Judul Tabel
      'action' => site_url('po_umum2/create'), //Alamat Untuk Action Form
      'button' => "Tambah po", //Nama Button
      //'po' => $this->M_po->get_all('PO Umum') //Load Data Purchase Order Umum
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['po'] = $this->M_po->searching_umum($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'), 'PO Umum');
  $this->load->view('administrator/po_umum2/V_po_umum', $data);
}


   //------------------------------------Pencarian Store
function pencarian_store()
{
 $data = array(
         'title_master' => "Laporan Purchase Order Umum", //Judul 
         'title' => "Laporan Purchase Order Umum", //Judul Tabel
         //'po' => $this->M_po->get_all('PO TTI') //Load Data Purchase Order
       );
 $data['aksessetting']= $this->M_login->aksessetting();
 $data['aksesmenu'] = $this->M_login->aksesmenu();
 $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
 if ($this->input->post('date_from') == '') {
   $data['po'] = $this->M_po->searching_store_like($this->input->post('keyword'));
 } else {
   $data['po'] = $this->M_po->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
 }
 $this->load->view('administrator/po_umum2/V_laporan_po_umum', $data);
}



    //----------------------Detail per data
public function show($id)
{
  $data['po'] = $this->M_po->get_by_id_umum($id);
  $data['podetail'] = $this->M_po->get_po_detail_umum($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/po_umum2/V_show', $data);
}

    //------------------------Get Merk
public function getmerk()
{
  $id_corak = $this->input->post('id_corak');
  $data = $this->M_po->getmerk($id_corak);
  echo json_encode($data);
}

    //------------------------Get warna
public function getwarna()
{
  $id_kodecorak = $this->input->post('id_kodecorak');
  $data = $this->M_po->getwarna($id_kodecorak);
  echo json_encode($data);
}

public function simpan_po_umum2(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data['id_po'] = $this->M_po->tampilkan_id_po_umum2();
  if($data['id_po']!=""){
    // foreach ($data['id_po'] as $value) {
      $urutan= substr($data['id_po']->IDPOUmum, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }
  $data_atas = array(
    'IDPOUmum' => $urutan_id,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    // 'Tanggal_selesai' => $data1['Tanggal_Selesai'],
    'IDSupplier' => $data1['IDSupplier'],
    'IDAgen' => $data1['IDAgen'],
    'Total_qty' => str_replace(".", "", $data1['Total_qty']),
    'Saldo_qty' => str_replace(".", "", $data1['Total_qty']),
    'Catatan' => $data1['Catatan'],
    'Batal' => 'aktif',
    'Grand_total' => str_replace(".", "", $data1['Grand_total']),
    'IDMataUang' => 1,
    'Kurs' => 14000,
  );


  $this->M_po->add_po_umum2($data_atas); 
      // $last_insert_id = $this->M_po->terakhir_id(); 

  $data_bawah = null;
  foreach($data2 as $row){
    $data['id_po_detail'] = $this->M_po->tampilkan_id_po_detail_umum2();
    if($data['id_po_detail']!=""){
      // foreach ($data['id_po_detail'] as $value) {
        $urutan= substr($data['id_po_detail']->IDPOUmumDetail, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }
    $data_bawah = array(
      'IDPOUmumDetail' => $urutan_id_detail,
      'IDPOUmum'=>$urutan_id,
      'IDGroupBarang' => $row['GroupBarang'],
      'IDBarang' => $row['Nama'],
      'Qty' => str_replace(".", "", $row['Qty']),
      'Harga_satuan' => str_replace(".", "",$row['Harga']),
      'Saldo' => str_replace(".", "", $row['Qty']),
      'IDSatuan' => $row['Satuan'],
      'Sub_total' => str_replace(".", "",$row['Total']),
      'IDMataUang' => 1,
      'Kurs' => 14000,
    );
    $this->M_po->add_detail_po_umum2($data_bawah);
  }
      // $this->M_po->add($data_atas);
  echo json_encode($data_atas);

}

public function ubah_po(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDPO = $this->input->post('_id');

  $data_atas = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDSupplier' => $data1['IDSupplier'],
     'IDAgen' => $data1['IDAgen'],
    'Total_qty' => str_replace(".", "", $data1['Total_qty']),
    'Saldo_qty' => str_replace(".", "", $data1['Total_qty']),
    'Catatan' => $data1['Catatan'],
    'Grand_total' => str_replace(".", "", $data1['Grand_total']),
  );
  if($this->M_po->update_master_umum($IDPO,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M_po->drop_umum($row['IDPOUmumDetail']);
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
       $data['id_po_detail'] = $this->M_po->tampilkan_id_po_detail_umum2();
       if($data['id_po_detail']!=""){
        // foreach ($data['id_po_detail'] as $value) {
          $urutan= substr($data['id_po_detail']->IDPOUmumDetail, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_detail = 'P000001';
      }
      $data_atas = array(
        'IDPOUmumDetail' => $urutan_id_detail,
        'IDPOUmum'=>$IDPO,
        'IDGroupBarang' => $row['IDGroupBarang'],
        'IDBarang' => $row['Nama'],
        'Qty' => str_replace(".", "", $row['Qty']),
        'Harga_satuan' => str_replace(".", "", $row['Harga_satuan']),
        'Saldo' => str_replace(".", "", $row['Qty']),
        'IDSatuan' => $row['IDSatuan'],
        'Sub_total' => str_replace(".", "", $row['Sub_total']),
        'IDMataUang' => 1,
        'Kurs' => 14000,
      );
      if($row['IDPOUmumDetail'] ==''){
        $this->M_po->add_detail_po_umum2($data_atas);
      }
    }
  }
  
  echo true;
  
}else{
  echo false;  
}
}

function print_umum($nomor)
{

  $data['print'] = $this->M_po->print_po_umum($nomor);
  $id= $data['print']->IDPOUmum;
  $data['printdetail'] = $this->M_po->print_po_umum_detail($id);
  $this->load->view('administrator/po_umum2/V_print', $data);
}

 public function exporttoexcel_umum(){
      // create file name
        $fileName = 'POU '.time().'.xlsx';  
    // load excel library
        $this->load->library('excel');
        $empInfo = $this->M_po->exsport_excel_umum();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nomor');  
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Supplier'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Barang'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Qty'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Satuan'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Harga Satuan'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Sub Total'); 
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['Tanggal']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['Nomor']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['Nama']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['Nama_Barang']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Qty']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['Satuan']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['Harga_satuan']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['Sub_total']);
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
    // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);        
    }

}

/* End of file Po */
