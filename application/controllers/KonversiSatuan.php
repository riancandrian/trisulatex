
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class KonversiSatuan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_konversi_satuan');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
			$data['konversisatuan']=$this->M_konversi_satuan->tampilkan_konversi_satuan();
			$data['aksessetting']= $this->M_login->aksessetting();
			$data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
			$this->load->view('administrator/konversi_satuan/V_konversi_satuan.php', $data);
	}

	public function tambah_konversi()
	{
			$data['satuan']=$this->M_konversi_satuan->tampilkan_satuan();
			$data['aksessetting']= $this->M_login->aksessetting();
			$data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
			$this->load->view('administrator/konversi_satuan/V_tambah_konversi_satuan.php', $data);
	}
	public function proses_konversi()
	{
			$berat= $this->input->post('satuan_berat');
			$qty= str_replace(",", ".", $this->input->post('qty'));
			$ringan = $this->input->post('satuan_ringan');

			$simpan= $this->input->post('simpan');
			$simtam= $this->input->post('simtam');


			$database=array(
				'IDSatuanBerat'=>$berat,
				'Qty'=>$qty,
				'IDSatuanKecil'=>$ringan
			);
			$this->M_konversi_satuan->tambah_konversi($database);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
			if($simpan){
				redirect('KonversiSatuan/index');
			}elseif($simtam){
				redirect('KonversiSatuan/tambah_konversi');
			}

			
	}
	public function hapus_konversi($id)
	{

		$this->M_konversi_satuan->hapus_data_konversi($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
			redirect('KonversiSatuan/index');
	}

		public function edit_konversi($id)
	{
			$data['edit']=$this->M_konversi_satuan->ambil_konversi_byid($id);
			$data['satuan']= $this->M_konversi_satuan->tampilkan_satuan();
			$data['aksessetting']= $this->M_login->aksessetting();
			$data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
			$this->load->view('administrator/konversi_satuan/V_edit_konversi', $data);
	}

	public function proses_edit_konversi($id)
	{

			$berat= $this->input->post('satuan_berat');
			$qty= str_replace(",", ".", $this->input->post('qty'));
			$ringan = $this->input->post('satuan_ringan');

			$simpan= $this->input->post('simpan');
			$simtam= $this->input->post('simtam');


			$database=array(
				'IDSatuanBerat'=>$berat,
				'Qty'=>$qty,
				'IDSatuanKecil'=>$ringan
			);
			$this->M_konversi_satuan->aksi_edit_konversi($id, $database);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
			redirect('KonversiSatuan/index');
	}

	 public function pencarian()
    {
    	$data['aksessetting']= $this->M_login->aksessetting();
    	$data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        
        $kolom= addslashes($this->input->post('jenispencarian'));
        $keyword= addslashes($this->input->post('keyword'));

        if ($kolom!="" && $keyword!="") {
            $data['konversisatuan']=$this->M_konversi_satuan->cari_by_kolom($kolom,$keyword);
        }elseif ($keyword!="") {
            $data['konversisatuan']=$this->M_konversi_satuan->cari_by_keyword($keyword);
        }else {
            $data['konversisatuan']=$this->M_konversi_satuan->tampilkan_konversi_satuan();
        }
        $this->load->view('administrator/konversi_satuan/V_konversi_satuan', $data);
    }
}

?>