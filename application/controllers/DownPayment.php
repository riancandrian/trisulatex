
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class DownPayment extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_downpayment');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index()
	{
		$data['downpayment']=$this->M_downpayment->tampilkan_downpayment();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/down_payment/V_downpayment', $data);
	}

	public function tambah_downpayment()
	{
		$data['no_dp'] = $this->M_downpayment->getCodeDP();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/down_payment/V_tambah_downpayment', $data);
	}
	public function simpan()
	{
		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$simbar= $this->input->post('simbar');

		$nominal= str_replace(".", "", $this->input->post('nominal_dp'));

		$data['id_down_payment']=$this->M_downpayment->tampilkan_downpayment();
		if($data['id_down_payment']!=""){
			foreach ($data['id_down_payment'] as $value) {
				$urutan= substr($value->IDDownPayment, 1);
			}
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$data = array(
			'IDDownPayment' => $urutan_id,
			'NoDP' => $this->input->post('no_dp'),
			'NominalDP' => $nominal,
			'Keterangan' => $this->input->post('keterangan'),
			'Aktif' => 'aktif'
		);

		$this->M_downpayment->simpan($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('downpayment/index');
		}elseif($simtam){
			redirect('downpayment/tambah_downpayment');
		}
	}
	public function edit($id)
	{
		$data['downpayment'] = $this->M_downpayment->get_downpayment_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/down_payment/V_edit_downpayment', $data);
	}
	public function update()
	{
		$nominal= str_replace(".", "", $this->input->post('nominal_dp'));
		$data = array(
			'NoDP' => $this->input->post('no_dp'),
			'NominalDP' => $nominal,
			'Keterangan' => $this->input->post('keterangan'),
		);
		echo $this->input->post('id');
		$this->M_downpayment->update_downpayment($this->input->post('id'),$data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Ubah</div></center>");
		redirect('downpayment/index');
	}
	public function hapus_downpayment($id)
	{

		$this->M_downpayment->hapus_data_downpayment($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
		redirect('downpayment/index');
	}
	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['downpayment']=$this->M_downpayment->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['downpayment']=$this->M_downpayment->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['downpayment']=$this->M_downpayment->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['downpayment']=$this->M_downpayment->cari_by_keyword($keyword);
		}else {
			$data['downpayment']=$this->M_downpayment->tampilkan_downpayment();
		}
		$this->load->view('administrator/down_payment/V_downpayment', $data);
	}
}

?>