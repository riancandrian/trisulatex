<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sos extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_sos');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
    date_default_timezone_set('Asia/Jakarta');
  }
  
  public function index()
  {
    $data = array(
          'title_master' => "Master Sales Order Non Kain", //Judul 
          'title' => "List Data Sales Order Non Kain", //Judul Tabel
          'action' => site_url('sos/create'), //Alamat Untuk Action Form
          'button' => "Tambah So", //Nama Button
          'sos' => $this->M_sos->get_all() //Load Data Sales Order Seragam
        );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/sos/V_sos', $data);
  }

    //--------------------Store Laporan So
  public function laporan_sos()
  {
    $data = array(
        'title_master' => "Laporan Sales Order Non Kain", //Judul 
        'title' => "Laporan Sales Order Non Kain", //Judul Tabel
        'sos' => $this->M_sos->get_all_procedure() //Load Data Sales Order Seragam
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/sos/V_laporan_sos', $data);
  }

    //Tambah Data Sales Order Seragam
  public function create()
  {
    $this->load->model('M_corak');
    $data['kodesos'] = $this->M_sos->no_sos(date('Y'));
    $data['trisula'] = $this->M_sos->get_customer();
    $data['satuan'] = $this->M_sos->get_satuan();
    $data['groupbarang'] = $this->M_sos->get_group_barang();
    $data['barang'] = $this->M_sos->get_barang_seragam();
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
    $this->load->view('administrator/sos/V_tambah_sos', $data);
  }

  function getbarang()
    {
        $IDGroupBarang = $this->input->post('IDGroupBarang');
        $data = $this->M_sos->cek_group_barang($IDGroupBarang);
        echo json_encode($data);
    }

    //Ubah data Sales Order Seragam
  public function edit($id)
  {
    $this->load->model('M_corak');
    $data['datasos'] = $this->M_sos->get_by_id($id);
    $data['datasosdetail'] = $this->M_sos->get_sos_detail($id);
    $data['kodesos'] = $this->M_sos->no_sos(date('Y'));
    $data['barang'] = $this->M_sos->get_barang_seragam();
    $data['trisula'] = $this->M_sos->get_customer();
    $data['satuan'] = $this->M_sos->get_satuan();

    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/sos/V_edit_sos', $data);
  }


    //----------------------Aksi Ubah Batal single
  public function set_edit($id)
  {
   $cek = $this->M_sos->get_by_id($id);
     //die(print_r($cek));
   $batal = "-";
   if ($cek->Batal == "aktif") {
     $batal ="tidak aktif";
   } else {
     $batal = "aktif";
   }
   $data = array(
     'Batal' => $batal
   );

   $this->M_sos->update($id,$data);
   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
   redirect('sos/index');
 }

   //----------------------Aksi Ubah Batal multiple
 public function delete_multiple()
 {
  $ID_att = $this->input->post('msg');
  if (empty($ID_att)) {
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
    redirect("sos/index");
  }
  $result = array();
  foreach($ID_att AS $key => $val){
   $result[] = array(
    "IDSOS" => $ID_att[$key],
    "Batal"  => 'tidak aktif'
  );
 }
 $this->db->update_batch('tbl_sales_order_seragam', $result, 'IDSOS');
 $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
 redirect("sos/index");
}

    //----------------------Detail per data
public function show($id)
{
  $data['sos'] = $this->M_sos->get_by_id($id);
  $data['sosdetail'] = $this->M_sos->get_sos_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/sos/V_show', $data);
}

    //------------------------Get Merk
public function getmerk()
{
  $id_corak = $this->input->post('id_corak');
  $data = $this->M_sos->getmerk($id_corak);
  echo json_encode($data);
}

    //------------------------Get warna
public function getwarna()
{
  $id_kodecorak = $this->input->post('id_kodecorak');
  $data = $this->M_sos->getwarna($id_kodecorak);
  echo json_encode($data);
}

// ----------------------------- Get Harga
public function get_harga()
{
  $id_barang = $this->input->post('id_barang');
  $id_satuan = $this->input->post('id_satuan');
  
  $data = $this->M_sos->get_harga($id_barang, $id_satuan);
  echo json_encode($data);
}

//---------------------------Sismpan SOS

public function simpan_sos(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');

  $data['id_sos'] = $this->M_sos->tampilkan_id_sos();
  if($data['id_sos']!=""){
    // foreach ($data['id_sos'] as $value) {
      $urutan= substr($data['id_sos']->IDSOS, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }
  $data_atas = array(
    'IDSOS' => $urutan_id,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCustomer' => $data1['IDCustomer'],
    'TOP' => $data1['TOP'],
    'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo_qty' => $data1['Saldo'],
    'No_po_customer' => $data1['No_po_customer'],
    'Grand_total' => str_replace(".", "", $data1['Grand_total']),
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
    'Status_ppn' => $data1['Status_ppn'],
    'DPP' => str_replace(".", "", $data1['DPP']),
    'PPN' => str_replace(".", "", $data1['PPN']),
  );
  $this->M_sos->add($data_atas); 
  $data_bawah = null;
  foreach($data2 as $row){
   $data['id_sos_detail'] = $this->M_sos->tampilkan_id_sos_detail();
   if($data['id_sos_detail']!=""){
    // foreach ($data['id_sos_detail'] as $value) {
      $urutan= substr($data['id_sos_detail']->IDSOSDetail, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
     $urutan_id_detail = 'P000001';
  }
  $data_bawah = array(
    'IDSOSDetail' => $urutan_id_detail,
    'IDSOS'=>$urutan_id,
    'IDBarang' => $row['IDBarang'],
    'IDCorak' => $row['Corak'],
    'IDMerk' => $row['IDMerk'],
    'IDWarna' => $row['Warna'],
    'Qty' => $row['Qty'],
    'IDSatuan' => $row['IDSatuan'],
    'Harga' => str_replace(".", "", $row['Harga']),
    'Sub_total' => str_replace(".", "", $row['Sub_total']),
  );
  $this->M_sos->add_detail($data_bawah);
 
}
      // $this->M_sos->add($data_atas);
      echo json_encode($data_bawah);

}

public function ubah_sos(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDSOS = $this->input->post('_id');

  $data_atas = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'IDCustomer' => $data1['IDCustomer'],
    // 'TOP' => $data1['TOP'],
    'Tanggal_jatuh_tempo' => $data1['Tanggal_jatuh_tempo'],
    'IDMataUang' => $data1['IDMataUang'],
    'Kurs' => $data1['Kurs'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo_qty' => $data1['Saldo'],
    'No_po_customer' => $data1['No_po_customer'],
    'Grand_total' => $data1['Grand_total'],
    'Keterangan' => $data1['Keterangan'],
    'Batal' => $data1['Batal'],
  );

  if($this->M_sos->update_master($IDSOS,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M_sos->drop($row['IDSOSDetail']);
                  // console.log('delete temp');
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
        $data['id_sos_detail'] = $this->M_sos->tampilkan_id_sos_detail();
        if($data['id_sos_detail']!=""){
          foreach ($data['id_sos_detail'] as $value) {
            $urutan= substr($value->IDSOSDetail, 1);
          }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_detail = 'P000001';
        }
        $data_atas = array(
          'IDSOSDetail' => $urutan_id_detail,
          'IDSOS'=>$IDSOS,
          'IDBarang' => $row['IDBarang'],
          'IDCorak' => $row['Corak'],
          'IDMerk' => $row['IDMerk'],
          'IDWarna' => $row['Warna'],
          'Qty' => $row['Qty'],
          'IDSatuan' => $row['IDSatuan'],
          'Harga' => $row['Harga'],
          'Sub_total' => $row['Sub_total'],
        );
        if($row['IDSOSDetail'] ==''){
          $this->M_sos->add_detail($data_atas);
        }
      }
    }

    echo true;

  }else{
    echo false;  
        // echo json_encode($data_atas);
  }
      //   $this->M_sos->update_master($IDSOS,$data_atas);
      //   if($data3 > 0){
      //       foreach($data3 as $row){
      //           $this->M_sos->drop($row['IDSOSDetail']);
      //           //console.log('delete temp');
      //       }
      //   }


      //   foreach($data2 as $row2){
      //     $data_bawah = array(
      //         'IDSOS'=>$row2['IDSOS'],
      //         'IDWarna' => $row2['IDWarna'],
      //         'Qty' => $row2['Qty'],
      //         'Saldo' => $row2['Saldo'],
      //     );
      //     $this->M_sos->add_detail($data_bawah);
      // }
        // echo json_encode($data2);
}

  //--------------------------------pencarian
function pencarian()
{
  $data = array(
        'title_master' => "Master Sales Order Non Kain", //Judul 
        'title' => "List Data Sales Order Non Kain", //Judul Tabel
        'action' => site_url('sos/create'), //Alamat Untuk Action Form
        'button' => "Tambah So", //Nama Button
        //'po' => $this->M_sos->get_all('PO TTI') //Load Data Sales Order Seragam
      );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['sos'] = $this->M_sos->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
  $this->load->view('administrator/sos/V_sos', $data);
}

function print_data_sos($nomor)
{

  $data['print'] = $this->M_sos->print_sos($nomor);
  $id= $data['print']->IDSOS;
  $data['printdetail'] = $this->M_sos->print_sos_detail($id);
  $this->load->view('administrator/sos/V_print', $data);
}
    //------------------------------------Pencarian Store
     //--------------------------------pencarian
function pencarian_store()
{
  $data = array(
      'title_master' => "Laporan Sales Order Non Kain", //Judul 
      'title' => "Laporan Sales Order Non Kain", //Judul Tabel
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  if ($this->input->post('date_from') == '') {
    $data['sos'] = $this->M_sos->searching_store_like($this->input->post('keyword'));
  } else {
    $data['sos'] = $this->M_sos->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
  }
  $this->load->view('administrator/sos/V_laporan_sos', $data);
}
}

/* End of file So */
