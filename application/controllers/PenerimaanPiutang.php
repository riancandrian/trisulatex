<?php

class PenerimaanPiutang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_penerimaan_piutang');
        $this->load->model('M_invoice_penjualan');
        $this->load->model('M_umcustomer');
        $this->load->model('M_agen');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
        $this->load->helper('convert_function');
    }

    public function index()
    {
        $data['datas'] = $this->M_penerimaan_piutang->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/penerimaan_piutang/V_index', $data);
    }

    public function create()
    {
        //$data['inv'] = $this->M_penerimaan_piutang->tampil_inv();
        $data['code'] = $this->M_penerimaan_piutang->last_code(date('Y'));
        $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $data['customer'] = $this->M_penerimaan_piutang->get_customer();
        $data['bank'] = $this->M_penerimaan_piutang->tampil_bank();
        $this->load->view('administrator/penerimaan_piutang/V_create', $data);
    }

    function check_piutang()
    {
        $return['data']  = $this->M_penerimaan_piutang->check_piutang($this->input->post('IDCustomer'));
        if ($return['data']) {
            $return['error'] = false;
        }else{
            $return['error'] = true;
        }

        echo json_encode($return);
    }

    public function laporan_penerimaan_piutang()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/penerimaan_piutang/V_laporan_penerimaan_piutang', $data);
    }

    public function pencarian_lap_penerimaan_piutang()
    {
        $date_from= $this->input->post('date_from');
        $date_until= $this->input->post('date_until');
        $search_type= $this->input->post('jenispencarian');
        $keyword= $this->input->post('keyword');

        $data['laporanpenerimaanpiutang'] = $this->M_penerimaan_piutang->searching_laporan_penerimaan_piutang($date_from, $date_until, $search_type, $keyword);
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/penerimaan_piutang/V_laporan_penerimaan_piutang', $data);
    }

        function getcoa() {
        $jenis = $this->input->post('jenis_pembayaran');
        $supplier = $this->input->post('supplier');
        if($jenis=='transfer'){
            $data = $this->M_penerimaan_piutang->getcoa_transfer($jenis);
        }elseif($jenis=='giro')
        {
            $data = $this->M_penerimaan_piutang->getcoa_giro($jenis);
        }elseif($jenis=='cash')
        {
            $data = $this->M_penerimaan_piutang->getcoa_cash($jenis);
        }elseif($jenis=='nota_kredit')
        {
            $data = $this->M_penerimaan_piutang->getcoa_nota_kredit($supplier);
        }
        elseif($jenis=='pembulatan')
        {
            $data = $this->M_penerimaan_piutang->getcoa_pembulatan($jenis);
        }
        echo json_encode($data);
    }

    function store()
    {
        $data1 = $this->input->post('data1');
        $data2 = $this->input->post('data2');
        $data3 = $this->input->post('data3');

        $data['id_hutang'] = $this->M_penerimaan_piutang->tampilkan_id_hutang();

        if($data['id_hutang']!=""){
            foreach ($data['id_hutang'] as $value) {
                $urutan= substr($value->IDTP, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
            $urutan_id = 'P000001';
        }

        $data = array(
            'IDTP' => $urutan_id,
            'Tanggal' => $data1['Tanggal_TP'],
            'Nomor' => $data1['nomor_TP'],
            'IDCustomer' => $data1['IDCustomer'],
            //'IDMataUang' => ,
            //'Kurs' =>,
            'Total' => $data1['total'],
            //'Uang_muka' => ,
            //'Selisih' => ,
            //'Kompensasi_um' => ,
            'Pembayaran' => $data1['Pembayaran'],
            'Kelebihan_bayar' => $data1['kelebihan_bayar'],
            'Keterangan' => $data1['Keterangan'],
            'Batal' => 'aktif',
        );

        if($this->M_penerimaan_piutang->add($data)){
            $data = null;
            foreach($data2 as $row){
                $data['id_hutang_detail'] = $this->M_penerimaan_piutang->tampilkan_id_hutang_detail();
                if($data['id_hutang_detail']!=""){
                    foreach ($data['id_hutang_detail'] as $value) {
                        $urutan= substr($value->IDTPDet, 1);
                    }
                    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
                }else{
                    $urutan_id_detail = 'P000001';
                }
                $data = array(
                    'IDTPDet' => $urutan_id_detail,
                    'IDTP' => $urutan_id,
                    'IDFJ' => $this->M_penerimaan_piutang->get_penjualan($row['No_Faktur'])->IDFJK ? $this->M_penerimaan_piutang->get_penjualan($row['No_Faktur'])->IDFJK : NULL,
                    'Nomor' => $this->M_penerimaan_piutang->get_penjualan($row['No_Faktur'])->Nomor ? $this->M_penerimaan_piutang->get_penjualan($row['No_Faktur'])->Nomor : NULL,
                    'Tanggal_fj' => $this->M_penerimaan_piutang->get_penjualan($row['No_Faktur'])->Tanggal ? $this->M_penerimaan_piutang->get_penjualan($row['No_Faktur'])->Tanggal : NULL,
                    //'Nilai_fj' => ,
                    'Telah_diterima' => $row['Telah_diterima'],
                    //'Diterima' => ,
                    //'UM' => ,
                    //'Retur' => ,
                    //'Saldo' => ,
                    //'Selisih' =>
                    'IDPiutang' => $row['IDPiutang'],
                );
                $this->M_penerimaan_piutang->add_detail($data);

                //jurnal
                $data['id_jurnal'] = $this->M_penerimaan_piutang->tampilkan_id_jurnal();
                if($data['id_jurnal']!=""){
                  foreach ($data['id_jurnal'] as $value) {
                    $urutan= substr($value->IDJurnal, 1);
                }
                $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                $urutan_id_jurnal_debet_utang= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
            }else{
              $urutan_id_jurnal_debet_utang = 'P000001';
          }

          $save_jurnal_debet_utang = array(
              'IDJurnal'=>$urutan_id_jurnal_debet_utang,
              'Tanggal' => $data1['Tanggal_TP'],
              'Nomor' => $data1['nomor_TP'],
              'IDFaktur' => $urutan_id,
              'IDFakturDetail' => $urutan_id_detail,
              'Jenis_faktur' => 'PP',
              'IDCOA' => $data1['namacoa'],
              'Debet' => $row['Telah_diterima'],
              'Kredit' => 0,
              'IDMataUang' => 1,
              'Kurs' => 14000,
              'Total_debet' => 0,
              'Total_kredit' => $row['Telah_diterima'],
              'Keterangan' => $data1['Keterangan'],
              'Saldo' => $row['Telah_diterima'],
          );
          $this->M_penerimaan_piutang->save_jurnal($save_jurnal_debet_utang);

           $data['cekgroupcustomer'] = $this->M_invoice_penjualan->cek_group_customer($data1['IDCustomer']);
          if ($data['cekgroupcustomer']->Nama_Group_Customer == "PIHAK BERELASI") {
            $groupcustomer = 'P000011';
        } elseif ($data['cekgroupcustomer']->Nama_Group_Customer == "PIHAK KETIGA") {
            $groupcustomer = 'P000009';
        }

          $data['id_jurnal'] = $this->M_penerimaan_piutang->tampilkan_id_jurnal();
          if($data['id_jurnal']!=""){
              foreach ($data['id_jurnal'] as $value) {
                $urutan= substr($value->IDJurnal, 1);
            }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id_jurnal_kredit_bank= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
          $urutan_id_jurnal_kredit_bank = 'P000001';
      }

      $save_jurnal_kredit_bank = array(
          'IDJurnal'=>$urutan_id_jurnal_kredit_bank,
          'Tanggal' => $data1['Tanggal_TP'],
          'Nomor' => $data1['nomor_TP'],
          'IDFaktur' => $urutan_id,
          'IDFakturDetail' => $urutan_id_detail,
          'Jenis_faktur' => 'PP',
          'IDCOA' => $groupcustomer,
          'Debet' => 0,
          'Kredit' => $row['Telah_diterima'],
          'IDMataUang' => 1,
          'Kurs' => 14000,
          'Total_debet' => 0,
          'Total_kredit' =>$row['Telah_diterima'],
          'Keterangan' => $data1['Keterangan'],
          'Saldo' => $row['Telah_diterima'],
      );
      $this->M_penerimaan_piutang->save_jurnal($save_jurnal_kredit_bank);

      $this->M_penerimaan_piutang->update_piutang($row['Pembayaran'], $row['Saldo_Akhir'], $row['IDPiutang']);
  }

  foreach($data3 as $row2){
    $data['id_hutang_bayar'] = $this->M_penerimaan_piutang->tampilkan_id_hutang_bayar();
    if($data['id_hutang_bayar']!=""){
        foreach ($data['id_hutang_bayar'] as $value) {
            $urutan= substr($value->IDTPBayar, 1);
        }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_grand_total= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
        $urutan_id_grand_total = 'P000001';
    }
    if($row2['jenis_bayar'] == "Giro"){
        $data4 = array(
            'IDTPBayar' => $urutan_id_grand_total,
            'IDTP' => $urutan_id,
            'Jenis_pembayaran' => $row2['jenis_bayar'],
            'IDCOA' => $row2['namacoa'],
            'Nomor_giro' => $row2['no_giro'],
                // 'Nominal_pembayaran' => $data1['nominal'],
            'IDMataUang' => 1,
            'Kurs' => 14000,
                //'Status_giro' => ,
            'Tanggal_giro' => $row2['tgl_giro']
        );
    }else{
        $data4 = array(
            'IDTPBayar' => $urutan_id_grand_total,
            'IDTP' => $urutan_id,
            'Jenis_pembayaran' => $row2['jenis_bayar'],
            'IDCOA' => $row2['namacoa'],
                //'Nomor_giro' => ,
                // 'Nominal_pembayaran' => $data1['nominal'],
            'IDMataUang' => 1,
            'Kurs' => 14000,
                //'Status_giro' => ,
                // 'Tanggal_giro' => $data1['tgl_giro']
        );
    }

    $this->M_penerimaan_piutang->add_bayar($data4);
}

if($data['Pembayaran'] > 0){
$data['id_um_customer']=$this->M_umcustomer->tampilkan_umcustomer();
if($data['id_um_customer']!=""){
    foreach ($data['id_um_customer'] as $value) {
        $urutan_um_customer= substr($value->IDUmCustomer, 1);
    }
    $hasil_um_customer = base_convert(base_convert($urutan_um_customer, 36, 10) + 1, 10, 36);
    $urutan_id_um_customer= 'P'.str_pad($hasil_um_customer, 6, 0, STR_PAD_LEFT);
}else{
    $urutan_id_um_customer = 'P000001';
}
$dataum= array(
    'IDUMCustomer' => $urutan_id_um_customer,
    'IDCustomer' => $data1['IDCustomer'],
    'Tanggal_UM' => $data1['Tanggal_TP'],
    'Nomor_Faktur' => $data1['nomor_TP'],
    'Nilai_UM' => $data1['Pembayaran'],
    'Saldo_UM' => $data1['Pembayaran'],
    'IDFaktur' => $urutan_id
);
$this->M_penerimaan_piutang->add_um_customer($dataum);
}

echo true;
}else{
    echo false;
}
}

function show($id)
{
    $nomor = '';
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['data'] = $this->M_penerimaan_piutang->find($id, $nomor);
        //$data['invdetail'] = $this->M_penerimaan_piutang->find_detail($id, $nomor);
    $data['invdetail'] = "";
    $this->load->view('administrator/penerimaan_piutang/V_show', $data);
}

function print_($nomor)
{
    $id = '_';
    $data['print'] = $this->M_penerimaan_piutang->find($id, $nomor);
    $data['printdetail'] = $this->M_penerimaan_piutang->find_detail($id, $nomor);
    $this->load->view('administrator/penerimaan_piutang/V_print', $data);
}

public function status_gagal($id)
{
    $data = array(
        'Batal' => 'tidak aktif',
    );

    $this->M_penerimaan_piutang->update_status($data, array('IDTP' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('PenerimaanPiutang/index');
}

public function status_berhasil($id)
{
    $data = array(
        'Batal' => 'aktif',
    );

    $this->M_penerimaan_piutang->update_status($data, array('IDTP' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('PenerimaanPiutang/index');
}

public function delete_multiple()
{
    $ID_att = $this->input->post('msg');
    if($ID_att != ""){
        $result = array();
        foreach($ID_att AS $key => $val){
            $result[] = array(
                "IDTP" => $ID_att[$key],
                "Batal"  => 'tidak aktif'
            );
        }
        $this->db->update_batch('tbl_penerimaan_piutang', $result, 'IDTP');
        redirect("PenerimaanPiutang/index");
    }else{
        redirect("PenerimaanPiutang/index");
    }
}

function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['datas'] = $this->M_penerimaan_piutang->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), trim($this->input->post('keyword')));
    $this->load->view('administrator/penerimaan_piutang/V_index', $data);
}

function edit($id)
{
    $nomor = '';
    $data['bank'] = $this->M_penerimaan_piutang->tampil_bank();
    $data['customer'] = $this->M_penerimaan_piutang->tampil_customer();
    $data['data'] = $this->M_penerimaan_piutang->find($id, $nomor);
        //$data['detail'] = $this->M_pembayaran_hutang->find_detail($id, $nomor);
    $data['bayar'] = $this->M_penerimaan_piutang->find_bayar($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/penerimaan_piutang/V_edit', $data);
}

function piutang_bayar()
{
    $return['data']  = $this->M_penerimaan_piutang->piutang_bayar($this->input->post('IDCustomer'));
    if ($return['data']) {
        $return['error'] = false;
    }else{
        $return['error'] = true;
    }

    echo json_encode($return);
}

function update()
{
    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');

    $data = array(
        'Tanggal' => $data1['Tanggal_TP'],
        'Nomor' => $data1['nomor_TP'],
            //'IDSupplier' => $data1['IDSupplier'],
            //'IDMataUang' => ,
            //'Kurs' =>,
            //'Total' => $data1['total'],
        'Uang_muka' => $data1['Uang_muka'],
            //'Selisih' => ,
        'Kompensasi_um' => $data1['Kompensasi_um'],
    );

    $this->M_penerimaan_piutang->update_master($data1['IDTP'], $data);

    if($data2['namacoa']=="Cash")
    {
        $datas5 = array(
            'Jenis_pembayaran' => $data1['jenis'],
            'IDCOA' => 0,
                //'Nomor_giro' => ,
            'Nominal_pembayaran' => $data1['nominal'],
            'IDMataUang' => 1,
            'Kurs' => '14000',
                //'Status_giro' => ,
            'Tanggal_giro' => $data1['tgl_giro']
        );
    }else{
        $datas5 = array(
            'Jenis_pembayaran' => $data1['jenis'],
            'IDCOA' => $data1['namacoa'],
                //'Nomor_giro' => ,
            'Nominal_pembayaran' => $data1['nominal'],
            'IDMataUang' => 1,
            'Kurs' => '14000',
                //'Status_giro' => ,
            'Tanggal_giro' => $data1['tgl_giro']
        );
    }
    $this->M_penerimaan_piutang->update_bayar($data1['IDTP'], $datas5);

}
}