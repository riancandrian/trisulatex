

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class GroupCOA extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_group_coa');
		$this->load->model('M_login');

		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['groupcoa']= $this->M_group_coa->tampil_group_coa();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/group_coa/V_group_coa.php', $data);
	}

	public function tambah_group_coa()
	{
		
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/group_coa/V_tambah_group_coa', $data);
	}
	public function hapus_group_coa($id)
	{

		$this->M_group_coa->hapus_data_group_coa($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
		redirect('GroupCOA/index');
	}
	public function proses_group_coa()
	{
		$data['id_group_coa']= $this->M_group_coa->tampil_id_group_coa();
		if($data['id_group_coa']!=""){
			// foreach ($data['id_group_coa'] as $value) {
				$urutan= substr($data['id_group_coa']->IDGroupCOA, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$id_group_coa= $urutan_id;
		$kode= $this->input->post('kode');
		$group = $this->input->post('group');
		$normal = $this->input->post('normal');

		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$simbar= $this->input->post('simbar');


		$data=array(
			'IDGroupCOA'=> $id_group_coa,
			'Kode_Group_COA'=>$kode,
			'Nama_Group'=>$group,
			'Normal_Balance'=>$normal,
			'Aktif'=> 'aktif'
		);
		$this->M_group_coa->tambah_group_coa($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('GroupCOA/index');
		}elseif($simtam){
			redirect('GroupCOA/tambah_group_coa');
		}

		
	}

	public function edit_group_coa($id)
	{
		$data['edit']=$this->M_group_coa->ambil_group_coa_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/group_coa/V_edit_group_coa', $data);
	}

	public function proses_edit_group_coa($id)
	{

		$kode= $this->input->post('kode');
		$group = $this->input->post('group');
		$normal = $this->input->post('normal');
		
		
		$data=array(
			'Kode_Group_COA'=>$kode,
			'Nama_Group'=>$group,
			'Normal_Balance'=>$normal
		);
		$this->M_group_coa->aksi_edit_group_coa($id, $data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Ubah</div></center>");
		redirect('GroupCOA/index');
	}

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		
		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['groupcoa']=$this->M_group_coa->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['groupcoa']=$this->M_group_coa->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['groupcoa']=$this->M_group_coa->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['groupcoa']=$this->M_group_coa->cari_by_keyword($keyword);
		}else {
			$data['groupcoa']=$this->M_group_coa->tampil_group_coa();
		}
		$this->load->view('administrator/group_coa/V_group_coa', $data);
	}
}

?>