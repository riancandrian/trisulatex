
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Barang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_barang');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['barang']=$this->M_barang->tampilkan_barang();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/barang/V_barang.php', $data);
	}
	public function tambah_barang()
	{

		$data['groupbarang']= $this->M_barang->tampil_group_barang();
		$data['merk']= $this->M_barang->tampil_merk();
		$data['satuan']= $this->M_barang->tampil_satuan();
		$data['corak']= $this->M_barang->tampil_corak();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/barang/V_tambah_barang', $data);
	}
	public function proses_barang()
	{

		$data['id_barang']=$this->M_barang->tampilkan_urutan_barang();
		if($data['id_barang']!=""){
			// foreach ($data['id_barang'] as $value) {
				$urutan= substr($data['id_barang']->IDBarang, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}

		$id_barang= $urutan_id;
		$group= $this->input->post('group');
		$kode= $this->input->post('kode');
		$nama = $this->input->post('nama');
		$merk = $this->input->post('merk');
		$status = $this->input->post('status');
		$satuan = $this->input->post('satuan');
		$corak = $this->input->post('corak');

		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');


		$data=array(
			'IDBarang'=> $id_barang,
			'IDGroupBarang'=>$group,
			'Kode_Barang'=>$kode,
			'Nama_Barang'=>$nama,
			'IDMerk'=>$merk,
			'IDSatuan'=>$satuan,
			'IDCorak'=>$corak,
			'Aktif'=> 'aktif'
		);
		$this->M_barang->tambah_barang($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('Barang/index');
		}elseif($simtam){
			redirect('Barang/tambah_barang');
		}


	}
	public function hapus_barang($id)
	{
		$data['poget']= $this->M_barang->get_po($id);
		$data['pogetumum']= $this->M_barang->get_po_umum($id);

		if(count($data['poget']) > 0){
			if(($data['poget']->IDBarang!=$id)){   
				$this->M_barang->hapus_data_barang($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
				redirect('Barang/index');
			}else{
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
				redirect('Barang/index');
			}
		}elseif(count($data['pogetumum']) > 0){
			if(($data['pogetumum']->IDBarang!=$id)){   
				$this->M_barang->hapus_data_barang($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
				redirect('Barang/index');
			}else{
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
				redirect('Barang/index');
			}
		}else{
			$this->M_barang->hapus_data_barang($id);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
			redirect('Barang/index');
		}

	}

	public function edit_barang($id)
	{
		$data['edit']=$this->M_barang->ambil_barang_byid($id);
		$data['groupbarang']= $this->M_barang->tampil_group_barang();
		$data['merk']= $this->M_barang->tampil_merk();
		$data['satuan']= $this->M_barang->tampil_satuan();
		$data['corak']= $this->M_barang->tampil_corak();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/barang/V_edit_barang', $data);
	}

	public function proses_edit_barang($id)
	{

		$group= $this->input->post('group');
		$kode= $this->input->post('kode');
		$nama = $this->input->post('nama');
		$merk = $this->input->post('merk');
		$status = $this->input->post('status');
		$satuan = $this->input->post('satuan');
		$corak = $this->input->post('corak');


		$data=array(
			'IDGroupBarang'=>$group,
			'Kode_Barang'=>$kode,
			'Nama_Barang'=>$nama,
			'IDMerk'=>$merk,
			'IDSatuan'=>$satuan,
			'IDCorak'=>$corak
		);
		$this->M_barang->aksi_edit_barang($id, $data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil diubah</div></center>");
		redirect('Barang/index');
	}

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['barang']=$this->M_barang->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['barang']=$this->M_barang->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['barang']=$this->M_barang->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['barang']=$this->M_barang->cari_by_keyword($keyword);
		}else {
			$data['barang']=$this->M_barang->tampilkan_barang();
		}
		$this->load->view('administrator/barang/V_barang', $data);
	}

	public function show($id)
	{
		$data['barang'] = $this->M_barang->getDetailId($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/barang/V_show', $data);
	}

	public function get_merk()
	{
		$corak = $this->input->post('id_corak');

		$data = $this->M_barang->get_merk($corak);
		echo json_encode($data);
	}
}

?>