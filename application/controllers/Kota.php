
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Kota extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_kota');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['kota']=$this->M_kota->tampilkan_kota();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/kota/V_kota.php', $data);
	}
	public function tambah_kota()
	{
		
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/kota/V_tambah_kota', $data);
	}
	public function proses_kota()
	{
		$data['id_kota']=$this->M_kota->tampilkan_id_kota();
		if($data['id_kota']!=""){
			// foreach ($data['id_kota'] as $value) {
				$urutan= substr($data['id_kota']->IDKota, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$id_kota= $urutan_id;
		$kode= $this->input->post('kode');
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$status = $this->input->post('status');

		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$simbar= $this->input->post('simbar');
		$simsup= $this->input->post('simsup');


		$data=array(
			'IDKota'=>$id_kota,
			'Kode_Kota'=>$kode,
			'Provinsi'=>$provinsi,
			'Kota'=>$kota,
			'Aktif'=> 'aktif'
		);
		$this->M_kota->tambah_kota($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('Kota/index');
		}elseif($simtam){
			redirect('Kota/tambah_kota');
		}elseif($simbar){
			redirect('Customer/tambah_customer');
		}elseif($simsup){
			redirect('Supplier/create');
		}

		
	}
	public function hapus_kota($id)
	{

		$this->M_kota->hapus_data_kota($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
		redirect('Kota/index');
	}

	public function edit_kota($id)
	{
		$data['edit']=$this->M_kota->ambil_kota_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/kota/V_edit_kota', $data);
	}

	public function proses_edit_kota($id)
	{

		$kode= $this->input->post('kode');
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$status = $this->input->post('status');
		
		
		$data=array(
			'Kode_Kota'=>$kode,
			'Provinsi'=>$provinsi,
			'Kota'=>$kota
		);
		$this->M_kota->aksi_edit_kota($id, $data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
		redirect('Kota/index');
	}

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['kota']=$this->M_kota->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['kota']=$this->M_kota->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['kota']=$this->M_kota->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['kota']=$this->M_kota->cari_by_keyword($keyword);
		}else {
			$data['kota']=$this->M_kota->tampilkan_kota();
		}
		$this->load->view('administrator/kota/V_kota', $data);
	}
}

?>