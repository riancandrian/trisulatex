<?php

class GroupCustomer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_group_customer');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
       $data['groupcustomer'] = $this->M_group_customer->tampilkan_group_customer();
       $data['aksessetting']= $this->M_login->aksessetting();
       $data['aksesmenu']= $this->M_login->aksesmenu();
       $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
       $this->load->view('administrator/group_customer/V_group_customer.php', $data);
   }

   public function tambah_groupCustomer()
   {
     
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/group_customer/V_tambah_groupcustomer', $data);
}

public function simpan()
{
   $data['id_group_customer'] = $this->M_group_customer->tampilkan_id_group_customer();
   if($data['id_group_customer']!=""){
    // foreach ($data['id_group_customer'] as $value) {
      $urutan= substr($data['id_group_customer']->IDGroupCustomer, 1);
  // }
  $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
  $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
}else{
  $urutan_id = 'P000001';
}
$simpan= $this->input->post('simpan');
$simtam= $this->input->post('simtam');
$simbar= $this->input->post('simbar');
$simbarum= $this->input->post('simbarum');

$data = array(
    'IDGroupCustomer' => $urutan_id,
    'Kode_Group_Customer' => $this->input->post('kode_group'),
    'Nama_Group_Customer' => $this->input->post('groupcustomer'),
    'Aktif' => 'aktif'
);

$this->M_group_customer->simpan($data);
$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
if($simpan){
    redirect('GroupCustomer/index');
}elseif($simtam){
    redirect('GroupCustomer/tambah_groupCustomer');
}elseif($simbar){
    redirect('Customer/tambah_customer');
}
            // elseif($simbarum){
            //     redirect('Umcustomer/tambah_umcustomer');
            // }
}

public function edit($id)
{
    $data['groupcustomer'] = $this->M_group_customer->getById($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/group_customer/V_edit_groupcustomer.php', $data);
}

public function update()
{
    $data = array(
     'Kode_Group_Customer' => $this->input->post('kode_group'),
     'Nama_Group_Customer' => $this->input->post('groupcustomer')
 );

    $this->M_group_customer->update($data, array('IDGroupCustomer' => $this->input->post('id')));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('GroupCustomer/index');
}

public function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();

    $kolom= addslashes($this->input->post('jenispencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $keyword!="") {
        $data['groupcustomer']=$this->M_group_customer->cari_by_kolom($kolom,$keyword);
    }elseif ($keyword!="") {
        $data['groupcustomer']=$this->M_group_customer->cari_by_keyword($keyword);
    }else {
        $data['groupcustomer']=$this->M_group_customer->tampilkan_group_customer();
    }
    $this->load->view('administrator/group_customer/V_group_customer', $data);
}

public function delete($id)
{
    // $this->M_group_customer->delete($id);
    // $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil dihapus</div></center>");
    // redirect('GroupCustomer');


    $data['sokget']= $this->M_barang->get_so_customer($id);
    $data['sosget']= $this->M_barang->get_sos_customer($id);

    if(count($data['sokget']) > 0){
      if(($data['sokget']->IDGroupCustomer!=$id)){   
        $this->M_group_customer->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
        redirect('GroupCustomer');
      }else{
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
        redirect('GroupCustomer');
      }
    }elseif(count($data['sosget']) > 0){
      if(($data['sosget']->IDGroupCustomer!=$id)){   
        $this->M_group_customer->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
        redirect('GroupCustomer');
      }else{
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
       redirect('GroupCustomer');
      }
    }else{
      $this->M_group_customer->delete($id);
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
     redirect('GroupCustomer');
    }
}
}