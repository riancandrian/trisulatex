
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Umcustomer extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_umcustomer');
		$this->load->helper('convert_function');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$data['umcustomer']=$this->M_umcustomer->tampilkan_umcustomer();
		$this->load->view('administrator/umcustomer/V_umcustomer', $data);
	}

	public function tambah_umcustomer()
	{
		
		$data['customer']=$this->M_umcustomer->get_customer();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/umcustomer/V_tambah_umcustomer', $data);
	}
	public function simpan()
	{
		$nilai= str_replace(".", "", $this->input->post('Nilai_UM'));
		$saldo= str_replace(".", "", $this->input->post('Saldo_UM'));
		$data['id_um_customer']=$this->M_umcustomer->tampilkan_urutan_umcustomer();
		if($data['id_um_customer']!=""){
			foreach ($data['id_um_customer'] as $value) {
				$urutan= substr($value->IDUMCustomer, 1);
			}
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$data = array(
			'IDUMCustomer' =>  $urutan_id,
			'IDCustomer' => $this->input->post('IDCustomer'),
			'Tanggal_UM' => $this->input->post('Tanggal_UM'),
			'Nomor_Faktur' => $this->input->post('Nomor_Faktur'),
			'Nilai_UM' => $nilai,
			'Saldo_UM' => $nilai
		);

		$this->M_umcustomer->simpan($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
		redirect('umcustomer/tambah_umcustomer');
	}
	public function edit($id)
	{
		$data['customer']=$this->M_umcustomer->get_customer();
		$data['umcustomer'] = $this->M_umcustomer->get_umcustomer_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/umcustomer/V_edit_umcustomer', $data);
	}
	public function update()
	{
		$nilai= str_replace(".", "", $this->input->post('Nilai_UM'));
		$saldo= str_replace(".", "", $this->input->post('Saldo_UM'));

		$data = array(
			'IDCustomer' => $this->input->post('IDCustomer'),
			'Tanggal_UM' => $this->input->post('Tanggal_UM'),
			'Nomor_Faktur' => $this->input->post('Nomor_Faktur'),
			'Nilai_UM' => $nilai,
			'Saldo_UM' => $nilai,
		);
		echo $this->input->post('id');
		$this->M_umcustomer->update_umcustomer($this->input->post('id'),$data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
		redirect('umcustomer/index');
	}
	public function hapus_umcustomer($id)
	{
		$this->M_umcustomer->hapus_data_umcustomer($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
		redirect('Umcustomer/index');
	}
	public function pencarian()
	{

		$kolom= addslashes($this->input->post('jenispencarian'));
		$keyword= addslashes($this->input->post('keyword'));
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		if ($kolom!="" && $keyword!="") {
			$data['umcustomer']=$this->M_umcustomer->cari_by_kolom($kolom,$keyword);
		}elseif ($keyword!="") {
			$data['umcustomer']=$this->M_umcustomer->cari_by_keyword($keyword);
		}else {
			$data['umcustomer']=$this->M_umcustomer->tampilkan_umcustomer();
		}
		$this->load->view('administrator/umcustomer/V_umcustomer', $data);
	}
}

?>