
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Gudang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_gudang');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['gudang']=$this->M_gudang->tampilkan_gudang();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/gudang/V_gudang', $data);
	}

	public function tambah_gudang()
	{
		
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/gudang/V_tambah_gudang', $data);
	}
	public function simpan()
	{
		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$data['gudang']=$this->M_gudang->tampilkan_id_gudang();
		if($data['gudang']!=""){
			// foreach ($data['gudang'] as $value) {
				$urutan= substr($data['gudang']->IDGudang, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$data = array(
			'IDGudang'=> $urutan_id,
			'Kode_Gudang' => $this->input->post('Kode_Gudang'),
			'Nama_Gudang' => $this->input->post('Nama_Gudang'),
			'Aktif' => 'aktif'
		);

		$this->M_gudang->simpan($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('Gudang/index');
		}elseif($simtam){
			redirect('Gudang/tambah_gudang');
		}
	}
	public function edit($id)
	{
		$data['gudang'] = $this->M_gudang->get_gudang_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/gudang/V_edit_gudang', $data);
	}
	public function update()
	{
		$data = array(
			'Kode_Gudang' => $this->input->post('Kode_Gudang'),
			'Nama_Gudang' => $this->input->post('Nama_Gudang')
		);
		echo $this->input->post('id');
		$this->M_gudang->update_gudang($this->input->post('id'),$data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
		redirect('Gudang/index');
	}
	public function hapus_gudang($id)
	{

		$this->M_gudang->hapus_data_gudang($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
		redirect('Gudang/index');
	}
	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['gudang']=$this->M_gudang->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['gudang']=$this->M_gudang->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['gudang']=$this->M_gudang->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['gudang']=$this->M_gudang->cari_by_keyword($keyword);
		}else {
			$data['gudang']=$this->M_gudang->tampilkan_gudang();
		}
		$this->load->view('administrator/gudang/V_gudang', $data);
	}
}

?>