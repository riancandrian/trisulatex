<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PoUmum extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_po');
    $this->load->model('M_agen');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
    $this->load->helper('convert_function');
    date_default_timezone_set('Asia/Jakarta');
  }
  
  public function index()
  {
    $data = array(
          'title_master' => "Master Purchase Order Kain Non Trisula", //Judul 
          'title' => "List Data Purchase Order Kain Non Trisula", //Judul Tabel
          'action' => site_url('po_umum/create'), //Alamat Untuk Action Form
          'button' => "Tambah po", //Nama Button
          'po' => $this->M_po->get_all('PO Umum') //Load Data Purchase Order Umum
        );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/po_umum/V_po_umum', $data);
  }


     //--------------------Store Laporan PO
  public function laporan_po()
  {
   $data = array(
         'title_master' => "Laporan Purchase Order Kain Non Trisula", //Judul 
         'title' => "Laporan Purchase Order Kain Non Trisula", //Judul Tabel
         'po' => $this->M_po->get_all_procedure('PO Umum') //Load Data Purchase Order
       );
   $data['aksessetting']= $this->M_login->aksessetting();
   $data['aksesmenu']= $this->M_login->aksesmenu();
   $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
   $this->load->view('administrator/po_umum/V_laporan_po_umum', $data);
 }


    //Tambah Data Purchase Order Umum
 public function create()
 {
  $this->load->model('M_corak');
  $this->load->model('M_supplier');

  $data['id_po_detail'] = $this->M_po->tampilkan_id_po_detail();
  $data['kodepo'] = $this->M_po->no_po('PO Umum',date('Y'));
  $data['supplier'] = $this->M_supplier->get_all();
  $data['trisula'] = $this->M_po->get_supplier();
  $data['corak'] = $this->M_corak->all();
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
  $this->load->view('administrator/po_umum/V_tambah_po_umum', $data);
}

    //Tambah Data Purchase Order Umum
public function create_action()
{
  $data = array(
    'Kode_po' => $this->input->post('Kode_po'),
    'po' => $this->input->post('po'),
    'Aktif' => $this->input->post('Aktif')

  );
  $this->M_po->insert($data);
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
  redirect('po_umum/create');
}

    //Ubah data Purchase Order Umum
public function edit($id)
{
  $this->load->model('M_corak');
  $data['datapo'] = $this->M_po->get_by_id($id);
  $data['datapodetail'] = $this->M_po->get_po_detail($id);
      // $data['kodepo'] = $this->M_po->no_po('PO Umum');
  $data['trisula'] = $this->M_po->get_supplier();
  $data['corak'] = $this->M_corak->all();
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/po_umum/v_edit_po_umum', $data);
}


    //----------------------Aksi Ubah Batal single
public function set_edit($id)
{
 $cek = $this->M_po->get_by_id($id);
 $data['pbget'] = $this->M_po->tampilkan_get_pb($id);
 echo count($data['pbget']);
 if(count($data['pbget']) > 0){
    // foreach($data['pbget'] as $valuepb) {
  if($data['pbget']->IDPO!=$id){   

   $batal = "-";
   if ($cek->Batal == "aktif") {
     $batal ="tidak aktif";
   } else {
     $batal = "aktif";
   }
   $data = array(
     'Batal' => $batal
   );

   $this->M_po->update($id,$data);
   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");

 }else{
  $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data PO Sudah Digunakan</div></center>");
  echo "tidak";
  
}
  // }
}else{
 $batal = "-";
 if ($cek->Batal == "aktif") {
   $batal ="tidak aktif";
 } else {
   $batal = "aktif";
 }
 $data = array(
   'Batal' => $batal
 );

 $this->M_po->update($id,$data);
 $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
}
redirect('PoUmum/index');
}

   //----------------------Aksi Ubah Batal multiple
public function delete_multiple()
{
  $ID_att = $this->input->post('msg');
  if (empty($ID_att)) {
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Tidak ada data yang dipilih</div></center>");
    redirect("PoUmum/index");
  }
  $result = array();
  foreach($ID_att AS $key => $val){
   $result[] = array(
    "IDPO" => $ID_att[$key],
    "Batal"  => 'tidak aktif'
  );
 }
 $this->db->update_batch('tbl_purchase_order', $result, 'IDPO');
 $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
 redirect("PoUmum/index");
}


   //-----------------------Pencarian
function pencarian()
{
  $data = array(
      'title_master' => "Master Purchase Order Kain Non Trisula", //Judul 
      'title' => "List Data Purchase Order Kain Non Trisula", //Judul Tabel
      'action' => site_url('po_umum/create'), //Alamat Untuk Action Form
      'button' => "Tambah po", //Nama Button
      //'po' => $this->M_po->get_all('PO Umum') //Load Data Purchase Order Umum
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  $data['po'] = $this->M_po->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'), 'PO Umum');
  $this->load->view('administrator/po_umum/V_po_umum', $data);
}


   //------------------------------------Pencarian Store
function pencarian_store()
{
 $data = array(
         'title_master' => "Laporan Purchase Order Kain Non Trisula", //Judul 
         'title' => "Laporan Purchase Order Kain Non Trisula", //Judul Tabel
         //'po' => $this->M_po->get_all('PO TTI') //Load Data Purchase Order
       );
 $data['aksessetting']= $this->M_login->aksessetting();
 $data['aksesmenu'] = $this->M_login->aksesmenu();
 $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
 if ($this->input->post('date_from') == '') {
   $data['po'] = $this->M_po->searching_store_like('PO Umum', $this->input->post('keyword'));
 } else {
   $data['po'] = $this->M_po->searching_store('PO Umum', $this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
 }
 $this->load->view('administrator/po_umum/V_laporan_po_umum', $data);
}



    //----------------------Detail per data
public function show($id)
{
  $data['po'] = $this->M_po->get_by_id($id);
  $data['podetail'] = $this->M_po->get_po_detail($id);
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu']= $this->M_login->aksesmenu();
  $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
  $this->load->view('administrator/po_umum/V_show', $data);
}

    //------------------------Get Merk
public function getmerk()
{
  $id_corak = $this->input->post('id_corak');
  $data = $this->M_po->getmerk($id_corak);
  echo json_encode($data);
}

    //------------------------Get warna
public function getwarna()
{
  $id_kodecorak = $this->input->post('id_kodecorak');
  $data = $this->M_po->getwarna($id_kodecorak);
  echo json_encode($data);
}

public function simpan_po(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data['id_po'] = $this->M_po->tampilkan_id_po();
  if($data['id_po']!=""){
    // foreach ($data['id_po'] as $value) {
      $urutan= substr($data['id_po']->IDPO, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
  }else{
    $urutan_id = 'P000001';
  }
  $data_atas = array(
    'IDPO' => $urutan_id,
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'Tanggal' => $data1['Tanggal'],
    'Tanggal_selesai' => $data1['Tanggal_Selesai'],
    'IDSupplier' => $data1['IDSupplier'],
    'IDCorak' => $data1['corak'],
    'IDMerk' => $data1['merk'],
    'IDAgen' => $data1['IDAgen'],
    'Lot' => $data1['Lot'],
    'Jenis_Pesanan' => $data1['Jenis_Pesanan'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo' => $data1['Saldo'],
    'PIC' => $data1['PIC'],
    'Prioritas' => $data1['Prioritas'],
    'Bentuk' => $data1['Bentuk'],
    'Panjang' => $data1['Panjang'],
    'Point' => $data1['Point'],
    'Kirim' => $data1['Kirim'],
    'Stamping' => $data1['Stamping'],
    'Posisi' => $data1['Posisi'],
    'Posisi1' => $data1['Posisi1'],
    'Album' => $data1['Sample'],
    'M10' => $data1['M10'],
    'Kain' => $data1['Kain'],
    'Lembaran' => $data1['Lembaran'],
    'Keterangan' => $data1['Keterangan'],
    'Jenis_PO' => $data1['Jenis_PO'],
    'Batal' => $data1['Batal'],
    'IDMataUang' => 2,
    "Kurs" => '-',
    'IDBarang' => $data1['IDBarang'],
    'Grand_total' =>  str_replace(".", "", $data1['Grand_total'])
  );


  $this->M_po->add($data_atas); 
      // $last_insert_id = $this->M_po->terakhir_id(); 

  $data_bawah = null;
  foreach($data2 as $row){
    $data['id_po_detail'] = $this->M_po->tampilkan_id_po_detail();
    if($data['id_po_detail']!=""){
      // foreach ($data['id_po_detail'] as $value) {
        $urutan= substr($data['id_po_detail']->IDPODetail, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_detail = 'P000001';
    }
    $data_bawah = array(
      'IDPODetail' => $urutan_id_detail,
      'IDPO'=>$urutan_id,
      'IDWarna' => $row['IDWarna'],
      'Qty' => str_replace(".", "", $row['Qty']),
      'Saldo' => str_replace(".", "",$row['Saldo']),
      'Harga' => str_replace(".", "",$row['Harga']),
    );
    $this->M_po->add_detail($data_bawah);
  }
      // $this->M_po->add($data_atas);
  echo json_encode($data_bawah);

}

public function ubah_po(){

  $data1 = $this->input->post('_data1');
  $data2 = $this->input->post('_data2');
  $data3 = $this->input->post('_data3');
  $IDPO = $this->input->post('_id');

  $data_atas = array(
    'Tanggal' => $data1['Tanggal'],
    'Nomor' => $data1['Nomor'],
    'Tanggal' => $data1['Tanggal'],
    'Tanggal_selesai' => $data1['Tanggal_Selesai'],
    'IDSupplier' => $data1['IDSupplier'],
    'IDCorak' => $data1['corak'],
    'IDMerk' => $data1['merk'],
    'IDAgen' => $data1['IDAgen'],
    'Lot' => $data1['Lot'],
    'Jenis_Pesanan' => $data1['Jenis_Pesanan'],
    'Total_qty' => $data1['Total_qty'],
    'Saldo' => $data1['Saldo'],
    'PIC' => $data1['PIC'],
    'Prioritas' => $data1['Prioritas'],
    'Bentuk' => $data1['Bentuk'],
    'Panjang' => $data1['Panjang'],
    'Point' => $data1['Point'],
    'Kirim' => $data1['Kirim'],
    'Stamping' => $data1['Stamping'],
    'Posisi' => $data1['Posisi'],
    'Posisi1' => $data1['Posisi1'],
    'Album' => $data1['Sample'],
    'M10' => $data1['M10'],
    'Kain' => $data1['Kain'],
    'Lembaran' => $data1['Lembaran'],
    'Keterangan' => $data1['Keterangan'],
    'Jenis_PO' => $data1['Jenis_PO'],
    'Batal' => $data1['Batal'],
    'IDMataUang' => 2,
    "Kurs" => '-',
    'IDBarang' => $data1['IDBarang'],
    'Grand_total' =>  str_replace(".", "", $data1['Grand_total'])
  );
  if($this->M_po->update_master($IDPO,$data_atas)){
    $data_atas = null;
    if($data3 > 0){
      foreach($data3 as $row){
        $this->M_po->drop($row['IDPODetail']);
                  // console.log('delete temp');
      }
    }
    if($data2 != null){
      foreach($data2 as $row){
       $data['id_po_detail'] = $this->M_po->tampilkan_id_po_detail();
       if($data['id_po_detail']!=""){
        // foreach ($data['id_po_detail'] as $value) {
          $urutan= substr($data['id_po_detail']->IDPODetail, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
        $urutan_id_detail = 'P000001';
      }
      $data_atas = array(
        'IDPODetail' => $urutan_id_detail,
        'IDPO'=> $IDPO,
        'IDWarna' => $row['IDWarna'],
        'Qty' => str_replace(".", "", $row['Qty']),
        'Saldo' => str_replace(".", "", $row['Qty']),
        'Harga' => str_replace(".", "",$row['Harga'])
      );
      if($row['IDPODetail'] ==''){
        $this->M_po->add_detail($data_atas);
      }
    }
  }
  
  echo true;
  
}else{
  echo false;  
        // echo json_encode($data_atas);
}
      //   $this->M_po->update_master($IDPO,$data_atas);
      //   if($data3 > 0){
      //       foreach($data3 as $row){
      //           $this->M_po->drop($row['IDPODetail']);
      //           //console.log('delete temp');
      //       }
      //   }


      //   foreach($data2 as $row2){
      //     $data_bawah = array(
      //         'IDPO'=>$row2['IDPO'],
      //         'IDWarna' => $row2['IDWarna'],
      //         'Qty' => $row2['Qty'],
      //         'Saldo' => $row2['Saldo'],
      //     );
      //     $this->M_po->add_detail($data_bawah);
      // }
        // echo json_encode($data2);
}

function print_umum($nomor)
{

  $data['print'] = $this->M_po->print_po($nomor);
  $id= $data['print']->IDPO;
  $data['printdetail'] = $this->M_po->print_po_detail($id);
  $this->load->view('administrator/po_umum/V_print', $data);
}

public function exporttoexcel(){
      // create file name
        $fileName = 'PONT'.time().'.xlsx';  
    // load excel library
        $this->load->library('excel');
        $empInfo = $this->M_po->exsport_excel('PO Umum');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nomor');  
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Supplier'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Corak'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Warna'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Merk'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Lot'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Jenis Pesanan'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'PIC'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Prioritas'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Bentuk'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Panjang'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Point'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Kirim'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Stamping'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Posisi'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Posisi 1'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'M10'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Kain'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Lembaran'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Qty'); 
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['Tanggal']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['Nomor']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['Nama']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['Corak']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Warna']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['Merk']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['Lot']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['Jenis_Pesanan']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['PIC']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['Prioritas']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['Bentuk']);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['Panjang']);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['Point']);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['Kirim']);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['Stamping']);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['Posisi']);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['Posisi1']);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['M10']);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['Kain']);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['Lembaran']);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['Qty']);
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
    // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);        
    }

}

/* End of file Po */
