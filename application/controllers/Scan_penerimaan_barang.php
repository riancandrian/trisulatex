<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scan_penerimaan_barang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_login');
        $this->load->model('M_scan');
        date_default_timezone_set("Asia/Jakarta");
    }

    function index()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/scan_penerimaan/v_index', $data);
    }

    public function getTampungan()
    {
        $barcode = $this->input->post('barcode');
        $data = $this->M_scan->getimport($barcode);
        echo json_encode($data);
    }

    public function getin()
    {
        $barcode = $this->input->post('barcode');
        $data = $this->M_scan->getin($barcode);
        echo json_encode($data);

    }

    public function input_data()
    {
        //Parameter Cek data sama si tbl_in dan tbl_stok
        $data_cek = array(
            'Barcode' => $this->input->post('Barcode')
        );

        //Cek apakah ada data yang sama
        $cek1 = $this->M_scan->cek_ketersediaan($data_cek, "tbl_in");
        if ($cek1) {
            $response[0] = "Proses Behasil, kasus data ada yang sama";
        }
        //Kalau Gaada yang sama insert itga tabel
        else {

            // Tabel In
            $data = array(
                'Barcode' => $this->input->post('Barcode'),
                'IDBarang' => $this->input->post('Barang'),
                'IDCorak' => $this->input->post('Corak'),
                'IDWarna' => $this->input->post('Warna'),
                'IDMerk' => $this->M_scan->get_IDcorak($this->input->post('Corak'))->IDMerk ? $this->M_scan->get_IDcorak($this->input->post('Corak'))->IDMerk : 0,
                'Panjang_yard' => $this->input->post('Saldo_yard'),
                'Panjang_meter' => $this->input->post('Saldo_meter'),
                'Grade' => $this->input->post('Grade'),
                'Lebar' => $this->input->post('Lebar'),
                'IDSatuan' => $this->input->post('Satuan')
            );
            /*
            // tabel stok
            $data3 = array(
                'Tanggal' => date('Y-m-d'),
                //'Nomor_faktur' => '',
                'IDFaktur' => $this->input->post('IDT'),
                //'IDFakturDetail' => 1,
                'Jenis_faktur' => '',
                'Barcode' => $this->input->post('Barcode'),
                'IDBarang' => $this->M_scan->get_IDbarang($this->input->post('Corak'))->IDBarang ? $this->M_scan->get_IDbarang($this->input->post('Corak'))->IDBarang : 0,
                'IDCorak' => $this->M_scan->get_IDcorak($this->input->post('Corak'))->IDCorak ? $this->M_scan->get_IDcorak($this->input->post('Corak'))->IDCorak : 0,
                'IDWarna' => $this->M_scan->get_IDwarna($this->input->post('Warna'))->IDWarna ? $this->M_scan->get_IDwarna($this->input->post('Warna'))->IDWarna : 0,
                'IDGudang' => $this->M_scan->get_IDgudang()->IDGudang ? $this->M_scan->get_IDgudang()->IDGudang : 0,
                'Qty_yard' => $this->input->post('Qty_Yard'),
                'Qty_meter' => $this->input->post('Qty_Meter'),
                'Saldo_yard' => $this->input->post('Qty_Yard'),
                'Saldo_meter' => $this->input->post('Qty_Meter'),
                'Grade' => $this->input->post('Grade'),
                'IDSatuan' => $this->M_scan->get_IDsatuan($this->input->post('Satuan'))->IDSatuan ? $this->M_scan->get_IDsatuan($this->input->post('Satuan'))->IDSatuan : 0,
                'Harga' => 0,
                'IDMataUang' => 1,
                'Kurs' => '',
                'Total' => 0
            );

            $ID_stock = $this->M_scan->save_stock($data3);

            // Tabel kartu Stok
            $data2 = array(
                'Tanggal' => date('Y-m-d'),
                //'Nomor_faktur' => '',
                'IDFaktur' => $this->input->post('IDT'),
                //'IDFakturDetail' => 1,
                'IDStok' => $ID_stock,
                'Jenis_faktur' => '',
                'Barcode' => $this->input->post('Barcode'),
                'IDBarang' => $this->M_scan->get_IDbarang($this->input->post('Corak'))->IDBarang ? $this->M_scan->get_IDbarang($this->input->post('Corak'))->IDBarang : 0,
                'IDCorak' => $this->M_scan->get_IDcorak($this->input->post('Corak'))->IDCorak ? $this->M_scan->get_IDcorak($this->input->post('Corak'))->IDCorak : 0,
                'IDWarna' => $this->M_scan->get_IDwarna($this->input->post('Warna'))->IDWarna ? $this->M_scan->get_IDwarna($this->input->post('Warna'))->IDWarna : 0,
                'IDGudang' => $this->M_scan->get_IDgudang()->IDGudang ? $this->M_scan->get_IDgudang()->IDGudang : 0,
                'Masuk_yard' => $this->input->post('Qty_Yard'),
                'Masuk_meter' => $this->input->post('Qty_Meter'),
                'Keluar_yard' => 0,
                'Keluar_meter' => 0,
                'Grade' => $this->input->post('Grade'),
                'IDSatuan' => $this->M_scan->get_IDsatuan($this->input->post('Satuan'))->IDSatuan ? $this->M_scan->get_IDsatuan($this->input->post('Satuan'))->IDSatuan : 0,
                'Harga' => 0,
                'IDMataUang' => 1,
                'Kurs' => '',
                'Total' => 0
            );*/

            $response[0] = "tidak ada sama";

            if ($this->M_scan->save($data)) {

                $data = array(
                    'IDGudang' => '1',
                    // 'Saldo_yard' => 0,
                    // 'Saldo_meter' => 0
                );
                $this->M_scan->update_tbl_stok($data, $data_cek);

                $datas = array(
                     'Saldo_yard' => 0,
                     'Saldo_meter' => 0
                );
                $this->M_scan->update_tbl_tampungan($datas, $data_cek);

                $response[0] = "Berhasil";
            } else {
                $response[0] ="Gagal";
            }

        }
        echo json_encode($response);
    }

    public function index_scan()
    {
        $data['Scan'] = $this->M_scan->all_scan();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $this->load->view('administrator/scan_penerimaan/laporan_scan', $data);
    }

    function pencarian()
    {
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu'] = $this->M_login->aksesmenu();
        $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
        $data['Scan'] = $this->M_scan->searching_store_like_scan($this->input->post('keyword'));
        
        $this->load->view('administrator/scan_penerimaan/laporan_scan', $data);
    }

}