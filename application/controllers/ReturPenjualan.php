<?php

class ReturPenjualan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_retur_penjualan');
        $this->load->model('M_invoice_penjualan');
        $this->load->model('M_retur_pembelian');
        $this->load->model('M_agen');
        $this->load->model('M_login');
        $this->load->helper('convert_function');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
        $data['retur'] = $this->M_retur_penjualan->all();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/retur_penjualan/V_index', $data);
    }

    public function tambah_retur_penjualan()
    {
        $data['inv'] = $this->M_retur_penjualan->tampil_inv();
        $data['koderetur'] = $this->M_retur_penjualan->no_retur(date('Y'));
        $data['namaagent'] = $this->M_agen->ambil_agen_byid('P000001');
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $data['supplier'] = $this->M_retur_penjualan->tampil_supplier();
        $data['grandtotal'] = $this->M_retur_penjualan->tampil_invoice_grand_total();
        $this->load->view('administrator/retur_penjualan/V_create', $data);
    }

    function check_invoice_penjualan()
    {
        $return['data']  = $this->M_retur_penjualan->check_invoice_penjualan($this->input->post('Nomor'));
        if ($return['data']) {
            $return['error'] = false;
        }else{
            $return['error'] = true;
        }

        echo json_encode($return);
    }

    function cek_grand_total()
    {
        $Nomor = $this->input->post('Nomor');
        $data = $this->M_retur_penjualan->cek_grand_total($Nomor);
        echo json_encode($data);
    }

    function simpan_retur_penjualan()
    {
        $data1 = $this->input->post('data1');
        $data2 = $this->input->post('data2');
        // $data3 = $this->input->post('data3');
        $data['id_retur'] = $this->M_retur_penjualan->tampilkan_id_retur();

        if($data['id_retur']!=""){
            // foreach ($data['id_retur'] as $value) {
                $urutan= substr($data['id_retur']->IDRPK, 1);
            // }
            $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }else{
            $urutan_id = 'P000001';
        }

        $data = array(
            'IDRPK' => $urutan_id,
            'Tanggal' => $data1['Tanggal'],
            'Nomor' => $data1['RJ'],
            'IDFJK' => $this->M_retur_penjualan->get_TPKbyNomor($data1['Nomor'])->IDFJK ? $this->M_retur_penjualan->get_TPKbyNomor($data1['Nomor'])->IDFJK : 0,
            'IDCustomer' => $data1['IDSupplier'],
            'Tanggal_fj' => $data1['TanggalInv'],
            // 'Total_qty_yard' => $data1['totalqty'],
            // 'Total_qty_meter' => $data1['totalmeter'],
            // 'Saldo_yard' => $data1['totalqty'],
            // 'Saldo_meter' => $data1['totalmeter'],
            'Discount' => $data1['Discount_total'],
            'Status_ppn' =>  $data1['Status_ppn'],
            'Keterangan' => $data1['Keterangan'],
            'Batal' => 'aktif',
        );
// $this->M_retur_penjualan->add($data);
        if($this->M_retur_penjualan->add($data)){
            $data = null;
            foreach($data2 as $row){
                $data['id_retur_detail'] = $this->M_retur_penjualan->tampilkan_id_retur_detail();
                if($data['id_retur_detail']!=""){
                    // foreach ($data['id_retur_detail'] as $value) {
                        $urutan= substr($data['id_retur_detail']->IDRPKDetail, 1);
                    // }
                    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
                }else{
                    $urutan_id_detail = 'P000001';
                }
                $data = array(
                    'IDRPKDetail' => $urutan_id_detail,
                    'IDRPK' => $urutan_id,
                    'Barcode' => $row['Barcode'],
                    'IDBarang' => $row['IDBarang'],
                    'IDCorak' => $row['IDCorak'],
                    'IDWarna' => $row['IDWarna'],
                    'IDMerk' => $row['IDMerk'],
                    'Qty_yard' => $row['Qty_yard'],
                    'Qty_meter' => $row['Qty_meter'],
                    'Saldo_yard' => $row['Qty_yard'],
                    'Saldo_meter' => $row['Qty_meter'],
                    'Grade' => $row['Grade'],
                    'IDSatuan' => $row['IDSatuan'],
                    'Harga' => str_replace(".", "", $row['Harga']),
                    'Sub_total' => str_replace(".", "", $row['Sub_total']),
                );
                $this->M_retur_penjualan->add_detail($data);

                 //save stock
                $getstokbyid = $this->M_retur_pembelian->getstokbyid($row['Barcode']);
                $data_stok = array(
                  'Saldo_yard' => $getstokbyid->Saldo_yard + $row['Qty_yard'] ,
                  'Saldo_meter' => $getstokbyid->Saldo_meter + $row['Qty_meter']
              );
                $this->M_retur_pembelian->update_stok($row['Barcode'], $data_stok);


            //save stock card
                $data['id_kartu_stok'] = $this->M_retur_pembelian->tampilkan_id_kartu_stok();
                if($data['id_kartu_stok']!=""){
                  // foreach ($data['id_kartu_stok'] as $value) {
                    $urutan= substr($data['id_kartu_stok']->IDKartuStok, 1);
                // }
                $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                $urutan_id_detail_kartustok= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
            }else{
              $urutan_id_detail_kartustok = 'P000001';
          }
          $save_stock_card = array(
              'IDKartuStok'=>$urutan_id_detail_kartustok,
              'Tanggal' => $data1['Tanggal'],
              'Nomor_faktur' => $data1['RJ'],
              'IDFaktur' => $urutan_id,
              'IDFakturDetail' => $urutan_id_detail,
              'IDStok' => '-',
              'Jenis_faktur' => 'RJ',
              'Barcode' => $row['Barcode'],
              'IDBarang' => $row['IDBarang'],
              'IDCorak' => $row['IDCorak'],
              'IDWarna' => $row['IDWarna'],
              'Masuk_yard' => 0,
              'Masuk_meter' => 0,
              'Keluar_yard' => $row['Qty_yard'],
              'Keluar_meter' => $row['Qty_meter'],
              'Grade' => $row['Grade'],
              'IDSatuan' => $row['IDSatuan'],
              'Harga' => str_replace(".", "", $row['Harga']),
              'IDMataUang' => 1,
              'Kurs' => '',
              'Total' => str_replace(".", "",$row['Sub_total']),
          );

          $this->M_retur_pembelian->save_stock_card($save_stock_card);

          $data['cekgroupcustomer'] = $this->M_invoice_penjualan->cek_group_customer($data1['IDCustomer']);
          if ($data['cekgroupcustomer']->Nama_Group_Customer == "PIHAK BERELASI") {
            $groupcustomer = 'P000011';
        } elseif ($data['cekgroupcustomer']->Nama_Group_Customer == "PIHAK KETIGA") {
            $groupcustomer = 'P000009';
        }

        $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
        if($data['id_jurnal']!=""){
          // foreach ($data['id_jurnal'] as $value) {
            $urutan= substr($data['id_jurnal']->IDJurnal, 1);
        // }
        $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
        $urutan_id_jurnal= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id_jurnal = 'P000001';
  }

  $save_jurnal_kredit_piutang = array(
      'IDJurnal'=>$urutan_id_jurnal,
      'Tanggal' =>  $data1['Tanggal'],
      'Nomor' =>  $data1['RJ'],
      'IDFaktur' => $urutan_id,
      'IDFakturDetail' => $urutan_id_detail,
      'Jenis_faktur' => 'RJ',
      'IDCOA' => $groupcustomer,
      'Debet' => 0,
      'Kredit' => str_replace(".", "", $row['Harga']),
      'IDMataUang' => 1,
      'Kurs' => 14000,
      'Total_debet' => 0,
      'Total_kredit' => str_replace(".", "", $row['Harga']),
      'Keterangan' => $data1['Keterangan'],
      'Saldo' => str_replace(".", "", $row['Harga']),
  );
  $this->M_retur_pembelian->save_jurnal($save_jurnal_kredit_piutang);

  $data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
  if($data['id_jurnal']!=""){
      // foreach ($data['id_jurnal'] as $value) {
        $urutan= substr($data['id_jurnal']->IDJurnal, 1);
    // }
    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
    $urutan_id_jurnal_debet_ppn= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
}else{
  $urutan_id_jurnal_debet_ppn = 'P000001';
}

$save_jurnal_debet_ppn = array(
  'IDJurnal'=>$urutan_id_jurnal_debet_ppn,
  'Tanggal' =>  $data1['Tanggal'],
  'Nomor' =>  $data1['RJ'],
  'IDFaktur' => $urutan_id,
  'IDFakturDetail' => $urutan_id_detail,
  'Jenis_faktur' => 'RJ',
  'IDCOA' => 'P000445',
  'Debet' => str_replace(".", "",$row['Harga'])*0.1,
  'Kredit' => 0,
  'IDMataUang' => 1,
  'Kurs' => 14000,
  'Total_debet' => str_replace(".", "",$row['Harga'])*0.1,
  'Total_kredit' => 0,
  'Keterangan' => $data1['Keterangan'],
  'Saldo' => str_replace(".", "",$row['Harga'])*0.1,
);
$this->M_retur_pembelian->save_jurnal($save_jurnal_debet_ppn);

$data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
if($data['id_jurnal']!=""){
  // foreach ($data['id_jurnal'] as $value) {
    $urutan= substr($data['id_jurnal']->IDJurnal, 1);
// }
$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
$urutan_id_jurnal_debet_retur= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
}else{
  $urutan_id_jurnal_debet_retur = 'P000001';
}

$save_jurnal_debet_retur = array(
  'IDJurnal'=>$urutan_id_jurnal_debet_retur,
  'Tanggal' =>  $data1['Tanggal'],
  'Nomor' =>  $data1['RJ'],
  'IDFaktur' => $urutan_id,
  'IDFakturDetail' => $urutan_id_detail,
  'Jenis_faktur' => 'RJ',
  'IDCOA' => 'P000466',
  'Debet' =>str_replace(".", "",$row['Harga']),
  'Kredit' => 0,
  'IDMataUang' => 1,
  'Kurs' => 14000,
  'Total_debet' => str_replace(".", "",$row['Harga']),
  'Total_kredit' => 0,
  'Keterangan' => $data1['Keterangan'],
  'Saldo' => str_replace(".", "",$row['Harga']),
);
$this->M_retur_pembelian->save_jurnal($save_jurnal_debet_retur);

$data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
if($data['id_jurnal']!=""){
  // foreach ($data['id_jurnal'] as $value) {
    $urutan= substr($data['id_jurnal']->IDJurnal, 1);
// }
$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
$urutan_id_jurnal_debet_pers= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
}else{
  $urutan_id_jurnal_debet_pers = 'P000001';
}

$save_jurnal_debet_pers = array(
  'IDJurnal'=>$urutan_id_jurnal_debet_pers,
  'Tanggal' =>  $data1['Tanggal'],
  'Nomor' =>  $data1['RJ'],
  'IDFaktur' => $urutan_id,
  'IDFakturDetail' => $urutan_id_detail,
  'Jenis_faktur' => 'RJ',
  'IDCOA' => 'P000466',
  'Debet' => str_replace(".", "",$row['Harga']),
  'Kredit' => 0,
  'IDMataUang' => 1,
  'Kurs' => 14000,
  'Total_debet' => str_replace(".", "",$row['Harga']),
  'Total_kredit' => 0,
  'Keterangan' => $data1['Keterangan'],
  'Saldo' => str_replace(".", "",$row['Harga']),
);
$this->M_retur_pembelian->save_jurnal($save_jurnal_debet_pers);

$data['id_jurnal'] = $this->M_retur_pembelian->tampilkan_id_jurnal();
if($data['id_jurnal']!=""){
  // foreach ($data['id_jurnal'] as $value) {
    $urutan= substr($data['id_jurnal']->IDJurnal, 1);
// }
$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
$urutan_id_jurnal_kredit_hpp= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
}else{
  $urutan_id_jurnal_kredit_hpp = 'P000001';
}

$save_jurnal_kredit_hpp = array(
  'IDJurnal'=>$urutan_id_jurnal_kredit_hpp,
  'Tanggal' =>  $data1['Tanggal'],
  'Nomor' =>  $data1['RJ'],
  'IDFaktur' => $urutan_id,
  'IDFakturDetail' => $urutan_id_detail,
  'Jenis_faktur' => 'RJ',
  'IDCOA' => 'P000433',
  'Debet' => 0,
  'Kredit' => str_replace(".", "",$row['Harga']),
  'IDMataUang' => 1,
  'Kurs' => 14000,
  'Total_debet' => 0,
  'Total_kredit' => str_replace(".", "",$row['Harga']),
  'Keterangan' => $data1['Keterangan'],
  'Saldo' => str_replace(".", "",$row['Harga']),
);
$this->M_retur_pembelian->save_jurnal($save_jurnal_kredit_hpp);
}

            // $data['id_retur_grand_total'] = $this->M_retur_penjualan->tampilkan_id_retur_grand_total();
            // if($data['id_retur_grand_total']!=""){
            //     foreach ($data['id_retur_grand_total'] as $value) {
            //         $urutan= substr($value->IDRPKGrandTotal, 1);
            //     }
            //     $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
            //     $urutan_id_grand_total= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
            // }else{
            //     $urutan_id_grand_total = 'P000001';
            // }

            // $data4 = array(
            //     'IDRPKGrandTotal' => $urutan_id_grand_total,
            //     'IDRPK' => $urutan_id,
            //     'IDMataUang' => 1,
            //     'Kurs' => '14000',
            //     'Pembayaran' => $data1['Pembayaran'],
            //     'DPP' => $data1['DPP'],
            //     'Discount' => $data1['Discount_total'],
            //     'PPN' => $data1['PPN'],
            //     'Grand_total' => $data1['total_invoice'],
            //     'Sisa' => 1
            // );
            // $this->M_retur_penjualan->add_grand_total($data4);

echo true;
}else{
    echo false;
}
}

function print_($nomor)
{

    $data['print'] = $this->M_retur_penjualan->print_retur_pembelian($nomor);
    $id= $data['print']->IDRPK;
    $data['printdetail'] = $this->M_retur_penjualan->print_retur_pembelian_detail($id);
    $data['printgrandtotal'] = $this->M_retur_penjualan->print_grand_total($id);
    $this->load->view('administrator/retur_penjualan/V_print', $data);
}

function show($id)
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['data'] = $this->M_retur_penjualan->find($id);
    $data['datadetail'] = $this->M_retur_penjualan->find_detail($id);
    $this->load->view('administrator/retur_penjualan/V_show', $data);
}

function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu'] = $this->M_login->aksesmenu();
    $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
    $data['retur'] = $this->M_retur_penjualan->searching($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('jenispencarian'), $this->input->post('keyword'));
    $this->load->view('administrator/retur_penjualan/V_index', $data);
}

public function status_gagal($id)
{
    $data = array(
        'Batal' => 'tidak aktif',
    );

    $this->M_retur_penjualan->update_status($data, array('IDRPK' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('ReturPenjualan/index');
}

public function status_berhasil($id)
{
    $data = array(
        'Batal' => 'aktif',
    );

    $this->M_retur_penjualan->update_status($data, array('IDRPK' => $id));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('ReturPenjualan/index');
}

public function delete_multiple()
{
    $ID_att = $this->input->post('msg');
    if($ID_att != ""){
        $result = array();
        foreach($ID_att AS $key => $val){
            $result[] = array(
                "IDRPK" => $ID_att[$key],
                "Batal"  => 'tidak aktif'
            );
        }
        $this->db->update_batch('tbl_retur_penjualan_kain', $result, 'IDRPK');
        redirect("ReturPenjualan/index");
    }else{
        redirect("ReturPenjualan/index");
    }

}

public function edit($id)
{
    $data['detail'] = $this->M_retur_penjualan->find_detail($id);
    $data['supplier'] = $this->M_retur_penjualan->tampil_supplier();
    $data['retur'] = $this->M_retur_penjualan->find($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/retur_penjualan/V_edit', $data);
}

public function ubah_retur_penjualan()
{
    $data1 = $this->input->post('data1');
    $data2 = $this->input->post('data2');
    $data3 = $this->input->post('data3');

    $data = array(
        'Tanggal' => $data1['Tanggal'],
        'Nomor' => $data1['RJ'],
        'IDFJK' => $this->M_retur_penjualan->get_TPKbyNomor($data1['Nomor'])->IDFJK ? $this->M_retur_penjualan->get_TPKbyNomor($data1['Nomor'])->IDFJK : 0,
        'IDCustomer' => $data1['IDSupplier'],
        'Tanggal_fj' => $data1['TanggalInv'],
        'Total_qty_yard' => $data1['totalqty'],
        'Total_qty_meter' => $data1['totalmeter'],
        'Saldo_yard' => $data1['totalqty'],
        'Saldo_meter' => $data1['totalmeter'],
        'Discount' => $data1['Discount_total'],
        'Status_ppn' => $this->M_retur_penjualan->get_TPKbyNomor($data1['Nomor'])->Status_ppn ? $this->M_retur_penjualan->get_TPKbyNomor($data1['Nomor'])->Status_ppn : 0,
        'Keterangan' => $data1['Keterangan']
    );

    if($this->M_retur_penjualan->update_master($data1['IDRPK'],$data)){
        $data = null;
        if($data3 > 0){
            foreach($data3 as $row){
                $this->M_retur_penjualan->drop($row['IDRPKDetail']);
            }
        }
        $this->M_retur_penjualan->drop($data1['IDRPK'], $data);

        if($data2 != null){
            foreach($data2 as $row){

                $data['id_retur_detail'] = $this->M_retur_penjualan->tampilkan_id_retur_detail();
                if($data['id_retur_detail']!=""){
                    // foreach ($data['id_retur_detail'] as $value) {
                        $urutan= substr($data['id_retur_detail']->IDRPKDetail, 1);
                    // }
                    $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
                    $urutan_id_detail= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
                }else{
                    $urutan_id_detail = 'P000001';
                }
                $data = array(
                    'IDRPKDetail' => $urutan_id_detail,
                    'IDRPK' => $data1['IDRPK'],
                    'Barcode' => $row['Barcode'],
                    'IDBarang' => $row['IDBarang'],
                    'IDCorak' => $row['IDCorak'],
                    'IDWarna' => $row['IDWarna'],
                    'IDMerk' => $row['IDMerk'],
                    'Qty_yard' => $row['Qty_yard'],
                    'Qty_meter' => $row['Qty_meter'],
                    'Saldo_yard' => $row['Qty_yard'],
                    'Saldo_meter' => $row['Qty_meter'],
                    'Grade' => $row['Grade'],
                    'IDSatuan' => $row['IDSatuan'],
                    'Harga' => $row['Harga'],
                    'Sub_total' => $row['Sub_total'],
                );

                    //if($row['IDRPKDetail'] == ''){
                $this->M_retur_penjualan->add_detail($data);
                    //}elseif($row['IDRPKDetail'] != ''){
                    //    $this->M_retur_penjualan->drop($row['IDRPKDetail'], $data);
                    //}
            }
        }

            // $datas = array(
            //     'IDRPK' => $data1['IDRPK'],
            //     'IDMataUang' => 1,
            //     'Kurs' => '14000',
            //     'Pembayaran' => $data1['Pembayaran'],
            //     'DPP' => $data1['DPP'],
            //     'Discount' => $data1['Discount_total'],
            //     'PPN' => $data1['PPN'],
            //     'Grand_total' => $data1['total_invoice'],
            //     'Sisa' => 2
            // );
            // $this->M_retur_penjualan->update_grand_total($data1['IDRPK'],$datas);

        echo true;

    }else{
        echo false;
    }
}

public function laporan_retur_kain()
{
    $data = array(
        'title_master' => "Laporan Retur Penjualan Kain", //Judul 
        'title' => "Laporan Retur Penjualan Kain", //Judul Tabel
        'retur' => $this->M_retur_penjualan->get_all_procedure()
    );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/retur_penjualan/V_laporan', $data);
}

function pencarian_store()
{
  $data = array(
      'title_master' => "Laporan Retur Penjualan Kain", //Judul 
        'title' => "Laporan Retur Penjualan Kain", //Judul Tabel
    );
  $data['aksessetting']= $this->M_login->aksessetting();
  $data['aksesmenu'] = $this->M_login->aksesmenu();
  $data['aksesmenudetail'] = $this->M_login->aksesmenudetail();
  if ($this->input->post('date_from') == '') {
    $data['inv'] = $this->M_retur_penjualan->searching_store_like($this->input->post('keyword'));
} else {
    $data['inv'] = $this->M_retur_penjualan->searching_store($this->input->post('date_from'), $this->input->post('date_until'), $this->input->post('keyword'));
}
$this->load->view('administrator/retur_penjualan/V_laporan', $data);
}
}