<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input extends CI_Controller {

  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_input', 'mdl');
    date_default_timezone_set("Asia/Jakarta");
  }
  

  public function index()
  {
    
    $data = array(
      'title_master' => "Data In", //Judul 
      'title' => "Barcode Scan", //Judul Tabel
    );
  
    $this->load->view('administrator/in/index.php', $data);
  }

  public function getimport()
  {
    $barcode = $this->input->post('barcode');
    $data = $this->mdl->getimport($barcode);
    echo json_encode($data);
    
  }

  public function input_data()
  {
    $data = array(
      'IDImport' => $this->input->post('IDImport'),
      'Tanggal' => date('Y-m-d'),
      'Buyer' => $this->input->post('Buyer'),
      'Barcode' => $this->input->post('Barcode'),
      'NoSO' => $this->input->post('NoSO'),
      'Party' => $this->input->post('Party'),
      'Indent' => $this->input->post('Indent'),
      'CustDes' => $this->input->post('CustDes'),
      'WarnaCust' => $this->input->post('WarnaCust'),
      'Panjang_Yard' => floatval($this->input->post('Panjang_Yard')),
      'Panjang_Meter' => floatval($this->input->post('Panjang_Meter')),
      'Grade' => $this->input->post('Grade'),
      'Remark' => $this->input->post('Remark'),
      'Lebar' => $this->input->post('Lebar'),
      'Satuan' => $this->input->post('Satuan'),
     );

     $data2 = array(
      'IDFaktur' => $this->input->post('IDImport'),
      'Tanggal' => date('Y-m-d'),
      'Barcode' => $this->input->post('Barcode'),
      'NoSO' => $this->input->post('NoSO'),
      'CustDes' => $this->input->post('CustDes'),
      'WarnaCust' => $this->input->post('WarnaCust'),
      'Masuk_Yard' => floatval($this->input->post('Panjang_Yard')),
      'Masuk_Meter' => floatval($this->input->post('Panjang_Meter')),
      'Keluar_Yard' => 0,
      'Keluar_Meter' => 0,
      'Grade' => $this->input->post('Grade'),
      'Lebar' => $this->input->post('Lebar'),
      'Satuan' => $this->input->post('Satuan'),
     );

     $data3 = array(
      'IDFaktur' => $this->input->post('IDImport'),
      'Tanggal' => date('Y-m-d'),
      'Barcode' => $this->input->post('Barcode'),
      'NoSO' => $this->input->post('NoSO'),
      'Party' => $this->input->post('Party'),
      'Indent' => $this->input->post('Indent'),
      'CustDes' => $this->input->post('CustDes'),
      'WarnaCust' => $this->input->post('WarnaCust'),
      'Panjang_Yard' => floatval($this->input->post('Panjang_Yard')),
      'Panjang_Meter' => floatval($this->input->post('Panjang_Meter')),
      'Saldo_Yard' => floatval($this->input->post('Saldo_Yard')),
      'Saldo_Meter' => floatval($this->input->post('Saldo_Meter')),
      'Grade' => $this->input->post('Grade'),
      'Remark' => $this->input->post('Remark'),
      'Lebar' => $this->input->post('Lebar'),
      'Satuan' => $this->input->post('Satuan'),
     );
     
  
     if ($this->mdl->insert_data($data, $data2, $data3)) {
      $response[0] = "Berhasil";
     } else {
       $response[0] ="Gagal";
     }
     echo json_encode($response);
  }

}

/* End of file Input.php */
