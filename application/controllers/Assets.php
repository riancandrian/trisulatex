<?php

class Assets extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_asset');
        $this->load->model('M_login');
        $this->load->helper('form', 'url');
    }

    public function index()
    {
        $data['assets'] = $this->M_asset->tampilkan_asset();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/asset/V_asset.php', $data);
    }

    public function tambah_asset()
    {
        $data['coa_asset'] = $this->M_asset->coa_asset();
        $data['coa_akumulasi'] = $this->M_asset->coa_akumulasi();
        $data['coa_beban'] = $this->M_asset->coa_beban();
        $data['group_asset'] = $this->M_asset->group_asset();
        $data['aksessetting']= $this->M_login->aksessetting();
        $data['aksesmenu']= $this->M_login->aksesmenu();
        $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
        $this->load->view('administrator/asset/V_tambah_asset', $data);
    }

    function simpan()
    {
        $simpan= $this->input->post('simpan');
        $simtam= $this->input->post('simtam');

        $data['id_asset'] = $this->M_asset->tampilkan_id_asset();
        if($data['id_asset'] !=""){
            // foreach ($data['id_asset'] as $value) {
              $urutan= substr($data['id_asset']->IDAsset, 1);
          // }
          $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
          $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
      }else{
          $urutan_id = 'P000001';
      }
      
      $data = array(
        'IDAsset' => $urutan_id,
        'Kode_Asset' => $this->input->post('kode_asset'),
        'Nama_Asset' => $this->input->post('nama_asset'),
        'IDGroupAsset' => $this->input->post('group_asset'),
        'Aktif' => 'aktif',
        'Coa_Asset' => $this->input->post('coa_asset'),
        'Coa_Akumulasi_Asset' => $this->input->post('coa_akumulasi'),
        'Coa_Beban_Asset' =>  $this->input->post('coa_beban')
    );

      $this->M_asset->simpan($data);
      $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data berhasil di Simpan</div></center>");
      if($simpan){
        redirect('Assets/index');
    }elseif($simtam){
        redirect('Assets/tambah_asset');
    }
}

public function edit($id)
{
    $data['coa_asset'] = $this->M_asset->coa_asset();
    $data['coa_akumulasi'] = $this->M_asset->coa_akumulasi();
    $data['coa_beban'] = $this->M_asset->coa_beban();
    $data['group_asset'] = $this->M_asset->group_asset();
    $data['Assets'] = $this->M_asset->getById($id);
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/asset/V_edit_asset', $data);
}

public function update()
{
    $data = array(
        'Kode_Asset' => $this->input->post('kode_asset'),
        'Nama_Asset' => $this->input->post('nama_asset'),
        'IDGroupAsset' => $this->input->post('group_asset'),
        'Coa_Asset' => $this->input->post('coa_asset'),
        'Coa_Akumulasi_Asset' => $this->input->post('coa_akumulasi'),
        'Coa_Beban_Asset' =>  $this->input->post('coa_beban')
    );

    $this->M_asset->update($data, array('IDAsset' => $this->input->post('id')));
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil diubah</div></center>");
    redirect('Assets/index');
}

public function delete($id)
{
    $this->M_asset->delete($id);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil dihapus</div></center>");
    redirect('Assets');
}

public function pencarian()
{
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $kolom= addslashes($this->input->post('jenispencarian'));
    $status= addslashes($this->input->post('statuspencarian'));
    $keyword= addslashes($this->input->post('keyword'));

    if ($kolom!="" && $status!="" && $keyword!="") {
        $data['assets']=$this->M_asset->cari($kolom,$status,$keyword);
    }elseif ($kolom!="" && $keyword!="") {
        $data['assets']=$this->M_asset->cari_by_kolom($kolom,$keyword);
    }elseif ($status!="") {
        $data['assets']=$this->M_asset->cari_by_status($status);
    }elseif ($keyword!="") {
        $data['assets']=$this->M_asset->cari_by_keyword($keyword);
    }else {
        $data['assets']=$this->M_asset->tampilkan_asset();
    }
    $this->load->view('administrator/asset/V_asset', $data);
}
}