
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class GroupBarang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_group_barang');
		$this->load->model('M_login');

		$this->load->helper('form', 'url');
	}

	public function index()
	{
		$data['groupbarang']=$this->M_group_barang->tampilkan_group_barang();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/group_barang/V_group_barang.php', $data);
	}

	public function tambah_group_barang()
	{
		
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/group_barang/V_tambah_group_barang', $data);
	}
	public function hapus_group_barang($id)
	{

		$this->M_group_barang->hapus_data_group_barang($id);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
		redirect('GroupBarang/index');
	}
	public function proses_group_barang()
	{
		$data['id_group_barang']=$this->M_group_barang->tampilkan_id_group_barang();
		if($data['id_group_barang']!=""){
			// foreach ($data['id_group_barang'] as $value) {
				$urutan= substr($data['id_group_barang']->IDGroupBarang, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$id_group_barang= $urutan_id;
		$kode= $this->input->post('kode');
		$nama = $this->input->post('nama');
		$status = $this->input->post('status');

		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$simbar= $this->input->post('simbar');


		$data=array(
			'IDGroupBarang' => $id_group_barang,
			'Kode_Group_Barang'=>$kode,
			'Group_Barang'=>$nama,
			'Aktif'=> 'aktif'
		);
		$this->M_group_barang->tambah_group_barang($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('GroupBarang/index');
		}elseif($simtam){
			redirect('GroupBarang/tambah_group_barang');
		}elseif($simbar){
			redirect('Barang/tambah_barang');
		}

		
	}

	public function edit_group_barang($id)
	{
		$data['edit']=$this->M_group_barang->ambil_group_barang_byid($id);
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/group_barang/V_edit_group_barang', $data);
	}

	public function proses_edit_group_barang($id)
	{

		$kode= $this->input->post('kode');
		$nama = $this->input->post('nama');
		$status = $this->input->post('status');
		
		
		$data=array(
			'Kode_Group_Barang'=>$kode,
			'Group_Barang'=>$nama
		);
		$this->M_group_barang->aksi_edit_group_barang($id, $data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Ubah</div></center>");
		redirect('GroupBarang/index');
	}

	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		
		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['groupbarang']=$this->M_group_barang->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['groupbarang']=$this->M_group_barang->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['groupbarang']=$this->M_group_barang->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['groupbarang']=$this->M_group_barang->cari_by_keyword($keyword);
		}else {
			$data['groupbarang']=$this->M_group_barang->tampilkan_group_barang();
		}
		$this->load->view('administrator/group_barang/V_group_barang', $data);
	}
}

?>