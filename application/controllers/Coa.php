
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Coa extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_coa');
		$this->load->model('M_login');
		$this->load->helper('form', 'url');
		$this->load->helper('convert_function');
	}

	public function index()
	{
		$data['coa']=$this->M_coa->tampilkan_coa();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/coa/V_coa', $data);
	}

	public function tambah_coa()
	{
		
		$data['groupcoa']= $this->M_coa->tampilkan_groupcoa();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/coa/V_tambah_coa', $data);
	}
	public function simpan()
	{
		$simpan= $this->input->post('simpan');
		$simtam= $this->input->post('simtam');
		$modal= str_replace(".", "", $this->input->post('Modal'));
		// $kredit= str_replace(".", "", $this->input->post('Kredit'));
		$data['id_coa'] = $this->M_coa->tampilkan_id_coa();
		if($data['id_coa']!=""){
			// foreach ($data['id_coa'] as $value) {
				$urutan= substr($data['id_coa']->IDCoa, 1);
			// }
			$hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
			$urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
		}else{
			$urutan_id = 'P000001';
		}
		$data = array(
			'IDCoa' => $urutan_id,
			'Kode_COA' => $this->input->post('Kode_COA'),
			'Nama_COA' => $this->input->post('Nama_COA'),
			'Status' => $this->input->post('Status'),
			'IDGroupCOA' => $this->input->post('Nama_Group'),
			'Modal' => $modal,
			'Aktif' => 'aktif'
		);

		$this->M_coa->simpan($data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Simpan</div></center>");
		if($simpan){
			redirect('Coa/tambah_coa');
		}elseif($simtam){
			redirect('Coa/tambah_coa');
		}
	}
	public function edit($id)
	{
		$data['coa'] = $this->M_coa->get_coa_byid($id);
		$data['groupcoa']= $this->M_coa->tampilkan_groupcoa();
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();
		$this->load->view('administrator/coa/V_edit_coa', $data);
	}
	public function update()
	{
		$modal= str_replace(".", "", $this->input->post('Modal'));
		// $kredit= str_replace(".", "", $this->input->post('Kredit'));

		$data = array(
			'Kode_COA' => $this->input->post('Kode_COA'),
			'Nama_COA' => $this->input->post('Nama_COA'),
			'Status' => $this->input->post('Status'),
			'IDGroupCOA' => $this->input->post('Nama_Group'),
			'Modal' => $modal
		);
		echo $this->input->post('id');
		$this->M_coa->update_coa($this->input->post('id'),$data);
		$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil diubah</div></center>");
		redirect('coa/index');
	}
	public function hapus_coa($id)
	{

		$data['coagetjurnal']= $this->M_coa->get_coa_jurnal($id);

		if(count($data['coagetjurnal']) > 0){
			if(($data['coagetjurnal']->IDCOA!=$id)){   
				$this->M_coa->hapus_data_coa($id);
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
				redirect('coa/index');
			}else{
				$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
				redirect('coa/index');
			}
		}else{
			$this->M_coa->hapus_data_coa($id);
			$this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil dihapus</div></center>");
			redirect('coa/index');
		}
	}
	public function pencarian()
	{
		$data['aksessetting']= $this->M_login->aksessetting();
		$data['aksesmenu']= $this->M_login->aksesmenu();
		$data['aksesmenudetail']= $this->M_login->aksesmenudetail();

		$kolom= addslashes($this->input->post('jenispencarian'));
		$status= addslashes($this->input->post('statuspencarian'));
		$keyword= addslashes($this->input->post('keyword'));

		if ($kolom!="" && $status!="" && $keyword!="") {
			$data['coa']=$this->M_coa->cari($kolom,$status,$keyword);
		}elseif ($kolom!="" && $keyword!="") {
			$data['coa']=$this->M_coa->cari_by_kolom($kolom,$keyword);
		}elseif ($status!="") {
			$data['coa']=$this->M_coa->cari_by_status($status);
		}elseif ($keyword!="") {
			$data['coa']=$this->M_coa->cari_by_keyword($keyword);
		}else {
			$data['coa']=$this->M_coa->tampilkan_coa();
		}
		$this->load->view('administrator/coa/V_coa', $data);
	}
}

?>