<!-- Programmer : Rais Naufal Hawari
 Date       : 12-07-2018 -->

 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Merk extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_merk');
    $this->load->model('M_login');
    $this->load->helper('form', 'url');
  }

  public function index()
  {
    $data = array(
        'title_master' => "Master Merk", //Judul 
        'title' => "List Data Merk", //Judul Tabel
        'action' => site_url('Merk/create'), //Alamat Untuk Action Form
        'button' => "Tambah Merk", //Nama Button
        'Merk' => $this->M_merk->get_all() //Load data Merk
      );
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/merk/merk_list.php', $data);
  }
  
  //Tambah Data Merk
  public function create()
  {
    $data = array(
        'title_master' => "Master Merk", //Judul 
        'title' => "Tambah Data Merk" , //Judul Tabel
        'action' => site_url('Merk/create_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Merk'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'Kode_Merk' => set_value('Kode_Merk'), //Kode Merk
        'IDMerk' => set_value('IDMerk'), //Id 
        'Merk' => set_value('Merk'), //Merk
        'Aktif' => set_value('Aktif') //Aktif
      );
    
    $data['aksessetting']= $this->M_login->aksessetting();
    $data['aksesmenu']= $this->M_login->aksesmenu();
    $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
    $this->load->view('administrator/merk/merk_form.php', $data);
  }
  
  //Tambah Data Merk
  public function create_action()
  {
    $data['id_merk'] = $this->M_merk->tampilkan_id_merk();
    if($data['id_merk']!=""){
      // foreach ($data['id_merk'] as $value) {
        $urutan= substr($data['id_merk']->IDMerk, 1);
      // }
      $hasil = base_convert(base_convert($urutan, 36, 10) + 1, 10, 36);
      $urutan_id= 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
    }else{
      $urutan_id = 'P000001';
    }
    $data = array(
      'IDMerk' => $urutan_id,
      'Kode_Merk' => $this->input->post('Kode_Merk'),
      'Merk' => $this->input->post('Merk'),
      'Aktif' => 'aktif'
      
    );
    $this->M_merk->insert($data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Simpan</div></center>");
    redirect('Merk/create');
  }
  
  //Edit Data Merk
  public function update($id)
  {
    $cari = $this->M_merk->get_by_id($id);//Cari apakah data yang dimaksud ada di database
    if($cari) {
      $data = array(
        'title_master' => "Master Merk", //Judul 
        'title' => "Ubah Data Merk" , //Judul Tabel
        'action' => site_url('Merk/update_action'), //Alamat Untuk Action Form
        'action_back' => site_url('Merk'),//Alamat Untuk back
        'button' => "Simpan Data", //Nama Button
        'Kode_Merk' => set_value('Kode_Merk', $cari->Kode_Merk), //Kode Merk
        'IDMerk' => set_value('IDMerk', $cari->IDMerk), //Id 
        'Merk' => set_value('Merk', $cari->Merk), //Merk
        'Aktif' => set_value('Aktif', $cari->Aktif) //Aktif
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $this->load->view('administrator/merk/merk_form.php', $data);
    } else {
      redirect('Merk/index');
    }
  }

  //Aksi Ubah Data
  public function update_action()
  {

    $id = $this->input->post('IDMerk'); //Get id sebagai penanda record yang akan dirubah
    $data = array(
      'Kode_Merk' => $this->input->post('Kode_Merk'),
      'Merk' => $this->input->post('Merk')
    );
    $this->M_merk->update($id,$data);
    $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Ubah</div></center>");
    redirect('Merk/index');
  }

  //Hapus Data Merk
  public function delete($id)
  {
      // $cari = $this->M_merk->get_by_id($id); //Cari apakah data yang dimaksud ada di database

      // if ($cari) {
      //   $this->M_merk->delete($id);
      //   $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di Hapus</div></center>");
      //   redirect('Merk/index');
      // } else {
      //   $this->session->set_flashdata('message', 'Maaf Hapus Gagal, Data Tidak Ditemukan');
      //   redirect('Merk/index');
      // }

      $data['merkget']= $this->M_merk->get_merk($id);
      $data['merkgetso']= $this->M_merk->get_merk_so($id);
      $data['merkgetsos']= $this->M_merk->get_merk_sos($id);
      $data['merkgetbarang']= $this->M_merk->get_barang($id);

    if(count($data['merkget']) > 0){
      if(($data['merkget']->IDMerk!=$id)){   
        $this->M_merk->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
       redirect('Merk/index');
      }else{
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
        redirect('Merk/index');
      }
    }elseif(count($data['merkgetso']) > 0){
      if(($data['merkgetso']->IDMerk!=$id)){   
        $this->M_merk->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
       redirect('Merk/index');
      }else{
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
        redirect('Merk/index');
      }
    }elseif(count($data['merkgetbarang']) > 0){
      if(($data['merkgetbarang']->IDMerk!=$id)){   
        $this->M_merk->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
       redirect('Merk/index');
      }else{
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
        redirect('Merk/index');
      }
    }elseif(count($data['merkgetsos']) > 0){
      if(($data['merkgetsos']->IDMerk!=$id)){   
        $this->M_merk->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
       redirect('Merk/index');
      }else{
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\"> Data Sudah Digunakan </div></center>");
        redirect('Merk/index');
      }
    }else{
      $this->M_merk->delete($id);
        $this->session->set_flashdata("Pesan", "<center><div class=\"pesan sukses\" id=\"alert\">Data berhasil di Hapus</div></center>");
        redirect('Merk/index');
    }
    }
    
    public function pencarian()
    {

      $data = array(
        'title_master' => "Master Merk", //Judul 
        'title' => "List Data Merk", //Judul Tabel
        'action' => site_url('Merk/create'), //Alamat Untuk Action Form
        'button' => "Tambah Merk", //Nama Button
      );
      $data['aksessetting']= $this->M_login->aksessetting();
      $data['aksesmenu']= $this->M_login->aksesmenu();
      $data['aksesmenudetail']= $this->M_login->aksesmenudetail();
      $kolom= addslashes($this->input->post('jenispencarian'));
      $status= addslashes($this->input->post('statuspencarian'));
      $keyword= addslashes($this->input->post('keyword'));

      if ($kolom!="" && $status!="" && $keyword!="") {
        $data['Merk']=$this->M_merk->cari($kolom,$status,$keyword);
      }elseif ($kolom!="" && $keyword!="") {
        $data['Merk']=$this->M_merk->cari_by_kolom($kolom,$keyword);
      }elseif ($status!="") {
        $data['Merk']=$this->M_merk->cari_by_status($status);
      }elseif ($keyword!="") {
        $data['Merk']=$this->M_merk->cari_by_keyword($keyword);
      }else {
        $data['Merk']=$this->M_merk->get_all();
      }
      $this->load->view('administrator/merk/merk_list', $data);
    }
    

  }

  /* End of file Merk.php */
